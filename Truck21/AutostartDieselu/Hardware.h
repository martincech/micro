//*****************************************************************************
//
//    Hardware.h   Autostart diesel hardware definitions
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "Cpu/uMKE06Z128xxx4.h"
#include "Cpu/Io.h"
#include "Unisys/Uni.h"

//-----------------------------------------------------------------------------
// Diesel 1
//-----------------------------------------------------------------------------
// O
#define RUN1_EN                     KINETIS_PIN_PORTC03
#define START1_EN                   KINETIS_PIN_PORTC02
#define GLOW1_EN                    KINETIS_PIN_PORTD06
#define STOPSOL1_EN                 KINETIS_PIN_PORTD07
// I
#define EXT1_ON                     KINETIS_PIN_PORTC06
#define EXT1_ON_PIN_FUNCTION        KINETIS_PIN_KBI0_P22_PORTC06_FUNCTION
#define OIL1_ERR                    KINETIS_PIN_PORTE03
#define OIL1_ERR_PIN_FUNCTION       KINETIS_PIN_KBI1_P3_PORTE03_FUNCTION
#define TEMP1_ERR                   KINETIS_PIN_PORTE02
#define TEMP1_ERR_PIN_FUNCTION      KINETIS_PIN_KBI1_P2_PORTE02_FUNCTION
#define AIR1_ERR                    KINETIS_PIN_PORTG03
#define AIR1_ERR_PIN_FUNCTION       KINETIS_PIN_KBI1_P19_PORTG03_FUNCTION

#define Diesel1PortInit()      PinFunction(RUN1_EN, KINETIS_GPIO_FUNCTION); \
                               PinFunction(START1_EN, KINETIS_GPIO_FUNCTION); \
                               PinFunction(GLOW1_EN, KINETIS_GPIO_FUNCTION); \
                               PinFunction(STOPSOL1_EN, KINETIS_GPIO_FUNCTION); \
                               PinFunction(EXT1_ON,   EXT1_ON_PIN_FUNCTION); \
                               PinFunction(OIL1_ERR,  OIL1_ERR_PIN_FUNCTION); \
                               PinFunction(TEMP1_ERR, TEMP1_ERR_PIN_FUNCTION); \
                               PinFunction(AIR1_ERR,  AIR1_ERR_PIN_FUNCTION); \
                               GpioOutput(RUN1_EN);GpioClr(RUN1_EN); \
                               GpioOutput(START1_EN);GpioClr(START1_EN); \
                               GpioOutput(GLOW1_EN);GpioClr(GLOW1_EN); \
                               GpioOutput(STOPSOL1_EN);GpioClr(STOPSOL1_EN)
                               

//-----------------------------------------------------------------------------
// Diesel 2
//-----------------------------------------------------------------------------
// O
#define RUN2_EN                     KINETIS_PIN_PORTD05
#define START2_EN                   KINETIS_PIN_PORTC01
#define GLOW2_EN                    KINETIS_PIN_PORTF07
#define STOPSOL2_EN                 KINETIS_PIN_PORTC00
//I
#define EXT2_ON                     KINETIS_PIN_PORTG02
#define EXT2_ON_PIN_FUNCTION        KINETIS_PIN_KBI1_P18_PORTG02_FUNCTION
#define OIL2_ERR                    KINETIS_PIN_PORTG01
#define OIL2_ERR_PIN_FUNCTION       KINETIS_PIN_KBI1_P17_PORTG01_FUNCTION
#define TEMP2_ERR                   KINETIS_PIN_PORTG00
#define TEMP2_ERR_PIN_FUNCTION      KINETIS_PIN_KBI1_P16_PORTG00_FUNCTION
#define AIR2_ERR                    KINETIS_PIN_PORTC05
#define AIR2_ERR_PIN_FUNCTION       KINETIS_PIN_KBI0_P21_PORTC05_FUNCTION

#define Diesel2PortInit()      PinFunction(RUN2_EN, KINETIS_GPIO_FUNCTION); \
                               PinFunction(START2_EN, KINETIS_GPIO_FUNCTION); \
                               PinFunction(GLOW2_EN, KINETIS_GPIO_FUNCTION); \
                               PinFunction(STOPSOL2_EN, KINETIS_GPIO_FUNCTION); \
                               PinFunction(EXT2_ON,   EXT2_ON_PIN_FUNCTION); \
                               PinFunction(OIL2_ERR,  OIL2_ERR_PIN_FUNCTION); \
                               PinFunction(TEMP2_ERR, TEMP2_ERR_PIN_FUNCTION); \
                               PinFunction(AIR2_ERR,  AIR2_ERR_PIN_FUNCTION); \
                               GpioOutput(RUN2_EN);GpioClr(RUN2_EN); \
                               GpioOutput(START2_EN);GpioClr(START2_EN); \
                               GpioOutput(GLOW2_EN);GpioClr(GLOW2_EN); \
                               GpioOutput(STOPSOL2_EN);GpioClr(STOPSOL2_EN)
//-----------------------------------------------------------------------------
// Electro motor
//-----------------------------------------------------------------------------
//O
#define ELMOT_EN                    KINETIS_PIN_PORTA07
//I
#define ELMOT_ON                    KINETIS_PIN_PORTC07
#define ELMOT_ON_PIN_FUNCTION       KINETIS_PIN_KBI0_P23_PORTC07_FUNCTION
#define WALL_AVAILABLE              KINETIS_PIN_PORTD02
#define WALL_AVAILABLE_PIN_FUNCTION KINETIS_PIN_KBI0_P26_PORTD02_FUNCTION

#define ElmotPortInit()      PinFunction(ELMOT_EN, KINETIS_GPIO_FUNCTION); \
                             PinFunction(ELMOT_ON, ELMOT_ON_PIN_FUNCTION); \
                             PinFunction(WALL_AVAILABLE, WALL_AVAILABLE_PIN_FUNCTION); \
                             GpioOutput(ELMOT_EN);GpioClr(ELMOT_EN)

//-----------------------------------------------------------------------------
// Keyboard control
//-----------------------------------------------------------------------------
//I
#define SWITCH_TRUCK_TRAILER              KINETIS_PIN_PORTA06
#define SWITCH_TRUCK_TRAILER_PIN_FUNCTION KINETIS_PIN_KBI0_P6_PORTA06_FUNCTION
#define SWITCH_HYDRO                      KINETIS_PIN_PORTE04
#define SWITCH_HYDRO_PIN_FUNCTION         KINETIS_PIN_KBI1_P4_PORTE04_FUNCTION
#define SWITCH_ELMOT                      KINETIS_PIN_PORTF01
#define SWITCH_ELMOT_PIN_FUNCTION         KINETIS_PIN_KBI1_P9_PORTF01_FUNCTION
#define SWITCH_DIESEL                     KINETIS_PIN_PORTF00
#define SWITCH_DIESEL_PIN_FUNCTION        KINETIS_PIN_KBI1_P8_PORTF00_FUNCTION
#define KBD_ON_OFF                        KINETIS_PIN_PORTB02
#define KBD_ON_OFF_PIN_FUNCTION           KINETIS_PIN_KBI0_P10_PORTB02_FUNCTION
#define KBD_START                         KINETIS_PIN_PORTB03
#define KBD_START_PIN_FUNCTION            KINETIS_PIN_KBI0_P11_PORTB03_FUNCTION
  
#define KbdPortInit()        PinFunction(SWITCH_TRUCK_TRAILER, SWITCH_TRUCK_TRAILER_PIN_FUNCTION); \
                             PinFunction(SWITCH_HYDRO, SWITCH_HYDRO_PIN_FUNCTION); \
                             PinFunction(SWITCH_ELMOT, SWITCH_ELMOT_PIN_FUNCTION); \
                             PinFunction(SWITCH_DIESEL, SWITCH_DIESEL_PIN_FUNCTION); \
                             PinFunction(KBD_ON_OFF, KBD_ON_OFF_PIN_FUNCTION); \
                             PinFunction(KBD_START, KBD_START_PIN_FUNCTION)

//-----------------------------------------------------------------------------
// Ventilation system
//-----------------------------------------------------------------------------
//O
#define VENT_START_EN               KINETIS_PIN_PORTB00
//I
#define VENT_RQST_ON                KINETIS_PIN_PORTA01
#define VENT_RQST_ON_PIN_FUNCTION   KINETIS_PIN_KBI0_P8_PORTB00_FUNCTION


#define VentPortInit()       PinFunction(VENT_START_EN, KINETIS_GPIO_FUNCTION); \
                             PinFunction(VENT_RQST_ON, VENT_RQST_ON_PIN_FUNCTION); \
                             GpioOutput(VENT_START_EN);GpioClr(VENT_START_EN)
//-----------------------------------------------------------------------------
// Cooling system
//-----------------------------------------------------------------------------
//O
#define COOL_START_EN               KINETIS_PIN_PORTB01
//I
#define COOL_RQST_ON                KINETIS_PIN_PORTA00
#define COOL_RQST_ON_PIN_FUNCTION   KINETIS_PIN_KBI0_P0_PORTA00_FUNCTION

#define CoolPortInit()              PinFunction(COOL_START_EN, KINETIS_GPIO_FUNCTION); \
                                    PinFunction(COOL_RQST_ON, COOL_RQST_ON_PIN_FUNCTION); \
                                    GpioOutput(COOL_START_EN);GpioClr(COOL_START_EN)

//-----------------------------------------------------------------------------
// Indicators & signalization
//-----------------------------------------------------------------------------
//O
#define LED_GLOW                    KINETIS_PIN_PORTI04
#define LED_EXT                     KINETIS_PIN_PORTH01
#define LED_OIL                     KINETIS_PIN_PORTH00
#define LED_TEMP                    KINETIS_PIN_PORTE06
#define LED_SUPPLY_EN               KINETIS_PIN_PORTB05
#define LED_AIR                     KINETIS_PIN_PORTE05
#define SIRENA_EN                   KINETIS_PIN_PORTH07
#define DPLUS                       KINETIS_PIN_PORTD00
#define TMC_SIGNAL                  KINETIS_PIN_PORTD01


#define IndicatorsPortInit()        PinFunction(LED_GLOW, KINETIS_GPIO_FUNCTION); \
                                    PinFunction(LED_EXT, KINETIS_GPIO_FUNCTION); \
                                    PinFunction(LED_OIL, KINETIS_GPIO_FUNCTION); \
                                    PinFunction(LED_TEMP, KINETIS_GPIO_FUNCTION); \
                                    PinFunction(LED_SUPPLY_EN, KINETIS_GPIO_FUNCTION); \
                                    PinFunction(LED_AIR, KINETIS_GPIO_FUNCTION); \
                                    PinFunction(SIRENA_EN, KINETIS_GPIO_FUNCTION); \
                                    PinFunction(DPLUS, KINETIS_GPIO_FUNCTION); \
                                    PinFunction(TMC_SIGNAL, KINETIS_GPIO_FUNCTION); \
                                    GpioOutput(LED_GLOW);GpioClr(LED_GLOW); \
                                    GpioOutput(LED_EXT);GpioClr(LED_EXT); \
                                    GpioOutput(LED_OIL);GpioClr(LED_OIL); \
                                    GpioOutput(LED_TEMP);GpioClr(LED_TEMP); \
                                    GpioOutput(LED_SUPPLY_EN);GpioClr(LED_SUPPLY_EN); \
                                    GpioOutput(LED_AIR);GpioClr(LED_AIR); \
                                    GpioOutput(SIRENA_EN);GpioClr(SIRENA_EN); \
                                    GpioOutput(DPLUS);GpioClr(DPLUS); \
                                    GpioOutput(TMC_SIGNAL);GpioClr(TMC_SIGNAL)

//-----------------------------------------------------------------------------
// Analog measurement
//-----------------------------------------------------------------------------
#define BATTERY_CMEAS               KINETIS_PIN_PORTF04
#define BATTERY_CMEAS_PIN_FUNCTION  KINETIS_PIN_ADC0_SE12_PORTF04_FUNCTION
#define TAHAC_UMEAS                 KINETIS_PIN_PORTF05
#define TAHAC_UMEAS_PIN_FUNCTION    KINETIS_PIN_ADC0_SE13_PORTF05_FUNCTION
#define NAVES_UMEAS                 KINETIS_PIN_PORTF06
#define NAVES_UMEAS_PIN_FUNCTION    KINETIS_PIN_ADC0_SE14_PORTF06_FUNCTION

#define AnalogPortInit()            PinFunction(BATTERY_CMEAS, BATTERY_CMEAS_PIN_FUNCTION); \
                                    PinFunction(BATTERY_CMEAS, BATTERY_CMEAS_PIN_FUNCTION); \
                                    PinFunction(BATTERY_CMEAS, BATTERY_CMEAS_PIN_FUNCTION)

//-----------------------------------------------------------------------------
//CAN BUS
//-----------------------------------------------------------------------------
#define CAN_TX                      KINETIS_PIN_PORTE07
#define CAN_TX_PIN_FUNCTION         KINETIS_PIN_CAN0_TX_PORTE07_FUNCTION
#define CAN_RX                      KINETIS_PIN_PORTH02
#define CAN_RX_PIN_FUNCTION         KINETIS_PIN_CAN0_RX_PORTH02_FUNCTION
#define CAN_STBY                    KINETIS_PIN_PORTH06
#define CAN_STBY_PIN_FUNCTION       KINETIS_GPIO_FUNCTION

#define CanPortInit()               PinFunction(CAN_TX, CAN_TX_PIN_FUNCTION); \
                                    PinFunction(CAN_RX, CAN_RX_PIN_FUNCTION); \
                                    PinFunction(CAN_STBY, CAN_STBY_PIN_FUNCTION)

#endif