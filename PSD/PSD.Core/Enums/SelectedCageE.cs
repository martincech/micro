﻿namespace PSD.Core.Enums
{
   public enum SelectedCageE
   {
      Unselected = -1,
      EntryCage = 0,
      Cage = 1
   }
}
