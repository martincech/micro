﻿namespace PSD.Core.Enums
{
   public enum DeleteTypeE
   {
      Unknown = -1,
      Cage = 0,
      Parameter = 1
   }
}
