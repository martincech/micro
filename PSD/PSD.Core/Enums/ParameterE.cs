namespace PSD.Core.Enums
{
   public enum ParameterE
   {
      ParameterTwin,
      ParameterCracked,
      ParameterMembrane,
      ParameterDead,
      ParameterOk
   }  
}