﻿namespace PSD.Core.Models
{
   public class CommandKey
   {
      public int Command;
      public string Text;
      public int KeyCode;

      public CommandKey(int command, string text, int keyCode)
      {
         Command = command;
         Text = text;
         KeyCode = keyCode;
      }
   }
}
