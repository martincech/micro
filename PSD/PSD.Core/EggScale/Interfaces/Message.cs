﻿namespace PSD.Core.EggScale.Interfaces
{
   public enum Message
   {
      StateChange,
      Read,
      Write,
      DeviceName,
      Toast
   }
}
