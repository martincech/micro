﻿namespace PSD.Core.EggScale.Interfaces
{
   public enum State
   {
      None,          // we're doing nothing
      Listen,        // now listening for incoming connections
      Connecting,    // now initiating an outgoing connection
      Connected,     // now connected to a remote device
      Ok,
      Error
   }
}
