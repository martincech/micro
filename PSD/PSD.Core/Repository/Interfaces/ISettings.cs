﻿using System.Collections.Generic;
using PSD.Core.Models;

namespace PSD.Core.Repository.Interfaces
{
   public interface ISettings
   {
      string PickerName { get; set; }
      string LastFileName { get; set; }
      string LastFileFolder { get; set; }
      IEnumerable<CommandKey> Keys { get; set; }

      void Save(object context);
      void Load(object context);
      void LoadDefault();
   }
}
