﻿using System;
using PSD.Core.Enums;

namespace PSD.Core.Helpers
{
   public class DeleteCageContent
   {
      public DeleteTypeE Type;
      public double PositionX;                  // x position for detect delete gesture
      public double PositionY;                  // y position for detect delete gesture
      public int Position = -1;                 // index of deleting cage
      public int ParameterIndex = -1;           // index of parameter at cage
      
      private readonly double deleteDistance;   // value for delete authorization
      private const int DELETE_Y_DISTANCE = 80; // deviation of axis-y

      /// <summary>
      /// Constructor for set deleting parameters.
      /// </summary>
      /// <param name="widthPixels">current display width</param>
      public DeleteCageContent(double widthPixels)
      {
         Type = DeleteTypeE.Unknown;
         deleteDistance = widthPixels * 0.2;
      }

      /// <summary>
      /// Check distance between start and end of gesture to control 
      /// if user want delete content.
      /// </summary>
      /// <param name="currentPositionX">end position of X</param>
      /// <param name="currentPositionY">end position of Y</param>
      /// <returns>true - gesture to delete content</returns>
      public bool CanDelete(double currentPositionX, double? currentPositionY = null)
      {
         if (currentPositionY == null)
         {
            return currentPositionX - PositionX > deleteDistance;
         }

         return currentPositionX - PositionX > deleteDistance && 
            Math.Abs(currentPositionY.Value - PositionY) < DELETE_Y_DISTANCE;
      }
   }
}
