using Android.Content.Res;
using Android.Widget;

namespace PSD.Droid.Helpers
{
   public static class ListViewHelpers
   {
      private const int LOWER_DENSITY_CONST = 54;
      private const int HIGHER_DENSITY_CONST = 56;
      private const double THRESHOLD = 1.5;

      public static int Density
      {
         get
         {
            var density = Resources.System.DisplayMetrics.Density;
            var @const = HIGHER_DENSITY_CONST;
            if (density < THRESHOLD)
            {
               @const = LOWER_DENSITY_CONST;
            }
            return (int)(density * @const);
         }
      }

      /// <summary>
      /// Dynamically set height of <see cref="ListView"/> based on its content
      /// </summary>
      /// <param name="listView">A <see cref="ListView"/> to set height on</param>
      public static int SetListViewHeightBasedOnItems(this ListView listView)
      {
         var listAdapter = listView.Adapter;
         if (listAdapter == null) return 0;

         var numberOfItems = listAdapter.Count;
         // Get total height of all items.
         int totalItemsHeight;
         // empty cage without any parameters
         if (numberOfItems == 0)
         {
            totalItemsHeight = Density;
         }
         else
         {
            totalItemsHeight = Density * numberOfItems;
         }
         listView.VerticalScrollBarEnabled = false;                

         // Get total height of all item dividers.
         var totalDividersHeight = listView.DividerHeight * (numberOfItems - 1);

         // Set list height.
         var @params = listView.LayoutParameters;
         @params.Height = totalItemsHeight + totalDividersHeight;
         listView.LayoutParameters = @params;
         listView.RequestLayout();
         return @params.Height;
      }
   }
}