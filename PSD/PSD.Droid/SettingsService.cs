using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Preferences;
using Android.Views;
using PSD.Core.Models;
using PSD.Core.Repository.Interfaces;

namespace PSD.Droid
{
   public class SettingsService : ISettings
   {
      #region Private fields

      // Constants for parsing data
      private const string PICKER_NAME = "PickerName";
      private const string LAST_FILE_NAME = "LastFileName";
      private const string LAST_FILE_FOLDER = "LastFileFolder";

      // Control keys
      /// <summary>
      /// Predefined control keys.
      /// Key - string number (adroid resource string id)
      /// Value - keycode
      /// </summary>
      private readonly IEnumerable<KeyValuePair<int, int>> predefinedKeys;

      // Default keys values
      private const int TWIN_KEY_CODE_DEFAULT = (int) Keycode.F1;
      private const int CRACKED_KEY_CODE_DEFAULT = (int) Keycode.F2;
      private const int MEMBRANE_KEY_CODE_DEFAULT = (int) Keycode.F3;
      private const int MORTALITY_KEY_CODE_DEFAULT = (int) Keycode.F4;

      private const int NUM0_KEY_CODE_DEFAULT = (int)Keycode.Num0;
      private const int NUM1_KEY_CODE_DEFAULT = (int)Keycode.Num1;
      private const int NUM2_KEY_CODE_DEFAULT = (int)Keycode.Num2;
      private const int NUM3_KEY_CODE_DEFAULT = (int)Keycode.Num3;
      private const int NUM4_KEY_CODE_DEFAULT = (int)Keycode.Num4;
      private const int NUM5_KEY_CODE_DEFAULT = (int)Keycode.Num5;
      private const int NUM6_KEY_CODE_DEFAULT = (int)Keycode.Num6;
      private const int NUM7_KEY_CODE_DEFAULT = (int)Keycode.Num7;
      private const int NUM8_KEY_CODE_DEFAULT = (int)Keycode.Num8;
      private const int NUM9_KEY_CODE_DEFAULT = (int)Keycode.Num9;

      #endregion

      #region Public interfaces

      #region Constructor

      public SettingsService()
      {
         PickerName = "";
         LastFileFolder = "";
         LastFileName = "";

         predefinedKeys = new List<KeyValuePair<int, int>>
         {
            new KeyValuePair<int, int>(Resource.String.Twin, TWIN_KEY_CODE_DEFAULT),
            new KeyValuePair<int, int>(Resource.String.Cracked, CRACKED_KEY_CODE_DEFAULT),
            new KeyValuePair<int, int>(Resource.String.Membrane, MEMBRANE_KEY_CODE_DEFAULT),
            new KeyValuePair<int, int>(Resource.String.Mortality, MORTALITY_KEY_CODE_DEFAULT),
            new KeyValuePair<int, int>(Resource.String.Number0, NUM0_KEY_CODE_DEFAULT),
            new KeyValuePair<int, int>(Resource.String.Number1, NUM1_KEY_CODE_DEFAULT),
            new KeyValuePair<int, int>(Resource.String.Number2, NUM2_KEY_CODE_DEFAULT),
            new KeyValuePair<int, int>(Resource.String.Number3, NUM3_KEY_CODE_DEFAULT),
            new KeyValuePair<int, int>(Resource.String.Number4, NUM4_KEY_CODE_DEFAULT),
            new KeyValuePair<int, int>(Resource.String.Number5, NUM5_KEY_CODE_DEFAULT),
            new KeyValuePair<int, int>(Resource.String.Number6, NUM6_KEY_CODE_DEFAULT),
            new KeyValuePair<int, int>(Resource.String.Number7, NUM7_KEY_CODE_DEFAULT),
            new KeyValuePair<int, int>(Resource.String.Number8, NUM8_KEY_CODE_DEFAULT),
            new KeyValuePair<int, int>(Resource.String.Number9, NUM9_KEY_CODE_DEFAULT)
         };
      }

      #endregion

      #region Implementation of ISettings

      public string PickerName { get; set; } // Picker name
      public string LastFileName { get; set; } // Last campaign name used for file
      public string LastFileFolder { get; set; } // Last name used for folder

      public IEnumerable<CommandKey> Keys { get; set; } // Collection of control keys

      public void Save(object context)
      {
         var preferences = PreferenceManager.GetDefaultSharedPreferences((Context) context);
         var editor = preferences.Edit();
         editor.PutString(PICKER_NAME, PickerName);
         editor.PutString(LAST_FILE_NAME, LastFileName);
         editor.PutString(LAST_FILE_FOLDER, LastFileFolder);
         SaveKeys(editor);
         editor.Apply();
      }

      public void Load(object context)
      {
         var preferences = PreferenceManager.GetDefaultSharedPreferences((Context) context);
         PickerName = preferences.GetString(PICKER_NAME, "");
         var defaultName = Program.GetDefaultName();
         var fileDefName = defaultName;
         if (!PickerName.Equals(""))
         {
            fileDefName += "_" + PickerName;
         }
         LastFileName = preferences.GetString(LAST_FILE_NAME, fileDefName);
         LastFileFolder = preferences.GetString(LAST_FILE_FOLDER, defaultName);

         LoadKeys(preferences);
      }

      public void Delete(object context)
      {
         var preferences = PreferenceManager.GetDefaultSharedPreferences((Context) context);
         var editor = preferences.Edit();
         editor.Clear();
         editor.Apply();
         LoadDefault();
      }

      public void LoadDefault()
      {
         // Default control keys
         Keys = predefinedKeys.Select(item => new CommandKey(
            item.Key, Application.Context.Resources.GetString(item.Key), item.Value))
            .ToList();
      }

      #endregion

      #endregion

      #region Private helpers

      private void LoadKeys(ISharedPreferences preferences)
      {
         if (preferences == null) return;

         var list = new List<CommandKey>();
         foreach (var item in predefinedKeys)
         {
            var value = preferences.GetInt(item.Key.ToString(), item.Value);
            list.Add(new CommandKey(item.Key, Application.Context.Resources.GetString(item.Key), value));
         }
         Keys = list;
      }

      private void SaveKeys(ISharedPreferencesEditor editor)
      {
         if (editor == null) return;

         foreach (var item in Keys)
         {
            editor.PutInt(item.Command.ToString(), item.KeyCode);
         }
      }

      #endregion
   }
}