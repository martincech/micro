using Android.Widget;

namespace PSD.Droid.ViewHolders
{
   public class KeySettingsItemViewHolder : Java.Lang.Object
   {
      public TextView Command { get; set; }
      public TextView KeyCode { get; set; }
      public ImageView WarningImage { get; set; }
   }
}