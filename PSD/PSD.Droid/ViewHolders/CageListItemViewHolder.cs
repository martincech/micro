using Android.Widget;

namespace PSD.Droid.ViewHolders
{
   public class CageListItemViewHolder : Java.Lang.Object
   {
      public RelativeLayout Layout { get; set; }

      public TextView NoParameters { get; set; }
      public EditText CageNumber { get; set; }
      public TextView DividerHorizontal { get; set; }
      public ListView CageContent { get; set; }
   }
}