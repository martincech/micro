﻿using BatLibrary;
using Utilities.Observable;

namespace DesktopTests.Converters
{
   public class ConvertorViewModel : ObservableObject
   {
      private Weight weightOne;
      private Weight weightTwo;

      private Weight.WeightUnits unitOne;
      private Weight.WeightUnits unitTwo;

      public Weight WeightOne
      {
         get { return weightOne; }
         set { SetProperty(ref weightOne, value); }
      }

      public Weight.WeightUnits UnitOne
      {
         get { return unitOne; }
         set
         {
            SetProperty(ref unitOne, value);
            UpdateWeights();
         }
      }

      public Weight WeightTwo
      {
         get { return weightTwo; }
         set { SetProperty(ref weightTwo, value); }
      }

      public Weight.WeightUnits UnitTwo
      {
         get { return unitTwo; }
         set
         {
            SetProperty(ref unitTwo, value);
            UpdateWeights();
         }
      }

      private void UpdateWeights()
      {
         RaisePropertyChanged("WeightOne");
         RaisePropertyChanged("WeightTwo");
      }
   }
}
