﻿using System;
using System.IO;
using BatLibrary;
using Communication.Readers;
using Communication.Readers.Interfaces;
using Communication.Samples;
using Communication.SensorManagers;
using Recognition;
using Recognition.Types;

namespace WeightReadTester
{
   class Program
   {
      private static IWeightReader _realWeightReader;
      private static FCFDARecognition _recognizer;
      private static ITemperatureReader _temperatureReader;
      private static ICo2Reader _co2Reader;
      private static IHumidityReader _humidityReader;
      private const string AVERAGING_FILE = "AverageFilter.csv";
      private const string ACCEPTANCE_FILE = "Acceptance.csv";
      private const string STABLE_FILE = "StableFilter.csv";
      private const string DETECTION_FILE = "Detection.csv";
      private const string CALIBRATION_FILE = "Calibration.csv";
      private const string TEMPERATURE_FILE = "Temperature.csv";
      private const string CO2_FILE = "Co2.csv";
      private const string HUMIDITY_FILE = "Humidity.csv";

      static void Main(string[] args)
      {
         _recognizer = new FCFDARecognition();
         _recognizer.AveragingFiltration.Recognized += AveragingFiltrationOnRecognized;
         _recognizer.Calibration.Recognized += CalibrationOnRecognized;
         _recognizer.Detection.Recognized += DetectionOnRecognized;
         _recognizer.StableFiltration.Recognized += StableFiltrationOnRecognized;
         _recognizer.Acceptance.Recognized += AcceptanceOnRecognized;
         var elo = new EloConnectedSensorsManager();
         elo.DeviceConnected += EloOnDeviceConnected;

         Console.ReadKey();
      }

      private static void AcceptanceOnRecognized(object sender, BirdWeight birdWeight)
      {
         WriteToFile(ACCEPTANCE_FILE, birdWeight);
      }

      private static void StableFiltrationOnRecognized(object sender, StableWeight stableWeight)
      {
         WriteToFile(STABLE_FILE, stableWeight);
      }

      private static void DetectionOnRecognized(object sender, DifferenceWeight differenceWeight)
      {
         WriteToFile(DETECTION_FILE, differenceWeight);
      }

      private static void CalibrationOnRecognized(object sender, CalibratedWeight calibratedWeight)
      {
         WriteToFile(CALIBRATION_FILE, calibratedWeight);
      }

      private static void AveragingFiltrationOnRecognized(object sender, AverageWeight averageWeight)
      {
         WriteToFile(AVERAGING_FILE, averageWeight);
      }

      private static void WriteToFile(string file, Weight weight)
      {
         WriteToFile(file, weight.AsG);
      }

      private static void WriteToFile(string file, double value)
      {
         using (var writer = File.AppendText(file))
         {
            writer.Write("{0};{1}\n", DateTime.Now, value);
         }
      }

      private static void EloOnDeviceConnected(object sender, ISensorReaderPack sensorReader)
      {
         _realWeightReader = sensorReader.WeightReader;
         _realWeightReader.NewSample += RealWeightReaderOnNewSample;
         _realWeightReader.StartReading();

         _temperatureReader = sensorReader.TemperatureReader;
         _temperatureReader.NewSample += TemperatureReaderOnNewSample;
         _temperatureReader.StartReading();

         _co2Reader = sensorReader.Co2Reader;
         _co2Reader.NewSample += Co2ReaderOnNewSample;
         _co2Reader.StartReading();

         _humidityReader = sensorReader.HumidityReader;
         _humidityReader.NewSample += HumidityReaderOnNewSample;
         _humidityReader.StartReading();
      }

      private static void HumidityReaderOnNewSample(object sender, HumiditySample humiditySample)
      {
         WriteToFile(HUMIDITY_FILE, humiditySample.Value);
      }

      private static void Co2ReaderOnNewSample(object sender, Co2Sample co2Sample)
      {
         WriteToFile(CO2_FILE, co2Sample.Value);
      }

      private static void TemperatureReaderOnNewSample(object sender, TemperatureSample temperatureSample)
      {
         WriteToFile(TEMPERATURE_FILE, temperatureSample.Value);
      }

      private static void RealWeightReaderOnNewSample(object sender, WeightSample weightSample)
      {
         _recognizer.Add(new RawWeight(weightSample.ToUnit(Weight.WeightUnits.G)));
      }
   }
}
