﻿@CurrentWeight
Feature: Show current weight on the screen
	In order to get the current weight of the birds in the flock
	As a farmer
	I want to see the current weight on the screen

Scenario Outline: Current weight on the platform is visible on the screen in selected units
	Given A raw <Signal> from the weight sensor is generated 
   And I select weighing <Unit>
	When I look on the screen
	Then the <Current> of bird is shown on the screen
Examples: 
| Unit | Signal						  | Current   |
| Kg   | 12500,12210,12850,15530	  | 1.32725   |
| Lb   | 12500,12210,12850,15530	  | 2.92609   |