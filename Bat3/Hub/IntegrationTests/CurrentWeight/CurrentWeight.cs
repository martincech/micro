﻿using System;
using System.Collections.Generic;
using System.Linq;
using BatLibrary;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace IntegrationTests.CurrentWeight
{
   [Binding]
   public sealed class AverageWeight : StepDefinitionsBase
   {
      private void Given()
      {
         ConnectSensorPack();
         App.MockMainWindowView.StartReading();
      }

      [Given(@"A raw (.*) from the weight sensor is generated")]
      public void GivenARawFromTheWeightSensorIsGenerated(IEnumerable<int> values)
      {
         Given();

         foreach (var value in values)
         {
            App.MockSensors.MockWeightReader.Raise(r => r.NewSample += null, null, new Communication.Samples.WeightSample(value, DateTime.Now));
         }
      }

      [Given(@"I select weighing (.*)")]
      public void GivenISelectWeighingKg(Weight.WeightUnits unit)
      {
         App.MockMainWindowView.Unit = unit;
      }

      [When(@"I look on the screen")]
      public void WhenILookOnTheScreen()
      {
         //?
      }

      [Then(@"the (.*) of bird is shown on the screen")]
      public void ThenTheOfBirdIsShownOnTheScreen(double average)
      {
         Assert.AreEqual(Math.Round(average, 4), Math.Round(App.MockMainWindowView.CurrentWeight, 4));
      }
   }
}
