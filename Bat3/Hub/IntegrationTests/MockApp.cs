﻿using BatLibrary;
using Communication.Readers;
using Communication.Readers.Interfaces;
using Communication.SensorManagers;
using Core.WeighingProcess.Weight;
using Desktop.ViewModels;
using Desktop.Wpf.Presentation;
using Moq;
using Ninject;
using UI.Windows;

namespace IntegrationTests
{
   public class MockApp
   {
      public MockApp()
      {
         DispatcherHelper.Initialize();
         MockSensors = new MockSensors();
        
         var iocKernel = new StandardKernel();
         App.InitIoc(iocKernel);
         iocKernel.Rebind<ISensorsManager>().ToConstant(MockSensors.ConnectedSensorsManager.Object);

         MockMainWindowView = new MockMainWindowView(App.GetMainWindowViewModel(iocKernel));
         WeighingProcess = iocKernel.Get<IWeighingProcess>();
      }

      public MockSensors MockSensors { get; set; }
      public MockMainWindowView MockMainWindowView { get; set; }

      public IWeighingProcess WeighingProcess { get; set; }
   }

   public class MockSensors
   {
      public MockSensors()
      {
         MockCo2Reader = new Mock<ICo2Reader>();
         MockTemperatureReader = new Mock<ITemperatureReader>();
         MockHumidityReader = new Mock<IHumidityReader>();
         MockWeightReader = new Mock<IWeightReader>();

         MockSensorReaderPack = new Mock<ISensorReaderPack>();
         MockSensorReaderPack.Setup(s => s.Co2Reader).Returns(MockCo2Reader.Object);
         MockSensorReaderPack.Setup(s => s.TemperatureReader).Returns(MockTemperatureReader.Object);
         MockSensorReaderPack.Setup(s => s.HumidityReader).Returns(MockHumidityReader.Object);
         MockSensorReaderPack.Setup(s => s.WeightReader).Returns(MockWeightReader.Object);

         ConnectedSensorsManager = new Mock<ISensorsManager>();
      }

      public Mock<ISensorsManager> ConnectedSensorsManager { get; set; }

      public Mock<ISensorReaderPack> MockSensorReaderPack { get; set; }

      public Mock<IWeightReader> MockWeightReader { get; set; }

      public Mock<ICo2Reader> MockCo2Reader { get; set; }

      public Mock<ITemperatureReader> MockTemperatureReader { get; set; }

      public Mock<IHumidityReader> MockHumidityReader { get; set; }
   }

   public class MockMainWindowView
   {
      private readonly MainWindowViewModel mainWindowViewModel;

      public MockMainWindowView(MainWindowViewModel mainWindowViewModel)
      {
         this.mainWindowViewModel = mainWindowViewModel;
      }

      public void StartReading()
      {
         mainWindowViewModel.RunRealCommand.Execute(null);
      }

      public Weight.WeightUnits Unit {
         get { return mainWindowViewModel.WeightUnit; }
         set { mainWindowViewModel.WeightUnit = value; }
      }

      public double CurrentWeight { get { return mainWindowViewModel.Weight.As(Unit); } }

      #region Implementation of IWeightStatistics

      /// <summary>
      /// Number of birds weighted today
      /// </summary>
      public int Count
      {
         get { return mainWindowViewModel.Count; }
      }

      /// <summary>
      /// Average weight of the birds
      /// </summary>
      public double Average
      {
         get { return mainWindowViewModel.Average.As(Unit); }
        
      }

      /// <summary>
      /// Uniformity of the birds
      /// </summary>
      public double Uniformity
      {
         get { return mainWindowViewModel.Uniformity; }
      }

      /// <summary>
      /// Coeficient of variation of the birds
      /// </summary>
      public double Cv
      {
         get { return mainWindowViewModel.Cv; }
      }

      /// <summary>
      /// Standard deviation
      /// </summary>
      public double Sigma
      {
         get { return mainWindowViewModel.Sigma; }
      }

      /// <summary>
      /// Gain from yesterday
      /// </summary>
      public double Gain
      {
         get { return mainWindowViewModel.Gain.As(Unit); }
      }

      #endregion
   }
}
