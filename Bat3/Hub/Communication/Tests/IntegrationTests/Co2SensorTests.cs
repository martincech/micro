﻿using System;
using System.Reflection;
using Communication.Readers;
using Communication.Samples;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Communication.Tests.IntegrationTests
{
   [TestClass]
   public class Co2SensorTests
   {
      private Co2ReaderTestBase co2Test;
      private double readInterval;

      [TestInitialize]
      public void Init()
      {
         co2Test = new Co2ReaderTestBase();
         var sensor = co2Test.CreateSensorConnection();
         var reader = new Co2Reader(sensor);
         co2Test.SetReader(reader);

         var po = new PrivateObject(reader);
         readInterval = (double)po.GetField("SAMPLE_RATE_READ_PERIOD_MS", BindingFlags.NonPublic | BindingFlags.Static);
         co2Test.Interval = (int)Math.Ceiling(readInterval);
      }

      [TestMethod]
      public void Read_OneSample()
      {
         co2Test.NewSampleFired_WhenStartReading();
      }

      [TestMethod]
      public void Read_SameSampleMultiply()
      {
         co2Test.ReadSameSample(true);
      }

      [TestMethod]
      public void Read_MultipleDifferentSamples()
      {
         co2Test.NewSampleFiredMultiple_WhenStartReading(false);
      }

      [TestMethod]
      public void NotRead_WhenStopReading()
      {
         co2Test.NewSampleNotFired_WhenStopReading(false);
      }

      [TestMethod]
      public void Read_WhenRestartReading()
      {
         co2Test.NewSampleFired_WhenRestartReading(false);
      }
   }

   internal class Co2ReaderTestBase : ReaderTest<Co2Reader, Co2Sample, long>
   {
   }
}
