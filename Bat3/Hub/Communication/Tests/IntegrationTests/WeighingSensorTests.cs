﻿using System;
using System.Reflection;
using Communication.Readers;
using Communication.Samples;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Communication.Tests.IntegrationTests
{
   [TestClass]
   public class WeighingSensorTests
   {
      private WeightReaderTestBase weightTest;
      private double readInterval;

      [TestInitialize]
      public void Init()
      {
         weightTest = new WeightReaderTestBase();
         var sensor = weightTest.CreateSensorConnection();
         var reader = new WeightReader(sensor);
         weightTest.SetReader(reader);

         var po = new PrivateObject(reader);
         readInterval = (int)po.GetField("SAMPLE_RATE_READ_TIMEOUT", BindingFlags.NonPublic | BindingFlags.Static);
         weightTest.Interval = (int)Math.Ceiling(readInterval);
      }

      [TestMethod]
      public void Read_OneSample()
      {
         weightTest.NewSampleFired_WhenStartReading();
      }

      [TestMethod]
      public void Read_SameSampleMultiply()
      {
         weightTest.ReadSameSample(false);
      }

      [TestMethod]
      public void Read_MultipleDifferentSamples()
      {
         weightTest.NewSampleFiredMultiple_WhenStartReading(true);
      }

    
      [TestMethod]
      public void NotRead_WhenStopReading()
      {
         weightTest.NewSampleNotFired_WhenStopReading(true);
      }

    
      [TestMethod]
      public void Read_WhenRestartReading()
      {
         weightTest.NewSampleFired_WhenRestartReading(true);
      }


   }

   internal class WeightReaderTestBase : ReaderTest<WeightReader, WeightSample, int>
   {
   }
}
