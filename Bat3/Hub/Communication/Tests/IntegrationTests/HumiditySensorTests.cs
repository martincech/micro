﻿using System;
using System.Reflection;
using Communication.Readers;
using Communication.Samples;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Communication.Tests.IntegrationTests
{
   [TestClass]
   public class HumiditySensorTests
   {
      private HumidityReaderTestBase humidityTest;
      private double readInterval;

      [TestInitialize]
      public void Init()
      {
         humidityTest = new HumidityReaderTestBase();
         var sensor = humidityTest.CreateSensorConnection();
         var reader = new HumidityReader(sensor);
         humidityTest.SetReader(reader);

         var po = new PrivateObject(reader);
         readInterval = (double)po.GetField("SAMPLE_RATE_READ_PERIOD_MS", BindingFlags.NonPublic | BindingFlags.Static);
         humidityTest.Interval = (int)Math.Ceiling(readInterval);
      }

      [TestMethod]
      public void Read_OneSample()
      {
         humidityTest.NewSampleFired_WhenStartReading();
      }

      [TestMethod]
      public void Read_SameSampleMultiply()
      {
         humidityTest.ReadSameSample(true);
      }

      [TestMethod]
      public void Read_MultipleDifferentSamples()
      {
         humidityTest.NewSampleFiredMultiple_WhenStartReading(false);
      }

      [TestMethod]
      public void NotRead_WhenStopReading()
      {
         humidityTest.NewSampleNotFired_WhenStopReading(false);
      }

      [TestMethod]
      public void Read_WhenRestartReading()
      {
         humidityTest.NewSampleFired_WhenRestartReading(false);
      }
   }

   internal class HumidityReaderTestBase : ReaderTest<HumidityReader, HumiditySample, byte>
   {
   }
}
