﻿using BatLibrary;
using Communication.Samples;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Communication.Tests.UnitTests.Samples
{
   [TestClass]
   public class TemperatureSampleTests
   {
      private const double EXEMPLAR = 20.0; //°C

      [TestMethod]
      public void ConvertToFahrenheit()
      {
         var sample = new TemperatureSample { Value = EXEMPLAR };
         Assert.AreEqual(ConvertCelsiusToFahrenheit(EXEMPLAR), sample.ConvertTemperature(Temperature.Fahrenheit));
      }

      [TestMethod]
      public void ConvertToCelsius()
      {
         var sample = new TemperatureSample { Value = EXEMPLAR };
         Assert.AreEqual(EXEMPLAR, sample.ConvertTemperature(Temperature.Celsius));
      }

      private double ConvertCelsiusToFahrenheit(double c)
      {
         return ((9.0 / 5.0) * c) + 32;
      }
   }
}
