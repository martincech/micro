﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Communication.SensorConnection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modbus.Device;
using Moq;

namespace Communication.Tests.UnitTests
{
   [TestClass]
   public class ModbusSensorConnectionTests
   {
      #region Private fields

      private ModbusSensorConnection modbus;
      private Mock<IModbusMaster> mockMaster;
      private readonly ushort[] data = {10, 11, 12};
      private int samplesToRead;

      #endregion

      [TestInitialize]
      public void Init()
      {
         mockMaster = new Mock<IModbusMaster>();
         modbus = new ModbusSensorConnection(mockMaster.Object);

         mockMaster.Setup(s => s.ReadHoldingRegisters(It.IsAny<byte>(), It.IsAny<ushort>(), It.IsAny<ushort>())).Returns(data);

         var po = new PrivateObject(modbus);
         samplesToRead = (int)po.GetField("SAMPLES_TO_READ", BindingFlags.NonPublic | BindingFlags.Static);
      }


      [TestMethod]
      public void ReadTemperature_RightRegisterAndReturnValue()
      {
         CheckOutputForDifferentInputs(ModbusSensorHoldingRegs.SP_HOLDING_SENSOR_DATA_TEMPERATURE, () => modbus.ReadTemperature());
      }

      [TestMethod]
      public void ReadCO2_RightRegisterAndReturnValue()
      {
         CheckOutputForDifferentInputs(ModbusSensorHoldingRegs.SP_HOLDING_SENSOR_DATA_CARBON_DIOXIDE, () => modbus.ReadCO2());
      }

      [TestMethod]
      public void ReadHumidity_RightRegisterAndReturnValue()
      {
         CheckOutputForDifferentInputs(ModbusSensorHoldingRegs.SP_HOLDING_SENSOR_DATA_HUMIDITY, () => modbus.ReadHumidity());
      }

      [TestMethod]
      public void ReadSampleRate_RightRegisterAndReturnValue()
      {
         CheckOutputForDifferentInputs(ModbusSensorHoldingRegs.SP_HOLDING_WEIGHT_DATA_SAMPLES_RATE, () => modbus.ReadSampleRate());
      }

      [TestMethod]
      public void ReadWeights_RightRegisterAndReturnValue()
      {
         CheckWeightsOutputForDifferentInputs(ModbusSensorHoldingRegs.SP_HOLDING_WEIGHT_DATA_TIMESTAMP_1, () => modbus.ReadWeights());
      }


      #region Private helpers

      private void CheckOutputForDifferentInputs(ModbusSensorHoldingRegs reg, Func<int?> method)
      {
         CompareResult(reg, data.FirstOrDefault(), method());
         mockMaster.Setup(s => s.ReadHoldingRegisters(It.IsAny<byte>(), It.IsAny<ushort>(), It.IsAny<ushort>()))
            .Returns((ushort[]) null);
         CompareResult(reg, null, method());
      }

      private void CompareResult(ModbusSensorHoldingRegs reg, int? expected, int? result)
      {
         mockMaster.Verify(v => v.ReadHoldingRegisters(1, (ushort)reg, 1));
         Assert.AreEqual(expected, result);
      }

      private void CheckWeightsOutputForDifferentInputs(ModbusSensorHoldingRegs reg, Func<IEnumerable<int>> method)
      {
         CompareWeightResult(reg, 1, method());
         mockMaster.Setup(s => s.ReadHoldingRegisters(It.IsAny<byte>(), It.IsAny<ushort>(), It.IsAny<ushort>()))
            .Returns((ushort[])null);
         CompareWeightResult(reg, 0, method());
      }

      private void CompareWeightResult(ModbusSensorHoldingRegs reg, int expectedCount, IEnumerable<int> result)
      {
         mockMaster.Verify(v => v.ReadHoldingRegisters(1, (ushort)reg, (ushort)samplesToRead));
         Assert.AreEqual(expectedCount, result.Count());
      }
      
      #endregion
   }
}
