﻿using Communication.Readers;
using Communication.Samples;
using Communication.Tests.UnitTests.Readers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable once CheckNamespace
namespace Communication.Tests
{
   [TestClass]
   public class Co2ReaderTest
   {
      private Co2MockReaderTestBase co2MockTest;

      [TestInitialize]
      public void Init()
      {
         co2MockTest = new Co2MockReaderTestBase();
         var sensor = co2MockTest.CreateSensorConnection();
         co2MockTest.SetReader(new Co2Reader(sensor));
      }

      [TestMethod]
      public void NewSampleFired_WhenStartReading()
      {
         co2MockTest.NewSampleFired_WhenStartReading();
      }

      [TestMethod]
      public void NewSampleFiredMultiple_WhenStartReading()
      {
         co2MockTest.NewSampleFiredMultiple_WhenStartReading(true);
      }

      [TestMethod]
      public void ChangeReadingParameter()
      {
         co2MockTest.ReadingParameterChange();
      }
   }

   internal class Co2MockReaderTestBase : MockReaderTest<Co2Reader, Co2Sample, long>
   {

   }
}
