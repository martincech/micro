﻿using Communication.Readers;
using Communication.Samples;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Communication.Tests.UnitTests.Readers
{
   [TestClass]
   public class TemperatureReaderTest
   {
      private TemperatureMockReaderTestBase temperatureMockTest;

      [TestInitialize]
      public void Init()
      {
         temperatureMockTest = new TemperatureMockReaderTestBase();
         var sensor = temperatureMockTest.CreateSensorConnection();
         temperatureMockTest.SetReader(new TemperatureReader(sensor));
      }

      [TestMethod]
      public void NewSampleFired_WhenStartReading()
      {
         temperatureMockTest.NewSampleFired_WhenStartReading();
      }

      [TestMethod]
      public void NewSampleFiredMultiple_WhenStartReading()
      {
         temperatureMockTest.NewSampleFiredMultiple_WhenStartReading(true);
      }

      [TestMethod]
      public void ChangeReadingParameter()
      {
         temperatureMockTest.ReadingParameterChange();
      }
   }

   internal class TemperatureMockReaderTestBase : MockReaderTest<TemperatureReader, TemperatureSample, double>
   {
   }
}
