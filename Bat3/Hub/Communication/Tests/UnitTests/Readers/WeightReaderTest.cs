﻿using Communication.Readers;
using Communication.Samples;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Communication.Tests.UnitTests.Readers
{
   [TestClass]
   public class WeightReaderTest
   {
      private WeightMockReaderTestBase weightMockTest;
      
      [TestInitialize]
      public void Init()
      {
         weightMockTest = new WeightMockReaderTestBase();
         var sensor = weightMockTest.CreateSensorConnection();

         // set sample rate
         var reader = new WeightReader(sensor);
         var po = new PrivateObject(reader);
         po.SetField("sampleRateHz", 10);

         weightMockTest.SetReader(reader);
      }

      [TestMethod]
      public void NewSampleFired_WhenStartReading()
      {
         weightMockTest.NewSampleFired_WhenStartReading();
      }

      [TestMethod]
      public void NewSampleFiredMultiple_WhenStartReading()
      {
         weightMockTest.NewSampleFiredMultiple_WhenStartReading(false);
      }

      [TestMethod]
      public void ChangeReadingParameter()
      {
         weightMockTest.ReadingParameterChange();
      }
   }

   internal class WeightMockReaderTestBase : MockReaderTest<WeightReader, WeightSample, int>
   {
   }
}
