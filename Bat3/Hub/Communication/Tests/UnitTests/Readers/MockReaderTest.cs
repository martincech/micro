﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Timers;
using Communication.Readers;
using Communication.Samples;
using Communication.SensorConnection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Timer = System.Timers.Timer;

namespace Communication.Tests.UnitTests.Readers
{
   [TestClass]
   internal abstract class MockReaderTest<T, U, V>
      where T : SampleReader<U>
      where U : SenzorSample<V>
   {
      #region Private fields

      protected T Reader;
      private Mock<ISensorConnection> mockSensorConnection;
      protected int ExpectedValue = 155;
      protected const int SAMPLE_RATE = 10;
      protected List<U> ReadValues;

      #endregion   

      public void SetReader(T readerInstance)
      {
         Reader = readerInstance;
      }

      public virtual void NewSampleFired_WhenStartReading()
      {
         Reader.NewSample += (sender, args) =>
         {
            Assert.AreEqual(ExpectedValue, Convert.ToInt32(args.Value));
            ReadValues.Add(args);
         };
         Assert.IsFalse(ReadValues.Any());
         Reader.StartReading();

         StopSensorReadTimer();
         RaisedSensorReadEvent();
         Assert.IsTrue(ReadValues.Any());
      }

      public virtual void NewSampleFiredMultiple_WhenStartReading(bool readOnlyDifferentValue)
      {
         NewSampleFired_WhenStartReading();
         Assert.IsTrue(Reader.IsReading);

         const int cycle = 3;
         var expectedValuesCount = readOnlyDifferentValue ? 1 : cycle + 1;
         for (var i = 0; i < cycle; i++)
         {
            RaisedSensorReadEvent();
         }
         Assert.AreEqual(expectedValuesCount, ReadValues.Count);

         ExpectedValue += 10;
         InitMethodsOutput();
         RaisedSensorReadEvent();
         Assert.AreEqual(expectedValuesCount + 1, ReadValues.Count);
      }
    
      public void ReadingParameterChange()
      {
         NewSampleFired_WhenStartReading();
         Assert.IsTrue(Reader.IsReading);
         Reader.StopReading();
         Assert.IsFalse(Reader.IsReading);
         Reader.StartReading();
         Assert.IsTrue(Reader.IsReading);
      }

      #region Private helpers

      internal ISensorConnection CreateSensorConnection()
      {
         ReadValues = new List<U>();
         mockSensorConnection = new Mock<ISensorConnection>();
         InitMethodsOutput();
         return mockSensorConnection.Object;
      }

      protected void InitMethodsOutput(int add = 0)
      {
         if (mockSensorConnection == null) return;

         mockSensorConnection.Setup(s => s.ReadCO2()).Returns(ExpectedValue + add);
         mockSensorConnection.Setup(s => s.ReadHumidity()).Returns(ExpectedValue + add);
         mockSensorConnection.Setup(s => s.ReadTemperature()).Returns(ExpectedValue + add);
         mockSensorConnection.Setup(s => s.ReadWeights()).Returns(new List<int> { ExpectedValue });
         mockSensorConnection.Setup(s => s.ReadSampleRate()).Returns(SAMPLE_RATE);
      }

      private void StopSensorReadTimer()
      {
         var baseType = typeof(T).BaseType;
         if (baseType == null) return;

         var timerInfo = baseType.GetField("readSenzorTimer", BindingFlags.NonPublic | BindingFlags.Instance);
         if (timerInfo == null) return;

         var timer = (Timer)timerInfo.GetValue(Reader);
         timer.Stop();
      }

      private void RaisedSensorReadEvent()
      {
         var baseType = typeof(T).BaseType;
         if (baseType == null) return;

         var handlerInfo = baseType.GetField("timerHandler", BindingFlags.NonPublic | BindingFlags.Instance);
         if (handlerInfo == null) return;

         var handler = (ElapsedEventHandler)handlerInfo.GetValue(Reader);
         handler(null, null);

         if (typeof(T) == typeof(WeightReader))
         {  // for right synchronization
            System.Threading.Thread.Sleep(5);
         }
      }

      #endregion
   }
}
