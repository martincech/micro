using System;

namespace Communication.Samples
{
   [Serializable]
   public class SenzorSample<T>
   {
      public DateTime TimeStamp { get; set; }
      public T Value { get; internal set; }
   }
}