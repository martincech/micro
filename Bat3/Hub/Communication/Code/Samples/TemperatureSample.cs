﻿using System;
using BatLibrary;

namespace Communication.Samples
{
   [Serializable]
   public class TemperatureSample : SenzorSample<double>
   {
      public double ConvertTemperature(Temperature toUnit)
      {
         if (toUnit == Temperature.Fahrenheit)
         {
            return Value * 9 / 5 + 32;
         }
         return Value;
      }
   }
}
