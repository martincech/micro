﻿using System;
using Communication.Readers;

namespace Communication.SensorManagers
{
   public interface ICo2SensorManager
   {
      event EventHandler<ICo2Reader> DeviceConnected;
      event EventHandler<ICo2Reader> DeviceDisconnected;
   }
}
