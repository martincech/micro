using System;
using Communication.Readers;

namespace Communication.SensorManagers
{
   public interface IWeightSensorManager
   {
      event EventHandler<IWeightReader> DeviceConnected;
      event EventHandler<IWeightReader> DeviceDisconnected;
   }
}