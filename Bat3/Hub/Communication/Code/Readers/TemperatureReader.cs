using System;
using System.Runtime.CompilerServices;
using Communication.Samples;
using Communication.SensorConnection;

namespace Communication.Readers
{
   internal class TemperatureReader : SampleReader<TemperatureSample>, ITemperatureReader
   {
      private int? lastTemperature;
      private const double SAMPLE_RATE_READ_PERIOD_MS = 200;

      internal TemperatureReader(ISensorConnection sensorConnection) : base(sensorConnection)
      {
      }

      [MethodImpl(MethodImplOptions.Synchronized)]
      private void ReadTemperatureFromSensor()
      {
         var temperature = DoSensorAction(sr => sr.ReadTemperature());
         if (temperature == lastTemperature || !IsReading) return;

         lastTemperature = temperature;
         TemperatureReaded();
      }

      private void TemperatureReaded()
      {
         if (!lastTemperature.HasValue) return;
         var sample = new TemperatureSample { TimeStamp = DateTime.Now, Value = lastTemperature.Value};
         OnNewSample(sample);
      }

      #region Overrides of SampleReader<TemperatureSample>

      public override event EventHandler<TemperatureSample> NewSample;

      protected virtual void OnNewSample(TemperatureSample e)
      {
         var handler = NewSample;
         if (handler != null) handler(this, e);
      }

      /// <summary>
      /// Start reading samples
      /// </summary>
      public override void StartReading()
      {
         if (IsReading)
         {
            return;
         }
         //ReadTemperatureFromSensor();
         SetTimerToReadTemperature();
      }

      private void SetTimerToReadTemperature()
      {
         StopReadingTimer();
         StartReadingTimer(SAMPLE_RATE_READ_PERIOD_MS, (sender, args) => ReadTemperatureFromSensor());
      }

      /// <summary>
      /// Stop reading samples
      /// </summary>
      public override void StopReading()
      {
         if (!IsReading)
         {
            return;
         }
         StopReadingTimer();
         lastTemperature = 0;
      }

      #endregion
   }
}