using System;
using System.Runtime.CompilerServices;
using Communication.Samples;
using Communication.SensorConnection;

namespace Communication.Readers
{
   internal class Co2Reader : SampleReader<Co2Sample>, ICo2Reader
   {
      private int? lastCo2;
      private const double SAMPLE_RATE_READ_PERIOD_MS = 200;

      internal Co2Reader(ISensorConnection sensorConnection) : base(sensorConnection)
      {
      }

      [MethodImpl(MethodImplOptions.Synchronized)]
      private void ReadCo2FromSensor()
      {
         var co2 = DoSensorAction(sr => sr.ReadCO2());
         if (lastCo2 == co2 || !IsReading) return;
         lastCo2 = co2;
         Co2Readed();
      }

      private void Co2Readed()
      {
         if (!lastCo2.HasValue) return;
         var sample = new Co2Sample { TimeStamp = DateTime.Now, Value = lastCo2.Value };
         OnNewSample(sample);
      }

      public override event EventHandler<Co2Sample> NewSample;

      protected virtual void OnNewSample(Co2Sample e)
      {
         var handler = NewSample;
         if (handler != null) handler(this, e);
      }

      /// <summary>
      /// Start reading samples
      /// </summary>
      public override void StartReading()
      {
         if (IsReading)
         {
            return;
         }
         //ReadCo2FromSensor();
         SetTimerToReadCo2();
      }

      private void SetTimerToReadCo2()
      {
         StopReadingTimer();
         StartReadingTimer(SAMPLE_RATE_READ_PERIOD_MS, (sender, args) => ReadCo2FromSensor());
      }

      /// <summary>
      /// Stop reading samples
      /// </summary>
      public override void StopReading()
      {
         if (!IsReading)
         {
            return;
         }
         StopReadingTimer();
      }
   }
}