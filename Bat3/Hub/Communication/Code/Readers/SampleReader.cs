using System;
using System.Timers;
using Communication.SensorConnection;

namespace Communication.Readers
{
   internal abstract class SampleReader<T> : ISampleReader<T>, IDisposable
   {
      private Timer readSenzorTimer;
      private readonly ISensorConnection sensorConnection;
      private ElapsedEventHandler timerHandler;

      protected SampleReader(ISensorConnection sensorConnection)
      {
         this.sensorConnection = sensorConnection;

      }
     
      #region Protected helpers

      protected void StartReadingTimer(double interval, ElapsedEventHandler elapsedHandler)
      {
         if (IsReading)
         {
            return;
         }
         if (readSenzorTimer != null)
         {
            readSenzorTimer.Elapsed -= timerHandler;
            readSenzorTimer.Stop();
         }

         timerHandler = elapsedHandler;
         readSenzorTimer = new Timer(interval) {AutoReset = true};
         readSenzorTimer.Elapsed += timerHandler;
         readSenzorTimer.Start();
         IsReading = true;
      }

      protected void StopReadingTimer()
      {
         if (!IsReading)
         {
            return;
         }
         readSenzorTimer.Stop();
         IsReading = false;
      }

      protected U DoSensorAction<U>(Func<ISensorConnection, U> func)
      {
         lock (sensorConnection)
         {
            return func(sensorConnection);
         }
      }

      #endregion

      public bool IsReading { get; private set; }

      #region Implementation of IDisposable

      public void Dispose()
      {
         StopReadingTimer();
      }
      #endregion

      public abstract event EventHandler<T> NewSample;

      /// <summary>
      /// Start reading samples
      /// </summary>
      public abstract void StartReading();

      /// <summary>
      /// Stop reading samples
      /// </summary>
      public abstract void StopReading();
      
   }
}