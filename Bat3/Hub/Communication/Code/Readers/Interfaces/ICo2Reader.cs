using Communication.Samples;

namespace Communication.Readers
{
   public interface ICo2Reader : ISampleReader<Co2Sample>
   {
   }
}