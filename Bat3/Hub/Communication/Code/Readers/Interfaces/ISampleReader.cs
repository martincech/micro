using System;

namespace Communication.Readers
{
   public interface ISampleReader<T>
   {
      /// <summary>
      /// Event raised when new sample read from the senzor
      /// </summary>
      event EventHandler<T> NewSample;
    
      /// <summary>
      /// Start reading samples
      /// </summary>
      void StartReading();
      /// <summary>
      /// Stop reading samples
      /// </summary>
      void StopReading();
      /// <summary>
      /// Is sample reading in progress?
      /// </summary>
      bool IsReading { get; }
   }
}