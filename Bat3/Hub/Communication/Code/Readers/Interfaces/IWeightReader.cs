using System;
using Communication.Samples;

namespace Communication.Readers
{
   public interface IWeightReader : ISampleReader<WeightSample>
   {
      /// <summary>
      /// Report to the listener that there was a loss of some weights during the read from the sensor.
      /// This should cause the caller to read faster the weights from the sensor.
      /// This event reports count of lost weights.
      /// </summary>
      event EventHandler<int> WeightsLost;
   };
}