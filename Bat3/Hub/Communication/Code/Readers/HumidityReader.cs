using System;
using System.Runtime.CompilerServices;
using Communication.Samples;
using Communication.SensorConnection;

namespace Communication.Readers
{
   internal class HumidityReader : SampleReader<HumiditySample>, IHumidityReader
   {

      private byte? lastHumidity;
      private const double SAMPLE_RATE_READ_PERIOD_MS = 200;

      internal HumidityReader(ISensorConnection sensorConnection)
         : base(sensorConnection)
      {
      }


      [MethodImpl(MethodImplOptions.Synchronized)]
      private void ReadHumidityFromSensor()
      {
         var humidity = DoSensorAction(sr => sr.ReadHumidity());
         if (lastHumidity == humidity || !IsReading) return;
         lastHumidity = (byte?)humidity;
         HumidityReaded();
      }

      private void HumidityReaded()
      {
         if (!lastHumidity.HasValue) return;
         var sample = new HumiditySample() { TimeStamp = DateTime.Now, Value = lastHumidity.Value };
         OnNewSample(sample);
      }

      #region Overrides of SampleReader<HumiditySample>

      public override event EventHandler<HumiditySample> NewSample;

      protected virtual void OnNewSample(HumiditySample e)
      {
         var handler = NewSample;
         if (handler != null) handler(this, e);
      }

      /// <summary>
      /// Start reading samples
      /// </summary>
      public override void StartReading()
      {
         if (IsReading)
         {
            return;
         }
         //ReadHumidityFromSensor();
         SetTimerToReadHumidity();
      }

      private void SetTimerToReadHumidity()
      {
         StopReadingTimer();
         StartReadingTimer(SAMPLE_RATE_READ_PERIOD_MS, (sender, args) => ReadHumidityFromSensor());
      }

      /// <summary>
      /// Stop reading samples
      /// </summary>
      public override void StopReading()
      {
         if (!IsReading)
         {
            return;
         }
         StopReadingTimer();
         lastHumidity = 0;
      }

      #endregion
   }
}