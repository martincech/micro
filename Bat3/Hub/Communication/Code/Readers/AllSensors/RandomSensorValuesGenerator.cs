using System;
using System.Threading;
using System.Threading.Tasks;
using Communication.Samples;

namespace Communication.Readers.AllSensors
{
   public class RandomSensorValuesGenerator : IWeightReader, ITemperatureReader, ICo2Reader, IHumidityReader
   {
      private CancellationTokenSource cancellation;
      private Task task;
      private bool isReading;
      private event EventHandler<TemperatureSample> NewTemperatureSample;
      private event EventHandler<Co2Sample> NewCo2Sample;
      private event EventHandler<HumiditySample> NewHumiditySample;

      public RandomSensorValuesGenerator()
      {
         Init();
      }

      private void Init()
      {
         cancellation = new CancellationTokenSource();
         task = new Task(() =>
         {
            var random = new Random();
            while (!cancellation.IsCancellationRequested)
            {
               var value = random.Next(1, 3);
               var weight = 0;
               var result = value%3;

               if (result == 0)
               {
                  weight = random.Next(390, 550);
               }

               if (result == 1)
               {
                  weight = random.Next(350, 449);
               }

               if (result == 2)
               {
                  weight = random.Next(450, 550);
               }
            

               for (var i = 0; i < 30 && !cancellation.IsCancellationRequested; i++)
               {
                  var sample = new WeightSample(weight);
                  Thread.Sleep(10);
                  OnNewWeight(sample);
               }

               double temperature = random.Next(19, 30);
               long co2 = random.Next(450, 2200);
               int humidity = random.Next(40, 99);
               var tSample = new TemperatureSample { TimeStamp = DateTime.Now, Value = temperature };
               var co2Sample = new Co2Sample { TimeStamp = DateTime.Now, Value = co2 };
               var humiditySample = new HumiditySample { TimeStamp = DateTime.Now, Value = (byte)humidity };

               OnNewTemperatureSample(tSample);
               OnNewCo2Sample(co2Sample);
               OnNewHumiditySample(humiditySample);
            }
         }, cancellation.Token);
      }

      #region Implementation of IWeightReader

      public event EventHandler<int> WeightsLost;
      public event EventHandler<WeightSample> NewSample;
      public event EventHandler<DateTime> ReadError;

      event EventHandler<TemperatureSample> ISampleReader<TemperatureSample>.NewSample
      {
         add { NewTemperatureSample += value; }
         remove { NewTemperatureSample -= value; }
      }

      event EventHandler<Co2Sample> ISampleReader<Co2Sample>.NewSample
      {
         add { NewCo2Sample += value; }
         remove { NewCo2Sample -= value; }
      }

      event EventHandler<HumiditySample> ISampleReader<HumiditySample>.NewSample
      {
         add { NewHumiditySample += value; }
         remove { NewHumiditySample -= value; }
      }

      public void StartReading()
      {
         if (IsReading)
         {
            return;
         }
         Init();
         task.Start();
         isReading = true;
      }

      public void StopReading()
      {
         if (!IsReading)
         {
            return;
         }
         cancellation.Cancel();
         try
         {
            task.Wait();
         }
         catch (Exception){}

         isReading = false;
      }

      public bool IsReading
      {
         get { return isReading; }
         set
         {
            if (value)
            {
               StartReading();
            }
            else
            {
               StopReading();
            }
         }
      }

      #endregion

      protected virtual void OnWeightsLost(int e)
      {
         var handler = WeightsLost;
         if (handler != null) handler(this, e);
      }

      protected virtual void OnNewWeight(WeightSample e)
      {
         var handler = NewSample;
         if (handler != null) handler(this, e);
      }

      protected virtual void OnNewTemperatureSample(TemperatureSample e)
      {
         var handler = NewTemperatureSample;
         if (handler != null) handler(this, e);
      }

      protected virtual void OnNewCo2Sample(Co2Sample e)
      {
         var handler = NewCo2Sample;
         if (handler != null) handler(this, e);
      }

      protected virtual void OnNewHumiditySample(HumiditySample e)
      {
         var handler = NewHumiditySample;
         if (handler != null) handler(this, e);
      }

      protected virtual void OnReadError(DateTime e)
      {
         var handler = ReadError;
         if (handler != null) handler(this, e);
      }
   }
}