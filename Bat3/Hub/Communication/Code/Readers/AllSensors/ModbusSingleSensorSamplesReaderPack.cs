using Communication.SensorConnection;
using Modbus.Device;

namespace Communication.Readers.AllSensors
{
   internal class ModbusSingleSensorSamplesReaderPack : SingleSensorSamplesReaderPack
   {
      public ModbusSingleSensorSamplesReaderPack(IModbusMaster master, byte slaveAddress) :
         base(new ModbusSensorConnection(master, slaveAddress)) { }
   }
}