using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using Communication.Samples;
using Communication.SensorConnection;

namespace Communication.Readers
{
   internal sealed class WeightReader : SampleReader<WeightSample>, IWeightReader
   {

      #region Private fields

      private const int SAMPLE_RATE_READ_TIMEOUT = 500; //ms
      private readonly Queue<int> readBuffer;
      private int sampleRateHz;

      #endregion

      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      internal WeightReader(ISensorConnection sensorConnection) :base(sensorConnection)
      {
         sensorConnection.WeightsLost += SensorOnWeightsLost;
         readBuffer = new Queue<int>();
      }

      [MethodImpl(MethodImplOptions.Synchronized)]
      private void ReadWeigthsFromSenzor()
      {
         var weights = DoSensorAction(sr => sr.ReadWeights());
         foreach (var weight in weights)
         {
            lock (readBuffer)
            {
               readBuffer.Enqueue(weight);
            }
         }
         if (readBuffer.Count != 0)
         {
            ThreadPool.QueueUserWorkItem(param => WeightsReaded());
         }
      }
      
      [MethodImpl(MethodImplOptions.Synchronized)]
      private void ReadSampleRateFromSenzor()
      {
         var sampleRate = DoSensorAction(sr=>sr.ReadSampleRate());
         if (sampleRate == null)
         {
            sampleRateHz = 0;
         }
         else
         {
            sampleRateHz = (int)sampleRate;
         }
      }

      #region Implementation of IWeightReader

      public event EventHandler<int> WeightsLost;
      public override event EventHandler<WeightSample> NewSample;
      
      public override void StartReading()
      {
         if (IsReading)
         {
            return;
         }
         //ReadSampleRateFromSenzor();
         //ReadWeigthsFromSenzor();
         SetTimerToReadWeights();
      }

      public override void StopReading()
      {
         if (!IsReading)
         {
            return;
         }
         StopReadingTimer();
      }

      private void OnNewWeight(WeightSample e)
      {
         var handler = NewSample;
         if (handler != null) handler(this, e);
      }

      private void OnWeightsLost(int e)
      {
         var handler = WeightsLost;
         if (handler != null) handler(this, e);
      }

      #endregion

      #region Helpers

      private void SetTimerToReadWeights()
      {
         StopReadingTimer();
         if (sampleRateHz == 0)
         {
            StartReadingTimer(SAMPLE_RATE_READ_TIMEOUT, (sender, args) =>
            {
               ReadSampleRateFromSenzor();
               if (sampleRateHz != 0)
               {
                  SetTimerToReadWeights();
               }
            });
         }
         else
         {
            StartReadingTimer((1 / (double)sampleRateHz) * 1000, (sender, args) => ReadWeigthsFromSenzor());
         }
      }

      [MethodImpl(MethodImplOptions.Synchronized)]
      private void WeightsReaded()
      {
         lock (readBuffer)
         {
            while (readBuffer.Count != 0)
            {
               var sample = new WeightSample(readBuffer.Dequeue());
               Monitor.Exit(readBuffer);
               OnNewWeight(sample);
               Monitor.Enter(readBuffer);
            }
         }
      }

      private void SensorOnWeightsLost(object sender, int e)
      {
         OnWeightsLost(e);
      }
      #endregion


   }
}