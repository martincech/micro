using System;
using System.Collections.Generic;

namespace Communication.SensorConnection
{
   internal interface ISensorConnection
   {
      /// <summary>
      /// Read the sample rate from the senzor
      /// </summary>
      /// <returns></returns>
      int? ReadSampleRate();

      /// <summary>
      /// Read all the weights from the senzor
      /// </summary>
      /// <returns>Collection of raw weights as measured by the senzor</returns>
      IEnumerable<int> ReadWeights();

      /// <summary>
      /// Report to the listener that there was a loss of some weights during the read from the sensor.
      /// This should cause teh caller to read faster the weights from the sensor.
      /// </summary>
      event EventHandler<int> WeightsLost;

      /// <summary>
      /// Report to the listener that there was a loss of connection during the read from the sensor.
      /// </summary>
      event EventHandler<DateTime> ConnectionLost;

      /// <summary>
      /// Read current temperature on the sensor.
      /// </summary>
      /// <returns>Temperature in [�C]</returns>
      int? ReadTemperature();

      /// <summary>
      /// Read value of Carbon dioxide in air
      /// </summary>
      /// <returns>CO2 in air in [ppm] (particles per milion)</returns>
      // ReSharper disable once InconsistentNaming
      int? ReadCO2();

      /// <summary>
      /// Read relative humidity in %
      /// </summary>
      /// <returns>Relative humiditiy in %</returns>
      int? ReadHumidity();
   }
}