using System;
using System.Collections.Generic;
using System.Linq;
using Modbus.Device;

namespace Communication.SensorConnection
{
   internal class ModbusSensorConnection : ISensorConnection
   {
      #region Constants

      internal const int REGISTERS_PER_SAMPLE = 3;
      internal const int MAXIMUM_WEIGHTS_PER_SINGLE_QUERY = (125/REGISTERS_PER_SAMPLE);
      internal const int INVALID_TIMESTAMP = 0xFFFF;

      #endregion


      #region Private fields
      private const int SAMPLES_TO_READ = MAXIMUM_WEIGHTS_PER_SINGLE_QUERY * REGISTERS_PER_SAMPLE;
      private ushort expectedTimestamp;
      private readonly IModbusMaster master;
      private readonly byte slaveAddress;

      #endregion

      public ModbusSensorConnection(IModbusMaster master, byte? slaveAddress = null)
      {
         this.master = master;
#if DEBUG
         if (master.Transport != null)
         {
#endif
            master.Transport.ReadTimeout = 1000;
            master.Transport.WriteTimeout = 1000;
            master.Transport.Retries = 2;
            master.Transport.SlaveBusyUsesRetryCount = true;
            master.Transport.WaitToRetryMilliseconds = 200;
#if DEBUG
         }
#endif
         this.slaveAddress = slaveAddress ?? 1;
      }

      #region Implementation of ISensorConnection

      public event EventHandler<int> WeightsLost;

      public event EventHandler<DateTime> ConnectionLost;

      /// <summary>
      /// Read the sample rate from the senzor
      /// </summary>
      /// <returns>`0 if sample rate cannot be detected, sample rate in [Hz] otherwise</returns>
      public int? ReadSampleRate()
      {
         var regs = ReadHoldingRegisters(ModbusSensorHoldingRegs.SP_HOLDING_WEIGHT_DATA_SAMPLES_RATE, 1);
         if (regs == null) return null;
         return regs.FirstOrDefault();
      }      

      public IEnumerable<int> ReadWeights()
      {
         var readBuffer = new List<int>();
         var weighingRegs = ReadHoldingRegisters(ModbusSensorHoldingRegs.SP_HOLDING_WEIGHT_DATA_TIMESTAMP_1, SAMPLES_TO_READ);
         if (weighingRegs == null)
         { 
            return readBuffer;
         }

         var wRegs = weighingRegs as ushort[] ?? weighingRegs.ToArray();
         for (var i = 0; i < wRegs.Count(); i += REGISTERS_PER_SAMPLE)
         {
            var timestamp = wRegs[i];
            // stop on first invalid timestamp - no data on modbus yet
            if (timestamp == INVALID_TIMESTAMP)
            {
               break;
            }
            // rewind already processed samples
            if (expectedTimestamp != INVALID_TIMESTAMP)
            {
               if (timestamp < expectedTimestamp)
               {
                  expectedTimestamp = timestamp;
                  continue;
               }
               if (timestamp != expectedTimestamp)
               {
                  // there was some loss of data
                  OnWeightsLost(timestamp - expectedTimestamp);
               }
            }

            var weight = (wRegs[i + 1] << 16) | wRegs[i + 2];
            readBuffer.Add(weight);
            expectedTimestamp = (ushort) (timestamp + 1);
         }
         return readBuffer;
      }

      /// <summary>
      /// Read current temperature on the sensor.
      /// </summary>
      /// <returns>Temperature in [�C]</returns>
      public int? ReadTemperature()
      {
         var regs = ReadHoldingRegisters(ModbusSensorHoldingRegs.SP_HOLDING_SENSOR_DATA_TEMPERATURE, 1);
         if (regs == null) return null;
         return regs.FirstOrDefault();
      }

      /// <summary>
      /// Read value of Carbon dioxide in air
      /// </summary>
      /// <returns>CO2 in air in [ppm] (particles per milion)</returns>
      // ReSharper disable once InconsistentNaming
      public int? ReadCO2()
      {
         var regs = ReadHoldingRegisters(ModbusSensorHoldingRegs.SP_HOLDING_SENSOR_DATA_CARBON_DIOXIDE, 1);
         if (regs == null) return null;
         return regs.FirstOrDefault();
      }

      /// <summary>
      /// Read relative humidity in %
      /// </summary>
      /// <returns>Relative humiditiy in %</returns>
      public int? ReadHumidity()
      {
         var regs = ReadHoldingRegisters(ModbusSensorHoldingRegs.SP_HOLDING_SENSOR_DATA_HUMIDITY, 1);
         if (regs == null) return null;
         return regs.FirstOrDefault();
      }

      #endregion

      #region Helpers

      private IEnumerable<ushort> ReadHoldingRegisters(ModbusSensorHoldingRegs from, ushort count)
      {
         try
         {
            return master.ReadHoldingRegisters(slaveAddress,
               GetRegisterAddress(from), count);
         }
         catch (Exception)
         {
            OnConnectionLost(DateTime.Now);
            return null;
         }
      }

      private static ushort GetRegisterAddress(ModbusSensorHoldingRegs reg)
      {
         return (ushort) reg;
      }

      #region Event invocators

      protected virtual void OnWeightsLost(int e)
      {
         var handler = WeightsLost;
         if (handler != null) handler(this, e);
      }

      protected virtual void OnConnectionLost(DateTime e)
      {
         var handler = ConnectionLost;
         if (handler != null) handler(this, e);
      }

      #endregion

      #endregion

     
   }
}