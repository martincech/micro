﻿using BatLibrary;

namespace Recognition
{
   public interface IWeightProcessingAdministrator<in T>
      where T : Weight
   {
      void Add(T rawWeight);
   }
}