﻿using BatLibrary;

namespace Recognition.Types
{
   public class AverageWeight : FilterWeight
   {
      /// <summary>
      /// Init weight in grams
      /// </summary>
      /// <param name="grams">initialization value of weight as grams</param>
      public AverageWeight(double grams = 0) : base(grams)
      {
      }

      public AverageWeight(Weight weight) : base(weight)
      {
      }

      public AverageWeight(double? grams) : base(grams)
      {
      }

      public AverageWeight(double weight, WeightUnits units) : base(weight, units)
      {
      }

      public AverageWeight(double? grams, double highPassGrams)
         : base(grams, highPassGrams)
      {
      }
   }
}
