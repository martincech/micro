namespace Recognition.Types
{
   public enum DifferenceMode
   {
      Increased,
      Decreased
   }
}