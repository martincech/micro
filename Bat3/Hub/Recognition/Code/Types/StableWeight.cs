﻿using BatLibrary;

namespace Recognition.Types
{
   public class StableWeight : Weight
   {
      /// <summary>
      /// Init weight in grams
      /// </summary>
      /// <param name="grams">initialization value of weight as grams</param>
      public StableWeight(double grams = 0) : base(grams)
      {
      }

      public StableWeight(Weight weight) : base(weight)
      {
      }

      public StableWeight(double? grams) : base(grams)
      {
      }

      public StableWeight(double weight, WeightUnits units) : base(weight, units)
      {
      }
   }
}
