﻿using System.Runtime.Serialization;
using BatLibrary;

namespace Recognition.Types
{
   [DataContract]
   public class BirdWeight : Weight
   {
      /// <summary>
      /// Init weight in grams
      /// </summary>
      /// <param name="grams">initialization value of weight as grams</param>
      public BirdWeight(double grams = 0) : base(grams)
      {
      }

      public BirdWeight(Weight weight) : base(weight)
      {
      }

      public BirdWeight(double? grams) : base(grams)
      {
      }

      public BirdWeight(double weight, WeightUnits units) : base(weight, units)
      {
      }

      [DataMember]
      public DifferenceMode Difference { get; set; }
      [DataMember]
      public Sex Sex { get; set; }
   }
}
