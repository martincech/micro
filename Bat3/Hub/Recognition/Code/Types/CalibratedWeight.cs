﻿using BatLibrary;

namespace Recognition.Types
{
   public class CalibratedWeight : FilterWeight
   {
      /// <summary>
      /// Init weight in grams
      /// </summary>
      /// <param name="grams">initialization value of weight as grams</param>
      public CalibratedWeight(double grams = 0) : base(grams)
      {
      }

      public CalibratedWeight(Weight weight) : base(weight)
      {
      }

      public CalibratedWeight(double? grams) : base(grams)
      {
      }

      public CalibratedWeight(double weight, WeightUnits units) : base(weight, units)
      {
      }

      public CalibratedWeight(double? grams, double highPassGrams)
         : base(grams, highPassGrams)
      {
      }

      public CalibratedWeight(double? grams, Weight highPassGrams)
         : base(grams, highPassGrams)
      {
      }
   }
}
