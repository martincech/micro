﻿using System;
using BatLibrary;

namespace Recognition
{
   public interface IWeightProcessingEventer<U>
      where U : Weight
   {
      event EventHandler<U> Recognized;
   }
}