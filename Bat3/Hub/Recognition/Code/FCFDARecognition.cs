﻿using System;
using BatLibrary;
using Recognition.RecognitionParts;
using Recognition.Types;

namespace Recognition
{
   // ReSharper disable once InconsistentNaming
   public class FCFDARecognition : IBirdRecognition
   {
      #region Private fields

      private Calibration calibration;
      private AveragingFiltration averagingFiltration;
      private StableFiltration stableFiltration;
      private Detection detection;
      private Acceptance acceptance;

      #endregion

      #region Public

      public static class DefaultParameters
      {
         // ReSharper disable InconsistentNaming
         public static readonly int AVERGING_WINDOW = 10;
         public static readonly int STABLE_WINDOW = 5;
         public static readonly Weight STABLE_RANGE = new Weight(5);
         public static readonly Weight MALE_LOW = new Weight(35);
         public static readonly Weight MALE_HIGH = new Weight(44.9);
         public static readonly Weight FEMALE_LOW = new Weight(45);
         public static readonly Weight FEMALE_HIGH = new Weight(55);
         public static readonly AcceptanceSexMode ACCEPTANCE_MODE = AcceptanceSexMode.Mixed;
         public static readonly AcceptanceStepMode STEP_MODE = AcceptanceStepMode.Both;
         public static readonly Sex SEX_MODE = Sex.Undefined;
         public static readonly double VALUE_MULTIPLIER = 1; // 0.19416;
         public static readonly StableWeight VALUE_OFFSET = new StableWeight();// new StableWeight(1579); //
      }

      public FCFDARecognition()
      {
         var filterParams = new FilterParams
         {
            AveragingWindow = DefaultParameters.AVERGING_WINDOW,
            StableRange = DefaultParameters.STABLE_RANGE,
            StableWindow = DefaultParameters.STABLE_WINDOW
         };

         AveragingFiltration = new AveragingFiltration(filterParams);
         Calibration = new Calibration(new CalibrationParams
         {
            ValueMultiplier = DefaultParameters.VALUE_MULTIPLIER,
            ValueOffset = DefaultParameters.VALUE_OFFSET
         }) {FilterParams = filterParams};
         StableFiltration = new StableFiltration(filterParams);
         Detection = new Detection();
         Acceptance = new Acceptance(new AcceptanceParams
         {
            MaleLow = DefaultParameters.MALE_LOW,
            MaleHigh = DefaultParameters.MALE_HIGH,
            FemaleLow = DefaultParameters.FEMALE_LOW,
            FemaleHigh = DefaultParameters.FEMALE_HIGH,
            AcceptanceSexMode = DefaultParameters.ACCEPTANCE_MODE,
            SexMode = DefaultParameters.SEX_MODE,
            StepMode = DefaultParameters.STEP_MODE
         });
      }

      public void Add(RawWeight rawWeight)
      {
         AveragingFiltration.Add(rawWeight);   // first module in chain
      }

      public Calibration Calibration
      {
         get { return calibration; }
         private set
         {
            if (value == null)
            {
               return;
            }
            calibration = value;
            averagingFiltration.Successor = calibration;
            calibration.Successor = StableFiltration;
         }
      }

      public AveragingFiltration AveragingFiltration
      {
         get { return averagingFiltration; }
         private set
         {
            if (value == null)
            {
               return;
            }
            averagingFiltration = value;
            averagingFiltration.Successor = Calibration;
         }
      }

      public StableFiltration StableFiltration
      {
         get { return stableFiltration; }
         private set
         {
            if (value == null)
            {
               return;
            }
            stableFiltration = value;
            calibration.Successor = stableFiltration;
            stableFiltration.Successor = Detection;
         }
      }

      public Detection Detection
      {
         get { return detection; }
         private set
         {
            if (value == null)
            {
               return;
            }
            detection = value;
            stableFiltration.Successor = detection;
            detection.Successor = Acceptance;
         }
      }

      public Acceptance Acceptance
      {
         get { return acceptance; }
         private set
         {
            if (value == null)
            {
               return;
            }
            if (acceptance != null)
            {
               acceptance.Recognized -= OnRecognized;
            }
            acceptance = value;
            detection.Successor = acceptance;
            acceptance.Recognized += OnRecognized;
         }
      }

      public AcceptanceParams AcceptanceParams
      {
         get
         {
            return Acceptance == null ? null : Acceptance.Params;
         }
         set { Acceptance.Params = value; }
      }


      #endregion

      public event EventHandler<BirdWeight> Recognized;
      protected virtual void OnRecognized(object sender, BirdWeight birdWeight)
      {
         var handler = Recognized;
         if (handler != null) handler(this, birdWeight);
      }
   }
}
