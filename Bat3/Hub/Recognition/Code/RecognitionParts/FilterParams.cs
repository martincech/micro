using System;
using BatLibrary;

namespace Recognition.RecognitionParts
{
   public class FilterParams
   {
      #region Private field

      private int averagingWindow;
      private int stableWindow;
      private int stableWindowMinimum;
      private int averagingWindowMinimum;
      private bool stableWindowValidation;
      private bool averagingWindowValidation;

      #endregion

      public Weight StableRange { get; set; }
      public int StableWindow
      {
         get { return stableWindow; }
         set
         {
            if (stableWindowValidation && value < StableWindowMinimum)
            {
               throw new ArgumentOutOfRangeException();
            }
            stableWindow = value;
         }
      }

      public int AveragingWindow 
      {
         get { return averagingWindow; }
         set
         {
            if (averagingWindowValidation && value < AveragingWindowMinimum)
            {
               throw new ArgumentOutOfRangeException();
            }
            averagingWindow = value;
         }
      }

      #region Validation

      public int StableWindowMinimum
      {
         get { return stableWindowMinimum; }
         set
         {
            stableWindowMinimum = value;
            stableWindowValidation = true;
         }
      }

      public int AveragingWindowMinimum
      {
         get { return averagingWindowMinimum; }
         set
         {
            averagingWindowMinimum = value;
            averagingWindowValidation = true;
         }
      }

      #endregion
   }
}