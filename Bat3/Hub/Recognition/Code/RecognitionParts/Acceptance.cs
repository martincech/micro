using BatLibrary;
using Recognition.Types;

namespace Recognition.RecognitionParts
{
   /// <summary>
   /// Filter samples in accepted range of values for male/female 
   /// </summary>
   public class Acceptance : RecognitionChainPart<DifferenceWeight, BirdWeight>
   {
      public Acceptance(AcceptanceParams acceptanceParams)
      {
         Params = acceptanceParams;
      }

      public AcceptanceParams Params { get; set; }

      /// <summary>
      /// Add sample to filter
      /// </summary>
      /// <param name="rawWeight">sample value</param>
      public override void Add(DifferenceWeight rawWeight)
      {
         var birdSample = new BirdWeight(rawWeight.AsG){ TimeStamp = rawWeight.TimeStamp };
         if (Params == null)
         {
            OnRecognized(birdSample);
            return;
         }
         var sex = Params.SexMode;

         var weight = birdSample.AsG;
         var maleHigh = GetWeightOrDefault(Params.MaleHigh);
         var maleLow = GetWeightOrDefault(Params.MaleLow);
         var femaleHigh = GetWeightOrDefault(Params.FemaleHigh);
         var femaleLow = GetWeightOrDefault(Params.FemaleLow);
         var undefinedLow = GetWeightOrDefault(Params.UndefinedLow);
         var undefinedHigh = GetWeightOrDefault(Params.UndefinedHigh);

         if (rawWeight.Difference == DifferenceMode.Increased)
         {
            if (Params.StepMode == AcceptanceStepMode.Leave)
            {
               return;
            }
         }
         else
         {
            if (Params.StepMode == AcceptanceStepMode.Enter)
            {
               return;
            }
         }
         birdSample.Difference = rawWeight.Difference;

         //Calculate treshold when male and female target weights overlap
         if (Params.AcceptanceSexMode == AcceptanceSexMode.Mixed)
         {
            sex = Sex.Undefined;
            if (maleHigh > femaleHigh &&
                maleLow < femaleHigh)
            {
               var diff = (femaleHigh - maleLow)/2;
               femaleHigh -= diff;
               maleLow += diff;
            }

            if (femaleHigh > maleHigh &&
                femaleLow < maleHigh)
            {
               var diff = (maleHigh - femaleLow)/2;
               maleHigh -= diff;
               femaleLow += diff;
            }
         }

         //Determine if value is male/female 
         var isRecognized = false;
         if (sex == Sex.Undefined && Params.AcceptanceSexMode != AcceptanceSexMode.Mixed) // sex == Sex.Undefined
         {
            if (undefinedLow <= weight && weight <= undefinedHigh)
            {
               birdSample.Sex = Sex.Undefined;
               isRecognized = true;
            }
         }
         else
         {
            if (sex != Sex.Male)
            {
               if (femaleLow <= weight && weight <= femaleHigh)
               {
                  birdSample.Sex = Sex.Female;
                  isRecognized = true;
               }
            }
            if (sex != Sex.Female)
            {
               if (maleLow <= weight && weight <= maleHigh)
               {
                  birdSample.Sex = Sex.Male;
                  isRecognized = true;
               }
            }
         }
         if (isRecognized)
         {
            OnRecognized(birdSample);
         }
      }

      private static double GetWeightOrDefault(Weight weight)
      {
         return weight == null ? 0 : weight.AsG;
      }
   }
}