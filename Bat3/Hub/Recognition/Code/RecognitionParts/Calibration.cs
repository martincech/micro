﻿using System;
using BatLibrary;
using Recognition.Types;

namespace Recognition.RecognitionParts
{
   /// <summary>
   /// Convert to sensor output value to weight weight
   /// </summary>
   public class Calibration :
      RecognitionChainPart<AverageWeight, CalibratedWeight>,
      ICalibration
   {
      #region Private

      private const byte CALIBRATION_SAMPLE_DIFFERENCE = 10; // +-10%

      private bool calibrating;
      private Weight firstStableSample;
      private Weight calibrationWeight;
      private CalibrationParams calibrationParams;
      private FilterParams filterParams;
      private StableFiltration filtration;

      #endregion

      public Calibration()
      {
      }

      public Calibration(CalibrationParams calibrationParams)
      {
         CalibrationParams = calibrationParams;
      }

      #region Calibration process

      public event EventHandler<Weight> CalibrationWeightStabilized;

      public event EventHandler Calibrated;

      public void StartCalibration(Weight forWeight)
      {
         calibrationWeight = forWeight;
         calibrating = true;
         firstStableSample = null;
      }

      /// <summary>
      /// Stop calibration process
      /// </summary>
      public void CancelCalibration()
      {
         StopCalibration();
      }

      #endregion

      public CalibrationParams CalibrationParams
      {
         get { return calibrationParams; }
         set
         {
            StopCalibration();
            calibrationParams = value;
         }
      }

      public FilterParams FilterParams
      {
         get { return filterParams; }
         set
         {
            StopCalibration();
            filterParams = value;
            FiltrationInit();
         }
      }

      public override void Add(AverageWeight rawWeight)
      {
         if (calibrating)
         {
            if (filtration == null || filtration.Params == null || rawWeight.HighPass == null)
            {
               throw new NullReferenceException();
            }
            filtration.Add(new CalibratedWeight(rawWeight.AsG, rawWeight.HighPass.AsG));
         }
         else
         {
            Weight sampleValue;
            if (CalibrationParams == null)
            {
               sampleValue = rawWeight;
            }
            else
            {
               sampleValue = new Weight((rawWeight.AsG - CalibrationParams.ValueOffset.AsG) * CalibrationParams.ValueMultiplier);
            }

            OnRecognized(new CalibratedWeight(sampleValue.AsG, rawWeight.HighPass)
            {
               TimeStamp = rawWeight.TimeStamp
            });
         }
      }

      #region Private helpers

      private void FiltrationInit()
      {
         if (filtration != null)
         {
            filtration.Recognized -= FiltrationOnRecognized;
         }
         filtration = new StableFiltration(FilterParams);
         filtration.Recognized += FiltrationOnRecognized;
      }

      private void FiltrationOnRecognized(object sender, StableWeight stableWeight)
      {
         if (firstStableSample == null)
         {
            firstStableSample = stableWeight;
            OnStabilized(calibrationWeight);
            return;
         }


         var min = (100 - (double)CALIBRATION_SAMPLE_DIFFERENCE)/100 * firstStableSample;
         var max = (100 + (double)CALIBRATION_SAMPLE_DIFFERENCE) / 100 * firstStableSample;


         if (!(stableWeight.AsG < min || stableWeight.AsG > max))
         {
            return;
         }

         var secondStableSample = stableWeight;
         var diff = firstStableSample - secondStableSample;

         var offset = firstStableSample < secondStableSample ? firstStableSample : secondStableSample;
         var multiplier = calibrationWeight.AsG / diff.AsG;

         CalibrationParamsFound(multiplier, offset);
      }

      private void CalibrationParamsFound(double multiplier, Weight offset)
      {
         if (CalibrationParams == null)
         {
            CalibrationParams = new CalibrationParams();
         }


         CalibrationParams.ValueMultiplier = Math.Abs(multiplier);
         CalibrationParams.ValueOffset = new StableWeight(offset);
         StopCalibration();
         OnCalibrated();
      }

      private void StopCalibration()
      {
         calibrating = false;
         calibrationWeight = null;
         firstStableSample = null;
      }

      protected virtual void OnStabilized(Weight weight)
      {
         var handler = CalibrationWeightStabilized;
         if (handler != null) handler(this, weight);
      }

      protected virtual void OnCalibrated()
      {
         var handler = Calibrated;
         if (handler != null) handler(this, EventArgs.Empty);
      }

      #endregion
   }
}
