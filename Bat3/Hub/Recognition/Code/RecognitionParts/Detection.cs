﻿using Recognition.Types;

namespace Recognition.RecognitionParts
{
   public class Detection : RecognitionChainPart<StableWeight, DifferenceWeight>
   {
      #region Private

      private StableWeight lastStableWeight;

      #endregion

      public Detection()
      {
         lastStableWeight = new StableWeight();
      }

      public override void Add(StableWeight rawWeight)
      {
         var weight = rawWeight - lastStableWeight;
         lastStableWeight = rawWeight;
         OnRecognized(new DifferenceWeight(weight.AsG) { TimeStamp = rawWeight.TimeStamp});
      }
   }
}
