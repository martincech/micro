﻿using System;
using Recognition.Types;

namespace Recognition.RecognitionParts
{
   public class StableFiltration : RecognitionChainPart<CalibratedWeight, StableWeight>
   {
      #region Private fields

      private byte stableCounter;
      private bool stable;
      private const int PARAMS_STABLE_WINDOW_MINIMUM = 1;

      #endregion

      /// <summary>
      /// Filtration constructor
      /// </summary>
      /// <param name="fParams">filtration parametrs</param>
      public StableFiltration(FilterParams fParams)
      {
         Params = fParams;
         SetParametersLimits();

         stableCounter = 0;
         stable = false;
      }

      public FilterParams Params { get; set; }

      /// <summary>
      /// Add measured value into buffer
      /// </summary>
      /// <param name="rawWeight">measured value</param>
      public override void Add(CalibratedWeight rawWeight)
      {
         if (Params == null)
         {
            OnRecognized(new StableWeight(rawWeight.AsG) { TimeStamp = rawWeight.TimeStamp });
            return;
         }
         if (rawWeight.HighPass == null)
         {
            throw new ArgumentNullException();
         }

         if (rawWeight.HighPass < Params.StableRange.AsG)
         {
            if (stableCounter < Params.StableWindow)
            {
               stableCounter++;
            }
         }
         else
         {
            stableCounter = 0;
         }

         // check for stability duration
         if (stableCounter < Params.StableWindow)
         {
            stable = false;
            return;
         }

         //Return stable value only once
         if (stable) return;
         stable = true;
         OnRecognized(new StableWeight(rawWeight) { TimeStamp = rawWeight.TimeStamp });
      }


      private void SetParametersLimits()
      {
         if (Params != null)
         {
            Params.StableWindowMinimum = PARAMS_STABLE_WINDOW_MINIMUM;
         }
      }
      
   }
}
