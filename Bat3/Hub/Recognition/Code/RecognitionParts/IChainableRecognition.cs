﻿using BatLibrary;

namespace Recognition.RecognitionParts
{
   public interface IChainableRecognition<in T, U> : IWeightProcessingProcessing<T, U>
      where T : Weight
      where U : Weight
   {
      IWeightProcessingAdministrator<U> Successor { get; set; }
   }
}
