﻿using System;
using BatLibrary;

namespace Recognition.RecognitionParts
{
   /// <summary>
   /// Interface calibration of weight sensor
   /// </summary>
   public interface ICalibration
   {
      /// <summary>
      /// Fire when value is stabilized
      /// </summary>
      event EventHandler<Weight> CalibrationWeightStabilized;

      /// <summary>
      /// Fire when weight sensor is calibrated
      /// </summary>
      event EventHandler Calibrated;

      /// <summary>
      /// Start calibration
      /// </summary>
      /// <param name="forWeight">Calibration weight</param>
      void StartCalibration(Weight forWeight);

      /// <summary>
      /// Stop calibration process
      /// </summary>
      void CancelCalibration();

      /// <summary>
      /// Set calibration filter params
      /// </summary>
      FilterParams FilterParams { get; set; }
   }
}