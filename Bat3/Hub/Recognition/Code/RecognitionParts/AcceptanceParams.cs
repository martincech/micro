using BatLibrary;

namespace Recognition.RecognitionParts
{
   public class AcceptanceParams
   {
      public Weight MaleLow;
      public Weight MaleHigh;

      public Weight FemaleLow;
      public Weight FemaleHigh;

      public Weight UndefinedLow;
      public Weight UndefinedHigh;

      public AcceptanceStepMode StepMode;
      public AcceptanceSexMode AcceptanceSexMode;
      public Sex SexMode;
   }
}