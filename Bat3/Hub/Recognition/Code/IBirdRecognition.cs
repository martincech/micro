﻿using Recognition.RecognitionParts;
using Recognition.Types;

namespace Recognition
{

   public interface IBirdRecognition : IWeightProcessingProcessing<RawWeight, BirdWeight>
   {
      AcceptanceParams AcceptanceParams { get; set; }
      Calibration Calibration { get; }
   }
}
