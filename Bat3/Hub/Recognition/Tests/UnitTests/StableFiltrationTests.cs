﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BatLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Recognition.RecognitionParts;
using Recognition.Types;
using RecognitionTests.IntegrationTests;

namespace RecognitionTests.UnitTests
{
   [TestClass]
   public class StableFiltrationTests
   {
      private StableFiltration filtration;

      [TestInitialize]
      public void Init()
      {
         filtration = new StableFiltration(new FilterParams
         {
            StableRange = FiltrationTests.STABLE_RANGE,
            StableWindow = FiltrationTests.STABLE_WINDOW
         });
      }

      [TestMethod]
      public void Filter_WhenStable()
      {
         var samples = TestData.StablePreFilteredData(FiltrationTests.STABLE_RANGE.AsG);
         var stabilizedValue = 0.0;
         var sampleIndex = 0;
         var stableIndex = -1;

         var index = sampleIndex;
         filtration.Recognized += (sender, eventArgs) =>
         {
            if (stableIndex < 0)
            {
               stabilizedValue = eventArgs.AsG;
               stableIndex = index;
            }
            else
            {
               Assert.Fail("CalibrationWeightStabilized more than once!");
            }
         };

         foreach (var sample in samples)
         {
            filtration.Add(new CalibratedWeight(sample.AsG, sample.HighPass.AsG) { TimeStamp = DateTime.Now });
            sampleIndex++;
         }

         Assert.IsTrue(stableIndex > -1);
         Assert.IsTrue(stabilizedValue > 0);
         Debug.WriteLine("StableIndex " + stableIndex + " value " + stabilizedValue);
         var stableSamples = samples.ToList().Skip(stableIndex + 1 - FiltrationTests.STABLE_WINDOW).Take(FiltrationTests.STABLE_WINDOW);
         var result = stableSamples.Any(value => (value.AsG - FiltrationTests.STABLE_RANGE.AsG) > stabilizedValue && stabilizedValue > (value.AsG - FiltrationTests.STABLE_RANGE.AsG));
         Assert.IsFalse(result);
      }

      [TestMethod]
      public void Filter_WhenStableMoreThanOnce()
      {
         var samples = TestData.StablePreFilteredData(FiltrationTests.STABLE_RANGE.AsG);
         var stabilized = new List<double[]>();
         var sampleIndex = 0;

         var index = sampleIndex;
         filtration.Recognized += (sender, eventArgs) =>
         {
            stabilized.Add(new[] { index, eventArgs.AsG });
         };

         foreach (var sample in samples)
         {
            filtration.Add(new CalibratedWeight(sample.AsG, sample.HighPass.AsG));
            sampleIndex++;
         }

         foreach (var sample in samples)
         {
            filtration.Add(new CalibratedWeight(sample.AsG + 50, sample.HighPass.AsG));                  
            sampleIndex++;
         }

         foreach (var sample in samples)
         {
            filtration.Add(new CalibratedWeight(sample.AsG + 100, sample.HighPass.AsG));
            sampleIndex++;
         }

         Assert.IsTrue(stabilized.Count == 3);
         foreach (var stab in stabilized)
         {
            Assert.IsTrue(stab[1] > 0);
            var stableSamples = samples.ToList().Skip((int)stab[0] + 1 - FiltrationTests.STABLE_WINDOW).Take(FiltrationTests.STABLE_WINDOW);
            var result = stableSamples.Any(value => (value.AsG - FiltrationTests.STABLE_RANGE.AsG) > stab[1] && stab[1] > (value.AsG - FiltrationTests.STABLE_RANGE.AsG));
            Assert.IsFalse(result);
            Debug.WriteLine("StableIndex " + stab[0] + " value " + stab[1]);
         }
      }

    
      [TestMethod]
      public void Filter_WhenUnstable()
      {
         var samples = TestData.UnStablePreFilteredData(FiltrationTests.STABLE_WINDOW);

         filtration.Recognized += (sender, eventArgs) =>
         {
            Assert.Fail("Cant stabilize unstabilized values!");
         };

         foreach (var sample in samples)
         {
            filtration.Add(new CalibratedWeight(sample.AsG, sample.HighPass.AsG));
         }
         foreach (var sample in samples)
         {
            filtration.Add(new CalibratedWeight(sample.AsG + 50, sample.HighPass.AsG));
         }
      }

      [TestMethod]
      [ExpectedException(typeof(ArgumentOutOfRangeException))]
      public void ParamsOutOfRange_StableWindow()
      {
         filtration.Params.StableWindow = filtration.Params.StableWindowMinimum - 1;
      }

      [TestMethod]
      [ExpectedException(typeof(ArgumentNullException))]
      public void Filter_WeightWithoutHighPass()
      {
         var samples = TestData.STABLE_SAMPLES;
         foreach (var sample in samples)
         {
            filtration.Add(new CalibratedWeight(sample) { TimeStamp = DateTime.Now });
         }
      }


      [TestMethod]
      public void NullParameters_NoFiltration()
      {
         filtration.Params = null;
         var samples = TestData.STABLE_SAMPLES;
         var recognizedIndex = 0;
         var recognizedValues = new List<Weight>();
         filtration.Recognized += (sender, eventArgs) =>
         {
            recognizedIndex++;
            recognizedValues.Add(new Weight(eventArgs.AsG));
         };

         foreach (var sample in samples)
         {
            filtration.Add(new CalibratedWeight(sample));
         }
         Assert.AreEqual(samples.Count(), recognizedIndex);
         CollectionAssert.AreEqual(samples, recognizedValues);
      }
   }
}
