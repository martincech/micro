using System;

namespace Core
{
   public interface ICommands
   {
      void RunRealProcessing();

      bool CanRunRealProcessing();

      void StopRealProcessing();

      bool CanStopRealProcessing();

      void RunRandomProcessing();

      bool CanRunRandomProcessing();

      void RunCalibrationProcessing();

      bool CanRunCalibrationProcessing();

      void RunNextDay();

      bool CanRunNextDay();

      event EventHandler CanRunChanged;

      event EventHandler ConnectionLost;
   }
}