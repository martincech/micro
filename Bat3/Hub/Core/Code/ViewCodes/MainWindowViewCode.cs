﻿using System;
using System.ComponentModel;
using BatLibrary;
using Core.Data;
using Core.Helpers;
using Core.Interfaces;

namespace Core.ViewCodes
{
   public class MainWindowViewCode
   {
      private const double WEIGHT_RAW_CONSTANT = 10.0;

      private const int GRAM_DECIMALS = 0;
      private const int DECIMALS = 4;

      private readonly IMainWindowViewModel viewModel;
      private readonly DataModel dataModel;

      public MainWindowViewCode(DataModel dataModel, IMainWindowViewModel viewModel)
      {
         // TODO vm.propertychanged
         this.dataModel = dataModel;
         this.dataModel.PropertyChanged += data_PropertyChanged;

         this.viewModel = viewModel;
         this.viewModel.PropertyChanged += viewModel_PropertyChanged;
      }

      void viewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         if (NameHelper.GetName(() => viewModel.Unit) == e.PropertyName)
         {
            if (dataModel.Unit != viewModel.Unit)
            {
               dataModel.Unit = viewModel.Unit;
            }
         }
         if (NameHelper.GetName(() => viewModel.IsError) == e.PropertyName)
         {
            if (viewModel.IsError)
            {
               AlertCommand();
            }
         }
      }

      private void data_PropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         if (NameHelper.GetName(() => dataModel.ActualWeight) == e.PropertyName)
         {
            viewModel.ActualWeight = ConvertWeight(dataModel.ActualWeight.Value);
            UpdateStatistic();
         }
         if (NameHelper.GetName(() => dataModel.Unit) == e.PropertyName && dataModel.ActualWeight != null)
         {
            viewModel.ActualWeight = ConvertWeight(dataModel.ActualWeight.Value);
            UpdateStatistic();
         }
         if (NameHelper.GetName(() => dataModel.Statistic) == e.PropertyName)
         {
            UpdateStatistic();
         }
      }

      private void UpdateStatistic()
      {
         viewModel.Statistic.Average = ConvertWeight(dataModel.Statistic.Average);
         viewModel.Statistic.Gain = ConvertWeight(dataModel.Statistic.Gain);
         viewModel.Statistic.Count = dataModel.Statistic.Count;
         viewModel.Statistic.Cv = Math.Round(dataModel.Statistic.Cv, 1);
         viewModel.Statistic.Sigma = Math.Round(dataModel.Statistic.Sigma, 1);
         viewModel.Statistic.Uniformity = Math.Round(dataModel.Statistic.Uniformity, 1);
      }

      // TODO - spouští se na nastavení true na Error property
      private void AlertCommand()
      {
         dataModel.AlertText = "ERROR" + DateTime.Now;
      }

      /// <summary>
      /// Convert Raw weight to selected units
      /// </summary>
      /// <returns>converted value</returns>
      private double ConvertWeight(double weight)
      {
         var result = BatLibrary.ConvertWeight.Convert(weight / WEIGHT_RAW_CONSTANT, Units.G, dataModel.Unit);

         var decimals = GRAM_DECIMALS;
         if (dataModel.Unit != Units.G)
         {
            decimals = DECIMALS;
         }

         return Math.Round(result, decimals);
      }
   }
}
