﻿using BatLibrary;

namespace Core.Statistics
{

   public interface IWeightStatisticsCalculator
   {
      void AddSample(Weight sample);
      void ResetCalculation();
   }
}
