﻿using System;
using System.Collections.Generic;
using System.Linq;
using BatLibrary;
using Utilities.Extensions;

namespace Core.Statistics
{
   internal class WeightStatisticsCalculator : IWeightStatisticsCalculator
   {
      public const int DEFAULT_UNIFORMITY = 10;

      #region Private fields

      private readonly IWeightStatistics weightStatistics;
      private readonly List<Weight> values;
      private double uniformityRangePercent;
      private Weight sum;
      private Weight squareSum;
      private Weight previousAverage;

      #endregion

      public WeightStatisticsCalculator(
         IWeightStatistics weightStatistics) : this(weightStatistics, DEFAULT_UNIFORMITY)
      {
      }

      public WeightStatisticsCalculator(
         IWeightStatistics weightStatistics,
         double uniformityRangePercent,
         IEnumerable<Weight> samples = null)
      {
         sum = new Weight();
         squareSum = new Weight();
         previousAverage = new Weight();
         this.weightStatistics = weightStatistics;
         values = new List<Weight>();
         this.uniformityRangePercent = uniformityRangePercent;
         if (samples != null)
         {
            AddSample(samples);
         }
      }

      public double UniformityRangePercentPercent
      {
         get { return uniformityRangePercent; }
         set
         {
            uniformityRangePercent = value;
            RecalcTodayStats();
         }
      }

      /// <summary>
      /// Add samples to statistic.
      /// </summary>
      /// <param name="samples">list of <see cref="Weight"/></param>
      public void AddSample(IEnumerable<Weight> samples)
      {
         if (samples == null) return;

         samples.ForEach(AddSample);
      }

      /// <summary>
      /// Add one sample to statistic.
      /// </summary>
      /// <param name="sample"><see cref="Weight"/></param>
      public void AddSample(Weight sample)
      {
         if (sample == null) return;

         sum += sample;
         squareSum += sample*sample;
         values.Add(sample);
         RecalcTodayStats();
      }

      public void ResetCalculation()
      {
         previousAverage = CalcAverage();
         sum = new Weight();
         squareSum = new Weight();
         values.Clear();
         RecalcTodayStats();
      }

      #region Calculations

      protected double CalcSigma()
      {
         if (values.Count <= 1)
         {
            return 0;
         }
         var sigma = Math.Sqrt(1.0/(values.Count - 1)*(squareSum.AsG - (sum.AsG*sum.AsG/values.Count)));
         if (sigma < 0)
         {
            sigma *= -1;
         }
         return sigma;
      }

      protected double CalcUniformity()
      {
         if (values.Count == 0)
         {
            return 100.0; // empty list
         }
         var average = CalcAverage();
         var min = average*(100 - uniformityRangePercent)/100.0;
         var max = average*(100 + uniformityRangePercent)/100.0;
         var inside = values.Count(v => v >= min && v <= max);
         return 100.0*inside/(values.Count);
      }

      protected Weight CalcAverage()
      {
         if (values.Count == 0)
         {
            return new Weight();
         }
         return sum/values.Count;
      }

      protected double CalcVariation()
      {
         var average = CalcAverage();
         if (average == 0)
         {
            return 0;
         }
         return CalcSigma()/average.AsG*100.0;
      }

      protected Weight CalcGain()
      {
         if(values.Count == 0) return new Weight(0);
         return previousAverage == 0 ? CalcAverage() : CalcAverage() - previousAverage;
      }

      protected void RecalcTodayStats()
      {
         weightStatistics.Count = values.Count;
         weightStatistics.Average = CalcAverage();
         weightStatistics.Uniformity = CalcUniformity();
         weightStatistics.Sigma = CalcSigma();
         weightStatistics.Cv = CalcVariation();
         weightStatistics.Gain = CalcGain();
      }

      #endregion
   }
}
