﻿namespace Core.Sensors.Weight
{
   public interface IActualWeightOnSensor
   {
      BatLibrary.Weight Weight { get; set; }
   }
}