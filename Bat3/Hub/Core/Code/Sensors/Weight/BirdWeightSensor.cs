﻿using System;
using System.Diagnostics;
using Communication.Readers;
using Communication.Samples;
using Core.Statistics;
using Recognition;
using Recognition.Types;

namespace Core.Sensors.Weight
{
   public class BirdWeightSensor : Sensor<BirdWeight>, IDisposable
   {
      #region Private fields

      private IWeightReader reader;
      private IBirdRecognition recognition;

      #endregion

      public BirdWeightSensor() { }

      public BirdWeightSensor(
         IWeightReader reader,
         IBirdRecognition recognition, 
         IWeightStatisticsCalculator weightStatistics)
      {
         Reader = reader;
         Recognition = recognition;
         WeightStatistics = weightStatistics;
      }

      public IWeightReader Reader
      {
         get { return reader; }
         set
         {
            if (reader != null)
            {
               reader.NewSample -= NewWeightSample;
            }
            reader = value;

            if (value == null) return;
            reader.NewSample += NewWeightSample;
         }
      }

      public IBirdRecognition Recognition
      {
         get { return recognition; }
         set
         {
            if (recognition != null)
            {
               recognition.Recognized -= RecognitionOnRecognized;
            }
            recognition = value;
            
            if (value == null) return;
            recognition.Recognized += RecognitionOnRecognized;
         }
      }

      public IWeightStatisticsCalculator WeightStatistics { get; set; }

      #region Helpers

      private void NewWeightSample(object sender, WeightSample sample)
      {
         if (sample == null) { return; }

         if (Recognition == null) throw new NullReferenceException();

         Recognition.Add(new RawWeight(sample.ToUnit(BatLibrary.Weight.WeightUnits.G))
         {
            TimeStamp = sample.TimeStamp
         });
      }

      private void RecognitionOnRecognized(object sender, BirdWeight birdWeight)
      {
         if (birdWeight == null) { return; }

         if (WeightStatistics != null)
         {
            WeightStatistics.AddSample(birdWeight);
         }
         Values.Add(birdWeight);
      }
      
      #endregion

      #region Implementation of IDisposable

      /// <summary>
      /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
      /// </summary>
      public void Dispose()
      {
         Debug.WriteLine("disposing");
      }

      #endregion
   }
}