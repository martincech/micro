﻿using System.Collections.ObjectModel;
using System.Linq;

namespace Core.Sensors
{
   public abstract class Sensor<TSample>
   {    
      protected Sensor()
      {
         Values = new ObservableCollection<TSample>();
      }


      public TSample LastValue
      {
         get { return Values.LastOrDefault(); }
      }

      public ObservableCollection<TSample> Values { get; protected set; }
   }
}
