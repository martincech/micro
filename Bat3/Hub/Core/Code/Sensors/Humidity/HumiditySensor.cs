﻿using Communication.Readers;
using Communication.Samples;

namespace Core.Sensors.Humidity
{
   public class HumiditySensor : Sensor<HumiditySample>
   {
      private IHumidityReader reader;
      private readonly IActualHumidityOnSensor humiditySensor;

      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public HumiditySensor(IActualHumidityOnSensor humiditySensor)
      {
         this.humiditySensor = humiditySensor;
      }

      public IHumidityReader Reader
      {
         get { return reader; }
         set
         {
            if (reader != null)
            {
               reader.NewSample -= NewSample;
            }
            reader = value;

            if (value == null) return;
            value.NewSample += NewSample;
         }
      }

      private void NewSample(object sender, HumiditySample e)
      {
         if (e == null) { return; }

         if (humiditySensor != null)
         {
            humiditySensor.Humidity = e.Value;
         }
         Values.Add(e);
      }
   }
}
