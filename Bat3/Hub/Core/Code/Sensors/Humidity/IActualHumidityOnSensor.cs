﻿namespace Core.Sensors.Humidity
{
   public interface IActualHumidityOnSensor
   {
      byte Humidity { get; set; }
   }
}
