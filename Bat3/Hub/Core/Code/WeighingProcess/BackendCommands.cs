﻿using System;
using Communication.Readers;
using Communication.SensorManagers;
using Core.Calibration;
using Core.Sensors;
using Core.WeighingProcess.Co2;
using Core.WeighingProcess.Humidity;
using Core.WeighingProcess.Temperature;
using Core.WeighingProcess.Weight;

namespace Core.WeighingProcess
{
   internal class BackendCommands : ICommands
   {
      public void RunRealProcessing()
      {
         if (!CanRunRealProcessing())
         {
            return;
         }
         if (randomGenerator != null)
         {
            randomGenerator.StopReading();
         }
         weighingProcess.ProcessFromSensor(realWeightReader);
         temperatureProcess.ProcessFromSensor(realTemperatureReader);
         co2Process.ProcessFromSensor(realCo2Reader);
         humidityProcess.ProcessFromSensor(realHumidityReader);
         realWeightReader.StartReading();
         realTemperatureReader.StartReading();
         realCo2Reader.StartReading();
         realHumidityReader.StartReading();

         realCo2Reader.NewSample += (senderN, args) =>
         {
            actualValueOnSensor.SensorsConnected = true;
         };

         //TODO: get PARAMS and START DATE from user
         if (weighingCycle.IsRunning) weighingCycle.StopCycle();
         weighingCycle.SetDayParams(WeightParamsSample.GetParams());
         weighingCycle.StartCycle(new DateTime(1, 1, 1, 22, 0, 0));
        
         RaiseCommandCanExecuteChanged();
      }

      public bool CanRunRealProcessing()
      {
         return realWeightReader != null && !realWeightReader.IsReading;
      }

      public void StopRealProcessing()
      {
         if (realWeightReader != null && realWeightReader.IsReading)
         {
            realWeightReader.StopReading();
         }
         if (realTemperatureReader != null && realTemperatureReader.IsReading)
         {
            realTemperatureReader.StopReading();
         }
         if (realCo2Reader != null && realCo2Reader.IsReading)
         {
            realCo2Reader.StopReading();
         }
         if (realHumidityReader != null && realHumidityReader.IsReading)
         {
            realHumidityReader.StopReading();
         }
         RaiseCommandCanExecuteChanged();
      }

      public bool CanStopRealProcessing()
      {
         return realWeightReader != null && realWeightReader.IsReading;
      }

      public void RunRandomProcessing()
      {
         if (!CanRunRandomProcessing())
         {
            return;
         }
         if (realWeightReader != null)
         {
            realWeightReader.StopReading();
         }

         if (realTemperatureReader != null)
         {
            realTemperatureReader.StopReading();
         }

         if (realCo2Reader != null)
         {
            realCo2Reader.StopReading();
         }

         if (realHumidityReader != null)
         {
            realHumidityReader.StopReading();
         }

         weighingProcess.ProcessFromSensor(randomGenerator);
         temperatureProcess.ProcessFromSensor(randomGenerator as ITemperatureReader);
         co2Process.ProcessFromSensor(randomGenerator as ICo2Reader);
         humidityProcess.ProcessFromSensor(randomGenerator as IHumidityReader);
         randomGenerator.StartReading();
         RaiseCommandCanExecuteChanged();
      }

      public bool CanRunRandomProcessing()
      {
         return randomGenerator != null && !randomGenerator.IsReading;
      }

      public void RunCalibrationProcessing()
      {
         if (!CanRunRandomProcessing())
         {
            return;
         }
         calibrationProcess.InitCalibration(weighingProcess.WeightSensor.Recognition.Calibration);
      }

      public bool CanRunCalibrationProcessing()
      {
         return realWeightReader != null && realWeightReader.IsReading;
      }

      public void RunNextDay()
      {
         weighingCycle.NextDay();
      }

      public bool CanRunNextDay()
      {
         return weighingCycle.IsRunning;
      }


      public event EventHandler CanRunChanged;

      public event EventHandler ConnectionLost;

      #region Private fieds

      private readonly IWeighingProcess weighingProcess;
      private readonly ISensorsManager sensorManager;
      private readonly ITemperatureProcess temperatureProcess;
      private readonly ICo2Process co2Process;
      private readonly IHumidityProcess humidityProcess;
      private readonly ICalibrationProcess calibrationProcess;
      private readonly IWeightReader randomGenerator;
      private readonly IActualValueOnSensor actualValueOnSensor;
      private readonly IWeighingCycle weighingCycle;

      private IWeightReader realWeightReader;
      private ITemperatureReader realTemperatureReader;
      private ICo2Reader realCo2Reader;
      private IHumidityReader realHumidityReader;



      #endregion

      public BackendCommands(
         IWeighingProcess weighingProcess,
         IWeightReader randomGenerator,
         ISensorsManager sensorManager,
         ITemperatureProcess temperatureProcess,
         ICo2Process co2Process,
         IHumidityProcess humidityProcess,
         ICalibrationProcess calibrationProcess,
         IActualValueOnSensor actualValueOnSensor,
         IWeighingCycle weighingCycle)
      {
         this.weighingProcess = weighingProcess;
         this.randomGenerator = randomGenerator;
         this.sensorManager = sensorManager;
         this.temperatureProcess = temperatureProcess;
         this.co2Process = co2Process;
         this.humidityProcess = humidityProcess;
         this.calibrationProcess = calibrationProcess;
         this.actualValueOnSensor = actualValueOnSensor;
         this.weighingCycle = weighingCycle;
         InitSensorConnections();
      }


      protected virtual void RaiseCommandCanExecuteChanged()
      {
         var handler = CanRunChanged;
         if (handler != null) handler(this, EventArgs.Empty);
      }

      protected virtual void OnConnectionLost()
      {
         var handler = ConnectionLost;
         if (handler != null) handler(this, EventArgs.Empty);
      }

      #region Helpers

      private void InitSensorConnections()
      {
         InitSensorsManagers();
         InitSensorConnectionLost();
      }

      private void InitSensorsManagers()
      {
         sensorManager.DeviceConnected += (sender, reader) =>
         {
            temperatureProcess.ProcessFromSensor(reader.TemperatureReader);
            realTemperatureReader = reader.TemperatureReader;
            realCo2Reader = reader.Co2Reader;
            realHumidityReader = reader.HumidityReader;
            actualValueOnSensor.TerminalConnected = true;

            var setToProcess = false;
            if (realWeightReader != null)
            {
               if (
                  weighingProcess.WeightSensor != null &&
                  weighingProcess.WeightSensor.Reader == realWeightReader)
               {
                  weighingProcess.ProcessFromSensor(null);
                  setToProcess = true;
               }
               realWeightReader.StopReading();
            }
            realWeightReader = reader.WeightReader;
            //realWeightReader.StartReading();
            if (setToProcess)
            {
               weighingProcess.ProcessFromSensor(realWeightReader);
            }
            RaiseCommandCanExecuteChanged();

         };
         sensorManager.DeviceDisconnected += (sender, reader) =>
         {
            reader.Co2Reader.StopReading();
            reader.TemperatureReader.StopReading();
            reader.HumidityReader.StartReading();
            actualValueOnSensor.TerminalConnected = false;
            actualValueOnSensor.SensorsConnected = false;

            if (
             weighingProcess.WeightSensor != null &&
             weighingProcess.WeightSensor.Reader == realWeightReader)
            {
               weighingProcess.ProcessFromSensor(null);
            }
            realWeightReader.StopReading();
            realWeightReader = null;
            RaiseCommandCanExecuteChanged();
         };
      }

      private void InitSensorConnectionLost()
      {

         sensorManager.ConnectionLost += (sender, args) =>
         {
            if (actualValueOnSensor.SensorsConnected)
            {
               actualValueOnSensor.SensorsConnected = false;
               OnConnectionLost();
            }
         };
      }

      #endregion
   }
}
