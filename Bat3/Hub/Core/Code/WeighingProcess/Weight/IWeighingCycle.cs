﻿using System;
using System.Collections.Generic;
using Recognition.RecognitionParts;

namespace Core.WeighingProcess.Weight
{
   public interface IWeighingCycle
   {
      /// <summary>
      /// Returns true if weighing cycle is running
      /// </summary>
      bool IsRunning { get; }
      /// <summary>
      /// Set acceptance parametrs for each day
      /// </summary>
      /// <param name="dayParams">Collection of weighing paramametrs</param>
      void SetDayParams(Dictionary<int, AcceptanceParams> dayParams);
      /// <summary>
      /// Returns actual day and weighing parametrs
      /// </summary>
      KeyValuePair<int, AcceptanceParams> ActiveDay { get; }
      /// <summary>
      /// Start weighing cycle
      /// </summary>
      /// <param name="dayEndTime">Time when weighing cycle go to next day</param>
      /// <returns></returns>
      bool StartCycle(DateTime dayEndTime);
      /// <summary>
      /// Stop weighing cycle
      /// </summary>
      void StopCycle();
      /// <summary>
      /// Go to next weighing day
      /// </summary>
      void NextDay();
   }
}