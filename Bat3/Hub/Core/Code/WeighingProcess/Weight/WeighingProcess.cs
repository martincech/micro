﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Communication.Readers;
using Core.Sensors;
using Core.Sensors.Weight;
using Core.Statistics;
using Recognition;
using Recognition.RecognitionParts;

namespace Core.WeighingProcess.Weight
{
   internal class WeighingProcess : IWeighingProcess
   {
      private readonly ISensorHistory sensorHistory;

      public WeighingProcess(
         Func<IBirdRecognition> birdRecognitionFactory,
         Func<IWeightStatisticsCalculator> weightStatisticsCalculatorFactory,
         ISensorHistory sensorHistory)
      {
         this.sensorHistory = sensorHistory;
         BirdRecognitionFactory = birdRecognitionFactory;
         WeightStatisticsCalculatorFactory = weightStatisticsCalculatorFactory;
         WeightSensor = new BirdWeightSensor();
         WeightSensor.Values.CollectionChanged += Weights_CollectionChanged;
      }

      void Weights_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         foreach (var newItem in e.NewItems)
         {
            sensorHistory.WeightList.Add(new KeyValuePair<int, BatLibrary.Weight>(WeighingDay, newItem as BatLibrary.Weight));
         }
      }

      #region Overrides of IWeighingProcess

      public void CloseDay(AcceptanceParams acceptanceForToday)
      {
         WeightSensor.Recognition.AcceptanceParams = acceptanceForToday;
         WeightSensor.WeightStatistics.ResetCalculation();
      }

      private Func<IBirdRecognition> BirdRecognitionFactory { get; set; }
      private Func<IWeightStatisticsCalculator> WeightStatisticsCalculatorFactory { get; set; }

      public BirdWeightSensor WeightSensor { get; private set; }
      public int WeighingDay { get; set; }

      public void ProcessFromSensor(IWeightReader sensor)
      {
         if (sensor == null)
         {
            WeightSensor.Reader = null;
            return;
         }
         if (WeightSensor.Reader == sensor)
         {
            return;
         }
         var recognition = BirdRecognitionFactory();
         var calculator = WeightStatisticsCalculatorFactory();

         WeightSensor.WeightStatistics = calculator;
         WeightSensor.Recognition = recognition;
         WeightSensor.Reader = sensor;
      }

      #endregion

   }
}
