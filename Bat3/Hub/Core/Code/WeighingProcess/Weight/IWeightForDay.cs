﻿namespace Core.WeighingProcess.Weight
{
   public interface IWeightForDay
   {
      /// <summary>
      /// Expected weight for actual weighing day
      /// </summary>
      BatLibrary.Weight ExpectedWeight { get; set; }
      /// <summary>
      /// Minimal weight for actual weighing day
      /// </summary>
      BatLibrary.Weight MinExpectedWeight { get; set; }
      /// <summary>
      /// Maximal weight for actual weighing day
      /// </summary>
      BatLibrary.Weight MaxExpectedWeight { get; set; }
      /// <summary>
      /// Actual weighing day
      /// </summary>
      int WeighingDay { get; set; }
   }
}
