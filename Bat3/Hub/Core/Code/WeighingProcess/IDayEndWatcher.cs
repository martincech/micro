using System;

namespace Core.WeighingProcess
{
   public interface IDayEndWatcher
   {
      /// <summary>
      /// Set time of the end of the day.
      /// Set to null if watcher should not watch
      /// </summary>
      DateTime? DayEndTime { get; set; }
      /// <summary>
      /// This event is raised when the end of the day is recognized
      /// </summary>
      event EventHandler DayEnded;
      /// <summary>
      /// This returns true if the watcher is watching the end of the day
      /// </summary>
      bool IsWatching { get; }
   }
}