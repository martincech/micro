﻿using System;
using System.Timers;

namespace Core.WeighingProcess
{
   internal class DayEndWatcher : IDayEndWatcher
   {
      #region Private properties

      private const int DAY_MS = 24*60*60*1000; // 1 day in milliseconds
      private Timer timer;                                        // timer for checking day end
      private DateTime? dayEndTime;

      #endregion

      public DayEndWatcher() : this(null)
      {
      }

      public DayEndWatcher(DateTime? dayEndTime)
      {
         if (dayEndTime == null) return;
         DayEndTime = dayEndTime.Value;
      }

      public event EventHandler DayEnded;
      public DateTime? DayEndTime
      {
         get { return dayEndTime; }
         set
         {
            dayEndTime = value;
            RestartTimer();
         }
      }

      public bool IsWatching { get { return dayEndTime != null; } }

      #region Helpers


      private void RestartTimer()
      {
         StopTimer();
         StartTimer();
      }

      private void StopTimer()
      {
         if (timer == null) return;

         timer.Elapsed -= TimerOnElapsed;
         timer.Enabled = false;
      }

      private void StartTimer()
      {
         if (DayEndTime == null || DayEndTime.Value == default(DateTime))
         {
            return;
         }
         timer = new Timer();
         var interval = DayEndTime.Value.TimeOfDay.Subtract(DateTime.Now.TimeOfDay).TotalMilliseconds;
         if (interval <= 0)
         {
            interval += DAY_MS;
         }
         timer.Interval = interval; //elapsed timer on dayEnd time
         timer.Elapsed += TimerOnElapsed;
         timer.Enabled = true;
      }

      private void TimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
      {
         OnDayEnded();
         RestartTimer();
      }

      private void OnDayEnded()
      {
         var handler = DayEnded;
         if (handler != null) handler(this, EventArgs.Empty);
      }

      #endregion

   }
}