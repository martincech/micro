﻿using Core.SensorReader;

namespace Core.WeighingProcess
{
   public interface ISensorSamplesRecognition
   {
      IWeightSamplesRecognition WeightSamplesRecognition { get; }
      ITemperatureSamplesRecognition TemperatureSamplesRecognition { get; }
      ICo2SamplesRecognition Co2SamplesRecognition { get; }
      IHumiditySamplesRecognition HumiditySamplesRecognition { get; }
   }
}