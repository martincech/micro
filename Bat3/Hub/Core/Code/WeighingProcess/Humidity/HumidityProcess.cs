﻿using Communication.Readers;
using Communication.Samples;
using Core.Sensors;
using Core.Sensors.Humidity;

namespace Core.WeighingProcess.Humidity
{
   internal class HumidityProcess : IHumidityProcess
   {
      private readonly ISensorHistory sensorHistory;

      public HumidityProcess(IActualValueOnSensor humidityReport, ISensorHistory sensorHistory)
      {
         this.sensorHistory = sensorHistory;
         HumiditySensor = new HumiditySensor(humidityReport);
         HumiditySensor.Values.CollectionChanged += Humiditys_CollectionChanged;
      }

      void Humiditys_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
      {
         foreach (var newItem in e.NewItems)
         {
            sensorHistory.HumidityList.Add(newItem as HumiditySample);
         }
      }

      #region Implementation of IHumidityProcess

      public HumiditySensor HumiditySensor { get; private set; }

      public void ProcessFromSensor(IHumidityReader sensor)
      {
         HumiditySensor.Reader = sensor;
      }

      #endregion
   }
}
