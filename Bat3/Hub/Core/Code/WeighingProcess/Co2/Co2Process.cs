﻿using System.Collections.Specialized;
using Communication.Readers;
using Communication.Samples;
using Core.Sensors;
using Core.Sensors.Co2;

namespace Core.WeighingProcess.Co2
{
   internal class Co2Process : ICo2Process
   {
      private readonly ISensorHistory sensorHistory;

      public Co2Process(IActualValueOnSensor co2Report, ISensorHistory sensorHistory)
      {
         this.sensorHistory = sensorHistory;
         Co2Sensor = new Co2Sensor(co2Report);
         Co2Sensor.Values.CollectionChanged += Co2s_CollectionChanged;
      }

      void Co2s_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         foreach (var newItem in e.NewItems)
         {
            sensorHistory.Co2List.Add(newItem as Co2Sample);
         }
      }

      #region Implementation of ICo2Process

      public Co2Sensor Co2Sensor { get; private set; }

      public void ProcessFromSensor(ICo2Reader sensor)
      {  
         Co2Sensor.Reader = sensor;
      }

      #endregion
   }
}
