﻿using Communication.Readers;
using Core.Sensors.Co2;

namespace Core.WeighingProcess.Co2
{
   public interface ICo2Process
   {
      Co2Sensor Co2Sensor { get; }
      void ProcessFromSensor(ICo2Reader sensor);
   }
}
