using System;
using Communication.Readers;
using Communication.Readers.AllSensors;
using Core.Calibration;
using Core.Statistics;
using Core.WeighingProcess;
using Core.WeighingProcess.Weight;
using Ninject;
using Ninject.Modules;
using Recognition;

namespace Core
{
   public class CoreModule : NinjectModule
   {
      #region Overrides of NinjectModule

      /// <summary>
      /// Loads the module into the kernel.
      /// </summary>
      public override void Load()
      {
         Bind<IDayEndWatcher, DayEndWatcher>().To<DayEndWatcher>();
         Bind<IWeightStatisticsCalculator, WeightStatisticsCalculator>().To<WeightStatisticsCalculator>();
         Bind<IBirdRecognition, FCFDARecognition, ReportableCfdaRecognition>().To<ReportableCfdaRecognition>();
         Bind<IWeighingProcess, WeighingProcess.Weight.WeighingProcess>().To<WeighingProcess.Weight.WeighingProcess>().InSingletonScope();
         Bind<WeighingProcess.Temperature.ITemperatureProcess>().To<WeighingProcess.Temperature.TemperatureProcess>();
         Bind<WeighingProcess.Co2.ICo2Process>().To<WeighingProcess.Co2.Co2Process>();
         Bind<WeighingProcess.Humidity.IHumidityProcess>().To<WeighingProcess.Humidity.HumidityProcess>();
         
         Bind<ICalibrationProcess, CalibrationProcess>().To<CalibrationProcess>();

         Bind<IWeightReader, ITemperatureReader>().To<RandomSensorValuesGenerator>()
            .WhenInjectedInto<BackendCommands>();

         Bind<IWeighingCycle>().To<WeighingCycle>();

         Bind<Func<IBirdRecognition>>().ToMethod(c => () => Kernel.Get<IBirdRecognition>());
         Bind<Func<IWeightStatisticsCalculator>>().ToMethod(c => () => Kernel.Get<IWeightStatisticsCalculator>());

         Bind<ICommands, BackendCommands>().To<BackendCommands>().InSingletonScope();
         CheckDependencies();
      }

      private void CheckDependencies()
      {
         // TODO check that all types can be created and hence that nothing is missing for proper functionality
      }

      #endregion
   }
}