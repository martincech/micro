﻿using Recognition.RecognitionParts;

namespace Core.Calibration
{
   public interface ICalibrationProcess
   {
      void InitCalibration(ICalibration cal);
   }
}