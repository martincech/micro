﻿namespace Core.Calibration
{
   public enum CalibrationProcessState
   {
      DefaultInsertCalibrationWeightRequest,
      Sampling,
      RemoveCalibrationWeightRequest,
      CalibrationComplete,
      Canceled
   }
}