﻿using System;
using BatLibrary;

namespace Core.Calibration
{
   /// <summary>
   /// Interface calibration of weight sensor
   /// </summary>
   public interface ICalibrationProcessMediator
   {

      CalibrationProcessState CurrentProcessState { get; set; }

      event EventHandler<Weight> StartCalibration;
      event EventHandler CancelCalibration;
   }
}
