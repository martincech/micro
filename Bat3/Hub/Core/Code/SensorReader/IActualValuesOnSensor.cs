﻿namespace Core.SensorReader
{
   public interface IActualValuesOnSensor
   {
      double ActualWeight { get; set; }
      double Temperature { get; set; }
      long Co2 { get; set; }
      byte Humidity { get; set; }
   }
}