﻿using Communication.Readers;
using Core.Sensors;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CoreTests.UnitTests.Sensors
{
   internal class SensorTester<TReader, VSensor, TSample> 
      where TReader : class, ISampleReader<TSample>
      where VSensor : Sensor<TSample>
      where TSample : new()
   {
      #region Private fields

      private readonly Mock<TReader> mockReader;
      private readonly VSensor sensor;

      #endregion

      #region Constructor

      public SensorTester(Mock<TReader> mReader, VSensor sensor)
      {
         mockReader = mReader;
         this.sensor = sensor;
      }

      #endregion

      #region Public

      public void CollectionChangedTest()
      {
         Assert.AreEqual(0, sensor.Values.Count);
         mockReader.Raise(m => m.NewSample += null, null, new TSample());
         Assert.AreEqual(1, sensor.Values.Count);
      }

      public void CollectionNotChangedTest_WhenNewSampleIsNull()
      {
         Assert.AreEqual(0, sensor.Values.Count);
         mockReader.Raise(m => m.NewSample += null, null, null);
         Assert.AreEqual(0, sensor.Values.Count);
      }

      public void ChangeReaderTest(Mock<TReader> anotherMockReader)
      {
         var originCount = sensor.Values.Count;
         var expectedCount = originCount + 1;
        
         anotherMockReader.Raise(m => m.NewSample += null, null, new TSample());
         Assert.AreEqual(expectedCount, sensor.Values.Count);
         // old reader has no effect
         mockReader.Raise(m => m.NewSample += null, null, new TSample());
         Assert.AreEqual(expectedCount, sensor.Values.Count);
      }

      public void RemoveReaderTest()
      {
         mockReader.Raise(m => m.NewSample += null, null, new TSample());
         Assert.AreEqual(0, sensor.Values.Count);
      }

      public void LastValueTest()
      {
         var sample = new TSample();
         mockReader.Raise(m => m.NewSample += null, null, new TSample());
         Assert.AreNotEqual(sample, sensor.LastValue);
         mockReader.Raise(m => m.NewSample += null, null, sample);
         Assert.AreEqual(sample, sensor.LastValue);
      }

      public void LastValueTest_WhenNewSampleIsNull()
      { 
         mockReader.Raise(m => m.NewSample += null, null, null);
         Assert.AreEqual(null, sensor.LastValue);
      }

      public void LastValueTest_WhenLastNewSampleIsNull()
      {
         var sample = new TSample();
         mockReader.Raise(m => m.NewSample += null, null, sample);
         Assert.AreEqual(sample, sensor.LastValue);
         mockReader.Raise(m => m.NewSample += null, null, null);
         Assert.AreEqual(sample, sensor.LastValue);
      }

      #endregion
   }
}
