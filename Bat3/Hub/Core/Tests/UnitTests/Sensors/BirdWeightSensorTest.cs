﻿using System;
using BatLibrary;
using Communication.Readers;
using Communication.Samples;
using Core.Sensors.Weight;
using Core.Statistics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Recognition;
using Recognition.Types;

namespace CoreTests.UnitTests.Sensors
{
   [TestClass]
   public class BirdWeightSensorTest
   {
      #region Private helpers

      private Mock<IWeightReader> mockReader;
      private Mock<IWeightStatisticsCalculator> mockStatisticCalc;
      private BirdWeightSensor sensor;
      private MockRecognizer mockRecognition;
      private Mock<IBirdRecognition> moqRecognition;

      #endregion

      [TestInitialize]
      public void TestInit()
      {
         mockReader = new Mock<IWeightReader>();
         var reader = mockReader.Object;
         mockRecognition = new MockRecognizer();
         mockStatisticCalc = new Mock<IWeightStatisticsCalculator>();
         var statCalc = mockStatisticCalc.Object;
         sensor = new BirdWeightSensor(reader, mockRecognition, statCalc);
      }


      [TestMethod]
      public void Values_CollectionChanged_WhenNewSample()
      {
         Assert.AreEqual(0, sensor.Values.Count);
         mockReader.Raise(m => m.NewSample += null, null, new WeightSample());
         Assert.AreEqual(1, sensor.Values.Count);
      }

      [TestMethod]
      public void Values_CollectionNotChanged_WhenNewSampleIsNull()
      {
         Assert.AreEqual(0, sensor.Values.Count);
         mockReader.Raise(m => m.NewSample += null, null, null);
         Assert.AreEqual(0, sensor.Values.Count);
      }

      [TestMethod]
      public void ChangeReader()
      {
         Values_CollectionChanged_WhenNewSample();

         var anotherReader = new Mock<IWeightReader>();
         sensor.Reader = anotherReader.Object;
         Assert.AreEqual(anotherReader.Object, sensor.Reader);
         anotherReader.Raise(m => m.NewSample += null, null, new WeightSample());
         Assert.AreEqual(2, sensor.Values.Count);
         // old reader has no effect
         mockReader.Raise(m => m.NewSample += null, null, new WeightSample());
         Assert.AreEqual(2, sensor.Values.Count);
      }

      [TestMethod, ExpectedException(typeof(NullReferenceException))]
      public void RemoveRecognition()
      {
         sensor.Recognition = null;
         TestCollectionChanged();
      }

      [TestMethod]
      public void RemoveReader()
      {
         sensor.Reader = null;
         TestCollectionChanged();
      }

      [TestMethod]
      public void LastValue()
      {
         var sampleDate = new DateTime(2016, 1, 20, 20, 0, 0);
         mockReader.Raise(m => m.NewSample += null, null, new WeightSample());
         Assert.AreNotEqual(sampleDate, sensor.LastValue.TimeStamp);
         mockReader.Raise(m => m.NewSample += null, null, new WeightSample() { TimeStamp = sampleDate });
         Assert.AreEqual(sampleDate, sensor.LastValue.TimeStamp);
      }

      [TestMethod]
      public void LastVale_WhenNewSampleIsNull()
      {
         Assert.AreEqual(null, sensor.LastValue);
         mockReader.Raise(m => m.NewSample += null, null, null);
         Assert.AreEqual(null, sensor.LastValue);
      }

      [TestMethod]
      public void LastVale_WhenLastNewSampleIsNull()
      {
         var sample = new WeightSample() { TimeStamp = DateTime.Now };
         mockReader.Raise(m => m.NewSample += null, null, sample);
         Assert.AreEqual(sample.TimeStamp, sensor.LastValue.TimeStamp);
         mockReader.Raise(m => m.NewSample += null, null, null);
         Assert.AreEqual(sample.TimeStamp, sensor.LastValue.TimeStamp);
      }

      [TestMethod]
      public void WeightStatisticsCalculator_AddSampleCalled_WhenNewSample()
      {
         var sample = new WeightSample() { TimeStamp = DateTime.Now, Value = 450 };
         mockStatisticCalc.Verify(s => s.AddSample(It.IsAny<Weight>()), Times.Never);
         mockReader.Raise(r => r.NewSample += null, null, sample);
         mockStatisticCalc.Verify(s => s.AddSample(It.IsAny<Weight>()), Times.Once);
      }

      [TestMethod]
      public void WeightStatisticsCalculator_AddSampleNotCalled_WhenNewSampleIsNull()
      {
         WeightStatisticsCalculator_AddSampleCalled_WhenNewSample();
         mockReader.Raise(r => r.NewSample += null, null, null);
         mockStatisticCalc.Verify(s => s.AddSample(It.IsAny<Weight>()), Times.Once);
      }

      [TestMethod, ExpectedException(typeof(NullReferenceException))]
      public void LastValue_WhenActualCo2OnSensorIsNull()
      {
         CreateBirdWeightSensorWithoutActualCo2OnSensor();
         LastValue();
      }

      [TestMethod, ExpectedException(typeof(NullReferenceException))]
      public void Values_WhenActualCo2OnSensorIsNull()
      {
         CreateBirdWeightSensorWithoutActualCo2OnSensor();
         Values_CollectionChanged_WhenNewSample();
      }

      [TestMethod]
      public void Recognition_AddCalled_WhenNewSample()
      {
         CreateBirdWeightSensorWithRecognitionMock();
         moqRecognition.Verify(v => v.Add(It.IsAny<RawWeight>()), Times.Never);
         mockReader.Raise(r => r.NewSample += null, null, new WeightSample());
         moqRecognition.Verify(v => v.Add(It.IsAny<RawWeight>()), Times.Once);
      }

      [TestMethod]
      public void Recognition_AddNotCalled_WhenNewSampleIsNull()
      {
         CreateBirdWeightSensorWithRecognitionMock();
         moqRecognition.Verify(v => v.Add(It.IsAny<RawWeight>()), Times.Never);
         mockReader.Raise(r => r.NewSample += null, null, null);
         moqRecognition.Verify(v => v.Add(It.IsAny<RawWeight>()), Times.Never);
      }

      private void CreateBirdWeightSensorWithRecognitionMock()
      {
         moqRecognition = new Mock<IBirdRecognition>();
         moqRecognition.Setup(s => s.Add(It.IsAny<RawWeight>())).Raises(r => r.Recognized += null, null, new BirdWeight() { TimeStamp = It.IsAny<DateTime>() });
         sensor.Recognition = moqRecognition.Object;
      }

      private void CreateBirdWeightSensorWithoutActualCo2OnSensor()
      {
         sensor = new BirdWeightSensor { Reader = mockReader.Object };
      }

      private void TestCollectionChanged()
      {
         mockReader.Raise(m => m.NewSample += null, null, new WeightSample());
      }

   }
}
