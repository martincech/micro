﻿using System;
using Communication.Readers;
using Communication.Samples;
using Core.Sensors.Co2;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CoreTests.UnitTests.Sensors
{
   [TestClass]
   public class Co2SensorTests
   {
      #region Private fields

      private Mock<ICo2Reader> mockReader;
      private Mock<IActualCo2OnSensor> mockCo2Sensor;
      private Co2Sensor sensor;
      private SensorTester<ICo2Reader, Co2Sensor, Co2Sample> tester;

      #endregion

      [TestInitialize]
      public void TestInit()
      {
         mockReader = new Mock<ICo2Reader>();
         mockCo2Sensor = new Mock<IActualCo2OnSensor>();
         sensor = new Co2Sensor(mockCo2Sensor.Object) { Reader = mockReader.Object };
         tester = new SensorTester<ICo2Reader, Co2Sensor, Co2Sample>(mockReader, sensor);
      }

      [TestMethod]
      public void Values_CollectionChanged_WhenNewSample()
      {
         tester.CollectionChangedTest();
      }

      [TestMethod]
      public void Values_CollectionNotChanged_WhenNewSampleIsNull()
      {
         tester.CollectionNotChangedTest_WhenNewSampleIsNull();
      }

      [TestMethod]
      public void ChangeReader()
      {
         Values_CollectionChanged_WhenNewSample();

         var anotherMockReader = new Mock<ICo2Reader>();
         sensor.Reader = anotherMockReader.Object;
         Assert.AreEqual(anotherMockReader.Object, sensor.Reader);
         tester.ChangeReaderTest(anotherMockReader);
      }

      [TestMethod]
      public void RemoveReader()
      {
         sensor.Reader = null;
         tester.RemoveReaderTest();
      }

      [TestMethod]
      public void LastValue()
      {
         tester.LastValueTest();
      }

      [TestMethod]
      public void LastVale_WhenNewSampleIsNull()
      {
         tester.LastValueTest_WhenNewSampleIsNull();
      }

      [TestMethod]
      public void LastVale_WhenLastNewSampleIsNull()
      {
         tester.LastValueTest_WhenLastNewSampleIsNull();
      }

      [TestMethod]
      public void ActualCo2OnSensorIsChanged_WhenNewSample()
      {
         var sample = new Co2Sample() {TimeStamp = DateTime.Now, Value = 1560};
         mockCo2Sensor.VerifySet(s => s.Co2 = It.IsAny<long>(), Times.Never);
         mockReader.Raise(r=>r.NewSample+=null,null,sample);
         mockCo2Sensor.VerifySet(s => s.Co2 = It.IsAny<long>(), Times.Once);
         mockCo2Sensor.VerifySet(s => s.Co2 = sample.Value, Times.Once);
      }

      [TestMethod]
      public void ActualCo2OnSensorIsNotChanged_WhenNewSampleIsNull()
      {
         ActualCo2OnSensorIsChanged_WhenNewSample();
         mockReader.Raise(r => r.NewSample += null, null, null);
         mockCo2Sensor.VerifySet(s => s.Co2 = It.IsAny<long>(), Times.Once);
      }

      [TestMethod]
      public void LastValue_WhenActualCo2OnSensorIsNull()
      {
         CreateCo2SensorWithoutActualCo2OnSensor();
         LastValue();
      }

      [TestMethod]
      public void Values_WhenActualCo2OnSensorIsNull()
      {
         CreateCo2SensorWithoutActualCo2OnSensor();
         Values_CollectionChanged_WhenNewSample();
      }

      private void CreateCo2SensorWithoutActualCo2OnSensor()
      {
         sensor = new Co2Sensor(null) { Reader = mockReader.Object };
         tester = new SensorTester<ICo2Reader, Co2Sensor, Co2Sample>(mockReader, sensor);
      }
   }
}
