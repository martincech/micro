﻿using System;
using Communication.Readers;
using Communication.Samples;
using Core.Sensors.Temperature;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CoreTests.UnitTests.Sensors
{
   [TestClass]
   public class TemperatureSensorTests
   {
      #region Private fields

      private Mock<ITemperatureReader> mockReader;
      private Mock<IActualTemperatureOnSensor> mockTemperatureSensor; 
      private TemperatureSensor sensor;
      private SensorTester<ITemperatureReader, TemperatureSensor, TemperatureSample> tester;

      #endregion

      [TestInitialize]
      public void TestInit()
      {
         mockReader = new Mock<ITemperatureReader>();
         mockTemperatureSensor = new Mock<IActualTemperatureOnSensor>();
         sensor = new TemperatureSensor(mockTemperatureSensor.Object) {Reader = mockReader.Object};
         tester = new SensorTester<ITemperatureReader, TemperatureSensor, TemperatureSample>(mockReader, sensor);
      }


      [TestMethod]
      public void Values_CollectionChanged_WhenNewSample()
      {
         tester.CollectionChangedTest();
      }

      [TestMethod]
      public void Values_CollectionNotChanged_WhenNewSampleIsNull()
      {
         tester.CollectionNotChangedTest_WhenNewSampleIsNull();
      }

      [TestMethod]
      public void ChangeReader()
      {
         Values_CollectionChanged_WhenNewSample();

         var anotherMockReader = new Mock<ITemperatureReader>();
         sensor.Reader = anotherMockReader.Object;
         Assert.AreEqual(anotherMockReader.Object, sensor.Reader);
         tester.ChangeReaderTest(anotherMockReader);
      }

      [TestMethod]
      public void RemoveReader()
      {
         sensor.Reader = null;
         tester.RemoveReaderTest();
      }

      [TestMethod]
      public void LastValue()
      {
         tester.LastValueTest();
      }

      [TestMethod]
      public void LastVale_WhenNewSampleIsNull()
      {
         tester.LastValueTest_WhenNewSampleIsNull();
      }

      [TestMethod]
      public void LastVale_WhenLastNewSampleIsNull()
      {
         tester.LastValueTest_WhenLastNewSampleIsNull();
      }

      [TestMethod]
      public void ActualTemperatureOnSensorIsChanged_WhenNewSample()
      {
         var sample = new TemperatureSample() { TimeStamp = DateTime.Now, Value = 1560 };
         mockTemperatureSensor.VerifySet(s => s.Temperature = It.IsAny<double>(), Times.Never);
         mockReader.Raise(r => r.NewSample += null, null, sample);
         mockTemperatureSensor.VerifySet(s => s.Temperature = It.IsAny<double>(), Times.Once);
         mockTemperatureSensor.VerifySet(s => s.Temperature = sample.Value, Times.Once);
      }

      [TestMethod]
      public void ActualTemperatureOnSensorIsNotChanged_WhenNewSampleIsNull()
      {
         ActualTemperatureOnSensorIsChanged_WhenNewSample();
         mockReader.Raise(r => r.NewSample += null, null, null);
         mockTemperatureSensor.VerifySet(s => s.Temperature = It.IsAny<double>(), Times.Once);
      }

      [TestMethod]
      public void LastValue_WhenActualTemperatureOnSensorIsNull()
      {
         CreateTemperatureSensorWithoutActualTemperatureOnSensor();
         LastValue();
      }

      [TestMethod]
      public void Values_WhenActualTemperatureOnSensorIsNull()
      {
         CreateTemperatureSensorWithoutActualTemperatureOnSensor();
         Values_CollectionChanged_WhenNewSample();
      }

      private void CreateTemperatureSensorWithoutActualTemperatureOnSensor()
      {
         sensor = new TemperatureSensor(null) { Reader = mockReader.Object };
         tester = new SensorTester<ITemperatureReader, TemperatureSensor, TemperatureSample>(mockReader, sensor);
      }
   }
}
