﻿using System;
using BatLibrary;
using Core.Calibration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Recognition.RecognitionParts;

namespace CoreTests.UnitTests
{
   [TestClass]
   public class CalibrationProcessTests
   {
      #region Private fields

      private Mock<ICalibrationProcessMediator> mockCalibrationProcessMediator;
      private ICalibrationProcessMediator calibrationProcessMediator;
      private ICalibrationProcess calibrationProcess;
      private Mock<ICalibration> mockCalibration;
      private const int CALIBRATED_WEIGHT = 500;
     
      #endregion
         
      [TestInitialize]
      public void TestInit()
      {
         mockCalibrationProcessMediator = new Mock<ICalibrationProcessMediator>();
         mockCalibrationProcessMediator.SetupAllProperties();
         calibrationProcessMediator = mockCalibrationProcessMediator.Object;
         mockCalibration = new Mock<ICalibration>();
         calibrationProcess = new CalibrationProcess(calibrationProcessMediator);
      }


      [TestMethod]
      public void TestWithMockCalibration_ThatStateChangesAccordingly()
      {
         RunCalibrationProcess();
      }

    
      [TestMethod]
      public void TestThatAfterFinishingTheProcessIsAgainInDefaultState()
      {      
         TestWithMockCalibration_ThatStateChangesAccordingly();
         InitCalibration();
         Assert.AreEqual(CalibrationProcessState.DefaultInsertCalibrationWeightRequest, calibrationProcessMediator.CurrentProcessState);
      }

      [TestMethod]
      public void RunCalibrationMoreThanOnce()
      {     
         TestWithMockCalibration_ThatStateChangesAccordingly();
         TestWithMockCalibration_ThatStateChangesAccordingly();
         TestWithMockCalibration_ThatStateChangesAccordingly();
         Assert.AreEqual(CalibrationProcessState.CalibrationComplete, calibrationProcessMediator.CurrentProcessState);
      }

      [TestMethod]
      public void CancelCalibrationAndStartAgain()
      {
         //cancel calibration after each part and then run whole process again        
         //cancel after init
         InitCalibration();
         CancelCalibration();
         RunCalibrationProcess();

         //cancel after start
         InitCalibration();
         StartCalibration();
         CancelCalibration();
         RunCalibrationProcess();

         //cancel during process
         InitCalibration();
         StartCalibration();
         CalibrationAddWeight();
         CancelCalibration();
         RunCalibrationProcess();
      }

      #region Private helpers

      private void RunCalibrationProcess()
      {
         InitCalibration();
         StartCalibration();
         CalibrationAddWeight();
         CalibrationRemoveWeight();
      }

      private void InitCalibration()
      {
         calibrationProcess.InitCalibration(mockCalibration.Object);
         Assert.AreEqual(CalibrationProcessState.DefaultInsertCalibrationWeightRequest,
            calibrationProcessMediator.CurrentProcessState);
      }

      private void StartCalibration()
      {
         mockCalibrationProcessMediator.Raise(m => m.StartCalibration += null, null, new Weight(CALIBRATED_WEIGHT));
         Assert.AreEqual(CalibrationProcessState.Sampling, calibrationProcessMediator.CurrentProcessState);
      }

      private void CalibrationAddWeight()
      {
         mockCalibration.Raise(m => m.CalibrationWeightStabilized += null, null, new Weight(CALIBRATED_WEIGHT));

         Assert.AreEqual(CalibrationProcessState.RemoveCalibrationWeightRequest,
            calibrationProcessMediator.CurrentProcessState);
      }

      private void CalibrationRemoveWeight()
      {
         mockCalibration.Raise(m => m.Calibrated += null, new EventArgs()); 
         Assert.AreEqual(CalibrationProcessState.CalibrationComplete, calibrationProcessMediator.CurrentProcessState);
      }

      private void CancelCalibration()
      {
         mockCalibrationProcessMediator.Raise(m => m.CancelCalibration += null, new EventArgs());
         Assert.AreEqual(CalibrationProcessState.Canceled, calibrationProcessMediator.CurrentProcessState);
      }

      #endregion
   }
}
