﻿using System;
using System.Collections.Generic;
using System.Linq;
using BatLibrary;
using Core.WeighingProcess;
using Core.WeighingProcess.Weight;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Recognition.RecognitionParts;

namespace CoreTests.UnitTests.Weighing
{
   [TestClass]
   public class WeighingCycleTests
   {
      private WeighingCycle weighingCycle;
      private Dictionary<int, AcceptanceParams> acceptanceParams;
      private Mock<IDayEndWatcher> mockDayEndWatcher;

      [TestInitialize]
      public void TestInit()
      {
         const Sex sex = Sex.Undefined;
         const AcceptanceSexMode sexMode = AcceptanceSexMode.Single;
         const byte marginBelow = 30;
         const byte marginAbow = 30;

         var mockWeighingProcess = new Mock<IWeighingProcess>();
         var mockWeightForDay = new Mock<IWeightForDay>();
         mockDayEndWatcher = new Mock<IDayEndWatcher>();
         mockDayEndWatcher.SetupSet(s =>s.DayEndTime = It.IsAny<DateTime>()).Callback(() =>
         {
            mockDayEndWatcher.SetupGet(s => s.IsWatching).Returns(true);
         });
         mockDayEndWatcher.SetupSet(s => s.DayEndTime = null).Callback(() =>
         {
            mockDayEndWatcher.SetupGet(s => s.IsWatching).Returns(false);
         });


         weighingCycle = new WeighingCycle(mockWeighingProcess.Object, mockDayEndWatcher.Object, mockWeightForDay.Object);

         acceptanceParams = new Dictionary<int, AcceptanceParams>()
         {
            { 1, CreateAcceptanceParams(sex, sexMode, new Weight(50), marginBelow, marginAbow) },
            { 2, CreateAcceptanceParams(sex, sexMode, new Weight(100), marginBelow, marginAbow) },
            { 3, CreateAcceptanceParams(sex, sexMode, new Weight(150), marginBelow, marginAbow) }
         };
      }

      [TestMethod]
      public void StartWeighingCycleWithAcceptanceParams()
      {
         weighingCycle.SetDayParams(acceptanceParams);
         Assert.IsFalse(weighingCycle.IsRunning);
         var result = weighingCycle.StartCycle(DateTime.Now.AddDays(-1));
         Assert.IsTrue(result);
         Assert.IsTrue(weighingCycle.IsRunning);
      }

      [TestMethod]
      public void StartWeighingCycleWithoutAcceptanceParams()
      {
         var result = weighingCycle.StartCycle(DateTime.Now);
         Assert.IsFalse(result);
         Assert.IsFalse(weighingCycle.IsRunning);
      }

      [TestMethod]
      public void StopWeighingCycle()
      {
         StartWeighingCycleWithAcceptanceParams();
         weighingCycle.StopCycle();
         Assert.IsFalse(weighingCycle.IsRunning);
      }

      [TestMethod]
      public void RestartWeighingCycle()
      {
         StopWeighingCycle();
         var result = weighingCycle.StartCycle(DateTime.Now.AddDays(-1));
         Assert.IsTrue(result);
         Assert.IsTrue(weighingCycle.IsRunning);

         Assert.AreEqual(acceptanceParams.FirstOrDefault().Key, weighingCycle.ActiveDay.Key);
         Assert.AreEqual(acceptanceParams.FirstOrDefault().Value, weighingCycle.ActiveDay.Value);
      }

      [TestMethod]
      public void StartWeighingParameters()
      {
         StartWeighingCycleWithAcceptanceParams();
         Assert.AreEqual(acceptanceParams.FirstOrDefault().Key, weighingCycle.ActiveDay.Key);
         Assert.AreEqual(acceptanceParams.FirstOrDefault().Value, weighingCycle.ActiveDay.Value);
      }

      [TestMethod]
      public void WeighingNextDayParameters()
      {
         StartWeighingParameters();
         weighingCycle.NextDay();
         Assert.AreEqual(acceptanceParams.ElementAt(1).Key, weighingCycle.ActiveDay.Key);
         Assert.AreEqual(acceptanceParams.ElementAt(1).Value, weighingCycle.ActiveDay.Value);
      }

      [TestMethod]
      public void NextDayStarte()
      {
         weighingCycle.SetDayParams(acceptanceParams);
         weighingCycle.StartCycle(DateTime.Now);
        
         Assert.AreEqual(acceptanceParams.ElementAt(0).Key, weighingCycle.ActiveDay.Key);
         Assert.AreEqual(acceptanceParams.ElementAt(0).Value, weighingCycle.ActiveDay.Value);
         weighingCycle.NextDay();
         Assert.AreEqual(acceptanceParams.ElementAt(1).Key, weighingCycle.ActiveDay.Key);
         Assert.AreEqual(acceptanceParams.ElementAt(1).Value, weighingCycle.ActiveDay.Value);
      }

      [TestMethod]
      public void ChangeDayParamsWhenWeighingCycleIsStopped()
      {
         StartWeighingCycleWithAcceptanceParams();
         weighingCycle.StopCycle();
         mockDayEndWatcher.Setup(s => s.IsWatching).Returns(false);
         weighingCycle.SetDayParams(acceptanceParams);
      }


      [TestMethod, ExpectedException(typeof(InvalidOperationException))]
      public void FireExceptionByChangingDayParamsWhenWeighingCycleIsRuning()
      {
         StartWeighingCycleWithAcceptanceParams();
         weighingCycle.SetDayParams(acceptanceParams);
      }

      #region Private helpers

      private AcceptanceParams CreateAcceptanceParams(Sex sex, AcceptanceSexMode sexMode, Weight awerageWeight, byte marginBelow, byte marginAbow)
      {
         var minWeight = new Weight(awerageWeight * (100 - marginBelow));
         var maxWeight = new Weight(awerageWeight * (100 + marginAbow));

         var acceptanceParam = new AcceptanceParams() { AcceptanceSexMode = sexMode, SexMode = sex };
         if (sex == Sex.Undefined && sexMode != AcceptanceSexMode.Mixed) // sex == Sex.Undefined
         {
            acceptanceParam.UndefinedHigh = maxWeight;
            acceptanceParam.UndefinedLow = minWeight;
         }
         else
         {
            if (sex != Sex.Male)
            {
               acceptanceParam.FemaleHigh = maxWeight;
               acceptanceParam.FemaleLow = minWeight;
            }
            if (sex != Sex.Female)
            {
               acceptanceParam.MaleHigh = maxWeight;
               acceptanceParam.MaleLow = minWeight;
            }
         }

         return acceptanceParam;
      }

      #endregion
   }


}
