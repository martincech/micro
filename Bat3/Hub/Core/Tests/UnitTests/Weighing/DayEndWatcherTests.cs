﻿using System;
using System.Reflection;
using System.Threading;
using Core.WeighingProcess;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Timer = System.Timers.Timer;

namespace CoreTests.UnitTests.Weighing
{
   [TestClass]
   public class DayEndWatcherTests
   {
      private const int DELAY = 1000;

      [TestMethod]
      public void IsNotWatchingWhenNoDayEndSet()
      {
         var dayEndWatcher = new DayEndWatcher();
         Assert.IsFalse(dayEndWatcher.IsWatching);
      }

      [TestMethod]
      public void IsWatchingWhenDayEndIsSet()
      {
         var actualTime = DateTime.Now.TimeOfDay;
         var dayEndTime = new DateTime(2015, 12, 12, actualTime.Hours, actualTime.Minutes, actualTime.Seconds);
         dayEndTime = dayEndTime.AddSeconds(10);
         var dayEndWatcher = new DayEndWatcher(dayEndTime);
         Assert.IsTrue(dayEndWatcher.IsWatching);
      }

      [TestMethod]
      public void DayEndEventRaisedWhenDayEndIsSet()
      {
         var raised = false;
         const int raiseAfterS = 1;
         var actualTime = DateTime.Now.TimeOfDay;
         
         var dayEndTime = new DateTime(2015, 12, 12, actualTime.Hours, actualTime.Minutes, actualTime.Seconds + raiseAfterS);
         var dayEndWatcher = new DayEndWatcher(dayEndTime);
         
         dayEndWatcher.DayEnded += (sender, args) => raised = true;
         Thread.Sleep(raiseAfterS * DELAY);
         Assert.IsTrue(dayEndWatcher.IsWatching);
         Assert.IsTrue(raised);
      }

      [TestMethod]
      public void DayEndEventNotRaisedWhenDayEndIsNotSet()
      {
         var raised = false;
         const int raiseAfterS = 1;
         var dayEndWatcher = new DayEndWatcher();

         dayEndWatcher.DayEnded += (sender, args) => raised = true;
         Thread.Sleep(raiseAfterS * DELAY);
         Assert.IsFalse(dayEndWatcher.IsWatching);
         Assert.IsFalse(raised);
      }

      [TestMethod]
      public void DayEndEventNotRaisedWhenDayEndIsUnSet()
      {
         var raised = false;
         const int raiseAfterS = 1;
         var actualTime = DateTime.Now.TimeOfDay;

         var dayEndTime = new DateTime(2015, 12, 12, actualTime.Hours, actualTime.Minutes, actualTime.Seconds + raiseAfterS);
         var dayEndWatcher = new DayEndWatcher(dayEndTime);

         dayEndWatcher.DayEnded += (sender, args) => raised = true;
         dayEndWatcher.DayEndTime = null;
         Thread.Sleep(raiseAfterS * DELAY);
         Assert.IsFalse(dayEndWatcher.IsWatching);
         Assert.IsFalse(raised);
      }

      
      [TestMethod]
      public void DayEndEventRaisedMultipleTimesWhenDayEndIsSet()
      {
         var raised = false;
         const int times = 5;

         var dayEndWatcher = new DayEndWatcher();
         var actualTime = DateTime.Now;
         var dayEndTime = new DateTime(actualTime.Year, actualTime.Month, actualTime.Day,
            actualTime.Hour, actualTime.Minute, actualTime.Second + 1);

         dayEndWatcher.DayEnded += (sender, args) =>
         {
            raised = true;
         };
         dayEndWatcher.DayEndTime = dayEndTime;

         for (var i = 0; i < times; i++)
         {  // simulate day end for 100 ms
            var po = new PrivateObject(dayEndWatcher);
            var timer = (Timer)po.GetField("timer", BindingFlags.NonPublic | BindingFlags.Instance);
            timer.Interval = 100;
            po.SetField("timer", BindingFlags.NonPublic | BindingFlags.Instance, timer);

            Thread.Sleep((int)timer.Interval + 20);
            Assert.IsTrue(dayEndWatcher.IsWatching);

            Assert.IsTrue(raised);
            raised = false;
         }
      }
   }
}
