﻿using System;
using System.Linq;
using Communication.Readers;
using Communication.Samples;
using Core.Statistics;
using Core.WeighingProcess.Weight;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Recognition;
using Recognition.RecognitionParts;
using Recognition.Types;

namespace CoreTests.UnitTests.WeighingProcess
{
   [TestClass]
   public class WeighingProcessTest
   {
      private BirdWeight sample;
      
      private MockSensorHistory mockSensorHistory;
      private Mock<IWeightReader> mockWeightReader;

      private Mock<IBirdRecognition> mockBirdRecognition;
      private Func<IBirdRecognition> mockBirdRecognitionFunc;

      private Mock<IWeightStatisticsCalculator> mockWeightStatisticsCalculator;
      private Func<IWeightStatisticsCalculator> mockWeightStatisticsCalculatorFunc;

      private IWeighingProcess weighingProcess;

      

      [TestInitialize]
      public void Init()
      {
         sample = new BirdWeight(54) { TimeStamp = DateTime.Now };

         mockBirdRecognition = new Mock<IBirdRecognition>();
         mockBirdRecognition.Setup(s => s.Add(It.IsAny<RawWeight>())).Raises(r => r.Recognized += null, null, sample);
         mockBirdRecognitionFunc = () => mockBirdRecognition.Object;

         mockWeightStatisticsCalculator = new Mock<IWeightStatisticsCalculator>();
         mockWeightStatisticsCalculatorFunc = () => mockWeightStatisticsCalculator.Object;
        
         mockSensorHistory = new MockSensorHistory();
         mockWeightReader = new Mock<IWeightReader>();

         weighingProcess = new Core.WeighingProcess.Weight.WeighingProcess(mockBirdRecognitionFunc, mockWeightStatisticsCalculatorFunc, mockSensorHistory);
         weighingProcess.ProcessFromSensor(mockWeightReader.Object);
      }

      [TestMethod]
      public void SensorHistoryChanged_WhenNewSample()
      {
         Assert.AreEqual(0, mockSensorHistory.WeightList.Count);
         mockWeightReader.Raise(s => s.NewSample += null, null, new WeightSample((int)sample.AsG, sample.TimeStamp));
         Assert.AreEqual(1, mockSensorHistory.WeightList.Count);
         Assert.AreEqual(sample, mockSensorHistory.WeightList.Last().Value);
   
      }

      [TestMethod]
      public void SensorHistoryNotChanged_WhenNewSampleIsNull()
      {
         Assert.AreEqual(0, mockSensorHistory.WeightList.Count);
         mockWeightReader.Raise(s => s.NewSample += null, null, null);
         Assert.AreEqual(0, mockSensorHistory.WeightList.Count);
      }


      [TestMethod]
      public void WeightSensorReaderChanged_WhenWeightReaderChanged()
      {
         var newReader = new Mock<IWeightReader>().Object;
         Assert.AreNotEqual(newReader, weighingProcess.WeightSensor.Reader);
         weighingProcess.ProcessFromSensor(newReader);
         Assert.AreEqual(newReader, weighingProcess.WeightSensor.Reader);
      }

      [TestMethod]
      public void WeightStatisticsReseted_WhenCloseDay()
      {
         mockWeightStatisticsCalculator.Verify(s => s.ResetCalculation(), Times.Never);
         weighingProcess.CloseDay(new AcceptanceParams());
         mockWeightStatisticsCalculator.Verify(s=>s.ResetCalculation(), Times.Once);
      }

      [TestMethod]
      public void RecognitionAcceptanceParamsCahnged_WhenCloseDay()
      {
         var newParams = new AcceptanceParams();
         mockBirdRecognition.VerifySet(s => s.AcceptanceParams = It.IsAny<AcceptanceParams>(), Times.Never);
         weighingProcess.CloseDay(newParams);
         mockBirdRecognition.VerifySet(s => s.AcceptanceParams = newParams, Times.Once);
      }
   }
}
