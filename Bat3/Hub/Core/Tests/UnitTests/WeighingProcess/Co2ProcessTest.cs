﻿using System;
using System.Linq;
using Communication.Readers;
using Communication.Samples;
using Core.Sensors;
using Core.WeighingProcess.Co2;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CoreTests.UnitTests.WeighingProcess
{
   [TestClass]
   public class Co2ProcessTest
   {
      private Mock<IActualValueOnSensor> mockActualValueOnSensor;
      private MockSensorHistory mockSensorHistory;
      private Mock<ICo2Reader> mockCo2Reader;

      private ICo2Process co2Process;

      [TestInitialize]
      public void Init()
      {
         mockActualValueOnSensor = new Mock<IActualValueOnSensor>();
         mockSensorHistory = new MockSensorHistory();
         mockCo2Reader = new Mock<ICo2Reader>();

         co2Process = new Co2Process(mockActualValueOnSensor.Object, mockSensorHistory);
         co2Process.ProcessFromSensor(mockCo2Reader.Object);
      }

      [TestMethod]
      public void SensorHistoryAndActualCo2OnSensorChanged_WhenNewSample()
      {
         var sample = new Co2Sample {TimeStamp = DateTime.Now, Value = 1200};
         Assert.AreEqual(0, mockSensorHistory.Co2List.Count);
         mockActualValueOnSensor.VerifySet(s => s.Co2 = It.IsAny<long>(), Times.Never);
         mockCo2Reader.Raise(s=>s.NewSample+=null, null, sample);
         Assert.AreEqual(1,mockSensorHistory.Co2List.Count);
         Assert.AreEqual(sample, mockSensorHistory.Co2List.Last());
         mockActualValueOnSensor.VerifySet(s=>s.Co2=sample.Value, Times.Once());
      }

      [TestMethod]
      public void SensorHistoryAndActualCo2OnSensorNotChanged_WhenNewSampleIsNull()
      {
         Assert.AreEqual(0, mockSensorHistory.Co2List.Count);
         mockActualValueOnSensor.VerifySet(s => s.Co2 = It.IsAny<long>(), Times.Never);
         mockCo2Reader.Raise(s => s.NewSample += null, null, null);
         Assert.AreEqual(0, mockSensorHistory.Co2List.Count);
         mockActualValueOnSensor.VerifySet(s => s.Co2 = It.IsAny<long>(), Times.Never);
      }
   

      [TestMethod]
      public void C2SensorReaderChanged_WhenCo2ReaderChanged()
      {
         var newReader = new Mock<ICo2Reader>().Object;
         Assert.AreNotEqual(newReader, co2Process.Co2Sensor.Reader);
         co2Process.ProcessFromSensor(newReader);
         Assert.AreEqual(newReader, co2Process.Co2Sensor.Reader);
      }

   }
}
