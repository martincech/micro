using System.Collections.Generic;
using BatLibrary;
using Communication.Samples;
using Core.Sensors;

namespace CoreTests.UnitTests.WeighingProcess
{
   internal class MockSensorHistory : ISensorHistory
   {
      private readonly List<Co2Sample> co2List;
      private readonly List<HumiditySample> humidityList;
      private readonly List<TemperatureSample> temperatureList;
      private readonly List<KeyValuePair<int, Weight>> weightList;

      internal MockSensorHistory()
      {
         co2List = new List<Co2Sample>();
         humidityList = new List<HumiditySample>();
         temperatureList = new List<TemperatureSample>();
         weightList = new List<KeyValuePair<int, Weight>>();
      }
      #region Implementation of ISensorHistory

      public List<KeyValuePair<int, Weight>> WeightList
      {
         get { return weightList; }
      }

      public List<Co2Sample> Co2List
      {
         get { return co2List; }
      }

      public List<HumiditySample> HumidityList
      {
         get { return humidityList; }
      }

      public List<TemperatureSample> TemperatureList
      {
         get { return temperatureList; }
      }

      #endregion
   }
}