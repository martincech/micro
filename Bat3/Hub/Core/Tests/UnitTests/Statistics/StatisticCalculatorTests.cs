﻿using System;
using System.Collections.Generic;
using System.Linq;
using BatLibrary;
using Core.Statistics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Recognition.Types;

namespace CoreTests.UnitTests.Statistics
{
   [TestClass]
   public class StatisticCalculatorTests
   {
      private const double TOLERANCE = 0.000001;
      private static WeightStatisticsCalculator _weightStatisticsCalculator;
      private static MockWeightStatistics _weightStatistics;
      private const int UNIFORMITY_RANGE = 10;
      [TestInitialize]
      public void Init()
      {
         _weightStatistics = new MockWeightStatistics();
         _weightStatisticsCalculator = new WeightStatisticsCalculator(_weightStatistics, UNIFORMITY_RANGE);      
      }


      [TestMethod]
      public void AddSample_WhenSampleIsNull()
      {
         BirdWeight sample = null;
         var samples = new List<BirdWeight>(TestData.Weights);
         _weightStatisticsCalculator.AddSample(samples[0]);
         _weightStatisticsCalculator.AddSample(samples[1]);
         Assert.AreEqual(2, _weightStatistics.Count);
         _weightStatisticsCalculator.AddSample(sample);
         Assert.AreEqual(2, _weightStatistics.Count);
         CompareWithExpected(new List<BirdWeight>(samples.Take(2)));
      }

      [TestMethod]
      public void AddSample_WhenSamplesIsNull()
      {
         List<BirdWeight> nullSamples = null;
         var samples = new List<BirdWeight>(TestData.Weights);
         _weightStatisticsCalculator.AddSample(samples);
         Assert.AreEqual(samples.Count, _weightStatistics.Count);
         _weightStatisticsCalculator.AddSample(nullSamples);
         Assert.AreEqual(samples.Count, _weightStatistics.Count);
         CompareWithExpected(new List<BirdWeight>(samples));
      }

      [TestMethod]
      public void AddSample_WhenSamplesHasNull()
      {
         List<BirdWeight> nullSamples = null;
         var samples = new List<BirdWeight>(TestData.Weights);
         samples.Insert(1,null);
         _weightStatisticsCalculator.AddSample(samples);
         Assert.AreEqual(samples.Count - 1, _weightStatistics.Count);
         CompareWithExpected(new List<BirdWeight>(samples));
      }

      [TestMethod]
      public void ResetCalculation_WhenNoSample()
      {
         Assert.AreEqual(0,_weightStatistics.Count);
         _weightStatisticsCalculator.ResetCalculation();
         Assert.AreEqual(0, _weightStatistics.Count);
      }

      [TestMethod]
      public void ResetCalculation_WhenHasSample()
      {
         var samples = new List<BirdWeight>(TestData.Weights);
         _weightStatisticsCalculator.AddSample(samples[0]);
         Assert.AreEqual(1, _weightStatistics.Count);
         _weightStatisticsCalculator.ResetCalculation();
         Assert.AreEqual(0, _weightStatistics.Count);
      }

      [TestMethod]
      public void CalculateStatisticsForToday()
      {
         foreach (var sample in TestData.NoSexNoModeWeights)
         {
            _weightStatisticsCalculator.AddSample(sample);
         }
         CompareWithExpected(TestData.NoSexNoModeWeights);
      }

      [TestMethod]
      public void CalculateStatisticsForMoreDays()
      {
         const int days = 5;
         var dayWeights = new List<List<BirdWeight>>();
         var prevAverage = new Weight();
         for (var i = 0; i < days; i++)
         {
            dayWeights.Add(TestData.RandomBirdWeightsForDay(i));
         }
         foreach (var dayWeight in dayWeights)
         {
            foreach (var birdWeight in dayWeight)
            {
               _weightStatisticsCalculator.AddSample(birdWeight);
            }
            CompareWithExpected(dayWeight);
            if (dayWeight != dayWeights[0])
            {
               Assert.AreEqual(_weightStatistics.Average - prevAverage, _weightStatistics.Gain);
            }
            prevAverage = _weightStatistics.Average;
            _weightStatisticsCalculator.ResetCalculation();
         }
      }

      [TestMethod]
      public void CalculateUniformity_SamplesOutOfRange()
      {
         _weightStatisticsCalculator.UniformityRangePercentPercent = 10;
         foreach (var sample in TestData.WeightsWithBigDifferent)
         {
            _weightStatisticsCalculator.AddSample(sample);
         }
         CompareWithExpected(TestData.WeightsWithBigDifferent);
      }

      private static void CompareWithExpected(List<BirdWeight> expectedValues)
      {
         const int decimals = 4;
         expectedValues = expectedValues.Where(s => s != null).ToList();
         Func<BirdWeight, double> selector = bw => bw.AsG;
         var sum = expectedValues.Sum(selector);
         var squareSum = expectedValues.Select(bw => bw.AsG * bw.AsG).Sum();
         var expectedAverage = expectedValues.Average(selector);
         var expectedCount = expectedValues.Count;
         var uniformityMin = expectedAverage*(100 - UNIFORMITY_RANGE)/100.0;
         var uniformityMax = expectedAverage*(100 + UNIFORMITY_RANGE)/100.0;
         var inUniformityRage =
            expectedValues.Count(bw => (bw.AsG >= uniformityMin) && (bw.AsG <= uniformityMax));
         var expectedUniformity = (inUniformityRage / (double)expectedCount) * 100;
         var expectedSigma = Math.Sqrt(1.0/(expectedValues.Count - 1)*(squareSum - (sum*sum/expectedValues.Count)));
         var expectedCv = expectedSigma/expectedAverage*100;
         Assert.IsTrue(Math.Abs(Math.Round(expectedAverage, decimals) - Math.Round(_weightStatistics.Average.AsG, decimals)) <
                       TOLERANCE);
         Assert.AreEqual(expectedCount, _weightStatistics.Count);
         Assert.IsTrue(Math.Abs(Math.Round(expectedCv, decimals) - Math.Round(_weightStatistics.Cv, decimals)) < TOLERANCE);
         Assert.IsTrue(Math.Abs(Math.Round(expectedSigma, decimals) - Math.Round(_weightStatistics.Sigma, decimals)) <
                       TOLERANCE);
         Assert.IsTrue(Math.Abs(expectedUniformity - _weightStatistics.Uniformity) < TOLERANCE);
      }
   }
}
