﻿using System;
using System.Collections.Generic;
using System.Linq;
using BatLibrary;
using Recognition.Types;

namespace CoreTests
{
   public class TestData
   {
      public static List<BirdWeight> RandomBirdWeightsForDay(int day)
      {
         var rnd = new Random();
         var randomSamples = new List<BirdWeight>();
         for (var i = 0; i < 10; i++)
         {
            var sample = new BirdWeight(rnd.Next(i, i + 10))
            {
               TimeStamp = new DateTime(2015, 12, day+1, 8, 0, i)
            };
            
            randomSamples.Add(sample);
         }
         return randomSamples;
      }
 
      public static List<BirdWeight> NoSexNoModeWeights = new List<BirdWeight>
      {
         new BirdWeight(50) {TimeStamp = new DateTime(2015, 12, 12, 8, 0, 0)},
         new BirdWeight(49) {TimeStamp = new DateTime(2015, 12, 12, 8, 0, 1)},
         new BirdWeight(55) {TimeStamp = new DateTime(2015, 12, 12, 8, 0, 2)},
         new BirdWeight(51) {TimeStamp = new DateTime(2015, 12, 12, 8, 0, 3)},
         new BirdWeight(50.5) {TimeStamp = new DateTime(2015, 12, 12, 8, 0, 4)},
         new BirdWeight(49.9) {TimeStamp = new DateTime(2015, 12, 12, 8, 0, 5)},
         new BirdWeight(50.1) {TimeStamp = new DateTime(2015, 12, 12, 8, 0, 6)},
         new BirdWeight(52) {TimeStamp = new DateTime(2015, 12, 12, 8, 0, 7)},
         new BirdWeight(50.1) {TimeStamp = new DateTime(2015, 12, 12, 8, 0, 8)},
         new BirdWeight(50.5) {TimeStamp = new DateTime(2015, 12, 12, 8, 0, 9)}
      };

      public static List<BirdWeight> WeightsWithBigDifferent = new List<BirdWeight>
      {
         new BirdWeight(50) {TimeStamp = new DateTime(2015, 12, 12, 8, 0, 0)},
         new BirdWeight(40) {TimeStamp = new DateTime(2015, 12, 12, 8, 0, 1)},
         new BirdWeight(38) {TimeStamp = new DateTime(2015, 12, 12, 8, 0, 2)},
         new BirdWeight(65) {TimeStamp = new DateTime(2015, 12, 12, 8, 0, 3)},
         new BirdWeight(50.5) {TimeStamp = new DateTime(2015, 12, 12, 8, 0, 4)}
      };

      public static List<BirdWeight> Weights = new List<BirdWeight>
      {
         new BirdWeight(45) {Sex = Sex.Male, Difference = DifferenceMode.Increased, TimeStamp = DateTime.Now},
         new BirdWeight(47.5) {Sex = Sex.Female, Difference = DifferenceMode.Increased, TimeStamp = DateTime.Now.AddDays(1)}
      };

      public static List<double> Temperatures = new List<double> { 24.5, 22.3 };

      public static Curve LinearCurve()
      {
         var curve = new Curve();
         for (var i = 1; i < 10; i++)
         {
            curve.Points.Add(new CurvePoint(i, new Weight(i + 10)));
         }
         return curve;
      }

      public static readonly double[] StableSamplesG = { 65, 35, 54, 46, 53, 47, 52, 48, 51, 50, 50, 52, 51, 51.5, 52, 51.4, 55.5 };
      public static readonly double[] UnstableSamplesG = { 55, 45, 54, 46, 53, 47, 52, 48, 51, 105, 60, 52, 51, 11.5, 52 };

      public static readonly Weight[] StableSamples = StableSamplesG.Select(s=>new Weight(s)).ToArray();
      public static readonly Weight[] UnstableSamples = UnstableSamplesG.Select(s=>new Weight(s)).ToArray();
   }
}