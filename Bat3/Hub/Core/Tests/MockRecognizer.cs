﻿using System;
using Recognition;
using Recognition.RecognitionParts;
using Recognition.Types;

namespace CoreTests
{
   public class MockRecognizer : IBirdRecognition
   {
      public MockRecognizer()
      {
         Calibration = new Calibration();
      }

      #region Implementation of IRecognition

      public void Add(RawWeight sample)
      {
         OnRecognized(new BirdWeight(sample.AsG) { TimeStamp = sample.TimeStamp });
      }

      public event EventHandler<BirdWeight> Recognized;

      protected virtual void OnRecognized(BirdWeight e)
      {
         var handler = Recognized;
         if (handler != null) handler(this, e);
      }

      #endregion

      public Calibration Calibration { get; set; }
      public AcceptanceParams AcceptanceParams { get; set; }

   }
}