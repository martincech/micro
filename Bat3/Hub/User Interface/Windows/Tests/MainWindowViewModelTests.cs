﻿using Desktop.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UI.WindowsTests
{
   [TestClass]
   public class MainWindowViewModelTests
   {
      private AlertViewModel alertViewModel;
      private MainWindowViewModel mainViewModel;

      [TestInitialize]
      public void Init()
      {
         alertViewModel = new AlertViewModel();

         mainViewModel = new MainWindowViewModel(alertViewModel);
      }
      [TestMethod]
      public void ViewModel_WhenPropertyChangsed_Error()
      {
         mainViewModel.IsError = false;
         Assert.IsTrue(string.IsNullOrEmpty(alertViewModel.Text));
         mainViewModel.IsError = true;
         Assert.IsFalse(string.IsNullOrEmpty(alertViewModel.Text));
      }

   }
}
