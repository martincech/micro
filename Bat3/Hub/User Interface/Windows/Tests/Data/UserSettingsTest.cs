﻿using System.IO;
using BatLibrary;
using Core.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Saver.Format;
using Saver.Storage;

namespace UI.WindowsTests.Data
{
   [TestClass]
   public class UserSettingsTest
   {
      private const string TEMP_FILE = "UserSettings.tmp";
      private UserSettings userSettings;
      private BasePersistentStorage<UserSettings> storage;

      [TestInitialize]
      public void Init()
      {
         storage = new BasePersistentStorage<UserSettings>(new XmlSaver<UserSettings>(TEMP_FILE));
         userSettings = new UserSettings();
      }

      [TestCleanup]
      public void CleanUp()
      {
         if (File.Exists(TEMP_FILE))
         {
            File.Delete(TEMP_FILE);
         }
      }

      [TestMethod]
      public void Save_UserSettings_WhenNotExist()
      {
         const Weight.WeightUnits expectedUnit = Weight.WeightUnits.LB;
         const Temperature expectedTemepratureUnit = Temperature.Fahrenheit;

         userSettings.WeightUnit = expectedUnit;
         userSettings.TemperatureUnit = expectedTemepratureUnit;

         Assert.IsFalse(File.Exists(TEMP_FILE));
         userSettings.Save(storage);

         userSettings.WeightUnit = Weight.WeightUnits.G;
         userSettings.TemperatureUnit = Temperature.Celsius;
         Assert.AreEqual(Weight.WeightUnits.G, userSettings.WeightUnit);
         Assert.AreEqual(Temperature.Celsius, userSettings.TemperatureUnit);

         Assert.IsTrue(File.Exists(TEMP_FILE));
         userSettings.Load(storage);

         Assert.AreEqual(expectedUnit, userSettings.WeightUnit);
         Assert.AreEqual(expectedUnit, userSettings.WeightUnit);

         userSettings.Load(storage);
      }

      [TestMethod]
      public void Load_UserSettings_WhenNotExist()
      {
         Assert.IsFalse(File.Exists(TEMP_FILE));
         userSettings.Load(storage);
         Assert.AreEqual(Weight.WeightUnits.KG, userSettings.WeightUnit);
         Assert.AreEqual(Temperature.Celsius, userSettings.TemperatureUnit);
      }
   }
}
