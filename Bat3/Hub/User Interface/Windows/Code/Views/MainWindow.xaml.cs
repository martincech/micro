﻿using System.Windows;
using Desktop.ViewModels;

namespace UI.Windows.Views
{
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow
   {
      public MainWindow()
      {
         InitializeComponent();
      }

      private void Button_Click(object sender, RoutedEventArgs e)
      {
         var mainWindowViewModel = DataContext as MainWindowViewModel;
         if (mainWindowViewModel != null) mainWindowViewModel.IsError = true;
      }
   }

   
}
