﻿using System.Windows;
using System.Windows.Controls;

namespace UI.Windows.Views.Calibration
{
   /// <summary>
   /// Interaction logic for CalibratingView.xaml
   /// </summary>
   public partial class CalibratingView : UserControl
   {
      public CalibratingView()
      {
         InitializeComponent();
      }

      public void ButtonBase_OnClick(object sender, RoutedEventArgs e)
      {
         Window parentWindow = Window.GetWindow(this);
         var button = (sender as Button);

         if (button == null || button.Command == null)
         {
            parentWindow.Close();
            return;
         }

         if (button.Command.CanExecute(null))
         {
            button.Command.Execute(null);
         }
         parentWindow.Close();
      }
   }
}
