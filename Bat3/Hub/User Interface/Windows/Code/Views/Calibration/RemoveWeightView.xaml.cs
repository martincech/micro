﻿using System.Windows;
using System.Windows.Controls;

namespace UI.Windows.Views.Calibration
{
   /// <summary>
   /// Interaction logic for RemoveWeightView.xaml
   /// </summary>
   public partial class RemoveWeightView : UserControl
   {
      public RemoveWeightView()
      {
         InitializeComponent();
      }

      public void ButtonBase_OnClick(object sender, RoutedEventArgs e)
      {
         Window parentWindow = Window.GetWindow(this);
         var button = (sender as Button);

         if (button == null || button.Command == null)
         {
            parentWindow.Close();
            return;
         }

         if (button.Command.CanExecute(null))
         {
            button.Command.Execute(null);
         }
         parentWindow.Close();
      }
   }
}
