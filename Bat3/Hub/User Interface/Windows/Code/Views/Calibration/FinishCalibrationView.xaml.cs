﻿using System.Windows;
using System.Windows.Controls;

namespace UI.Windows.Views.Calibration
{
   /// <summary>
   /// Interaction logic for FinishCalibrationView.xaml
   /// </summary>
   public partial class FinishCalibrationView : UserControl
   {
      public FinishCalibrationView()
      {
         InitializeComponent();
      }

      private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
      {
         var parentWindow = Window.GetWindow(this);
         parentWindow.Close();
      }
   }
}
