using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using BatLibrary;
using Core;
using Core.Calibration;
using Core.Sensors;
using Desktop;
using Desktop.ViewModels;
using Desktop.Wpf.Applications;
using Desktop.Wpf.Presentation;
using UI.Windows.Views;

namespace UI.Windows
{
   public class FrontendCommands
   {
      private readonly ICalibrationProcessMediator calibrationMediator;
      private readonly ICommands backend;

      public ICommand ShowAlertCommand
      {
         get { return showAlertCommand; }
      }

      public ICommand RunCalibrationCommand
      {
         get { return runCalibrationCommand; }
      }

      public ICommand RunRandomProcessing
      {
         get { return runRandomProcessing; }
      }

      public ICommand RunRealProcessing
      {
         get { return runRealProcessing; }
      }

      public ICommand StopRealProcessing
      {
         get { return stopRealProcessing; }
      }

      public ICommand RunNextDayCommand
      {
         get { return runNextDayCommand; }
      }

      public RelayCommand<SensorType> ShowHistoryCommand
      {
         get { return showHistoryCommand; }
      }

      #region Private members

      private RelayCommand<IAlert> showAlertCommand;
      private RelayCommand<Weight.WeightUnits> runCalibrationCommand;
      private RelayCommand<SensorType> showHistoryCommand;

      private RelayCommand runRandomProcessing;
      private RelayCommand runRealProcessing;
      private RelayCommand stopRealProcessing;
      private RelayCommand runNextDayCommand;
      private List<Command> allCommands;

      private AlertView alertWindow;
      private CalibrationView calibrationWindow;
      private ICalibrationUnit calibrationUnit;
      private readonly SensorHistoryViewModel sensorHistory;

      #endregion

      public FrontendCommands(
         ICalibrationProcessMediator calibrationMediator,
         ICalibrationUnit calibrationUnit,
         SensorHistoryViewModel sensorHistory,
         ICommands backend)
      {
         this.calibrationMediator = calibrationMediator;
         this.calibrationUnit = calibrationUnit;
         this.sensorHistory = sensorHistory;
         this.backend = backend;
         backend.CanRunChanged += BackendOnCanRunChanged;
         backend.ConnectionLost += BackendOnConnectionLost;
         CreateCommands();
      }

      private void BackendOnConnectionLost(object sender, EventArgs e)
      {
         IAlert alert = new AlertViewModel();
         alert.Text = "Lost connection with sensor pack.";
         alert.Title = "Connection lost";

         alertWindow = null;
         RaiseCommandCanExecuteChanged();

         if (showAlertCommand.CanExecute(alert))
         {
            showAlertCommand.Execute(alert);
         }
      }

      private void CreateCommands()
      {
         allCommands = new List<Command>();
         CreateShowAlertCommand();
         CreateShowCalibrationCommand();
         CreateRunRealProcessingCommand();
         CreateStopRealProcessingCommand();
         CreateRunRandomProcessingCommand();
         CreateRunNextDayCommand();
         CreateShowHistoryCommand();

         allCommands.Add(showAlertCommand);
         allCommands.Add(runCalibrationCommand);
         allCommands.Add(runRandomProcessing);
         allCommands.Add(runRealProcessing);
         allCommands.Add(runNextDayCommand);
         allCommands.Add(showHistoryCommand);
         //Debug.Assert(allCommands.Count == GetType().GetFields().Count(fi => fi!=null && fi.GetType().IsAssignableFrom(typeof(ICommand))),
         //   "Some command not in allCommands list");
      }

      private void CreateRunNextDayCommand()
      {
         runNextDayCommand = new RelayCommand(
            () =>
            {
               backend.RunNextDay();
               RaiseCommandCanExecuteChanged();
            },
            () => backend.CanRunNextDay()); ;
      }

      private void CreateRunRandomProcessingCommand()
      {
         runRandomProcessing = new RelayCommand(
            () =>
            {
               backend.RunRandomProcessing();
               RaiseCommandCanExecuteChanged();
            },
            () => backend.CanRunRandomProcessing());
      }

      private void CreateRunRealProcessingCommand()
      {
         runRealProcessing = new RelayCommand(
            () =>
            {
               backend.RunRealProcessing();
               RaiseCommandCanExecuteChanged();
            },
            () => backend.CanRunRealProcessing());
      }

      private void CreateStopRealProcessingCommand()
      {
         stopRealProcessing = new RelayCommand(
            () =>
            {
               backend.StopRealProcessing();
               RaiseCommandCanExecuteChanged();
            },
            () => backend.CanStopRealProcessing());
      }

      private void CreateShowCalibrationCommand()
      {
         runCalibrationCommand = new RelayCommand<Weight.WeightUnits>(
            unit =>
            {
               calibrationMediator.CurrentProcessState = CalibrationProcessState.DefaultInsertCalibrationWeightRequest;
               calibrationUnit.WeightUnit = unit;
               calibrationWindow = new CalibrationView
               {
                  DataContext = calibrationMediator,
                  Owner = Application.Current.MainWindow
               };
               backend.RunCalibrationProcessing();
               calibrationWindow.ShowDialog();
               calibrationWindow.Closed += CalibrationWindowOnClosed;
               RaiseCommandCanExecuteChanged();
            }, unit =>
               backend.CanRunCalibrationProcessing()
            );
      }

      private void CreateShowAlertCommand()
      {
         showAlertCommand = new RelayCommand<IAlert>(
            a =>
            {
               DispatcherHelper.RunAsync(() =>
               {
                  alertWindow = new AlertView {DataContext = a, Owner = Application.Current.MainWindow};

                  alertWindow.ShowDialog();
                  //alertWindow.Closing += AlertWindowOnClosed; Event do not fire after window is closed
                  RaiseCommandCanExecuteChanged();
               });
              
            }, a => a != null && alertWindow == null);
      }

      private void CreateShowHistoryCommand()
      {
         showHistoryCommand = new RelayCommand<SensorType>(
           type =>
           {
              DispatcherHelper.RunAsync(() =>
              {
                 sensorHistory.ActiveSensor = type;
                 SensorHistoryView view = new SensorHistoryView()
                 {
                    DataContext = sensorHistory,
                    Owner = Application.Current.MainWindow
                 };
                 view.ShowDialog();
              });

           }, type => true);
      }

      private void CalibrationWindowOnClosed(object sender, EventArgs eventArgs)
      {
         calibrationWindow = null;
         RaiseCommandCanExecuteChanged();
      }

      private void RaiseCommandCanExecuteChanged()
      {
         DispatcherHelper.RunAsync(() =>
         {
            foreach (var command in allCommands)
            {
               command.RaiseCanExecuteChanged();
            }
         });
      }

      private void BackendOnCanRunChanged(object sender, EventArgs eventArgs)
      {
         RaiseCommandCanExecuteChanged();
      }
   }
}