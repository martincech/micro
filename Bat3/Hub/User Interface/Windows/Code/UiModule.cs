﻿using Core.Calibration;
using Core.Sensors;
using Core.Statistics;
using Core.WeighingProcess.Weight;
using Desktop.ViewModels;
using Ninject.Modules;

namespace UI.Windows
{
   public class UiModule : NinjectModule
   {
      #region Overrides of NinjectModule

      /// <summary>
      /// Loads the module into the kernel.
      /// </summary>
      public override void Load()
      {
         // binding of view models
         Bind<IAlert, AlertViewModel>().To<AlertViewModel>();
         Bind<IActualValueOnSensor, IWeightStatistics, IWeightForDay, MainWindowViewModel>()
            .To<MainWindowViewModel>().InSingletonScope();
         Bind<ICalibrationProcessMediator, ICalibrationUnit, CalibrationViewModel>()
            .To<CalibrationViewModel>().InSingletonScope();
         Bind<ISensorHistory, SensorHistoryViewModel>()
            .To<SensorHistoryViewModel>().InSingletonScope();

         Bind<FrontendCommands>().ToSelf().InSingletonScope();
      }

      #endregion
   }
}
