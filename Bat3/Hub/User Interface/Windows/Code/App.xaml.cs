﻿using System;
using System.Diagnostics;
using System.Windows;
using Communication.SensorManagers;
using Core;
using Desktop.ViewModels;
using Desktop.Wpf.Presentation;
using Ninject;
using UI.Windows.Views;

namespace UI.Windows
{
   public partial class App
   {
      public App()
      {
         InitializeComponent();
      }

      #region Helpers

      private static void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
      {
         var ex = (Exception)e.ExceptionObject;
         MessageBox.Show(ex.Message + ex.StackTrace,
            "Error", MessageBoxButton.OK, MessageBoxImage.Stop);
      }
    
      #endregion

      #region Startup code

      [STAThread]
      [DebuggerNonUserCode]
      public static void Main()
      {
         DispatcherHelper.Initialize();
         var iocKernel = new StandardKernel();

         InitIoc(iocKernel);
         // init frontend
         var application = new App { MainWindow = new MainWindow { DataContext = GetMainWindowViewModel(iocKernel) } };
         application.Run();
      }

      internal static void InitIoc(StandardKernel iocKernel)
      {
         iocKernel.Bind<ISensorsManager>()
            .To<EloConnectedSensorsManager>()
            .InSingletonScope();

         iocKernel.Load(new UiModule());
         iocKernel.Load(new CoreModule());
      }

      internal static MainWindowViewModel GetMainWindowViewModel(StandardKernel iocKernel)
      {
         var frontedCommands = iocKernel.Get<FrontendCommands>();
         // main view model
         var mainVm = iocKernel.Get<MainWindowViewModel>();
         mainVm.RunRandomCommand = frontedCommands.RunRandomProcessing;
         mainVm.RunRealCommand = frontedCommands.RunRealProcessing;
         mainVm.RunCalibrationCommand = frontedCommands.RunCalibrationCommand;
         mainVm.ShowAllertCommand = frontedCommands.ShowAlertCommand;
         mainVm.NextDayCommand = frontedCommands.RunNextDayCommand;
         mainVm.ShowHistoryCommand = frontedCommands.ShowHistoryCommand;
         return mainVm;
      }

      protected override void OnStartup(StartupEventArgs e)
      {
         base.OnStartup(e);
         AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
         MainWindow.Show();
      }

      #endregion
   }
}
