﻿using System;
using System.Windows.Input;
using BatLibrary;
using Communication.Samples;
using Core.Sensors;
using Core.Statistics;
using Core.WeighingProcess.Weight;
using Utilities.Observable;

namespace Desktop.ViewModels
{
   public class MainWindowViewModel : ObservableObject,
      IActualValueOnSensor,
      IWeightStatistics,
      IWeightForDay
   {
   
      private Weight weight;
      private double temperature;
      private Temperature temperatureUnit;
      private long co2;
      private byte humidity;
      private int count;
      private Weight average;
      private double uniformity;
      private double cv;
      private double sigma;
      private Weight gain;
      private Weight.WeightUnits weightUnit;
      private IAlert alert;
      private bool isError;
      private bool sensorsConnected;
      private bool terminalConnected;
      private Weight expectedWeight;
      private int weighingDay;

      private Weight minExpectedWeight;
      private Weight maxExpectedWeight;

      public MainWindowViewModel(IAlert alert)
      {
        
         Alert = alert;
         weightUnit = Weight.WeightUnits.KG;
      }

      #region Implementation of IActualTemperatureOnSensor

      public double Temperature
      {
         get
         {
            if (TemperatureUnit == BatLibrary.Temperature.Fahrenheit)
            {
               return temperature * 1.8 + 32;
            }
            return temperature;
         }
         set { SetProperty(ref temperature, value); }
      }

      #endregion

      #region Implementation of IActualWeightOnSensor

      public Weight Weight
      {
         get { return weight; }
         set { SetProperty(ref weight, value); }
      }

      #endregion

      #region Implementation of IWeightStatistics

      /// <summary>
      /// Number of birds weighted today
      /// </summary>
      public int Count
      {
         get { return count; }
         set { SetProperty(ref count, value); }
      }

      /// <summary>
      /// Average weight of the birds
      /// </summary>
      public Weight Average
      {
         get { return average; }
         set { SetProperty(ref average, value); }
      }

      /// <summary>
      /// Uniformity of the birds
      /// </summary>
      public double Uniformity
      {
         get { return uniformity; }
         set { SetProperty(ref uniformity, value); }
      }

      /// <summary>
      /// Coeficient of variation of the birds
      /// </summary>
      public double Cv
      {
         get { return cv; }
         set { SetProperty(ref cv, value); }
      }

      /// <summary>
      /// Standard deviation
      /// </summary>
      public double Sigma
      {
         get { return sigma; }
         set { SetProperty(ref sigma, value); }
      }

      public Temperature TemperatureUnit
      {
         get { return temperatureUnit; }
         set
         {
            SetProperty(ref temperatureUnit, value);
            RaisePropertyChanged("Temperature");
         }
      }

      /// <summary>
      /// Gain from yesterday
      /// </summary>
      public Weight Gain
      {
         get { return gain; }
         set { SetProperty(ref gain, value); }
      }

      #endregion

      public Weight.WeightUnits WeightUnit
      {
         get { return weightUnit; }
         set { SetProperty(ref weightUnit, value); }
      }

      public ICommand StartCalibrationCommand { get; set; }

      public long Co2
      {
         get { return co2; }
         set { SetProperty(ref co2, value); }
      }

      public byte Humidity
      {
         get { return humidity; }
         set { SetProperty(ref humidity, value); }
      }


      public bool SensorsConnected
      {
         get { return sensorsConnected; }
         set { SetProperty(ref sensorsConnected, value); }
      }

      public bool TerminalConnected
      {
         get { return terminalConnected; }
         set { SetProperty(ref terminalConnected, value); }
      }

      public bool IsError
      {
         get { return isError; }
         set
         {
            SetProperty(ref isError, value);
            if (value)
            {
               ExecuteAllert();
            }
         }
      }

      public ICommand RunRandomCommand { get; set; }
      public ICommand RunRealCommand { get; set; }
      public ICommand ShowAllertCommand { get; set; }
      public ICommand RunCalibrationCommand { get; set; }
      public ICommand NextDayCommand { get; set; }
      public ICommand ShowHistoryCommand { get; set; }

      public IAlert Alert
      {
         get { return alert; }
         private set { SetProperty(ref alert, value); }
      }

      private void ExecuteAllert()
      {
         Alert.Text = "ERROR" + DateTime.Now;
         Alert.Title = "Important error";
         if (ShowAllertCommand != null
            && ShowAllertCommand.CanExecute(Alert))
         {
            ShowAllertCommand.Execute(Alert);
         }

      }

      #region Implementation of IWeightForDay

      public Weight ExpectedWeight
      {
         get { return expectedWeight; }
         set { SetProperty(ref expectedWeight, value); }
      }

      public Weight MinExpectedWeight
      {
         get { return minExpectedWeight; }
         set { SetProperty(ref minExpectedWeight, value); }
      }

      public Weight MaxExpectedWeight
      {
         get { return maxExpectedWeight; }
         set { SetProperty(ref maxExpectedWeight, value); }
      }

      public int WeighingDay
      {
         get { return weighingDay; }
         set { SetProperty(ref weighingDay, value); }
      }

      #endregion
   }
}
