﻿using Desktop.Properties;
using Utilities.Observable;

namespace Desktop.ViewModels
{
   public class AlertViewModel : ObservableObject, IAlert
   {
      #region Private fields

      private string text;
      private string title;

      #endregion

      #region Public interfaces

      public AlertViewModel()
      {
         Title = Resources.error;
      }


      public string Text
      {
         get { return text; }
         set
         {
            SetProperty(ref text, value);
         }
      }
      public string Title
      {
         get { return title; }
         set { SetProperty(ref title, value); }
      }

      #endregion
   }
}
