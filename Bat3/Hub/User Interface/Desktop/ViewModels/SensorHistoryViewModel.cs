﻿using System;
using System.Collections.Generic;
using System.Linq;
using BatLibrary;
using Communication.Samples;
using Core.Sensors;
using Utilities.Observable;

namespace Desktop.ViewModels
{
   public class SensorHistoryViewModel : ObservableObject, ISensorHistory
   {
      private List<KeyValuePair<int, Weight>> weightList;
      private List<Co2Sample> co2List;
      private List<HumiditySample> humidityList;
      private List<TemperatureSample> temperatureList;
      private SensorType activeSensor;

      public SensorHistoryViewModel()
      {
         WeightList = new List<KeyValuePair<int, Weight>>();
         Co2List = new List<Co2Sample>();
         HumidityList = new List<HumiditySample>();
         TemperatureList = new List<TemperatureSample>();
      }

      public SensorType ActiveSensor
      {
         get { return activeSensor; }
         set
         {
            SetProperty(ref activeSensor, value);
            RaisePropertyChanged("SensorValues");
         }
      }

      public IEnumerable<SensorType> SensorTypeValues
      {
         get
         {
            return Enum.GetValues(typeof(SensorType))
                .Cast<SensorType>();
         }
      }

      public List<KeyValuePair<int, Weight>> WeightList
      {
         get { return weightList; }
         private set { SetProperty(ref weightList, value); }
      }

      public List<Co2Sample> Co2List
      {
         get { return co2List; }
         private set { SetProperty(ref co2List, value); }
      }

      public List<dynamic> SensorValues
      {
         get
         {
            List<dynamic> data = null;
            switch (ActiveSensor)
            {
               case SensorType.Weight:
                  data = new List<dynamic>(WeightList.Select(s=> new {Value = s.Value.AsG, Day = s.Key, s.Value.TimeStamp}));
                  break;
               case SensorType.Temperature:
                  data = new List<dynamic>(TemperatureList);
                  break;
               case SensorType.Humidity:
                  data = new List<dynamic>(HumidityList);
                  break;
               case SensorType.CarbonDioxide:
                  data = new List<dynamic>(Co2List);
                  break;
            }
            if (data != null)
            {
               data.Reverse();
            }
            return new List<dynamic>(data);
         }
      } 


      public List<HumiditySample> HumidityList
      {
         get { return humidityList; }
         private set { SetProperty(ref humidityList, value); }
      }

      public List<TemperatureSample> TemperatureList
      {
         get { return temperatureList; }
         private set { SetProperty(ref temperatureList, value); }
      }
   }
}
