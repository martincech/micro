﻿using System;
using System.Windows;
using System.Windows.Input;
using BatLibrary;
using Core.Calibration;
using Desktop.Wpf.Applications;
using Utilities.Observable;

namespace Desktop.ViewModels
{
   public class CalibrationViewModel : ObservableObject, ICalibrationProcessMediator, ICalibrationUnit
   {
      private readonly double MIN_CALIBRATION_WEIGHT = 50;
      private readonly double MAX_CALIBRATION_WEIGHT = 2000;
      private readonly double DEFAULT_CALIBRATION_WEIGHT = 100;
      private const Weight.WeightUnits DEFAULT_UNITS = Weight.WeightUnits.G;

      private RelayCommand startCalibrationCommand;
      private RelayCommand cancelCalibrationCommand;
      private Weight calibrationWeight;
      private double minCalibrationWeight;
      private double maxCalibrationWeight;
      private CalibrationProcessState currentProcessState;
      private Weight.WeightUnits weightUnit;


      public CalibrationViewModel()
      {
         weightUnit = DEFAULT_UNITS;
         MinCalibrationWeight = MIN_CALIBRATION_WEIGHT;
         MaxCalibrationWeight = MAX_CALIBRATION_WEIGHT;
         CalibrationWeight = new Weight(DEFAULT_CALIBRATION_WEIGHT);
      }


      public ICommand StartCalibrationCommand
      {
         get
         {
            return startCalibrationCommand ?? (startCalibrationCommand =
               new RelayCommand(
                  () =>
                  {
                     OnStartCalibration(new Weight(CalibrationWeight.AsG, Weight.WeightUnits.G));
                     UpdateCommands();
                  },
                  () => CurrentProcessState == CalibrationProcessState.DefaultInsertCalibrationWeightRequest));
         }
      }

      public ICommand CancelCalibrationCommand
      {
         get
         {
            return cancelCalibrationCommand ?? (cancelCalibrationCommand =
               new RelayCommand(
                  () =>
                  {
                     OnCancelCalibration();
                     UpdateCommands();
                  },
                  () =>
                     CurrentProcessState != CalibrationProcessState.DefaultInsertCalibrationWeightRequest &&
                     CurrentProcessState != CalibrationProcessState.Canceled));
         }
      }

      public Weight CalibrationWeight
      {
         get { return calibrationWeight; }
         set
         {
            SetProperty(ref calibrationWeight, value);
         }
      }

      public double MinCalibrationWeight
      {
         get { return minCalibrationWeight; }
         set { SetProperty(ref minCalibrationWeight, value); }
      }

      public double MaxCalibrationWeight
      {
         get { return maxCalibrationWeight; }
         set { SetProperty(ref maxCalibrationWeight, value); }
      }

      public Weight.WeightUnits WeightUnit
      {
         get { return weightUnit; }
         set { SetProperty(ref weightUnit, value); }
      }


      private void UpdateCommands()
      {
         Application.Current.Dispatcher.BeginInvoke(new Action(() =>
         {
            ((RelayCommand) StartCalibrationCommand).RaiseCanExecuteChanged();
            ((RelayCommand) CancelCalibrationCommand).RaiseCanExecuteChanged();
         }));
      }

      #region Implementation of ICalibrationProcessMediator

      public CalibrationProcessState CurrentProcessState
      {
         get { return currentProcessState; }
         set { SetProperty(ref currentProcessState, value); }
      }

      public event EventHandler<Weight> StartCalibration;

      protected virtual void OnStartCalibration(Weight e)
      {
         var handler = StartCalibration;
         if (handler != null) handler(this, e);
      }

      public event EventHandler CancelCalibration;

      protected virtual void OnCancelCalibration()
      {
         var handler = CancelCalibration;
         if (handler != null) handler(this, EventArgs.Empty);
      }

      #endregion
   }
}
