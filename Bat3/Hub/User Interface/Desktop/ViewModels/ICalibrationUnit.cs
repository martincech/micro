﻿using BatLibrary;

namespace Desktop.ViewModels
{
   public interface ICalibrationUnit
   {
      Weight.WeightUnits WeightUnit { get; set; }
   }
}
