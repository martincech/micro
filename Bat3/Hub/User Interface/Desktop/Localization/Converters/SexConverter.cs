using BatLibrary;
using Desktop.Properties;

namespace Desktop.Localization.Converters
{
   /// <summary>
   /// Define the type converter for the <see cref="Sex"/> Enum
   /// </summary>
   public class SexConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public SexConverter()
         : base(typeof(Sex), Resources.ResourceManager)
      {
      }
   }
}