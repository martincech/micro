﻿using System;
using System.Globalization;
using System.Windows.Data;
using BatLibrary;

namespace Desktop.Localization.Converters
{
   public class UnitToBoolConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         var val = (Weight.WeightUnits)value;
         return parameter != null && parameter.ToString().ToLower().Equals(val.ToString().ToLower());
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         var val = (bool) value;
         Weight.WeightUnits weightUnit;
         if (val && parameter != null && Enum.TryParse(parameter.ToString(), true, out weightUnit))
         {
            return weightUnit;
         }
         return null;
      }
   }
}
