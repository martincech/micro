﻿using System;
using System.Resources;

namespace Desktop.Localization.Converters
{
   /// <summary>
   /// 
   /// </summary>
   public class ReverseResourceEnumConverter : ResourceEnumConverter
   {
      /// <summary>
      /// 
      /// </summary>
      /// <param name="type"></param>
      /// <param name="resourceManager"></param>
      public ReverseResourceEnumConverter(Type type, ResourceManager resourceManager)
         : base(type, resourceManager)
      {
      }
   }
}
