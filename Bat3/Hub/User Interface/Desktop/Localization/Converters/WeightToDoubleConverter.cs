﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using BatLibrary;

namespace Desktop.Localization.Converters
{
   /// <summary>
   /// In WPF View must be parametr Shared set to false (x:Shared="False") in Window/UserControl resources
   /// to prevent sharing lastUnit between controls
   /// </summary>
   public class WeightToDoubleConverter : IMultiValueConverter
   {

      private Weight.WeightUnits lastUnit = Weight.WeightUnits.G;

      #region Implementation of IMultiValueConverter

      /// <summary>
      /// Converts source values to a value for the binding target. The data binding engine calls this method when it propagates the values from source bindings to the binding target.
      /// </summary>
      /// <returns>
      /// A converted value.If the method returns null, the valid null value is used.A return value of <see cref="T:System.Windows.DependencyProperty"/>.<see cref="F:System.Windows.DependencyProperty.UnsetValue"/> indicates that the converter did not produce a value, and that the binding will use the <see cref="P:System.Windows.Data.BindingBase.FallbackValue"/> if it is available, or else will use the default value.A return value of <see cref="T:System.Windows.Data.Binding"/>.<see cref="F:System.Windows.Data.Binding.DoNothing"/> indicates that the binding does not transfer the value or use the <see cref="P:System.Windows.Data.BindingBase.FallbackValue"/> or the default value.
      /// </returns>
      /// <param name="values">The array of values that the source bindings in the <see cref="T:System.Windows.Data.MultiBinding"/> produces. The value <see cref="F:System.Windows.DependencyProperty.UnsetValue"/> indicates that the source binding has no value to provide for conversion.</param><param name="targetType">The type of the binding target property.</param><param name="parameter">The converter parameter to use.</param><param name="culture">The culture to use in the converter.</param>
      public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
      {
         if (values.Count() < 2)
         {
            return null;
         }

         double ret = 0;
         if (values[0] is Weight && values[1] is Weight.WeightUnits)
         {
            var weigt = (Weight) values[0];
            var units = (Weight.WeightUnits) values[1];
            lastUnit = units;
            switch (units)
            {
               case Weight.WeightUnits.KG:
                  ret = weigt.AsKg;
                  break;
               case Weight.WeightUnits.G:
                  ret = weigt.AsG;
                  break;
               case Weight.WeightUnits.LB:
                  ret = weigt.AsLb;
                  break;
               default:
                  throw new ArgumentOutOfRangeException();
            }
         }


         if (targetType == typeof (String))
         {
            return string.Format("{0:#0.000}",ret);
         }
         return ret;
      }

      /// <summary>
      /// Converts a binding target value to the source binding values.
      /// </summary>
      /// <returns>
      /// An array of values that have been converted from the target value back to the source values.
      /// </returns>
      /// <param name="value">The value that the binding target produces.</param><param name="targetTypes">The array of types to convert to. The array length indicates the number and types of values that are suggested for the method to return.</param><param name="parameter">The converter parameter to use.</param><param name="culture">The culture to use in the converter.</param>
      public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
      {
         if (targetTypes.Count() == 2)
         {
            var returnValue = new Weight((double) value, lastUnit);
            return new object[] { returnValue, lastUnit };
         }

         return null;
      }

      #endregion
   }
}
