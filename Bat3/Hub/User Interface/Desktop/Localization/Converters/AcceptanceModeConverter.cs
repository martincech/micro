using BatLibrary;
using Desktop.Properties;
using Recognition.RecognitionParts;

namespace Desktop.Localization.Converters
{
   /// <summary>
   /// Define the type converter for the <see cref="AcceptanceSexMode"/> Enum
   /// </summary>
   public class AcceptanceModeConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public AcceptanceModeConverter()
         : base(typeof(Sex), Resources.ResourceManager)
      {
      }
   }
}