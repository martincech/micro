﻿using System;
using System.Globalization;
using System.Windows.Data;
using BatLibrary;

namespace Desktop.Localization.Converters
{
   public class TemperatureUnitToBoolConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         var val = (Temperature)value;
         return parameter != null && parameter.ToString().ToLower().Equals(val.ToString().ToLower());
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         var val = (bool)value;
         Temperature unit;
         if (val && parameter != null && Enum.TryParse(parameter.ToString(), true, out unit))
         {
            return unit;
         }
         return null;
      }
   }
}
