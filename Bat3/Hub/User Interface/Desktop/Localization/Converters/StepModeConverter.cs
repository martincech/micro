﻿using Desktop.Properties;
using Recognition.RecognitionParts;

namespace Desktop.Localization.Converters
{
   /// <summary>
   /// Define the type converter for the <see cref="AcceptanceStepMode"/> Enum
   /// </summary>
   public class StepModeConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public StepModeConverter()
         : base(typeof(AcceptanceStepMode), Resources.ResourceManager)
      {
      }
   }
}
