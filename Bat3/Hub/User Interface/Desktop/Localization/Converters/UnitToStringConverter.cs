﻿using System;
using System.Globalization;
using System.Windows.Data;
using Desktop.Properties;

namespace Desktop.Localization.Converters
{
   public class UnitToStringConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         var resManager = Resources.ResourceManager;
         var res = resManager.GetString(value.ToString());
         if (String.IsNullOrEmpty(res))
         {
            return value.ToString();
         }
         return res;
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
