using System;
using Android.App;
using Android.Runtime;

namespace UI.Android.Activities
{
   [Application(Theme = "@android:style/Theme.Holo.Light.NoActionBar")]
   public class App : Application
   {
      public App(IntPtr javaReference, JniHandleOwnership transfer) :
         base(javaReference, transfer)
      {
      }

      public override void OnCreate()
      {
         base.OnCreate();

         InitIoc();
         LoadSettings();
      }

      #region Private helpers

      private void InitIoc()
      {
         //var IocKernel = new StandardKernel();
         //IocKernel.Bind<IPersistentStorage<UserSettings>>().To<BasePersistentStorage<UserSettings>>().InSingletonScope();
      }

      private void LoadSettings()
      {
         // Load settings
      }

      #endregion
   }
}