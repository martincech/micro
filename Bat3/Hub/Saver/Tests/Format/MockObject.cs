using System;

namespace SaverTests.Format
{
   [Serializable]
   public class MockObject
   {
      public int IntValue { get; set; }
      public MockSubObject SubObject { get; set; }
   }
}