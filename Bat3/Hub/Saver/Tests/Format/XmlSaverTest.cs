﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Saver.Format;

namespace SaverTests.Format
{
   [TestClass]
   public class XmlSaverTest
   {
      private const string TEMP_FILE = "TestFile.xml";
      private XmlSaver<MockObject> saver;

      private readonly List<MockObject> testList;

      public XmlSaverTest()
      {
         testList = new List<MockObject>()
         {
            new MockObject(){IntValue = 0, SubObject = new MockSubObject(){StringValue = "a"}},
            new MockObject(){IntValue = 1, SubObject = new MockSubObject(){StringValue = "b"}},
            new MockObject(){IntValue = 2, SubObject = new MockSubObject(){StringValue = "c"}},
            new MockObject(){IntValue = 3, SubObject = new MockSubObject(){StringValue = "d"}},
            new MockObject(){IntValue = 4, SubObject = new MockSubObject(){StringValue = "e"}},
            new MockObject(){IntValue = 5, SubObject = new MockSubObject(){StringValue = "f"}},
            new MockObject(){IntValue = 6, SubObject = new MockSubObject(){StringValue = "g"}},
            new MockObject(){IntValue = 7, SubObject = new MockSubObject(){StringValue = "h"}},
            new MockObject(){IntValue = 8, SubObject = new MockSubObject(){StringValue = "i"}},
            new MockObject(){IntValue = 9, SubObject = new MockSubObject(){StringValue = "j"}}
         };
      }

      [TestInitialize]
      public void Init()
      {
         if (File.Exists(TEMP_FILE))
         {
            File.Delete(TEMP_FILE);
         }
         saver = new XmlSaver<MockObject>(TEMP_FILE);
      }

      [TestCleanup]
      public void Clean()
      {
         File.Delete(TEMP_FILE);
      }

      [TestMethod]
      public void AppendAll_WhenFileNotExist()
      {
         saver.Append(testList);
         var result = saver.Read();
         AreEqual(testList, result);
      }

      [TestMethod]
      public void AppendAll_WhenFileExist()
      {
         var newItems = new List<MockObject>()
         {
            new MockObject() {IntValue = 100, SubObject = new MockSubObject() {StringValue = "y"}},
            new MockObject() {IntValue = 101, SubObject = new MockSubObject() {StringValue = "z"}}
         };

         AppendAll_WhenFileNotExist();
         saver.Append(newItems);
         var result = saver.Read();

         AreEqual(newItems, result.Skip(testList.Count));
      }

      [TestMethod]
      public void Append_PerformanceTest()
      {
         var samplesCount = 500;

         CancellationTokenSource token = new CancellationTokenSource();

         // Create new stopwatch
         Stopwatch stopwatch = new Stopwatch();
         //Write n samples one by one and measure time
         stopwatch.Start();
         for (int i = 0; i < samplesCount; i++)
         {
            saver.Append(new List<MockObject>() { new MockObject() { IntValue = 999, SubObject = new MockSubObject() { StringValue = "AAAASASASAS" } } });
         }
         stopwatch.Stop();
         Console.WriteLine("Time elapsed to save {1} samples: {0}", stopwatch.Elapsed, samplesCount);

         stopwatch.Reset();
         //Read n samplesall written samples and measure time
         stopwatch.Start();
         var result1 = saver.Read();
         stopwatch.Stop();
         Console.WriteLine("Read {1} samples: {0}", stopwatch.Elapsed, result1.Count());

         //Count number of number of written samples in 1 sec after n writen samples
         var task = Task.Run(() =>
         {
            try
            {
               while (!token.IsCancellationRequested)
               {
                  saver.Append(new List<MockObject>() { new MockObject() { IntValue = 999, SubObject = new MockSubObject() { StringValue = "AAAASASASAS" } } });
               }
            }
            catch (Exception)
            {
            }
         }, token.Token);
         Thread.Sleep(1000);
         token.Cancel();
         task.Wait(token.Token);

         var result2 = saver.Read();
         Console.WriteLine("Written {0} samples in 1 sec when file contains {1} saples.", result2.Count() - result1.Count(), result1.Count());

         stopwatch.Reset();
         stopwatch.Start();
         saver.Append(new List<MockObject>() { new MockObject() { IntValue = 999, SubObject = new MockSubObject() { StringValue = "AAAASASASAS" } } });
         stopwatch.Stop();

         Console.WriteLine("Write 1 sample after {0} samples: {1}", result2.Count(), stopwatch.Elapsed);
        
      }

      private void AreEqual(IEnumerable<MockObject> expectedData, IEnumerable<MockObject> resultData)
      {
         var expected = expectedData.ToList();
         var result = resultData.ToList();
         if (expectedData != null)
         {
            Assert.IsNotNull(result);
            Assert.AreEqual(expected.Count(), result.Count());
         }
         else
         {
            Assert.Fail("Expected collection of values cannot be null.");
         }

         for (var i = 0; i < expected.Count(); i++)
         {
            AreEqual(expected.ElementAt(i), result.ElementAt(i));
         }
      }

      private void AreEqual(MockObject expected, MockObject result)
      {
         Assert.AreEqual(expected.IntValue, result.IntValue);
         Assert.AreEqual(expected.SubObject.StringValue, result.SubObject.StringValue);
      }

      [TestMethod]
      public void AppendAll_Parallel()
      {
         const int threadCount = 5;
         var iteration = 0;
         var lockO = new object();
         Parallel.For(0, threadCount, t =>
        {
           IQueryable<MockObject> result;
           lock (lockO)
           {
              saver.Append(testList);
              result = saver.Read();
           }
           iteration++;
           Assert.AreEqual(iteration * testList.Count, result.Count());
        });
      }
   }
}
