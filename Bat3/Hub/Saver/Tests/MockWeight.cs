﻿using System;

namespace SaverTests
{
   public class MockWeight
   {
      public double Value { get; set; }
      public DateTime TimeStamp { get; set; }
      public MockSex Sex { get; set; }
   }

   public enum MockSex
   {
      Male,
      Female,
      Undefined
   }
}
