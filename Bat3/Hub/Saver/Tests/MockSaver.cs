using System.Collections.Generic;
using System.Linq;
using Saver.Interfaces;

namespace SaverTests
{
   public class MockSaver<T> : ISaver<T>
   {

      private readonly List<T> dataHolder;

      public MockSaver()
      {
         dataHolder = new List<T>();
      }

      #region Implementation of ISaver<T>

      public void Append(IEnumerable<T> dataList)
      {
         dataHolder.AddRange(dataList);
      }

      public IQueryable<T> Read()
      {
         return dataHolder.AsQueryable();
      }

      public void Update(IEnumerable<T> dataList)
      {
         dataHolder.Clear();
         dataHolder.AddRange(dataList);
      }

      #endregion
   }
}