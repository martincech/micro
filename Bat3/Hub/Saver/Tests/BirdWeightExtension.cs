using System;
using System.Linq;
using Saver.Storage;

namespace SaverTests
{
   static class BirdWeightExtension
   {
      public static IQueryable<MockWeight> ReadByDay(this BasePersistentStorage<MockWeight> storage, DateTime date)
      {
         var result = storage.Read();
         return result.Where(s => s.TimeStamp.Date == date.Date);
      }
   }
}