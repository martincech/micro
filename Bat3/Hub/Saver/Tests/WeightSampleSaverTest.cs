﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Saver.Storage;

namespace SaverTests
{
   [TestClass]
   public class WeightSampleSaverTest
   {
      private BasePersistentStorage<MockWeight> persistentStorage;
      private readonly List<MockWeight> samples;

      public WeightSampleSaverTest()
      {
         samples = new List<MockWeight>
         {
            new MockWeight {Sex = MockSex.Male, TimeStamp = DateTime.Now,Value = 1},
            new MockWeight {Sex = MockSex.Female, TimeStamp = DateTime.Now.AddDays(1),Value = 2},
            new MockWeight {Sex = MockSex.Undefined, TimeStamp = DateTime.Now.AddDays(1),Value = 3},
            new MockWeight {Sex = MockSex.Male, TimeStamp = DateTime.Now,Value = 4}
         };
      }

      [TestInitialize]
      public void Init()
      {
         persistentStorage = new BasePersistentStorage<MockWeight>(new MockSaver<MockWeight>());
      }

      [TestMethod]
      public void Append()
      {
         var sample = samples.First();
         persistentStorage.Append(sample);
         var result = persistentStorage.Read();
         AreEqual(sample, result.First());
      }

      [TestMethod]
      public void Append_Collection()
      {
         persistentStorage.Append(samples);
         var result = persistentStorage.Read().ToList();
         AreEqual(samples, result);
      }

      [TestMethod]
      public void Update()
      {
         persistentStorage.Update(samples);
         var result = persistentStorage.Read().ToList();
         AreEqual(samples, result);
      }

      [TestMethod]
      public void Update_WhenExist()
      {
         Append_Collection();
         persistentStorage.Update(samples);
         var result = persistentStorage.Read().ToList();
         AreEqual(samples, result);
      }

      [TestMethod]
      public void ReadByDate()
      {
         Append_Collection();

         var sample = samples.First();
         var expected = samples.Where(w => w.TimeStamp.Date == sample.TimeStamp.Date);
         var result = persistentStorage.ReadByDay(sample.TimeStamp);
         AreEqual(expected, result);
      }


      private void AreEqual(IEnumerable<MockWeight> expectedData, IEnumerable<MockWeight> resultData)
      {
         var expected = expectedData.ToList();
         var result = resultData.ToList();
         if (expectedData != null)
         {
            Assert.IsNotNull(result);
            Assert.AreEqual(expected.Count(), result.Count());
         }
         else
         {
            Assert.Fail("Expected collection of values cannot be null.");
         }


         for (var i = 0; i < expected.Count(); i++)
         {
            AreEqual(expected.ElementAt(i), result.ElementAt(i));
         }
      }

      private void AreEqual(MockWeight expected, MockWeight result)
      {
         Assert.AreEqual(expected.Sex, result.Sex);
         Assert.AreEqual(expected.TimeStamp, result.TimeStamp);
         Assert.AreEqual(expected.Value, result.Value);
      }
   }
}
