﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Xml;
using System.Xml.Serialization;
using Saver.Interfaces;

namespace Saver.Format
{
   public class XmlSaver<T> : ISaver<T> where T : new()
   {
      private const string TEMP_FILE = ".tmp";
      private readonly string filePath;
      private readonly XmlWriterSettings writerSettings;

      public XmlSaver(string filePath)
      {
         this.filePath = filePath;
         writerSettings = new XmlWriterSettings {Indent = true, IndentChars = " "};
      }

      public string FilePath
      {
         get { return filePath; }
      }

      #region Implementation of IPersistentStorage<T>

      public IQueryable<T> Read()
      {
         lock (typeof(XmlSaver<T>))
         {
            var list = new EnumerableQuery<T>(new List<T>());
            if (!File.Exists(filePath))
            {
               return list;
            }

            try
            {
               using (var reader = XmlReader.Create(filePath))
               {
                  var serializer = new XmlSerializer(typeof (List<T>));
                  var dataList = (List<T>) serializer.Deserialize(reader);
                  if (dataList.Any())
                  {
                     return new EnumerableQuery<T>(new List<T>(dataList));
                  }
               }
               return list;
            }
            catch (Exception)
            {
               return list;
            }
         }
      }

      public void Append(IEnumerable<T> dataList)
      {
         lock (typeof(XmlSaver<T>))
         {
            if (WriteToFileIfNotExists(dataList))
            {
               return;
            }
            using (var reader = XmlReader.Create(filePath))
            using (var writer = XmlWriter.Create(filePath + TEMP_FILE, writerSettings))
            {
               var collectionName = "";
               while (reader.Read())
               {
                  switch (reader.NodeType)
                  {
                     case XmlNodeType.XmlDeclaration:
                        break;
                     case XmlNodeType.Element:
                        if (string.IsNullOrEmpty(collectionName))
                        {
                           collectionName = reader.LocalName;
                        }
                        WriteElement(writer, reader);
                        break;
                     case XmlNodeType.EndElement:
                        WriteEndElement(dataList, reader, writer, collectionName);
                        break;
                     case XmlNodeType.Text:
                        writer.WriteRaw(SecurityElement.Escape(reader.Value));
                        break;
                     case XmlNodeType.CDATA:
                        writer.WriteCData(reader.Value);
                        break;
                  }
               }
            }

            ReplaceFileByTempFile();
         }
      }

      public void Update(IEnumerable<T> dataList)
      {
         lock (typeof (XmlSaver<T>))
         {
            File.Delete(filePath);
            WriteToFileIfNotExists(dataList);
         }
      }

      #endregion

      private bool WriteToFileIfNotExists(IEnumerable<T> dataList)
      {
         if (File.Exists(filePath)) return false;
         using (var writer = XmlWriter.Create(filePath, writerSettings))
         {
            var serializer = new XmlSerializer(typeof (List<T>));
            serializer.Serialize(writer, dataList);
         }
         return true;
      }

      private static void WriteElement(XmlWriter writer, XmlReader reader)
      {
         writer.WriteStartElement(reader.Prefix, reader.LocalName, reader.NamespaceURI);
         if (reader.HasAttributes)
         {
            while (reader.MoveToNextAttribute())
            {
               writer.WriteAttributeString(reader.Prefix, reader.LocalName, reader.NamespaceURI, reader.Value);
            }
         }
         if (reader.IsEmptyElement)
            writer.WriteEndElement();
      }

      private static void WriteEndElement(IEnumerable<T> dataList, XmlReader reader, XmlWriter writer, string collectionName)
      {
         if (reader.Name == collectionName)
         {
            var serializer = new XmlSerializer(typeof (T));
            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            foreach (var data in dataList)
            {
               serializer.Serialize(writer, data, ns);
            }
         }
         writer.WriteEndElement();
      }

      private void ReplaceFileByTempFile()
      {
         if (!File.Exists(filePath) || !File.Exists(filePath + TEMP_FILE)) return;
         File.Delete(filePath);
         File.Move(filePath + TEMP_FILE, filePath);
      }
   }
}
