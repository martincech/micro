﻿using System.Collections.Generic;
using System.Linq;
using Saver.Interfaces;

namespace Saver.Storage
{
   public class BasePersistentStorage<T> : IPersistentStorage<T>
   {
      private readonly ISaver<T> saver;

      public BasePersistentStorage(ISaver<T> saver)
      {
         this.saver = saver;
      }

      #region Implementation of IPersistentStorage<T>

      public void Append(T data)
      {
         Append(new List<T> {data});
      }

      public void Append(IEnumerable<T> dataList)
      {
         saver.Append(dataList);
      }

      public void Update(IEnumerable<T> dataList)
      {
         saver.Update(dataList);
      }

      public void Update(T data)
      {
         saver.Update(new List<T> {data});
      }

      public IQueryable<T> Read()
      {
         return saver.Read();
      }
      #endregion
   }
}
