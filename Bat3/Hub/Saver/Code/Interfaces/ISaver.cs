﻿using System.Collections.Generic;
using System.Linq;

namespace Saver.Interfaces
{
   public interface ISaver<T>
   {
      void Append(IEnumerable<T> dataList);
      void Update(IEnumerable<T> dataList);
      IQueryable<T> Read();
   }
}