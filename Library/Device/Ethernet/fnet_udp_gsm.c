//******************************************************************************
//
//   fnet_udp_gsm.c     TCP stack on GSM device implementation
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "fnet_config.h"

#if FNET_CFG_UDP_GSM

#include "fnet.h"
#include "fnet_error.h"
#include "fnet_socket.h"
#include "fnet_socket_prv.h"
#include "fnet_timer_prv.h"
#include "fnet_udp_gsm.h"
#include "fnet_isr.h"
#include "fnet_checksum.h"
#include "fnet_prot.h"
#include "fnet_stdlib.h"
#include "fnet_debug.h"
#include "Message/MGsm.h"

static void fnet_udp_gsm_timo( void *cookie );
//timer timed out
static void fnet_udp_gsm_status_read(void *sock);
//timo poll routine
static int fnet_udp_gsm_init( void );
//initialization
static void fnet_udp_gsm_release( void );
//free

// socket api
static int fnet_udp_gsm_attach( fnet_socket_t *sk );
static int fnet_udp_gsm_detach( fnet_socket_t *sk );
static int fnet_udp_gsm_connect( fnet_socket_t *sk, struct sockaddr *foreign_addr);
static int fnet_udp_gsm_rcv( fnet_socket_t *sk, char *buf, int len, int flags, struct sockaddr *foreign_addr);
static int fnet_udp_gsm_snd( fnet_socket_t *sk, char *buf, int len, int flags, const struct sockaddr *foreign_addr);
static int fnet_udp_gsm_shutdown( fnet_socket_t *sk, int how );

/*****************************************************************************
 * Protocol API structure.
 ******************************************************************************/
/* Socket API interface */
static const fnet_socket_prot_if_t fnet_udp_gsm_socket_api =
{
    0,                        /* TRUE = connection required by protocol.*/
    fnet_udp_gsm_attach,      /* A new socket has been created.*/
    fnet_udp_gsm_detach,
    fnet_udp_gsm_connect,
    0,
    fnet_udp_gsm_rcv,
    fnet_udp_gsm_snd,
    fnet_udp_gsm_shutdown,
    0,
    0,
    0
};

/* Protocol interface.*/
fnet_prot_if_t fnet_udp_gsm_prot_if =
{
    0, 
    AF_INET,                           /* Protocol family.*/
    SOCK_DGRAM,                       /* Socket type.*/
    FNET_IP_PROTOCOL_UDP,              /* Protocol number.*/
    fnet_udp_gsm_init,
    fnet_udp_gsm_release,
    0,                                 /* Control input (from below).*/
    0, 
    &fnet_udp_gsm_socket_api           /* Socket API */
};

static fnet_timer_desc_t fnet_udp_gsm_timer;

/************************************************************************
* NAME: fnet_udp_gsm_init
*
* DESCRIPTION: This function performs a protocol initialization.
*
* RETURNS: If no error occurs, this function returns ERR_OK. Otherwise
*          it returns FNET_ERR.
*************************************************************************/
static int fnet_udp_gsm_init( void )
{
   /* Create the timer.*/
//   fnet_udp_gsm_timer = fnet_timer_new(FNET_TCP_SLOWTIMO*2 / FNET_TIMER_PERIOD_MS, fnet_udp_gsm_timo, 0);

//   if(!fnet_udp_gsm_timer)
//   {
//       fnet_udp_gsm_timer = 0;
//       return FNET_ERR;
//   }
   MGsmPowerOn();
   return FNET_OK;
}
/************************************************************************
* NAME: fnet_udp_gsm_release
*
* DESCRIPTION: This function resets and deletes sockets and releases timers.
*
* RETURNS: None.
*************************************************************************/
static void fnet_udp_gsm_release( void )
{
   /* Release sockets.*/
   while(fnet_udp_gsm_prot_if.head){
       fnet_udp_gsm_detach(fnet_udp_gsm_prot_if.head);
   }
   /* Free timers.*/
//   fnet_timer_free(fnet_udp_gsm_timer);
//   fnet_udp_gsm_timer = 0;
   MGsmPowerResume();
}

/************************************************************************
* NAME: fnet_udp_gsm_attach
*
* DESCRIPTION: This function performs the initialization of the  
*              socket's options
*
* RETURNS: If no error occurs, this function returns FNET_OK. Otherwise
*          it returns FNET_ERR.
*************************************************************************/
static int fnet_udp_gsm_attach( fnet_socket_t *sk )
{
   sk->send_buffer.count_max = FNET_CFG_SOCKET_UDP_TX_BUF_SIZE;
   sk->receive_buffer.count_max = FNET_CFG_SOCKET_UDP_TX_BUF_SIZE;
   return (FNET_OK);
}

/************************************************************************
* NAME: fnet_udp_gsm_detach
*
* DESCRIPTION: This function performs the connection termination.
*
* RETURNS: If no error occurs, this function returns FNET_OK. Otherwise
*          it returns FNET_ERR.
*************************************************************************/

static int fnet_udp_gsm_detach( fnet_socket_t *sk )
{
   if( sk->state == SS_CONNECTED){
      while( !MGsmSocketClose(sk->descriptor)){}
   }
   fnet_socket_release( &fnet_udp_gsm_prot_if.head, sk);
   fnet_memset_zero(&sk->foreign_addr, sizeof(sk->foreign_addr));
   sk->state == SS_UNCONNECTED;
   return FNET_OK;
}

/************************************************************************
* NAME: fnet_udp_gsm_connect
*
* DESCRIPTION: This function performs the connection establishment.
*   
* RETURNS: If no error occurs, this function returns FNET_OK. Otherwise
*          it returns FNET_ERR.
*************************************************************************/
static int fnet_udp_gsm_connect( fnet_socket_t *sk, struct sockaddr *foreign_addr)
{
   if( !MGsmSocketOpenUdp( sk->descriptor, (struct sockaddr_in *)foreign_addr)){
      fnet_socket_set_error(sk, FNET_ERR_HOSTUNREACH);
      return FNET_ERR;
   }

   //wait for socket creation in GSM module
   while( MGsmSocketState(sk->descriptor) != SS_CONNECTED){}
   sk->state = SS_CONNECTED;
   sk->foreign_addr = *foreign_addr;
   if( !MGsmSocketAddress( sk->descriptor, (struct sockaddr_in *)&sk->local_addr)){
      while( !MGsmSocketClose(sk->descriptor)){}
      return FNET_ERR;
   }

   return FNET_OK;
}

/************************************************************************
* NAME: fnet_udp_gsm_rcv
*
* DESCRIPTION: This function receives the data
*
* RETURNS: If no error occurs, this function returns the length
*          of the received data. Otherwise, it returns FNET_ERR.
*************************************************************************/

static int fnet_udp_gsm_rcv( fnet_socket_t *sk, char *buf, int len, int flags, struct sockaddr *addr)
{
struct sockaddr      *foreign_addr;
int rcvLen;

   FNET_COMP_UNUSED_ARG(flags);

   if(addr){
       foreign_addr = addr;
   } else{
       foreign_addr = &sk->foreign_addr;
   }

   if( sk->state != SS_CONNECTED || foreign_addr != &sk->foreign_addr){
      fnet_socket_set_error(sk, FNET_ERR_NOTCONN);
      return FNET_ERR;
   }
   rcvLen = MGsmSocketReceive( sk->descriptor, buf, len);
   if( rcvLen < 0) {
      return FNET_ERR;
   }
   return rcvLen;
}

/************************************************************************
* NAME: fnet_udp_gsm_snd
*
* DESCRIPTION: This sends the data that can be sent.
*
* RETURNS: If no error occurs, this function returns the length
*          of the send data
*          Otherwise, it returns FNET_ERR.
*************************************************************************/
static int fnet_udp_gsm_snd( fnet_socket_t *sk, char *buf, int len, int flags, const struct sockaddr *addr)
{
struct sockaddr   *foreign_addr;

   FNET_COMP_UNUSED_ARG(flags);

   if(addr){
       foreign_addr = (struct sockaddr*)addr;
   } else{
       foreign_addr = &sk->foreign_addr;
   }

   if( sk->state != SS_CONNECTED || foreign_addr != &sk->foreign_addr){
      fnet_socket_set_error(sk, FNET_ERR_NOTCONN);
      return FNET_ERR;
   }
   return MGsmSocketSend( sk->descriptor, buf, len);
}

/************************************************************************
* NAME: fnet_udp_gsm_shutdown
*
* DESCRIPTION: This function closes a write-half, read-half, or
*              both halves of the connection.
*
* RETURNS: If no error occurs, this function returns FNET_OK. Otherwise
*          it returns FNET_ERR.
*************************************************************************/

static int fnet_udp_gsm_shutdown( fnet_socket_t *sk, int how )
{

    /* Shutdown the writing.*/
    if(how & SD_WRITE && !sk->send_buffer.is_shutdown)
    {
        /* Set the flag of the buffer.*/
        sk->send_buffer.is_shutdown = 1;
    }

    /* Shutdown the reading.*/
    if(how & SD_READ && !sk->receive_buffer.is_shutdown)
    {
        fnet_socket_buffer_release(&sk->receive_buffer);
        /* Set the flag of the buffer (Data can't be read).*/
        sk->receive_buffer.is_shutdown = 1;
    }

    return FNET_OK;
}

/************************************************************************
* NAME: fnet_udp_gsm_timo
*
* DESCRIPTION: This function processes the timeouts.
*
* RETURNS: None.
*************************************************************************/
//static fnet_poll_desc_t stateService = FNET_ERR;

//static void fnet_udp_gsm_timo( void *cookie )
//{
//fnet_socket_t *sk;

//   sk = fnet_udp_gsm_prot_if.head;
//   while(sk)
//   {
//      if( sk->state == SS_UNCONNECTED && stateService == FNET_ERR){
//         stateService =
//               fnet_poll_service_register( &fnet_udp_gsm_status_read, (void *)sk);
//      }
//      sk = sk->next;
//   }
//}

//static void fnet_udp_gsm_status_read(void *sock)
////timo poll routine
//{
//fnet_socket_t *sk = (fnet_socket_t *)sock;
//fnet_socket_state_t skState;
//skState = MGsmSocketState(sk->descriptor);
//   if( MGsmRegistered()){
//      skState = MGsmSocketState(sk->descriptor);
//      if(!(sk->state == SS_CONNECTED && skState == SS_CONNECTING)){
//      //communication problem with GSM module (locked by someone else or ...
//         if( sk->state != skState){
//            switch (skState){
//               case SS_CONNECTED :
//                  if( !MGsmSocketAddress( sk->descriptor, (struct sockaddr_in *)&sk->local_addr)){
//                     return; // try again later
//                  }
//                  break;

//               case SS_UNCONNECTED :
//                  if( !MGsmSocketClose( sk->descriptor)){
//                     return; // try close again next time
//                  }

//               default :
//                  break;
//            }
//         }
//         sk->state = skState;
//      }
//   }

//   fnet_poll_service_unregister( stateService);
//   stateService = FNET_ERR;
//}

#endif
