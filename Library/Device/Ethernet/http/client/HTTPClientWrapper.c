
#include "HTTPClientWrapper.h"

#ifdef FNET_STACK
#include "Ethernet/EthernetLibrary.h"

static struct hostent HostEnt;
static TYesNo dnsDone;

static void _DnsCallback(fnet_address_family_t addr_family, const char *addr_list, int addr_list_size, long cookie)
// dns resolve callback function
{
   if( addr_list == FNET_NULL){
      HostEnt.h_name = NULL;
   } else {

      HostEnt.h_length = addr_list_size;
      HostEnt.h_addr_list = (char**)&addr_list;
   }
   dnsDone = YES;
}

struct hostent *gethostbyname(const char *name)
{
struct sockaddr_in   addr;

   char DnsServerAddress[FNET_IP4_ADDR_STR_SIZE];
   EthernetLibraryDnsServer( DnsServerAddress, FNET_IP4_ADDR_STR_SIZE);
   struct fnet_dns_params dnsParams = {
      {0,0,0},               /* DNS server address */
      (char*)name,           /* host name to resolve */
      AF_INET,
      (fnet_dns_handler_resolved_t)&_DnsCallback,          /* callback pointer */
      0                       /* no cookie needed */
   };
   fnet_inet_aton(DnsServerAddress, (struct in_addr*)&dnsParams.dns_server_addr);
   if( fnet_dns_init(&dnsParams) == FNET_ERR){
      return NULL;
   }

   HostEnt.h_name = (char*)name;
   HostEnt.h_aliases = NULL;
   HostEnt.h_addrtype = AF_INET;
   dnsDone = NO;
   // wait until adress is resolved or not
   while(!dnsDone){
      MultitaskingReschedule();
   }
   if(HostEnt.h_name == NULL){
      return NULL;
   }
   return &HostEnt;
}

unsigned long inet_addr(const char *addr)
{
  struct in_addr ret;
  fnet_inet_aton((char*)addr, &ret);
  return ret.s_addr;
}

unsigned short htons(unsigned short hostshort)
{
  return FNET_HTONS(hostshort);
}

int FdIsSet(SOCKET fd, fd_set *set)
{
   uint32 i;
   for(i = 0; i < ((fd_set *)(set))->fd_count; i++) {
      if (((fd_set *)(set))->fd_array[i] == (fd)) {
         return 1;
      }
   }
   return 0;
}

int select(int nfds, fd_set *readfds,fd_set *writefds,fd_set *exceptfds,const struct timeval *timeout)
{
   int events = 0;
   int i;
   int errCode = FNET_OK;
   unsigned long optVal;
   int optLen = sizeof(unsigned long);
   long waitTime = fnet_timer_ms2ticks(((timeout->tv_sec*1000) + (long)(timeout->tv_usec/1000))) + fnet_timer_ticks();
   fd_set rdToClear;
   fd_set wrToClear;
   FD_ZERO(&rdToClear);
   FD_ZERO(&wrToClear);
   do{
     for(i = 0; i < nfds; i++){
       if(readfds && FD_ISSET(i, readfds)){
         // READ FDS
         errCode = getsockopt(i, SOL_SOCKET, SO_RCVNUM, (char*)&optVal, &optLen);
         if(errCode != FNET_OK){return -1;}
         if(optVal != 0){
           //ready
           events++;
           FD_CLR(i, &rdToClear);
         } else {
           //not ready
           FD_SET(i, &rdToClear);
         }
       }
       if(writefds && FD_ISSET(i, writefds)){
         // WRITE FDS
         errCode = getsockopt(i, SOL_SOCKET, SO_SNDNUM, (char*)&optVal, &optLen);
         if(errCode != FNET_OK){return -1;}
         if(optVal == 0){
           //ready
           events++;
           FD_CLR(i, &wrToClear);
         } else {
           //not ready
           FD_SET(i, &wrToClear);
         }
       }
       // exceptfds
     }
     if(events != 0){break;}
     MultitaskingReschedule();
   }while(fnet_timer_ticks() < waitTime);

   for(i = 0; i < nfds; i++){
     if(readfds){
       if(FD_ISSET(i, &rdToClear)){
         FD_CLR(i, readfds);
       }
     }
     if(writefds){
       if(FD_ISSET(i, &wrToClear)){
         FD_CLR(i, writefds);
       }
     }
     if(exceptfds){
       FD_CLR(i, exceptfds);
     }
   }
   return events;
}

#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Section      : Stdc: HTTPWrapperIsAscii
// Last updated : 15/05/2005
// Notes	    : Same as stdc: isascii
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int HTTPWrapperIsAscii(int c)
{
    return (!(c & ~0177));
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Section      : Stdc: HTTPWrapperToUpper
// Last updated : 15/05/2005
// Notes	    : Convert character to uppercase.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int HTTPWrapperToUpper(int c)
{
    // -32
    if(HTTPWrapperIsAscii(c) > 0)
    {
        if(c >= 97 && c <= 122)
        {
            return (c - 32);
        }
    }

    return c;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Section      : Stdc: HTTPWrapperToLower
// Last updated : 13/06/2006
// Notes	       : Convert character to lowercase.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int HTTPWrapperToLower(int c)
{
    // +32
    if(HTTPWrapperIsAscii(c) > 0)
    {
        if(c >= 65 && c <= 90)
        {
            return (c + 32);
        }
    }

    return c;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Section      : Stdc: isalpha
// Last updated : 15/05/2005
// Notes	    : returns nonzero if c is a particular representation of an alphabetic character
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int HTTPWrapperIsAlpha(int c)
{

    if(HTTPWrapperIsAscii(c) > 0)
    {
        if( (c >= 97 && c <= 122) || (c >= 65 && c <= 90))
        {
            return c;
        }
    }
    return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Section      : Stdc: isalnum
// Last updated : 15/05/2005
// Notes	    : returns nonzero if c is a particular representation of an alphanumeric character
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int HTTPWrapperIsAlNum(int c)
{
    if(HTTPWrapperIsAscii(c) > 0)
    {

        if(HTTPWrapperIsAlpha(c) > 0)
        {
            return c;
        }

        if( c >= 48 && c <= 57)
        {
            return c;
        }

    }
    return 0;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Section      : HTTPWrapper_itoa
// Last updated : 15/05/2005
// Notes	    : same as stdc itoa() // hmm.. allmost the same
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

char* HTTPWrapperItoa(char *s,int a)
{
    unsigned int b;

    if(a > 2147483647)
    {
        return 0; // overflow
    }

    if (a < 0) b = -a, *s++ = '-';
    else b = a;
    for(;a;a=a/10) s++;
    for(*s='\0';b;b=b/10) *--s=b%10+'0';
    return s;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Section      : HTTPWrapper_ShutDown
// Last updated : 15/05/2005
// Notes	    : Handles parameter changes in the socket shutdown()
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int HTTPWrapperShutDown (int s,int how)
{
    return shutdown(s,how);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Section      : HTTPWrapper_GetSocketError
// Last updated : 15/05/2005
// Notes	    : WSAGetLastError Wrapper (Win32 Specific)
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int HTTPWrapperGetSocketError (int s)
{
#ifdef FNET_STACK
    return fnet_error_get();
#elif defined(_WIN32)
    return WSAGetLastError();
#else
    return errno;
#endif
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Section      : HTTPWrapper_GetHostByName
// Last updated : 15/05/2005
// Notes	    : gethostbyname for Win32
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

unsigned long HTTPWrapperGetHostByName(char *name,unsigned long *address)
{
    HTTP_HOSTNET     *HostEntry;
    int     iPos = 0, iLen = 0,iNumPos = 0,iDots =0;
    long    iIPElement;
    char    c = 0;
    char    Num[4];
    int     iHostType = 0; // 0 : numeric IP

    // Check if the name is an IP or host
    iLen = strlen(name);
    for(iPos = 0; iPos <= iLen;iPos++)
    {
        c = name[iPos];
        if((c >= 48 && c <= 57)  || (c == '.') )
        {
            // c is numeric or dot
            if(c != '.')
            {
                // c is numeric
                if(iNumPos > 3)
                {
                    iHostType++;
                    break;
                }
                Num[iNumPos] = c;
                Num[iNumPos + 1] = 0;
                iNumPos ++;
            }
            else
            {
                iNumPos = 0;
                iDots++;
                iIPElement = atol(Num);
                if(iIPElement > 256 || iDots > 3)
                {
                    return 0; // error invalid IP
                }
            }
        }
        else
        {
            break; // this is an alpha numeric address type
        }
    }

    if(c == 0 && iHostType == 0 && iDots == 3)
    {
        iIPElement = atol(Num);
        if(iIPElement > 256)
        {
            return 0; // error invalid IP
        }
    }
    else
    {
        iHostType++;
    }

    if(iHostType > 0)
    {

        HostEntry = gethostbyname(name);
        if(HostEntry)
        {
            *(address) = *((u_long*)HostEntry->h_addr_list[0]);

            //*(address) = (unsigned long)HostEntry->h_addr_list[0];
            return 1; // Error
        }
        else
        {
            return 0; // OK
        }
    }

    else // numeric address - no need for DNS resolve
    {
        *(address) = inet_addr(name);
        return 1;

    }
    return 0;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Section      : HTTPWrapper_GetRandomeNumber
// Last updated : 15/05/2005
// Notes	    : GetRandom number for Win32
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void HTTPWrapperInitRandomeNumber()
{
    srand((unsigned int)time(NULL));
}

int HTTPWrapperGetRandomeNumber()
{
    int num;
    num = (int)(((double) rand()/ ((double)RAND_MAX+1)) * 16);
    return num;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Section      : HTTPWrapper_GetRTC
// Last updated : 15/05/2005
// Author Name	: Eitan Michaelson
// Notes	    : Get uptime under Win32 & AMT
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

long HTTPWrapperGetUpTime()
{
#ifdef FNET_STACK
    return fnet_timer_ticks();
#elif defined(_WIN32)

    long lTime = 0;

    lTime = (GetTickCount() / CLOCKS_PER_SEC);
    return lTime;
#else

   struct timespec tp;

   clock_gettime(CLOCK_MONOTONIC , &tp);
   return tp.tv_sec;

#endif
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Section      : TSL Wrapper
// Last updated : 15/05/2005
// Notes	    : HTTPWrapper_Sec_Connect
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int HTTPWrapperSSLConnect(int s,const struct sockaddr *name,int namelen,char *hostname)
{
    return -1;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int HTTPWrapperSSLNegotiate(int s,const struct sockaddr *name,int namelen,char *hostname)
{
    return -1;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int HTTPWrapperSSLSend(int s,char *buf, int len,int flags)
{
    return -1;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int HTTPWrapperSSLRecv(int s,char *buf, int len,int flags)
{
    return -1;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int HTTPWrapperSSLRecvPending(int s)
{
    return -1;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int HTTPWrapperSSLClose(int s)
{
    return -1;

}
