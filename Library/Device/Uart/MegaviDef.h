//*****************************************************************************
//
//   MegaviDef.h  MEGAVI protocol definitions
//   Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __MegaviDef_H__
   #define __MegaviDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

//------------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------------

#define MEGAVI_SMS_SIZE 160       // megavi SMS message size
#define MEGAVI_REPLY_BASE  0x80   // reply code base

#define MEGAVI_LEADER  0x55       // frame leader character
#define MEGAVI_TRAILER 0xAA       // frame trailer character

//-----------------------------------------------------------------------------
// Commands
//-----------------------------------------------------------------------------

typedef enum {
   MEGAVI_COMMAND_UNDEFINED,           // undefined/unused command
   MEGAVI_COMMAND_VERSION,             // get version info
   MEGAVI_COMMAND_MALE_DATA_GET,       // get statistics & weighings for male gender
   MEGAVI_COMMAND_FEMALE_DATA_GET,     // get statistics & weighings for female gender
   MEGAVI_COMMAND_SMS_SEND,            // send SMS
   MEGAVI_COMMAND_SMS_STATUS_GET,      // get SMS status
   _MEGAVI_COMMAND_LAST                // enum delimiter
} EMegaviCommand;

//-----------------------------------------------------------------------------
// SMS request code
//-----------------------------------------------------------------------------

#define MEGAVI_SMS_REQUEST_INVALID    '\0'                 // no SMS request
#define MEGAVI_SMS_REQUEST_SHORT      ' '                  // SMS request without data
#define MEGAVI_SMS_REQUEST_MIN        ((char)(' ' + 1))    // SMS request character min
#define MEGAVI_SMS_REQUEST_MAX        '~'                  // SMS request character max

//-----------------------------------------------------------------------------
// SMS target code
//-----------------------------------------------------------------------------

#define MEGAVI_SMS_TARGET_BROADCAST   '\0'                 // broadcast SMS to all phone numbers
#define MEGAVI_SMS_TARGET_MIN         ' '                  // single SMS reply character min
#define MEGAVI_SMS_TARGET_MAX         '~'                  // single SMS reply character max

//-----------------------------------------------------------------------------
// Short command
//-----------------------------------------------------------------------------

typedef struct {
   byte Command;                       // command code MEGAVI_COMMAND_VERSION, MEGAVI_COMMAND_MALE_DATA_GET, MEGAVI_COMMAND_FEMALE_DATA_GET
} TMegaviCommand;

//-----------------------------------------------------------------------------
// SMS send command
//-----------------------------------------------------------------------------

typedef struct {
   byte Command;                       // command code MEGAVI_COMMAND_SMS_SEND
   byte Target;                        // SMS destination code
   char Text[ MEGAVI_SMS_SIZE];        // SMS text
} TMegaviCommandSmsSend;

//-----------------------------------------------------------------------------
// SMS status command
//-----------------------------------------------------------------------------

typedef struct {
   byte Command;                       // command code MEGAVI_SMS_STATUS_GET
   byte Slot;                          // requested SMS slot
} TMegaviCommandSmsStatusGet;

//*****************************************************************************

//-----------------------------------------------------------------------------
// Replies
//-----------------------------------------------------------------------------

typedef enum {
   MEGAVI_REPLY_UNDEFINED = MEGAVI_REPLY_BASE, // undefined/unused reply
   MEGAVI_REPLY_VERSION,          // version reply
   MEGAVI_REPLY_MALE_DATA,        // statistics & weighings data reply for male gender
   MEGAVI_REPLY_FEMALE_DATA,      // statistics & weighings data reply for female gender
   MEGAVI_REPLY_SMS_SEND,         // SMS send reply
   MEGAVI_REPLY_SMS_STATUS,       // SMS status reply
   _MEGAVI_REPLY_LAST             // enum delimiter
} EMegaviReply;

//-----------------------------------------------------------------------------
// Data reply statistics
//-----------------------------------------------------------------------------

typedef struct {
   word  Target;
   word  Count;
   word  Average;
   int32 Gain;
   word  Sigma;
   word  Uniformity;
} TMegaviStatistics;

//-----------------------------------------------------------------------------
// Weight
//-----------------------------------------------------------------------------

typedef word TMegaviWeight;

//-----------------------------------------------------------------------------
// Male/Female data response
//-----------------------------------------------------------------------------

#define MEGAVI_WEIGHT_COUNT  50                    // last weighings count

typedef struct {
   TMegaviStatistics Statistics;
   TMegaviWeight     Weight[ MEGAVI_WEIGHT_COUNT];
} TMegaviData;

//-----------------------------------------------------------------------------
// Version reply
//-----------------------------------------------------------------------------

typedef struct {
   byte Reply;                         // reply code MEGAVI_REPLY_VERSION
   byte Minor;                         // minor version number
   byte Major;                         // major version number
} TMegaviReplyVersion;


//-----------------------------------------------------------------------------
// Data reply
//-----------------------------------------------------------------------------

typedef struct {
   byte      Reply;                    // reply code MEGAVI_REPLY_MALE_DATA, MEGAVI_REPLY_FEMALE_DATA
   byte      SmsRequest;               // SMS request code
   TMegaviData Data;                   // statistics & weighings data
} TMegaviReplyData;

//-----------------------------------------------------------------------------
// SMS send reply
//-----------------------------------------------------------------------------

typedef struct {
   byte Reply;                         // reply code MEGAVI_REPLY_SMS_SEND
   byte Slot;                          // SMS slot code
} TMegaviReplySmsSend;

#define MEGAVI_SMS_PHONEBOOK_ERROR 0xFD     // phonebook error unable send broadcast
#define MEGAVI_SMS_TARGET_ERROR    0xFE     // unknown target phone number
#define MEGAVI_SMS_SLOT_ERROR      0xFF     // unable get SMS slot

//-----------------------------------------------------------------------------
// SMS status reply
//-----------------------------------------------------------------------------

typedef struct {
   byte Reply;                         // reply code
   byte Status;                        // SMS status code
} TMegaviReplySmsStatus;

// SMS status codes :
typedef enum {
   MEGAVI_SMS_STATUS_UNDEFINED,   // unknown status
   MEGAVI_SMS_STATUS_EMPTY,       // SMS slot empty
   MEGAVI_SMS_STATUS_PENDING,     // SMS pending for send
   MEGAVI_SMS_STATUS_SENDED,      // SMS already sended
   MEGAVI_SMS_STATUS_ERROR = 0xFF // unable deliver SMS
} EMegaviSmsStatus;

#endif