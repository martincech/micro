//*****************************************************************************
//
//    UartStatus.h  RS232/485 status
//    Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __UartStatus_H__
   #define __UartStatus_H__

//-----------------------------------------------------------------------------
// UART status
//-----------------------------------------------------------------------------

typedef enum {
   UART_IDLE,                     // stopped, no activity
   // receive status :
   UART_RECEIVE_ACTIVE,           // receiving active
   UART_RECEIVE_PAUSED,           // receiving paused
   UART_RECEIVE_FRAMING_ERROR,    // receiving terminated with framing/parity error
   UART_RECEIVE_OVERRUN_ERROR,    // receive interrupt overrun
   UART_RECEIVE_SIZE_ERROR,       // received characters count greater than buffer size
   UART_RECEIVE_HEADER_CRC_ERROR, // wrong received header CRC
   UART_RECEIVE_CRC_ERROR,        // wrong received frame CRC
   UART_RECEIVE_REPLY_TIMEOUT,    // no characters received up to reply timeout
   UART_RECEIVE_TIMEOUT,          // intercharacter timeout   
   UART_RECEIVE_FRAME,            // complete frame received
   _UART_RECEIVE_LAST,
   // send status :
   _UART_SEND = _UART_RECEIVE_LAST,
   UART_SEND_ACTIVE,              // send active
   UART_SEND_WAITING,
   UART_SEND_DONE,                // send done
   _UART_SEND_LAST
} EUartStatus;

   
//-----------------------------------------------------------------------------
// UART timeout
//-----------------------------------------------------------------------------

#define UART_TIMEOUT_OFF    0     // without timeout

#endif
