//*****************************************************************************
//
//    NativeDef.h  UART native mode
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __NativeDef_H__
   #define __NativeDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

//------------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------------

// maximum packet data size :   
#ifndef NATIVE_DATA_SIZE
   #define NATIVE_DATA_SIZE  128
#endif

// frame leader character :
#define NATIVE_LEADER  0x55

//------------------------------------------------------------------------------
// Data types
//------------------------------------------------------------------------------
   
typedef byte TNativeAddress;
typedef word TNativeSize;
typedef byte TNativeHeaderCrc;
typedef word TNativeCrc;

#ifdef __WIN32__
   #pragma pack( push, 1)                   // byte alignment
#endif

//------------------------------------------------------------------------------
// Frame
//------------------------------------------------------------------------------

typedef struct {
   byte             Leader;
   TNativeAddress   Address;
   TNativeSize      Size;
   TNativeHeaderCrc HeaderCrc;
   byte             Data[ NATIVE_DATA_SIZE];
   TNativeCrc       Crc;
} __packed TNativeFrame;

#ifdef __WIN32__
   #pragma pack( pop)                       // original alignment
#endif

//------------------------------------------------------------------------------
// Parsing macros
//------------------------------------------------------------------------------

#define NATIVE_LEADER_OFFSET      0
#define NATIVE_ADDRESS_OFFSET     offsetof( TNativeFrame, Address)
#define NATIVE_SIZE_OFFSET        offsetof( TNativeFrame, Size)
#define NATIVE_HEADER_CRC_OFFSET  offsetof( TNativeFrame, HeaderCrc)
#define NATIVE_DATA_OFFSET        offsetof( TNativeFrame, Data)
#define NativeCrcOffset( Size)    (NATIVE_DATA_OFFSET + (Size))

// frame constants :
#define NativeFrameSize( Size)    (sizeof( TNativeFrame) - NATIVE_DATA_SIZE + (Size))
#define NativeCrc( Frame)         ((TNativeCrc)((TNativeCrc)(Frame)->Data[ (Frame)->Size] | ((TNativeCrc)(Frame)->Data[ (Frame)->Size + 1] << 8)))

// header CRC calculation :
#define NativeHeaderCrc( Frame)   ((TNativeHeaderCrc)-((Frame)->Address + ((Frame)->Size & 0xFF) + ((Frame)->Size >> 8)))


#endif
