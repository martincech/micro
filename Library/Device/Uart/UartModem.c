//******************************************************************************
//
//   UartModem.c   Serial port modem support
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "Uart/UartModem.h"
#include "Uart/Uart.h"
#include <string.h>

#define UART_MODEM_BAUD_DEFAULT            9600
#define UART_MODEM_TIMEOUT_DEFAULT         500

// enable data logging
#ifdef UART_MODEM_MONITOR
   #define MDUMP( Buffer, Size)         _MonitorDump( Buffer, Size)
   #define MREPORT( Msg)                _MonitorReport( Msg)
   
   #define MONITOR_BUFFER_SIZE  31

   static char _MonitorBuffer[ MONITOR_BUFFER_SIZE + 1];

   static void _MonitorDump( char *Buffer, int Size);
   // Send <Buffer> with <Size> to monitor port

   static void _MonitorReport( const char *Message);
   // Send <Message> to monitor port
#else
   #define MDUMP( Buffer, Size)
   #define MREPORT( Msg)
#endif

//------------------------------------------------------------------------------
//   Initialize
//------------------------------------------------------------------------------

void UartModemInit( void)
// Initialize
{
   UartInit( UART_MODEM_CHANNEL);
   UartSetup( UART_MODEM_CHANNEL, UART_MODEM_BAUD_DEFAULT, UART_MODEM_FORMAT);
   UartTimeoutSet( UART_MODEM_CHANNEL, UART_MODEM_TIMEOUT_DEFAULT, UART_MODEM_INTERCHARACTER_TIMEOUT);
   UartSendDelaySet( UART_MODEM_CHANNEL, 0);
   UartModeSet( UART_MODEM_CHANNEL, UART_MODE_BINARY_MASTER);
} // UartModemInit


//------------------------------------------------------------------------------
//   Deinitialize
//------------------------------------------------------------------------------

void UartModemDeinit( void)
// Deinitialiize
{
   UartDeinit( UART_MODEM_CHANNEL);
}

//------------------------------------------------------------------------------
//   Baud rate
//------------------------------------------------------------------------------

void UartModemBaudRateSet( int BaudRate)
// Set modem <BaudRate>
{
   UartSetup( UART_MODEM_CHANNEL, BaudRate, UART_MODEM_FORMAT);
} // UartModemBaudRateSet

//------------------------------------------------------------------------------
//   Rx timeout
//------------------------------------------------------------------------------

void UartModemTimeoutSet( int Timeout)
// Set receive <Timeout>
{
   UartTimeoutSet( UART_MODEM_CHANNEL, Timeout, UART_MODEM_INTERCHARACTER_TIMEOUT);
} // UartModemTimeoutSet

//------------------------------------------------------------------------------
//   Rx Buffer
//------------------------------------------------------------------------------

void UartModemBufferSet( void *Buffer, int Size)
// Set Rx <Buffer> with <Size>
{
   UartBufferSet( UART_MODEM_CHANNEL, Buffer, Size);
} // UartModemBufferSet

//------------------------------------------------------------------------------
//   Send
//------------------------------------------------------------------------------

void UartModemSend( char *String, int Size)
// Send <String> with optional <Size>
{
   if( Size == 0){
      Size = strlen( String);
   }
   MDUMP( String, Size);
   UartFlush( UART_MODEM_CHANNEL);
   UartSend( UART_MODEM_CHANNEL, String, Size);
} // UartModemSend

//------------------------------------------------------------------------------
//   Receive
//------------------------------------------------------------------------------

TYesNo UartModemReceive( int Size)
// Receive buffer with <Size> (may be zero - receive up to timeout)
{
int   RxSize;
char *RxBuffer;

   RxSize   = UartReceiveSize( UART_MODEM_CHANNEL);
   RxBuffer = UartReceiveBuffer( UART_MODEM_CHANNEL);
   MDUMP( RxBuffer, RxSize);
   RxBuffer[ RxSize] = '\0';           // add zero terminator
   if( Size != 0){
      if( RxSize < Size){
         return( NO);
      }
      RxBuffer[ Size] = '\0';          // truncate to requested size
   }
   return( YES);
} // UartModemReceive

//------------------------------------------------------------------------------
//   Modem status
//------------------------------------------------------------------------------

TUartModemStatus UartModemStatus( void)
// Check for last operation status
{
   switch( UartReceiveStatus( UART_MODEM_CHANNEL)){
      case UART_RECEIVE_ACTIVE :
         return( UART_MODEM_RECEIVE_ACTIVE);    // continue with receiving

      case UART_RECEIVE_REPLY_TIMEOUT :
      case UART_RECEIVE_FRAMING_ERROR :
      case UART_RECEIVE_OVERRUN_ERROR :
      case UART_RECEIVE_SIZE_ERROR :
         MREPORT( "Rx error");
         MDUMP( UartReceiveBuffer( UART_MODEM_CHANNEL), UartReceiveSize( UART_MODEM_CHANNEL));
         return( UART_MODEM_RECEIVE_ERROR);      // receive error

      case UART_RECEIVE_TIMEOUT :
         MREPORT( "Rx ok");
         return( UART_MODEM_RECEIVE_DONE);       // receive O.K.

      case UART_SEND_ACTIVE :
         return( UART_MODEM_SEND_ACTIVE);

      case UART_SEND_DONE :
         return( UART_MODEM_SEND_DONE);

      default :
         // unexpected status
         MREPORT( "Status error");
         return( UART_MODEM_RECEIVE_ERROR);     // receive error
    }
} // UartModemStatus

#ifdef UART_MODEM_MONITOR
//------------------------------------------------------------------------------
//   Monitor
//------------------------------------------------------------------------------

static void _MonitorDump( char *Buffer, int Size)
// Send <Buffer> with <Size> to monitor port
{
   UartSend( UART_MODEM_MONITOR, Buffer, Size);
   while( UartSendStatus( UART_MODEM_MONITOR) != UART_SEND_DONE);
} // _MonitorDump

static void _MonitorReport( const char *Message)
// Send <Message> to monitor port
{
   strncpyx( _MonitorBuffer, Message, MONITOR_BUFFER_SIZE);
   UartSend( UART_MODEM_MONITOR, _MonitorBuffer, strlen( _MonitorBuffer));
   while( UartSendStatus( UART_MODEM_MONITOR) != UART_SEND_DONE);
} // _MonitorReport

#endif // UART_MODEM_MONITOR
