//*****************************************************************************
//
//    Uart.h       UART communication services
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Uart_H__
   #define __Uart_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __UartDef_H__
   #include "Uart/UartDef.h"
#endif

#ifndef __UartStatus_H__
   #include "Uart/UartStatus.h"
#endif

// UART device address :
typedef enum {
   UART_UART0,
   UART_UART1,
   UART_UART2,
   UART_UART3,
   UART_UART4,
   UART_UART5,
   UART_UART6,
   UART_UART7,
   UART_UART8,
   UART_UART9,
   _UART_LAST
} EUartAddress;

 // UART operating mode :
typedef enum {
   UART_MODE_BINARY_MASTER,       // binary mode master side
   UART_MODE_BINARY_SLAVE,        // binary mode slave side
   UART_MODE_ASCII_MASTER,        // ASCII framing mode master side
   UART_MODE_ASCII_SLAVE,         // ASCII framing mode slave size
   UART_MODE_NATIVE_MASTER,       // native framing protocol master side
   UART_MODE_NATIVE_SLAVE,        // native framing protocol slave side
   UART_MODE_MEGAVI_MASTER,       // megavi protocol master side
   UART_MODE_MEGAVI_SLAVE,        // megavi protocol slave side
   UART_MODE_DACS_SLAVE,          // Dacs protocol slave side
   UART_MODE_MODBUS,              // Modbus protocol master & slave side
   _UART_MODE_LAST
} EUartMode;

// enumeration types :
typedef byte TUartAddress;
typedef byte TUartFormat;
typedef byte TUartMode;
typedef byte TUartStatus;
typedef word TUartReceiveAddress;

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void UartInit( TUartAddress Address);
// Communication initialisation

void UartDeinit( TUartAddress Uart);
// Communication deinitialisation

void UartSetup( TUartAddress Uart, unsigned Baud, TUartFormat Format);
// Set communication parameters

void UartFramingSet( TUartAddress Address, char LeaderCharacter, char TrailerCharacter);
// Set framing characters based communication

void UartFramingSetTrailerSeq( TUartAddress Uart, char LeaderCharacter, char *TrailerSeq);
// Set framing characters based communication with trailer sequence of N characters(TrailerSeq is '\0' terminated)

void UartFramingSetLeaderSeq( TUartAddress Uart, char* LeaderSeq, char TrailerCharacter);
// Set framing characters based communication with leader sequence of N characters(LeaderSeq is '\0' terminated)

void UartFramingSetLeaderTrailerSeq( TUartAddress Uart, char* LeaderSeq, char *TrailerSeq);
// Set framing characters based communication with leader and trailer sequence of N characters(TrailerSeq and LeaderSeq is '\0' terminated)

void UartBufferSet( TUartAddress Address, void *Buffer, int Size);
// Set receive buffer

void UartBufferMove( TUartAddress Uart, int Offset);
// Move buffer by offset, pre-offset data are thrown

void UartTimeoutSet( TUartAddress Uart, unsigned ReplyTimeout, unsigned IntercharacterTimeout);
// Set timeout based communication

void UartSendDelaySet( TUartAddress Address, unsigned SendDelay);
// Start send after <Delay>

void UartSendAddressSet( TUartAddress Address, TUartReceiveAddress SendAddress);
// Set protocol specific send address field

void UartModeSet( TUartAddress Address, TUartMode Mode);
// Set operating mode

//-----------------------------------------------------------------------------

void UartReceive( TUartAddress Address);
// Start receiveing

TUartStatus UartReceiveStatus( TUartAddress Address);
// Returns receiving status

void *UartReceiveBuffer( TUartAddress Address);
// Returns receive buffer

int UartReceiveSize( TUartAddress Address);
// Returns received data size

TUartReceiveAddress UartReceiveAddress( TUartAddress Address);

//-----------------------------------------------------------------------------

void UartSend( TUartAddress Address, void *Buffer, int Size);
// Start Tx with <Buffer> and <Size>

TUartStatus UartSendStatus( TUartAddress Address);
// Returns send status

//-----------------------------------------------------------------------------

void UartFlush( TUartAddress Address);
// Flush Rx/Tx buffer

void UartStop( TUartAddress Address);
// Stop Rx/Tx discard Rx/Tx data

//-----------------------------------------------------------------------------

void UartTimer( void);
// Join to external 1ms timer tick

//-----------------------------------------------------------------------------
// Direct control
//-----------------------------------------------------------------------------

TYesNo UartTxBusy( TUartAddress Address);
// Returns YES if transmitter is busy

void UartTxChar( TUartAddress Address, byte Char);
// Transmit <Char> byte

//-----------------------------------------------------------------------------

TYesNo UartRxChar( TUartAddress Address, byte *Char);
// Returns YES and received byte on <Char>,
// or NO on timeout

TYesNo UartRxWait( TUartAddress Address, unsigned Timeout);
// Waits for receive up to <Timeout> miliseconds

#endif
