//*****************************************************************************
//
//    Kbx.h        Keyboard matrix services
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Kbx_H__
   #define __Kbx_H__

#ifndef __Hardware_H__
   #include "Hardware.h"               // keycodes
#endif

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void KbxInit( void);
// Initialization

int KbxRead( void);
// Read keyboard matrix

#endif
