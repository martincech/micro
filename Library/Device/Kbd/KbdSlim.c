//*****************************************************************************
//
//    KbdSlim.c    Minimum keyboard services
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Kbd/Kbd.h"
#include "Kbd/Kbx.h"              // Keyboard matrix
#include "System/System.h"        // Operating system
#include "Cpu/Cpu.h"              // WatchDog

static word _LastKey;             // last pressed key

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void KbdInit( void)
// Initialization
{
   _LastKey = K_RELEASED;              // context key (getch)
   KbxInit();
} // KbdInit

//-----------------------------------------------------------------------------
// Test
//-----------------------------------------------------------------------------

TYesNo kbhit( void)
// Returns YES on key hit
{
   _LastKey = KbdGet();
   if( _LastKey == K_RELEASED){
      return( NO);                     // nothing pressed
   }
   return( YES);
} // kbhit

//-----------------------------------------------------------------------------
// Wait key
//-----------------------------------------------------------------------------

int getch( void)
// Waits for key, returns it
{
int Key;

   if( _LastKey == K_RELEASED){
      // wait for press
      while( !kbhit()){
         WatchDog();
      }
   }
   Key = _LastKey;                     // remember
   // wait for release :
   while( KbdGet() != K_RELEASED){
       WatchDog();
   }
   _LastKey = K_RELEASED;              // release
   return( Key);
} // getch

//-----------------------------------------------------------------------------
// Get key
//-----------------------------------------------------------------------------

int KbdGet( void)
// Test for key, returns K_IDLE for no key pressed
{
int Key, Key2;

   Key = KbxRead();
   if( Key == K_RELEASED){
      return( K_RELEASED);             // nothing pressed
   }
   SysDelay( 10);
   Key2 = KbxRead();
   if( Key2 == K_RELEASED){
      return( K_RELEASED);             // try later
   }
   if( Key != Key2){
      return( K_RELEASED);             // try later
   }
   return( Key);                       // valid key
} // KbdGet
