//*****************************************************************************
//
//   Accu.h       Accumulator
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Accu_H__
   #define __Accu_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#define ACCU_CAPACITY_INVALID    ((byte)0xFF)

void AccuInit( void);
// Initialization

void AccuExecute( void);
// Execute

TYesNo AccuReady( void);
// Ready

byte AccuCapacityRemaining( void);
// Remaining capacity %

word AccuVoltage( void);
// Voltage mV

int16 AccuCurrent( void);
// Current 100uA

int8 AccuTemperature( void);
// Temperature �C

#endif
