//*****************************************************************************
//
//   AccuSB.c       Accu Smart Battery
//   Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#include "Accu/Accu.h"
#include "SmartBattery/SmartBattery.h"

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void AccuInit( void)
// Initialization
{
return;
byte ManufacturerName[50];
   SBInit();
} // AccuInit

//-----------------------------------------------------------------------------
// Ready
//-----------------------------------------------------------------------------

TYesNo AccuReady( void)
// Ready
{
   return NO;
} // AccuReady

//-----------------------------------------------------------------------------
// Capacity
//-----------------------------------------------------------------------------

byte AccuCapacityRemaining( void)
// Remaining capacity %
{
   return 0;
word State;
   if(!SBRelativeStateOfCharge( &State)) {
      return ACCU_CAPACITY_INVALID;
   }
   return State;
} // AccuCapacityRemaining

//-----------------------------------------------------------------------------
// Voltage
//-----------------------------------------------------------------------------

word AccuVoltage( void)
// Voltage mV
{
   return 0;
word Voltage;
   if(!SBVoltage( &Voltage)) {
      return 0;
   }
   return Voltage;
} // AccuVoltage

//-----------------------------------------------------------------------------
// Current
//-----------------------------------------------------------------------------

int16 AccuCurrent( void)
// Current mA
{
   return 0;
word Current;
   if(!SBCurrent( &Current)) {
      return 0;
   }
   return Current;
} // AccuCurrent

//-----------------------------------------------------------------------------
// Temperature
//-----------------------------------------------------------------------------

int8 AccuTemperature( void)
// Temperature �C
{

   return 0;
word Temperature;
   if(!SBTemperature( &Temperature)) {
      return 0;
   }
   return (Temperature - 2732) / 10;
} // AccuTemperature