//*****************************************************************************
//
//   Max17047.h       Max17047 fuel gauge
//   Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Max17047_H__
   #define __Max17047_H__
   
#include "Unisys/Uni.h"

#define MAX17047_IIC_ADDRESS  0b0110110

typedef struct {
   TYesNo Ber : 1;
   TYesNo Bei : 1;
   TYesNo Aen : 1;
   TYesNo FTHRM : 1;
   TYesNo ETHRM : 1;
   byte ALSH : 1;
   TYesNo I2CSH : 1;
   TYesNo SHDN : 1;
   TYesNo Tex : 1;
   TYesNo Ten : 1;
   TYesNo AINSH : 1;
   byte ALRTp : 1;
   TYesNo Vs : 1;
   TYesNo Ts : 1;
   TYesNo Ss : 1;
   byte _dummy : 1; 
} __packed TMax17047Config;

typedef struct {
   byte _dummy0 : 1;
   TYesNo POR : 1;
   byte _dummy1 : 1;
   byte Bst : 1;
   byte _dummy2 : 4;
   TYesNo Vmn : 1;
   TYesNo Tmn : 1;
   TYesNo Smn : 1;
   TYesNo Bi : 1;
   TYesNo Vmx : 1;
   TYesNo Tmx : 1;
   TYesNo Smx : 1;
   TYesNo Br : 1;
} __packed TMax17047Status;

typedef struct {
   TMax17047Status Status;
   word VALRT_Threshold;
   word TALRT_Threshold;
   word SALRT_Threshold;
   word AtRate;
   const word RemCapREP;
   const word SOCREP;
   const word Age;
   word Temperature;
   const word VCELL;
   const word Current;
   const word AverageCurrent;
   const word _dummy0;
   const word SOCMIX;
   const word SOCAV;
   const word RemCapMIX;
   word FullCAP;
   const word TTE;
   word QResidual00;
   word FullSOCThr;
   const word _dummy1;
   const word _dummy2;
   word AverageTemperature;
   word Cycles;
   word DesignCap;
   const word AverageVCELL;
   word MaxMinTemperature;
   word MaxMinVCELL;
   word MaxMinCurrent;
   TMax17047Config CONFIG;
   word ICHGTerm;
   const word RemCapAV;
   const word _dummy3;
   const word Version;
   word QResidual10;
   word FullCapNom;
   word TempNom;
   word TempLim;
   const word _dummy4;
   const word AIN;
   word LearnCFG;
   word FilterCFG;
   word RelaxCFG;
   word MiscCFG;
   word TGAIN;
   word TOFF;
   word CGAIN;
   word COFF;
   const word _dummy5;
   const word _dummy6;
   word QResidual20;
   const word _dummy7[3];
   word Iavg_rempty;
   word FCTC;
   word RCOMP0;
   word TempCo;
   word V_empty;
   const word _dummy8[2];
   const word FSTAT;
   const word TIMER;
   word SHDNTIMER;
   const word _dummy9[2];
   word QResidual30;
   const word _dummy10[2];
   word dQacc;
   word dPacc;
   const word _dummy11[0x4C - 0x47 + 1];
   word QH;
   const word _dummy12[0x7F - 0x4E + 1];
   word CharacterizationTable[0xAF - 0x80 + 1];
   const word _dummy13[0xFA - 0xB0 + 1];
   const word VFOCV;
   const word _dummy14[0xFE - 0xFC + 1];
   const word SOCVF;
} __packed TMax17047Descriptor;

#define Max17047AddressOf( Item)     (offsetof(TMax17047Descriptor, Item) / 2)

void Max17047Init( void);
// Initialisation

TYesNo Max17047Read( byte Address, word *Data);
// Read register

TYesNo Max17047Write( byte Address, word Data);
// Write register

TYesNo Max17047ReadMultiple( word *Data, byte Address, byte Count);
// Read <Data> from <Address> of size <Count>, returns data read
   
TYesNo Max17047WriteMultiple( word *Data, byte Address, byte Count);
// Write <Data> from <Address> of size <Count>, returns data written

#endif
