//*****************************************************************************
//
//   max17047Iic.h       Max17047 IIC
//   Version 1.0         (c) Veit Electronics
//
//*****************************************************************************

#ifndef __max17047Iic_H__
   #define __max17047Iic_H__

#include "Iic/Iic.h"
#include "Hardware.h"

#define max17047IicInit()                          IicInit( MAX17047_IIC_CHANNEL)
// Interface initialisation

#define max17047IicStart()                         IicStart( MAX17047_IIC_CHANNEL)
// Sends start sequence

#define max17047IicStop()                          IicStop( MAX17047_IIC_CHANNEL)
// Sends stop sequence

#define max17047IicSend( value)                    IicSend( MAX17047_IIC_CHANNEL, value)
// Sends <value>, returns YES if ACK

#define max17047IicReceive( ack)                   IicReceive( MAX17047_IIC_CHANNEL, ack)
// Returns received byte, sends confirmation <ack>.
// <ack> = YES sends ACK, NO sends NOT ACK

#endif
