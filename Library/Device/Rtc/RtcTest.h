//*****************************************************************************
//
//    RtcTest.h   Real-time clock device test
//    Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __RtcTest_H__
   #define __RtcTest_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

TYesNo RtcTest( void);
// Run RTC test

#endif
