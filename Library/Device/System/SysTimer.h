//-----------------------------------------------------------------------------
//
//    SysTimer.h   Simple system timer
//    Version 1.0  (c) VEIT Electronics
//
//-----------------------------------------------------------------------------

#ifndef __SysTimer_H__
   #define __SysTimer_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

void SysTimerStart( void);
// Start system timer

word SysTimer( void);
// Returns milisecond timer

void SysTimerExecute( void);
// Timer user executive (callback)

#endif
