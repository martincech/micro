//-----------------------------------------------------------------------------
//
//    System.h     Operating system primitives
//    Version 1.0  (c) VEIT Electronics
//
//-----------------------------------------------------------------------------

#ifndef __System_H__
   #define __System_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __Delay_H__
   #include "System/Delay.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif

// Timer count calculations :
#define TimerFastCount( ms)      ((ms) / TIMER_FAST_PERIOD)
#define TimerSlowCount( s)       (((s) * 1000U) / TIMER_SLOW_PERIOD)

//-----------------------------------------------------------------------------
// Application initialization
//-----------------------------------------------------------------------------
void AppInit();

//-----------------------------------------------------------------------------
// Cooperative multitasking
//-----------------------------------------------------------------------------

void SysInit( void);
// Initialization

int SysScheduler( void);
// Scheduler cycle, returns any event

int SysSchedulerEnable( TYesNo Enable);
// Start/Terminate

int SysSchedulerBasic( void);
// Basic scheduler cycle, returns any event

int SysSchedulerDefault( void);
// Default scheduler cycle, returns any event

int SysEventWait( void);
// Scheduler loop, returns nonempty event

//-----------------------------------------------------------------------------
// System keyboard control
//-----------------------------------------------------------------------------
typedef enum{
   SYS_KEY_DEFAULT   = 0,        // allow only arrows + enter/esc
   SYS_KEY_NUMBERS   = 0x0001,   // allow keyboard numbers
   SYS_KEY_LETTER_LC = 0x0002,   // allow keyboard lower case letters (+ space)
   SYS_KEY_LETTER_UC = 0x0004,   // allow keyboard upper case letters (+ space)
   SYS_KEY_LETTERS   = SYS_KEY_LETTER_LC | SYS_KEY_LETTER_UC,
   SYS_KEY_SPECIAL   = 0x0008   // allow special keys such as _#! etc..(+space)
} ESysKeyAscii;

void SysKeyAscii(byte SysKeyMask);
// System keyboard semantics control

//-----------------------------------------------------------------------------
// Start/Terminate
//-----------------------------------------------------------------------------

void SysReboot( void);
// Reboot

void SysBootloader( void);
// Request bootloader

void SysOff( void);
// Request system off

//------------------------------------------------------------------------------
//   Inactivity Timeout
//------------------------------------------------------------------------------

void SysTimeoutDisable( void);
// Disable timeout

void SysTimeoutEnable( void);
// Enable timeout

void SysTimeoutReset( void);
// Reset timeout

//-----------------------------------------------------------------------------
// Flash
//-----------------------------------------------------------------------------

void SysFlashReset( void);
// Restart flashing

//-----------------------------------------------------------------------------
// Alarm
//-----------------------------------------------------------------------------

void SysAlarm( word ms, int Event);
// Send alarm <Event> after <ms>

void SysAlarmDisable( void);
// Deactivate alarm

//-----------------------------------------------------------------------------
// System timer
//-----------------------------------------------------------------------------

word SysTimer( void);
// Returns milisecond timer

dword SysTime( void);
// return second timer

word SysTimerNaked( void);
// Get milisecond timer from interrupt

void SysTimerExecute( void);
// Timer user executive

//-----------------------------------------------------------------------------
// System clock
//-----------------------------------------------------------------------------

dword SysClock( void);
// Returns internal representation of actual date/time

dword SysClockNaked( void);
// Get actual date/time from interrupt

void SysClockSet( dword Clock);
// Set actual date/time to <DateTime> internal representation

//-----------------------------------------------------------------------------

dword SysDateTime( void);
// Returns wall clock date/time

void SysDateTimeSet( dword DateTime);
// Set system clock by wall clock <DateTime>

TYesNo SysDateTimeInvalid( void);
// Has system invalid time?

//-----------------------------------------------------------------------------
// Internal use only
//-----------------------------------------------------------------------------

void _SysTimerDisable( void);
// Stop timer interrupts

void _SysTimerEnable( void);
// Start timer interrupts

//-----------------------------------------------------------------------------
// CPU utility
//-----------------------------------------------------------------------------

void SysException( byte Type, dword Address);
// CPU exception callback template

void SysStop( void);
// CPU halt (endless loop)

void SysIdleEnter( void);
// Enter CPU sleep mode

#ifdef __cplusplus
   }
#endif

#endif
