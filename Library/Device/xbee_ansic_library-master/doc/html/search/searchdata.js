var indexSectionsWithContent =
{
  0: "6_abcdefghijlmnopqrstuvwxz",
  1: "_abcejmprstwxz",
  2: "abcdefhijoprstuwxz",
  3: "_acefhmpstwxz",
  4: "_abcdefghilmnopqrstvwxz",
  5: "beimpsuwxz",
  6: "_wx",
  7: "dsx",
  8: "_cdistxz",
  9: "6abcdfghinorstuwxz",
  10: "dot"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

