//*****************************************************************************
//
//    Bq2425x.h    Bq2425x
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Bq2425x_H__
   #define __Bq2425x_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Register 0x00
typedef struct {
   byte   FAULT    : 4;
   byte   STAT     : 2;
   TYesNo WD_EN    : 1;
   TYesNo WD_FAULT : 1;
} __packed TRegister1;

typedef enum {
   STAT_CHARGE_READY,
   STAT_CHARGE_IN_PROGRESS,
   STAT_CHARGE_DONE,
   STAT_FAULT,
} EStat;

typedef enum {
   FAULT_NORMAL,
   FAULT_INPUT_OVP,
   FAULT_INPUT_UVLO,
   FAULT_SLEEP,
   FAULT_BATTERY_TEMPERATURE,
   FAULT_BATTERY_OVP,
   FAULT_THERMAL_SHUTDOWN,
   FAULT_TIMER,
   FAULT_NO_BATTERY_CONNECTED,
   FAULT_ISER_SHORT,
   FAULT_INPUT_AND_LDO_LOW
} EFault;

// Register 0x01
typedef struct {
   TYesNo HZ_MODE  : 1;
   byte        CE  : 1;
   TYesNo EN_TERM  : 1;
   TYesNo EN_STAT  : 1;
   byte IIN_ILIMIT : 3;
   TYesNo Reset    : 1;
} __packed TRegister2;

typedef enum {
   ILIMIT_USB2_100MA,
   ILIMIT_USB2_150MA,
   ILIMIT_USB2_500MA,
   ILIMIT_USB3_900MA,
   ILIMIT_CHARGER_1500MA,
   ILIMIT_CHARGER_2000MA,
   ILIMIT_EXTERNAL,
   ILIMIT_NO_LIMIT
} EIInLimit;

// Register 0x02
typedef struct {
   byte USB_DET  : 2;
   byte VBATREG  : 6;
} __packed TRegister3;

typedef enum {
   USBDET_DCP,
   USBDET_CDP,
   USBDET_SDP,
   USBDET_APPLE_NON_STANDARD
} EUsbDet;

typedef enum {
   VBATREG_3500MV,
   VBATREG_3520MV,
   VBATREG_3540MV,
   VBATREG_3560MV,
   VBATREG_3580MV,
   VBATREG_3600MV,
   VBATREG_3620MV,
   VBATREG_3640MV,
   VBATREG_3660MV,
   VBATREG_3680MV,
   VBATREG_3700MV,
   VBATREG_3720MV,
   VBATREG_3740MV,
   VBATREG_3760MV,
   VBATREG_3780MV,
   VBATREG_3800MV,
   VBATREG_3820MV,
   VBATREG_3840MV,
   VBATREG_3860MV,
   VBATREG_3880MV,
   VBATREG_3900MV,
   VBATREG_3920MV,
   VBATREG_3940MV,
   VBATREG_3960MV,
   VBATREG_3980MV,
   VBATREG_4000MV,
   VBATREG_4020MV,
   VBATREG_4040MV,
   VBATREG_4060MV,
   VBATREG_4080MV,
   VBATREG_4100MV,
   VBATREG_4120MV,
   VBATREG_4140MV,
   VBATREG_4160MV,
   VBATREG_4180MV,
   VBATREG_4200MV,
   VBATREG_4220MV,
   VBATREG_4240MV,
   VBATREG_4260MV,
   VBATREG_4280MV,
   VBATREG_4300MV,
   VBATREG_4320MV,
   VBATREG_4340MV,
   VBATREG_4360MV,
   VBATREG_4380MV,
   VBATREG_4400MV,
   VBATREG_4420MV,
   VBATREG_4440MV
} EVBatReg;

// Register 0x03
typedef struct {
   byte ITERM : 3;
   byte ICHG :  5;
} __packed TRegister4;

typedef enum {
   ITERM_50MA,
   ITERM_75MA,
   ITERM_100MA,
   ITERM_125MA,
   ITERM_150MA,
   ITERM_175MA,
   ITERM_200MA,
   ITERM_225MA
} EIterm;

typedef enum {
   ICHG_500MA,
   ICHG_550MA,
   ICHG_600MA,
   ICHG_650MA,
   ICHG_700MA,
   ICHG_750MA,
   ICHG_800MA,
   ICHG_850MA,
   ICHG_900MA,
   ICHG_950MA,
   ICHG_1000MA,
   ICHG_1050MA,
   ICHG_1100MA,
   ICHG_1150MA,
   ICHG_1200MA,
   ICHG_1250MA,
   ICHG_1300MA,
   ICHG_1350MA,
   ICHG_1400MA,
   ICHG_1450MA,
   ICHG_1500MA,
   ICHG_1550MA,
   ICHG_1600MA,
   ICHG_1650MA,
   ICHG_1700MA,
   ICHG_1750MA,
   ICHG_180MA,
   ICHG_1850MA,
   ICHG_1900MA,
   ICHG_1950MA,
   ICHG_2000MA
} EIChg;

// Register 0x04
typedef struct {
   byte VINDPM      : 3;
   TYesNo CE_STATUS : 1;
   TYesNo DPDM_EN   : 1;
   TYesNo LOW_CHG   : 1;
   byte LOOP_STATUS : 2;
} __packed TRegister5;

typedef enum {
   VINDPM_0MV,
   VINDPM_80MV,
   VINDPM_160MV,
   VINDPM_240MV,
   VINDPM_320MV,
   VINDPM_400MV,
   VINDPM_480MV,
   VINDPM_560MV,
} EVinDpm;

typedef enum {
   LOOP_STATUS_NO_LOOP,
   LOOP_STATUS_VIN_DPM,
   LOOP_STATUS_INPUT_CURRENT,
   LOOP_STATUS_THERMAL
} ELoopStatus;

// Register 0x05
typedef struct {
   byte TS_STAT          : 3;
   TYesNo TS_EN          : 1;
   TYesNo SYSOFF         : 1;
   byte TMR              : 2;
   TYesNo _2XTMR_EN      : 1;
} __packed TRegister6;

typedef enum {
   TS_STAT_NORMAL,
   TS_STAT_HOT,
   TS_STAT_WARM,
   TS_STAT_COOL,
   TS_STAT_COLD,
   TS_STAT_COLD_FREEZE,
   TS_STAT_FREEZE,
   TS_STAT_OPEN
} ETsStat;

typedef enum {
   TMR_075HOUR,
   TMR_6HOUR,
   TMR_9HOUR,
   TMR_DISABLE,
} ETmr;

// Register 0x06
typedef struct {
   byte dummy           : 2;
   TYesNo FORCE_PTM     : 1;
   TYesNo FORCE_BAT_DET : 1;
   TYesNo CLR_VDP       : 1;
   byte VOVP            : 3;
} __packed TRegister7;

typedef enum {
   VOVP_6V,
   VOVP_6_5V,
   VOVP_7V,
   VOVP_8V,
   VOVP_9V,
   VOVP_9_5V,
   VOVP_10V,
   VOVP_10_5V,
} EVOvp;


typedef struct {
   TRegister1  Register1;
   TRegister2  Register2;
   TRegister3  Register3;
   TRegister4  Register4;
   TRegister5  Register5;
   TRegister6  Register6;
   TRegister7  Register7;
}  __packed TBq2425xMemoryMap;

typedef union {
   TRegister1  Register1;
   TRegister2  Register2;
   TRegister3  Register3;
   TRegister4  Register4;
   TRegister5  Register5;
   TRegister6  Register6;
   TRegister7  Register7;
}  TMemoryMapUnion;

#define Bq2425xAddressOf( Item)    offsetof(TBq2425xMemoryMap, Item)


void Bq2425xInit( void);
// Initialisation

TYesNo Bq2425xReadMultiple( TBq2425xMemoryMap *Data);
// Read <Data>

TYesNo Bq2425xWriteMultiple( TBq2425xMemoryMap *Data);
// Write <Data>

TYesNo Bq2425xWrite(byte Address, byte Data);
// Write <Data> to <Address>

TYesNo Bq2425xRead(byte Address, byte *Data);
// Read <Data> from <Address>

#endif
