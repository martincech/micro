//*****************************************************************************
//
//   bq2425xIic.h         Bq2425x IIC
//   Version 1.0         (c) Veit Electronics
//
//*****************************************************************************

#ifndef __bq2425xIic_H__
   #define __bq2425xIic_H__

#include "Iic/Iic.h"
#include "Hardware.h"

#define bq2425xIicInit()                          IicInit( BQ2425X_IIC_CHANNEL)
// Interface initialisation

#define bq2425xIicStart()                         IicStart( BQ2425X_IIC_CHANNEL)
// Sends start sequence

#define bq2425xIicStop()                          IicStop( BQ2425X_IIC_CHANNEL)
// Sends stop sequence

#define bq2425xIicSend( value)                    IicSend( BQ2425X_IIC_CHANNEL, value)
// Sends <value>, returns YES if ACK

#define bq2425xIicReceive( ack)                   IicReceive( BQ2425X_IIC_CHANNEL, ack)
// Returns received byte, sends confirmation <ack>.
// <ack> = YES sends ACK, NO sends NOT ACK

#endif
