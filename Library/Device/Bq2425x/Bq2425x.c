//*****************************************************************************
//
//    ChagerBq2425x.c    Charger
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Bq2425x.h"
#include "Bq2425xIic.h"

#define BQ2425X_ADDRESS    0x6A

//-----------------------------------------------------------------------------
// Init
//-----------------------------------------------------------------------------

void Bq2425xInit( void)
// Initialisation
{
   Bq2425xPortInit();
   Bq2425xCeDeassert();
   bq2425xIicInit();
} // Bq2425xInit

//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------

TYesNo Bq2425xReadMultiple( TBq2425xMemoryMap *Data)
// Read <Data>
{
byte *DataB = (byte *) Data;
byte Count = sizeof(TBq2425xMemoryMap);
   bq2425xIicStart();
   if(!bq2425xIicSend(IicStartByte(BQ2425X_ADDRESS, IIC_WRITE))) {
      bq2425xIicStop();
      return NO;
   }
   if(!bq2425xIicSend(Bq2425xAddressOf(Register1))) {
      bq2425xIicStop();
      return NO;
   }
   bq2425xIicStart();
   
   if(!bq2425xIicSend(IicStartByte(BQ2425X_ADDRESS, IIC_READ))) {
      bq2425xIicStop();
      return NO;
   }
   while(Count--) {
      if(Count) {
         *DataB++ = bq2425xIicReceive(YES);
      } else {
         *DataB++ = bq2425xIicReceive(NO);
      }
   }   
   bq2425xIicStop();
   return YES;
} // Max17047Read

//-----------------------------------------------------------------------------
// Write
//-----------------------------------------------------------------------------

TYesNo Bq2425xWriteMultiple( TBq2425xMemoryMap *Data)
// Write <Data>
{
   byte *DataB = (byte *) Data;
   byte Count = sizeof(TBq2425xMemoryMap);
   bq2425xIicStart();
   if(!bq2425xIicSend(IicStartByte(BQ2425X_ADDRESS, IIC_WRITE))) {
      bq2425xIicStop();
      return NO;
   }
   if(!bq2425xIicSend(Bq2425xAddressOf(Register1))) {
      bq2425xIicStop();
      return NO;
   }
   while(Count--) {
      if(!bq2425xIicSend(*DataB++)) {
         bq2425xIicStop();
         return NO;
      }
   }
   bq2425xIicStop();
   return YES;
} // Bq2425xWrite

//-----------------------------------------------------------------------------
// Write
//-----------------------------------------------------------------------------

TYesNo Bq2425xWrite(byte Address, byte Data)
// Write <Data> to <Address>
{
   bq2425xIicStart();
   if(!bq2425xIicSend(IicStartByte(BQ2425X_ADDRESS, IIC_WRITE))) {
      bq2425xIicStop();
      return NO;
   }
   if(!bq2425xIicSend(Address)) {
      bq2425xIicStop();
      return NO;
   }
   if(!bq2425xIicSend(Data)) {
      bq2425xIicStop();
      return NO;
   }
   bq2425xIicStop();
   return YES;
} // Write

//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------

TYesNo Bq2425xRead(byte Address, byte *Data)
// Read <Data> from <Address>
{
   bq2425xIicStart();
   if(!bq2425xIicSend(IicStartByte(BQ2425X_ADDRESS, IIC_WRITE))) {
      bq2425xIicStop();
      return NO;
   }
   if(!bq2425xIicSend(Address)) {
      bq2425xIicStop();
      return NO;
   }
   bq2425xIicStart();
   if(!bq2425xIicSend(IicStartByte(BQ2425X_ADDRESS, IIC_READ))) {
      bq2425xIicStop();
      return NO;
   }
   *Data = bq2425xIicReceive(NO);
   bq2425xIicStop();
   return YES;
} // Read