//*****************************************************************************
//
//    ChagerBq2425x.c    Charger with JEITA conditioning
//    Version 1.0        (c) VEIT Electronics
//
//*****************************************************************************

#include "Charger/Charger.h"
#include "Cpu/Cpu.h"
#include "Bq2425x.h"
#include <stddef.h>

typedef enum {
   OFF,
   INIT,
   WAITING_DETECTION,
   STARTING_CHARGE,
   CHARGING
} EState;

typedef struct {
   word Voltage;
   word Current;
   int8 Temperature;
} TJeitaPoint;

#define INVALID                     ((byte)(-1))

#define JEITA_CURVE_POINT_COUNT     (sizeof(JeitaCurve)/sizeof(JeitaCurve[0]))

const TJeitaPoint JeitaCurve[] = {
   { VBATREG_4200MV,     INVALID ,   0 },
   { VBATREG_4200MV,  ICHG_750MA ,  10 },
   { VBATREG_4200MV, ICHG_1500MA ,  45 },
   { VBATREG_4060MV, ICHG_1500MA ,  60 },
   { VBATREG_4060MV,     INVALID ,  128 }
};

static byte State;
static byte StateInt;
static TYesNo VinChanged;
static byte ChargerType;
static byte JeitaPoint;
static byte LastJeitaPoint;

Bq2425xIrqHandler();
// Vin IRQ handler

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void ChargerInit( void)
// Initialisation
{
byte Value;
TMemoryMapUnion *Reg = (TMemoryMapUnion *)&Value;
   VinChanged = YES;
   Bq2425xInit();
   Reg->Register2.Reset = NO;
   Reg->Register2.IIN_ILIMIT = ILIMIT_CHARGER_1500MA;
   Reg->Register2.CE = YES;
   Reg->Register2.HZ_MODE = NO;
   Bq2425xWrite( Bq2425xAddressOf(Register2), Value);
   Reg->Register1.WD_EN = NO;
   Bq2425xWrite( Bq2425xAddressOf(Register1), Value);
   Bq2425xIrqInit();
   Bq2425xIrqEnable();
} // ChargerInit

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

void ChargerExecute( void) {
byte Value;
TMemoryMapUnion *Reg = (TMemoryMapUnion *) &Value;
TYesNo _VinChanged;
TYesNo _Vin;

   InterruptDisable();
   _VinChanged = VinChanged;
   VinChanged = NO;
   InterruptEnable();
   
   if(_VinChanged) {
      State = CHARGER_STATUS_OFF;
      ChargerType = INVALID;
      _Vin = !Bq2425xPg();
      Bq2425xCeDeassert();
      if(_Vin) {
         StateInt = INIT;
      } else {
         StateInt = OFF;
      }
   }
   
   switch(StateInt) {
      case OFF:
         break;
         
      case INIT:
         Reg->Register2.Reset = YES;
         Reg->Register2.IIN_ILIMIT = ILIMIT_USB2_100MA;
         Reg->Register2.CE = YES;
         Reg->Register2.HZ_MODE = NO;
         if(!Bq2425xWrite( Bq2425xAddressOf(Register2), Value)) {
            break;
         }
         // Read charger fault; after charger connection it is sometimes set to Input UVLO
         if(!Bq2425xRead( Bq2425xAddressOf(Register1), &Value)) {
            break;
         }        
         Reg->Register1.WD_EN = NO;
         if(!Bq2425xWrite( Bq2425xAddressOf(Register1), Value)) {
            break;
         }
         if(ChargerType != INVALID) { // Charger already detected
            Reg->Register5.DPDM_EN = NO;
            StateInt = STARTING_CHARGE;
         } else {
            Reg->Register5.DPDM_EN = YES;
            StateInt = WAITING_DETECTION;
         }
         Reg->Register5.LOW_CHG = NO;
         Reg->Register5.VINDPM = 0;
         if(!Bq2425xWrite( Bq2425xAddressOf(Register5), Value)) {
            break;
         }
         break;

      case WAITING_DETECTION:
         /*if(!Bq2425xRead( Bq2425xAddressOf(Register5), &Value)) {
            break;
         }
         if(Reg->Register5.DPDM_EN) { // detection still in progress
            break;
         }
         if(!Bq2425xRead( Bq2425xAddressOf(Register3), &Value)) {
            break;
         }
         ChargerType = Reg->Register3.USB_DET;*/
         
     case STARTING_CHARGE:
         /*switch(ChargerType) {
            case USBDET_DCP:
               Reg->Register2.IIN_ILIMIT = ILIMIT_CHARGER_1500MA;
               break;
         
            case USBDET_CDP:
               Reg->Register2.IIN_ILIMIT = ILIMIT_CHARGER_1500MA;
               break;
         
            case USBDET_SDP:
               Reg->Register2.IIN_ILIMIT = ILIMIT_USB2_100MA;
               break;
         
            default:*/
               Reg->Register2.IIN_ILIMIT = ILIMIT_USB3_900MA;
               /*break;
         }*/
         Reg->Register2.Reset = NO;
         Reg->Register2.CE = NO;
         Reg->Register2.HZ_MODE = NO;
         Reg->Register2.EN_TERM = YES;
         if(!Bq2425xWrite( Bq2425xAddressOf(Register2), Value)) {
            break;
         }
         Reg->Register6._2XTMR_EN = YES;
         Reg->Register6.TMR = TMR_DISABLE;
         Reg->Register6.SYSOFF = NO;
         Reg->Register6.TS_EN = NO;
         if(!Bq2425xWrite( Bq2425xAddressOf(Register6), Value)) {
            break;
         }
         LastJeitaPoint = JEITA_CURVE_POINT_COUNT;
         JeitaPoint = 0;
         StateInt = CHARGING;
         break;
         
      case CHARGING:
         // Do JEITA
         if(LastJeitaPoint != JeitaPoint) { // update charging current & voltage
            if(JeitaCurve[JeitaPoint].Current == INVALID) {
               State = CHARGER_STATUS_ERROR;
               Bq2425xCeDeassert();
            } else {
               Reg->Register4.ICHG = JeitaCurve[JeitaPoint].Current;
               Reg->Register4.ITERM = ITERM_50MA;
               if(!Bq2425xWrite( Bq2425xAddressOf(Register4), Value)) {
                  break;
               }
               Reg->Register3.VBATREG = JeitaCurve[JeitaPoint].Voltage;
               if(!Bq2425xWrite( Bq2425xAddressOf(Register3), Value)) {
                  break;
               }
               Bq2425xCeAssert();
            }
            LastJeitaPoint = JeitaPoint;
         }

         if(!Bq2425xRead( Bq2425xAddressOf(Register1), &Value)) {
            break;
         }

         if(Reg->Register1.FAULT) {
            StateInt = INIT;
            State = CHARGER_STATUS_ERROR;
            break;
         }
         
         switch(Reg->Register1.STAT) {
            case STAT_CHARGE_IN_PROGRESS:
               State = CHARGER_STATUS_CHARGING;
               break;
               
            case STAT_CHARGE_DONE:
               State = CHARGER_STATUS_DONE;
               break;
            
            case STAT_CHARGE_READY:
               StateInt = INIT;
               break;
               
            default:
               State = CHARGER_STATUS_ERROR;
               StateInt = INIT;
               break;
         }
         break;
   }
} // ChargerExecute

//-----------------------------------------------------------------------------
// Temperature
//-----------------------------------------------------------------------------

void ChargerTemperatureSet( int8 Temperature)
// Set battery temperature
{
   for(JeitaPoint = 0 ; JeitaPoint < JEITA_CURVE_POINT_COUNT - 1 ; JeitaPoint++) {
      if(Temperature < JeitaCurve[JeitaPoint].Temperature) {
         break;
      }
   }
} // ChargerTemperatureSet

//-----------------------------------------------------------------------------
// Status
//-----------------------------------------------------------------------------

byte ChargerStatus( void)
// Charger status
{
   return State; 
} // ChargerStatus

//-----------------------------------------------------------------------------
// Vin handler
//-----------------------------------------------------------------------------

Bq2425xIrqHandler()
// Vin IRQ handler
{
   Bq2425xIrqClear();
   VinChanged = YES;
   ChargerInputEvent();
}