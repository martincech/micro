//*****************************************************************************
//
//    Timer.h      Timer calculations
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Timer_H__
   #define __Timer_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

typedef word TShortTimer;

// Timer (word) operations :
#define TimerAfter(    Actual, Requested)   ((int16)((int16)(Requested) - (int16)(Actual))    <  0)
#define TimerBefore(   Actual, Requested)   ((int16)((int16)(Actual)    - (int16)(Requested)) <  0)
#define TimerAfterEq(  Actual, Requested)   ((int16)((int16)(Actual)    - (int16)(Requested)) >= 0)
#define TimerBeforeEq( Actual, Requested)   ((int16)((int16)(Requested) - (int16)(Actual))    >= 0)

typedef dword TTimer;

// Time (dword) operations :
#define TimeAfter(     Actual, Requested)   ((int32)((int32)(Requested)  - (int32)(Actual))     <  0)
#define TimeBefore(    Actual, Requested)   ((int32)((int32)(Actual)     - (int32)(Requested))  <  0)
#define TimeAfterEq(   Actual, Requested)   ((int32)((int32)(Actual)     - (int32)(Requested))  >= 0)
#define TimeBeforeEq(  Actual, Requested)   ((int32)((int32)(Requested)  - (int32)(Actual))     >= 0)

#endif
