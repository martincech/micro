//*****************************************************************************
//
//    Adp5062.c    Linear Li-ion battery charger
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Adp5062.h"
#include "Adp5062Iic.h"
#include <stddef.h>

#define ADP5062_ADDRESS    0x14

// Register 0x00
typedef struct {
   byte Model : 4;
   byte Manufacturer : 4;
} TManufacturerModelId;

// Register 0x01
typedef struct {
   byte dummy : 4;
   byte Revision : 4;
} TSiliconRevision;

typedef enum {
   ILIM_100_MA,
   ILIM_150_MA,
   ILIM_200_MA,
   ILIM_250_MA,
   ILIM_300_MA,
   ILIM_400_MA,
   ILIM_500_MA,
   ILIM_600_MA,
   ILIM_700_MA,
   ILIM_800_MA,
   ILIM_900_MA,
   ILIM_1000_MA,
   ILIM_1200_MA,
   ILIM_1500_MA,
   ILIM_1800_MA,
   ILIM_2100_MA
} EIlim;

// Register 0x02
typedef struct {
   byte ILim : 4;
   byte reserved : 1;
   byte dummy : 3;
} TVinxPinSettings;

typedef enum {
   CHARGING_VTG_LIMIT_32_V,
   CHARGING_VTG_LIMIT_34_V,
   CHARGING_VTG_LIMIT_37_V,
   CHARGING_VTG_LIMIT_38_V,
} EChargingVtgLimit;

typedef enum {
   CHARGING_VTG_360_V,
   CHARGING_VTG_362_V,
   CHARGING_VTG_364_V,
   CHARGING_VTG_366_V,
   CHARGING_VTG_368_V,
   CHARGING_VTG_370_V,
   CHARGING_VTG_372_V,
   CHARGING_VTG_374_V,
   CHARGING_VTG_376_V,
   CHARGING_VTG_378_V,
   CHARGING_VTG_380_V,
   CHARGING_VTG_382_V,
   CHARGING_VTG_384_V,
   CHARGING_VTG_386_V,
   CHARGING_VTG_388_V,
   CHARGING_VTG_390_V,
   CHARGING_VTG_392_V,
   CHARGING_VTG_394_V,
   CHARGING_VTG_396_V,
   CHARGING_VTG_398_V,
   CHARGING_VTG_400_V,
   CHARGING_VTG_402_V,
   CHARGING_VTG_404_V,
   CHARGING_VTG_406_V,
   CHARGING_VTG_408_V,
   CHARGING_VTG_410_V,
   CHARGING_VTG_412_V,
   CHARGING_VTG_414_V,
   CHARGING_VTG_416_V,
   CHARGING_VTG_418_V,
   CHARGING_VTG_420_V,
   CHARGING_VTG_422_V,
   CHARGING_VTG_424_V,
   CHARGING_VTG_426_V,
   CHARGING_VTG_428_V,
   CHARGING_VTG_430_V,
   CHARGING_VTG_432_V,
   CHARGING_VTG_434_V,
   CHARGING_VTG_436_V,
   CHARGING_VTG_438_V,
   CHARGING_VTG_440_V,
   CHARGING_VTG_442_V,
   CHARGING_VTG_444_V,
   CHARGING_VTG_444_V_1,
   CHARGING_VTG_446_V,
   CHARGING_VTG_448_V,
   CHARGING_VTG_450_V
} EChargingVtg;

// Register 0x03
typedef struct {
   byte ChargingVtgLimit : 2;
   byte ChargingVtg : 6;
} TTerminationSettings;

typedef enum {
   TRICKLE_CHARGING_CURRENT_5_MA,
   TRICKLE_CHARGING_CURRENT_10_MA,
   TRICKLE_CHARGING_CURRENT_20_MA,
   TRICKLE_CHARGING_CURRENT_80_MA
} ETrickleChargingCurrent;

typedef enum {
   CHARGING_CURRENT_50_MA,
   CHARGING_CURRENT_100_MA,
   CHARGING_CURRENT_150_MA,
   CHARGING_CURRENT_200_MA,
   CHARGING_CURRENT_250_MA,
   CHARGING_CURRENT_300_MA,
   CHARGING_CURRENT_350_MA,
   CHARGING_CURRENT_400_MA,
   CHARGING_CURRENT_450_MA,
   CHARGING_CURRENT_500_MA,
   CHARGING_CURRENT_550_MA,
   CHARGING_CURRENT_600_MA,
   CHARGING_CURRENT_650_MA,
   CHARGING_CURRENT_700_MA,
   CHARGING_CURRENT_750_MA,
   CHARGING_CURRENT_800_MA,
   CHARGING_CURRENT_850_MA,
   CHARGING_CURRENT_900_MA,
   CHARGING_CURRENT_950_MA,
   CHARGING_CURRENT_1000_MA,
   CHARGING_CURRENT_1050_MA,
   CHARGING_CURRENT_1100_MA,
   CHARGING_CURRENT_1200_MA,
   CHARGING_CURRENT_1300_MA
} EChargingCurrent;

// Register 0x04
typedef struct {
   byte TrickleChargingCurrent : 2;
   byte ChargingCurrent : 5;
   byte dummy : 1;
} TChargingCurrentSettings;

typedef enum {
   WEAK_VTG_27_V,
   WEAK_VTG_28_V,   
   WEAK_VTG_29_V,
   WEAK_VTG_30_V,
   WEAK_VTG_31_V,
   WEAK_VTG_32_V,
   WEAK_VTG_33_V,
   WEAK_VTG_34_V
} EWeakVtg;

typedef enum {
   TRICKLE_VTG_20_V,
   TRICKLE_VTG_25_V,   
   TRICKLE_VTG_26_V,
   TRICKLE_VTG_29_V
} ETrickleVtg;

typedef enum {
   RECHARGE_80_MV,
   RECHARGE_140_MV,   
   RECHARGE_200_MV,
   RECHARGE_260_MV
} ERechargeVtg;

typedef enum {
   RECHAGE_ENABLE,
   RECHAGE_DISABLE
} ERechargeDisable;

// Register 0x05
typedef struct {
   byte WeakVtg : 3;
   byte TrickleVtg : 2;
   byte RechargeVtg : 2;
   byte RechargeDisable : 1;
} TVoltageThresholds;

typedef enum {
   RESET_WATCH_DOG_0,
   RESET_WATCH_DOG_1
} EResetWatchDog;

typedef enum {
   PERIOD_WATCH_DOG_32_SEC,
   PERIOD_WATCH_DOG_64_SEC
} EPeriodWatchDog;

typedef enum {
   EN_WATCH_DOG_DISABLED,
   EN_WATCH_DOG_ENAABLED
} EEnWatchDog;

typedef enum {
   CHARGE_TIMER_PERIOD_30_SEC,
   CHARGE_TIMER_PERIOD_60_SEC
} EChargeTimerperiod;

typedef enum {
   ENABLE_CHARGE_TIMER_DISABLED,
   ENABLE_CHARGE_TIMER_ENABLED
} EEnChargeTimer;

typedef enum {
   ENABLE_CHARGE_COMPLETE_TIMER_DISABLED,
   ENABLE_CHARGE_COMPLETE_TIMER_ENABLED
} EEnChargeCompleteTimer;

// Register 0x06
typedef struct {
   byte ResetWatchDog : 1;
   byte PeriodWatchDog : 1;
   byte EnWatchDog : 1;
   byte ChargeTimerperiod : 1;
   byte EnChargeTimer : 1;
   byte EnChargeCompleteTimer : 1;
   byte dummy : 2;
} TTimerSettings;

typedef enum {
   ENABLE_CHARGING_DISABLED,
   ENABLE_CHARGING_ENABLED
} EEnCharging;

typedef enum {
   ENABLE_END_OF_CHARGE_DISABLED,
   ENABLE_END_OF_CHARGE_ENABLED
} EEnEndOfCharge;

typedef enum {
   DISABLE_LDO_ENABLED,
   DISABLE_LDO_DISABLED
} EDisableLdo;

typedef enum {
   ENABLE_THERM_DISABLED,
   ENABLE_THERM_ENABLED
} EEnableThr;

typedef enum {
   ENABLE_BATTERY_MONITOR_DISABLED,
   ENABLE_BATTERY_MONITOR_ENABLED
} EEnableBatteryMonitor;

typedef enum {
   DISABLE_ADP5062_NORMAL_OPERATION,
   DISABLE_ADP5062_DISABLED
} EDisableAdp5062;

// Register 0x07
typedef struct {
   byte EnCharging : 1;
   byte dummy0 : 1;
   byte EnEndOfCharge : 1;
   byte DisableLdo : 1;
   byte EnableTherm : 1;
   byte EnableBatteryMonitor : 1;
   byte DisableAdp5062 : 1;
   byte dummy1 : 1;
} TFunctionalSettings1;

typedef enum {
   SYSTEM_VTG_43_V,
   SYSTEM_VTG_44_V,
   SYSTEM_VTG_45_V,
   SYSTEM_VTG_46_V,
   SYSTEM_VTG_47_V,
   SYSTEM_VTG_48_V,
   SYSTEM_VTG_49_V,
   SYSTEM_VTG_50_V
} ESystemVtg;

typedef enum {
   IDEAL_DIODE_ENABLED_CONSTANTLY_WHEN_VSYS_IS_LOWER_THAN_VBAT,
   IDEAL_DIODE_ENABLED_WHEN_VSYS_IS_LOWER_THAN_VBAT_AND_WHEN_VBAT_IS_HIGHER_THAN_WEAK_VTG,
   IDEAL_DIODE_DISABLED,
   IDEAL_DIODE_DISABLED_1
} EIdealDiode;

typedef enum {
   ENABLE_CHARGING_VTG_LIMIT_DISABLED,
   ENABLE_CHARGING_VTG_LIMIT_ENABLED
} EEnChargingVtgLimit;

typedef enum {
   JEITA_SELECT_JEITA_1_SELECTED,
   JEITA_SELECT_JEITA_2_SELECTED
} EJeitaSelect;


typedef enum {
   ENABLE_JEITA_COMPLIANCE_DISABLED,
   ENABLE_JEITA_COMPLIANCE_ENABLED
} EEnJeita;

// Register 0x08
typedef struct {
   byte SystemVtg : 3;
   byte IdealDiode : 2;
   byte EnChargingVtgLimit : 1;
   byte JeitaSelect : 1;
   byte EnJeita : 1;
} TFunctionalSettings2;

typedef enum {
   ENABLE_VIN_INTERRUPT_DISABLED,
   ENABLE_VIN_INTERRUPT_ENABLED
} EEnVinInterrupt;

typedef enum {
   ENABLE_CHARGE_INTERRUPT_DISABLED,
   ENABLE_CHARGE_INTERRUPT_ENABLED
} EEnChargeInterrupt;

typedef enum {
   ENABLE_BATTERY_INTERRUPT_DISABLED,
   ENABLE_BATTERY_INTERRUPT_ENABLED
} EEnBatteryInterrupt;

typedef enum {
   ENABLE_THERM_INTERRUPT_DISABLED,
   ENABLE_THERM_INTERRUPT_ENABLED
} EEnThermInterrupt;

typedef enum {
   ENABLE_OVER_TEMPERATURE_INTERRUPT_DISABLED,
   ENABLE_OVER_TEMPERATURE_INTERRUPT_ENABLED
} EEnOverTemperatureInterrupt;

typedef enum {
   ENABLE_WATCH_DOG_INTERRUPT_DISABLED,
   ENABLE_WATCH_DOG_INTERRUPT_ENABLED
} EEnWatchDogInterrupt;

typedef enum {
   ENABLE_THERMAL_LIMIT_INTERRUPT_DISABLED,
   ENABLE_THERMAL_LIMIT_INTERRUPT_ENABLED
} EEnThermLimitInterrupt;

// Register 0x09
typedef struct {
   byte EnVinInterrupt : 1;
   byte EnChargeInterrupt : 1;
   byte EnBatteryInterrupt : 1;
   byte EnThermInterrupt : 1;
   byte EnOverTemperatureInterrupt : 1;
   byte EnWatchDogInterrupt : 1;
   byte EnThermLimitInterrupt : 1;
   byte dummy : 1;
} TInterruptEnable;

// Register 0x0A
typedef struct {
   byte VinInterrupt : 1;
   byte ChargeInterrupt : 1;
   byte BatteryInterrupt : 1;
   byte ThermInterrupt : 1;
   byte OverTemperatureInterrupt : 1;
   byte WatchDogInterrupt : 1;
   byte ThermLimitInterrupt : 1;
   byte dummy : 1;
} TInterruptActive;

typedef enum {
   CHARGER_STATUS_OFF,
   CHARGER_STATUS_TRICKLE_CHARGE,
   CHARGER_STATUS_FAST_CHARGE_CC_MODE,
   CHARGER_STATUS_FAST_CHARGE_CV_MODE,
   CHARGER_STATUS_CHARGE_COMPLETE,
   CHARGER_STATUS_LDO_MODE,
   CHARGER_STATUS_TRICKLE_OR_FAST_CHARGE_PERIOD_EXPIRED,
   CHARGER_STATUS_BATTERY_DETECTION
} EChargerStatus;

// Register 0x0B
typedef struct {
   byte ChargerStatus : 3;
   byte ChargeDone : 1;
   byte ThermLimited : 1;
   byte VinIlimited : 1;
   byte VinOk : 1;
   byte VinOverVtg : 1;
} TChargerStatus1;

typedef enum {
   BATTERY_STATUS_BATTERY_MONITOR_OFF,
   BATTERY_STATUS_NO_BATTERY,
   BATTERY_STATUS_VBAT_IS_LOWER_THAN_TRICKLE_VTG,
   BATTERY_STATUS_TRICKLE_VTG_IS_LOWER_THAN_VBAT_AND_VBAT_IS_LOWER_THAN_WEAK_VTG,
   BATTERY_STATUS_VBAT_IS_HIGHER_THAN_WEAK_WTG
} EBatteryStatus;

typedef enum {
   RECHARGE_LIMIT_INFORMATION_VBAT_IS_HIGHER_THAN_TRICKLE_VTG,
   RECHARGE_LIMIT_INFORMATION_VBAT_IS_LOWER_THAN_TRICKLE_VTG
} ERechargeLimitInformation;

typedef enum {
   THERM_STATUS_OFF,
   THERM_STATUS_BATTERY_COLD,
   THERM_STATUS_BATTERY_COOL,
   THERM_STATUS_BATTERY_WARM,
   THERM_STATUS_BATTERY_HOT,
   THERM_STATUS_THERMISTOR_OK = 7
} EThermStatus;

// Register 0x0C
typedef struct {
   byte BatteryStatus : 3;
   byte RechargeLimitInformation : 1;
   byte dummy : 1;
   byte ThermStatus :3;
} TChargerStatus2;

// Register 0x0D
typedef struct {
   byte OverTemperature140DegC : 1;
   byte OverTemperature130DegC : 1;
   byte dummy0 : 1;
   byte BatteryShort : 1;
   byte dummy1 : 4;
} TFault;

typedef enum {
   BATTERY_SHORT_THERSHOLD_VTG_20_V,
   BATTERY_SHORT_THERSHOLD_VTG_21_V,
   BATTERY_SHORT_THERSHOLD_VTG_22_V,
   BATTERY_SHORT_THERSHOLD_VTG_23_V,
   BATTERY_SHORT_THERSHOLD_VTG_24_V,
   BATTERY_SHORT_THERSHOLD_VTG_25_V,
   BATTERY_SHORT_THERSHOLD_VTG_26_V,
   BATTERY_SHORT_THERSHOLD_VTG_27_V,
} EBatteryShortThresholdVtg;

typedef enum {
   BATTERY_SHORT_TIMEOUT_TIMER_1_SEC,
   BATTERY_SHORT_TIMEOUT_TIMER_2_SEC,
   BATTERY_SHORT_TIMEOUT_TIMER_4_SEC,
   BATTERY_SHORT_TIMEOUT_TIMER_10_SEC,
   BATTERY_SHORT_TIMEOUT_TIMER_30_SEC,
   BATTERY_SHORT_TIMEOUT_TIMER_60_SEC,
   BATTERY_SHORT_TIMEOUT_TIMER_120_SEC,
   BATTERY_SHORT_TIMEOUT_TIMER_180_SEC
} EBatteryShortTimeoutImer;

// Register 0x10
typedef struct {
   byte BatteryShortThresholdVtg : 3;
   byte dummy : 2;
   byte BatteryShortTimeoutImer : 3;
} TBatteryShort;

typedef enum {
   SYTSEM_ENABLE_PIN_SET_IS_ACTIVATED_WHEN_LDO_IS_ACTIVE_AND_VSYS_IS_AVAILABLE,
   SYTSEM_ENABLE_PIN_SET_IS_ACTIVTED_BY_ISO_Bx_BATTERY_CHARGING_MODE,
   SYTSEM_ENABLE_PIN_SET_IS_ACTIVTED_AND_ISOLATION_FET_IS_DISABLED_WHEN_VBAT_IS_LOWER_THAN_WEAK_VTG,
   SYTSEM_ENABLE_PIN_SET_IS_ACTIVE_IN_LDO_MODE_WHEN_CHARGER_IS_DISABLED
} ESystemEnablePinSet;

typedef enum {
   C5_END_OF_CHARGE_CURRENT_DISABLED,
   C5_END_OF_CHARGE_CURRENT_IS_1_5_CHARGING_CURRENT
} EC5EndOfChargeCurrent;
 

typedef enum {
   C10_END_OF_CHARGE_CURRENT_DISABLED,
   C10_END_OF_CHARGE_CURRENT_IS_1_5_CHARGING_CURRENT
} EC10EndOfChargeCurrent;

typedef enum {
   C20_END_OF_CHARGE_CURRENT_DISABLED,
   C20_END_OF_CHARGE_CURRENT_IS_1_5_CHARGING_CURRENT
} EC20EndOfChargeCurrent;

typedef enum {
   TERMINATION_CURRENT_12_5_MA,
   TERMINATION_CURRENT_32_5_MA,
   TERMINATION_CURRENT_52_5_MA,
   TERMINATION_CURRENT_72_5_MA,
   TERMINATION_CURRENT_92_5_MA,
   TERMINATION_CURRENT_117_5_MA,
   TERMINATION_CURRENT_142_5_MA,
   TERMINATION_CURRENT_170_MA
} ETerminationCurrent;

// Register 0x11
typedef struct {
   byte SystemEnablePinSet : 2;
   byte C5EndOfChargeCurrent : 1;
   byte C10EndOfChargeCurrent : 1;
   byte C20EndOfChargeCurrent : 1;
   byte TerminationCurrent : 3;
} TIend;

typedef struct {
   TManufacturerModelId       ManufacturerModelId;       // 0x00
   TSiliconRevision           SiliconRevision;           // 0x01
   TVinxPinSettings           VinxPinSettings;           // 0x02
   TTerminationSettings       TerminationSettings;       // 0x03
   TChargingCurrentSettings   ChargingCurrentSettings;   // 0x04
   TVoltageThresholds         VoltageThresholds;         // 0x05
   TTimerSettings             TimerSettings;             // 0x06
   TFunctionalSettings1       FunctionalSettings1;       // 0x07
   TFunctionalSettings2       FunctionalSettings2;       // 0x08
   TInterruptEnable           InterruptEnable;           // 0x09
   TInterruptActive            InterruptActive;          // 0x0A
   TChargerStatus1            ChargerStatus1;            // 0x0B
   TChargerStatus2            ChargerStatus2;            // 0x0C
   TFault                     Fault;                     // 0x0D
   TBatteryShort              BatteryShort;              // 0x0E
   TIend                      Iend;                      // 0x0F
}  TMemoryMap;

#define AddressOf(item)    offsetof(TMemoryMap, item)

static TYesNo Write(byte Address, byte Data);
// Write <Data> to <Address>

static TYesNo Read(byte Address, byte *Data);
// Read <Data> from <Address>

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void Adp5062Init( void)
// Initialisation
{
   Adp5062PortInit();
   iicAdp5062Init();
} // Adp5062Init

//-----------------------------------------------------------------------------
// Vin OK
//-----------------------------------------------------------------------------

TYesNo Adp5062VinIsOk( void)
// Input voltage OK
{
byte Data;
TChargerStatus1 *ChargerStatus1 = (TChargerStatus1 *)&Data;

   Read(AddressOf(ChargerStatus1), &Data);
   return ChargerStatus1->VinOk;
} // Adp5062VinIsOk

//-----------------------------------------------------------------------------
// Enable Interrupt
//-----------------------------------------------------------------------------
#warning Volani EnableInterrupt prepise puvodni nastaveni preruseni 
void Adp5062InterruptEnable( byte Interrupt)
// Enable <Interrupt>
{
   Write(AddressOf(InterruptEnable), Interrupt);
} // Adp5062InterruptEnable

//-----------------------------------------------------------------------------
// Interrupt
//-----------------------------------------------------------------------------

byte Adp5062Interrupt( void)
// Check for <Interrupt>
{
byte Data;
   Read(AddressOf(InterruptActive), &Data);
   return Data;
}

//-----------------------------------------------------------------------------
// Input current limit
//-----------------------------------------------------------------------------

void Adp5062InputLimitHigh( TYesNo Enable)
// Controls DIG_IO1 pin
{
   if(Enable) {
      Adp5062DigIo1Set();
   } else {
      Adp5062DigIo1Clear();
   }
}

//-----------------------------------------------------------------------------
// Input current limit
//-----------------------------------------------------------------------------

void Adp5062InputLimitSet( int Miliamps)
// Set input limit
{
int Decoding;
byte Data;
TVinxPinSettings *VinxPinSettings = (TVinxPinSettings *)&Data;
   if(Miliamps <= 100) {
      Decoding = ILIM_100_MA;
   } else if(Miliamps <= 150) {
      Decoding = ILIM_150_MA;
   } else if(Miliamps <= 200) {
      Decoding = ILIM_200_MA;
   } else if(Miliamps <= 250) {
      Decoding = ILIM_250_MA;
   } else if(Miliamps <= 300) {
      Decoding = ILIM_300_MA;
   } else if(Miliamps <= 400) {
      Decoding = ILIM_400_MA;
   } else if(Miliamps <= 500) {
      Decoding = ILIM_500_MA;
   } else if(Miliamps <= 600) {
      Decoding = ILIM_600_MA;
   } else if(Miliamps <= 700) {
      Decoding = ILIM_700_MA;
   } else if(Miliamps <= 800) {
      Decoding = ILIM_800_MA;
   } else if(Miliamps <= 900) {
      Decoding = ILIM_900_MA;
   } else if(Miliamps <= 1000) {
      Decoding = ILIM_1000_MA;
   } else if(Miliamps <= 1200) {
      Decoding = ILIM_1200_MA;
   } else if(Miliamps <= 1500) {
      Decoding = ILIM_1500_MA;
   } else if(Miliamps <= 1800) {
      Decoding = ILIM_1800_MA;
   } else {
      Decoding = ILIM_2100_MA;
   }
   VinxPinSettings->ILim = Decoding;
   VinxPinSettings->reserved = 0;
   VinxPinSettings->dummy = 0;
   Write(AddressOf(VinxPinSettings), Data);
} // Adp5062SetInputLimit

//-----------------------------------------------------------------------------
// Charger enable
//-----------------------------------------------------------------------------

void Adp5062ChargeEnable(TYesNo Enable)
// Enable/disable charging
{
byte Data;
TFunctionalSettings1 *Settings = (TFunctionalSettings1 *)&Data;

   Data = 0;
   Settings->EnCharging = Enable;
   Settings->EnEndOfCharge = YES;
   Write(AddressOf(FunctionalSettings1), Data);
}

//-----------------------------------------------------------------------------
// Clear fault
//-----------------------------------------------------------------------------

void Adp5062ClearFault( void)
// Clear temperature and charger faults
{
   Write(AddressOf(Fault), 0xFF);
} // Adp5062ClearFault

//-----------------------------------------------------------------------------
// System voltage
//-----------------------------------------------------------------------------

void Adp5062SystemVoltageSet( int VoltageMv)
// Set system voltage
{
int Decoding;
byte Data;
TFunctionalSettings2 *FunctionalSettings2 = (TFunctionalSettings2 *)&Data;
   if(VoltageMv <= 4300) {
      Decoding = SYSTEM_VTG_43_V;
   } else if(VoltageMv <= 4400) {
      Decoding = SYSTEM_VTG_44_V;
   } else if(VoltageMv <= 4500) {
      Decoding = SYSTEM_VTG_45_V;
   } else if(VoltageMv <= 4600) {
      Decoding = SYSTEM_VTG_46_V;
   } else if(VoltageMv <= 4700) {
      Decoding =SYSTEM_VTG_47_V ;
   } else if(VoltageMv <= 4800) {
      Decoding = SYSTEM_VTG_48_V;
   } else if(VoltageMv <= 4900) {
      Decoding = SYSTEM_VTG_49_V;
   } else {
      Decoding = SYSTEM_VTG_50_V;
   }
   FunctionalSettings2->SystemVtg = Decoding;
   FunctionalSettings2->IdealDiode = IDEAL_DIODE_ENABLED_CONSTANTLY_WHEN_VSYS_IS_LOWER_THAN_VBAT;
   FunctionalSettings2->EnChargingVtgLimit = NO;
   FunctionalSettings2->JeitaSelect = JEITA_SELECT_JEITA_1_SELECTED;
   FunctionalSettings2->EnJeita = NO;
   Write(AddressOf(FunctionalSettings2), Data);
} // Adp5062SystemVoltageSet
 
//-----------------------------------------------------------------------------
// Charger status
//-----------------------------------------------------------------------------

TAdp5062ChargerStatus Adp5062ChargerStatusGet( void)
// Get charger status
{
byte Data;
TChargerStatus1 *ChargerStatus1 = (TChargerStatus1 *)&Data;
TAdp5062ChargerStatus Status;

   Read(AddressOf(ChargerStatus1), &Data);
   Status.OverVoltage = ChargerStatus1->VinOverVtg;
   Status.VoltageOk =  ChargerStatus1->VinOk;
   Status.CurrentLimited = ChargerStatus1->VinIlimited;
   Status.ThermalLimited = ChargerStatus1->ThermLimited;
   Status.Charged = ChargerStatus1->ChargeDone;
   Status.ChargerStatus = ChargerStatus1->ChargerStatus;

   return Status;
}

//-----------------------------------------------------------------------------
// Battery status
//-----------------------------------------------------------------------------

TAdp5062BatteryStatus Adp5062BatteryStatusGet( void)
// Get battery status
{
byte Data;
TChargerStatus2 *ChargerStatus2 = (TChargerStatus2 *)&Data;
TFault *Fault = (TFault *)&Data;
TAdp5062BatteryStatus Status;

   Read(AddressOf(ChargerStatus2), &Data);
   Status.ThermalStatus = ChargerStatus2->ThermStatus;
   Status.Status = ChargerStatus2->BatteryStatus;
   Read(AddressOf(Fault), &Data);
   Status.Shorted = Fault->BatteryShort;

   return Status;
}

//******************************************************************************

//-----------------------------------------------------------------------------
// Write
//-----------------------------------------------------------------------------

static TYesNo Write(byte Address, byte Data)
// Write <Data> to <Address>
{
   iicAdp5062Start();
   if(!iicAdp5062Send(IicStartByte(ADP5062_ADDRESS, IIC_WRITE))) {
      iicAdp5062Stop();
      return NO;
   }
   if(!iicAdp5062Send(Address)) {
      iicAdp5062Stop();
      return NO;
   }
   if(!iicAdp5062Send(Data)) {
      iicAdp5062Stop();
      return NO;
   }
   iicAdp5062Stop();
   return YES;
} // Write

//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------

static TYesNo Read(byte Address, byte *Data)
// Read <Data> from <Address>
{
   iicAdp5062Start();
   if(!iicAdp5062Send(IicStartByte(ADP5062_ADDRESS, IIC_WRITE))) {
      iicAdp5062Stop();
      return NO;
   }
   if(!iicAdp5062Send(Address)) {
      iicAdp5062Stop();
      return NO;
   }
   iicAdp5062Start();
   if(!iicAdp5062Send(IicStartByte(ADP5062_ADDRESS, IIC_READ))) {
      iicAdp5062Stop();
      return NO;
   }
   *Data = iicAdp5062Receive(NO);
   iicAdp5062Stop();
   return YES;
} // Read