//*****************************************************************************
//
//    Adp5062Iic.h     Adp5062Iic I2C controller
//    Version 1.0      (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Adp5062Iic_H__
   #define __Adp5062Iic_H__
   
//--- Default I2C access
#include "Iic/Iic.h"

#define iicAdp5062Init()                //IicInit(IIC_ADP5062_CHANNEL)
// Interface initialisation

#define iicAdp5062Start()               //IicStart(IIC_ADP5062_CHANNEL)
// Sends start sequence

#define iicAdp5062Stop()                //IicStop(IIC_ADP5062_CHANNEL)
// Sends stop sequence

#define iicAdp5062Send( Value)          NO//IicSend( IIC_ADP5062_CHANNEL, Value)
// Sends <value>, returns YES if ACK

#define iicAdp5062Receive( Ack)         NO//IicReceive( IIC_ADP5062_CHANNEL, Ack)
// Returns received byte, sends confirmation <ack>.
// <ack> = YES sends ACK, NO sends NOT ACK

#endif
