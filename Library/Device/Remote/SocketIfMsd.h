//*****************************************************************************
//
//    SocketIfMsd.h         Mass storage device socket interface
//    Version 1.0           (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __SocketIfMsd_H__
   #define __SocketIfMsd_H__

#include "Socket.h"

#ifdef __cplusplus
   extern "C" {
#endif

#ifdef __WIN32__
   void SocketIfMsdSetSerialNumber(int num);
#endif

TYesNo SocketIfMsdTest(void);

TYesNo SocketIfMsdOpenForReceiveCmd( void);
// Open socket for cmd receive

TYesNo SocketIfMsdOpenForSendCmd( TSocket *Socket);
// Open socket for send cmd

dword SocketIfMsdTotalReceiveSize();
// total size of the file with receive commands

byte SocketIfMsdPermission( TSocket *Socket);
// Permission

TYesNo SocketIfMsdListen( TSocket *Socket);
// Listen

byte SocketIfMsdState( TSocket *Socket);
// Gets state of socket

TYesNo SocketIfMsdReceive( TSocket *Socket, void *Buffer, int Size);
// Receive into <Buffer> with <Size>

int SocketIfMsdReceiveSize( TSocket *Socket);
// Gets number of received bytes

TYesNo SocketIfMsdSend( TSocket *Socket, const void *Buffer, int Size);
// Send <Buffer> with <Size>

void SocketIfMsdClose( TSocket *Socket);
// Close socket

TYesNo SocketIfMsdFormat( void);
// format MSD 
#ifdef __cplusplus
   }
#endif
#endif
