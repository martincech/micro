//*****************************************************************************
//
//    SocketIfMsdDef.h  MSD socket interface
//    Version 1.0       (c) VEIT Electronics
//
//*****************************************************************************

#ifndef _SocketIfMsdDef_H_
   #define _SocketIfMsdDef_H_

#include "Unisys/Uni.h"

#define SOCKET_MSD_BASE_DIRECTORY "Bat2"
#define SOCKET_MSD_CLIENT_SUFIX    "_C"
#define SOCKET_MSD_CMD_FILE     ".bin"

#define SOCKET_MSD_COM_NUMBER_MAX         999
#define SOCKET_MSD_FILE_NAME_LENGTH       8 + sizeof(SOCKET_MSD_CLIENT_SUFIX) + sizeof(SOCKET_MSD_CMD_FILE)
                                          // 8 is number of character when 32 bit number printed as hex to string
#define FIRMWARE_FILENAME  "Bat2Fw.bin"
#endif
