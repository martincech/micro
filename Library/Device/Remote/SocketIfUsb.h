//*****************************************************************************
//
//    SocketIfUsb.h     Usb socket interface
//    Version 1.0       (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __SocketIfUsb_H__
   #define __SocketIfUsb_H__

#include "Socket.h"

byte SocketIfUsbPermission( TSocket *Socket);
// Permission

TYesNo SocketIfUsbListen( TSocket *Socket);
// Listen

void SocketIfUsbInit( void);
// Initialization

void SocketIfUsbExecute( void);
// Execute

void SocketIfUsbDeinit( void);
// Deinitialization

void SocketIfUsbTimer( void);
// Timer

byte SocketIfUsbState( TSocket *Socket);
// Gets state of socket

TYesNo SocketIfUsbReceive( TSocket *Socket, void *Buffer, int Size);
// Receive into <Buffer> with <Size>

int SocketIfUsbReceiveSize( TSocket *Socket);
// Gets number of received bytes

TYesNo SocketIfUsbSend( TSocket *Socket, const void *Buffer, int Size);
// Send <Buffer> with <Size>

void SocketIfUsbClose( TSocket *Socket);
// Close socket

TYesNo SocketIfUsbConnected( void);
// Socket connected

TYesNo SocketIfUsbReady( void);
// Socket ready
#endif