//*****************************************************************************
//
//    SocketIfMsd.c     Mass storage device socket interface
//    Version 1.0       (c) VEIT Electronics
//
//*****************************************************************************

#include "Remote/SocketIfMsd.h"
#include "Remote/SocketIfMsdDef.h"
#include "File/Efs.h"
#include "Remote/Frame.h"
#include "Console/conio.h"
#include "Config/Configuration.h"
#include <string.h>
#define CHUNK_LENGTH   32

static TFrameState _State;
static byte         Status;
static TYesNo   Occupied;
static TEfsFile File;
static EFileMode Permission;

static int ReceiveFileNumber; // name of rcv file

#ifdef __WIN32__
static int SerialNumber; 
extern TDeviceVersion Bat2Version;
void SocketIfMsdSetSerialNumber(int num)
{
   Bat2Version.SerialNumber = num;
}

#endif
//------------------------------------------------------------------------------
//   Permission
//------------------------------------------------------------------------------

byte SocketIfMsdPermission( TSocket *Socket)
// Write only
{  
   return Permission;
} // SocketIfMsdPermission


TYesNo SocketIfMsdListen( TSocket *Socket) {
   if(Occupied) {
      return NO;
   }
   if(Status == SOCKET_STATE_DISCONNECTED) {
      return NO;
   }
   Occupied = YES;
   return YES;
}



TYesNo SocketIfMsdFormat( void) {
// format MSD 
   if(!EfsInit()) {
      return NO;
   }
   if(!EfsFormat()) {
      EfsSwitchOff();
      return NO;
   }
   EfsSwitchOff();
   return YES;
}


//------------------------------------------------------------------------------
//  Test
//------------------------------------------------------------------------------

#define TEST_PATTERN             "Bat2Test"
#define TEST_PATTERN_SIZE        sizeof(TEST_PATTERN) - 1
#define TEST_DIRECTORY           "Bat2Test"
#define TEST_DIRECTORY_NEW_NAME  "Bat2TestNew"
#define TEST_FILE                "Bat2Test"
#define TEST_REPEAT              1000

TYesNo SocketIfMsdTest( void)
// Memory test
{
byte Buffer[TEST_PATTERN_SIZE];
int i;
TYesNo DirectoryExisted;
TEfsFile File;
   if(!EfsInit()) {
      return NO;
   }

   DirectoryExisted = EfsDirectoryExists(TEST_DIRECTORY);

   if(!EfsDirectoryCreate(TEST_DIRECTORY)) {
      EfsSwitchOff();
      return NO;
   }

   if(!EfsFileCreate(&File, TEST_FILE, 0)) {
      EfsSwitchOff();
      return NO;
   }
   i = TEST_REPEAT;
   while(i--) {
      if(EfsFileWrite( &File, TEST_PATTERN, TEST_PATTERN_SIZE) != TEST_PATTERN_SIZE) {
         EfsSwitchOff();
         return NO;
      }
   }
   
   EfsFileClose( &File);
   if(!EfsFileOpen( &File, TEST_FILE)) {
      EfsSwitchOff();
      return NO;
   }

   i = TEST_REPEAT;
   while(i--) {
      if(EfsFileRead( &File, Buffer, TEST_PATTERN_SIZE) != TEST_PATTERN_SIZE) {
         EfsSwitchOff();
         return NO;
      }

      if(memcmp(Buffer, TEST_PATTERN, TEST_PATTERN_SIZE)) {
         EfsSwitchOff();
         return NO;
      }
   }

   EfsFileClose( &File);
   
   if(!EfsFileDelete(TEST_FILE)) {
      EfsSwitchOff();
      return NO;
   }

   if(!EfsDirectoryChange("..")) {
      EfsSwitchOff();
      return NO;
   }

   if(!DirectoryExisted) {
      if(!EfsDirectoryRename(TEST_DIRECTORY, TEST_DIRECTORY_NEW_NAME)) {
         EfsSwitchOff();
         return NO;
      }

      if(!EfsDirectoryDelete(TEST_DIRECTORY_NEW_NAME)) {
         EfsSwitchOff();
         return NO;
      }
   }
   EfsSwitchOff();
   return YES;
} // MemoryTest

//------------------------------------------------------------------------------
//  Open
//------------------------------------------------------------------------------

TYesNo SocketIfMsdOpenForReceiveCmd(void)
// Open socket for cmd receive
{
   int ComNumber;
   char FileName[SOCKET_MSD_FILE_NAME_LENGTH + 1];
   char Directory[SOCKET_MSD_FILE_NAME_LENGTH + 1];
   TYesNo Success;
   if(Status != SOCKET_STATE_DISCONNECTED) {
      return NO;
   }
   if (!EfsInit()) {
      return NO;
   }

   bprintf(Directory, "%s", SOCKET_MSD_BASE_DIRECTORY);
   if (!EfsDirectoryChange(Directory)) {
      EfsSwitchOff();
      return NO;
   }
   bprintf(Directory, "%08x", ConfigurationSerialNumber());
   if (!EfsDirectoryChange(Directory)) {
      EfsSwitchOff();
      return NO;
   }
   Success = NO;
   for(ComNumber = 0 ; ComNumber <= SOCKET_MSD_COM_NUMBER_MAX ; ComNumber++) {
#ifdef __WIN32__
      bprintf( FileName, "%d%s", ComNumber, SOCKET_MSD_CMD_FILE);
#else
      bprintf(FileName, "%d%s%s", ComNumber, SOCKET_MSD_CLIENT_SUFIX, SOCKET_MSD_CMD_FILE);
#endif
      if(!EfsFileExists( FileName)) {
         continue;
      }    
      Success = YES;
      ReceiveFileNumber = ComNumber;
      break;
   }

   if(!Success) {
      EfsSwitchOff();
      return NO;
   }

   if (!EfsFileOpen(&File, FileName)) {
      EfsSwitchOff();
      return NO;
   }
   Status = SOCKET_STATE_CONNECTED;
   Permission = FILE_MODE_READ_ONLY;
   return YES;
} // SocketIfMsdOpen

//------------------------------------------------------------------------------
//  Open
//------------------------------------------------------------------------------

TYesNo SocketIfMsdOpenForSendCmd( TSocket *Socket)
// Open socket for send cmd
{
int ComNumber;
char FileName[SOCKET_MSD_FILE_NAME_LENGTH + 1];
char Directory[SOCKET_MSD_FILE_NAME_LENGTH + 1];
TYesNo Success;

   if(Status != SOCKET_STATE_DISCONNECTED) {
      return NO;
   }
   if(!EfsInit()) {
      return NO;
   }
   bprintf(Directory, "%s", SOCKET_MSD_BASE_DIRECTORY);
   if(!EfsDirectoryChange( Directory)) {
      if(!EfsDirectoryCreate( Directory)) {
         return NO;
      }
   }
   bprintf(Directory, "%08x", ConfigurationSerialNumber());
   if (!EfsDirectoryChange(Directory)) {
      if (!EfsDirectoryCreate(Directory)) {
         return NO;
      }
   }

   Success = NO;
   for(ComNumber = 0 ; ComNumber <= SOCKET_MSD_COM_NUMBER_MAX ; ComNumber++) {
#ifndef __WIN32__
      bprintf(FileName, "%d%s", ComNumber, SOCKET_MSD_CMD_FILE);
#else
      bprintf(FileName, "%d%s%s", ComNumber, SOCKET_MSD_CLIENT_SUFIX, SOCKET_MSD_CMD_FILE);
#endif
      if (!EfsFileExists(FileName)) {
         if (!EfsFileCreate(&File, FileName, 0)) {
            EfsSwitchOff();
            return NO;
         }
         Success = YES;
         break;
      }
#ifdef __WIN32__
      else{
         if (!EfsFileOpen(&File, FileName, 0)) {
            continue;
         }
         EfsFileSeek(&File, 0, EFS_SEEK_END);
         Success = YES;
         break;
      }
#endif
   }

   if (!Success) {
      EfsSwitchOff();
      return NO;
}
#ifndef __WIN32__
   Socket->Interface = SOCKET_INTERFACE_MSD;
#endif
   Occupied = YES;
   Status = SOCKET_STATE_CONNECTED;
   Permission = FILE_MODE_WRITE_ONLY;
   return YES;
} // SocketIfMsdOpenForCmd


dword SocketIfMsdTotalReceiveSize()
// total size of the file with receive commands
{
   if(Permission != FILE_MODE_READ_ONLY){
      return 0;
   }
   return EfsFileSize( &File);
}

//------------------------------------------------------------------------------
//  State
//------------------------------------------------------------------------------

byte SocketIfMsdState( TSocket *Socket)
// Gets state of socket
{
   return Status;
} // SocketIfMsdState

//------------------------------------------------------------------------------
//  Receive
//------------------------------------------------------------------------------

TYesNo SocketIfMsdReceive( TSocket *Socket, void *Buffer, int Size)
// Receive into <Buffer> with <Size>
{
byte Chunk[CHUNK_LENGTH];
int ProccessedLength;
int ChunkLength;

   if(!FrameReceiveStart(&_State, Buffer, Size)) {
      Status = SOCKET_STATE_ERROR;
      return NO;
   }
   forever {
      ChunkLength = EfsFileRead( &File, Chunk, CHUNK_LENGTH);
      if(ChunkLength == 0) {
         SocketIfMsdClose( Socket);
         char FileName[SOCKET_MSD_FILE_NAME_LENGTH + 1];
#ifdef __WIN32__
         bprintf(FileName, "%d%s", ReceiveFileNumber, SOCKET_MSD_CMD_FILE);
#else
         bprintf(FileName, "%d%s%s", ReceiveFileNumber, SOCKET_MSD_CLIENT_SUFIX, SOCKET_MSD_CMD_FILE);
#endif
         EfsFileDelete(FileName);
         return YES;
      }
      ProccessedLength = FrameReceiveProccess( &_State, Chunk, ChunkLength);
      if(ProccessedLength < ChunkLength) {
         if(!EfsFileSeek( &File, ProccessedLength - ChunkLength, EFS_SEEK_CURRENT)) {
            Status = SOCKET_STATE_ERROR;
            return NO;
         }
      }
      switch(_State.Status) {
         case FRAME_STATE_RECEIVE_ACTIVE:
         case FRAME_STATE_RECEIVE_WAITING_HEADER :
            break;

         case FRAME_STATE_RECEIVE_FRAME:
            Status = SOCKET_STATE_RECEIVE_DONE;
            return YES;

         default:
            Status = SOCKET_STATE_ERROR;
            return NO;
      }
   }
} // SocketIfMsdReceive

//------------------------------------------------------------------------------
//  Receive size
//------------------------------------------------------------------------------

int SocketIfMsdReceiveSize( TSocket *Socket)
// Gets number of received bytes
{
   return _State.SizeProccessed;
} // SocketIfMsdReceiveSize

//------------------------------------------------------------------------------
//  Send
//------------------------------------------------------------------------------

TYesNo SocketIfMsdSend( TSocket *Socket, const void *Buffer, int Size)
// Send <Buffer> with <Size>
{
byte Chunk[CHUNK_LENGTH];
int ProccessedLength;

   if(!FrameSendInit(&_State, Buffer, Size)) {
      Status = SOCKET_STATE_ERROR;
      return NO;
   }
   forever {
      if(!FrameSendProccess( &_State, Chunk, CHUNK_LENGTH, &ProccessedLength)) {
         Status = SOCKET_STATE_ERROR;
         return NO;
      }
      if(!EfsFileWrite(&File, Chunk, ProccessedLength)) {
         Status = SOCKET_STATE_ERROR;
         return NO;
      }
      switch(_State.Status) {
         case FRAME_STATE_SEND_ACTIVE:
            break;

         case FRAME_STATE_SEND_DONE:
            Status = SOCKET_STATE_SEND_DONE;
            return YES;

         default:
            return NO;
      }
   }
} // SocketIfMsdSend

//------------------------------------------------------------------------------
//  Close
//------------------------------------------------------------------------------

void SocketIfMsdClose( TSocket *Socket)
// Close socket
{
   if(Status == SOCKET_STATE_DISCONNECTED){
      return;
   }
   EfsFileClose( &File);
   EfsSwitchOff();
   Occupied = NO;
   Status = SOCKET_STATE_DISCONNECTED;
   Permission = 0;
} // SocketIfMsdClose