//*****************************************************************************
//
//    SocketIfSms.h         Sms socket interface
//    Version 1.0           (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __SocketIfSms_H__
   #define __SocketIfSms_H__

#include "Socket.h"

byte SocketIfSmsPermission( TSocket *Socket);
// Permission

TYesNo SocketIfSmsListen( TSocket *Socket);
// Listen

void SocketIfSmsExecute( void);
// Executive

void SocketIfSmsTimer( void);
// Timer

byte SocketIfSmsState( TSocket *Socket);
// Gets state of socket

TYesNo SocketIfSmsReceive( TSocket *Socket, void *Buffer, int Size);
// Receive into <Buffer> with <Size>

int SocketIfSmsReceiveSize( TSocket *Socket);
// Gets number of received bytes

TYesNo SocketIfSmsSend( TSocket *Socket, const void *Buffer, int Size);
// Send <Buffer> with <Size>

void SocketIfSmsClose( TSocket *Socket);
// Close socket

#endif
