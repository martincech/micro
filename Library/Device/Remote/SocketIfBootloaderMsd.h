//*****************************************************************************
//
//    SocketIfBootloaderMsd.h         Mass storage device socket interface
//    Version 1.0           (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __SocketIfBootloaderMsd_H__
   #define __SocketIfBootloaderMsd_H__

#include "Socket.h"

#ifdef __cplusplus
   extern "C" {
#endif

#ifdef __WIN32__
   void SocketIfBootloaderMsdSetSerialNumber(int num);
#endif

TYesNo SocketIfBootloaderMsdTest(void);

TYesNo SocketIfBootloaderMsdOpenForReceiveCmd( void);
// Open socket for cmd receive

TYesNo SocketIfBootloaderMsdOpenForSendCmd( TSocket *Socket);
// Open socket for send cmd

byte SocketIfBootloaderMsdPermission( TSocket *Socket);
// Permission

TYesNo SocketIfBootloaderMsdListen( TSocket *Socket);
// Listen

byte SocketIfBootloaderMsdState( TSocket *Socket);
// Gets state of socket

TYesNo SocketIfBootloaderMsdReceive( TSocket *Socket, void *Buffer, int Size);
// Receive into <Buffer> with <Size>

int SocketIfBootloaderMsdReceiveSize( TSocket *Socket);
// Gets number of received bytes

TYesNo SocketIfBootloaderMsdSend( TSocket *Socket, const void *Buffer, int Size);
// Send <Buffer> with <Size>

void SocketIfBootloaderMsdClose( TSocket *Socket);
// Close socket

TYesNo SocketIfBootloaderMsdFormat( void);
// format MSD 
#ifdef __cplusplus
   }
#endif
#endif
