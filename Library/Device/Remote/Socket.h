   //*****************************************************************************
//
//    Socket.h           Socket
//    Version 1.0        (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Socket_H__
   #ifndef _MANAGED
      #define __Socket_H__
   #endif

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __SocketDef_H__
   #include "Remote/SocketDef.h"
#endif

#ifdef _MANAGED
namespace Bat2Library{
   public enum class SocketStateE{
#else
typedef enum {
#endif
   SOCKET_STATE_DISCONNECTED,
   SOCKET_STATE_CONNECTED,
   SOCKET_STATE_ERROR,
   SOCKET_STATE_RECEIVE_ACTIVE,
   SOCKET_STATE_RECEIVE_DONE,
   SOCKET_STATE_RECEIVE_ERROR,
   SOCKET_STATE_SEND_ACTIVE,
   SOCKET_STATE_SEND_DONE,
   SOCKET_STATE_SEND_ERROR
#ifndef _MANAGED
} ESocketState;
#else
   };
}
#endif

#ifndef _MANAGED
extern const TSocketDescriptor Sockets[SOCKET_COUNT];

#ifdef __cplusplus
   extern "C" {
#endif

#ifndef __File_H__
   #include "Memory/File.h"
#endif

TYesNo SocketListen( TSocket *Socket);

byte SocketPermission( TSocket *Socket);
// Permission

byte SocketState( TSocket *Socket);
// Gets state of socket

TYesNo SocketReceive( TSocket *Socket, void *Buffer, int Size);
// Receive into <Buffer> with <Size>

int SocketReceiveSize( TSocket *Socket);
// Gets number of received bytes

TYesNo SocketSend( TSocket *Socket, const void *Buffer, int Size);
// Send <Buffer> with <Size>

byte SocketInterfaceId( TSocket *Socket);
// Get interface id

void SocketClose( TSocket *Socket);
// Close socket

#ifdef __cplusplus
   }
#endif

#endif

#ifdef _MANAGED
   #undef _MANAGED
   #include "Socket.h"
   #define _MANAGED
#endif

#endif
