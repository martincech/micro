//*****************************************************************************
//
//    Frame.h           Frame
//    Version 1.0       (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Frame_H__
   #define __Frame_H__

#include "Unisys/Uni.h"

#ifdef __cplusplus
   extern "C" {
#endif

typedef enum {
   FRAME_STATE_IDLE,                     // stopped, no activity
   FRAME_STATE_ERROR,
   // receive status :
   FRAME_STATE_RECEIVE_ACTIVE,           // receiving active
   FRAME_STATE_RECEIVE_WAITING_HEADER,   // receiving active, header not completed
   FRAME_STATE_RECEIVE_SIZE_ERROR,       // received characters count greater than buffer size
   FRAME_STATE_RECEIVE_HEADER_ERROR,     // header start error
   FRAME_STATE_RECEIVE_HEADER_CRC_ERROR, // wrong received header CRC
   FRAME_STATE_RECEIVE_CRC_ERROR,        // wrong received frame CRC
   FRAME_STATE_RECEIVE_REPLY_TIMEOUT,    // no characters received up to reply timeout
   FRAME_STATE_RECEIVE_TIMEOUT,          // intercharacter timeout   
   FRAME_STATE_RECEIVE_FRAME,            // complete frame received
   _FRAME_STATE_RECEIVE_LAST,
   // send status :
   _FRAME_STATE_SEND = _FRAME_STATE_RECEIVE_LAST,
   FRAME_STATE_SEND_ACTIVE,              // send active
   FRAME_STATE_SEND_DONE,                // send done
   FRAME_STATE_SEND_TIMEOUT,             // send timeout
   _FRAME_STATE_SEND_LAST
} EFrameState;

typedef word TDataCrc;

typedef struct {
   byte *ReceiveBuffer;
   byte *SendBuffer;
   word RxTimeout;
   word RxReplyTimeout;
   word Timer;
   word ReceiveBufferSize;
   word SizeTotal;
   word SizeProccessed;
   TDataCrc DataCrc;
   byte Status;
   byte Index;
} TFrameState;

void FrameInit( TFrameState *State);
// Init frame processing

void FrameTimeoutSet( TFrameState *State, int RxTimeout, int RxReplyTimeout);
// Timeout set

TYesNo FrameSendInit( TFrameState *State, const void *Buffer, word Size);
// Init send processing

TYesNo FrameReceiveStart( TFrameState *State, void *Buffer, word Size);
// Init receive processing

TYesNo FrameSendProccess( TFrameState *State, const void *ChunkBuffer, int ChunkBufferSize, int *ChunkSize);
// Send process

int FrameReceiveProccess( TFrameState *State, void *ChunkBuffer, int ChunkBufferSize);
// Receive process

void FrameTimer( TFrameState *State);
// Timer, call periodically

#ifdef __cplusplus
   }
#endif

#endif
