//*****************************************************************************
//
//   FileNvm.h       File
//   Version 1.0     (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __FileNvm_H__
   #define __FileNvm_H__

#ifndef __File_H__
   #include "Memory/File.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif

byte FileLocalPermission( void);
// Permission

TYesNo FileLocalOpen( TFile *File, TFileName Filename, TFileMode Mode);
// Open

TYesNo FileLocalClose( TFile *File);
// Close

//------------------------------------------------------------------------------

TYesNo FileLocalSave( TFile *File, TFileAddress Address, const void *Data, int Size);
// Save <Data> with <Size> at <Address>

TYesNo FileLocalLoad( TFile *File, TFileAddress Address, void *Data, int Size);
// Load <Data> with <Size> from <Address>

TYesNo FileLocalFill( TFile *File, TFileAddress Address, byte Pattern, int Size);
// Write <Pattern> with <Size> at <Address>

TYesNo FileLocalMatch( TFile *File, TFileAddress Address, const void *Data, int Size);
// Compare <Data> with <Size> at <Address>

TFileSum FileLocalSum( TFile *File, TFileAddress Address, int Size);
// Sum NVM contents at <Address> with <Size>

TYesNo FileLocalMove( TFile *File, TFileAddress ToAddress, TFileAddress FromAddress, int Size);
// Move data <FromAddress> to <ToAddress> with <Size>. Check for overlapping

TYesNo FileLocalCommit( TFile *File);
// Commit sandboxed <File>

//------------------------------------------------------------------------------
#ifdef __cplusplus
   }
#endif

#endif
