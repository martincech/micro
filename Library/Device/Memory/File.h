//*****************************************************************************
//
//   File.h           File
//   Version 1.0     (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __File_H__
   #define __File_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __FileDef_H__
   #include "Memory/FileDef.h"
#endif


//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

#ifdef __WIN32__
   TYesNo FileSetup( void *Ptr);
   // Setup NVM
#endif

#ifdef __cplusplus
   extern "C" {
#endif

void FileInit( void);
// Initialize

TYesNo FileOpen( TFile *File, TFileName Filename, byte Mode);
// Open

TYesNo FileClose( TFile *File);
// Close

TYesNo FileIsOffline(TFile *File);
// Check is Offline

//------------------------------------------------------------------------------

void FileByteWrite( TFile *File, TFileAddress Address, byte Value);
// Write <Value> at <Address>

void FileWordWrite( TFile *File, TFileAddress Address, word Value);
// Write <Value> at <Address>

void FileDwordWrite( TFile *File, TFileAddress Address, dword Value);
// Write <Value> at <Address>

byte FileByteRead( TFile *File, TFileAddress Address);
// Returns value from <Address>

word FileWordRead( TFile *File, TFileAddress Address);
// Returns value from <Address>

dword FileDwordRead( TFile *File, TFileAddress Address);
// Returns value from <Address>

//------------------------------------------------------------------------------

TYesNo FileSave( TFile *File, TFileAddress Address, const void *Data, int Size);
// Save <Data> with <Size> at <Address>

TYesNo FileLoad( TFile *File, TFileAddress Address, void *Data, int Size);
// Load <Data> with <Size> from <Address>

TYesNo FileFill( TFile *File, TFileAddress Address, byte Pattern, int Size);
// Write <Pattern> with <Size> at <Address>

TYesNo FileMatch( TFile *File, TFileAddress Address, const void *Data, int Size);
// Compare <Data> with <Size> at <Address>

TFileSum FileSum( TFile *File, TFileAddress Address, int Size);
// Sum NVM contents at <Address> with <Size>

TYesNo FileMove( TFile *File, TFileAddress ToAddress, TFileAddress FromAddress, int Size);
// Move data <FromAddress> to <ToAddress> with <Size>. Check for overlapping

TYesNo FileClone( TFile *FileDest, TFile *FileSrc, TFileAddress Address, int Size);
// Copy <Size> data from <FileSrc> to <FileDest> starting from address <Address>

TYesNo FileCommit( TFile *File);
// Commit sandboxed <File>

//------------------------------------------------------------------------------
#ifdef __cplusplus
   }
#endif

#endif
