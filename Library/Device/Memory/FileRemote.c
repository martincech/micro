//******************************************************************************
//
//   FileRemoteMsd.c        File
//   Version 1.0            (c) VEIT Electronics
//
//******************************************************************************

#include "FileRemote.h"
#include "Remote/Socket.h"
#include "Remote/Cmd.h"
//#include "Remote/RcDef.h"

#define RC_FILES_COUNT    5

typedef struct {
   TFile *File;
} TRcFile;

static TYesNo CheckFile( TFile *File);
// Check file

static TRcFile FileHeap[RC_FILES_COUNT];

static TSocket *Socket;


TYesNo FileRemoteSetup( TSocket *_Socket) {
   Socket = _Socket;
   return YES;
} 

//------------------------------------------------------------------------------
//  Permission
//------------------------------------------------------------------------------

byte FileRemotePermission( void)
// Permission
{
   return SocketPermission( Socket) | FILE_MODE_SANDBOX;
} // FileRemotePermission

//------------------------------------------------------------------------------
//  Open
//------------------------------------------------------------------------------

TYesNo FileRemoteOpen( TFile *File, TFileName FileName, TFileMode FileMode)
// Open <FileName> with <Mode> access
{
int i;
TYesNo Found = NO;

   for( i = 0 ; i < RC_FILES_COUNT ; i++) {
      if(FileHeap[i].File == 0) {
         Found = YES;
         break;
      }
   }

   if(!Found) {
      return NO;
   }
   if(!CmdAndReplyFileOpen(Socket, FileName, FileMode)) {
      return NO;
   }
   FileHeap[i].File = File;

   return YES;
} // FileOpen

//------------------------------------------------------------------------------
//  Close
//------------------------------------------------------------------------------

TYesNo FileRemoteClose( TFile *File)
// Close <File>
{
int i;
   if(!CheckFile( File)) {
      return NO;
   }
   if(!CmdAndReplyFileClose(Socket, File)) {
      return NO;
   }
   for(i = 0 ; i < RC_FILES_COUNT ; i++) {
      if(FileHeap[i].File == File) {
         FileHeap[i].File = 0;
      }
   }
   return YES;
} // FileClose

//------------------------------------------------------------------------------
//  Save
//------------------------------------------------------------------------------

TYesNo FileRemoteSave( TFile *File, TFileAddress Address, const void *Buffer, int Size)
// Save <Data> with <Size> at <Address>
{
int SizeRemaining = Size;
int FragmentSize;
   if(!CheckFile( File)) {
      return NO;
   }

   while(SizeRemaining) {
      if(SizeRemaining < RC_FILE_DATA_MAX) {
         FragmentSize = SizeRemaining;
      } else {
         FragmentSize = RC_FILE_DATA_MAX;
      }

      if(!CmdAndReplyFileSave(Socket, File, Address, (void *)Buffer, FragmentSize)) {
         return NO;
      }

      Address += FragmentSize;
      Buffer = (byte *) Buffer + FragmentSize;
      SizeRemaining -= FragmentSize;
   }

   return YES;
} // FileSave

//------------------------------------------------------------------------------
//   Load
//------------------------------------------------------------------------------

TYesNo FileRemoteLoad( TFile *File, TFileAddress Address, void *Buffer, int Size)
// Load <Data> with <Size> from <Address>
{
int SizeRemaining = Size;
int FragmentSize;

   if(!CheckFile( File)) {
      return NO;
   }

   while(SizeRemaining) {
      if(SizeRemaining < RC_FILE_DATA_MAX) {
         FragmentSize = SizeRemaining;
      } else {
         FragmentSize = RC_FILE_DATA_MAX;
      }

      if(!CmdAndReplyFileLoad(Socket, File, Address, Buffer, FragmentSize)) {
         return NO;
      }

      Address += FragmentSize;
      Buffer = (byte *) Buffer + FragmentSize;
      SizeRemaining -= FragmentSize;
   }

   return YES;
} // FileLoad

//------------------------------------------------------------------------------
//   Fill
//------------------------------------------------------------------------------

TYesNo FileRemoteFill( TFile *File, TFileAddress Address, byte Pattern, int Size)
// Write <Pattern> with <Size> at <Address>
{
   if(!CheckFile( File)) {
      return NO;
   }
   if (!CmdFileFill(Socket, File, Address, Pattern, Size)) {
      return NO;
   }
   return YES;
} // FileFill

//------------------------------------------------------------------------------
//   Match
//------------------------------------------------------------------------------

TYesNo FileRemoteMatch( TFile *File, TFileAddress Address, const void *Data, int Size)
// Compare <Data> with <Size> at <Address>
{
   return NO;
} // FileMatch


//------------------------------------------------------------------------------
//  Commit
//------------------------------------------------------------------------------

TYesNo FileRemoteCommit( TFile *File)
// Commit sandboxed <File>
{
   if(!CheckFile( File)) {
      return NO;
   }

   if(!CmdAndReplyFileCommit(Socket, File)) {
      return NO;
   }

   return YES;
} // FileCommit

//------------------------------------------------------------------------------
//   Sum
//------------------------------------------------------------------------------

TFileSum FileRemoteSum( TFile *File, TFileAddress Address, int Size)
// Sum NVM contents at <Address> with <Size>
{
TFileSum Sum;
   if(!CheckFile( File)) {
      return 0;
   }
   if (!CmdAndReplyFileSum(Socket, File, Address, Size, &Sum)) {
      return 0;
   }

   return Sum;
} // FileSum

//******************************************************************************

//------------------------------------------------------------------------------
//   Check file
//------------------------------------------------------------------------------

static TYesNo CheckFile( TFile *File)
// Check file
{
   int i;
   if(!File) {
      return NO;
   }

   for(i = 0 ; i < RC_FILES_COUNT ; i++) {
      if(FileHeap[i].File == File) {
         return YES;
      }
   }
   return NO;
} // CheckFile