//*****************************************************************************
//
//   FileRemote.h     File
//   Version 1.0      (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __FileRemote_H__
   #define __FileRemote_H__

#include "File.h"
#include "Remote/Socket.h"

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

#ifdef __cplusplus
   extern "C" {
#endif

TYesNo FileRemoteSetup( TSocket *Socket);
// Setup NVM

byte FileRemotePermission( void);
// Permission

TYesNo FileRemoteOpen( TFile *File, TFileName FileName, TFileMode Mode);
// Open

TYesNo FileRemoteClose( TFile *File);
// Close

//------------------------------------------------------------------------------

TYesNo FileRemoteSave( TFile *File, TFileAddress Address, const void *Buffer, int Size);
// Save <Data> with <Size> at <Address>

TYesNo FileRemoteLoad( TFile *File, TFileAddress Address, void *Buffer, int Size);
// Load <Data> with <Size> from <Address>

TYesNo FileRemoteFill( TFile *File, TFileAddress Address, byte Pattern, int Size);
// Write <Pattern> with <Size> at <Address>

TYesNo FileRemoteMatch( TFile *File, TFileAddress Address, const void *Buffer, int Size);
// Compare <Data> with <Size> at <Address>

TFileSum FileRemoteSum( TFile *File, TFileAddress Address, int Size);
// Sum NVM contents at <Address> with <Size>

TYesNo FileRemoteCommit( TFile *File);
// Commit sandboxed <File>

//------------------------------------------------------------------------------
#ifdef __cplusplus
   }
#endif

#endif
