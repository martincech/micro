//*****************************************************************************
//
//   Ram.h       RAM memory
//   Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Ram_H__
   #define __Ram_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------
/*
void RamSave( dword Address, const void *Data, int Size);
// Save <Data> with <Size> at <Address>
*/
void RamLoad( dword Address, void *Data, int Size);
// Load <Data> with <Size> from <Address>

#endif
