//*****************************************************************************
//
//   NvmDef.h    Nonvolatile memory definitions
//   Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __NvmDef_H__
   #define __NvmDef_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// NVM invalid address :
#define NVM_ADDRESS_INVALID 0xFFFFFFFF
#define NVM_ADDRESS_MASK    0x0FFFFFFF   // direct memory chip access mask

// NVM address :
typedef dword TNvmAddress;

// NVM sum :
typedef dword TNvmSum;

#endif
