//*****************************************************************************
//
//    NvmNorFlashFram.c   Nonvolatile memory with NOR flash and FRAM paging
//    Version 1.0         (c) VEIT Electronics
//
//*****************************************************************************

#include "Memory/Nvm.h"
#include "System/System.h"        // SysDelay
#include "Fram/Fram.h"            // FRAM memory
#include "Flash/NorFlash.h"       // NOR Flash memory
#include "Memory/Ram.h"       // NOR Flash memory
#include <string.h>

#define NVM_COPY_SIZE   128

// Data read sections descriptor :
typedef struct {
   TNvmAddress PreAddress;        // Start address before cache
   TNvmAddress PreSize;           // Size before cache
   TNvmAddress InAddress;         // Start address inside cache
   TNvmAddress InSize;            // Size inside cache
   TNvmAddress PostAddress;       // Start address following cache
   TNvmAddress PostSize;          // Size following cache
} TSplitAddress;

// Project section data :
#include "Memory/NvmSection.c"

// Local functions :
static const TNvmSection *_SectionSearch( TNvmAddress Address);
// Returns section by <Address>

static void _CacheInit( void);
// Initialize cache pages

static void _CacheCommit( void);
// Commit cache pages

static void _CacheLoad( const TNvmSection *Section, TNvmAddress Address, void *Data, int Size);
// Read cache fragment from page <Address> to <Data>

static void _CacheSave( const TNvmSection *Section, TNvmAddress Address, const void *Data, int Size);
// Write data fragment to page <Address> at cache

static void _CacheFill( const TNvmSection *Section, TNvmAddress Address, byte Pattern, int Size);
// Fill cache fragment with <Pattern>

static TYesNo _CacheMatch( const TNvmSection *Section, TNvmAddress Address, const void *Data, int Size);
// Read cache fragment from page <Address> to <Data>

static TNvmSum _CacheSum( const TNvmSection *Section, TNvmAddress Address, int Size);
// Read cache fragment from page <Address> to <Data>

static TYesNo _CacheMove( const TNvmSection *Section, TNvmAddress Address);
// Check for cache hit at <Address> and move page cache if neeeded

static void _CacheBlockSave( const TNvmSection *Section, TNvmAddress Address, const void *Data, int Size);
// Write data fragment to page <Address> at cache

static void _CacheBlockFill( const TNvmSection *Section, TNvmAddress Address, byte Pattern, int Size);
// Read cache fragment from page <Address> to <Data>

static void _PageInit( const TNvmSection *Section);
// Initialize cached page

static TYesNo _PageSave( const TNvmSection *Section);
// Save page to Flash

static void _PageLoad( const TNvmSection *Section);
// Load page from Flash

static void _PageErase( const TNvmSection *Section);
// Erase Flash page and cache

static void _PageUpdate( const TNvmSection *Section);
// Update page header

static TNvmAddress _PageAddress( const TNvmSection *Section, TNvmAddress Address);
// Get page start address by <Address>

static TSplitAddress _AddressSplit( const TNvmSection *Section, TNvmAddress Address, int Size);
// Split <Address> with <Size> to sections

//------------------------------------------------------------------------------
//   Initialize
//------------------------------------------------------------------------------

void NvmInit( void)
// Initialize
{
   FramInit();
   FlashInit();
   _CacheInit();
} // NvmInit

//------------------------------------------------------------------------------
//   Commit
//------------------------------------------------------------------------------

void NvmCommit( void)
// Permanently save data (flush cache)
{
   _CacheCommit();
} // NvmCommit

//------------------------------------------------------------------------------
//   Format
//------------------------------------------------------------------------------

void NvmFormat( void)
// Clear all data
{
   // clear all data :
   FramFill( 0,  0xFF, FRAM_SIZE);
   FlashEraseAll();
   // prepare cache data :
   _CacheInit();
} // NvmFormat

//------------------------------------------------------------------------------
//   Shutdown
//------------------------------------------------------------------------------

void NvmShutdown( void)
// Shutdown mode
{
   FlashPowerDown();
   FramSleep();
} // NvmShutdown

//------------------------------------------------------------------------------
//   Read / Write
//------------------------------------------------------------------------------

void NvmByteWrite( TNvmAddress Address, byte Value)
// Write <Value> at <Address>
{
   NvmSave( Address, &Value, sizeof( Value));
} // NvmByteWrite

void NvmWordWrite( TNvmAddress Address, word Value)
// Write <Value> at <Address>
{
   NvmSave( Address, &Value, sizeof( Value));
} // NvmWordWrite

void NvmDwordWrite( TNvmAddress Address, dword Value)
// Write <Value> at <Address>
{
   NvmSave( Address, &Value, sizeof( Value));
} // NvmDwordWrite

//------------------------------------------------------------------------------

byte NvmByteRead( TNvmAddress Address)
// Returns value from <Address>
{
byte Value;

   NvmLoad( Address, &Value, sizeof( Value));
   return( Value);
} // NvmByteRead

word NvmWordRead( TNvmAddress Address)
// Returns value from <Address>
{
word Value;

   NvmLoad( Address, &Value, sizeof( Value));
   return( Value);
} // NvmWordRead

dword NvmDwordRead( TNvmAddress Address)
// Returns value from <Address>
{
dword Value;

   NvmLoad( Address, &Value, sizeof( Value));
   return( Value);
} // NvmDwordRead

//------------------------------------------------------------------------------
//   Save
//------------------------------------------------------------------------------

TYesNo NvmSave( TNvmAddress Address, const void *Data, int Size)
// Save <Data> with <Size> at <Address>
{
const TNvmSection *Section;
TSplitAddress      Split;
byte              *p;

   Section = _SectionSearch( Address);
   switch( Section->Mode){
      case NVM_SECTION_STANDARD :
      case NVM_SECTION_WRITE_ONLY :
         // with active page cache
         break;

      case NVM_SECTION_COMMIT_ONLY :
         // all data cached
         _CacheSave( Section, Address, Data, Size);             // save cached data
         return( YES);

      case NVM_SECTION_NOCACHE :
         // direct write to Flash
         return( FlashSave( Address & NVM_ADDRESS_MASK, Data, Size));

      case NVM_SECTION_CACHE_ONLY :
         // direct write to Fram
         FramSave( Address & NVM_ADDRESS_MASK, Data, Size);
         return( YES);

      case NVM_SECTION_RAM :
         return NO;
   }
   Split = _AddressSplit( Section, Address, Size);
   p     = (byte *)Data;
   // write inside cache first :
   if( Split.InSize){
      _CacheSave( Section, Split.InAddress, p + Split.PreSize, Split.InSize);
   }
   // write flash before cache :
   if( Split.PreSize){
      _CacheBlockSave( Section, Split.PreAddress, p, Split.PreSize);
   }
   // write after cache :
   if( Split.PostSize){
      _CacheBlockSave( Section, Split.PostAddress, p + Split.PreSize + Split.InSize, Split.PostSize);
   }
   return( YES);
} // NvmSave

//------------------------------------------------------------------------------
//   Load
//------------------------------------------------------------------------------

void NvmLoad( TNvmAddress Address, void *Data, int Size)
// Load <Data> with <Size> from <Address>
{
const TNvmSection *Section;
TSplitAddress      Split;
byte              *p;

   Section = _SectionSearch( Address);
   switch( Section->Mode){
      case NVM_SECTION_STANDARD :
      case NVM_SECTION_WRITE_ONLY :
         // with active page cache
         break;

      case NVM_SECTION_COMMIT_ONLY :
         // all data cached
         _CacheLoad( Section, Address, Data, Size);             // load cached data
         return;

      case NVM_SECTION_NOCACHE :
         // direct read from Flash
         FlashLoad( Address & NVM_ADDRESS_MASK, Data, Size);
         return;

      case NVM_SECTION_CACHE_ONLY :
         // direct read from Fram
         FramLoad( Address & NVM_ADDRESS_MASK, Data, Size);
         return;

      case NVM_SECTION_RAM :
         // direct read from RAM
         RamLoad( Address & NVM_ADDRESS_MASK, Data, Size);
         return;
   }
   Split = _AddressSplit( Section, Address, Size);
   p     = (byte *)Data;
   // read flash before cache :
   if( Split.PreSize){
      FlashLoad( Split.PreAddress & NVM_ADDRESS_MASK, p, Split.PreSize);
   }
   // read inside cache :
   if( Split.InSize){
      _CacheLoad( Section, Split.InAddress, p + Split.PreSize, Split.InSize);
   }
   // read after cache :
   if( Split.PostSize){
      FlashLoad( Split.PostAddress & NVM_ADDRESS_MASK, p + Split.PreSize + Split.InSize, Split.PostSize);
   }
} // NvmLoad

//------------------------------------------------------------------------------
//   Fill
//------------------------------------------------------------------------------

TYesNo NvmFill( TNvmAddress Address, byte Pattern, int Size)
// Write <Pattern> with <Size> at <Address>
{
const TNvmSection *Section;
TSplitAddress      Split;

   Section = _SectionSearch( Address);
   switch( Section->Mode){
      case NVM_SECTION_STANDARD :
      case NVM_SECTION_WRITE_ONLY :
         // with active page cache
            break;

      case NVM_SECTION_COMMIT_ONLY :
         // all data cached
         _CacheFill( Section, Address, Pattern, Size);          // save cached data
         return( YES);

      case NVM_SECTION_NOCACHE :
         // direct write to Flash
         return( FlashFill( Address & NVM_ADDRESS_MASK, Pattern, Size));

      case NVM_SECTION_CACHE_ONLY :
         // direct write to Fram
         FramFill( Address & NVM_ADDRESS_MASK, Pattern, Size);
         return( YES);
   }
   Split = _AddressSplit( Section, Address, Size);
   // write inside cache :
   if( Split.InSize){
      _CacheFill( Section, Split.InAddress, Pattern, Split.InSize);
   }
   // write flash before cache :
   if( Split.PreSize){
      _CacheBlockFill( Section, Split.PreAddress, Pattern, Split.PreSize);
   }
   // write after cache :
   if( Split.PostSize){
      _CacheBlockFill( Section, Split.PostAddress, Pattern, Split.PostSize);
   }
   return( YES);
} // NvmFill

//------------------------------------------------------------------------------
//   Match
//------------------------------------------------------------------------------

TYesNo NvmMatch( TNvmAddress Address, const void *Data, int Size)
// Compare <Data> with <Size> at <Address>
{
const TNvmSection *Section;
TSplitAddress      Split;
byte              *p;

   Section = _SectionSearch( Address);
   switch( Section->Mode){
      case NVM_SECTION_STANDARD :
      case NVM_SECTION_WRITE_ONLY :
         // with active page cache
         break;

      case NVM_SECTION_COMMIT_ONLY :
         // all data cached
         return( _CacheMatch( Section, Address, Data, Size));   // compare cached data

      case NVM_SECTION_NOCACHE :
         // direct match from Flash
         return( FlashMatch( Address & NVM_ADDRESS_MASK, Data, Size));

      case NVM_SECTION_CACHE_ONLY :
         // direct match from Fram
         return( FramMatch( Address & NVM_ADDRESS_MASK, Data, Size));
   }
   Split = _AddressSplit( Section, Address, Size);
   p     = (byte *)Data;
   // match flash before cache :
   if( Split.PreSize){
      if( !FlashMatch( Split.PreAddress & NVM_ADDRESS_MASK, p, Split.PreSize)){
         return( NO);
      }
   }
   // match inside cache :
   if( Split.InSize){
      if( !_CacheMatch( Section, Split.InAddress, p + Split.PreSize, Split.InSize)){
         return( NO);
      }
   }
   // match after cache :
   if( Split.PostSize){
      if( !FlashMatch( Split.PostAddress & NVM_ADDRESS_MASK, p + Split.PreSize + Split.InSize, Split.PostSize)){
         return( NO);
      }
   }
   return( YES);
} // NvmMatch

//------------------------------------------------------------------------------
//   Sum
//------------------------------------------------------------------------------

TNvmSum NvmSum( TNvmAddress Address, int Size)
// Sum NVM contents at <Address> with <Size>
{
const TNvmSection *Section;
TSplitAddress      Split;
TNvmSum            Sum;

   Section = _SectionSearch( Address);
   switch( Section->Mode){
      case NVM_SECTION_STANDARD :
      case NVM_SECTION_WRITE_ONLY :
         // with active page cache
         break;

      case NVM_SECTION_COMMIT_ONLY :
         // all data cached
         return( _CacheSum( Section, Address, Size));           // sum cached data

      case NVM_SECTION_NOCACHE :
         // direct sum from Flash
         return( FlashSum( Address & NVM_ADDRESS_MASK, Size));

      case NVM_SECTION_CACHE_ONLY :
         // direct sum from Fram
         return( FramSum( Address & NVM_ADDRESS_MASK, Size));
   }
   Sum = 0;
   Split = _AddressSplit( Section, Address, Size);
   // sum flash before cache :
   if( Split.PreSize){
      Sum += FlashSum( Split.PreAddress & NVM_ADDRESS_MASK, Split.PreSize);
   }
   // sum inside cache :
   if( Split.InSize){
      Sum += _CacheSum( Section, Split.InAddress, Split.InSize);
   }
   // sum after cache :
   if( Split.PostSize){
      Sum += FlashSum( Split.PostAddress & NVM_ADDRESS_MASK, Split.PostSize);
   }
   return( Sum);
} // NvmSum

//------------------------------------------------------------------------------
//   Move
//------------------------------------------------------------------------------

void NvmMove( TNvmAddress ToAddress, TNvmAddress FromAddress, int Size)
// Move data <FromAddress> to <ToAddress> with <Size>. Check for overlapping
{
byte        Buffer[ NVM_COPY_SIZE];
int        BlockSize;
TNvmAddress SrcAddress, DstAddress;

   if( ToAddress < FromAddress){
      // copy forward - from start to end
      SrcAddress = FromAddress;        // set address to start of block
      DstAddress = ToAddress;          // set address to start of block
      while( Size){
         // select block size :
         BlockSize = Size;
         if( BlockSize > NVM_COPY_SIZE){
            BlockSize = NVM_COPY_SIZE;
         }
         NvmLoad( SrcAddress, Buffer, BlockSize);
         NvmSave( DstAddress, Buffer, BlockSize);
         SrcAddress += BlockSize;      // move address to next block
         DstAddress += BlockSize;      // move address to next block
         Size       -= BlockSize;      // remainder of size
      }
   } else {
      // copy backward - from tail to head
      SrcAddress = FromAddress + Size; // set address to end of block
      DstAddress = ToAddress   + Size; // set address to end of block
      while( Size){
         // select block size :
         BlockSize = Size;
         if( BlockSize > NVM_COPY_SIZE){
            BlockSize = NVM_COPY_SIZE;
         }
         SrcAddress -= BlockSize;      // get tail address
         DstAddress -= BlockSize;      // get tail address
         NvmLoad( SrcAddress, Buffer, BlockSize);
         NvmSave( DstAddress, Buffer, BlockSize);
         Size       -= BlockSize;      // remainder of size
      }
   }
} // NvmMove

//******************************************************************************

//------------------------------------------------------------------------------
//   Section search
//------------------------------------------------------------------------------

static const TNvmSection *_SectionSearch( TNvmAddress Address)
// Returns section by <Address>
{
int i;
const TNvmSection *Section;

   for( i = 0; i < SECTION_COUNT; i++){
      Section = &_Section[ i];
      if( (Address >= Section->StartAddress) && (Address <= Section->EndAddress)){
         return( Section);
      }
   }
   return( &_Section[ 0]);
} // _SectionSearch

//------------------------------------------------------------------------------
//   Cache init
//------------------------------------------------------------------------------

static void _CacheInit( void)
// Initialize cache pages
{
byte i;
const TNvmSection *Section;

   for( i = 0; i < SECTION_COUNT; i++){
      Section = &_Section[ i];
      if( Section->Mode >= NVM_SECTION_NOCACHE){
         continue;                     // section without cache
      }
      _PageInit( Section);
   }
} // _CacheInit

//------------------------------------------------------------------------------
//   Cache save
//------------------------------------------------------------------------------

static void _CacheCommit( void)
// Save cache pages
{
byte i;
const TNvmSection *Section;

   for( i = 0; i < SECTION_COUNT; i++){
      Section = &_Section[ i];
      if( Section->Mode >= NVM_SECTION_NOCACHE){
         continue;                     // section without cache
      }
      _PageSave( Section);
   }
} // _CacheCommit

//------------------------------------------------------------------------------
//  Cache read
//------------------------------------------------------------------------------

static void _CacheLoad( const TNvmSection *Section, TNvmAddress Address, void *Data, int Size)
// Read cache fragment from page <Address> to <Data>
{
   Address -= Section->Cache->Address;                     // subtract cache base address
   Address += NvmPageDataAddress( Section->CacheAddress);  // add FRAM page start
   FramLoad( Address & NVM_ADDRESS_MASK, Data, Size);
} // _CacheLoad

//------------------------------------------------------------------------------
//  Cache write
//------------------------------------------------------------------------------

static void _CacheSave( const TNvmSection *Section, TNvmAddress Address, const void *Data, int Size)
// Write data fragment to page <Address> at cache
{
   Address -= Section->Cache->Address;                     // subtract cache base address
   Address += NvmPageDataAddress( Section->CacheAddress);  // add FRAM page start
   FramSave( Address & NVM_ADDRESS_MASK, Data, Size);
   Section->Cache->Status = NVM_PAGE_DIRTY;                // cache modified
   _PageUpdate( Section);                                  // update page header
} // _CacheSave

//------------------------------------------------------------------------------
//   Cache fill
//------------------------------------------------------------------------------

static void _CacheFill( const TNvmSection *Section, TNvmAddress Address, byte Pattern, int Size)
// Read cache fragment from page <Address> to <Data>
{
   Address -= Section->Cache->Address;                     // subtract cache base address
   Address += NvmPageDataAddress( Section->CacheAddress);  // add FRAM page start
   FramFill( Address & NVM_ADDRESS_MASK, Pattern, Size);
   Section->Cache->Status = NVM_PAGE_DIRTY;                // cache modified
   _PageUpdate( Section);                                  // update page header
} // _CacheFill

//------------------------------------------------------------------------------
//   Cache match
//------------------------------------------------------------------------------

static TYesNo _CacheMatch( const TNvmSection *Section, TNvmAddress Address, const void *Data, int Size)
// Read cache fragment from page <Address> to <Data>
{
   Address -= Section->Cache->Address;                     // subtract cache base address
   Address += NvmPageDataAddress( Section->CacheAddress);  // add FRAM page start
   return( FramMatch( Address & NVM_ADDRESS_MASK, Data, Size));
} // _CacheMatch

//------------------------------------------------------------------------------
//   Cache sum
//------------------------------------------------------------------------------

static TNvmSum _CacheSum( const TNvmSection *Section, TNvmAddress Address, int Size)
// Read cache fragment from page <Address> to <Data>
{
   Address -= Section->Cache->Address;                     // subtract cache base address
   Address += NvmPageDataAddress( Section->CacheAddress);  // add FRAM page start
   return( FramSum( Address & NVM_ADDRESS_MASK, Size));
} // _CacheSum

//------------------------------------------------------------------------------
//   Cache move
//------------------------------------------------------------------------------

static TYesNo _CacheMove( const TNvmSection *Section, TNvmAddress Address)
// Check for cache hit at <Address> and move page cache if neeeded
{
   if( Address >= Section->Cache->Address &&
       Address <  Section->Cache->Address + NvmPageSize( Section->PageSize)){
      return( YES);                    // cache hit
   }
   // save old page :
   if( !_PageSave( Section)){
      return( NO);                     // unable write flash
   }
   // get new page start address :
   Section->Cache->Address = _PageAddress( Section, Address);
   if( Section->Mode == NVM_SECTION_WRITE_ONLY){
      _PageErase( Section);
   } else {
      _PageLoad( Section);
   }
   return( YES);
} // _CacheMove

//------------------------------------------------------------------------------
//   Cache block save
//------------------------------------------------------------------------------

static void _CacheBlockSave( const TNvmSection *Section, TNvmAddress Address, const void *Data, int Size)
// Write data fragment to page <Address> at cache
{
dword       FragmentSize;
dword       PageSize;
const byte *p;

   PageSize     = NvmPageSize( Section->PageSize);
   p            = (const byte *)Data;
   FragmentSize = _PageAddress( Section, Address) + PageSize - Address; // size up to end of page
   while( Size){
      if( FragmentSize > Size){
         FragmentSize = Size;
      }
      _CacheMove( Section, Address);
      _CacheSave( Section, Address, p, FragmentSize);
      p           += FragmentSize;
      Address     += FragmentSize;
      Size        -= FragmentSize;
      FragmentSize = PageSize;
   }
} // _CacheBlockSave

//------------------------------------------------------------------------------
//   Cache block fill
//------------------------------------------------------------------------------

static void _CacheBlockFill( const TNvmSection *Section, TNvmAddress Address, byte Pattern, int Size)
// Read cache fragment from page <Address> to <Data>
{
dword FragmentSize;
dword PageSize;

   PageSize     = NvmPageSize( Section->PageSize);
   FragmentSize = _PageAddress( Section, Address) + PageSize - Address; // size up to end of page
   while( Size){
      if( FragmentSize > Size){
         FragmentSize = Size;
      }
      _CacheMove( Section, Address);
      _CacheFill( Section, Address, Pattern, FragmentSize);
      Address     += FragmentSize;
      Size        -= FragmentSize;
      FragmentSize = PageSize;
   }
} // _CacheBlockFill

//------------------------------------------------------------------------------
//   Page init
//------------------------------------------------------------------------------

static void _PageInit( const TNvmSection *Section)
// Initialize cache page
{
TNvmPage Page;

   // read page header :
   FramLoad( Section->CacheAddress, &Page, NvmPageHeaderSize());
   // check for header integrity :
   if( Page.Magic         == NVM_PAGE_MAGIC        &&
       Page.Cache.Status  < _NVM_PAGE_STATUS_LAST  &&
       Page.Cache.Address >= Section->StartAddress &&
       Page.Cache.Address <= Section->EndAddress){
      // valid page header
      *Section->Cache = Page.Cache;                   // load cache data
      return;
   }
   // uninitialized/corrupted page
   Section->Cache->Status  = NVM_PAGE_NEW;
   Section->Cache->Address = Section->StartAddress;   // set first page address
   // prepare page :
   if( Section->Mode == NVM_SECTION_WRITE_ONLY){
      _PageErase( Section);            // clear cache
   } else {
      _PageLoad( Section);             // load data from flash
   }
} // _PageInit

//------------------------------------------------------------------------------
//   Page save
//------------------------------------------------------------------------------

static TYesNo _PageSave( const TNvmSection *Section)
// Save page to Flash
{
TYesNo      NewPage;
int         Size;
byte        Data;
TNvmAddress Address;

   if( Section->Cache->Status != NVM_PAGE_DIRTY){
      return( YES);                         // cache matches with Flash
   }
   // save FRAM data :
   Size    = NvmPageSize( Section->PageSize);
   Address = Section->Cache->Address;
   NewPage = YES;                           // first page
   if( Address == NVM_ADDRESS_INVALID){
      return( NO);                          // invalid page
   }
   FramBlockReadStart( NvmPageDataAddress( Section->CacheAddress));
   while( Size--){
      // check for block erase :
      if( (Address % FLASH_ERASE_SIZE) == 0){
         FlashBlockErase( Address & NVM_ADDRESS_MASK);
         if( !FlashWaitForReady()){
            return( NO);
         }
      }
      if( NewPage){
         // New page start
         FlashBlockWriteStart( Address & NVM_ADDRESS_MASK);
         NewPage = NO;
      }
      Data = FramBlockReadData();
      FlashBlockWriteData( Data);
      Address++;
      if( (Address % FLASH_WRITE_SIZE) == 0){
         // New address is at a new page, write finished page
         FlashBlockWritePerform();          // previous page
         if( !FlashWaitForReady()){
            return( NO);
         }
         NewPage = YES;                     // start a new page
      }
   }
   FramBlockReadStop();
   // check for last page :
   if( !NewPage) {
      FlashBlockWritePerform();             // write last page remainder
      if( !FlashWaitForReady()){
         return( NO);
      }
   }
   // update page status :
   Section->Cache->Status = NVM_PAGE_SAVED;
   _PageUpdate( Section);                   // update header
   return( YES);
} // _PageSave

//------------------------------------------------------------------------------
//   Page load
//------------------------------------------------------------------------------

static void _PageLoad( const TNvmSection *Section)
// Load page from Flash
{
int         Size;
byte        Data;
TNvmAddress Address;

   Size    = NvmPageSize( Section->PageSize);
   Address = Section->Cache->Address;
   if( Address == NVM_ADDRESS_INVALID){
      return;                               // invalid page
   }
   FlashBlockReadStart( Address & NVM_ADDRESS_MASK);
   FramPageWriteStart( NvmPageDataAddress( Section->CacheAddress));
   do {
      Data = FlashBlockReadData();
      FramPageWriteData( Data);
   } while( --Size);
   FlashBlockReadStop();
   FramPageWritePerform();
   // update page status :
   Section->Cache->Status = NVM_PAGE_NEW;
   _PageUpdate( Section);                   // update header
} // _PageLoad

//------------------------------------------------------------------------------
//   Page erase
//------------------------------------------------------------------------------

static void _PageErase( const TNvmSection *Section)
// Erase Flash page and cache
{
TNvmAddress Address;

   Address = Section->Cache->Address;
   if( Address == NVM_ADDRESS_INVALID){
      return;                               // invalid page
   }
   // update page status :
   Section->Cache->Status = NVM_PAGE_NEW;
   FramFill( NvmPageDataAddress( Section->CacheAddress), 0xFF, NvmPageSize( Section->PageSize));
   _PageUpdate( Section);                   // update header
} // _PageErase

//------------------------------------------------------------------------------
//   Page update
//------------------------------------------------------------------------------

static void _PageUpdate( const TNvmSection *Section)
// Update page header
{
TNvmPage Page;

   Page.Magic =  NVM_PAGE_MAGIC;
   Page.Cache = *Section->Cache;
   FramSave( Section->CacheAddress & NVM_ADDRESS_MASK, &Page, NvmPageHeaderSize());
} // _PageUpdate

//------------------------------------------------------------------------------
//   Page address
//------------------------------------------------------------------------------

static TNvmAddress _PageAddress( const TNvmSection *Section, TNvmAddress Address)
// Get page start address by <Address>
{
TNvmAddress PageSize;

   PageSize = NvmPageSize( Section->PageSize);
   Address /= PageSize;
   Address *= PageSize;
   return( Address);
} // _PageAddress

//------------------------------------------------------------------------------
//   Sections
//------------------------------------------------------------------------------

static TSplitAddress _AddressSplit( const TNvmSection *Section, TNvmAddress Address, int Size)
// Split <Address> with <Size> to sections
{
TNvmAddress   EndAddress;
TNvmAddress   CacheAddress;
TNvmAddress   EndCacheAddress;
TNvmAddress   PageSize;
TSplitAddress ASection;

   memset( &ASection, 0, sizeof( TSplitAddress));
   PageSize        = NvmPageSize( Section->PageSize);
   EndAddress      = Address + Size - 1;
   CacheAddress    = Section->Cache->Address;
   EndCacheAddress = Section->Cache->Address + PageSize - 1;
   // check for data before cache :
   if( Address < CacheAddress){
      ASection.PreSize    = CacheAddress - Address;
      ASection.PreAddress = Address;
      if( ASection.PreSize > Size){
         ASection.PreSize = Size;
         return( ASection);                 // fit in Flash before cache
      }
   }
   // check for data inside cache :
   if( Address <= EndCacheAddress){
      ASection.InAddress = Address;
      if( Address < CacheAddress){
         ASection.InAddress = CacheAddress;
      }
      if( EndAddress <= EndCacheAddress){
         ASection.InSize = EndAddress - ASection.InAddress + 1;
         return( ASection);                 // fit in cache
      }
      // size up to end of cache :
      ASection.InSize = EndCacheAddress - ASection.InAddress + 1;
   }
   // check for data past cache :
   ASection.PostAddress = Address;
   if( Address < EndCacheAddress + 1){
      ASection.PostAddress = EndCacheAddress + 1;
   }
   ASection.PostSize = EndAddress - ASection.PostAddress + 1;
   return( ASection);
} // _AddressSplit
