//******************************************************************************
//
//   File.c                 File
//   Version 1.0            (c) VEIT Electronics
//
//******************************************************************************

#include "File.h"

#include "Cpu/Cpu.h"

#define FILE_HANDLE_COUNT     10

static TFile *FileHeap[FILE_HANDLE_COUNT];

static TYesNo CheckPosition( TFile *File, TFileAddress Position, int Size);
//

TYesNo FileIsOffline( TFile *File) {
   if(FileInterface[File->Item.Interface].Permission() & FILE_MODE_READ_ONLY) {
      return NO;
   }
   return YES;
}

//------------------------------------------------------------------------------
//  Open
//------------------------------------------------------------------------------

TYesNo FileOpen( TFile *File, TFileName FileName, TFileMode FileMode)
// Open <FileName> with <Mode> access
{
TFile *CurrentFile;
TFile **FreePosInHeap = 0;
byte Permission;
int i;
   for(i = 0 ; i < FILE_HANDLE_COUNT ; i++) {
      CurrentFile = FileHeap[i];
      if(!CurrentFile) {
         if(!FreePosInHeap) {
            FreePosInHeap = &FileHeap[i];
         }
         continue;
      }
      if(CurrentFile->Item.Name != FileItem[FileName].Name || CurrentFile->Item.Interface != FileItem[FileName].Interface) {
         continue;
      }
      return NO;
   }
   if(!FreePosInHeap) {
      return NO;
   }
   Permission = FileInterface[FileItem[FileName].Interface].Permission();

   if(FileMode & ~Permission) {
      // required mode not available
      return NO;
   }

   File->Item = FileItem[FileName];
   File->Name = FileName;
   File->Mode = FileMode;
   if(!FileInterface[File->Item.Interface].Open( File, FileItem[FileName].Name, FileMode)) {
      return NO;
   }
   *FreePosInHeap = File;
   return YES;
} // FileOpen

//------------------------------------------------------------------------------
//  Close
//------------------------------------------------------------------------------

TYesNo FileClose( TFile *File)
// Close <File>
{
int i;
   for(i = 0 ; i < FILE_HANDLE_COUNT ; i++) {
      if(FileHeap[i] != File) {
         continue;
      }
      if(!FileInterface[File->Item.Interface].Close( File)) {
         return NO;
      }
      FileHeap[i] = 0;
      return YES;
   }
   return NO;
} // FileClose

//------------------------------------------------------------------------------
//   Read/write
//------------------------------------------------------------------------------

byte FileByteRead( TFile *File, TFileAddress Address)
// Returns value from <Address>
{
byte Data;
   if(!FileLoad( File, Address, &Data, sizeof(byte))) {
      return 0;
   }
   return Data;
} // FileByteRead

word FileWordRead( TFile *File, TFileAddress Address)
// Returns value from <Address>
{
word Data;
   if(!FileLoad( File, Address, &Data, sizeof(word))) {
      return 0;
   }
   return Data;
} // FileWordRead

void FileByteWrite( TFile *File, TFileAddress Address, byte Value)
//
{
   FileSave( File, Address, &Value, sizeof(byte));
} // FileByteRead

//------------------------------------------------------------------------------
//  Save
//------------------------------------------------------------------------------

TYesNo FileSave( TFile *File, TFileAddress Address, const void *Data, int Size)
// Save <Data> with <Size> at <Address>
{
   if(!CheckPosition(File, Address, Size)) {
      return NO;
   }
   return FileInterface[File->Item.Interface].Save(File, Address, Data, Size);
} // FileSave

//------------------------------------------------------------------------------
//   Load
//------------------------------------------------------------------------------

TYesNo FileLoad( TFile *File, TFileAddress Address, void *Data, int Size)
// Load <Data> with <Size> from <Address>
{
   if(!CheckPosition(File, Address, Size)) {
      return NO;
   }
   return FileInterface[File->Item.Interface].Load(File, Address, Data, Size);
} // FileLoad

//------------------------------------------------------------------------------
//   Fill
//------------------------------------------------------------------------------

TYesNo FileFill( TFile *File, TFileAddress Address, byte Pattern, int Size)
// Write <Pattern> with <Size> at <Address>
{
   if(!CheckPosition(File, Address, Size)) {
      return NO;
   }
   return FileInterface[File->Item.Interface].Fill(File, Address, Pattern, Size);
} // FileFill

//------------------------------------------------------------------------------
//   Match
//------------------------------------------------------------------------------

TYesNo FileMatch( TFile *File, TFileAddress Address, const void *Data, int Size)
// Compare <Data> with <Size> at <Address>
{
   if(!CheckPosition(File, Address, Size)) {
      return NO;
   }
   return FileInterface[File->Item.Interface].Match(File, Address, Data, Size);
} // FileMatch

//------------------------------------------------------------------------------
//   Sum
//------------------------------------------------------------------------------

TFileSum FileSum( TFile *File, TFileAddress Address, int Size)
// Sum NVM contents at <Address> with <Size>
{
   if(!CheckPosition(File, Address, Size)) {
      return NO;
   }
   return FileInterface[File->Item.Interface].Sum(File, Address, Size);
} // FileSum

//------------------------------------------------------------------------------
//   Move
//------------------------------------------------------------------------------

TYesNo FileMove( TFile *File, TFileAddress ToAddress, TFileAddress FromAddress, int Size)
// Move data <FromAddress> to <ToAddress> with <Size>. Check for overlapping
{
   if(!CheckPosition(File, ToAddress, Size)) {
      return NO;
   }
   if(!CheckPosition(File, FromAddress, Size)) {
      return NO;
   }
   return FileInterface[File->Item.Interface].Move(File, ToAddress, FromAddress, Size);
} // FileMove

//------------------------------------------------------------------------------
//   Clone
//------------------------------------------------------------------------------

#define FRAGMENT_SIZE      512

TYesNo FileClone( TFile *FileDest, TFile *FileSrc, TFileAddress Address, int Size)
// Copy <Size> data from <FileSrc> to <FileDest> starting from address <Address>
{
byte Buffer[FRAGMENT_SIZE];
int FragmentSize;
   if(!CheckPosition(FileDest, Address, Size)) {
      return NO;
   }
   if(!CheckPosition(FileSrc, Address, Size)) {
      return NO;
   }

   while(Size) {
      if(Size > FRAGMENT_SIZE) {
         FragmentSize = FRAGMENT_SIZE;
      } else {
         FragmentSize = Size;
      }
      if(!FileInterface[FileSrc->Item.Interface].Load(FileSrc, Address, Buffer, FragmentSize)) {
         return NO;
      }
      if(!FileInterface[FileDest->Item.Interface].Save(FileDest, Address, Buffer, FragmentSize)) {
         return NO;
      }
      Address += FragmentSize;
      Size -= FragmentSize;
   }
   return YES;
} // FileClone

//------------------------------------------------------------------------------
//   Commit
//------------------------------------------------------------------------------

TYesNo FileCommit( TFile *File)
// Commit sandboxed <File>
{
   return FileInterface[File->Item.Interface].Commit(File);
}

//******************************************************************************

//------------------------------------------------------------------------------
//   Check position
//------------------------------------------------------------------------------

static TYesNo CheckPosition( TFile *File, TFileAddress Address, int Size)
// 
{
   int i;
   TFileDescriptor *Descriptor = &FileDescriptor[File->Item.Name];
   
   if(Descriptor->Size < Address + Size) {
      return NO;
   }
   for(i = 0 ; i < FILE_HANDLE_COUNT ; i++) {
      if(FileHeap[i] == File) {
         return YES;
      }
   }
   return NO;
} // CheckPosition
