//*****************************************************************************
//
//   Nvm.h       Nonvolatile memory
//   Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Nvm_H__
   #define __Nvm_H__

#ifndef __NvmDef_H__
   #include "Memory/NvmDef.h"
#endif

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

#ifdef __WIN32__
   TYesNo NvmSetup( char *Ptr);
   // Setup NVM
#endif

#ifdef __cplusplus
   extern "C" {
#endif

#ifdef OPTION_REMOTE
   TYesNo NvmCache( TNvmAddress Address, int Size);
   // Cache <Size> from <Address>
#else
   #define NvmCache( Address, Size)      YES
#endif

void NvmInit( void);
// Initialize

void NvmShutdown( void);
// Shutdown

void NvmCommit( void);
// Permanently save data (flush cache)

void NvmFormat( void);
// Clear all data

//------------------------------------------------------------------------------

void NvmByteWrite( TNvmAddress Address, byte Value);
// Write <Value> at <Address>

void NvmWordWrite( TNvmAddress Address, word Value);
// Write <Value> at <Address>

void NvmDwordWrite( TNvmAddress Address, dword Value);
// Write <Value> at <Address>

byte NvmByteRead( TNvmAddress Address);
// Returns value from <Address>

word NvmWordRead( TNvmAddress Address);
// Returns value from <Address>

dword NvmDwordRead( TNvmAddress Address);
// Returns value from <Address>

//------------------------------------------------------------------------------

TYesNo NvmSave( TNvmAddress Address, const void *Data, int Size);
// Save <Data> with <Size> at <Address>

void NvmLoad( TNvmAddress Address, void *Data, int Size);
// Load <Data> with <Size> from <Address>

TYesNo NvmFill( TNvmAddress Address, byte Pattern, int Size);
// Write <Pattern> with <Size> at <Address>

TYesNo NvmMatch( TNvmAddress Address, const void *Data, int Size);
// Compare <Data> with <Size> at <Address>

TNvmSum NvmSum( TNvmAddress Address, int Size);
// Sum NVM contents at <Address> with <Size>

void NvmMove( TNvmAddress ToAddress, TNvmAddress FromAddress, int Size);
// Move data <FromAddress> to <ToAddress> with <Size>. Check for overlapping

//------------------------------------------------------------------------------
#ifdef __cplusplus
   }
#endif

#endif
