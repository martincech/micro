//******************************************************************************
//
//   mempool.h    Memory pool implementation (malloc etc)
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************
#ifndef __MEMPOOL_H__
#define __MEMPOOL_H__

typedef long TMempoolDesc;

TMempoolDesc MempoolInit( void *PoolPtr, unsigned long PoolSize);
   // init new mempool and return its descriptor

void MempoolRelease( TMempoolDesc mempool);
   // release mempool

void *MempoolMalloc( TMempoolDesc mempool, unsigned nbytes);
   // malloc nbytes of data, return its descriptor

void MempoolFree( TMempoolDesc mempool, void *ap);
   // free previously malloced data

long MempoolFreeStatus(TMempoolDesc mempool);
   // return current free memory size

long MempoolMallocMax(TMempoolDesc mempool);
   // return maximum size of possible allocated chunk

#endif
