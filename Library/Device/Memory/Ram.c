//*****************************************************************************
//
//   Ram.c       RAM memory
//   Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#include "Ram.h"
#include "Memory/RamDef.h"
#include <string.h>

TRam Ram;
/*
//------------------------------------------------------------------------------
//  Save
//------------------------------------------------------------------------------

void RamSave( dword Address, const void *Data, int Size)
// Save <Data> with <Size> at <Address>
{
   if(Address + Size > sizeof(TRam)) {
      return;
   }
   memcpy(&Ram + Address, Data, Size);
} // RamSave
*/
//------------------------------------------------------------------------------
//  Load
//------------------------------------------------------------------------------

void RamLoad( dword Address, void *Data, int Size)
// Load <Data> with <Size> from <Address>
{
   if(Address + Size > sizeof(TRam)) {
      return;
   }
   memcpy(Data, &Ram + Address, Size);
} // RamLoad