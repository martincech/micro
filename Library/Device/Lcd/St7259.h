//*****************************************************************************
//
//    St7259.h      ST7259 generic graphic controller services
//    Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __St7259_H__
   #define __St7259_H__

#ifndef __Gpu_H__
   #include "Graphic/Gpu.h"            // initialize & flush
#endif

// GpuGoto range :
#define GPU_MAX_ROW       160          // rows count
#define GPU_MAX_COLUMN    80           // column triples

#define GPU_COLOR_BLACK      0x00
#define GPU_COLOR_DARKGRAY   0x40
#define GPU_COLOR_LIGHTGRAY  0x80
#define GPU_COLOR_WHITE      0xF8

//-----------------------------------------------------------------------------
// Extended GPU functions
//-----------------------------------------------------------------------------

void GpuOn( void);
// Display On

void GpuOff( void);
// Display Off

void GpuClear( void);
// Clear display

void GpuGoto( int Row, int Column);
// Set coordinate to <Row>,<Column>

void GpuWrite( unsigned Pattern);
// Write <Pattern> to current coordinate (increment <Column>)

void GpuDone( void);
// Write done

void GpuContrast( unsigned Value);
// Set display contrast

void GpuShutdown( void);
// Power off sequence

void GpuInitBuffer( void);
// Buffer initialize

#endif
