//*****************************************************************************
//
//   Ds2788.c       Ds2788 fuel gauge
//   Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#include "Accu/Accu.h"
#include "ds2788OneWire.h"
#include "Cpu/Cpu.h"

#define DS2788_READ     0x69
#define DS2788_WRITE    0x6C
#define DS2788_COPY     0x48
#define DS2788_RECALL   0xB8
#define DS2788_LOCK     0x6A

#define DS2788_CURRENT_RESOLUTION_PER_MOHM      15625 // 100nA
#define DS2788_VOLTAGE_RESOLUTION               4880 // uV
#define DS2788_CURRENT_RESOLUTION               DS2788_CURRENT_RESOLUTION_PER_MOHM / DS2788_CURRENT_SHUNT


#define DS2788_CURRENT_SHUNT     15 // mOhms

#define ACCU_PERIOD     10


#define CounterSet()    Counter = ACCU_PERIOD

static byte Counter;
static byte CapacityRemaining;

static TYesNo ReadByte( byte Address, byte *Data);
// Read byte

static TYesNo ReadWord( byte Address, word *Data);
// Read word

static byte _AccuCapacityRemaining( void);
// Remaining capacity %

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void AccuInit( void)
// Initialization
{
   ds2788OneWireInit();
   AccuExecute();
} // AccuInit

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

void AccuExecute( void)
// Execute
{
   if(Counter--) {
      return;
   }
   CounterSet();
   CapacityRemaining = _AccuCapacityRemaining();
} // AccuExecute

//-----------------------------------------------------------------------------
// Ready
//-----------------------------------------------------------------------------

TYesNo AccuReady( void)
// Ready
{
   return ds2788OneWireReset();
} // AccuCapacityRemaining

//-----------------------------------------------------------------------------
// Capacity
//-----------------------------------------------------------------------------

byte AccuCapacityRemaining( void)
// Remaining capacity %
{
   return CapacityRemaining;
} // AccuCapacityRemaining

//-----------------------------------------------------------------------------
// Voltage
//-----------------------------------------------------------------------------

word AccuVoltage( void)
// Voltage mV
{
word VoltageNative;
word Voltage;
   if(!ReadWord(0x0C, &VoltageNative)) {
      return 0;
   }
   Voltage = ((dword)(VoltageNative >> 5) * DS2788_VOLTAGE_RESOLUTION) / 1000;
   return Voltage;
} // AccuVoltage

//-----------------------------------------------------------------------------
// Current
//-----------------------------------------------------------------------------

int16 AccuCurrent( void)
// Current 100uA
{
int16 CurrentNative;
int16 Current;
   if(!ReadWord(0x0E, &CurrentNative)) {
      return 0;
   }
   Current = ((int32)CurrentNative * DS2788_CURRENT_RESOLUTION) / 1000;
   return Current;
} // AccuCurrent

//-----------------------------------------------------------------------------
// Temperature
//-----------------------------------------------------------------------------

int8 AccuTemperature( void)
// Temperature �C
{
int16 TemperatureNative;
int8 Temperature;
   if(!ReadWord(0x0A, &TemperatureNative)) {
      return 0;
   }
   Temperature = TemperatureNative >> 8;
   return Temperature;
} // AccuTemperature

//******************************************************************************

//-----------------------------------------------------------------------------
// Capacity
//-----------------------------------------------------------------------------

static byte _AccuCapacityRemaining( void)
// Remaining capacity %
{
byte Cap;
   if(!ReadByte(0x06, &Cap)) {
      Cap = ACCU_CAPACITY_INVALID;
   }
   return Cap;
} // AccuCapacityRemaining

//-----------------------------------------------------------------------------
// Read byte
//-----------------------------------------------------------------------------

static TYesNo ReadByte( byte Address, byte *Data)
// Read byte
{
   if(!ds2788OneWireReset()) {
      return NO;
   }
   ds2788OneWireByteWrite(ONE_WIRE_CMD_SKIP_ROM);
   ds2788OneWireByteWrite(DS2788_READ);
   ds2788OneWireByteWrite(Address);
   *Data = ds2788OneWireByteRead();
   return YES;
} // ReadByte

//-----------------------------------------------------------------------------
// Read word
//-----------------------------------------------------------------------------

static TYesNo ReadWord( byte Address, word *Data)
// Read word
{
   if(!ds2788OneWireReset()) {
      return NO;
   }
   ds2788OneWireByteWrite(ONE_WIRE_CMD_SKIP_ROM);
   ds2788OneWireByteWrite(DS2788_READ);
   ds2788OneWireByteWrite(Address);
   *Data = ds2788OneWireByteRead() << 8;
   *Data |= ds2788OneWireByteRead();
   *Data = ENDIAN_FROM_LITTLE_WORD(*Data);
   return YES;
} // ReadWord