//*****************************************************************************
//
//    OneWire.c          1-Wire bus services
//    Version 1.0        (c) Veit Electronics
//
//*****************************************************************************

#include "OneWire.h"
#include "System/System.h"
#include "Cpu/Cpu.h"
#include "Hardware.h"

// Overdrive speed
#define DELAY_RESET_LOW             70
#define DELAY_RESET_PRESENCE_HIGH   9
#define DELAY_RESET_HIGH            40

#define DELAY_WRITE_ONE_PULL        1
#define DELAY_WRITE_ONE_RELEASE     8

#define DELAY_WRITE_ZERO_PULL        8
#define DELAY_WRITE_ZERO_RELEASE     2

#define DELAY_READ_PULL              1
#define DELAY_READ_WAIT              1
#define DELAY_READ_RELEASE           7

static TYesNo ReadBit( byte Channel);

static void WriteBit( byte Channel, TYesNo value);

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void OneWireInit( void)
// Initialization
{
   OneWirePortInit();
} // OneWireInit

//-----------------------------------------------------------------------------
// Reset
//-----------------------------------------------------------------------------

TYesNo OneWireReset( TOneWireChannel Channel)
// Resets bus, returns YES if device present
{
TYesNo Present;

   if( !OneWireRead( Channel)){
      return NO;      // trvaly zkrat
   }
   InterruptDisable();
   OneWireSetOutput( Channel);
   SysUDelay(DELAY_RESET_LOW);
   OneWireSetInput( Channel);
   SysUDelay(DELAY_RESET_PRESENCE_HIGH);
   Present = !OneWireRead( Channel);
   InterruptEnable();
   
   SysUDelay(DELAY_RESET_HIGH);
   return( Present);
} // OneWireReset

//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------

byte OneWireByteRead( TOneWireChannel Channel)
// read byte
{
byte i;
byte value;

   value = 0;
   for( i = 0; i < 8; i++){
      value >>= 1;
      if( ReadBit( Channel)){
         value |= 0x80;
      }
   }
   return( value);
} // OneWireByteRead

//-----------------------------------------------------------------------------
// Write
//-----------------------------------------------------------------------------

void OneWireByteWrite( TOneWireChannel Channel, byte Value)
// Write byte
{
byte i;

   for( i = 0; i < 8; i++){
      WriteBit( Channel, Value & 1);
      Value >>= 1;
   }
} // OneWireByteWrite

//******************************************************************************

//-----------------------------------------------------------------------------
// Read bit
//-----------------------------------------------------------------------------

static TYesNo ReadBit( byte Channel)
{
byte i;
TYesNo  value;

   InterruptDisable();                       // kriticka sekce casovani
   OneWireSetOutput( Channel);
   SysUDelay(DELAY_READ_PULL);
   OneWireSetInput( Channel);
   SysUDelay(DELAY_READ_WAIT);
   value = OneWireRead( Channel);                          // precteni hodnoty
   InterruptEnable();                        // konec kriticke sekce

   SysUDelay(DELAY_READ_RELEASE);
   return( value);
} // ReadBit

//-----------------------------------------------------------------------------
// Write bit
//-----------------------------------------------------------------------------

static void WriteBit( byte Channel, TYesNo value)
{
byte TimeA;
byte TimeB;
   if(value) {
      TimeA = DELAY_WRITE_ONE_PULL;
      TimeB = DELAY_WRITE_ONE_RELEASE;
   } else {
      TimeA = DELAY_WRITE_ZERO_PULL;
      TimeB = DELAY_WRITE_ZERO_RELEASE;
   }

   InterruptDisable();
   OneWireSetOutput( Channel);
   SysUDelay(TimeA);
   OneWireSetInput( Channel);
   InterruptEnable(); 
   SysUDelay(TimeB);
} // WriteBit