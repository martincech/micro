//******************************************************************************
//
//   Sleep.h         Low power controller
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Sleep_H__
   #define __Sleep_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void SleepInit( void);
// Init

int SleepScheduler( void);
// Sleep scheduler

void SleepTimerSet(word Timer);
// Set wake up period

void SleepEnable( TYesNo Enable);
// <Enable> low power mode

void SleepDeep( void);
// Deep sleep, reset wake-up

#endif