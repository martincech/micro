//*****************************************************************************
//
//    SMBusSlave.c           SMBus
//    Version 1.0            (c) VEIT Electronics
//
//*****************************************************************************

#include "SMBusSlave.h"
#include "Iic/IicSlave.h"
#include "Hardware.h"
#include <string.h>

typedef enum {
   IDLE,
   SLAVE_SENDING,
   SLAVE_RECEIVING,
   ERROR
} ESMBusState;

typedef enum {
   FUNCTION_NONE,
   FUNCTION_WORD,
   FUNCTION_BLOCK
} EFunction;

static byte State;
static byte Function;
static byte InternalBuffer[sizeof(word)];
static byte *Buffer;
static int Length;
static int ProcessedLength;
static TYesNo CountReceived;
void SMBusSlaveInit( void) {
   IicSlaveInit();
   SMBusEnableInit();
   State = IDLE;
   Function = FUNCTION_NONE;
   SMBusEnable();
}



void SMBusSlaveWriteWord(word Data)
// Use only synchronously with SMBus callbacks
{
   memcpy(InternalBuffer, &Data, sizeof(Data));
   Buffer = InternalBuffer;
   Length = sizeof(Data);
   ProcessedLength = 0;
   Function = FUNCTION_WORD;
}

void SMBusSlaveWriteBlock(void *Data, int Size)
// Use only synchronously with SMBus callbacks
{
   Buffer = Data;
   Length = Size;
   ProcessedLength = 0;
   CountReceived = NO;
   Function = FUNCTION_BLOCK;
}

void SMBusSlaveReadWord( word *Data)
// Use only synchronously with SMBus callbacks
{
   Buffer = Data;
   Length = sizeof(word);
   ProcessedLength = 0;
   Function = FUNCTION_WORD;
}

void SMBusSlaveReadBlock(void *Data, int MaxSize)
// Use only synchronously with SMBus callbacks
{
   Buffer = Data;
   Length = MaxSize;
   ProcessedLength = 0;
   CountReceived = NO;
   Function = FUNCTION_BLOCK;
}

int SMBusSlaveReadLength( void)
// Read length
{
   return Length;   
}

void IicSlaveAddressed( TYesNo Rw)
// Addressed callback
{
   switch( State) {
      case IDLE:
      case SLAVE_RECEIVING:
      case SLAVE_SENDING:
         if(Rw) {
            State = SLAVE_RECEIVING;
         } else {
            State = SLAVE_SENDING;
         }
         switch(State) {
            case SLAVE_RECEIVING:
               SMBusSlaveReceiving();
               break;
               
            case SLAVE_SENDING:
               SMBusSlaveSending();
         }
         break;
      
      default:
         State = ERROR;
         SMBusSlaveError();
         break;
   }
}

void IicSlaveByteProcess(byte Data)
// Byte proccess (on I2C receive) callback
{
   int Count;
   switch( State) {
      case SLAVE_RECEIVING:
         switch(Function) {
            case FUNCTION_NONE:
               SMBusSlaveByteProcess( Data);
               break;
               
            case FUNCTION_BLOCK:
               if(!CountReceived) { // first received byte is length
                  Count = Data;
                  if(Count > Length) {
                     State = ERROR;
                     SMBusSlaveError();
                     break;
                  }
                  CountReceived = YES;
                  Length = Count;
                  break;
               }
               
            default:
               if(ProcessedLength < Length) {
                  Buffer[ProcessedLength++] = Data;
               } else {
                  State = ERROR;
                  SMBusSlaveError();
               }
               break;
         }
         break;

      default:
         State = ERROR;
         SMBusSlaveError();
         break;
   }
}

TYesNo IicSlaveByteGet( byte *Data)
// Byte get (before I2C send) callback
{
   switch( State) {
      case SLAVE_SENDING:
         switch(Function) {
            case FUNCTION_NONE:
               return SMBusSlaveByteGet( Data);
            
            case FUNCTION_BLOCK:
               if(!CountReceived) { // first byte to send is length
                  *Data = Length;
                  CountReceived = YES;
                  return YES;
               }
            
            default:
               if(ProcessedLength < Length) {
                  *Data = Buffer[ProcessedLength++];
                  return YES;
               }
               State = ERROR;
               SMBusSlaveError();
               return NO;
         }
         break;
         
      default:
         State = ERROR;
         SMBusSlaveError();
         return NO;
   }
}



void IicSlaveStopped( void)
// Stop callback
{
   if(State == ERROR) {
      Function = FUNCTION_NONE;;
      State = IDLE;
      return;
   }
   State = IDLE;
   switch(Function) {
      case FUNCTION_NONE:
         break;

      default:
         Function = FUNCTION_NONE;
         if(ProcessedLength == Length) {
            SMBusSlaveSuccess();
         } else {
            SMBusSlaveError();
         }
         break;
   }
}