//*****************************************************************************
//
//    SMBusMaster.c          SMBus
//    Version 1.0            (c) VEIT Electronics
//
//*****************************************************************************

#include "SMBus.h"
#include "Iic/Iic.h"
#include "Hardware.h"

// Hardware.h
#define SMBUS_ADDRESS   0x55

// SMBusIic.h
#define SMBusIicInit()        IicInit( SMBUS_IIC_CHANNEL)
#define SMBusIicStart()       IicStart( SMBUS_IIC_CHANNEL)
#define SMBusIicSend( Data)   IicSend( SMBUS_IIC_CHANNEL, Data)
#define SMBusIicReceive( Ack) IicReceive( SMBUS_IIC_CHANNEL, Ack)
#define SMBusIicStop()        IicStop( SMBUS_IIC_CHANNEL)



//
#define BusStartByte( Address, Read)   IicStartByte( Address, Read)

#define BUS_READ  IIC_READ
#define BUS_WRITE IIC_WRITE


void SMBusInit( void) {
   SMBusIicInit();
}


TYesNo SMBusWriteWord(byte Cmd, word Data) {
   SMBusIicStart();
   if(!SMBusIicSend( BusStartByte(SMBUS_ADDRESS, BUS_WRITE))) {
      SMBusIicStop();
      return NO;
   }
   if(!SMBusIicSend( Cmd)) {
      SMBusIicStop();
      return NO;
   }
   if(!SMBusIicSend( (byte) Data)) {
      SMBusIicStop();
      return NO;
   }
   if(!SMBusIicSend( (byte) (Data >> 8))) {
      SMBusIicStop();
      return NO;
   }
   SMBusIicStop();
   return YES;
}

TYesNo SMBusReadWord(byte Cmd, word *Data) {
   SMBusIicStart();
   if(!SMBusIicSend( BusStartByte(SMBUS_ADDRESS, BUS_WRITE))) {
      SMBusIicStop();
      return NO;
   }
   if(!SMBusIicSend( Cmd)) {
      SMBusIicStop();
      return NO;
   }
   SMBusIicStart();
   if(!SMBusIicSend( BusStartByte(SMBUS_ADDRESS, BUS_READ))) {
      SMBusIicStop();
      return NO;
   }
   *Data = SMBusIicReceive( YES);
   *Data |= SMBusIicReceive( NO) << 8;
   SMBusIicStop();
   return YES;
}

int SMBusReadBlock(byte Cmd, void *Data) {
int Length;
int i;
TYesNo Ack;
byte *Buffer;
   SMBusIicStart();
   if(!SMBusIicSend( BusStartByte(SMBUS_ADDRESS, BUS_WRITE))) {
      SMBusIicStop();
      return NO;
   }
   if(!SMBusIicSend( Cmd)) {
      SMBusIicStop();
      return NO;
   }
   SMBusIicStart();
   if(!SMBusIicSend( BusStartByte(SMBUS_ADDRESS, BUS_READ))) {
      SMBusIicStop();
      return NO;
   }
   Length = SMBusIicReceive( YES);
   if(!Length) {
      SMBusIicStop();
      return 0;
   }
   Buffer = Data;
   i = Length;
   while(--i) {
      *Buffer = SMBusIicReceive( YES);
      Buffer++;
   }
   *Buffer = SMBusIicReceive( NO);
   SMBusIicStop();
   return Length;
}