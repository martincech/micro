//*****************************************************************************
//
//    SmartBatteryDef.h    Smart battery definitions
//    Version 1.0          (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __SmartBatteryDef_H__
   #define __SmartBatteryDef_H__

#define SMART_BATTERY_ADDRESS   0x55

#define SMART_BATTERY_MANUFACTURER_ACCESS       0x00
#define SMART_BATTERY_REMAINING_CAPACITY_ALARM  0x01
#define SMART_BATTERY_REMAINING_TIME_ALARM      0x02
#define SMART_BATTERY_BATTERY_MODE              0x03
#define SMART_BATTERY_AT_RATE                   0x04
#define SMART_BATTERY_AT_RATE_TIME_TO_FULL      0x05
#define SMART_BATTERY_AT_RATE_TIME_TO_EMPTY     0x06
#define SMART_BATTERY_AT_RATE_OK                0x07
#define SMART_BATTERY_TEMPERATURE               0x08
#define SMART_BATTERY_VOLTAGE                   0x09
#define SMART_BATTERY_CURRENT                   0x0A
#define SMART_BATTERY_AVERAGE_CURRENT           0x0B
#define SMART_BATTERY_MAX_ERROR                 0x0C
#define SMART_BATTERY_RELATIVE_STATE_OF_CHARGE  0x0D
#define SMART_BATTERY_ABSOLUTE_STATE_OF_CHARGE  0x0E
#define SMART_BATTERY_REMAINING_CAPACITY        0x0F
#define SMART_BATTERY_FULL_CHARGE_CAPACITY      0x10
#define SMART_BATTERY_RUN_TIME_TO_EMPTY         0x11
#define SMART_BATTERY_AVERAGE_TIME_TO_EMPTY     0x12
#define SMART_BATTERY_AVERAGE_TIME_TO_FULL      0x13
#define SMART_BATTERY_CHARGING_CURRENT          0x14
#define SMART_BATTERY_CHARGING_VOLTAGE          0x15
#define SMART_BATTERY_BATTERY_STATUS            0x16
#define SMART_BATTERY_CYCLE_COUNT               0x17
#define SMART_BATTERY_DESIGN_CAPACITY           0x18
#define SMART_BATTERY_DESIGN_VOLTAGE            0x19
#define SMART_BATTERY_SPECIFICATION_INFO        0x1A
#define SMART_BATTERY_MANUFACTURE_DATE          0x1B
#define SMART_BATTERY_SERIAL_NUMBER             0x1C
#define SMART_BATTERY_MANUFACTURER_NAME         0x20
#define SMART_BATTERY_DEVICE_NAME               0x21
#define SMART_BATTERY_DEVICE_CHEMISTRY          0x22
#define SMART_BATTERY_MANUFACTURER_DATA         0x23
#define SMART_BATTERY_OPTIONAL_MFG_FUNCTION5    0x2F
#define SMART_BATTERY_OPTIONAL_MFG_FUNCTION4    0x2C
#define SMART_BATTERY_OPTIONAL_MFG_FUNCTION3    0x3D
#define SMART_BATTERY_OPTIONAL_MFG_FUNCTION2    0x3E
#define SMART_BATTERY_OPTIONAL_MFG_FUNCTION1    0x3F


#endif