//*****************************************************************************
//
//    SmartBatterySlave.c     Smart battery
//    Version 1.0            (c) VEIT Electronics
//
//*****************************************************************************

#include "SmartBattery.h"
#include "SmartBatterySlave.h"
#include "SmartBatteryDef.h"
#include "SMBus/SMBusSlave.h"
#include <string.h>

#define BLOCK_LENGTH_MAX        32



typedef union {
   word ManufacturerAccess;
   word RemainingCapacityAlarm;
   word RemainingTimeAlarm;
   word BatteryMode;
   word AtRate;
   byte ManufacturerName[BLOCK_LENGTH_MAX];
   byte DeviceName[BLOCK_LENGTH_MAX];
   byte DeviceChemistry[BLOCK_LENGTH_MAX];
   byte ManufacturerData[BLOCK_LENGTH_MAX];
} USmartBatteryCmd;



typedef struct {
   byte                Cmd;
   USmartBatteryCmd    Data;
} __packed TSmartBatteryCmd;



typedef struct {
   word ManufacturerAccess;
   word RemainingCapacityAlarm;
   word RemainingTimeAlarm;
   word BatteryMode;
   word AtRate;
   word AtRateTimeToFull;
   word AtRateTimeToEmpty;
   word AtRateOk;
   word Temperature;
   word Voltage;
   word Current;
   word AverageCurrent;
   word MaxError;
   word RelativeStateOfCharge;
   word AbsoluteStateOfCharge;
   word RemainingCapacity;
   word FullChargeCapacity;
} TSmartBatteryReply;





static TSmartBatteryCmd   Cmd;
static byte State;

typedef enum {
   IDLE,
   RECEIVING,
   SENDING
} TState;

void SmartBatteryInit( void)
// Init
{
   SMBusSlaveInit();
   State = IDLE;
}

void SMBusSlaveByteProcess( byte Data) {
   switch( State) {
      case RECEIVING:
         Cmd.Cmd = Data;
         switch(Cmd.Cmd) {
            case SMART_BATTERY_MANUFACTURER_ACCESS:
               break;
            
            case SMART_BATTERY_REMAINING_CAPACITY_ALARM:
               break;
            
            case SMART_BATTERY_REMAINING_TIME_ALARM:
               break;
            
            case SMART_BATTERY_BATTERY_MODE:
              break;
            
            case SMART_BATTERY_AT_RATE:
               break;
            
            case SMART_BATTERY_OPTIONAL_MFG_FUNCTION5:
               break;
            
            case SMART_BATTERY_OPTIONAL_MFG_FUNCTION4:
               break;
            
            case SMART_BATTERY_OPTIONAL_MFG_FUNCTION3:
               break;
            
            case SMART_BATTERY_OPTIONAL_MFG_FUNCTION2:
               break;
            
            case SMART_BATTERY_OPTIONAL_MFG_FUNCTION1:
               break;
            
            default:
              break;
         }
         break;

      default:
         break;
   }
}

TYesNo SMBusSlaveByteGet( byte *Data) {
   State = IDLE;
   return NO;
}

void SMBusSlaveReceiving( void) {
   State = RECEIVING;
}

void SMBusSlaveSending( void) {
word Data;
int Length;
   State = SENDING;
   switch(Cmd.Cmd) {
      case SMART_BATTERY_MANUFACTURER_ACCESS:
         break;
      
      case SMART_BATTERY_REMAINING_CAPACITY_ALARM:
         break;
      
      case SMART_BATTERY_REMAINING_TIME_ALARM:
         break;
         
      case SMART_BATTERY_BATTERY_MODE:
         break;
         
      case SMART_BATTERY_AT_RATE:
        break;
        
      case SMART_BATTERY_AT_RATE_TIME_TO_FULL:
         break;
         
      case SMART_BATTERY_AT_RATE_TIME_TO_EMPTY:
         break;
         
      case SMART_BATTERY_AT_RATE_OK:
         break;

      case SMART_BATTERY_TEMPERATURE:
         if(!SBTemperature( &Data)) {
            break;
         }
         SMBusSlaveWriteWord( Data);
         break;
      
      case SMART_BATTERY_VOLTAGE:
         if(!SBVoltage( &Data)) {
            break;
         }
         SMBusSlaveWriteWord( Data);
         break;
            
      case SMART_BATTERY_CURRENT:
         if(!SBCurrent( &Data)) {
            break;
         }
         SMBusSlaveWriteWord( Data);
         break;
            
      case SMART_BATTERY_AVERAGE_CURRENT:
         break; 
          
      case SMART_BATTERY_MAX_ERROR:
         break;
         
      case SMART_BATTERY_RELATIVE_STATE_OF_CHARGE:
         if(!SBRelativeStateOfCharge( &Data)) {
            break;
         }
         SMBusSlaveWriteWord( Data);
         break;
         
      case SMART_BATTERY_ABSOLUTE_STATE_OF_CHARGE:
         break;
      
      case SMART_BATTERY_REMAINING_CAPACITY:
         break;
      
      case SMART_BATTERY_FULL_CHARGE_CAPACITY:
         break;
      
      case SMART_BATTERY_RUN_TIME_TO_EMPTY:
         break;
      
      case SMART_BATTERY_AVERAGE_TIME_TO_EMPTY:
         break;
         
      case SMART_BATTERY_AVERAGE_TIME_TO_FULL:
         break;
         
      case SMART_BATTERY_CHARGING_CURRENT:
         break;
         
      case SMART_BATTERY_CHARGING_VOLTAGE:
         break;
      
      case SMART_BATTERY_BATTERY_STATUS:
         break;
      
      case SMART_BATTERY_CYCLE_COUNT:
         break;
      
      case SMART_BATTERY_DESIGN_CAPACITY:
         break;
      
      case SMART_BATTERY_DESIGN_VOLTAGE:
         break;
      
      case SMART_BATTERY_SPECIFICATION_INFO:
         break;
      
      case SMART_BATTERY_MANUFACTURE_DATE:
         break;
      
      case SMART_BATTERY_SERIAL_NUMBER:
         break;
      
      case SMART_BATTERY_MANUFACTURER_NAME:
         if(!SBManufacturerName(Cmd.Data.ManufacturerName)) {
            break;
         }
         Length = strlen(Cmd.Data.ManufacturerName);
         SMBusSlaveWriteBlock( Cmd.Data.ManufacturerName, Length);
         break;
      
      case SMART_BATTERY_DEVICE_NAME:
         if(!SBDeviceName(Cmd.Data.DeviceName)) {
            break;
         }
         Length = strlen(Cmd.Data.DeviceName);
         SMBusSlaveWriteBlock( Cmd.Data.DeviceName, Length);
         break;
      
      case SMART_BATTERY_DEVICE_CHEMISTRY:
         if(!SBDeviceChemistry(Cmd.Data.DeviceChemistry)) {
            break;
         }
         Length = strlen(Cmd.Data.DeviceChemistry);
         SMBusSlaveWriteBlock( Cmd.Data.DeviceChemistry, Length);
         break;
      
      case SMART_BATTERY_MANUFACTURER_DATA:
         Length = SBManufacturerData(Cmd.Data.ManufacturerData);
         if(Length == 0) {
            break;
         }
         SMBusSlaveWriteBlock( Cmd.Data.ManufacturerData, Length);
         break;
      
      case SMART_BATTERY_OPTIONAL_MFG_FUNCTION5:
         break;
      
      case SMART_BATTERY_OPTIONAL_MFG_FUNCTION4:
         break;
      
      case SMART_BATTERY_OPTIONAL_MFG_FUNCTION3:
         break;
      
      case SMART_BATTERY_OPTIONAL_MFG_FUNCTION2:
         break;
      
      case SMART_BATTERY_OPTIONAL_MFG_FUNCTION1:
         break;

      default:
         break;
   }
}

void SMBusSlaveSuccess( void) {
   switch(State) {
      case SENDING:
         break;
      
      case RECEIVING:
         switch(Cmd.Cmd) {
            case SMART_BATTERY_MANUFACTURER_ACCESS:
               break;
            
            case SMART_BATTERY_REMAINING_CAPACITY_ALARM:
               break;
            
            case SMART_BATTERY_REMAINING_TIME_ALARM:
               break;
            
            case SMART_BATTERY_BATTERY_MODE:
               break;
            
            case SMART_BATTERY_AT_RATE:
               break;
               
            case SMART_BATTERY_OPTIONAL_MFG_FUNCTION5:
               break;
               
            case SMART_BATTERY_OPTIONAL_MFG_FUNCTION4:
               break;
               
            case SMART_BATTERY_OPTIONAL_MFG_FUNCTION3:
               break;
               
            case SMART_BATTERY_OPTIONAL_MFG_FUNCTION2:
               break;
               
            case SMART_BATTERY_OPTIONAL_MFG_FUNCTION1:
               break;
               
            default:
               break;
         }
         break;
         
      default:
         break;
   }
   State = IDLE;
}

void SMBusSlaveError( void) {
   State = IDLE;
}