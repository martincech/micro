//*****************************************************************************
//
//    SmartBatteryMaster.c    Smart battery
//    Version 1.0            (c) VEIT Electronics
//
//*****************************************************************************

#include "SmartBattery.h"
#include "SmartBatteryDef.h"
#include "SMBus/SMBus.h"

//-----------------------------------------------------------------------------
//  Init
//-----------------------------------------------------------------------------

void SBInit( void) {
   SMBusInit();
} // SBInit

//-----------------------------------------------------------------------------
//  Relative state of charge
//-----------------------------------------------------------------------------

TYesNo SBVoltage( word *Voltage)
// Voltage
{
   return SMBusReadWord(SMART_BATTERY_VOLTAGE, Voltage);
} // SBVoltage

TYesNo SBCurrent( word *Current)
// Current
{
   return SMBusReadWord(SMART_BATTERY_CURRENT, Current);
} // SBCurrent

TYesNo SBTemperature( word *Temperature)
// Current
{
   return SMBusReadWord(SMART_BATTERY_TEMPERATURE, Temperature);
} // SBTemperature

TYesNo SBRelativeStateOfCharge( word *State)
// Relative state of charge
{
   return SMBusReadWord(SMART_BATTERY_RELATIVE_STATE_OF_CHARGE, State);
} // SBRelativeStateOfCharge


TYesNo SBManufacturerName( byte *Name)
// Manufacturer name
{
int Length;
   Length = SMBusReadBlock(SMART_BATTERY_MANUFACTURER_NAME, Name);
   if(Length == 0) {
      return NO;
   }
   Name[Length] = '\0';
   return YES;
} // SBManufacturerName


TYesNo SBDeviceChemistry( byte *Name)
// Device chemistry
{
int Length;
   Length = SMBusReadBlock(SMART_BATTERY_DEVICE_CHEMISTRY, Name);
   if(Length == 0) {
      return NO;
   }
   Name[Length] = '\0';
   return YES;
} // SBDeviceChemistry


TYesNo SBDeviceName( byte *Name)
// Device name
{
int Length;
   Length = SMBusReadBlock(SMART_BATTERY_DEVICE_NAME, Name);
   if(Length == 0) {
      return NO;
   }
   Name[Length] = '\0';
   return YES;
} // SBDeviceName


int SBManufacturerData( byte *Data)
// Manufacturer data
{
   return SMBusReadBlock(SMART_BATTERY_DEVICE_NAME, Data);
} // SBManufacturerData

