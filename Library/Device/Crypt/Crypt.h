//******************************************************************************
//
//   Crypt.h       Crypt
//   Version 1.0   (c) Veit Electronics
//
//******************************************************************************

#ifndef __Crypt_H__
   #define __Crypt_H__

#include "Unisys/Uni.h"
#include "Crypt/CryptConfig.h"

#ifdef __cplusplus
   extern "C" {
#endif

void CryptInit( void);
// Init

dword EncryptSize(dword DataSize);
// Length of encrypted data, originally <DataSize>

void Encrypt(void *Data, dword Length);
// Encrypt

void Decrypt(void *Data, dword Length);
// Decrypt

#ifdef __cplusplus
   }
#endif

#endif 

