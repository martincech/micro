//******************************************************************************
//
//   CryptConfig.h  Crypt config
//   Version 1.0   (c) Veit Electronics
//
//******************************************************************************

#ifndef __CryptConfig_H__
   #define __CryptConfig_H__

#define CRYPT_BLOCK_SIZE         16

#endif