//******************************************************************************
//
//   GsmDef.h     GSM data types
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "GsmDefPrv.h"
#include <string.h>


TGsmChannelParameters GsmChannelParams = {
   /* FirstSmsTimeout */ 5,
   /* FirstSmsRetries */ 1,
   /* SmsRetries */      3,
   /* AckSmsTimeout*/    3,
   /* StartValue */      2,
   /* alpha */           0.3,
   /* beta */            3,
   /* gama */            2
};
static int _CalcDa( byte* da)
// calculate DA field size
{
   // add DA length and TON/NPI field to actuall length
   // add one to DA whem odd, divide by 2->result size of semi-octets in bytes
   return ((da[0] + (da[0]&1)) >> 1) + 2;
}

int TSmsSubmitToRaw( TSmsSubmit *packet)
// convert structure representation to raw byte representation, return raw size
{
int  size;
int  i;

   //calc Da field size
   i = _CalcDa( packet->Da);
   i += packet->Da - &packet->Fo;//add Fo + Mr size
   //calc Ud field size
   if( packet->Dcs & SMS_DCS_8BIT){
      size = packet->Udl;
   } else if (packet->Dcs & SMS_DCS_UCS2){
      size = packet->Udl/2;
   } else {
      size = packet->Udl*7;
      size = (size >> 3) + ((size & 7) == 0? 0 : 1);//add one when nonzero mod
   }
   size += packet->Ud - &packet->Pid; // add pid, dcs, vp, udl fields
   memmove((&packet->Fo + i), &packet->Pid, size);
   return size + i;
}

int TSmsCommandToRaw(TSmsCommand *packet)
// convert structure representation to raw byte representation, return raw size
{
int  i;

   //calc Da field size
   i = _CalcDa( packet->Da);
   i += packet->Da - &packet->Fo;//add Fo + Mr size
   memmove((&packet->Fo + i), &packet->Cdl, packet->Cdl + 1);
   return packet->Cdl + 1 + i;
}

void TSmsRawToDeliver(TSmsDeliver *packet, int rawSize)
// convert raw representation to structure representation
{
int  i;

   //calc Da field size
   i = _CalcDa( packet->Oa);
   memmove(&packet->Pid, &packet->Oa[i], rawSize - i - sizeof(packet->Fo));
   memset(&packet->Oa[i], 0, SMS_ADDRESS_SIZE - i);
}

void TSmsRawToStatusReport(TSmsStatusReport *packet, int rawSize)
// convert raw representation to structure representation
{
int  i;

   //calc Da field size
   i = _CalcDa( packet->Ra);
   memmove(packet->Scts, &packet->Ra[i], rawSize - i - (packet->Ra - &packet->Fo));
   memset(&packet->Ra[i], 0, SMS_ADDRESS_SIZE - i);
}
