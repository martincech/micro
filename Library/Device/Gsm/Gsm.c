//******************************************************************************
//
//   GsmBase.c    GSM services
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "Gsm.h"
#include "GsmDefPrv.h"
#include "System/System.h"        // SysClock only
#include "Uart/UartModem.h"       // Modem serial port
#include "Parse/uParse.h"         // Text parser
#include "Multitasking/Multitasking.h"
#include "Convert/uBcd.h"
#include "Debug/uDebug.h"
#include <string.h>
#include <assert.h>
#include <stdio.h>

#ifdef __WIN32__
#include <windows.h>

#define bprintf           sprintf
#define bvprintf          vsprintf
#define SysDelay( ms)     Sleep( ms)
#else
#include "System/System.h"        // SysDelay
#include "Console/conio.h"        // text console
#endif

#define TIMEOUT_DEFAULT         500          // standard command timeout [ms]
#define TIMEOUT_LONGER_DEFAULT 1000          // standard command timeout [ms]
#define TIMEOUT_MEMORY_SET     5000          // memory switch timeout [ms]
#define TIMEOUT_CONFIG_SET     5000          // gsm phase selection
#define TIMEOUT_SMS_PROMPT     5000          // SMS send prompt [ms]
#define TIMEOUT_SHUTDOWN      10000
#define TIMEOUT_SMS_SEND      30000          // SMS send timeout [ms]
#define TIMEOUT_SMS_READ      30000          // SMS read timeout [ms]
#define TIMEOUT_SMS_DELETE    30000          // SMS delete timeout [ms]

#define _ReplyCheck( reply) /* check for reply : */    \
   if( !_ReplyMatch( reply)){ \
   _ReplyTimeoutDefault(); \
   return( NO);            \
   }

#define CONNECTION_PROFILE_ID 0

#define KEYWORD_SOCKET        "Socket"
#define KEYWORD_URC_MODE      "Tcp/WithURCs"
#define KEYWORD_ON            "on"
#define KEYWORD_OFF           "off"
#define KEYWORD_TCP_KEEPIDLE  ";keepidle="
#define KEYWORD_TCP_KEEPCNT   ";keepcnt="
#define KEYWORD_TCP_KEEPINTVL ";keepintvl="
//11 for ascii decimal number sizes(2xbyte + 1xWord)
#define TCP_PARAM_MAX_LEN    (sizeof(KEYWORD_TCP_KEEPIDLE) + sizeof(KEYWORD_TCP_KEEPCNT) + sizeof(KEYWORD_TCP_KEEPINTVL) + 11)
#define TCP_CLIENT_PREFIX  "socktcp://"
#define UDP_CLIENT_PREFIX  "sockudp://"

#define PARAMS_DELIMITER  ','
#define PORT_DELIMITER    ':'
#define LINE_DELIMITER    '\r'

// message terminator :
#define CTRL_Z  '\x1A'
#define ESC     '\x1B'
#define _ReplyTimeoutSet( t)     UartModemTimeoutSet( t)
#define _ReplyTimeoutDefault()   UartModemTimeoutSet( TIMEOUT_DEFAULT);
#define _ReplyTimeoutLDefault()  UartModemTimeoutSet( TIMEOUT_LONGER_DEFAULT);

// socket command parameter
enum SOCKETParamsPos{
   //common param when not removed
   PARAM_PROFILE_ID = 0,
   //SISO parameters
   PARAM_SRV_TYPE = 0,
   PARAM_SRV_STATE,
   PARAM_SOCKET_STATE,
   PARAM_RX_COUNT,
   PARAM_TX_COUNT,
   PARAM_LOCAL_ADDR,
   PARAM_REMOTE_ADDR,
   PARAM_REJECTED_COUNTER,
   //SCFG parameters
   PARAM_PARAM_NAME = 0,
   PARAM_PARAM_VALUE,
   //SISW parameters
   PARAM_WRITE_LEN = 0,
   //SISR parameters
   PARAM_READ_LEN = 0
};

// sms command parameter
enum SMSParamsPos{
   // AT+CMGS
   PARAM_CMGS_MR = 0,
   PARAM_CMGS_ACKPDU,
   // AT+CMGC
   PARAM_CMGC_MR = 0,
   // AT+CPMS
   PARAM_CPMS_R_MEM1 = 0,
   PARAM_CPMS_R_USED1,
   PARAM_CPMS_R_TOTAL1,
   PARAM_CPMS_R_MEM2,
   PARAM_CPMS_R_USED2,
   PARAM_CPMS_R_TOTAL2,
   PARAM_CPMS_R_MEM3,
   PARAM_CPMS_R_USED3,
   PARAM_CPMS_R_TOTAL3,
   PARAM_CPMS_W_USED1 = 0,
   PARAM_CPMS_W_TOTAL1,
   PARAM_CPMS_W_USED2,
   PARAM_CPMS_W_TOTAL2,
   PARAM_CPMS_W_USED3,
   PARAM_CPMS_W_TOTAL3,
   // AT+CMGR
   PARAM_CMGR_STAT = 0,
   PARAM_CMGR_LENGTH = 2,
   // URC codes params
   PARAM_CDSI_MEM3 = 0,
   PARAM_CDSI_INDEX,
   // CME ERROR code index
   PARAM_CME_ERROR_CODE = 0
};
// other commands parameters
enum OTHERParamPos{
   // +CMS ERROR:
   PARAM_CMS_ERROR_CODE = 0
};

typedef enum GsmMemoryLocation {
   GSM_MEM_SM,
   GSM_MEM_SR,
   GSM_MEM_ME
} TGsmMemoryLocation;

#ifdef __GSM_SMS_STATISTICS__
// statistics
TGsmSmsStatistics       GsmStatistics;
#endif
// Local variables
static char          _GsmBuffer[ GSM_BUFFER_SIZE];
static byte          _GsmMemorySize;

#define GMS_STATUS_MEM  GSM_MEM_SR
// constant strings :
static char REPLY_OK[]        = "\r\nOK\r\n";      // ok reply
static char MEM_STR[][5]      = {"SM","SR","ME"};
static char AT_CPMS[]         = "AT+CPMS";
static char AT_CNMI[]         = "AT+CNMI";
static char AT_CNMI_MODE[]    = "0,0,0,0,0";
static char AT_CSMS[]         = "AT+CSMS";
static char RESP_CPMS[]       = "\r\n+CPMS: ";
static char CMS_ERROR[]       = "\r\n+CMS ERROR: ";
static char PROMPT[]          = "\r\n> ";
static char SHUTDOWN[]        = "^SHUTDOWN";

// Local functions :
static void _CommandSend( char *Command);
// Send AT <Command>

static void _DataSend( char *Data, int Size);
// Send SMS <Data> with <Size>

static TYesNo _ReplyMatch( const char *Reply);
// Check for AT command <Reply> match

static TYesNo _ReplyGet(char **Reply);
// Get AT command <Reply>

static TYesNo _ReplyWait( void);
// Wait for reply received

static char *_ReplyPrefix( char *Reply, const char *Prefix);
// Check for <Reply> matches <Prefix> and removes prefix

static char *_ReplySkip( char *Reply, const char ch);
// Skip <Reply> up to character <ch>

static char *_ReplyNumber(char *Reply, signed *Number);
// Returns decimal <Number> from <Reply>

static char *_ReplyString( char *Reply, char *String, byte Size);
// Returns <String> with maximum <Size>

static TYesNo _ReplyGetParam( char* const paramString, int which, char**resFrom, char**resTo);
// find parameter in parameter string, return its position from->to

static char *_GsmNetSocketInfo(byte SockAddr);
// return socket parameters info (AT^SISO?) parameters

static char *_GsmNetValidParam( byte SockAddr, char *Reply);
// check params reply validity, return reply without first param or 0 if params are not for <sockaddr>

// enable URC reporting of status report
static TYesNo _GsmMemorySet(TGsmMemoryLocation to, int *currentUsed);
// set MEM1 memory location to <to>

static int _BufferToIra( byte *input, int inputSize, byte *output);
// convert pdu packet to IRA enchoding
static int _IraToBuffer(byte *data, int inputLen);
// convert IRA encoding to pdu packet

static void _GsmToAscii(char *c);
// from gsm alphabet to gsm alphabet
static void _AsciiToGsm(char *c);
// from asscii alphabet to gsm alphabet

#ifndef __WIN32__
static
#endif
int _TextToGsm( char *Text);
// Converts <Text> to GSM alphabet in sito, returns size

#ifndef __WIN32__
static
#endif
int _GsmToText(byte *GsmText, int Size, char *OutText);
// Converts <Text> with <Size> septets(7/8*byte) from GSM alphabet, return size in bytes

#ifndef __WIN32__
static
#endif
TYesNo _GsmSmRead( byte Address, byte **pdu, int *Length);
// Read SM from local storage
#ifndef __WIN32__
static
#endif
TYesNo _GsmSmsSubmit( TSmsSubmit *request, TSmsId *id);
// write submit pdu - send sms submit SM
#ifndef __WIN32__
static
#endif
TYesNo _GsmSmDelete( byte Address);
// Delete SM from local storage
#ifndef __WIN32__
static
#endif
TYesNo _GsmSmsCommand(TSmsId *id, ESmsCommandType ct);
// write command pdu - send command and return response
static void _TimeStampToTime(byte *ts, UDateTimeGauge *DateTime);
// convert SC timestamp to internal representation

static TYesNo PhoneBookRead(int Position, byte *PhoneNumber);
// Read phonebook <Position>, return <PhoneNumber>

static TYesNo PhoneBookOwnSet( void);
// Set active phonebook to own

//-----------------------------------------------------------------------------
//  Initialize
//-----------------------------------------------------------------------------

void GsmInit( void)
// Initialize
{
   UartModemInit();
   UartModemBaudRateSet( GSM_BAUD_RATE);
   UartModemBufferSet( _GsmBuffer, GSM_BUFFER_SIZE - 1);   // add zero terminator
   _ReplyTimeoutDefault();
#ifdef __GSM_SMS_STATISTICS__
   memset(&GsmStatistics, 0, sizeof(TGsmSmsStatistics));
#endif
} // GsmInit

void GsmDeinit( void)
{
   UartModemDeinit();
}

//-----------------------------------------------------------------------------
//  Shutdown
//-----------------------------------------------------------------------------

TYesNo GsmShutdown( void)
// Shutdown modem by AT command
{
   byte Trial;
   int i;
   // switch module off
   Trial = 2;
   do {
      // try to send command, when not possible just tell than it was ok
      _ReplyTimeoutDefault();
      _CommandSend( "AT+CFUN=0\r");
      if( !_ReplyMatch( REPLY_OK)){
         continue;
      }
#warning Dodelat korektni vypnuti GSM - pockat na ^SHUTDOWN
      for(i = 0 ; i < 500 ; i++) {
         SysDelay(10);
         MultitaskingReschedule();
      }

//      _CommandSend( "");
//      _ReplyTimeoutSet( TIMEOUT_SHUTDOWN);
//      if( !_ReplyMatch( SHUTDOWN)){
//         continue;
//      }
   } while( --Trial);
   return YES;
}

//-----------------------------------------------------------------------------
//  Reset
//-----------------------------------------------------------------------------

TYesNo GsmReset( void)
// Initialize modem (no PIN needed)
{
   byte Trial;

   // switch command echo off :
   Trial = 2;
   do {
      _CommandSend( "ATE0\r");
      if( _ReplyMatch( REPLY_OK)){
         break;
      }
      MultitaskingReschedule();
   } while( --Trial);
   if( Trial == 0){
      return( NO);
   }
   // Extended error mode :
   _CommandSend( "ATV1+CMEE=1\r");
   if( !_ReplyMatch( REPLY_OK)){
      return( NO);
   }
   return( YES);
} // GsmReset

//------------------------------------------------------------------------------
//  Registration
//------------------------------------------------------------------------------

TYesNo GsmRegistered( void)
// Returns YES on modem registered in network
{
   signed  Value;
   char *Reply;

   // check for registered :
   _CommandSend( "AT+CREG?\r");
   if( !_ReplyGet( &Reply)){
      return( NO);
   }
   Reply = _ReplyPrefix( Reply, "\r\n+CREG: ");
   if( !Reply){
      return( NO);
   }
   Reply = _ReplySkip( Reply, ',');    // skip field <n>
   if( !Reply){
      return( NO);
   }
   // field <stat> :
   if( !_ReplyNumber( Reply, &Value)){
      return( NO);
   }
   if( Value == 1 || Value == 5){
      return( YES);                    // registered or roaming
   }
   return( NO);
} // GsmRegistered

//------------------------------------------------------------------------------
//  Operator
//------------------------------------------------------------------------------

TYesNo GsmOperator( char *Name)
// Returns operator <Name>
{
   char *Reply;

   // check for operator :
   _CommandSend( "AT+COPS?\r");
   if( !_ReplyGet( &Reply)){
      return( NO);
   }
   Reply = _ReplyPrefix( Reply, "\r\n+COPS: ");
   if( !Reply){
      return( NO);
   }
   Reply = _ReplySkip( Reply, ',');    // skip field <mode>
   if( !Reply){
      return( NO);
   }
   Reply = _ReplySkip( Reply, ',');    // skip field <format>
   if( !Reply){
      return( NO);
   }
   if( !_ReplyString( Reply, Name, GSM_OPERATOR_SIZE)){
      return( NO);
   }
   return( YES);
} // GsmOperator

//------------------------------------------------------------------------------
//  Phone number
//------------------------------------------------------------------------------

TYesNo GsmPhoneNumber( char *PhoneNumber)
// Returns own <PhoneNumber>
{
   /*if(!PhoneBookOwnSet()) {
      return NO;
   }
   return PhoneBookRead(1, PhoneNumber);*/

   char *Reply;

   // check for operator :
   _CommandSend( "AT+CNUM\r");
   if( !_ReplyGet( &Reply)){
      return( NO);
   }
   Reply = _ReplyPrefix( Reply, "\r\n+CNUM: ");
   if( !Reply){
      return( NO);
   }
   Reply = _ReplySkip( Reply, ',');    // skip field <mode>
   if( !Reply){
      return( NO);
   }
   Reply = _ReplySkip( Reply, ',');    // skip field <format>
   if( !Reply){
      return( NO);
   }
   if( !_ReplyString( Reply, PhoneNumber, GSM_PHONE_NUMBER_SIZE)){
      return( NO);
   }
   return( YES);
} // GsmPhoneNumber

//------------------------------------------------------------------------------
//  Signal
//------------------------------------------------------------------------------

byte GsmSignalStrength( void)
// Returns relative signal strength
{
   signed  Value;
   char *Reply;

   // check for RSSI :
   _CommandSend( "AT+CSQ\r");
   if( !_ReplyGet( &Reply)){
      return( GSM_SIGNAL_UNDEFINED);
   }
   Reply = _ReplyPrefix( Reply, "\r\n+CSQ: ");
   if( !Reply){
      return( GSM_SIGNAL_UNDEFINED);
   }
   // field <rssi> :
   if( !_ReplyNumber( Reply, &Value)){
      return( GSM_SIGNAL_UNDEFINED);
   }
   return( Value);
} // GsmSignalStrength

//------------------------------------------------------------------------------
//  Check PIN
//------------------------------------------------------------------------------

TYesNo GsmPinReady( void)
// Returns YES on valid PIN
{
   char *Reply;

   _CommandSend( "AT+CPIN?\r");
   if( !_ReplyGet( &Reply)){
      return( NO);
   }
   Reply = _ReplyPrefix( Reply, "\r\n+CPIN: READY\r\n");
   if( !Reply){
      return( NO);
   }
   Reply = _ReplyPrefix( Reply, REPLY_OK);
   if( !Reply){
      return( NO);
   }
   return( YES);         // pin not needed
} // GsmPinReady

//------------------------------------------------------------------------------
//  Enter PIN
//------------------------------------------------------------------------------

TYesNo GsmPinEnter( char *Pin)
// Enters <Pin>
{
   // set pin :
   bprintf( _GsmBuffer, "AT+CPIN=\"%s\"\r", Pin);
   _CommandSend( _GsmBuffer);
   if( !_ReplyMatch( REPLY_OK)){
      return( NO);
   }
   SysDelay( 1000);                    // some wait before next command
   // unlock pin :
   bprintf( _GsmBuffer, "AT+CLCK=\"SC\",0,\"%s\"\r", Pin);
   _CommandSend( _GsmBuffer);
   if( !_ReplyMatch( REPLY_OK)){
      return( NO);
   }
   return( YES);
} // GsmPinEnter

//------------------------------------------------------------------------------
// SMS Initialize
//------------------------------------------------------------------------------

TYesNo GsmSmsInit( void)
// Initialize SMS services (with valid PIN only)
{
   char *Reply;

   // set SMS pdu mode :
   _ReplyTimeoutLDefault();
   _CommandSend( "AT+CMGF=0\r");_ReplyCheck(REPLY_OK);
   // check if modem phase 2+ feature supported
   bprintf(_GsmBuffer, "%s=?\r", AT_CSMS);_CommandSend( _GsmBuffer);
   if( !_ReplyGet( &Reply)){return NO;}
   Reply = _ReplyPrefix( Reply, "\r\n+CSMS: (0");
   if( !Reply){return NO;}
   Reply = _ReplyPrefix( Reply, ")");
   if( Reply){return NO;} // feature not supported
   //set phase 2+ feature
   _ReplyTimeoutSet(TIMEOUT_CONFIG_SET);
   bprintf( _GsmBuffer,"%s=1\r", AT_CSMS);_CommandSend( _GsmBuffer);
   if( !_ReplyGet( &Reply)){return NO;}
   Reply = _ReplyPrefix( Reply, "\r\n+CSMS: 1,1,1");
   if( !Reply){return NO;}// problem with feature setting
   Reply = _ReplySkip( Reply, '\n');
   if( !Reply){return( NO);} // missing crlf
   Reply = _ReplyPrefix( Reply, REPLY_OK);
   if( !Reply){return( NO);} // missing OK
   // keep sms link open feature
   _CommandSend( "AT+CMMS=2\r");_ReplyCheck( REPLY_OK);
   // event reporting configuration
   bprintf( _GsmBuffer, "%s=%s\r", AT_CNMI, AT_CNMI_MODE);
   _CommandSend( _GsmBuffer);
   _ReplyCheck(REPLY_OK);
   // Memory set
   if( !_GsmMemorySet(GSM_MEM_SM, NULL)){return NO;}
   return( YES);
} // GsmSmsInit

//------------------------------------------------------------------------------
//   SMS send single
//------------------------------------------------------------------------------

TYesNo GsmSmsSend( char *PhoneNumber, const char *Format, ...)
// Send SMS message by <Format> to <PhoneNumber>
{
   TSmsSubmit        send;
   int               byteSize;
   va_list           Arg;

   memset( &send, 0, sizeof(TSmsSubmit));
   send.Dcs = SMS_DCS_7BIT_DEFAULT;
   // number
   if( !_PhoneToAddress(PhoneNumber, send.Da)){
      //wrong number format
      return NO;
   }
   // text
   va_start( Arg, Format);
   bvprintf( _GsmBuffer, Format, Arg);
   byteSize = strlen( _GsmBuffer);
   if(byteSize < 0){return NO;}
   send.Udl = (byte) byteSize;
   if( send.Udl > GSM_SMS_SIZE){return NO;} // too long message
   va_end( Arg);
   // convert text to GSM alphabet :
   byteSize = _TextToGsm( _GsmBuffer);
   memcpy(&send.Ud, _GsmBuffer, byteSize);
   SmsPduClrSrr( send.Fo); // don't request status report
   SmsPduSetVpf(send.Fo, SMS_VPF_RELATIVE);
   send.Vp = GSM_SMS_EXPIRATION;
   if(_GsmSmsSubmit( &send, NULL)){
#ifdef __GSM_SMS_STATISTICS__
      GsmStatistics.SmsTSendCount++;
#endif
      return YES;
   }

   return NO;
} // GsmSmsSend

TYesNo GsmSmsSendBinary( char *PhoneNumber, char *Data, int size)
// Send single SMS message with binary data to <PhoneNumber>
// Send SMS message by <Format> to <PhoneNumber>
{
   TSmsSubmit        send;

   if( size > GSM_SMS_BYTE_SIZE || size < 0){
      return NO;
   }
   memset( &send, 0, sizeof(TSmsSubmit));
   send.Dcs = SMS_DCS_8BIT;
   // number
   if( !_PhoneToAddress(PhoneNumber, send.Da)){
      //wrong number format
      return NO;
   }
   send.Udl = size;
   memcpy(send.Ud, Data, size);
   SmsPduClrSrr( send.Fo); // don't request status report
   SmsPduSetVpf(send.Fo, SMS_VPF_RELATIVE);
   send.Vp = GSM_SMS_EXPIRATION;
   if(_GsmSmsSubmit( &send, NULL)){
#ifdef __GSM_SMS_STATISTICS__
      GsmStatistics.SmsBSendCount++;
#endif
      return YES;
   }
} // GsmSmsSend

//------------------------------------------------------------------------------
//   SMS read single
//------------------------------------------------------------------------------

TSmsReadStatus GsmSmsRead( char *PhoneNumber, char *Text, UDateTimeGauge *ReceivedTime)
// Gets single SMS if some available
{
   int   SavedCount;
   int   Address;
   int   Length;
   TSmsDeliver *Deliver;

   // check if any message in memory
   if( !_GsmMemorySet(GSM_MEM_SM, &SavedCount)){return GSM_SMS_READ_ERROR;}

   for( Address = 1; Address <= _GsmMemorySize; Address++){
      if(SavedCount == 0){return GSM_SMS_READ_EMPTY;}
      // read all messages in memory, check parameters
      if( !_GsmSmRead( Address, (byte**)&Deliver, &Length)){return GSM_SMS_READ_ERROR;}
      // Check type
      if(SmsPduGetMti(Deliver->Fo) == SMS_INVALID){continue;}
      SavedCount--;
      if(SmsPduGetMti(Deliver->Fo) != SMS_DELIVER){_GsmSmDelete(Address);continue;}
      if(SmsPduGetUdhi(Deliver->Fo) != 0){continue;} //udh present, not valid for simple sms
      TSmsRawToDeliver(Deliver, Length); // rearange packet
      // get phone number
      if( !_AddressToPhone(Deliver->Oa, PhoneNumber)){_GsmSmDelete(Address);return GSM_SMS_READ_ERROR;}
      // get message
      if( Deliver->Dcs == SMS_DCS_7BIT_DEFAULT){
         Length = _GsmToText(Deliver->Data.text.Ud, Deliver->Data.text.Udl, Text);
      } else if( Deliver->Dcs == SMS_DCS_8BIT){
         memcpy( Text, Deliver->Data.text.Ud, Deliver->Data.text.Udl);
         Length = Deliver->Data.text.Udl;
      } else {
         _GsmSmDelete(Address);
         continue;// invalid encoding
      }
      Text[Length] = '\0';
      // get timestamp
      *ReceivedTime = SysClock();
      _GsmSmDelete(Address);
      break;
   }
#ifdef __GSM_SMS_STATISTICS__
   GsmStatistics.SmsTRcvCount++;
#endif
   return( GSM_SMS_READ_OK);
} // GsmSmsRead

//------------------------------------------------------------------------------
//   Write single binary
//------------------------------------------------------------------------------

TYesNo _GsmDataWrite(TSmsBinaryData *data, TSmsId *id, int validityMins)
// send binary sms to network if possible
{
   TSmsSubmit        send;

   memset( &send, 0, sizeof(TSmsSubmit));
   send.Dcs = SMS_DCS_8BIT;
   // address copy
   memcpy(send.Da, id->Da, SMS_ADDRESS_SIZE);
   // data copy
   memcpy(&send.Udl, data, SmsBinaryDataLen(data->Udl));
   SmsPduSetSrr( send.Fo);  // request status report
   SmsPduSetUdhi( send.Fo); // set UDH indicator
   SmsPduSetVpf(send.Fo, SMS_VPF_RELATIVE);
   send.Vp = GsmSmsExpirationMins(validityMins);

#ifdef __GSM_SMS_STATISTICS__
   if( data->UdhLength == SMS_UDH_LEN){
      GsmStatistics.SmsBSendCount++;
   }else {
      GsmStatistics.SmsASendCount++;
   }
#endif
   return ( _GsmSmsSubmit( &send, id));
}

//------------------------------------------------------------------------------
//   SMS read single binary
//------------------------------------------------------------------------------

TYesNo _GsmDataRead( TSmsId *id, TSmsBinaryData *data)
// get binary sms from memory if some available
{
   int   SavedCount;
   int   Address;
   int   Length;
   TSmsDeliver *Deliver;

   // check if any message in memory
   if( !_GsmMemorySet(GSM_MEM_SM, &SavedCount)){return NO;}

   for( Address = 1; Address <= _GsmMemorySize; Address++){
      if(SavedCount == 0){return NO;}
      // read all messages in memory, check parameters
      if( !_GsmSmRead( Address, (byte**)&Deliver, &Length)){return NO;}
      // Check type
      if(SmsPduGetMti(Deliver->Fo) == SMS_INVALID){continue;}
      SavedCount--;
      if(SmsPduGetMti(Deliver->Fo) != SMS_DELIVER){_GsmSmDelete(Address);continue;}
      if(SmsPduGetUdhi(Deliver->Fo) != 1){continue;} //udh not present, not valid for binary sms
      TSmsRawToDeliver(Deliver, Length); // rearange packet
      // get data
      if( Deliver->Dcs != SMS_DCS_8BIT){
         // wrong encoding, delete
         _GsmSmDelete(Address);
         continue;
      }
      memcpy(data, &Deliver->Data.binary, SmsBinaryDataLen(Deliver->Data.binary.Udl));
      // get parameters
      memcpy(id->Da, Deliver->Oa, SMS_ADDRESS_SIZE);
      id->Status = SMS_ST_OK;
      id->Mr = 0;
      _TimeStampToTime( Deliver->Scts, &id->TimeStamp);
      _GsmSmDelete(Address);
      break;
   }
#ifdef __GSM_SMS_STATISTICS__
   if( data->UdhLength == SMS_UDH_LEN){
      GsmStatistics.SmsBRcvCount++;
   }else {
      GsmStatistics.SmsARcvCount++;
   }
#endif
   return( YES);
}

//------------------------------------------------------------------------------
//   SMS read status
//------------------------------------------------------------------------------

TYesNo _GsmStatusRead(TSmsId *id)
// read sms status report
{
   int   index;
   int   Length;
   int   SavedCount;
   TSmsStatusReport *status;

   // read status report from SR memory at index
   if( !_GsmMemorySet(GMS_STATUS_MEM, &SavedCount)){return NO;}
   if(SavedCount == 0){return NO;}

   for( index = 1; index <= _GsmMemorySize; index++){
      if( !_GsmSmRead(index, (byte**)&status, &Length)){return NO;}
      // Check type
      if(SmsPduGetMti(status->Fo) != SMS_STATUS_REPORT){continue;}
      TSmsRawToStatusReport(status, Length);
      id->Mr = status->Mr;
      memcpy(id->Da, status->Ra, SMS_ADDRESS_SIZE);
      id->Status = status->St;
      _TimeStampToTime( status->Dt, &id->TimeStamp);
      _GsmSmDelete(index);
      break;
   }
#ifdef __GSM_SMS_STATISTICS__
   GsmStatistics.SmsDeliveryCount++;
#endif
   return YES;
}

//------------------------------------------------------------------------------
//   Memory location change
//------------------------------------------------------------------------------

static TYesNo _GsmMemorySet(TGsmMemoryLocation to, int *currentUsed)
// set MEM1 memory location to <to>
{
   static TGsmMemoryLocation currentLocation = GSM_MEM_ME;
   char *Reply;
   char *ParFrom, *ParTo;
   int size;

   _ReplyTimeoutSet( TIMEOUT_MEMORY_SET);   // set long Rx timeout
   if( currentLocation != to){
      // write new memory location
      bprintf( _GsmBuffer, "%s=\"%s\",\"%s\",\"%s\"\r", AT_CPMS,
               MEM_STR[to], MEM_STR[GSM_MEM_ME], MEM_STR[GSM_MEM_SM]);
      _CommandSend( _GsmBuffer);
      if( !_ReplyGet( &Reply)){return NO;}
      Reply = _ReplyPrefix( Reply, RESP_CPMS);
      if( !Reply){return NO;} // feature not supported
      currentLocation = to;   //change location
      //get memory size
      if( !_ReplyGetParam( Reply, PARAM_CPMS_W_TOTAL1, &ParFrom, &ParTo)){return NO;}
      if( !_ReplyNumber(ParFrom, &size)){ return NO;}
      _GsmMemorySize = (byte) size;
      //get used size
      if(currentUsed){
         if( !_ReplyGetParam( Reply, PARAM_CPMS_W_USED1, &ParFrom, &ParTo)){return NO;}
         if( !_ReplyNumber(ParFrom, &size)){ return NO;}
         *currentUsed = (byte) size;
      }
   } else {
      // just read current used / total size parameters
      bprintf( _GsmBuffer, "%s?\r",AT_CPMS);
      _CommandSend( _GsmBuffer);
      if( !_ReplyGet( &Reply)){return NO;}
      Reply = _ReplyPrefix( Reply, "\r\n+CPMS: ");
      if( !Reply){return NO;} // response problem
      //get memory size
      if( !_ReplyGetParam( Reply, PARAM_CPMS_R_TOTAL1, &ParFrom, &ParTo)){return NO;}
      if( !_ReplyNumber(ParFrom, &size)){ return NO;}
      _GsmMemorySize = (byte) size;
      //get used size
      if(currentUsed){
         if( !_ReplyGetParam( Reply, PARAM_CPMS_R_USED1, &ParFrom, &ParTo)){return NO;}
         if( !_ReplyNumber(ParFrom, currentUsed)){ return NO;}
      }
   }
   return YES;
}

//------------------------------------------------------------------------------
//   SMS memory delete
//------------------------------------------------------------------------------

#ifndef __WIN32__
static
#endif
TYesNo _GsmSmDelete( byte Address)
// Delete SM from local storage
{
   //int i;
   // send Delete command :
   _ReplyTimeoutSet( TIMEOUT_SMS_DELETE);   // set long Rx timeout
   // TODO when problems with double sms receiving occurs, this could be solution
   //   for( i = 0; i < 2; i++){
   //       send command twice to be absolutely sure that sms is deleted
   bprintf( _GsmBuffer, "AT+CMGD=%d\r", Address);
   _CommandSend( _GsmBuffer);
   // check for reply :
   if( !_ReplyMatch( REPLY_OK)){
      _ReplyTimeoutDefault();
      return( NO);
   }
   //   }
   _ReplyTimeoutDefault();
   return( YES);
} // GsmSmsDelete

//------------------------------------------------------------------------------
//   SMS memory read
//------------------------------------------------------------------------------

#ifndef __WIN32__
static
#endif
TYesNo _GsmSmRead( byte Address, byte **pdu, int *Length)
// Read SM from local storage
{
   char *Reply;
   char *ParFrom, *ParTo;

   _ReplyTimeoutSet( TIMEOUT_SMS_READ);   // set long Rx timeout
   bprintf( _GsmBuffer, "AT+CMGR=%d\r", Address);
   _CommandSend( _GsmBuffer);
   if( !_ReplyGet( &Reply)){return( NO);}
   // check for empty position :
   if( _ReplyPrefix( Reply, CMS_ERROR) ||
       _ReplyPrefix( Reply, REPLY_OK)){
      *pdu = (byte *)Reply;
      SmsPduSetMti((*pdu)[0], SMS_INVALID);
      return YES;
   }

   // check for valid message :
   Reply = _ReplyPrefix( Reply, "\r\n+CMGR: ");
   if( !Reply){return( NO);}

   // unread message, get it's parameters
   if( !_ReplyGetParam(Reply, PARAM_CMGR_LENGTH, &ParFrom, &ParTo)){return NO;}
   if( !_ReplyNumber(ParFrom, Length)){ return NO;}
   if( *Length == 0){
      *pdu = (byte *)Reply;
      SmsPduSetMti((*pdu)[0], SMS_INVALID);
      return YES;
   }
   Reply = _ReplySkip(Reply, '\n'); //skip <CR><LF>
   if( !Reply){return( NO);}
   *Length = _IraToBuffer((byte *)Reply, *Length);
   *pdu = (byte *)Reply;
   return YES;
}

//------------------------------------------------------------------------------
//   Submit SM command
//------------------------------------------------------------------------------
#ifndef __WIN32__
static
#endif
TYesNo _GsmSmsSubmit(TSmsSubmit *request, TSmsId *id)
// write submit pdu - send sms and return report if requested
{
   char *Reply;
   char *ParFrom;
   char *ParTo;
   int  size;

   if( id){
      id->Mr = 0;
      id->Status = SMS_ST_UNKNOWN;
   }

   SmsPduSetMti( request->Fo, SMS_SUBMIT);
   request->Pid = 0;

   //reanrange memory and change size according to phone number len
   size = TSmsSubmitToRaw(request);
   // SMS send command :
   _ReplyTimeoutSet( TIMEOUT_SMS_PROMPT);
   bprintf( _GsmBuffer, "AT+CMGS=%d\r", size);
   //wait for prompt
   _CommandSend( _GsmBuffer);_ReplyCheck(PROMPT);
   // send message pdu :
   size = _BufferToIra( (byte*)request, size, (byte *)_GsmBuffer);
   _ReplyTimeoutSet( TIMEOUT_SMS_SEND);// set long Rx timeout
   _DataSend( _GsmBuffer, size); // send SMS data
   if( !_ReplyGet( &Reply)){return NO;}

   // check for error code
   if( (ParFrom = _ReplyPrefix( Reply, CMS_ERROR)) != 0){
      if( id){
         if( !_ReplyGetParam( Reply, PARAM_CME_ERROR_CODE, &ParFrom, &ParTo)){return NO;}
         if( !_ReplyNumber(ParFrom, &size)){ return NO;}// size is just placeholder
         id->Status = size;
      }
      return NO;
   }

   Reply = _ReplyPrefix( Reply, "\r\n+CMGS: ");
   if( !Reply){ return NO;}
   if( id){
      if( !_ReplyGetParam( Reply, PARAM_CMGS_MR, &ParFrom, &ParTo)){return NO;}
      if( !_ReplyNumber(ParFrom, &size)){ return NO;}
      id->Mr = (byte)size; // size is just placeholder
   }
   return( YES);
}

//------------------------------------------------------------------------------
//   Command SM command
//------------------------------------------------------------------------------

#ifndef __WIN32__
static
#endif
TYesNo _GsmSmsCommand(TSmsId *id, ESmsCommandType ct)
// write command pdu - send command and return response
{
   int size;
   char *Reply;
   TSmsCommand request;

   memset( &request, 0, sizeof(TSmsCommand));
   SmsPduSetMti( request.Fo, SMS_COMMAND);
   request.Pid = 0;
   if( ct < _SMS_CT_LAST){
      request.Ct = ct;
   } else {
      request.Ct = SMS_CT_ENQUIRY;
   }
   request.Mn = id->Mr;
   memcpy(request.Da, id->Da, SMS_ADDRESS_SIZE);
   request.Cdl = 0;
   size = TSmsCommandToRaw(&request);

   // SMS send command :
   _ReplyTimeoutSet( TIMEOUT_SMS_PROMPT);
   bprintf( _GsmBuffer, "AT+CMGC=%d\r", size);
   //wait for prompt
   _CommandSend( _GsmBuffer);_ReplyCheck(PROMPT);
   // send pdu
   size = _BufferToIra( (byte *)&request, size, (byte *)_GsmBuffer);
   _DataSend( _GsmBuffer, size);
   if( !_ReplyGet( &Reply)){return NO;}
   Reply = _ReplyPrefix( Reply, "\r\n+CMGC: ");
   if( !Reply){ return NO;}
   return( YES);
}

//------------------------------------------------------------------------------
//   buffer to/from IRA conversion
//------------------------------------------------------------------------------

static int _BufferToIra( byte *input, int inputSize, byte *output)
// convert pdu packet to IRA enchoding
{
   int i,j;
   // convert byte per byte to hexa format :
   for( i = 0; i < inputSize; i++){
      j = (i << 1) + 2;
      output[j] = uNibbleToHex(input[i] >> 4);  // upper nibble to char
      output[j | 1] = uNibbleToHex(input[i]);   // lower nibble to char
   }
   // prepend with SC address - 00 means use automatic
   output[0] = output[1] = uNibbleToHex(0);
   // append CTRL_Z to confirm message
   output[j+2] = CTRL_Z;
   inputSize = (inputSize << 1) + 3; //encoded data length + SC address size + ctrl_z size
   return inputSize;
}

static int _IraToBuffer(byte *buffer, int inputLen)
// convert IRA encoding to pdu packet
{
   int i,j;
   int inputSize;
   byte *data;
   byte *output;

   output = data = buffer;
   // skip SC address
   i = (uCharToHex(data[0]) << 4) |  (uCharToHex(data[1]) & 0x0F);
   i += 1;
   data = data + (i << 1);

   inputSize = inputLen << 1;
   // convert char per char from hexa format :
   for( i = 0; i < inputSize; i+=2){
      j = (i >> 1);
      output[j]  = uCharToHex(data[i]) << 4;
      output[j] |= uCharToHex(data[i | 1]) & 0x0F;
   }
   return inputLen;
}

//******************************************************************************


//------------------------------------------------------------------------------
//  Send command
//------------------------------------------------------------------------------

static void _CommandSend( char *Command)
// Send AT <Command>
{
   UartModemSend( Command, 0);
   //   while( UartModemStatus() == UART_MODEM_SEND_ACTIVE);
} // _CommandSend

//------------------------------------------------------------------------------
//  Send data
//------------------------------------------------------------------------------

static void _DataSend( char *Data, int Size)
// Send SMS <Data> with <Size>
{
   UartModemSend( Data, Size);
   //   while( UartModemStatus() == UART_MODEM_SEND_ACTIVE);
} // _DataSend

//------------------------------------------------------------------------------
//   Match
//------------------------------------------------------------------------------

static TYesNo _ReplyMatch( const char *Reply)
// Check for AT command <Reply> match
{
   if( !_ReplyWait()){
      return( NO);                     // timeout or error
   }
   if( !UartModemReceive( strlen( Reply))){
      return( NO);
   }
   return( strequ( _GsmBuffer, Reply));
} // _ReplyMatch

//------------------------------------------------------------------------------
//   Reply
//------------------------------------------------------------------------------

static TYesNo _ReplyGet( char **Reply)
// Get AT command <Reply>
{
   *Reply = _GsmBuffer;
   if( !_ReplyWait()){
      return( NO);                     // timeout or error
   }
   if( !UartModemReceive( 0)){
      return( NO);

   }
   return( YES);
} // _ReplyGet

//------------------------------------------------------------------------------
//   Reply wait
//------------------------------------------------------------------------------

static TYesNo _ReplyWait( void)
// Wait for reply received
{
   forever {
      switch( UartModemStatus()){
      case UART_MODEM_RECEIVE_DONE :
         return( YES);

      case UART_MODEM_RECEIVE_ERROR :
         return( NO);

      case UART_MODEM_RECEIVE_ACTIVE :
      case UART_MODEM_SEND_ACTIVE :
      case UART_MODEM_SEND_DONE :
         MultitaskingReschedule();
         break;                     // wait for send done

      default :
         return( NO);
      }
   }
} // _ReplyWait

//------------------------------------------------------------------------------
//   Prefix
//------------------------------------------------------------------------------

static char *_ReplyPrefix( char *Reply, const char *Prefix)
// Check for <Reply> matches <Prefix> and removes prefix
{
   int Size;

   Size = strlen( Prefix);
   if( Size == 0 || !strnequ( Reply, Prefix, Size)){
      return( 0);
   }
   return( Reply + Size);
} // _ReplyPrefix

//------------------------------------------------------------------------------
//   Skip
//------------------------------------------------------------------------------

static char *_ReplySkip( char *Reply, const char ch)
// Skip <Reply> up to character <ch>
{
   char *p;

   p = strchr( Reply, ch);
   if( p == 0){
      return( 0);
   }
   return( p + 1);
} // _ReplySkip

//------------------------------------------------------------------------------
//   Number
//------------------------------------------------------------------------------

static char *_ReplyNumber( char *Reply, signed *Number)
// Returns decimal <Number> from <Reply>
{
   signed Value;
   int      StringSize;

   StringSize = uParseNumber( Reply, &Value);
   if( StringSize == 0){
      return( 0);                      // wrong syntax
   }
   *Number = Value;
   return( Reply + StringSize);
} // _ReplyNumber

//------------------------------------------------------------------------------
//   String
//------------------------------------------------------------------------------

static char *_ReplyString( char *Reply, char *String, byte Size)
// Returns <String> with maximum <Size>
{
   int StringSize;

   StringSize = uParseString( Reply, String, Size);
   if( StringSize == 0){
      return( 0);                      // wrong syntax
   }
   return( Reply + StringSize);
} // _ReplyString

//------------------------------------------------------------------------------
//   To GSM
//------------------------------------------------------------------------------

#ifndef __WIN32__
static
#endif
int _TextToGsm( char *Text)
// Converts <Text> to GSM alphabet in sito, returns size
{
   int   Size;
   char *in;
   char *out;
   int   Count;
   int   bits;

   Count    = strlen( Text);
   out = in = Text;
   bits     = 0;
   Size     = 0;
   // encode to gsm alphabet
   _AsciiToGsm(in);
   while( Count--){
      // encode to gsm alphabet
      if(Count > 0){
         _AsciiToGsm(in+1);
      }
      if( bits == 7){
         in++;
         bits = 0;
         continue;
      }
      //pack into septets
      *out = (*in & 0x7F) >> bits;
      if( Count > 0){
         *out |= *(in+1) << (7 - bits);
      }
      //move on
      bits++;
      in++;
      out++;
      Size++;
   }
   return( Size);
} // _TextToGsm

//------------------------------------------------------------------------------
//   From GSM
//------------------------------------------------------------------------------

#ifndef __WIN32__
static
#endif
int _GsmToText( byte *GsmText, int Size, char *OutText)
// Converts <Text> with <Size> septets(7/8*byte) from GSM alphabet, return size in bytes
{
   byte *in;
   char *out;
   int   bits;

   out = OutText;
   in = GsmText + 1;
   bits = 8;
   // septets size to byte size
   Size = Size*7;
   Size = (Size >> 3) + ((Size & 7) == 0? 0 : 1);//add one when nonzero mod

   while( in <= (GsmText + Size)){
      //unpack septets
      if( bits == 8){
         *out = *(in-1) & 0x7F;
         // decode alphabet
         _GsmToAscii(out);
         out++;
         bits = 1;
         continue;
      }
      *out = ((*in << bits) | *(in-1) >> (8 - bits)) & 0x7F ;
      _GsmToAscii(out);
      out++;
      in++;
      bits++;
   }
   return Size;
} // _GsmToText

//------------------------------------------------------------------------------
//   From ascii
//------------------------------------------------------------------------------

static void _AsciiToGsm(char *c)
// from asscii alphabet to gsm alphabet
{
   if( *c == '@'){
      *c = '\0';
   }
}

//------------------------------------------------------------------------------
//   To Ascii
//------------------------------------------------------------------------------

static void _GsmToAscii(char *c)
// from gsm alphabet to gsm alphabet
{
   if( *c == '\0'){
      *c = '@';
   }
}

//------------------------------------------------------------------------------
//   Phone number to address
//------------------------------------------------------------------------------

TYesNo _PhoneToAddress(char *PhoneNumber, byte *Address)
// Converts ASCII <PhoneNumber> to address representation
{
   byte byteSize;
   int  i;
   int  j;
   char *p;

   byteSize = strlen(PhoneNumber);
   if( byteSize < 1){
      return NO;
   }
   memset(Address, 0, SMS_ADDRESS_SIZE);
   p = PhoneNumber;
   if( p[0] == '+'){
      //international number
      SmsPduSetTon(Address[1], SMS_TON_INTERNATIONAL);
      p++;
      byteSize--;
   } else {
      SmsPduSetTon(Address[1], SMS_TON_NATIONAL);
   }
   SmsPduSetNpi(Address[1], SMS_NPI_ISDN_PHONE);
   for(i = 0, j = 2; i < byteSize; i+=2, j++){
      if( p[i] < '0' || p[i] > '9'){
         //wrong number format
         return NO;
      }
      Address[j] = uCharToHex(p[i]);
      if( i+1 == byteSize){
         Address[j] |= 0xF0;
      } else {
         Address[j] |= uCharToHex(p[i+1]) << 4;
      }
   }

   Address[0] = byteSize;
   return YES;
}

//------------------------------------------------------------------------------
//   Address to phone number
//------------------------------------------------------------------------------

TYesNo _AddressToPhone(byte *Address, char *PhoneNumber)
// Converts address to ASCII <PhoneNumber> representation
{
   byte byteSize;
   int  i;
   char *p;
   byte *a;

   byteSize = Address[0];
   if( byteSize < 2){
      return NO;
   }
   if( SmsPduGetNpi(Address[1]) != SMS_NPI_ISDN_PHONE){
      //only phone numbers are valid
      return NO;
   }
   p = PhoneNumber;
   if( SmsPduGetTon(Address[1]) == SMS_TON_INTERNATIONAL){
      PhoneNumber[0] = '+';
      p++;
   }

   a = &Address[2];
   for( i = 0; i < byteSize; i++){
      if( i & 1){
         p[i] = uNibbleToHex(a[i>>1] >> 4);
      } else {
         p[i] = uNibbleToHex(a[i>>1]);
      }
   }
   p[i] = '\0';
   return YES;
}

//------------------------------------------------------------------------------
//   Timestamp conversion
//------------------------------------------------------------------------------

static void _TimeStampToTime(byte *ts, UDateTimeGauge *DateTime)
// convert SC timestamp to internal representation
{
   UNUSED_ARG(ts);

   *DateTime = SysTime();
}


//------------------------------------------------------------------------------
//   Network init
//------------------------------------------------------------------------------

TYesNo GsmNetInit( void)
// Initialize internet underlying connection( GPRS stack)
{
   char *Reply;
   char *paramBeg;
   char *paramEnd;

   // set internet connection profile
   _ReplyTimeoutDefault();
   bprintf( _GsmBuffer, "AT^SICS=%d,conType,\"GPRS0\"\r", CONNECTION_PROFILE_ID);
   _CommandSend( _GsmBuffer); _ReplyCheck( REPLY_OK);
   // set polling mode
   bprintf( _GsmBuffer, "AT^SCFG=\"%s\",\"%s\"\r", KEYWORD_URC_MODE, KEYWORD_OFF);
   _CommandSend( _GsmBuffer);
   if( !_ReplyGet( &Reply)){return NO;}
   if( !(Reply = _ReplyPrefix(Reply, "\r\n^SCFG: "))){return NO;}
   if( !_ReplyGetParam(Reply, PARAM_PARAM_NAME, &paramBeg, &paramEnd)){return NO;}
   if( strncasecmp( KEYWORD_URC_MODE, paramBeg, paramEnd - paramBeg) != 0){return NO;}
   if( !_ReplyGetParam(Reply, PARAM_PARAM_VALUE, &paramBeg, &paramEnd)){return NO;}
   if( strncasecmp( KEYWORD_OFF, paramBeg, paramEnd - paramBeg) != 0){return NO;}
   return( YES);
} // GsmSmsDelete

//------------------------------------------------------------------------------
//   Setup socket
//------------------------------------------------------------------------------

TYesNo GsmNetSocketSetup(byte SockAddr, const char* address, const char *port, TYesNo Tcp, unsigned short TcpKeepIdle, byte TcpKeepCnt, byte TcpKeepIntvl)
// Initialize socket service, when <Tcp> select Tcp socket else Udp, when <address> create client socket else server socket
{
   assert(strlen(address)!=0 && strlen(port)!=0);

   _ReplyTimeoutDefault();
   bprintf( _GsmBuffer, "AT^SISS=%d,srvType,\"%s\"\r", SockAddr, KEYWORD_SOCKET);
   _CommandSend( _GsmBuffer);_ReplyCheck( REPLY_OK);
   bprintf( _GsmBuffer, "AT^SISS=%d,conId,%d\r", SockAddr, CONNECTION_PROFILE_ID);
   _CommandSend( _GsmBuffer);_ReplyCheck( REPLY_OK);

   if( Tcp){
      char tcpParamBuf[TCP_PARAM_MAX_LEN];
      char *tcpParams = tcpParamBuf;
      if(TcpKeepIdle){
         tcpParams += sprintf(tcpParams, KEYWORD_TCP_KEEPIDLE);
         tcpParams += sprintf(tcpParams, "%d", TcpKeepIdle);
      }
      if(TcpKeepCnt){
         tcpParams += sprintf(tcpParams, KEYWORD_TCP_KEEPCNT);
         tcpParams += sprintf(tcpParams, "%d", TcpKeepCnt);
      }
      if(TcpKeepIntvl){
         tcpParams += sprintf(tcpParams, KEYWORD_TCP_KEEPINTVL);
         tcpParams += sprintf(tcpParams, "%d", TcpKeepIntvl);
      }
      *tcpParams = '\0';
      bprintf( _GsmBuffer, "AT^SISS=%d,address,\"%s%s:%s%s\"\r", SockAddr, TCP_CLIENT_PREFIX, address, port, tcpParamBuf);
   } else {
      bprintf( _GsmBuffer, "AT^SISS=%d,address,\"%s%s:%s\"\r", SockAddr, UDP_CLIENT_PREFIX, address, port);
   }

   _CommandSend( _GsmBuffer);_ReplyCheck( REPLY_OK);
   return YES;
}

//------------------------------------------------------------------------------
//   Open socket
//------------------------------------------------------------------------------

TYesNo GsmNetSocketOpen(byte SockAddr)
// open previously configured socket
{
   char *params;
   char *paramBeg;
   char *paramEnd;
   signed i;

   _ReplyTimeoutDefault();
   bprintf( _GsmBuffer, "AT^SISO=%d\r", SockAddr);
   _CommandSend( _GsmBuffer);_ReplyCheck( REPLY_OK);
   // ask status
   params = _GsmNetSocketInfo(SockAddr);
   if( params == 0){
      return NO;
   }
   // check parameter
   if( !_ReplyGetParam( params, PARAM_SRV_TYPE, &paramBeg, &paramEnd)){
      GsmNetSocketClose(SockAddr);
      return NO;
   }
   i = paramEnd - paramBeg + 1;
   if( !strnequ(KEYWORD_SOCKET, paramBeg, i)){
      GsmNetSocketClose(SockAddr);
      return NO;
   }
   // check parameter
   if( !_ReplyGetParam( params, PARAM_SRV_STATE, &paramBeg, &paramEnd)){
      GsmNetSocketClose(SockAddr);
      return NO;
   }

   if( !_ReplyNumber( paramBeg, &i)){
      GsmNetSocketClose(SockAddr);
      return NO;
   }
   if( i <= STATE_ALOCATED || i > STATE_UP){
      GsmNetSocketClose(SockAddr);
      return NO;
   }
   return YES;
}

//------------------------------------------------------------------------------
//   Close socket
//------------------------------------------------------------------------------
TYesNo GsmNetSocketClose(byte SockAddr)
// close socket
{
   _ReplyTimeoutDefault();
   bprintf( _GsmBuffer, "AT^SISC=%d\r", SockAddr);
   _CommandSend( _GsmBuffer);_ReplyCheck( REPLY_OK);
   return YES;
}


//-----------------------------------------------------------------------------
//  Local address
//-----------------------------------------------------------------------------

TYesNo GsmNetGetSocketAddress( byte SockAddr, char *address, char *port)
// return local socket address and port
{
   char *info;
   char *from;
   char *to;
   int len;

   _ReplyTimeoutDefault();
   info = _GsmNetSocketInfo( SockAddr);
   if( !_ReplyGetParam( info, PARAM_LOCAL_ADDR, &from, &to)){
      return NO;
   }
   //locate address:port delimiter
   if( !(info = strchr( from, PORT_DELIMITER))){
      return NO;
   }
   len = info - from;
   strncpy(address, from, len);
   address[len] = '\0';
   info++;
   len = to - info + 1;
   strncpy(port, info, len);
   port[len] = '\0';
   return YES;
}

//-----------------------------------------------------------------------------
//  Write
//-----------------------------------------------------------------------------
int GsmNetWrite( byte SockAddr, byte *data, int size)
// write data of size, return actual writen size
{
   char *Reply;
   char *paramBeg;
   char *paramEnd;
   signed num;

   _ReplyTimeoutDefault();
   bprintf( _GsmBuffer, "AT^SISW=%d,%d\r", SockAddr, size);
   _CommandSend( _GsmBuffer);
   if( !_ReplyGet( &Reply)){return 0;}
   if( !(Reply = _ReplyPrefix(Reply, "\r\n^SISW: "))){return 0;}
   if( !(Reply = _GsmNetValidParam(SockAddr, Reply))){return 0;}
   if( !_ReplyGetParam(Reply, PARAM_WRITE_LEN, &paramBeg, &paramEnd)){return NO;}
   _ReplyNumber( paramBeg, &num);
   if( num == 0){
      return 0;
   }
   memcpy( _GsmBuffer, data, num);
   _DataSend( _GsmBuffer, num);
   if( !_ReplyMatch( REPLY_OK)){return( 0);}
   return( num);
}

//-----------------------------------------------------------------------------
//  Read
//-----------------------------------------------------------------------------
int GsmNetRead( byte SockAddr, byte *data, int MaxSize)
// read data up to maxsize, retur actual read size
{
   char *Reply;
   char *paramBeg;
   char *paramEnd;
   signed num;
   int   len;

   _ReplyTimeoutDefault();
   // compute maximum read size according to gsm buffer
   len = sprintf( _GsmBuffer, "\r\n^SISR: %d,%d,%d\r\n\r\nOK\r\n", SockAddr, MaxSize, MaxSize);
   len = GSM_BUFFER_SIZE - len;
   if( MaxSize < len){
      len = MaxSize;
   }
   bprintf( _GsmBuffer, "AT^SISR=%d,%d\r", SockAddr, len);
   _CommandSend( _GsmBuffer);
   if( !_ReplyGet( &Reply)){return 0;}
   if( !(Reply = _ReplyPrefix(Reply, "\r\n^SISR: "))){return 0;}
   if( !(Reply = _GsmNetValidParam(SockAddr, Reply))){return 0;}
   if( !_ReplyGetParam(Reply, PARAM_READ_LEN, &paramBeg, &paramEnd)){return 0;}
   _ReplyNumber( paramBeg, &num);
   if( num > 0){
      if( !(Reply = _ReplySkip(Reply, '\n'))){
         return 0;
      }
      memcpy( data, Reply, num);
      Reply += num;
      if( !_ReplyPrefix( Reply, REPLY_OK)){
         return 0;
      }
   }
   return( num);
}

//-----------------------------------------------------------------------------
//  Socket state
//-----------------------------------------------------------------------------

TGsmNetSocketState GsmNetSocketState( byte SockAddr)
// read socket state
{
   char *params;
   char *paramFrom;
   char *paramTo;
   signed num;

   params = _GsmNetSocketInfo( SockAddr);
   if( params == 0){
      return STATE_UNDEFINED;
   }
   if( !_ReplyGetParam( params, PARAM_SRV_STATE, &paramFrom, &paramTo)){
      return STATE_UNDEFINED;
   }
   if( !_ReplyNumber( paramFrom, &num)){
      return STATE_UNDEFINED;
   }
   return (TGsmNetSocketState) num;
}

//-----------------------------------------------------------------------------
//  Local helpers
//-----------------------------------------------------------------------------

static char *_GsmNetSocketInfo(byte SockAddr)
// return socket parameters info (AT^SISO?) parameters
{
   char *Reply;

   bprintf( _GsmBuffer, "AT^SISO=%d,1\r", SockAddr);
   _CommandSend( _GsmBuffer);
   if( !_ReplyGet( &Reply)){return NO;}
   if( ! (Reply = _ReplyPrefix(Reply, "\r\n^SISO: "))){
      return 0;
   }
   return(_GsmNetValidParam(SockAddr, Reply));
}

static char *_GsmNetValidParam( byte SockAddr, char *Reply)
// check params reply validity, return reply without first param or 0 if params are not for <sockaddr>
{
   signed num;

   Reply = _ReplyNumber( Reply, &num);
   Reply = _ReplySkip( Reply, PARAMS_DELIMITER);
   if( num == SockAddr){
      return Reply;
   }
   return 0;
}

static TYesNo _ReplyGetParam( char* const paramString, int which, char **resFrom, char **resTo)
// find parameter in parameter string, retur its position from->to
{
   char *p = paramString;
   int i;

   if(*p == '\0'){
      return NO;
   }
   i = 0;
   *resFrom = p;
   do{
      if( *p == PARAMS_DELIMITER){
         if( i == which){
            break;
         }
         *resFrom = p+1;
         i++;
      }
      p++;
   }while( *p != LINE_DELIMITER && *p != '\0');

   if( i != which){
      return NO;
   }

   *resTo = p - 1;
   //parameter without ""
   if( **resFrom == '"'){
      (*resFrom)++;
   }
   if( **resTo == '"'){
      (*resTo)--;
   }
   if( *resFrom > *resTo){
      return NO;
   }
   return YES;
}

//------------------------------------------------------------------------------
//  Phone book read
//------------------------------------------------------------------------------

static TYesNo PhoneBookRead(int Position, byte *PhoneNumber)
// Read phonebook <Position>, return <PhoneNumber>
{
   char *Reply;
   bprintf( _GsmBuffer, "AT+CPBR=%d\r", Position);
   // check for operator :
   _CommandSend( _GsmBuffer);
   if( !_ReplyGet( &Reply)){
      return( NO);
   }
   bprintf( _GsmBuffer, "CPBR:%d,\r", Position);
   Reply = _ReplyPrefix( Reply, _GsmBuffer);
   if( !Reply){
      return( NO);
   }
   if( !_ReplyString( Reply, PhoneNumber, GSM_PHONE_NUMBER_SIZE)){
      return( NO);
   }
   return( YES);
} // PhoneBookRead

//------------------------------------------------------------------------------
//  Phone book own set
//------------------------------------------------------------------------------

static TYesNo PhoneBookOwnSet( void)
// Set active phonebook to own
{
   char *Reply;

   _CommandSend( "AT+CPBS=\"ON\"\r");
   if( !_ReplyGet( &Reply)){
      return( NO);
   }
   Reply = _ReplyPrefix( Reply, REPLY_OK);
   if( !Reply){
      return( NO);
   }
   return( YES);         // pin not needed
} // PhoneBookOwnSet
