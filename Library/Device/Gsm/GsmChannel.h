//******************************************************************************
//
//   GsmChannel.h     Gsm channel routines
//   Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#ifndef __GsmChannel_H__
#define __GsmChannel_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __GsmDef_H__
   #include "Gsm/GsmDef.h"
#endif

//-- SMS channel --------------------------------------------------------------

TYesNo GsmSmsChannelInit( void);
// Initialize SMS channel service

void GsmSmsChannelDeinit( void);
// Deinitialize channel

TSmsChannel GsmSmsChannelSend( char *PhoneNumber, byte *Data, int DataSize);
// Send data stream to <PhoneNumber>, SMS_INVALID_CHANNEL returned when problem occured

TSmsChannel GsmSmsChannelSendAndDel( char *PhoneNumber, byte *Data, int DataSize);
// Send data stream to <PhoneNumber>, SMS_INVALID_CHANNEL returned when problem occured
// delete channel after succesfull/unsuccesfull send, GsmSmsChannelSendStatus on this channel is not valid

TSmsChannelSendStatus GsmSmsChannelSendStatus( TSmsChannel channel);
// Get status of current channel

TSmsChannel GsmSmsChannelIncomming(char *PhoneNumber, int *DataSize);
// Check new incomming data, return channel descriptor and originator phone, when some data available, else return SMS_INVALID_CHANNEL

int GsmSmsChannelRead(TSmsChannel channel, byte *data, int maxSize);
// Read data from channel with incomming data (as previously get by GsmSmsChannelInput)
// return size of actually readed data or SMS_INVALID_CHANNEL when all data readed or wrong channel descriptor

void GsmSmsChannelClose( TSmsChannel channel);
// Close existing Send stop data sending prematurely

void GsmSmsChannelExecutive();
// Executive for sms channel I/O operations
// !!! Warning !!! GsmSmsInit must be executed before

TYesNo GsmSmsChannelHasLiveChannel( void);
// return YES if there is some existing channel(out or in)

#endif // __GsmChannel_H__


