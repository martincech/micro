//*****************************************************************************
//
//   Adt7410.h    ADT7410 I2C thermometer services
//   Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Adt7410_H__
   #define __Adt7410_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Conversion duration :
#define TEMP_WAIT_CONVERSION 250	   // conversion delay [ms]

// Status register :
#define TEMP_STATUS_TLOW       (1 << 4)
#define TEMP_STATUS_THIGH      (1 << 5)
#define TEMP_STATUS_TCRIT      (1 << 6)
#define TEMP_STATUS_BUSY       (1 << 7)

// Identification :
#define TEMP_IDENTIFICATION_MASK 0xF8
#define TEMP_REVISION_MASK       0x07
#define TEMP_IDENTIFICATION_CHIP 0xC8

#define TempSignatureMatch( id)  (((id) & TEMP_IDENTIFICATION_MASK) == TEMP_IDENTIFICATION_CHIP)

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

#define TempInit()   IicInit()
// Initialization

byte TempIdentification( void);
// Returns chip ID

byte TempStatus( void);
// Returns status

void TempStartConversion( void);
// Start conversion

void TempStartPeriodic( void);
// Start periodic conversion

void TempShutdown( void);
// Stop conversions

int16 TempRead( void);
// Read raw temperature

int16 TempConvert( int16 TempRaw);
// Convert <TempRaw> to [0.1C]

int16 TempConvertRaw( int16 Temp);
// Convert <Temp> [0.1C] to raw temperature

int16 TempMeasure( void);
// Measure temperature [0.1C]

#endif
