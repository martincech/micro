//*****************************************************************************
//
//    Ad7192f.c    A/D convertor AD7192 services
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Ad7192/Ad7192f.h"
#include "Ads1232/Ads1232.h"
#include "Filter/FilterRelative.h"

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void AdcInit( void)
// Initialisation
{
} // AdcInit

//-----------------------------------------------------------------------------
// Reset
//-----------------------------------------------------------------------------

void AdcReset( void)
// Reset communication
{

} // AdcReset

//-----------------------------------------------------------------------------
// Data rate
//-----------------------------------------------------------------------------

void AdcDataRateSet( word Hertz)
// Set conversion data rate
{
} // AdcDataRateSet

//-----------------------------------------------------------------------------
// Enable SINC3
//-----------------------------------------------------------------------------

void AdcSinc3Set( TYesNo Enable)
// <Enable> SINC3 filtering
{
} // AdcSinc3Set

//-----------------------------------------------------------------------------
// Enable CHOP
//-----------------------------------------------------------------------------

void AdcChopSet( TYesNo Enable)
// <Enable> CHOP filtering
{
} // AdcChopSet

//-----------------------------------------------------------------------------
// Idle
//-----------------------------------------------------------------------------

void AdcIdle( void)
// Set idle mode
{

} // AdcIdle

//-----------------------------------------------------------------------------
// Power down
//-----------------------------------------------------------------------------

void AdcPowerDown( void)
// Set power down mode
{

} // AdcPoweDown

//-----------------------------------------------------------------------------
// Start
//-----------------------------------------------------------------------------

void AdcStart( void)
// Start continuous conversion
{
   FilterStart();                                     // start filtering
} // AdcStart

//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void AdcStop( void)
// Stop continous conversion
{

} // AdcStop

//-----------------------------------------------------------------------------
// Raw read
//-----------------------------------------------------------------------------

TAdcValue AdcRawRead( void)
// Read actual conversion
{
TAdcValue Value;

   return( Value);
} // AdcRawRead

//-----------------------------------------------------------------------------
// Average read
//-----------------------------------------------------------------------------

TAdcValue AdcAverageRead( void)
// Read averaged value
{
TAdcValue Average;

   return( Average);
} // AdcAverageRead

//-----------------------------------------------------------------------------
// Low pass read
//-----------------------------------------------------------------------------

TAdcValue AdcLowPassRead( void)
// Read filtered conversion
{
TRawWeight Value = 125;

   return( Value);
} // AdcLowPassRead

//-----------------------------------------------------------------------------
// Stable value read
//-----------------------------------------------------------------------------

TYesNo AdcRead( TAdcValue *Weight)
// Read stable value
{
TYesNo Result;

   return( Result);
} // AdcRead

//-----------------------------------------------------------------------------
// Callback enable
//-----------------------------------------------------------------------------

void AdcCallbackEnable( TYesNo Enable)
// Enables/disables ADC callback
{
} // AdcCallBackEnable

TYesNo AdcRunning( void)
{
   return YES;
}

void AdcSetFromModbus(TAdcModbusSample *Samples, byte Count)
{

}
