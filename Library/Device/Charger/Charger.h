//*****************************************************************************
//
//    Charger.h    Charger
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Charger_H__
   #define __Charger_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __PowerDef_H__
   #include "Power/PowerDef.h"
#endif

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void ChargerInit( void);
// Initialisation

void ChargerExecute( void);
// Execute

void ChargerTemperatureSet( int8 Temperature);
// Set battery temperature

byte ChargerStatus( void);
// Charger status

void ChargerInputEvent( void);
// Input event callback

#endif
