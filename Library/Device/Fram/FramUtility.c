//*****************************************************************************
//
//    FramUtility.c  FRAM data access utility
//    Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#include "Fram/Fram.h"

#define FRAM_COPY_SIZE   64

//-----------------------------------------------------------------------------
// Save
//-----------------------------------------------------------------------------

void FramSave( dword Address, const void *Data, int Size)
// Save <Data> with <Size> at <Address>
{
byte *p;

   p = (byte *)Data;
   FramPageWriteStart( Address);
   do {
      FramPageWriteData( *p);
      p++;
   } while( --Size);
   FramPageWritePerform();
} // FramSave

//-----------------------------------------------------------------------------
// Load
//-----------------------------------------------------------------------------

void FramLoad( dword Address, void *Data, int Size)
// Load <Data> with <Size> from <Address>
{
byte *p;

   p = (byte *)Data;
   FramBlockReadStart( Address);
   do {
      *p = FramBlockReadData();
      p++;
   } while( --Size);
   FramBlockReadStop();
} // FramLoad

//------------------------------------------------------------------------------
//   Fill
//------------------------------------------------------------------------------

void FramFill( dword Address, byte Pattern, int Size)
// Write <Pattern> with <Size> at <Address>
{
   FramPageWriteStart( Address);
   do {
      FramPageWriteData( Pattern);
   } while( --Size);
   FramPageWritePerform();
} // FramFill

//------------------------------------------------------------------------------
//   Match
//------------------------------------------------------------------------------

TYesNo FramMatch( dword Address, const void *Data, int Size)
// Compare <Data> with <Size> at <Address>
{
byte *p;

   p = (byte *)Data;
   FramBlockReadStart( Address);
   do {
      if( *p != FramBlockReadData()){
         FramBlockReadStop();
         return( NO);
      }
      p++;
   } while( --Size);
   FramBlockReadStop();
   return( YES);
} // FramMatch

//------------------------------------------------------------------------------
//   Sum
//------------------------------------------------------------------------------

dword FramSum( dword Address, int Size)
// Sum NVM contents at <Address> with <Size>
{
dword Sum;

   Sum = 0;
   FramBlockReadStart( Address);
   do {
      Sum += FramBlockReadData();
   } while( --Size);
   FramBlockReadStop();
   return( Sum);
} // FramSum

//------------------------------------------------------------------------------
//   Move
//------------------------------------------------------------------------------

void FramMove( dword ToAddress, dword FromAddress, int Size)
// Move data <FromAddress> to <ToAddress> with <Size>. Check for overlapping
{
byte  Buffer[ FRAM_COPY_SIZE];
word  BlockSize;
dword SrcAddress, DstAddress;

   if( ToAddress < FromAddress){
      // copy forward - from start to end
      SrcAddress = FromAddress;        // set address to start of block
      DstAddress = ToAddress;          // set address to start of block
      while( Size){
         // select block size :
         BlockSize = Size;
         if( BlockSize > FRAM_COPY_SIZE){
            BlockSize = FRAM_COPY_SIZE;
         }
         FramLoad( SrcAddress, Buffer, BlockSize);
         FramSave( DstAddress, Buffer, BlockSize);
         SrcAddress += BlockSize;      // move address to next block
         DstAddress += BlockSize;      // move address to next block
         Size       -= BlockSize;      // remainder of size
      }
   } else {
      // copy backward - from tail to head
      SrcAddress = FromAddress + Size; // set address to end of block
      DstAddress = ToAddress   + Size; // set address to end of block
      while( Size){
         // select block size :
         BlockSize = Size;
         if( BlockSize > FRAM_COPY_SIZE){
            BlockSize = FRAM_COPY_SIZE;
         }
         SrcAddress -= BlockSize;      // get tail address
         DstAddress -= BlockSize;      // get tail address
         FramLoad( SrcAddress, Buffer, BlockSize);
         FramSave( DstAddress, Buffer, BlockSize);
         Size       -= BlockSize;      // remainder of size
      }
   }
} // FramMove
