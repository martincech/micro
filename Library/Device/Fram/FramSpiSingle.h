//*****************************************************************************
//
//   FramSpi.h   FRAM memory SPI interface
//   Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __FramSpi_H__
   #define __FramSpi_H__
   
//--- SPI default interface
#include "Spi/SpiSingle.h"

#define spiFramInit()              SpiInit()
// Initialize bus

#define spiFramAttach()            SpiAttach()
// Attach SPI bus - activate chipselect

#define spiFramRelease()           SpiRelease()
// Release SPI bus - deactivate chipselect

#define spiFramByteRead()          SpiByteRead()
// Returns byte from SPI

#define spiFramByteWrite( Value)   SpiByteWrite( Value)
// Write byte to SPI

#endif
