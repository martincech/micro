//*****************************************************************************
//
//    Fm25h20.c    FM25H20 FRAM services
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Fm25h20.h"
#include "System/System.h"        // SysDelay

// Commands :
#define FRAM_WREN    0x06              // Set Write Enable Latch (Enable Write)
#define FRAM_WRDI    0x04              // Reset Write Enable/Reset Flag Bit
#define FRAM_RDSR    0x05              // Read Status Register
#define FRAM_WRSR    0x01              // Write Status Register
#define FRAM_READ    0x03              // Read data
#define FRAM_WRITE   0x02              // Write data
#define FRAM_SLEEP   0xB9              // Sleep

#define FRAM_WAKEUP_TIME 450           // us

// Status register format :
#define FRAM_MASK_WEL  0x02            // device is Write Enabled
#define FRAM_MASK_BP0  0x04            // protection bit 0 (1/4 capacity)
#define FRAM_MASK_BP1  0x08            // protection bit 1 (1/2 capacity)
#define FRAM_MASK_WPEN 0x80            // Write Protect /WP pin control

// Local functions

static void WrenCmd( void);
// Send WREN instruction

static byte RdsrCmd( void);
// Returns status register

static void WrsrCmd( byte Value);
// Write status register

static void WrdiCmd( void);
// Set write disable


//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void FramInit()
// Initialization
{
   spiFramInit();
   FramWakeup();
   WrenCmd();                          // set write enable latch
   WrsrCmd( 0);                        // clear write protection
} // FramInit

#ifdef FRAM_PRESENT
//-----------------------------------------------------------------------------
// Check for presence
//-----------------------------------------------------------------------------

TYesNo FramIsPresent( void)
// Returns YES if memory is present
{
   WrenCmd();                          // write enable - set WEN bit in the status register
   if( !(RdsrCmd() & FRAM_MASK_WEL)){
      return( NO);                     // not present
   }
   WrdiCmd();                          // write disable - clear WEN bit
   if(   RdsrCmd() & FRAM_MASK_WEL){
      return( NO);                     // not present
   }
   return( YES);
} // FramIsPresent
#endif // FRAM_PRESENT

#ifdef FRAM_BYTE_READ
//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------

byte FramByteRead( dword Address)
// Returns single byte from <Address>
{
byte Value;

   FramBlockReadStart( Address);       // block start
   Value = FramBlockReadData();        // write data byte
   FramBlockReadStop();                // stop block read
   return( Value);
} // FramByteRead

#endif // FRAM_BYTE_READ

#ifdef FRAM_BYTE_WRITE
//-----------------------------------------------------------------------------
// Write
//-----------------------------------------------------------------------------

void FramByteWrite( dword Address, byte Value)
// Write <Value> at <Address>
{
   FramPageWriteStart( Address);       // start page write
   FramPageWriteData( Value);          // write data byte
   FramPageWritePerform();             // save data
} // FramByteWrite

#endif // FRAM_BYTE_WRITE

//-----------------------------------------------------------------------------
// Page read
//-----------------------------------------------------------------------------

void FramBlockReadStart( dword Address)
// Block read start
{
   spiFramAttach();                    // select CS
   spiFramByteWrite( FRAM_READ);       // READ instruction
   spiFramByteWrite( Address >> 16);   // high address
   spiFramByteWrite( Address >> 8);    // mid address
   spiFramByteWrite( Address);         // low address
} // FramBlockReadStart

//-----------------------------------------------------------------------------
//  Start write
//-----------------------------------------------------------------------------

void FramPageWriteStart( dword Address)
// Start page write at <Address>
{
   WrenCmd();                          // Set write enable latch
   spiFramAttach();                    // select CS
   spiFramByteWrite( FRAM_WRITE);      // WRITE instruction
   spiFramByteWrite( Address >> 16);   // high address
   spiFramByteWrite( Address >> 8);    // mid address
   spiFramByteWrite( Address);         // low address
} // FramPageWriteStart

//-----------------------------------------------------------------------------
// Sleep
//-----------------------------------------------------------------------------

void FramSleep( void)
// Enter sleep mode
{
   spiFramAttach();                    // select CS
   spiFramByteWrite( FRAM_SLEEP);
   spiFramRelease();                   // deselect CS
} // FramSleep

//-----------------------------------------------------------------------------
// Wakeup
//-----------------------------------------------------------------------------

void FramWakeup( void)
// Wakeup from sleep mode
{
   spiFramAttach();                    // select CS
   SysUDelay( FRAM_WAKEUP_TIME);       // wait for wakeup
   spiFramByteWrite( FRAM_WRDI);       // some command
   spiFramRelease();                   // deselect CS
} // FramWakeup

//-----------------------------------------------------------------------------
// Instruction WREN
//-----------------------------------------------------------------------------

static void WrenCmd()
// Send WREN instruction
{
   spiFramAttach();                    // select CS
   spiFramByteWrite( FRAM_WREN);
   spiFramRelease();                   // deselect CS
} // WrenCmd


//-----------------------------------------------------------------------------
// Instruction RDSR
//-----------------------------------------------------------------------------

static byte RdsrCmd()
// Returns status register
{
byte Value;

   spiFramAttach();                    // select CS
   spiFramByteWrite( FRAM_RDSR);
   Value  = spiFramByteRead();
   spiFramRelease();                   // deselect CS
   return( Value);
}  // RdsrCmd

//-----------------------------------------------------------------------------
// Instruction WRSR
//-----------------------------------------------------------------------------

static void WrsrCmd( byte Value)
// Write status register
{
   spiFramAttach();                    // select CS
   spiFramByteWrite( FRAM_WRSR);
   spiFramByteWrite( Value);
   spiFramRelease();                   // deselect CS
}  // WrsrCmd

//-----------------------------------------------------------------------------
// Instruction WRDI
//-----------------------------------------------------------------------------

static void WrdiCmd( void)
// Set write disable
{
   spiFramAttach();                    // select CS
   spiFramByteWrite( FRAM_WRDI);
   spiFramRelease();                   // deselect CS
} // WrdiCmd
