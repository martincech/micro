//*****************************************************************************
//
//    FramTest.c   FRAM memory test
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "FramTest.h"
#include "Console/conio.h"        // Console output

#define MAGIC_OFFSET  2
#define START_ADDRESS 0
#define TEST_SIZE     FRAM_SIZE

//-----------------------------------------------------------------------------
// Page test
//-----------------------------------------------------------------------------

TYesNo FramPageTest( void)
// Run memory page test
{
int i;
byte Value;

   // write pattern :
   FramPageWriteStart( START_ADDRESS);
   for( i = 0; i < TEST_SIZE; i++){
      FramPageWriteData( (i + MAGIC_OFFSET) & 0xFF);
   }
   FramPageWritePerform();
   // read pattern :
   FramBlockReadStart( START_ADDRESS);
   for( i = 0; i < TEST_SIZE; i++){
      Value = FramBlockReadData();
      if( Value != ((i + MAGIC_OFFSET) & 0xFF)){
         FramBlockReadStop();
         cprintf( "\nFRAM error @%04x : %02x\n", i, Value);
         return( NO);
      }
   }
   FramBlockReadStop();
   return( YES);
} // FramPageTest

//-----------------------------------------------------------------------------
// Page test write
//-----------------------------------------------------------------------------

TYesNo FramPageTestWrite( void)
// Run memory page test write
{
int i;
byte Value;

   // write pattern :
   FramPageWriteStart( START_ADDRESS);
   for( i = 0; i < TEST_SIZE; i++){
      FramPageWriteData( (i + MAGIC_OFFSET) & 0xFF);
   }
   FramPageWritePerform();
   return( YES);
} // FramPageTestWrite

//-----------------------------------------------------------------------------
// Page test check
//-----------------------------------------------------------------------------

TYesNo FramPageTestCheck( void)
// Run memory page test check
{
int i;
byte Value;

   // read pattern :
   FramBlockReadStart( START_ADDRESS);
   for( i = 0; i < TEST_SIZE; i++){
      Value = FramBlockReadData();
      if( Value != ((i + MAGIC_OFFSET) & 0xFF)){
         FramBlockReadStop();
         cprintf( "\nFRAM error @%04x : %02x\n", i, Value);
         return( NO);
      }
   }
   FramBlockReadStop();
   return( YES);
} // FramPageTestCheck

//-----------------------------------------------------------------------------
// Pattern test
//-----------------------------------------------------------------------------

TYesNo FramPatternTest( byte Pattern)
// Run memory pattern test
{
int  i;
byte Value;

   // write pattern :
   FramFill( START_ADDRESS, Pattern, TEST_SIZE);
   // read pattern :
   i = TEST_SIZE;
   FramBlockReadStart( START_ADDRESS);
   do {
      Value = FramBlockReadData();
      if( Value != Pattern){
         FramBlockReadStop();
         cprintf( "\nFRAM error @%04x : %02x\n", i, Value);
         return( NO);
      }
   } while( --i);
   FramBlockReadStop();
   return( YES);
} // FramPatternTest
