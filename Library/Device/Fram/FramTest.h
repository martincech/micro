//*****************************************************************************
//
//    FramTest.h   FRAM memory test
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __FramTest_H__
   #define __FramTest_H__

#ifndef __Fram_H__
   #include "Fram/Fram.h"
#endif   

TYesNo FramPageTest( void);
// Run memory page test

TYesNo FramPageTestWrite( void);
// Run memory page test write

TYesNo FramPageTestCheck( void);
// Run memory page test check

TYesNo FramPatternTest( byte Pattern);
// Run memory pattern test

#endif
