//*****************************************************************************
//
//   FramSpi.h    FRAM memory SPI interface
//   Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __FramSpi_H__
   #define __FramSpi_H__
   
#include "Spi/Spi.h"

#define spiFramInit()              SpiInit( SPI_FRAM_CHANNEL)
// Initialize bus

#define spiFramAttach()            SpiAttach( SPI_FRAM_CHANNEL)
// Attach SPI bus - activate chipselect

#define spiFramRelease()           SpiRelease( SPI_FRAM_CHANNEL)
// Release SPI bus - deactivate chipselect

#define spiFramByteRead()          SpiByteRead( SPI_FRAM_CHANNEL)
// Returns byte from SPI

#define spiFramByteWrite( Value)   SpiByteWrite( SPI_FRAM_CHANNEL, Value)
// Write byte to SPI

#endif
