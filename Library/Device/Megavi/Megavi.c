//*****************************************************************************
//
//    Megavi.c       Serial port Megavi communication
//    Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#include "Megavi.h"
#include "Uart/Uart.h"
#include "Megavi/MegaviConfig.h"  // Messaging configuration
#include "Hardware.h"

#include <string.h>

//------------------------------------------------------------------------------
//  Megavi module definitions
//------------------------------------------------------------------------------
#define MEGAVI_FORMAT                  UART_9BIT
#define MEGAVI_INTERCHARACTER_TIMEOUT  50
#define MEGAVI_BAUD_DEFAULT            19200
#define MEGAVI_SEND_DELAY              2

static TMegaviCommand _MegaviRxBuffer;
static TMegaviReply   _MegaviTxBuffer;
static EUartAddress   uartAddress;
//------------------------------------------------------------------------------
//   Initialize
//------------------------------------------------------------------------------

void MegaviInit( int uartPort)
// Initialize
{
   uartAddress = uartPort;
   UartInit( uartAddress);
   UartSetup( uartAddress, MEGAVI_BAUD_DEFAULT, MEGAVI_FORMAT);
   UartTimeoutSet( uartAddress, UART_TIMEOUT_OFF, MEGAVI_INTERCHARACTER_TIMEOUT);
   UartSendDelaySet( uartAddress, MEGAVI_SEND_DELAY);
   UartModeSet( uartAddress, UART_MODE_MEGAVI_SLAVE);
   UartBufferSet( uartAddress, &_MegaviRxBuffer, sizeof(TMegaviCommand));
}// MegaviInit

//------------------------------------------------------------------------------
//   Deinitialize
//------------------------------------------------------------------------------

void MegaviDeinit( void)
// Deinitialize
{
    MegaviListenStop();
    UartDeinit( uartAddress);
}

//------------------------------------------------------------------------------
//   Status
//------------------------------------------------------------------------------

TMegaviStatus MegaviStatus( void)
// Check for last operation status
{
   switch( UartReceiveStatus( uartAddress)){
      case UART_IDLE:
         return( MEGAVI_IDLE);

      case UART_RECEIVE_ACTIVE:
         return( MEGAVI_RECEIVE_LISTENING);

      case UART_RECEIVE_FRAMING_ERROR :
      case UART_RECEIVE_OVERRUN_ERROR :
      case UART_RECEIVE_SIZE_ERROR :
      case UART_RECEIVE_CRC_ERROR:
         return( MEGAVI_RECEIVE_ERROR);      // receive error

      case UART_RECEIVE_FRAME:
         if( UartReceiveAddress( uartAddress) != MegaviOptions.ID){
            MegaviListenStart();
            return( UART_RECEIVE_ACTIVE);           // wrong address, throw message
         }
         return( MEGAVI_RECEIVE_DATA_WAITING);       // receive O.K.

      case UART_SEND_ACTIVE :
         return( MEGAVI_SEND_ACTIVE);

      case UART_SEND_DONE :
         return( MEGAVI_SEND_DONE);

      default :
         // unexpected status
         return( MEGAVI_RECEIVE_ERROR);     // receive error     
   }
}// MegaviStatus

//------------------------------------------------------------------------------
//   RX reset
//------------------------------------------------------------------------------

void MegaviListenStart( void)
// Resets listening on port when error occurred
{
   UartFlush( uartAddress);
   UartReceive( uartAddress);
}

//------------------------------------------------------------------------------
//   RX/TX stop
//------------------------------------------------------------------------------

void MegaviListenStop( void)
// Stops listening on port
{
   UartStop( uartAddress);
}

//------------------------------------------------------------------------------
//   Read RX buffer
//------------------------------------------------------------------------------

TMegaviCommand *MegaviReceive( void)
// Reads incoming data from receive buffer, succeed only when MEGAVI_RECEIVE_DATA_WAITING 
{
int   ReceiveSize;

   switch( UartReceiveStatus( uartAddress)){
      case UART_RECEIVE_FRAME:
         break;                              // receive O.K., continue

      default :
         return NULL;
   }

   ReceiveSize    = UartReceiveSize( uartAddress);
   if( ReceiveSize < 1){
      return 0;
   }

   switch( _MegaviRxBuffer.CommandCode){
      case MEGAVI_COMMAND_VERSION :
      case MEGAVI_COMMAND_MALE_DATA_GET :
      case MEGAVI_COMMAND_FEMALE_DATA_GET :
         if( ReceiveSize != 1){
            return NULL;
         }
         break;
      
      case MEGAVI_COMMAND_SMS_SEND :
         if( ReceiveSize != sizeof(TMegaviCommandSmsSendData) + 1){
            return NULL;
         }
         break;
      
      case MEGAVI_COMMAND_SMS_STATUS_GET :
        if( ReceiveSize != sizeof(TMegaviCommandSmsStatusData) + 1){
            return NULL;
         }
         break;

      default :
         return NULL;
   }

   return &_MegaviRxBuffer;
}

//------------------------------------------------------------------------------
//   Send TX buffer
//------------------------------------------------------------------------------

TYesNo MegaviSend( void)
// Sends reply,  succeed only when MEGAVI_IDLE, MEGAVI_SEND_DONE, MEGAVI_RECEIVE_LISTENING or MEGAVI_RECEIVE_DATA_WAITING
{
int SendSize;

   switch( UartReceiveStatus( uartAddress)){
      case UART_IDLE:
      case UART_RECEIVE_FRAME:
      case UART_SEND_DONE :
      case UART_RECEIVE_ACTIVE :
         break;

      default :
         return NO;      
   }
   
   SendSize = sizeof(_MegaviTxBuffer.ReplyCode);
   switch( _MegaviTxBuffer.ReplyCode){
      case MEGAVI_REPLY_VERSION :
         SendSize += sizeof(TMegaviReplyVersionData);
         break;
      
      case MEGAVI_REPLY_MALE_DATA :
      case MEGAVI_REPLY_FEMALE_DATA :
         SendSize += sizeof(TMegaviReplyData);
         break;
      
      case MEGAVI_REPLY_SMS_SEND :
         SendSize += sizeof(TMegaviReplySmsSendData);
         break;

      case MEGAVI_REPLY_SMS_STATUS :
         SendSize += sizeof(TMegaviCommandSmsStatusData);
         break;

      default :
         return NO;
   }

   UartSendAddressSet( uartAddress, MegaviOptions.ID);
   UartSend( uartAddress, &_MegaviTxBuffer, SendSize);
   return YES;
}

//------------------------------------------------------------------------------
//   Get tx buffer pointer
//------------------------------------------------------------------------------

TMegaviReply *MegaviGetReplyBuffer()
// Returns buffer for outgoing message, should be filled before calling MegaviSend
{
    return &_MegaviTxBuffer;
}
