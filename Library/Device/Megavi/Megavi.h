//*****************************************************************************
//
//    Megavi.h       Serial port Megavi communication
//    Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Megavi_H__
   #define __Megavi_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __MegaviDef_H__
   #include "Megavi/MegaviDef.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif

//------------------------------------------------------------------------------
//  Data types
//------------------------------------------------------------------------------

typedef enum {
   MEGAVI_IDLE,
   MEGAVI_RECEIVE_LISTENING,           // Listening any incoming communication 
   MEGAVI_RECEIVE_ERROR,               // Error while processing incoming message
   MEGAVI_RECEIVE_DATA_WAITING,        // Incoming command successfully received
   MEGAVI_SEND_ACTIVE,                 // Sending of reply active
   MEGAVI_SEND_DONE,                   // Sending of reply finished
   _MEGAVI_LAST
} EMegaviStatus;

typedef byte TMegaviStatus;

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------
void MegaviInit( int uartPort);
// Initialize

void MegaviDeinit( void);
// Deinitialize

TMegaviStatus MegaviStatus( void);
// Check for last operation status

void MegaviListenStart( void);
// Starts listening on port

void MegaviListenStop( void);
// Stops listening on port

TMegaviCommand *MegaviReceive( void);
// Reads incoming data from receive buffer, succeed only when MEGAVI_RECEIVE_DATA_WAITING 
// return NULL pointer when no data avaible or data error

TYesNo MegaviSend( void);
// Sends reply,  succeed only when MEGAVI_IDLE, MEGAVI_SEND_DONE, MEGAVI_RECEIVE_LISTENING or MEGAVI_RECEIVE_DATA_WAITING
// and reply buffer validly set

TMegaviReply *MegaviGetReplyBuffer();
// Returns buffer for outgoing message, should be filled before calling MegaviSend

#ifdef __cplusplus
   }
#endif

#endif // __Megavi_H__
