   //*****************************************************************************
//
//   MegaviDef.h  MEGAVI protocol definitions
//   Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __MegaviDef_H__
   #define __MegaviDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

//------------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------------

#define MEGAVI_SMS_SIZE           160        // megavi SMS message size

#define MEGAVI_LEADER             0x55       // frame leader character
#define MEGAVI_TRAILER            0xAA       // frame trailer character 

#define MEGAVI_MODULE_CODE        0x0A       // bat 2 default module code as specified by SODALEC
//-----------------------------------------------------------------------------
// Command codes
//-----------------------------------------------------------------------------

typedef enum {
   MEGAVI_COMMAND_UNDEFINED,           // undefined/unused command
   MEGAVI_COMMAND_VERSION,             // get version info
   MEGAVI_COMMAND_MALE_DATA_GET,       // get statistics & weighings for male gender
   MEGAVI_COMMAND_FEMALE_DATA_GET,     // get statistics & weighings for female gender
   MEGAVI_COMMAND_SMS_SEND,            // send SMS
   MEGAVI_COMMAND_SMS_STATUS_GET,      // get SMS status
   _MEGAVI_COMMAND_LAST                // enum delimiter
} EMegaviCommand;

//-----------------------------------------------------------------------------
// Data for Sms send command
//-----------------------------------------------------------------------------

#define MEGAVI_SMS_TARGET_BROADCAST   '\0'                 // broadcast SMS to all phone numbers
#define MEGAVI_SMS_TARGET_MIN         ' '                  // single SMS reply character min
#define MEGAVI_SMS_TARGET_MAX         '~'                  // single SMS reply character max


#ifdef __WIN32__
   #pragma pack( push, 1)              // byte alignment
#endif

typedef byte      TMegaviSmsTarget;

typedef struct {
   TMegaviSmsTarget  SmsTarget;
   char             SmsText[MEGAVI_SMS_SIZE];
} __packed TMegaviCommandSmsSendData;

//-----------------------------------------------------------------------------
// Data for Sms status command
//-----------------------------------------------------------------------------
#include "SmsGate/SmsGateDef.h"

typedef struct {
   TSmsGateSlotNumber    SlotNumber;
} __packed TMegaviCommandSmsStatusData;

//-----------------------------------------------------------------------------
// Megavi command
//-----------------------------------------------------------------------------

typedef struct {
   byte                             CommandCode;   // Command code
   union {                                         // Data union, set according to Command field
      TMegaviCommandSmsSendData     SmsSendData;   // Send sms command data
      TMegaviCommandSmsStatusData   SmsStatusData; // Sms status command data
   };
} __packed TMegaviCommand;

#ifdef __WIN32__
   #pragma pack( pop)                  // original alignment
#endif

//*****************************************************************************
//-----------------------------------------------------------------------------
// Reply codes
//-----------------------------------------------------------------------------

#define MEGAVI_REPLY_BASE         0x80       // reply code base

typedef enum {
   MEGAVI_REPLY_UNDEFINED = MEGAVI_REPLY_BASE, // undefined/unused reply
   MEGAVI_REPLY_VERSION,          // version reply
   MEGAVI_REPLY_MALE_DATA,        // statistics & weighings data reply for male gender
   MEGAVI_REPLY_FEMALE_DATA,      // statistics & weighings data reply for female gender
   MEGAVI_REPLY_SMS_SEND,         // SMS send reply
   MEGAVI_REPLY_SMS_STATUS,       // SMS status reply
   _MEGAVI_REPLY_LAST             // enum delimiter
} EMegaviReply;

//-----------------------------------------------------------------------------
// Data for version reply
//-----------------------------------------------------------------------------
#define MEGAVI_MAJOR 0x01
#define MEGAVI_MINOR 0x01

#ifdef __WIN32__
   #pragma pack( push, 1)              // byte alignment
#endif

typedef struct {
   byte Minor;                         // minor version number
   byte Major;                         // major version number
} __packed TMegaviReplyVersionData;

//-----------------------------------------------------------------------------
// Get male/female data statistics
//-----------------------------------------------------------------------------

typedef struct {
   word  Target;
   word  Count;
   word  Average;
   int32 Gain;
   word  Sigma;
   word  Uniformity;
}__packed TMegaviReplyStatistics;

#ifdef __WIN32__
   #pragma pack( pop)                  // original alignment
#endif

//-----------------------------------------------------------------------------
// SMS request code
//-----------------------------------------------------------------------------

#define MEGAVI_SMS_REQUEST_NONE       '\0'                 // no SMS request
#define MEGAVI_SMS_REQUEST_SHORT      ' '                  // SMS request without data
#define MEGAVI_SMS_REQUEST_MIN        ((char)(' ' + 1))    // SMS request character min
#define MEGAVI_SMS_REQUEST_MAX        '~'                  // SMS request character max

typedef byte TMegaviSmsRequest;

//-----------------------------------------------------------------------------
// Male/Female data response
//-----------------------------------------------------------------------------

#define MEGAVI_WEIGHT_COUNT  50                    // last weighings count

typedef word TMegaviReplyWeight;

#ifdef __WIN32__
   #pragma pack( push, 1)              // byte alignment
#endif

typedef struct {
   TMegaviSmsRequest        SmsRequest;
   TMegaviReplyStatistics   Statistics;
   TMegaviReplyWeight       Weight[ MEGAVI_WEIGHT_COUNT];
} __packed  TMegaviReplyData;

#ifdef __WIN32__
   #pragma pack( pop)                  // original alignment
#endif
//-----------------------------------------------------------------------------
// SMS send data response
//-----------------------------------------------------------------------------
#include "SmsGate/SmsGateDef.h"

typedef TMegaviCommandSmsStatusData TMegaviReplySmsSendData;

//-----------------------------------------------------------------------------
// SMS status data response
//-----------------------------------------------------------------------------

typedef struct {
   byte Status;                        // SMS status code
} TMegaviReplySmsStatusData;



//-----------------------------------------------------------------------------
// Megavi reply
//-----------------------------------------------------------------------------


typedef struct {
   byte                          ReplyCode;        // reply code
   union {                                         // Data union, set according to Command field
      TMegaviReplyVersionData    Version;
      TMegaviReplyData           Data;
      TMegaviReplySmsSendData    SmsSendData;
      TMegaviReplySmsStatusData  SmsStatusData;
   };
} TMegaviReply;

#endif
