//******************************************************************************
//
//    FwLoader.c         FW bootloader
//    Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "FwLoader.h"
#include "FwStorage.h"
#include "Flash/IFlash.h"
#include "Fw/Fw.h"
#include <string.h>

static byte State;
static int Address;
static TYesNo Init, Test;

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void FwLoaderInit(void)
// Initialisation
{
   IFlashInit();
   
   State = FW_LOADER_BUSY;
   Init = YES;
   Test = YES;
} // FwLoaderInit

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

#define CHUNK_SIZE             64

void FwLoaderExecute( void)
// Execute
{
byte Buffer[CHUNK_SIZE];

   if(State != FW_LOADER_BUSY) {
      return;
   }

   if(Init) {
      FwInit( Test);
      FwStorageInit();
      Address = 0;
      Init = NO;
   }

   if(!FwStorageRead(Address, Buffer, CHUNK_SIZE)) {
      State = FW_LOADER_ERROR;
      return;
   }
   Address += CHUNK_SIZE;
   switch(FwExecute(Buffer, CHUNK_SIZE)) {
      case FW_STATUS_PROCESSING:
         break;

      case FW_STATUS_DONE:
         if(Test) {
            Test = NO;
            Init = YES;
            break;
         }
         FwStorageInvalidate();
         State = FW_LOADER_DONE;
         break;

      default:
         State = FW_LOADER_ERROR;
         break;
   }
} // FwLoaderExecute

//-----------------------------------------------------------------------------
// State
//-----------------------------------------------------------------------------

int FwLoaderState( void)
// State
{
   return State;
} // FwLoaderState

TYesNo FwLoaderReady( void) {
   return FwStorageReady();
}