//******************************************************************************
//
//    FwLoader.c         FW bootloader
//    Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __FwLoader_H__
   #define __FwLoader_H__

#include "Unisys/Uni.h"

typedef enum {
   FW_LOADER_IDLE,
   FW_LOADER_BUSY,
   FW_LOADER_DONE,
   FW_LOADER_ERROR,
} EFwLoaderStatus;

void FwLoaderInit(void);
// Initialisation

void FwLoaderExecute( void);
// Execute

int FwLoaderState( void);
// State

TYesNo FwLoaderReady( void);

#endif