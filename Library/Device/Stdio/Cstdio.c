//*****************************************************************************
//
//    Cstdio.c     Standard input/output via UART
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include <stdio.h>
#include "Hardware.h"
#include "Uart/Uart.h"

#define ComTxChar( c)          UartTxChar(CONSOLE_CHANNEL, c)
#define ComRxWait( Timeout)    UartRxWait(CONSOLE_CHANNEL, Timeout)
#define ComRxChar( c)          UartRxChar(CONSOLE_CHANNEL, c)

//-----------------------------------------------------------------------------
// Putchar
//-----------------------------------------------------------------------------

int __putchar( int __c)
// putchar
{
   ComTxChar( __c);
   return __c;
} // __putchar

//-----------------------------------------------------------------------------
// Keyboard hit
//-----------------------------------------------------------------------------
static byte Char;
static byte CharFlag = 0;

int kbhit( void) {
   if(CharFlag) {
      return 1;
   }

   if(!ComRxWait(1)) {
      return 0;
   }

   ComRxChar( &Char);

   if(Char == '\r') {
      return 0;
   }
   
   CharFlag = 1;

   return 1;
}

//-----------------------------------------------------------------------------
// Getchar
//-----------------------------------------------------------------------------

#define RX_BUFFER_SIZE              100
#define HasBeenCalledAfterReset()   (RxBufferPointer == 0)
#define Reset()                     (RxBufferPointer = 0)
#define PointerRewind()             (RxBufferPointer = RxBuffer)
#define ReplaceCarriageReturn( Ch)  (Ch == '\r' ? '\n' : Ch)
#define IsLineDelimiter( Ch)        (Ch == '\n')
#define IsBufferFull()              ((RxBufferPointer - RxBuffer) >= (RX_BUFFER_SIZE - 1))
#define Store( Ch)                  *RxBufferPointer = Ch;RxBufferPointer++
#define Echo( Ch)                   __putchar(Ch)
#define Read()                      *RxBufferPointer;RxBufferPointer++

static byte RxBuffer[ RX_BUFFER_SIZE];
static byte *RxBufferPointer;

int __getchar( void)
// Store characters until Line Delimiter received
{
   while(!kbhit());
   CharFlag = 0;
   return Char;

} // __getchar