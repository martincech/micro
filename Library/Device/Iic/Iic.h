//*****************************************************************************
//
//    Iic.h         I2C controller services
//    Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Iic_H__
   #define __Iic_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#define IIC_READ   1
#define IIC_WRITE  0
#define IicStartByte(Address, Read) ((Address << 1) | (Read))

// IIC address :
typedef enum {
   IIC_IIC0,
   IIC_IIC1,
   IIC_IIC2,
   IIC_IIC3,
   IIC_IIC4,
   IIC_IIC5,
   IIC_IIC6,
   IIC_IIC7,
   IIC_IIC8,
   IIC_IIC9
} EIicAddress;

// Enumeration types :
typedef byte TIicAddress;             // data type for EIicAddress

void IicInit( TIicAddress Iic);
// Interface initialisation

void IicDeinit( TIicAddress Iic);
// Deinitialize bus

void IicStart( TIicAddress Iic);
// Sends start sequence

void IicStop( TIicAddress Iic);
// Sends stop sequence

TYesNo IicSend( TIicAddress Iic, byte value);
// Sends <value>, returns YES if ACK

byte IicReceive( TIicAddress Iic, TYesNo ack);
// Returns received byte, sends confirmation <ack>.
// <ack> = YES sends ACK, NO sends NOT ACK

#endif
