//*****************************************************************************
//
//   At25dfxx.h     Flash memory AT25DFxxx
//   Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __At25dfxx_H__
   #define __At25dfxx_H__

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

#include "Flash/NorFlash.h"

void FlashPowerDown( void);
// Power down

void FlashPowerDownResume( void);
// Power down resume (wake up)

//-----------------------------------------------------------------------------
// Device data
//-----------------------------------------------------------------------------

#define FLASH_MANUFACTURER    0x1F

#if defined(__AT25DF641__)
   #define FLASH_WRITE_SIZE   256                // maximum block write size
   #define FLASH_ERASE_SIZE   (4  * 1024)        // sector erase size
   #define FLASH_PROTECT_SIZE (64 * 1024)        // sector protection size
   #define FLASH_SIZE         (8  * 1024 * 1024) // 64Mbits
   
   // Device identification :
   #define FLASH_DEVICE_ID1      0x48            // AT25DFxxx & density
   #define FLASH_DEVICE_ID2      0x00            // Subcode & product version
   #define FLASH_EXTENDED_LENGTH 0x00            // no additional info
#else
   #error "Unknown FLASH DATA device"
#endif


#endif
