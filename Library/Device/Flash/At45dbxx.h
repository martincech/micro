//*****************************************************************************
//
//   At45dbxx.h     Flash memory AT45DBxxx
//   Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __At45dbxx_H__
   #define __At45dbxx_H__
  
//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

#include "Flash/DataFlash.h"

//-----------------------------------------------------------------------------
// Device data
//-----------------------------------------------------------------------------

// Memory page size and pages count :
#if defined(__AT45DB161__) || defined(__AT45DB321__)
   #define FLASH_PAGE_SIZE     528

   #ifdef __AT45DB161__
      #define FLASH_PAGES          4096
      #define FLASH_SIGNATURE      0x2C
      #define FLASH_SIGNATURE_MASK 0x3C
   #endif
   #ifdef __AT45DB321__
      #define FLASH_PAGES          8192
      #define FLASH_SIGNATURE      0x34
      #define FLASH_SIGNATURE_MASK 0x3C
   #endif
#elif defined(__AT45DB081__) || defined(__AT45DB041__) || defined(__AT45DB021__) || defined(__AT45DB011__)
   #define FLASH_PAGE_SIZE     264

   #ifdef __AT45DB011__
      #define FLASH_PAGES          512
      #define FLASH_SIGNATURE      0x0C
      #define FLASH_SIGNATURE_MASK 0x3C
   #endif
   #ifdef __AT45DB021__
      #define FLASH_PAGES          1024
      #define FLASH_SIGNATURE      0x14
      #define FLASH_SIGNATURE_MASK 0x3C
   #endif
   #ifdef __AT45DB041__
      #define FLASH_PAGES          2048
      #define FLASH_SIGNATURE      0x18
      #define FLASH_SIGNATURE_MASK 0x38
   #endif
   #ifdef __AT45DB081__
      #define FLASH_PAGES          4096
      #define FLASH_SIGNATURE      0x20
      #define FLASH_SIGNATURE_MASK 0x38
   #endif
#elif defined(__AT45DB642__)
   #define FLASH_PAGE_SIZE 1056
   
   #define FLASH_PAGES     8192
   #error "AT45DB64 TODO !"
#else
   #error "Unknown FLASH DATA device"
#endif

#define FlashCheckSignature( Status)    (((Status) & FLASH_SIGNATURE_MASK) == FLASH_SIGNATURE)

#endif
