//*****************************************************************************
//
//   FlashSpi.h   Flash memory SPI interface
//   Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __FlashSpi_H__
   #define __FlashSpi_H__

#include "Spi/Spi.h"

#define spiFlashInit()              SpiInit( SPI_FLASH_CHANNEL)
// Initialize bus

#define spiFlashAttach()            SpiAttach( SPI_FLASH_CHANNEL)
// Attach SPI bus - activate chipselect

#define spiFlashRelease()           SpiRelease( SPI_FLASH_CHANNEL)
// Release SPI bus - deactivate chipselect

#define spiFlashByteRead()          SpiByteRead( SPI_FLASH_CHANNEL)
// Returns byte from SPI

#define spiFlashByteWrite( Value)   SpiByteWrite( SPI_FLASH_CHANNEL, Value)
// Write byte to SPI

#endif
