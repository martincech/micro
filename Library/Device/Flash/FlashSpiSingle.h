//*****************************************************************************
//
//   FlashSpi.h  Flash memory SPI interface
//   Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __FlashSpi_H__
   #define __FlashSpi_H__
   
//--- SPI default interface
#include "Spi/SpiSingle.h"

#define spiFlashInit()              SpiInit()
// Initialize bus

#define spiFlashAttach()            SpiAttach()
// Attach SPI bus - activate chipselect

#define spiFlashRelease()           SpiRelease()
// Release SPI bus - deactivate chipselect

#define spiFlashByteRead()          SpiByteRead()
// Returns byte from SPI

#define spiFlashByteWrite( Value)   SpiByteWrite( Value)
// Write byte to SPI

#endif
