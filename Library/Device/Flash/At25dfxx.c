//*****************************************************************************
//
//   At25dfxx.c     Flash memory AT25DFxxx
//   Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#include "At25dfxx.h"
#include "System/System.h"         // SysTime
#include "Timer/Timer.h"           // Time interval measuring

//-----------------------------------------------------------------------------
// Commands
//-----------------------------------------------------------------------------

#define FLASH_ARRAY_READ       0x03       // up to 45MHz
#define FLASH_BLOCK_ERASE      0x20       // 4k (FLASH_ERASE_SIZE) block erase
#define FLASH_CHIP_ERASE       0x60       // erase all device
#define FLASH_PROGRAM          0x02       // 1..FLASH_WRITE_SIZE bytes (up to 75MHz)
#define FLASH_WRITE_ENABLE     0x06       // write enable command
#define FLASH_SECTOR_UNPROTECT 0x39       // unprotect sector
#define FLASH_STATUS_READ      0x05       // read status 1..2 bytes
#define FLASH_STATUS1_WRITE    0x01       // write status1 byte
#define FLASH_STATUS2_WRITE    0x31       // write status2 byte
#define FLASH_DEVICE_ID_READ   0x9F       // read device identification

#define FLASH_POWER_DOWN       0xB9       // deep power down
#define FLASH_WAKEUP           0xAB       // resume from deep power down

#define FLASH_WAKEUP_TIME       30       // us

//-----------------------------------------------------------------------------
// Status
//-----------------------------------------------------------------------------

// status register flags byte 1 :
#define FLASH_STATUS_SPRL  (1 << 7)       // sector protection registers locked
//#define FLASH_STATUS_RES (1 << 6)       // spare
#define FLASH_STATUS_EPE   (1 << 5)       // erase or program error detected
#define FLASH_STATUS_WPP   (1 << 4)       // write protect pin status
#define FLASH_STATUS_SWP   (2 << 2)       // software protection status
#define FLASH_STATUS_WEL   (1 << 1)       // write enable
#define FLASH_STATUS_BSY   (1 << 0)       // device busy

// status register flags byte 2 :
//#define FLASH_STATUS_RES (1 << 7)       // spare
//#define FLASH_STATUS_RES (1 << 6)       // spare
//#define FLASH_STATUS_RES (1 << 5)       // spare
#define FLASH_STATUS_RSTE  (1 << 4)       // reset command enabled
#define FLASH_STATUS_SLE   (1 << 3)       // Sector lockdown enabled
#define FLASH_STATUS_PS    (1 << 2)       // Sector program suspended
#define FLASH_STATUS_ES    (1 << 1)       // Sector erase suspended
//#define FLASH_STATUS_BSY (1 << 0)       // device busy

// status commands :
#define FLASH_COMMAND_GLOBAL_UNPROTECT 0x00    // clear all protections

//-----------------------------------------------------------------------------

// Local functions :
static byte _StatusRead( void);
// Read first status byte

static void _StatusWrite( byte Status);
// Write first status byte

TYesNo _WaitForReady( dword Timeout);
// Wait for ready with <Timeout>

static void _AddressWrite( dword Address);
// Send <Address> to device

static void _WriteEnable( void);
// Write enable command

//static void _SectorUnprotect( dword Address);
// Unprotect sector with <Address>

static void _SectorUnprotectAll( void);
// Unprotect all sectors

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void FlashInit()
// Initialization
{
   spiFlashInit();
   FlashPowerDownResume();
   _SectorUnprotectAll();
} // FlashInit

//-----------------------------------------------------------------------------
// Wait
//-----------------------------------------------------------------------------

TYesNo FlashWaitForReady( void)
// Wait for ready
{
   return( _WaitForReady( FLASH_TIMEOUT));
} // FlashWaitForReady

//-----------------------------------------------------------------------------
// Identification
//-----------------------------------------------------------------------------

void FlashIdentification( TFlashDevice *Device)
// Read <Device> identification
{
byte i;
byte *p;
	
   spiFlashAttach();
   spiFlashByteWrite( FLASH_DEVICE_ID_READ);
   i = sizeof( TFlashDevice);
   p = (byte *)Device;
   do {
	   *p = spiFlashByteRead();
	   p++;
   } while( --i);
   spiFlashRelease();
} // FlashIdentification	

//-----------------------------------------------------------------------------
// Start read
//-----------------------------------------------------------------------------

void FlashBlockReadStart( dword Address)
// Start sequential read from flash array
{
   spiFlashAttach();
   spiFlashByteWrite( FLASH_ARRAY_READ);
   _AddressWrite( Address);
} // FlashBlockReadStart

//-----------------------------------------------------------------------------
// Start write
//-----------------------------------------------------------------------------

void FlashBlockWriteStart( dword Address)
// Start block write
{
   _WriteEnable();
   spiFlashAttach();
   spiFlashByteWrite( FLASH_PROGRAM);
   _AddressWrite( Address);
} // FlashBlockWriteStart

//-----------------------------------------------------------------------------
// Erase
//-----------------------------------------------------------------------------

void FlashBlockErase( dword Address)
// Erase block at <Address>
{
   _WriteEnable();
   spiFlashAttach();
   spiFlashByteWrite( FLASH_BLOCK_ERASE);
   _AddressWrite( Address);
   spiFlashRelease();
} // FlashBlockErase

//-----------------------------------------------------------------------------
// Erase all
//-----------------------------------------------------------------------------

TYesNo FlashEraseAll( void)
// Erase all device
{
   _WriteEnable();
   spiFlashAttach();
   spiFlashByteWrite( FLASH_CHIP_ERASE);
   spiFlashRelease();	
   return( _WaitForReady( FLASH_ERASE_TIMEOUT));
} // FlashEraseAll	

//-----------------------------------------------------------------------------
// Power down
//-----------------------------------------------------------------------------

void FlashPowerDown( void)
// Power down
{
   spiFlashAttach();
   spiFlashByteWrite( FLASH_POWER_DOWN);
   spiFlashRelease();
} // FlashPowerDown

//-----------------------------------------------------------------------------
// Resume
//-----------------------------------------------------------------------------

void FlashPowerDownResume( void)
// Power down resume (wake up)
{
   spiFlashAttach();
   spiFlashByteWrite( FLASH_WAKEUP);
   spiFlashRelease();
   SysUDelay(FLASH_WAKEUP_TIME);
} // FlashPowerDownResume

//******************************************************************************

//-----------------------------------------------------------------------------
// Short status
//-----------------------------------------------------------------------------

static byte _StatusRead( void)
// Read first status byte
{
byte Status;

   spiFlashAttach();
   spiFlashByteWrite( FLASH_STATUS_READ);
   Status = spiFlashByteRead();
   spiFlashRelease();
   return( Status);
} // _StatusRead

//-----------------------------------------------------------------------------
// Write short status
//-----------------------------------------------------------------------------

static void _StatusWrite( byte Status)
// Write first status byte
{
   _WriteEnable();
   spiFlashAttach();
   spiFlashByteWrite( FLASH_STATUS1_WRITE);
   spiFlashByteWrite( Status);
   spiFlashRelease();			
} // _StatusWrite

//-----------------------------------------------------------------------------
// Wait for ready
//-----------------------------------------------------------------------------

TYesNo _WaitForReady( dword Timeout)
// Wait for ready with <Timeout>
{
TTimer Now;
byte   Status;

   Now = SysTimer() + Timeout;
   do {
      Status = _StatusRead();
      if( Status & FLASH_STATUS_EPE){
         return( NO);                  // write/erase error
      }
      if( !(Status & FLASH_STATUS_BSY)){
         return( YES);
      }
   } while( TimeBefore( SysTimer(), Now));
   return( NO);		
} // _WaitForReady

//-----------------------------------------------------------------------------
// Write address
//-----------------------------------------------------------------------------

static void _AddressWrite( dword Address)
// Send <Address> to device
{
   spiFlashByteWrite( (byte)(Address >> 16));
   spiFlashByteWrite( (byte)(Address >> 8));
   spiFlashByteWrite( (byte)(Address >> 0));
} // _AddressWrite

//-----------------------------------------------------------------------------
// Write enable
//-----------------------------------------------------------------------------

static void _WriteEnable( void)
// Write enable command
{
   spiFlashAttach();
   spiFlashByteWrite( FLASH_WRITE_ENABLE);
   spiFlashRelease();
} // _WriteEnable

//-----------------------------------------------------------------------------
// Uprotect sectors
//-----------------------------------------------------------------------------

static void _SectorUnprotectAll( void)
// Unprotect all sectors
{		
	_StatusWrite( FLASH_COMMAND_GLOBAL_UNPROTECT);  // clear SPRL status	
	_StatusWrite( FLASH_COMMAND_GLOBAL_UNPROTECT);  // clear protection registers
} // _SectorUprotectAll
