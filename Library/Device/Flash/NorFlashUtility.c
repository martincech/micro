//*****************************************************************************
//
//    NorFlashUtility.c NOR Flash memory data access utility
//    Version 1.0       (c) VEIT Electronics
//
//*****************************************************************************

#include "NorFlash.h"

#if defined(__AT25DF641__)
   #include "Flash/At25dfxx.h"       // FLASH_WRITE_SIZE
#else
   #error "Unknown NOR Flash chip"
#endif

//-----------------------------------------------------------------------------
//  Save
//-----------------------------------------------------------------------------

TYesNo FlashSave( dword Address, const void *Data, int Size)
// Save <Data> with <Size> at <Address>
{
TYesNo     NewPage;
const byte *p;

   p       = (const byte *)Data;
   NewPage = YES;                           // first page
   while( Size--){
      if( NewPage){
         // New page start
         FlashBlockWriteStart( Address);
         NewPage = NO;
      }
      FlashBlockWriteData( *p);
      p++;
      Address++;
      if( (Address % FLASH_WRITE_SIZE) == 0){
         // New address is at a new page, write finished page
         FlashBlockWritePerform();          // previous page
         if( !FlashWaitForReady()){
            return( NO);
         }
         NewPage = YES;                     // start a new page
      }
   }
   // check for last page :
   if( !NewPage) {
      FlashBlockWritePerform();             // write last page remainder
      if( !FlashWaitForReady()){
         return( NO);
      }
   }
   return( YES);
} // FlashSave

//------------------------------------------------------------------------------
//   Load
//------------------------------------------------------------------------------

void FlashLoad( dword Address, void *Data, int Size)
// Load <Data> with <Size> from <Address>
{
byte *p;

   p = (byte *)Data;
   FlashBlockReadStart( Address);
   do {
      *p = FlashBlockReadData();
      p++;
   } while( --Size);
   FlashBlockReadStop();
} // FlashLoad

//------------------------------------------------------------------------------
//   Fill
//------------------------------------------------------------------------------

TYesNo FlashFill( dword Address, byte Pattern, int Size)
// Write <Pattern> with <Size> at <Address>
{
TYesNo     NewPage;

   NewPage = YES;                           // first page
   while( Size--){
      if( NewPage){
         // New page start
         FlashBlockWriteStart( Address);
         NewPage = NO;
      }
      FlashBlockWriteData( Pattern);
      Address++;
      if( (Address % FLASH_WRITE_SIZE) == 0){
         // New address is at a new page, write finished page
         FlashBlockWritePerform();          // previous page
         if( !FlashWaitForReady()){
            return( NO);
         }
         NewPage = YES;                     // start a new page
      }
   }
   // check for last page :
   if( !NewPage) {
      FlashBlockWritePerform();             // write last page remainder
      if( !FlashWaitForReady()){
         return( NO);
      }
   }
   return( YES);
} // FlashFill

//------------------------------------------------------------------------------
//   Match
//------------------------------------------------------------------------------

TYesNo FlashMatch( dword Address, const void *Data, int Size)
// Compare <Data> with <Size> at <Address>
{
byte *p;

   p = (byte *)Data;
   FlashBlockReadStart( Address);
   do {
      if( *p != FlashBlockReadData()){
         FlashBlockReadStop();
         return( NO);
      }
      p++;
   } while( --Size);
   FlashBlockReadStop();
   return( YES);
} // FlashMatch

//------------------------------------------------------------------------------
//   Sum
//------------------------------------------------------------------------------

dword FlashSum( dword Address, int Size)
// Sum NVM contents at <Address> with <Size>
{
dword Sum;

   Sum = 0;
   FlashBlockReadStart( Address);
   do {
      Sum += FlashBlockReadData();
   } while( --Size);
   FlashBlockReadStop();
   return( Sum);
} // FlashSum
