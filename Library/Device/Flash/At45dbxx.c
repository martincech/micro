//*****************************************************************************
//
//   At45dbxx.c     Flash memory AT45DBxxx
//   Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#include "At45dbxx.h"
#include "System/System.h"         // SysTime
#include "Timer/Timer.h"           // Time interval measuring

// Commands :

#define FLASH_ARRAY_READ      0xE8         // SPI mode 0 or 3
#define FLASH_PAGE_READ       0xD2
#define FLASH_BUFFER1_READ    0xD4
#define FLASH_BUFFER2_READ    0xD6
#define FLASH_STATUS_READ     0xD7

#define FLASH_BUFFER1_WRITE   0x84
#define FLASH_BUFFER2_WRITE   0x87
#define FLASH_BUFFER1_SAVE    0x83         // buffer 1 to main memory page with Erase
#define FLASH_BUFFER2_SAVE    0x86         // buffer 2 to main memory page with Erase
#define FLASH_PAGE_ERASE      0x81

#define FLASH_PAGE_TO_BUFFER1 0x53
#define FLASH_PAGE_TO_BUFFER2 0x55

// Memory addressing :

#if FLASH_PAGE_SIZE == 528
   // by linear address :
   #define MkBufferAddress( a)     ((a) & 0x1FF)
   #define MkPageAddress( a)       ((word)((a) >> 9))

   #define BufferLowAddress( b)    ((b) & 0x00FF)
   #define BufferHighAddress( b)   (((b) >> 8) & 0x01)
   #define PageLowAddress( p)      ((p) << 2)
   #define PageHighAddress( p)     ((p) >> 6)
#elif FLASH_PAGE_SIZE == 264
   // by linear address :
   #define MkBufferAddress( a)     ((a) & 0xFF)
   #define MkPageAddress( a)       ((word)((a) >> 8))

   #define BufferLowAddress( b)    ((b) & 0x00FF)
   #define BufferHighAddress( b)   0
   #define PageLowAddress( p)      ((p) << 1)
   #define PageHighAddress( p)     ((p) >> 7)
#else
   // AT45DB642 - 1056 buffer, 8192 pages
   #error "Unknown FLASH DATA device"
#endif

// status register flags :

#define FLASH_STATUS_RDY   0x80 // ready bit
#define FLASH_STATUS_COMP  0x40 // compare flag
#define FLASH_STATUS_FIXED 0x2C // model/capacity code
#define FLASH_STATUS_MASK  0xFC // valid status bits

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void FlashInit()
// Initialization
{
   spiFlashInit();
} // FlashInit

//-----------------------------------------------------------------------------
// Stav
//-----------------------------------------------------------------------------

byte FlashStatus( void)
// Read status byte
{
byte value;

   spiFlashAttach();                        // SPI mode 0
   spiFlashByteWrite( FLASH_STATUS_READ);
   value = spiFlashByteRead();
   spiFlashRelease();                       // deselect
   return( value);
} // FlashStatus

//-----------------------------------------------------------------------------
// Cekani
//-----------------------------------------------------------------------------

TYesNo FlashWaitForReady( void)
// Wait for ready
{
TTimer Now;

   Now = SysTimer() + FLASH_TIMEOUT;
   do {
      if( FlashStatus() & FLASH_STATUS_RDY){
         return( YES);
      }
   } while( TimeBefore( SysTimer(), Now));
   return( NO);
} // FlashWaitForReady

//-----------------------------------------------------------------------------
// Zapis do bufferu
//-----------------------------------------------------------------------------

void FlashBufferWriteStart( word Offset)
// Start sequential write
{
   spiFlashAttach();                        // SPI mode 0
   spiFlashByteWrite( FLASH_BUFFER1_WRITE);
   spiFlashByteWrite( 0);                   // don't care
   spiFlashByteWrite( BufferHighAddress( Offset));
   spiFlashByteWrite( BufferLowAddress( Offset));
} // FlashBufferWriteStart

//-----------------------------------------------------------------------------
// Ulozeni bufferu
//-----------------------------------------------------------------------------

void FlashBufferSave( word Page)
// Write buffer to flash
{
   spiFlashAttach();                        // SPI mode 0
   spiFlashByteWrite( FLASH_BUFFER1_SAVE);
   spiFlashByteWrite( PageHighAddress( Page));
   spiFlashByteWrite( PageLowAddress( Page));
   spiFlashByteWrite( 0);                   // don't care
   spiFlashRelease();                       // deselect
} // FlashBufferSave

//-----------------------------------------------------------------------------
// Nacteni bufferu
//-----------------------------------------------------------------------------

void FlashBufferLoad( word Page)
// Read buffer from flash
{
   spiFlashAttach();                        // SPI mode 0
   spiFlashByteWrite( FLASH_PAGE_TO_BUFFER1);
   spiFlashByteWrite( PageHighAddress( Page));
   spiFlashByteWrite( PageLowAddress( Page));
   spiFlashByteWrite( 0);                   // don't care
   spiFlashRelease();                       // deselect
} // FlashBufferLoad

//-----------------------------------------------------------------------------
// Cteni z pameti
//-----------------------------------------------------------------------------

void FlashBlockReadStart( word Page, word Offset)
// Start sequential read from flash array
{
   spiFlashAttach();                        // SPI mode 0
   spiFlashByteWrite( FLASH_ARRAY_READ);
   spiFlashByteWrite( PageHighAddress( Page));
   spiFlashByteWrite( PageLowAddress( Page) | BufferHighAddress( Offset));
   spiFlashByteWrite( BufferLowAddress( Offset));
   spiFlashByteWrite( 0);                   // additional don't care
   spiFlashByteWrite( 0);                   // additional don't care
   spiFlashByteWrite( 0);                   // additional don't care
   spiFlashByteWrite( 0);                   // additional don't care
} // FlashPageReadStart

//-----------------------------------------------------------------------------
// Cteni z bufferu
//-----------------------------------------------------------------------------

void FlashBufferReadStart( word Offset)
// Start sequential read from buffer
{
   spiFlashAttach();                        // SPI mode 0
   spiFlashByteWrite( FLASH_BUFFER1_READ);
   spiFlashByteWrite( 0);                   // don't care
   spiFlashByteWrite( BufferHighAddress( Offset));
   spiFlashByteWrite( BufferLowAddress(  Offset));
   spiFlashByteWrite( 0);                   // additional don't care
} // FlashBufferReadStart
