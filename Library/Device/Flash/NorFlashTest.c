//*****************************************************************************
//
//    NorFlashTest.c NOR flash memory test
//    Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#include "NorFlashTest.h"
#include "Console/conio.h"        // Console output

#define MAGIC_OFFSET     2
#define FLASH_TEST_SIZE  FLASH_SIZE

#if defined(__AT25DF641__)
   #include "Flash/At25dfxx.h"       // FLASH_WRITE_SIZE
#else
   #error "Unknown NOR Flash chip"
#endif   

//-----------------------------------------------------------------------------
// Signature test
//-----------------------------------------------------------------------------

TYesNo NorFlashSignature( void)
// Check for device signature
{
TFlashDevice Device;

   FlashIdentification( &Device);
   if( Device.Manufacturer != FLASH_MANUFACTURER){
      cprintf( "\nFLASH error manufacturer\n");
      return( NO);
   }
   if( Device.DeviceId1 != FLASH_DEVICE_ID1){
      cprintf( "\nFLASH error Device ID1\n");
      return( NO);
   }
   if( Device.DeviceId2 != FLASH_DEVICE_ID2){
      cprintf( "\nFLASH error Device ID2\n");
      return( NO);
   }
   if( Device.ExtendedLenght != FLASH_EXTENDED_LENGTH){
      cprintf( "\nFLASH error extended info\n");
      return( NO);
   }
   return( YES);
} // NorFlashSignature

//-----------------------------------------------------------------------------
// Blank test
//-----------------------------------------------------------------------------

TYesNo NorFlashBlankCheck( void)
// Erase & check
{
dword i, j;
byte  Value;

   if( !FlashEraseAll()){
      cprintf( "\nFLASH erase timeout\n");
	   return( NO);
   }
   // read pattern :
   for( i = 0; i < FLASH_TEST_SIZE; i += FLASH_WRITE_SIZE){
      FlashBlockReadStart( i);
      for( j = 0; j < FLASH_WRITE_SIZE; j++){
         Value = FlashBlockReadData();
         if( Value != 0xFF){
            FlashBlockReadStop();
            cprintf( "\nFLASH error @%04x : %02x\n", i, Value);
            return( NO);
         }
      }
      FlashBlockReadStop();
   }
   return( YES);
} // NorFlashBlankCheck

//-----------------------------------------------------------------------------
// Page test
//-----------------------------------------------------------------------------

TYesNo NorFlashPageTest( void)
// Run memory page test
{
   if( !FlashEraseAll()){
      cprintf( "\nFLASH erase timeout\n");
      return( NO);
   }
   if(!NorFlashPageTestWrite()) {
      return NO;
   }
   if(!NotFlashPageTestCheck()) {
      return NO;
   }
   return YES;
} // NorFlashPageTest

//-----------------------------------------------------------------------------
// Page test write
//-----------------------------------------------------------------------------

TYesNo NorFlashPageTestWrite( void)
// Run memory page test write
{
dword i, j;
byte  Value;
   // write pattern :
   for( i = 0; i < FLASH_TEST_SIZE; i += FLASH_WRITE_SIZE){
      FlashBlockWriteStart( i);
      for( j = 0; j < FLASH_WRITE_SIZE; j++){
         FlashBlockWriteData( (i + MAGIC_OFFSET) & 0xFF);
      }
      FlashBlockWritePerform();
      if( !FlashWaitForReady()){
         cprintf( "\nFLASH timeout\n");
         return( NO);
      }
   }
} // NorFlashPageTestWrite

//-----------------------------------------------------------------------------
// Page test check
//-----------------------------------------------------------------------------

TYesNo NorFlashPageTestCheck( void)
// Run memory page test check
{
dword i, j;
byte  Value;
   // read pattern :
   for( i = 0; i < FLASH_TEST_SIZE; i += FLASH_WRITE_SIZE){
      FlashBlockReadStart( i);
      for( j = 0; j < FLASH_WRITE_SIZE; j++){
         Value = FlashBlockReadData();
         if( Value != ((i + MAGIC_OFFSET) & 0xFF)){
            FlashBlockReadStop();
            cprintf( "\nFLASH error @%04x : %02x\n", i, Value);
            return( NO);
         }
      }
      FlashBlockReadStop();
   }
   return( YES);
} // NotFlashPageTestCheck

//-----------------------------------------------------------------------------
// Pattern test
//-----------------------------------------------------------------------------

TYesNo NorFlashPatternTest( byte Pattern)
// Run memory pattern test
{
dword i, j;
byte  Value;

   if( !FlashEraseAll()){
      cprintf( "\nFLASH erase timeout\n");
      return( NO);
   }
   // write pattern :
   FlashFill( 0, Pattern, FLASH_TEST_SIZE);
   // read pattern :
   for( i = 0; i < FLASH_TEST_SIZE; i += FLASH_WRITE_SIZE){
      FlashBlockReadStart( i);
      for( j = 0; j < FLASH_WRITE_SIZE; j++){
         Value = FlashBlockReadData();
         if( Value != Pattern){
            FlashBlockReadStop();
            cprintf( "\nFLASH error @%04x : %02x\n", i, Value);
            return( NO);
         }
      }
      FlashBlockReadStop();
   }
   return( YES);
} // NorFlashPatternTest
