//******************************************************************************
//
//   Efs.h           External File System
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Efs_H__
   #define __Efs_H__

#ifndef __Uni_H_
   #include "Unisys/Uni.h"
#endif

#ifndef __EfsDef_H_
   #include "File/EfsDef.h"
#endif

// Constants :
#define EFS_NAME_MAX       31     // File or directory name max. length
#define EFS_PATH_MAX       255    // File path max. length
#define EFS_DATA_SIZE_MAX  512    // Data max length

// Seek mode definitions :
typedef enum {
   EFS_SEEK_START,
   EFS_SEEK_END,
   EFS_SEEK_CURRENT
} EEfsSeekMode;

typedef enum {
   EFS_ERROR_OK,
   EFS_ERROR_NO_DRIVE,
   EFS_ERROR_INIT_DRIVE,
   EFS_ERROR_MOUNT,
   EFS_ERROR_UNKNOWN
} EEfsError;

//------------------------------------------------------------------------------
//  Simulator
//------------------------------------------------------------------------------

#ifdef __WIN32__
   void EfsRoot(const char *Directory);
   // Setup external filesystem root to <Directory>
#endif

#ifdef __cplusplus
   extern "C" {
#endif

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

TYesNo EfsInit( void);
// Initialisation

TYesNo EfsFormat( void);
// Format

TYesNo EfsClockSet( dword Clock);
// Synchronize filesystem with <Clock>

TYesNo EfsDirectoryExists( const char *Path);
// Check for directory <Name>

TYesNo EfsDirectoryCreate( const char *Path);
// Create new directory with <Name>

TYesNo EfsDirectoryChange( const char *Path);
// Change current directory to <Name>

TYesNo EfsDirectoryDelete( const char *Path);
// Delete directory <Name>

TYesNo EfsDirectoryRename( const char *Path, const char *NewName);
// Renames selected directory

TYesNo EfsFileExists( const char *Name);
// Check for file <Name>

TYesNo EfsFileCreate( TEfsFile *File, const char *FileName, dword Size);
// Create <FileName>

TYesNo EfsFileOpen( TEfsFile *File, const char *FileName);
// Open <FileName> for read/write

TYesNo EfsFileDelete( const char *FileName);
// Delete <FileName>

dword EfsFileSize( TEfsFile *File);

void EfsFileClose(  TEfsFile *File);
// Close opened file

word EfsFileWrite(  TEfsFile *File, const void *Data, word Count);
// Write <Data> with <Count> bytes, returns number of bytes written

word EfsFileRead(  TEfsFile *File, void *Data, word Count);
// Read <Data> with <Count> bytes, returns number of bytes read

TYesNo EfsFileSeek( TEfsFile *File, int32 Pos, EEfsSeekMode Whence);
// Seek at <Pos> starting from <Whence>

void EfsSwitchOff( void);
// Safely unmount drive

//------------------------------------------------------------------------------
#ifdef __cplusplus
   }
#endif

#endif
