//*****************************************************************************
//
//    Rcs.h        Remote control server
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Rcs_H__
   #define __Rcs_H__

void RcsInit( void);
// Initialize

void RcsExecute( void);
// Server executive
   
#endif
