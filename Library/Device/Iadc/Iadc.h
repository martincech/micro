//*****************************************************************************
//
//   Iadc.h      Internal A/D convertor functions
//   Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __IAdc_H__
   #define __IAdc_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void IAdcInit( void);
// Initialize convertor

word IAdcRead( byte Channel);
// Returns measured <Channel> value

#endif
