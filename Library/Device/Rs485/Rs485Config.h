//******************************************************************************
//
//   Rs485Config.h     RS485 module configuration
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Rs485Config_H__
#define __Rs485Config_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Rs485ConfigDef_H__
   #include "Rs485ConfigDef.h"
#endif

#define RS485_INTERFACE_COUNT 2

extern       TRs485Options Rs485Options[RS485_INTERFACE_COUNT];
extern const TRs485Options Rs485OptionsDefault[RS485_INTERFACE_COUNT];


#endif // __Rs485Config_H__


