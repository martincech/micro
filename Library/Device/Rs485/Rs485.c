//******************************************************************************
//
//   Rs485.c     Rs485 service routines
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "Rs485.h"
#include "Rs485Config.h"
#include "mb.h"
#include "Modbus/ModbusConfig.h"
#include "Megavi/MegaviTask.h"
#include "Dacs/DacsTask.h"

static TYesNo SomeInterfaceAsSensorPack = NO;
static byte PhysicalDevice(byte i);
     
void Rs485InitOne(byte i)
{
   switch( Rs485Options[i].Mode){
      case RS485_MODE_SENSOR_PACK :
      case RS485_MODE_MODBUS :
         eMBInit(ModbusOptions.Mode, ModbusOptions.Address, PhysicalDevice(i), ModbusOptions.BaudRate, ModbusOptions.Parity);
         eMBEnable();
         break;
      case RS485_MODE_MEGAVI :
         MegaviTaskInit( PhysicalDevice(i));
         break;

      case RS485_MODE_DACS :
         DacsTaskInit( PhysicalDevice(i));
         break;
   }
   if(Rs485Options[i].Mode == RS485_MODE_SENSOR_PACK){
      SomeInterfaceAsSensorPack = YES;
   }
}

void Rs485Init()
{
byte i;

   for( i = 0; i < RS485_INTERFACE_COUNT; i++){
      if( Rs485Options[i].Enabled){
         Rs485InitOne(i);
      }
   }
}

void Rs485Execute()
{
byte i;

   for( i = 0; i < RS485_INTERFACE_COUNT; i++){
      if( Rs485Options[i].Enabled){
         switch( Rs485Options[i].Mode){
            case RS485_MODE_SENSOR_PACK :
            case RS485_MODE_MODBUS :
               eMBPoll();
               break;

            case RS485_MODE_MEGAVI :
               MegaviTaskExecute();
               break;

            case RS485_MODE_DACS :
               DacsTaskExecute();
               break;
         }
      }
   }
}

void Rs485Free()
{
byte i;

   for( i = 0; i < RS485_INTERFACE_COUNT; i++){
      if( Rs485Options[i].Enabled){
         Rs485FreeOne(i);
      }
   }
}

void Rs485FreeOne(byte i)
{
   switch( Rs485Options[i].Mode){
      case RS485_MODE_SENSOR_PACK :
         SomeInterfaceAsSensorPack = NO;
      case RS485_MODE_MODBUS :
         eMBDisable();
         eMBClose();
         break;

      case RS485_MODE_MEGAVI :
         MegaviTaskFree();
         break;

      case RS485_MODE_DACS :
         DacsTaskFree();
         break;
   }
}

TYesNo Rs485ConfiguredAsSensorPack()
{
   return SomeInterfaceAsSensorPack;
}

static byte PhysicalDevice(byte i)
{
   if(i == 1){
      return RS485_1_PHYSICAL_DEVICE;
   }
   return RS485_0_PHYSICAL_DEVICE;
}
