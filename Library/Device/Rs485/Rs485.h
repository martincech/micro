//******************************************************************************
//
//   Rs485.h     Rs485 service routines
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Rs485_H__
#define __Rs485_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void Rs485InitOne(byte i);
void Rs485Init();
void Rs485Execute();
void Rs485Free();
void Rs485FreeOne(byte i);

TYesNo Rs485ConfiguredAsSensorPack();
// return whether some of RS485 interfaces is configured for sensor pack

#endif // __Rs485_H__


