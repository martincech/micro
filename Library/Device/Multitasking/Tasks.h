//******************************************************************************
//
//   Tasks.h       Bat2 Tasks
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Tasks_H__
   #define __Tasks_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

void TasksInit( void);
// Init tasks

void TasksRun( void);
// Run tasks

void TasksPause( void);
// Pause tasks

void TasksStop( void);
// Stop tasks

void TasksTimer( void);
// Timer, call on K_TIMER_SLOW

TYesNo TasksIdle( void);
// Checks for all tasks idle

void TasksEvent( int Event);
// do task events

#endif
