//*****************************************************************************
//
//    Multitasking.c     Multitasking
//    Version 1.0        (c) Veit Electronics
//
//*****************************************************************************

#include "Multitasking.h"
#include "System/System.h"
#include "Cpu/Cpu.h"
#include "Hardware.h"
#include <ctl.h>
#include <string.h>

static CTL_TASK_t MainTask;
#define TASK_PRIORITY         255

//-----------------------------------------------------------------------------
// Init
//-----------------------------------------------------------------------------

void MultitaskingInit( void)
// Initialization
{
   ctl_task_init(&MainTask, TASK_PRIORITY, "M");
} // MultitaskingInit

//-----------------------------------------------------------------------------
// Task run
//-----------------------------------------------------------------------------

void MultitaskingTaskRun( TTask *Task)
// Start task
{
   memset(Task->Stack, 0xCC, Task->StackSize);
   Task->Event = 0;
   ctl_task_run( &Task->Task, TASK_PRIORITY, (void (*)(void *))Task->EntryPoint, Task, "W", Task->StackSize / 4, Task->Stack, 32);
   Task->Idle = NO;
} // MultitaskingTaskRun

//-----------------------------------------------------------------------------
// Task exit
//-----------------------------------------------------------------------------

void MultitaskingTaskExit( TTask *Task)
// Terminates task, waits for task exit
{
   MultitaskingEventSet( Task, K_SHUTDOWN);

   while(Task->Task.state != CTL_STATE_SUSPENDED) {
      MultitaskingReschedule();
   }

   Task->Idle = YES;
} // MultitaskingTaskExit

//-----------------------------------------------------------------------------
// Event set
//-----------------------------------------------------------------------------

void MultitaskingEventSet( TTask *Task, int Event)
// Set event 0 - 31
{
   Task->Idle = NO;
   if(Event > 31) {
      Task->EventLong = Event;
   } else {
      Task->Event |= 1 << Event;
   }
} // MultitaskingTaskEventSet

//-----------------------------------------------------------------------------
// Task idle
//-----------------------------------------------------------------------------

TYesNo MultitaskingTaskIdle( TTask *Task)
// Checks for task idle
{
   return Task->Idle;
} // MultitaskingTaskIdle

//-----------------------------------------------------------------------------
// Task scheduler
//-----------------------------------------------------------------------------

int MultitaskingTaskScheduler( TTask *Task)
// Task scheduler
{
int Key;
   
   if(!Task->EventLong && !Task->Event) {
      Task->Idle = YES;
      return K_IDLE;
   }

   if(Task->EventLong) {
      Key = Task->EventLong;
      Task->EventLong = 0;
      return Key;
   }

   Key = 0;
   forever {
      if(Task->Event & (1 << Key)) {
         Task->Event &= ~(1 << Key);
         return Key;
      }
      Key++;
   }
} // MultitaskingTaskScheduler

//-----------------------------------------------------------------------------
// Reschedule
//-----------------------------------------------------------------------------
#include "Memory/Nvm.h"

void MultitaskingReschedule( void)
// Reschedule tasks
{
   //#warning Weighing watchdog
   /*if(InWeighingTask && TimeAfter(SysTime(), WatchdogExpiration)) {
      GClear();
      GTextAt(0, 0);
      cprintf("Watchdog");
      GFlush();
      forever;
   }*/

   /*if(StackGui[0] != STACK_MARKER || StackGui[1 + STACK_SIZE_GUI] != STACK_MARKER) {
      forever;
   }
   if(StackWeighing[0] != STACK_MARKER || StackWeighing[1 + STACK_SIZE_WEIGHING] != STACK_MARKER) {
      forever;
   }
   if(StackSms[0] != STACK_MARKER || StackSms[1 + STACK_SIZE_SMS] != STACK_MARKER) {
      forever;
   }*/
   ctl_task_reschedule();
} // MultitaskingReschedule

//-----------------------------------------------------------------------------
// Mutex
//-----------------------------------------------------------------------------

void MultitaskingMutexInit(TMutex *Mutex)
// Init mutex
{
   MultitaskingMutexRelease(Mutex);
} // MultitaskingMutexInit

static TYesNo MultitaskingMutexLock( TMutex *Mutex, TYesNo Block)
{
   forever {
      InterruptConditionallyDisable();
      if(!*Mutex) {
         *Mutex = 1;
         InterruptConditionallyEnable();
         break;
      }
      InterruptConditionallyEnable();
      if( !Block){
         return NO;
      }
      MultitaskingReschedule();
   }
   return YES;
}

void MultitaskingMutexSet( TMutex *Mutex)
// Set mutex
{
   MultitaskingMutexLock( Mutex, YES);
} // MultitaskingMutexSet

TYesNo MultitaskingMutexTrySet( TMutex *Mutex)
// try set mutex return immediatelly with lock status
{
   return MultitaskingMutexLock( Mutex, NO);
}

void MultitaskingMutexRelease( TMutex *Mutex)
// Release mutex
{
   *Mutex = 0;
} // MultitaskingMutexRelease

//******************************************************************************

void ctl_handle_error(CTL_ERROR_CODE_t e)
{
   forever;
} // ctl_handle_error