#include "Hardware.h"
#include "Cpu/Cpu.h"

#define __MK_xxx_H__
#define _MK_xxx_H_

#define LITTLE_ENDIAN

// asm keyword doesn't work in CrossStudio
#define asm          __asm__ __volatile__