//*****************************************************************************
//
//    wdt_kinetis.h     Freescale USB stack watchdog
//    Version 1.0       (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __wdt_kinetis_H__
   #define __wdt_kinetis_H__

#include "Cpu/Cpu.h"

#define Watchdog_Reset()      WatchDog()

#endif