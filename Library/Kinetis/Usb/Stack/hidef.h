//*****************************************************************************
//
//    hidef.h      Freescale USB stack hidef
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __hidef_H__
   #define __hidef_H__

#include "Cpu/Cpu.h"

#define EnableInterrupts InterruptEnable()
#define DisableInterrupts InterruptDisable()

#endif