#include "Unisys/Uni.h"
#include "Multitasking/Multitasking.h"
#include "System/System.h"

void _usb_khci_task(void);

void Poll( void) {
   MultitaskingReschedule();
   _usb_khci_task();
}

void time_delay(dword delay) 
{

   volatile dword i;
   
   for(i = 0 ; i < delay / 2 + 1 ; i++) {
      SysUDelay(400);
      MultitaskingReschedule();
   }

    /*
    volatile dword u32Delay;
    volatile dword u32Delay2;
    for(u32Delay=0; u32Delay<delay; u32Delay++)
        for(u32Delay2=0; u32Delay2<(0x1111/1); u32Delay2++);*/
}

dword _bsp_usb_host_init(void *param){
   return 0;
}