//*****************************************************************************
//
//    Descriptor.h    USB descriptor definitions
//    Version 1.0    (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Descriptor_H__
   #define __Descriptor_H__

#ifndef __Uni_H_
   #include "Unisys/Uni.h"
#endif

// Descriptor codes
#define USB_DESCRIPTOR_DEVICE            0x01
#define USB_DESCRIPTOR_CONFIGURATION     0x02
#define USB_DESCRIPTOR_STRING            0x03
#define USB_DESCRIPTOR_INTERFACE         0x04
#define USB_DESCRIPTOR_ENDPOINT          0x05

// Class specific descriptor codes
#define USB_DESCRIPTOR_HID	              0x21
#define USB_DESCRIPTOR_REPORT            0x22

// Device descriptor
typedef struct {
   uint8_t bLength;
   uint8_t bDescriptorType;
   uint16_t bcdUSB;
   uint8_t bDeviceClass;
   uint8_t bDeviceSubClass;
   uint8_t bDeviceProtocol;
   uint8_t bMaxPacketSize0;
   uint16_t idVendor;
   uint16_t idProduct;
   uint16_t bcdDevice;
   uint8_t iManufacturer;
   uint8_t iProduct;
   uint8_t iSerialNumber;
   uint8_t bNumConfigurations;
} __packed TUsbDeviceDescriptor;

// Configuration descriptor
typedef struct {
   uint8_t bLength;
   uint8_t bDescriptorType;
   uint16_t wTotalLength;
   uint8_t bNumInterfaces;
   uint8_t bConfigurationValue;
   uint8_t iConfiguration;
   uint8_t bmAttributes;
   uint8_t MaxPower;
} __packed TUsbConfigurationDescriptor;

// Interface descriptor
typedef struct {
   uint8_t bLength;
   uint8_t bDescriptorType;
   uint8_t bInterfaceNumber;
   uint8_t bAlternateSetting;
   uint8_t bNumEndpoints;
   uint8_t bInterfaceClass;
   uint8_t bInterfaceSubClass;
   uint8_t bInterfaceProtocol;
   uint8_t iInterface;
} __packed TUsbInterfaceDescriptor;

// Endpoint descriptor
typedef struct {
  uint8_t bLength;
  uint8_t bDescriptorType;
  uint8_t bEndpointAddress;
  uint8_t bmAttributes;
  uint16_t wMaxPacketSize;
  uint8_t bInterval;
} __packed TUsbEndpointDescriptor;

// HID descriptor
typedef struct {
  uint8_t  bLength;
  uint8_t  bDescriptorType;
  uint16_t wHIDClassSpecComp;
  uint8_t  bCountry;
  uint8_t  bNumDescriptors;
  uint8_t  b1stDescType;
  uint16_t w1stDescLength;
} __packed TUsbHidDescriptor;

typedef struct {
   uint8_t bLength;
   uint8_t bDescriptorType;
   uint8_t bString[];
} __packed TUsbStringDescriptor;

#define UsbStringDescriptorSize( StringSize)   (StringSize - 1 + sizeof(TUsbStringDescriptor))

#endif

