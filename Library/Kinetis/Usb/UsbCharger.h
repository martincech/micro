//*****************************************************************************
//
//    UsbCharger.h  Usb charger detection module
//    Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#ifndef __UsbCharger_H__
   #define __UsbCharger_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

typedef enum {
   CHARGER_UNKNOWN,
   CHARGER_STANDARD_HOST,
   CHARGER_CHARGING_PORT,
   CHARGER_DEDICATED
} EChargerType;

typedef byte TChargerType; 

void UsbChargerDetectionStart( void);
// Start sequence

TYesNo UsbChargerDetectionCompleted( void);
// Poll sequence

TChargerType UsbChargerCharger( void);
// Get sequence result

#endif
