//*****************************************************************************
//
//    port.h             Modbus port to Kinetis CortexMx target
//    Version 1.0        (c) Veit Electronics
//
//*****************************************************************************

#ifndef _PORT_H
   #define _PORT_H

/* ----------------------- Platform includes --------------------------------*/
#include "Uart/Uart.h"
#include "Cpu/Cpu.h"
#include "Unisys/Uni.h"

static void _UartModbusModeSet( TUartAddress Uart, TUartMode Mode);
// Set Modbus mode

static void __irq _Usart0ModbusHandler( void);
// USART0 Modbus handler

static void __irq _Usart1ModbusHandler( void);
// USART1 Modbus handler

static void __irq _Usart2ModbusHandler( void);
// USART2 Modbus handler

static void __irq _Usart3ModbusHandler( void);
// USART3 Modbus handler

static void __irq _Usart4ModbusHandler( void);
// USART4 Modbus handler

static void __irq _Usart5ModbusHandler( void);
// USART5 Modbus handler

static void __irq _Usart6ModbusHandler( void);
// USART6 Modbus handler

static void __irq _Usart7ModbusHandler( void);
// USART7 Modbus handler

static void __irq _Usart8ModbusHandler( void);
// USART8 Modbus handler

static void __irq _Usart9ModbusHandler( void);
// USART9 Modbus handler

static void _UsartModbusHandler( TUartAddress Address);
// Common Modbus handler

/* ----------------------- Defines ------------------------------------------*/
#define	INLINE                      inline
#define PR_BEGIN_EXTERN_C           extern "C" {
#define	PR_END_EXTERN_C             }

#define ENTER_CRITICAL_SECTION( )   vMBPortEnterCritical()
#define EXIT_CRITICAL_SECTION( )    vMBPortExitCritical()

typedef TYesNoEnum BOOL;

typedef byte UCHAR;
typedef char CHAR;

typedef uint16 USHORT;
typedef int16   SHORT;

typedef uint32 ULONG;
typedef int32    LONG;

#ifndef TRUE
#define TRUE                                    YES
#endif

#ifndef FALSE
#define FALSE                                   NO
#endif

#define MB_PORT_HAS_CLOSE	                1
#define MB_ASCII_TIMEOUT_WAIT_BEFORE_SEND_MS    2

#define assert(x) 
/* ----------------------- Prototypes ---------------------------------------*/
void            vMBPortEnterCritical( void );
void            vMBPortExitCritical( void );

#endif
