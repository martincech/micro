/* ----------------------- Platform includes --------------------------------*/
#include <stdlib.h>
#include "port.h"
#include "Uart/Uart.h"
#include "Hardware.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"

static TUartAddress uartAddress;

void vMBPortSerialEnable( BOOL xRxEnable, BOOL xTxEnable )
{
TUartDescriptor       *Uart;
   
    if(!xRxEnable && !xTxEnable){
      UartStop( uartAddress);
      return;
    }
    Uart = UartGet( uartAddress);

    if( xRxEnable )
    {
      if( Uart->Data->Status != UART_SEND_ACTIVE){
         UartReceive(uartAddress);
      }
    } else {
      usartRxInterruptDisable(Uart->Usart);
      usartRxDisable(Uart->Usart);
    }

    if( xTxEnable )
    {
      _UartSendStart(uartAddress, 0);
    } else {
      usartTxInterruptDisable( Uart->Usart);     // stop Tx
      if( Uart->Data->Status == UART_SEND_ACTIVE){
         usartTxDoneInterruptEnable( Uart->Usart);  // wait for Tx done
      }else{
         usartTxDisable(Uart->Usart);
      }
    }
}

BOOL xMBPortSerialInit( UCHAR ucPORT, ULONG ulBaudRate, UCHAR ucDataBits, eMBParity eParity )
{
EUartFormat UartFormat;

   UartInit(ucPORT);
   UartModeSet(ucPORT, UART_MODE_MODBUS);
   switch( eParity){
      case (MB_PAR_NONE):
         UartFormat = UART_7BIT_MARK;
         break;
      case (MB_PAR_ODD):
         UartFormat = UART_7BIT_ODD;
         break;
      case (MB_PAR_EVEN):
         UartFormat = UART_7BIT_EVEN;
         break;
   }
   if( ucDataBits == 8){
       UartFormat += UART_8BIT;
   }
   UartSetup( ucPORT, ulBaudRate, UartFormat);
   return TRUE;
}

void vMBPortSerialClose( void )
{
   UartStop(uartAddress);
   UartDeinit(uartAddress);
}

BOOL xMBPortSerialPutByte( CHAR ucByte )
{
    usartTxData( UsartGet(uartAddress), ucByte);
    return TRUE;
}

BOOL xMBPortSerialGetByte( CHAR * pucByte )
{
TUsart      *Usart;

   *pucByte = usartRxData( UsartGet(uartAddress));
   Usart = UsartGet(uartAddress);
   if( Usart->C1 & UART_C1_PE_MASK && !(Usart->C1 & UART_C1_M_MASK)){
      // 8th bit is parity
      *pucByte &= ~0x80;
   }
    return TRUE;
}


//------------------------------------------------------------------------------
//   Mode
//------------------------------------------------------------------------------

static void _UartModbusModeSet( TUartAddress Address, TUartMode Mode)
// Set Modbus mode
{
   usartInterruptDisable( UsartGet(Address));
   InterruptDisable();
   switch( Address){
#if UART_PORTS_COUNT >= 1
      case UART_UART0 :
         CpuIrqAttach( UART0_RX_TX_IRQn, UART0_PRIORITY, _Usart0ModbusHandler);
         CpuIrqAttach( UART0_ERR_IRQn,   UART0_PRIORITY, _Usart0ModbusHandler);
         CpuIrqEnable( UART0_RX_TX_IRQn);
         CpuIrqEnable( UART0_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 2
      case UART_UART1 :
         CpuIrqAttach( UART1_RX_TX_IRQn, UART1_PRIORITY, _Usart1ModbusHandler);
         CpuIrqAttach( UART1_ERR_IRQn,   UART1_PRIORITY, _Usart1ModbusHandler);
         CpuIrqEnable( UART1_RX_TX_IRQn);
         CpuIrqEnable( UART1_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 3
      case UART_UART2 :
         CpuIrqAttach( UART2_RX_TX_IRQn, UART2_PRIORITY, _Usart2ModbusHandler);
         CpuIrqAttach( UART2_ERR_IRQn,   UART2_PRIORITY, _Usart2ModbusHandler);
         CpuIrqEnable( UART2_RX_TX_IRQn);
         CpuIrqEnable( UART2_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 4
      case UART_UART3 :
         CpuIrqAttach( UART3_RX_TX_IRQn, UART3_PRIORITY, _Usart3ModbusHandler);
         CpuIrqAttach( UART3_ERR_IRQn,   UART3_PRIORITY, _Usart3ModbusHandler);
         CpuIrqEnable( UART3_RX_TX_IRQn);
         CpuIrqEnable( UART3_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 5
      case UART_UART4 :
         CpuIrqAttach( UART4_RX_TX_IRQn, UART4_PRIORITY, _Usart4ModbusHandler);
         CpuIrqAttach( UART4_ERR_IRQn,   UART4_PRIORITY, _Usart4ModbusHandler);
         CpuIrqEnable( UART4_RX_TX_IRQn);
         CpuIrqEnable( UART4_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 6
      case UART_UART5 :
         CpuIrqAttach( UART5_RX_TX_IRQn, UART5_PRIORITY, _Usart5ModbusHandler);
         CpuIrqAttach( UART5_ERR_IRQn,   UART5_PRIORITY, _Usart5ModbusHandler);
         CpuIrqEnable( UART5_RX_TX_IRQn);
         CpuIrqEnable( UART5_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 7
      case UART_UART6 :
         CpuIrqAttach( UART6_RX_TX_IRQn, UART6_PRIORITY, _Usart6ModbusHandler);
         CpuIrqAttach( UART6_ERR_IRQn,   UART6_PRIORITY, _Usart6ModbusHandler);
         CpuIrqEnable( UART6_RX_TX_IRQn);
         CpuIrqEnable( UART6_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 8
      case UART_UART7 :
         CpuIrqAttach( UART7_RX_TX_IRQn, UART7_PRIORITY, _Usart7ModbusHandler);
         CpuIrqAttach( UART7_ERR_IRQn,   UART7_PRIORITY, _Usart7ModbusHandler);
         CpuIrqEnable( UART7_RX_TX_IRQn);
         CpuIrqEnable( UART7_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 9
      case UART_UART8 :
         CpuIrqAttach( UART8_RX_TX_IRQn, UART8_PRIORITY, _Usart8ModbusHandler);
         CpuIrqAttach( UART8_ERR_IRQn,   UART8_PRIORITY, _Usart8ModbusHandler);
         CpuIrqEnable( UART8_RX_TX_IRQn);
         CpuIrqEnable( UART8_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 10
      case UART_UART9 :
         CpuIrqAttach( UART9_RX_TX_IRQn, UART9_PRIORITY, _Usart9ModbusHandler);
         CpuIrqAttach( UART9_ERR_IRQn,   UART9_PRIORITY, _Usart9ModbusHandler);
         CpuIrqEnable( UART9_RX_TX_IRQn);
         CpuIrqEnable( UART9_ERR_IRQn);
         break;
#endif
      default :
         break;
   }
   InterruptEnable();
} // _UartModbusModeSet

//------------------------------------------------------------------------------
//   IRQ Handler
//------------------------------------------------------------------------------

static void __irq _Usart0ModbusHandler( void)
// USART0 Modbus handler
{
   _UsartModbusHandler( UART_UART0);
} // _Usart0ModbusHandler

static void __irq _Usart1ModbusHandler( void)
// USART1 Modbus handler
{
   _UsartModbusHandler( UART_UART1);
} // _Usart1ModbusHandler

static void __irq _Usart2ModbusHandler( void)
// USART2 Modbus handler
{
   _UsartModbusHandler( UART_UART2);
} // _Usart2ModbusHandler

static void __irq _Usart3ModbusHandler( void)
// USART3 Modbus handler
{
   _UsartModbusHandler( UART_UART3);
} // _Usart3ModbusHandler

static void __irq _Usart4ModbusHandler( void)
// USART4 Modbus handler
{
   _UsartModbusHandler( UART_UART4);
} // _Usart4ModbusHandler

static void __irq _Usart5ModbusHandler( void)
// USART5 Modbus handler
{
   _UsartModbusHandler( UART_UART5);
} // _Usart5ModbusHandler

static void __irq _Usart6ModbusHandler( void)
// USART6 Modbus handler
{
   _UsartModbusHandler( UART_UART6);
} // _Usar

static void __irq _Usart7ModbusHandler( void)
// USART7 Modbus handler
{
   _UsartModbusHandler( UART_UART7);
} // _Usar

static void __irq _Usart8ModbusHandler( void)
// USART8 Modbus handler
{
   _UsartModbusHandler( UART_UART8);
} // _Usar

static void __irq _Usart9ModbusHandler( void)
// USART9 Modbus handler
{
   _UsartModbusHandler( UART_UART9);
} // _Usar

//------------------------------------------------------------------------------
//   Common Modbus Handler
//------------------------------------------------------------------------------

static void _UsartModbusHandler( TUartAddress Address)
// Common Modbus handler
{

TUartData   *UData;
TUsart      *Usart;
TUartDescriptor       *Uart;
byte        Data;
unsigned    Status;

   Uart = UartGet( Address);
   uartAddress = Address;   
   Usart = UsartGet(Address);
   Status = usartStatus( Usart);
//------------------------------------------------------------------------------
   // check for Rx errors :
   if( usartStatusRxError( Status)){
      usartStatusReset( Usart);
      return;
   }

   if( usartStatusRx( Status)){
      pxMBFrameCBByteReceived();
   }

   if( usartStatusTx( Status))
   {
      pxMBFrameCBTransmitterEmpty();
   }

   if( usartStatusTxDone( Status))
   {
      Uart->Data->Status = UART_SEND_DONE;
      UartTxDisable( Uart);                   // RS485 direction to Rx
      usartTxDoneInterruptDisable( Usart);
      usartTxDisable( Usart);
      UartReceive(uartAddress);
   }
}