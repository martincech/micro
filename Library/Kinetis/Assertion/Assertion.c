//******************************************************************************
//
//   Assertion.c  Bat2 assert debug macro implementation
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************
#include <assert.h>
#include <string.h>
#include "Console/conio.h"                // Console
#include "Gadget/DLabel.h"                // Labels
#include "Gadget/DLayout.h"
#include "Hardware.h"

#define EXPRESSION_Y_POS DLAYOUT_TITLE_H + 1
#define FILE_Y_POS       (EXPRESSION_Y_POS + 3*GCharHeight())
#define FILENAME_Y_POS   (FILE_Y_POS + 15)
#define LINE_Y_POS       (G_HEIGHT - GCharHeight())

void __assert(const char *__expression, const char *__filename, int __line)
{
const char *CPtr;
int CurWidth;

   GClear();
   DLayoutTitle( "!!Assertion failed!!");
   // print expression
   CPtr = __expression;
   GTextAt(0, EXPRESSION_Y_POS);
   CurWidth = 0;
   while(*CPtr != '\0'){
      CurWidth += GLetterWidth( *CPtr);
      if(CurWidth > G_WIDTH){
         cputch('\n');
         CurWidth = 0;
      }
      cputch(*CPtr);
      CPtr++;
   }
   // print filename
   DLabel( "File: ", 0, FILE_Y_POS);
   CPtr = __filename;
   GTextAt(0, FILENAME_Y_POS);
   CurWidth = 0;
   while(*CPtr != '\0'){
      CurWidth += GLetterWidth( *CPtr);
      if(CurWidth > G_WIDTH){
         cputch('\n');
         CurWidth = 0;
      }
      cputch(*CPtr);
      CPtr++;
   }

   DLabel( "Line: ", 0, LINE_Y_POS);
#ifndef DALIGN_RIGHT
   #define DALIGN_RIGHT
   DLabelFormat( G_WIDTH, LINE_Y_POS, "%d", __line);
   #undef DALIGN_RIGHT
#else
   DLabelFormat( G_WIDTH, LINE_Y_POS, "%d", __line);
#endif
   GFlush();

   //infinite loop
   while(1){}
}
