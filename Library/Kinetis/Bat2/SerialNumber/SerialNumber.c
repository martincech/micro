//******************************************************************************
//
//   Clock.c         Bat2 clock
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "Hardware.h"
#include "Clock/Pll.h"

#define PLL_PREDIVIDER    (F_CRYSTAL / 2000000)
#define PLL_MULTIPLIER    (F_USB / 2000000)

//------------------------------------------------------------------------------
//  Init
//------------------------------------------------------------------------------

void ClockInit( void)
// Init clock
{
	SIM->CLKDIV1 = ( 0
			| SIM_CLKDIV1_OUTDIV1(0)
			| SIM_CLKDIV1_OUTDIV2(0)
			| SIM_CLKDIV1_OUTDIV3(1)
			| SIM_CLKDIV1_OUTDIV4(1));

   // p�i MCG_C4_DRST_DRS == 3 za chv�li hod� hardfault, ostatn� volby jdou
   MCG->C4 &= ~MCG_C4_DRST_DRS_MASK;
   MCG->C4 |= MCG_C4_DRST_DRS(1);
   while((MCG->C4 & MCG_C4_DRST_DRS_MASK) != MCG_C4_DRST_DRS(1));
} // ClockInit

//------------------------------------------------------------------------------
//  Usb clock start
//------------------------------------------------------------------------------

void ClockUsbFsStart( void)
// Start USB clock
{
   //OSC->CR = OSC_CR_SC2P_MASK | OSC_CR_SC4P_MASK | OSC_CR_SC8P_MASK | OSC_CR_SC16P_MASK;
	PllInit(OSCINIT,   // Initialize the oscillator circuit
			F_CRYSTAL,  // CLKIN0 frequency
			LOW_POWER,     // Set the oscillator for low power mode
			CRYSTAL,     // Crystal or canned oscillator clock input
			PLL_PREDIVIDER,    // PLL predivider value
			PLL_MULTIPLIER,     // PLL multiplier
			PLL_ONLY);
} // UsbFsClockStart

//------------------------------------------------------------------------------
//  Usb clock stop
//------------------------------------------------------------------------------

void ClockUsbFsStop( void)
// Start USB clock
{
   PllShutdown();
} // UsbFsClockStop
