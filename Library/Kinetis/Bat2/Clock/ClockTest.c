//******************************************************************************
//
//   ClockTest.c     Clock test
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "ClockTest.h"
#include "Pwm/Pwm.h"
#include "Hardware.h"

#define DUTY                      50
#define DESIRED_FREQUENCY     250000

#define DIVIDER      (F_SYSTEM / DESIRED_FREQUENCY)

static void StartPwm( void);
// Start PWM

static void StopPwm( void);
// Stop PWM

//------------------------------------------------------------------------------
//  Start
//------------------------------------------------------------------------------

void ClockTestStart( void)
// Stop test clock
{
   StartPwm();
} // ClockTestStart

//------------------------------------------------------------------------------
//  Start USB
//------------------------------------------------------------------------------

void ClockTestStartUsb( void)
// Stop usb test clock
{
   ClockUsbFsStart();
   StartPwm();
} // ClockTestStartUsb

//------------------------------------------------------------------------------
//  Stop
//------------------------------------------------------------------------------

void ClockTestStop( void)
// Stop test clock
{
   StopPwm();
   ClockUsbFsStop();
} // ClockTestStop

//******************************************************************************

//------------------------------------------------------------------------------
//  Start PWM
//------------------------------------------------------------------------------

static void StartPwm( void)
// Start PWM
{
   Ftm2ClockEnable();
   FTM2->SC = 0;
   FTM2->CNTIN = 0;
   FTM2->CNT = 0;
   FTM2->MOD = DIVIDER - 1;
   FTM2->SC = FTM_SC_CLKS(1);
   PwmPortInit();
   FTM2->CONTROLS[BACKLIGHT_CHANNEL].CnSC = FTM_CnSC_MSB_MASK | FTM_CnSC_ELSB_MASK;
   FTM2->CONTROLS[BACKLIGHT_CHANNEL].CnV = (FTM2->MOD * DUTY) / 100;
} // StartPwm

//------------------------------------------------------------------------------
//  Stop PWM
//------------------------------------------------------------------------------

static void StopPwm( void)
// Stop PWM
{
   FTM2->SC = 0;
} // StopPwm