//******************************************************************************
//
//   ClockTest.h     Clock test
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef _Uni_H__
   #include "Unisys/Uni.h"
#endif

void ClockTestStart( void);
// Stop test clock

void ClockTestStop( void);
// Stop test clock