//******************************************************************************
//
//   Sleep.c         Bat2 Low power controller
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "Sleep/Sleep.h"
#include "Hardware.h"
#include "Cpu/Cpu.h"
#include "Usb/Usb.h"
#include "Ads1232/Ads1232.h"
#include "Lptmr/Lptmr.h"
#include "Multitasking/Tasks.h"
#include "Gui.h"
#include "Usb/Usb.h"

#define K_SLEEP_WAKE_ADC      (-1)

static volatile int Key;

//-----------------------------------------------------------------------------
// Init
//-----------------------------------------------------------------------------

void SleepInit( void)
// Init
{
   SIM->SOPT1CFG |= SIM_SOPT1CFG_USSWE_MASK;
   SIM->SOPT1 &= ~SIM_SOPT1_USBSSTBY_MASK;

   SMC->PMPROT = SMC_PMPROT_AVLLS_MASK | SMC_PMPROT_ALLS_MASK;

   LptmrInit();
   LlwuInit();
   LlwuEnable(KBD_R0_LLWU, KBD_R0_LLWU_FLAG);
   LlwuEnable(KBD_R1_LLWU, KBD_R1_LLWU_FLAG);
   //LlwuEnable(ADC_DOUT_RDY_LLWU, ADC_DOUT_RDY_LLWU_FLAG);
   //LlwuEnable(POWER_RS485_0_ON_LLWU, POWER_RS485_0_ON_LLWU_FLAG);
   //LlwuEnable(POWER_RS485_1_ON_LLWU, POWER_RS485_1_ON_LLWU_FLAG);
   //LlwuEnable(POWER_ETH_ON_LLWU, POWER_ETH_ON_LLWU_FLAG);
   //LlwuEnable(POWER_USB_DEVICE_ON_LLWU, POWER_USB_DEVICE_ON_LLWU_FLAG);
   //LlwuEnable(LLWU_LPTMR, LLWU_FLAG_NONE);
} // LpcInit

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

/*
 USB
 GSM
 Ethernet
 Wifi
 RS485
*/

#include "Message/GsmMessage.h"
#include "Rs485/Rs485Config.h"

int SleepScheduler( void)
// Sleep scheduler
{
   return K_IDLE;
   if(!TasksIdle()) {
      return K_IDLE;
   }
   if(!GuiIdle()) {
      return K_IDLE;
   }
   if(!UsbIdle()) {
      return K_IDLE;
   }
   // Neni potreba, GSM je vyuzivana pouze synchronn�m zp�sobem, tzn. thread, kter� ji vyu��v�
   // nebude IDLE, tud� nedojde k usp�n� nap�. mezi request a reply p�i komunikaci s GSM
   /*if(GsmMessage.PowerOptions.Mode != GSM_POWER_MODE_OFF) {
      return K_IDLE;
   }*/
   for(int i = 0; i < RS485_INTERFACE_COUNT ; i++) {
      if(Rs485Options[i].Enabled) {
         return K_IDLE;
      }
   }

   return K_IDLE;
   /*forever {
      Key = K_SLEEP_WAKE_ADC;
      SMC->PMCTRL = SMC_PMCTRL_STOPM(0x3);

      Stop();
      if(SMC->PMCTRL & SMC_PMCTRL_STOPA_MASK) {
         continue;
      }
      if(Key != K_SLEEP_WAKE_ADC) {
         break;
      }
      AdcReadSample();
   }*/
   
   return Key;
} // SleepScheduler

//-----------------------------------------------------------------------------
// Deep sleep
//-----------------------------------------------------------------------------

void SleepDeep( void)
// Deep sleep, reset wake-up
{
   SIM->SOPT1CFG |= SIM_SOPT1CFG_USSWE_MASK;
   SIM->SOPT1 |= SIM_SOPT1_USBSSTBY_MASK;

   SMC->PMCTRL = SMC_PMCTRL_STOPM(0x4); // VLLS
   SMC->VLLSCTRL = SMC_VLLSCTRL_VLLSM(0x1); // VLLS1

   WatchDogStopModeEnable( NO);
   Stop();
} // SleepDeep

//******************************************************************************

//-----------------------------------------------------------------------------
// LLWU event
//-----------------------------------------------------------------------------

void LlwuEvent(int Channel)
// Llwu callback
{

   switch(Channel) {
      case KBD_R0_LLWU:
      case KBD_R1_LLWU:
         Key = K_WAKE_UP;
         break;

      case ADC_DOUT_RDY_LLWU:
         Key = K_SLEEP_WAKE_ADC;
         break;

      //case POWER_RS485_0_ON_LLWU:
      //case POWER_RS485_1_ON_LLWU:
      case POWER_ETH_ON_LLWU:
      case POWER_USB_DEVICE_ON_LLWU:
         Key = K_WAKE_UP;
         break;

      case LLWU_LPTMR: // should be handled inside its own ISR
         break;

      default:
         break;
   }
} // LlwuEvent

//-----------------------------------------------------------------------------
// LPTmr event
//-----------------------------------------------------------------------------

void LPTmrEvent( void)
// LPTmr callback
{
   Key = K_TIMER_SLOW;
} // LPTmrEvent