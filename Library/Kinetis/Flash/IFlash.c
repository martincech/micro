//*****************************************************************************
//
//    IFlash.c       Kinetis internal flash programming
//    Version 1.0    (c) Veit Electronics
//
//*****************************************************************************

#include "IFlash.h"
#include "Hardware.h"
#include "Cpu/Cpu.h"
#include <string.h>

// flash commands
#define FCMD_PGM4       0x06
#define FCMD_PGM_ONCE   0x43
#define FCMD_READ_ONCE  0x41
#define SECTOR_ERASE    0x09

#define WRITE_BLOCK_SIZE  4

static void RunCommand( void);
// Run flash command - from RAM!

//-----------------------------------------------------------------------------
// Init
//-----------------------------------------------------------------------------

void IFlashInit( void)
// Init
{
   // turn off and invalidate any caching
   //FMC->PFB0CR = (0xf<<20) | (0x1<<19);
}

//-----------------------------------------------------------------------------
// Sector erase
//-----------------------------------------------------------------------------

TYesNo IFlashErase( dword StartSectorAddress, dword EndSectorAddress)
// Erase <SectorAddress> sector
{
dword Address;
   StartSectorAddress = StartSectorAddress & ~(MCU_FLASH_SECTOR_SIZE - 1);
   EndSectorAddress = (EndSectorAddress + MCU_FLASH_SECTOR_SIZE) & ~(MCU_FLASH_SECTOR_SIZE - 1);
   Address = StartSectorAddress;
   while(Address < EndSectorAddress) {
      FTFL->FCCOB0 = SECTOR_ERASE;
      FTFL->FCCOB1 = (byte)((Address >> 16) & 0xFF);
      FTFL->FCCOB2 = (byte)((Address >> 8)  & 0xFF);
      FTFL->FCCOB3 = (byte)((Address >> 0)  & 0xFF);

      InterruptDisable();
      FTFL->FSTAT = FTFL_FSTAT_ACCERR_MASK | FTFL_FSTAT_FPVIOL_MASK; // Write to clear
      RunCommand();
      InterruptEnable();

      if (FTFL->FSTAT != FTFL_FSTAT_CCIF_MASK) { // check error
         return NO;
      }
      Address += MCU_FLASH_SECTOR_SIZE;
   }
   return YES;
} // IFlashSectorErase

//-----------------------------------------------------------------------------
// Program
//-----------------------------------------------------------------------------

TYesNo IFlashProgram( dword Address, byte *Data, dword Size)
// Program <Address> by <Data> with <Size>
// <Address>, <Size> aligned to 4/8 multiplies depending on flash configuration
{
dword DataW;
byte FragmentLength;
   FTFL->FCCOB0 = FCMD_PGM4;
   Address &= ~(WRITE_BLOCK_SIZE - 1); // Must be 32/64-bit aligned (Flash address [2:0] = 000)
   Size /= WRITE_BLOCK_SIZE;
   
   while(Size) {
      FTFL->FCCOB1 = (byte)(Address >> 16);
      FTFL->FCCOB2 = (byte)((Address >> 8) & 0xFF);
      FTFL->FCCOB3 = (byte)(Address & 0xFF);

      memset((void *)&FTFL->FCCOB4, 0xFF, WRITE_BLOCK_SIZE);

      if(Size > WRITE_BLOCK_SIZE) {
         FragmentLength = WRITE_BLOCK_SIZE;
      } else {
         FragmentLength = Size;
      }

      DataW = ENDIAN_CONVERT_DWORD(*(dword *) Data);
      memcpy((void *)&FTFL->FCCOB4 + (4 - FragmentLength), &DataW + (4 - FragmentLength), FragmentLength);

      FTFL->FCCOB7 = *Data++;
      FTFL->FCCOB6 = *Data++;
      FTFL->FCCOB5 = *Data++;
      FTFL->FCCOB4 = *Data++;

      
      InterruptDisable();
      FTFL->FSTAT = FTFL_FSTAT_ACCERR_MASK | FTFL_FSTAT_FPVIOL_MASK; // Write to clear
      RunCommand();
      InterruptEnable();

      if (FTFL->FSTAT != FTFL_FSTAT_CCIF_MASK) { // check error
         return NO;
      }
      
      Address += WRITE_BLOCK_SIZE;
      Size--;
   }
   
   return YES;
} // IFlashProgram

//-----------------------------------------------------------------------------
// Read once
//-----------------------------------------------------------------------------

TYesNo IFlashReadOnce(byte Address, void *_Data, byte Length)
// Read once from <Address>
{
byte Ch;
byte *Data = (byte *) _Data;
   if(Length == 0) {
      return NO;
   }
   
byte IgnoreCount = Address % 4;
byte FragmentLength;
   while(Length) {
      FTFL->FCCOB0 = FCMD_READ_ONCE;
      FTFL->FCCOB1 = Address / 4;
         
      InterruptDisable();
      FTFL->FSTAT = FTFL_FSTAT_ACCERR_MASK | FTFL_FSTAT_FPVIOL_MASK; // Write to clear
      RunCommand();
      InterruptEnable();

      if (FTFL->FSTAT != FTFL_FSTAT_CCIF_MASK) { // check error
         return NO;
      }

      if(Length >= 4) {
         FragmentLength = 4;
      } else {
         FragmentLength = Length;
      }

      if(4 - IgnoreCount < FragmentLength) {
         FragmentLength = 4 - IgnoreCount;
      }

      memcpy(_Data, (void *)&FTFL->FCCOB7 + IgnoreCount, FragmentLength);

      Length -= FragmentLength;
      IgnoreCount = 0;

      Address += 4;
   }
   return YES;
} // IFlashReadOnce

//******************************************************************************

static void __attribute__ ((section (".data"))) RunCommand( void)
// Run flash command - from RAM!
{
   FTFL->FSTAT |= FTFL_FSTAT_CCIF_MASK; // launch
   while (!(FTFL->FSTAT & FTFL_FSTAT_CCIF_MASK));
} // RunCommand












#warning Netestovano/*
TYesNo IFlashProgramOnce(byte Address, const void *_Data, byte Length) 
// Program once <Address>, aligned to 4
{
byte Ch;
byte *Data = (byte *) _Data;
   if(Address % 4 != 0) {
      return NO;
   }
   if(Length == 0) {
      return NO;
   }
   Address = Address / 4;
byte FragmentLength;
   while(Length) {
      FTFL->FCCOB0 = FCMD_PGM_ONCE;
      FTFL->FCCOB1 = Address;

      if(Length >= 4) {
         FragmentLength = 4;
      } else {
         FragmentLength = Length;
      }

      memset(&FTFL->FCCOB4, 0xFF, 4);
      memcpy(&FTFL->FCCOB4, Data, FragmentLength);
      
      InterruptDisable();
      FTFL->FSTAT = FTFL_FSTAT_ACCERR_MASK | FTFL_FSTAT_FPVIOL_MASK; // Write to clear
      RunCommand();
      InterruptEnable();

      if (FTFL->FSTAT != FTFL_FSTAT_CCIF_MASK) { // check error
         return NO;
      }

      Address++;
      Length -= FragmentLength;
      Data += FragmentLength;
   }
   return YES;
}
*/