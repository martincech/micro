//*****************************************************************************
//
//    Eep.c         Kinetis Eeprom
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Eep.h"
#include "Hardware.h"
#include "Cpu/Cpu.h"

/*
   AN4282 - Using the Kinetis Family Enhanced EEPROM Functionality
   Before first read  - At startup FlexMemory initializes itself.
                      => Make sure FTFL->FCNFG EERDY is set
   Before every read  - Eeprom reads are not allowed while Eeprom write in progress.
                      => Make sure FTFL->FCNFG EERDY is set
   Before every write - Multiple concurrent writes to flash module are not allowed.
                      => Make sure FTFL->FSTAT CCIF is set
*/

static inline void ReadWaitForReady( void)    {while(!(FTFL->FCNFG & FTFL_FCNFG_EEERDY_MASK));}
static inline void WriteWaitForReady( void)   {while(!(FTFL->FSTAT & FTFL_FSTAT_CCIF_MASK));}

void EepInit( void)
// Initialization
{
   ReadWaitForReady();
}

void EepByteWrite( byte *Address, byte Value)
// Write byte <Value> to <Address>
{
   InterruptConditionallyDisable();
   WriteWaitForReady();
   *((byte *)EepAddress(Address)) = Value;
   ReadWaitForReady();
   InterruptConditionallyEnable();
}

void EepWordWrite( word *Address, word Value)
// Write word <Value> to <Address>
{
   InterruptConditionallyDisable();
   WriteWaitForReady();
   *((word *)EepAddress(Address)) = Value;
   ReadWaitForReady();
   InterruptConditionallyEnable();
}

void EepDwordWrite( dword *Address, dword Value)
// Write word <Value> to <Address>
{
   InterruptConditionallyDisable();
   WriteWaitForReady();
   *((dword *)EepAddress(Address)) = Value;
   ReadWaitForReady();
   InterruptConditionallyEnable();
}

void EepDataSave( void *Address, void *Data, word Size)
// Save <Data> with <Size> at <Address>
{
   InterruptConditionallyDisable();
   WriteWaitForReady();
   memcpy(EepAddress(Address), Data, Size);
   ReadWaitForReady();
   InterruptConditionallyEnable();
}