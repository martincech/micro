//*****************************************************************************
//
//    Eep.h        Eeprom
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Eep_H__
   #define __Eep_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#include <string.h>

#define FLEXRAM_START   0x14000000
#define EEP_OFFSET      FLEXRAM_START

#define EepAddress(Address)      ((Address) + EEP_OFFSET)

void EepInit( void);
// Initialization

inline byte EepByteRead( const byte *Address) {return *((byte *)EepAddress(Address));}
// Read byte from <Address>

inline word EepWordRead( const word *Address) {return *((word *)EepAddress(Address));}
// Read word from <Address>

inline dword EepDwordRead( const dword *Address) {return *((dword *)EepAddress(Address));}
// Read word from <Address>

void EepByteWrite( byte *Address, byte Value);
// Write byte <Value> to <Address>

void EepWordWrite( word *Address, word Value);
// Write word <Value> to <Address>

void EepDwordWrite( dword *Address, dword Value);
// Write word <Value> to <Address>

inline void EepDataLoad( const void *Address, void *Data, word Size)  {memcpy(Data, EepAddress(Address), Size);}
// Load <Data> with <Size> from <Address>

void EepDataSave( void *Address, void *Data, word Size);
// Save <Data> with <Size> at <Address>

#endif
