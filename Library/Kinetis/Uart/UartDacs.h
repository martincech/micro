//*****************************************************************************
//
//    UartDacs.h    UART Dacs local functions
//    Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

static void _UartDacsModeSet( TUartAddress Uart, TUartMode Mode);
// Set Dacs mode

static void __irq _Usart0DacsHandler( void);
// USART0 Dacs handler

static void __irq _Usart1DacsHandler( void);
// USART1 Dacs handler

static void __irq _Usart2DacsHandler( void);
// USART2 Dacs handler

static void __irq _Usart3DacsHandler( void);
// USART3 Dacs handler

static void __irq _Usart4DacsHandler( void);
// USART4 Dacs handler

static void __irq _Usart5DacsHandler( void);
// USART5 Dacs handler

static void __irq _Usart6DacsHandler( void);
// USART6 Dacs handler

static void __irq _Usart7DacsHandler( void);
// USART7 Dacs handler

static void __irq _Usart8DacsHandler( void);
// USART8 Dacs handler

static void __irq _Usart9DacsHandler( void);
// USART9 Dacs handler


static void _UsartDacsHandler( const TUartDescriptor *Uart);
// Common Dacs handler
