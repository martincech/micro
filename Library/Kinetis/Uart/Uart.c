//*****************************************************************************
//
//    Uart.c       UART complex services
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Uart/Uart.h"
#include "Cpu/Cpu.h"
#include "Cpu/UsartReg.h"
#include "System/System.h"
#include <string.h>

//------------------------------------------------------------------------------
//   UART descriptor
//------------------------------------------------------------------------------

#define UART_TRAILER_LEN_MAX        5            // number of maximal trailing characters
#define UART_LEADER_LEN_MAX         UART_TRAILER_LEN_MAX //number of maximal leading characters
#ifndef UART_PORTS_COUNT
   #define UART_PORTS_COUNT            1            // number of UARTS
#endif

#if UART_PORTS_COUNT > 10
   #error "Uart driver supports maximum of 10 UARTs"
#endif

typedef volatile UART_Type TUsart;

// Read/Write data :
typedef struct {
   // buffer :
   byte                *SendBuffer;                         // send buffer
   byte                *ReceiveBuffer;                      // receive buffer
   word                 ReceiveSizeMax;                     // receive buffer size
   word                 Size;                               // send/received size
   word                 Index;                              // active index
   byte                 Leader[UART_LEADER_LEN_MAX];        // frame leader sequence
   byte                 LeaderLen;                          // Frame leader sequence length
   byte                 Trailer[UART_TRAILER_LEN_MAX];      // frame trailer sequence
   byte                 TrailerLen;                         // frame trailer sequence length
   TUartReceiveAddress  Address;                            // received protocol address
   TUartReceiveAddress  SendAddress;                        // send protocol address
   word                 CheckSum;                           // computed checksum
   word                 Timer;                              // timeout/send delay timer
   volatile TUartStatus Status;                             // status
   // timers :
   word                 ReplyTimeout;
   word                 IntercharacterTimeout;
   word                 SendDelay;
} TUartData;

static TUartData _UartData[ UART_PORTS_COUNT];

// Read only data :
typedef struct {
   TUsart                 *Usart;                // USART hardware address
   TUartData              *Data;                 // read/write data pointer
   // tx enable :
   volatile unsigned      *TxEnableRegister;     // RS485 Tx enable register address
   volatile unsigned      *TxDisableRegister;    // RS485 Tx disable register address
   unsigned long           TxEnableMask;         // RS485 Tx enable bit mask
} TUartDescriptor;

// Uart descriptor :
static const TUartDescriptor _Uart[ UART_PORTS_COUNT] = {
#if UART_PORTS_COUNT >= 1
   { UART0, &_UartData[ 0], UART0_TX_ENABLE_REGISTER, UART0_TX_DISABLE_REGISTER, UART0_TX_ENABLE_MASK}
#endif
#if UART_PORTS_COUNT >= 2
   ,{ UART1, &_UartData[ 1], UART1_TX_ENABLE_REGISTER, UART1_TX_DISABLE_REGISTER, UART1_TX_ENABLE_MASK}
#endif
#if UART_PORTS_COUNT >= 3
   ,{ UART2, &_UartData[ 2], UART2_TX_ENABLE_REGISTER, UART2_TX_DISABLE_REGISTER, UART2_TX_ENABLE_MASK}
#endif
#if UART_PORTS_COUNT >= 4
   ,{ UART3, &_UartData[ 3], UART3_TX_ENABLE_REGISTER, UART3_TX_DISABLE_REGISTER, UART3_TX_ENABLE_MASK}
#endif
#if UART_PORTS_COUNT >= 5
   ,{ UART4, &_UartData[ 4], UART4_TX_ENABLE_REGISTER, UART4_TX_DISABLE_REGISTER, UART4_TX_ENABLE_MASK}
#endif
#if UART_PORTS_COUNT >= 6
   ,{ UART5, &_UartData[ 5], UART5_TX_ENABLE_REGISTER, UART5_TX_DISABLE_REGISTER, UART5_TX_ENABLE_MASK}
 #endif
#if UART_PORTS_COUNT >= 7
   ,{ UART6, &_UartData[ 6], UART6_TX_ENABLE_REGISTER, UART6_TX_DISABLE_REGISTER, UART6_TX_ENABLE_MASK}
 #endif
 #if UART_PORTS_COUNT >= 8
   ,{ UART7, &_UartData[ 7], UART7_TX_ENABLE_REGISTER, UART7_TX_DISABLE_REGISTER, UART7_TX_ENABLE_MASK}
 #endif
 #if UART_PORTS_COUNT >= 9
   ,{ UART8, &_UartData[ 8], UART8_TX_ENABLE_REGISTER, UART8_TX_DISABLE_REGISTER, UART8_TX_ENABLE_MASK}
 #endif
 #if UART_PORTS_COUNT >= 10
   ,{ UART9, &_UartData[ 9], UART9_TX_ENABLE_REGISTER, UART9_TX_DISABLE_REGISTER, UART9_TX_ENABLE_MASK}
 #endif
};

#define UsartGet( Uart)          _Uart[ Uart].Usart
#define UartGet( Uart)           &_Uart[ Uart]

#define UartTxEnable( Uart)      if((Uart)->TxEnableMask != 0){ *((Uart)->TxEnableRegister)  = (Uart)->TxEnableMask;}
#define UartTxDisable( Uart)     if((Uart)->TxEnableMask != 0){ *((Uart)->TxDisableRegister) = (Uart)->TxEnableMask;}

//-----------------------------------------------------------------------------
// Timer services
//-----------------------------------------------------------------------------

// Current task timer :
#define timerStart( Uart, t)      _SysTimerDisable(); timerSet( Uart, t); _SysTimerEnable()
#define timerStop( Uart)          _SysTimerDisable(); timerClear( Uart);  _SysTimerEnable()
// Timer for IRQ only :
#define timerSet( Uart, t)        (Uart)->Data->Timer = t
#define timerRestart( Uart)       (Uart)->Data->Timer = (Uart)->Data->IntercharacterTimeout
#define timerClear( Uart)         (Uart)->Data->Timer = 0

// Local functions :

static void _UartReceiveStart( const TUartDescriptor *Uart);
// Start receiving (used by IRQ handler too)

static void _UartTimerTick( const TUartDescriptor *Uart);
// Single device tick

static void RxSetStatusAndStop( const TUartDescriptor *Uart, EUartStatus Status);

static TYesNo UartReceivePause( TUartAddress Uart);
// Temporary pause RX data processing (reception has to be active), available only on interfaces with internal HW buffer
// Return YES when internal buffer available and receive paused, NO otherwise

static void UartReceiveUnpause( TUartAddress Uart);
// Allow data processing after previous pause

#ifdef UART_BINARY
   #include "Uart/UartBinary.h"
#endif

#ifdef UART_ASCII
   #include "Uart/UartAscii.h"
#endif

#ifdef UART_NATIVE
   #include "Uart/UartNative.h"
#endif

#ifdef UART_MEGAVI
   #include "Uart/UartMegavi.h"
#endif

#ifdef UART_DACS
   #include "Uart/UartDacs.h"
#endif

#ifdef UART_MODBUS
   #include "ModbusPort/port.h"
#endif
//-----------------------------------------------------------------------------
// Base services
//-----------------------------------------------------------------------------

#include "Uart/UartBase.c"

//-----------------------------------------------------------------------------
// Framing
//-----------------------------------------------------------------------------

void UartFramingSet( TUartAddress Address, char LeaderCharacter, char TrailerCharacter)
// Set framing characters based communication
{
const TUartDescriptor *Uart = UartGet( Address);

   Uart->Data->Leader[0]    = LeaderCharacter;
   Uart->Data->Leader[1]    = '\0';
   Uart->Data->LeaderLen    = 1;
   Uart->Data->Trailer[0]   = TrailerCharacter;
   Uart->Data->Trailer[1]   = '\0';
   Uart->Data->TrailerLen   = 1;
} // UartFramingSet

void UartFramingSetTrailerSeq( TUartAddress Address, char LeaderCharacter, char *TrailerSeq)
// Set framing characters based communication with trailer sequence of N characters(TrailerSeq is '\0' terminated)
{
const TUartDescriptor *Uart = UartGet( Address);

   Uart->Data->Leader[0]    = LeaderCharacter;
   Uart->Data->Leader[1]    = '\0';
   Uart->Data->LeaderLen    = 1;
   Uart->Data->TrailerLen   = strlen(TrailerSeq);
   if( Uart->Data->TrailerLen > UART_TRAILER_LEN_MAX){
      Uart->Data->TrailerLen = UART_TRAILER_LEN_MAX;
   }
   strncpy(Uart->Data->Trailer, TrailerSeq, Uart->Data->TrailerLen);
}

void UartFramingSetLeaderSeq( TUartAddress Address, char* LeaderSeq, char TrailerCharacter)
// Set framing characters based communication with leader sequence of N characters(LeaderSeq is '\0' terminated)
{
const TUartDescriptor *Uart = UartGet( Address);

   //Leader
   Uart->Data->LeaderLen   = strlen(LeaderSeq);
   if( Uart->Data->LeaderLen > UART_LEADER_LEN_MAX){
      Uart->Data->LeaderLen = UART_LEADER_LEN_MAX;
   }
   strncpy(Uart->Data->Leader, LeaderSeq, Uart->Data->LeaderLen);
   //Trailer
   Uart->Data->Trailer[0]   = TrailerCharacter;
   Uart->Data->Trailer[1]   = '\0';
   Uart->Data->TrailerLen   = 1;
}

void UartFramingSetLeaderTrailerSeq( TUartAddress Address, char* LeaderSeq, char *TrailerSeq)
// Set framing characters based communication with leader and trailer sequence of N characters(TrailerSeq and LeaderSeq is '\0' terminated)
{
const TUartDescriptor *Uart = UartGet( Address);

   //Leader
   Uart->Data->LeaderLen   = strlen(LeaderSeq);
   if( Uart->Data->LeaderLen > UART_LEADER_LEN_MAX){
      Uart->Data->LeaderLen = UART_LEADER_LEN_MAX;
   }
   strncpy(Uart->Data->Leader, LeaderSeq, Uart->Data->LeaderLen);
   //Trailer
   Uart->Data->TrailerLen   = strlen(TrailerSeq);
   if( Uart->Data->TrailerLen > UART_TRAILER_LEN_MAX){
      Uart->Data->TrailerLen = UART_TRAILER_LEN_MAX;
   }
   strncpy(Uart->Data->Trailer, TrailerSeq, Uart->Data->TrailerLen);
}
//-----------------------------------------------------------------------------
// Tx address
//-----------------------------------------------------------------------------

void UartSendAddressSet( TUartAddress Address, TUartReceiveAddress SendAddress)
// Set protocol specific send address field
{
const TUartDescriptor *Uart = UartGet( Address);
   Uart->Data->SendAddress = SendAddress;
} // UartSendAddressSet

//-----------------------------------------------------------------------------
// Tx delay
//-----------------------------------------------------------------------------

void UartSendDelaySet( TUartAddress Address, unsigned SendDelay)
// Start send after <Delay>
{
const TUartDescriptor *Uart = UartGet( Address);
   Uart->Data->SendDelay = (word)SendDelay;
} // UartSendDelaySet

//------------------------------------------------------------------------------
//   Buffer set
//------------------------------------------------------------------------------

void UartBufferSet( TUartAddress Address, void *Buffer, int Size)
// Set receive buffer
{
const TUartDescriptor *Uart = UartGet( Address);
TUartData *UData;

   UData = Uart->Data;
   UData->ReceiveBuffer  = Buffer;
   UData->ReceiveSizeMax = Size;
   Uart->Data->Size = Uart->Data->Index = 0;
} // UartBufferSet

//------------------------------------------------------------------------------
//   Buffer move
//------------------------------------------------------------------------------

void UartBufferMove( TUartAddress Address, int Offset)
// Move buffer by offset, pre-offset data are thrown
{
const TUartDescriptor *Uart = UartGet( Address);

   if( !Offset || !UartReceivePause( Address)){
      return;
   }
   
   if( Uart->Data->Size > Offset){
      Uart->Data->Size -= Offset;
      memmove(Uart->Data->ReceiveBuffer, Uart->Data->ReceiveBuffer + Offset, Uart->Data->Size);
   }else {
      Uart->Data->Size = 0;
   } 
   Uart->Data->Index = Uart->Data->Size;
   UartReceiveUnpause( Address);
}
//------------------------------------------------------------------------------
//   Mode
//------------------------------------------------------------------------------

void UartModeSet( TUartAddress Uart, TUartMode Mode)
// Set operating mode
{
TUsart *Usart;

   Usart = UsartGet( Uart);
   switch( Mode){
#ifdef UART_BINARY   
      case UART_MODE_BINARY_MASTER :
         // binary mode master side
      case UART_MODE_BINARY_SLAVE :
         // binary mode slave side
         _UartBinaryModeSet( Uart, Mode);
         break;
#endif // UART_BINARY

#ifdef UART_ASCII
      case UART_MODE_ASCII_MASTER :
         // ASCII framing mode master side
      case UART_MODE_ASCII_SLAVE :
         // ASCII framing mode slave size
         _UartAsciiModeSet( Uart, Mode);
         break;
#endif // UART_ASCII

#ifdef UART_NATIVE
      case UART_MODE_NATIVE_MASTER :
         // native framing protocol master side
      case UART_MODE_NATIVE_SLAVE :
         // native framing protocol slave side
         _UartNativeModeSet( Uart, Mode);
         break;
#endif // UART_NATIVE   

#ifdef UART_MEGAVI
      case UART_MODE_MEGAVI_MASTER :
         // megavi protocol master side
      case UART_MODE_MEGAVI_SLAVE :
         // megavi protocol slave side
         _UartMegaviModeSet( Uart, Mode);
         break;
#endif

#ifdef UART_DACS
      case UART_MODE_DACS_SLAVE :
         // megavi protocol slave side
         _UartDacsModeSet( Uart, Mode);
         break;
#endif
#ifdef UART_MODBUS
      case UART_MODE_MODBUS :
         _UartModbusModeSet( Uart, Mode);
         break;
#endif
      default :
         break;

   }
} // UartModeSet

//------------------------------------------------------------------------------
//   Receive
//------------------------------------------------------------------------------

void UartReceive( TUartAddress Address)
// Start receiveing
{
const TUartDescriptor *Uart = UartGet( Address);

   usartInterruptDisable( UsartGet( Address));      // stop interrupts
   timerStop( Uart);                          // disable timer
   _UartReceiveStart( Uart);
} // UartReceive

//------------------------------------------------------------------------------
//   Rx Status
//------------------------------------------------------------------------------

TUartStatus UartReceiveStatus( TUartAddress Address)
// Returns receiving status
{
const TUartDescriptor *Uart = UartGet( Address);

   return( Uart->Data->Status);
} // UartReceiveStatus

//------------------------------------------------------------------------------
//   Rx Buffer
//------------------------------------------------------------------------------

void *UartReceiveBuffer( TUartAddress Address)
// Returns receive buffer
{
const TUartDescriptor *Uart = UartGet( Address);
   return( Uart->Data->ReceiveBuffer);   
} // UartReceiveBuffer

//------------------------------------------------------------------------------
//   Rx Size
//------------------------------------------------------------------------------

int UartReceiveSize( TUartAddress Address)
// Returns received data size
{
const TUartDescriptor *Uart = UartGet( Address);
   return( Uart->Data->Size);
} // UartReceiveSize

//------------------------------------------------------------------------------
//   Rx Address
//------------------------------------------------------------------------------

TUartReceiveAddress UartReceiveAddress( TUartAddress Address)
// Returns protocol specific received address field
{
const TUartDescriptor *Uart = UartGet( Address);
   return( Uart->Data->Address);
} // UartReceiveAddress

//------------------------------------------------------------------------------
//   Send
//------------------------------------------------------------------------------

void UartSend( TUartAddress Address, void *Buffer, int Size)
// Start Tx with <Buffer> and <Size>
{
const TUartDescriptor *Uart = UartGet( Address);
TUartData *UData;
TUsart *Usart;

   Usart = UsartGet( Address);
   UData = Uart->Data;
   usartInterruptDisable( Usart);
   timerStop( Uart);               // disable timer
   // remember Tx data :
   UData->SendBuffer = (byte *)Buffer;
   UData->Size       = Size;                // send size
   UData->Index      = 0;                   // send first character
   UData->Status     = UART_SEND_ACTIVE;
   UartFlush( Address);                        // flush old Rx data
   if( UData->SendDelay == 0){
      UartTxEnable( Uart);         // RS485 direction to Tx
      usartTxInterruptEnable( Usart);       // enable Tx data interrupt
      usartTxEnable( Usart);
      return;
   }
   timerStart( Uart, UData->SendDelay); // start send delay
} // UartSend

//------------------------------------------------------------------------------
//   Rx Status
//------------------------------------------------------------------------------

TUartStatus UartSendStatus( TUartAddress Address)
// Returns send status
{
const TUartDescriptor *Uart = UartGet( Address);

   // check for Rx state :
   if( Uart->Data->Status < UART_SEND_ACTIVE){
      return( UART_SEND_DONE);
   }
   return( Uart->Data->Status);
} // UartSendStatus


//------------------------------------------------------------------------------
//   Flush
//------------------------------------------------------------------------------

void UartFlush( TUartAddress Address)
// Flush Rx/Tx buffer
{
TUsart *Usart;
byte    ch;

   Usart = UsartGet( Address);
   timerStop( UartGet( Address));           // disable timer
   while( usartRxReady( Usart)){
      ch = usartRxData( Usart);        // read Rx data
   }
   if( usartRxError( Usart)){
      usartStatusReset( Usart);        // reset line error
   }
} // UartFlush

//------------------------------------------------------------------------------
//   Stop
//------------------------------------------------------------------------------

void UartStop( TUartAddress Address)
// Stop Rx/Tx discard Rx/Tx data
{
const TUartDescriptor *Uart = UartGet( Address);

   timerStop( Uart);                      // disable timer
   usartInterruptDisable( UsartGet(Address));  // disable iterrupts
   usartReset( UsartGet(Address));             // reset device
   Uart->Data->Status = UART_IDLE;        // set state to idle
   usartRxDisable( Uart->Usart);
   usartTxDisable( Uart->Usart);
} // UartStop

//------------------------------------------------------------------------------
//   Pause
//------------------------------------------------------------------------------

#include <assert.h>
TYesNo UartReceivePause( TUartAddress Address)
// Temporary pause RX data processing (reception has to be active), available only on interfaces with internal HW buffer
// Return YES when paused, NO otherwise
{
const TUartDescriptor *Uart = UartGet( Address);
   
   if( Uart->Data->Status != UART_RECEIVE_ACTIVE){
      return NO;
   }
   
   //assert( usartRxFifoSize( UsartGet(Address)) != 0);
   usartRxInterruptDisable( UsartGet(Address));
   Uart->Data->Status = UART_RECEIVE_PAUSED;
   return YES;
}

//------------------------------------------------------------------------------
//   Unpause
//------------------------------------------------------------------------------

void UartReceiveUnpause( TUartAddress Address)
// Allow data processing after previous pause
{
const TUartDescriptor *Uart = UartGet( Address);
   
   if( Uart->Data->Status != UART_RECEIVE_PAUSED){
      return;
   } 
   Uart->Data->Status = UART_RECEIVE_ACTIVE;
   usartRxInterruptEnable( UsartGet(Address));
}


//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

void UartTimer( void)
// Join to external 1ms timer tick
{
TUartAddress Address;
const TUartDescriptor *Uart;
   for(Address = UART_UART0 ; Address < UART_UART0 + UART_PORTS_COUNT ; Address++) {
      Uart = UartGet( Address);
      if( Uart->Data->Timer) {
         _UartTimerTick( Uart);
      }
   }
} // UartTimer


//******************************************************************************

//------------------------------------------------------------------------------
//   Receive start
//------------------------------------------------------------------------------

static void _UartReceiveStart( const TUartDescriptor *Uart)
// Start receiving (used by IRQ handler too)
{
TUartData *UData;

   UData = Uart->Data;
   timerClear( Uart);                            // disable timer
   // prepare Rx data :
   UData->Size   = 0;                            // no data received
   UData->Index  = 0;                            // receive leader
   UData->Status = UART_RECEIVE_ACTIVE;          // wait for first character
   // start reply timeout :
   if( UData->ReplyTimeout != UART_TIMEOUT_OFF){
      timerSet( Uart, UData->ReplyTimeout);      // start reply timeout
   }
   // enable receiver :   
   usartRxInterruptEnable( Uart->Usart);  // enable Rx data interrupt
   usartRxEnable( Uart->Usart);
} // _UartReceiveStart

//------------------------------------------------------------------------------
//   Timer tick
//------------------------------------------------------------------------------

static void _UartTimerTick( const TUartDescriptor *Uart)
// Single device tick
{
   if( Uart->Data->Status == UART_RECEIVE_PAUSED){
      return;                               // paused
   }
   if( --Uart->Data->Timer){
      return;                               // not expired
   }
   // check for delayed Tx :
   if( Uart->Data->Status == UART_SEND_ACTIVE){
      UartTxEnable( Uart);                  // RS485 direction to Tx
      usartTxInterruptEnable( Uart->Usart); // enable Tx data interrupt
      usartTxEnable( Uart->Usart);
      return;
   }
   // check for Rx timeout :
   if( Uart->Data->Status == UART_RECEIVE_ACTIVE){
      if( Uart->Data->Index == 0){
         Uart->Data->Status = UART_RECEIVE_REPLY_TIMEOUT;
      } else {
         Uart->Data->Status = UART_RECEIVE_TIMEOUT;
      }
      usartRxInterruptDisable( Uart->Usart);
      usartRxDisable( Uart->Usart);
   }
} // _UartTimerTick

//------------------------------------------------------------------------------
//   Rx status 
//------------------------------------------------------------------------------

static void RxSetStatusAndStop( const TUartDescriptor *Uart, EUartStatus Status)
// Set status of Uart to Status, dissable RX interrupt and timer
{
   Uart->Data->Status = Status;
   timerClear( Uart);                            // disable timeout
   usartRxInterruptDisable( Uart->Usart);        // disable Rx data interrupt   
   usartRxDisable( Uart->Usart);
}


static void _UartSendStart( TUartAddress Address, int Delay)
// Initiates send with <Delay>
{
const TUartDescriptor *Uart = UartGet( Address);
   if(Delay == 0) {
      Uart->Data->Status     = UART_SEND_ACTIVE;
      UartTxEnable( Uart);                    // RS485 direction to Tx
      usartTxInterruptEnable( UsartGet(Address));  // enable Tx data interrupt
      usartTxEnable( Uart->Usart);
      return;
   }
   Uart->Data->Status     = UART_SEND_WAITING;
   usartRxInterruptEnable( UsartGet(Address));       // enable Rx data interrupt
   usartRxEnable( Uart->Usart);
   timerStart( Uart, Uart->Data->SendDelay); // start send delay
} // _UartSendStart


//------------------------------------------------------------------------------
//   Services by mode
//------------------------------------------------------------------------------

#ifdef UART_SIMPLE
   #include "Uart/UartSimple.c"     // simple controlled UART services
#endif   
   
#ifdef UART_BINARY
   #include "Uart/UartBinary.c"     // binary mode UART services
#endif

#ifdef UART_ASCII
   #include "Uart/UartAscii.c"      // ASCII framing UART services
#endif

#ifdef UART_NATIVE
   #include "Uart/UartNative.c"     // native protocol UART services
#endif

#ifdef UART_MEGAVI
   #include "Uart/UartMegavi.c"     // MEGAVI protocol UART services
#endif

#ifdef UART_DACS
   #include "Uart/UartDacs.c"      // Dacs protocol UART services
#endif

#ifdef UART_MODBUS
   #include "ModbusPort/portserial.c"
#endif