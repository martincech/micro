//*****************************************************************************
//
//    UartMegavi.c UART Megavi local functions
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Megavi/MegaviDef.h"             // Megavi protocol definitions

//------------------------------------------------------------------------------
// Parsing macros
//------------------------------------------------------------------------------

#define MEGAVI_LEADER_OFFSET           0
#define MEGAVI_ID_OFFSET               1
#define MEGAVI_COMMAND_CODE_OFFSET     2
#define MEGAVI_REPLY_CODE_OFFSET       MEGAVI_COMMAND_CODE_OFFSET
#define MEGAVI_DATA_OFFSET             MEGAVI_COMMAND_CODE_OFFSET
#define MegaviXorDataOffset( Size)    (MEGAVI_DATA_OFFSET + (Size))
#define MegaviTrailerOffset( Size)    (MegaviXorDataOffset(Size) + 1)

//------------------------------------------------------------------------------
// Register set/get macros
//------------------------------------------------------------------------------

#define _UsartTxData( Usart, Data)           usartTxDataBit8( Usart, 0)usartTxData( Usart, Data)
#define _UsartTxControl( Usart, Control)     usartTxDataBit8( Usart, 1)usartTxData( Usart, Control)
#define _UsartRxIsControlParity( Usart)      (usartRxDataBit8( Usart) != 0)

// Local functions :

static byte _CalculateCrc( byte *Data, word Size);
// Calculate XOR from <Data> of <Size> length

//------------------------------------------------------------------------------
//   Mode
//------------------------------------------------------------------------------

static void _UartMegaviModeSet( TUartAddress Address, TUartMode Mode)
// Set Megavi mode
{
const TUartDescriptor *Uart = UartGet( Address);

   usartInterruptDisable( UsartGet(Address));
   InterruptDisable();
   switch( Address){
#if UART_PORTS_COUNT >= 1
      case UART_UART0 :
         CpuIrqAttach( UART0_RX_TX_IRQn, UART0_PRIORITY, _Usart0MegaviHandler);
         CpuIrqAttach( UART0_ERR_IRQn,   UART0_PRIORITY, _Usart0MegaviHandler);
         CpuIrqEnable( UART0_RX_TX_IRQn);
         CpuIrqEnable( UART0_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 2
      case UART_UART1 :
         CpuIrqAttach( UART1_RX_TX_IRQn, UART1_PRIORITY, _Usart1MegaviHandler);
         CpuIrqAttach( UART1_ERR_IRQn,   UART1_PRIORITY, _Usart1MegaviHandler);
         CpuIrqEnable( UART1_RX_TX_IRQn);
         CpuIrqEnable( UART1_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 3
      case UART_UART2 :
         CpuIrqAttach( UART2_RX_TX_IRQn, UART2_PRIORITY, _Usart2MegaviHandler);
         CpuIrqAttach( UART2_ERR_IRQn,   UART2_PRIORITY, _Usart2MegaviHandler);
         CpuIrqEnable( UART2_RX_TX_IRQn);
         CpuIrqEnable( UART2_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 4
      case UART_UART3 :
         CpuIrqAttach( UART3_RX_TX_IRQn, UART3_PRIORITY, _Usart3MegaviHandler);
         CpuIrqAttach( UART3_ERR_IRQn,   UART3_PRIORITY, _Usart3MegaviHandler);
         CpuIrqEnable( UART3_RX_TX_IRQn);
         CpuIrqEnable( UART3_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 5
      case UART_UART4 :
         CpuIrqAttach( UART4_RX_TX_IRQn, UART4_PRIORITY, _Usart4MegaviHandler);
         CpuIrqAttach( UART4_ERR_IRQn,   UART4_PRIORITY, _Usart4MegaviHandler);
         CpuIrqEnable( UART4_RX_TX_IRQn);
         CpuIrqEnable( UART4_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 6
      case UART_UART5 :
         CpuIrqAttach( UART5_RX_TX_IRQn, UART5_PRIORITY, _Usart5MegaviHandler);
         CpuIrqAttach( UART5_ERR_IRQn,   UART5_PRIORITY, _Usart5MegaviHandler);
         CpuIrqEnable( UART5_RX_TX_IRQn);
         CpuIrqEnable( UART5_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 7
      case UART_UART6 :
         CpuIrqAttach( UART6_RX_TX_IRQn, UART6_PRIORITY, _Usart6MegaviHandler);
         CpuIrqAttach( UART6_ERR_IRQn,   UART6_PRIORITY, _Usart6MegaviHandler);
         CpuIrqEnable( UART6_RX_TX_IRQn);
         CpuIrqEnable( UART6_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 8
      case UART_UART7 :
         CpuIrqAttach( UART7_RX_TX_IRQn, UART7_PRIORITY, _Usart7MegaviHandler);
         CpuIrqAttach( UART7_ERR_IRQn,   UART7_PRIORITY, _Usart7MegaviHandler);
         CpuIrqEnable( UART7_RX_TX_IRQn);
         CpuIrqEnable( UART7_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 9
      case UART_UART8 :
         CpuIrqAttach( UART8_RX_TX_IRQn, UART8_PRIORITY, _Usart8MegaviHandler);
         CpuIrqAttach( UART8_ERR_IRQn,   UART8_PRIORITY, _Usart8MegaviHandler);
         CpuIrqEnable( UART8_RX_TX_IRQn);
         CpuIrqEnable( UART8_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 10
      case UART_UART9 :
         CpuIrqAttach( UART9_RX_TX_IRQn, UART9_PRIORITY, _Usart9MegaviHandler);
         CpuIrqAttach( UART9_ERR_IRQn,   UART9_PRIORITY, _Usart9MegaviHandler);
         CpuIrqEnable( UART9_RX_TX_IRQn);
         CpuIrqEnable( UART9_ERR_IRQn);
         break;
#endif
      default :
         break;
   }
   InterruptEnable();
} // _UartMegaviModeSet

//------------------------------------------------------------------------------
//   IRQ Handler
//------------------------------------------------------------------------------

static void __irq _Usart0MegaviHandler( void)
// USART0 Megavi handler
{
   _UsartMegaviHandler( UartGet(  UART_UART0));
} // _Usart0MegaviHandler

static void __irq _Usart1MegaviHandler( void)
// USART1 Megavi handler
{
   _UsartMegaviHandler( UartGet(  UART_UART1));
} // _Usart1MegaviHandler

static void __irq _Usart2MegaviHandler( void)
// USART2 Megavi handler
{
   _UsartMegaviHandler( UartGet(  UART_UART2));
} // _Usart2MegaviHandler

static void __irq _Usart3MegaviHandler( void)
// USART3 Megavi handler
{
   _UsartMegaviHandler( UartGet(  UART_UART3));
} // _Usart3MegaviHandler

static void __irq _Usart4MegaviHandler( void)
// USART4 Megavi handler
{
   _UsartMegaviHandler( UartGet(  UART_UART4));
} // _Usart4MegaviHandler

static void __irq _Usart5MegaviHandler( void)
// USART5 Megavi handler
{
   _UsartMegaviHandler( UartGet(  UART_UART5));
} // _Usart5MegaviHandler

static void __irq _Usart6MegaviHandler( void)
// USART6 Megavi handler
{
   _UsartMegaviHandler( UartGet(  UART_UART6));
} // _Usar

static void __irq _Usart7MegaviHandler( void)
// USART7 Megavi handler
{
   _UsartMegaviHandler( UartGet(  UART_UART7));
} // _Usar

static void __irq _Usart8MegaviHandler( void)
// USART8 Megavi handler
{
   _UsartMegaviHandler( UartGet(  UART_UART8));
} // _Usar

static void __irq _Usart9MegaviHandler( void)
// USART9 Megavi handler
{
   _UsartMegaviHandler( UartGet(  UART_UART9));
} // _Usar

//------------------------------------------------------------------------------
//   Common Megavi Handler
//------------------------------------------------------------------------------

static void _UsartMegaviHandler( const TUartDescriptor *Uart)
// Common Megavi handler
{
TUartData *UData;
TUsart    *Usart;
byte       DataByte;
TYesNo     DataByteIsControl;
word       Index;
unsigned   Status;

   Usart = Uart->Usart;
   UData = Uart->Data;
   Status = usartStatus( Usart);
   // check for Rx errors :
   if( usartStatusRxError( Status)){
      if( (UData->Status == UART_RECEIVE_ACTIVE)){
         if(usartStatusRxFraming( Status)){
            if( UData->Index >= MEGAVI_COMMAND_CODE_OFFSET){
               RxSetStatusAndStop( Uart, UART_RECEIVE_FRAMING_ERROR);
            } else {
               UData->Index = MEGAVI_LEADER_OFFSET;
            }
         }
         if( usartStatusRxOverrun( Status)){
            RxSetStatusAndStop( Uart, UART_RECEIVE_OVERRUN_ERROR);
         }
         timerRestart( Uart);                   // start intercharacter timeout
      }
      usartStatusReset( Usart);
   }
//------------------------------------------------------------------------------
   // check for Rx data :
   if( usartStatusRx( Status)){
      DataByte = usartRxData( Usart);
      DataByteIsControl = _UsartRxIsControlParity( Usart);
      if( UData->Status == UART_RECEIVE_ACTIVE){
         //erase parity bit from data when parity enabled
         if( Usart->C1 & UART_C1_PE_MASK && !(Usart->C1 & UART_C1_M_MASK)){
            // 8th bit is parity
            DataByte &= ~0x80;
         }
         switch( UData->Index){
            case MEGAVI_LEADER_OFFSET :
               if( DataByte != MEGAVI_LEADER || !DataByteIsControl){
                  return;
               }
               break;

            case MEGAVI_ID_OFFSET :
               if( DataByteIsControl){
                  RxSetStatusAndStop( Uart, UART_RECEIVE_FRAMING_ERROR);
                  return;
               }
               if( (DataByte & 0x0F) != MEGAVI_MODULE_CODE){
                  timerRestart( Uart);                   // start intercharacter timeout
                  UData->Index = MEGAVI_LEADER_OFFSET;   // wrong module code, wait for new frame
                  return;
               }
               UData->Address = DataByte;
               break;

            case MEGAVI_COMMAND_CODE_OFFSET :
               if( DataByteIsControl){
                  RxSetStatusAndStop( Uart, UART_RECEIVE_FRAMING_ERROR);
                  return;
               }
               switch( DataByte){
                  case MEGAVI_COMMAND_VERSION :
                  case MEGAVI_COMMAND_MALE_DATA_GET :
                  case MEGAVI_COMMAND_FEMALE_DATA_GET :
                     UData->Size = 1;                    // command code
                     break;

                  case MEGAVI_COMMAND_SMS_SEND :
                     UData->Size = MEGAVI_SMS_SIZE + 2; // command code + SmsTarget
                     break;

                  case MEGAVI_COMMAND_SMS_STATUS_GET :
                     UData->Size = 2;                   // command code + SlotNumber
                     break;

                  default:
                     timerRestart( Uart);                  // start intercharacter timeout
                     UData->Index = MEGAVI_LEADER_OFFSET;  // wrong command code, wait for new frame
                     return;
               }

               if( UData->Size > UData->ReceiveSizeMax){
                  RxSetStatusAndStop( Uart, UART_RECEIVE_SIZE_ERROR);
                  return;
               }
               UData->ReceiveBuffer[0] = DataByte;
               break;

            default :
               Index = UData->Index;
               if( Index == MegaviTrailerOffset( UData->Size)){
                  if(( DataByte == MEGAVI_TRAILER) && DataByteIsControl){
                     RxSetStatusAndStop( Uart, UART_RECEIVE_FRAME);
                  } else {
                     RxSetStatusAndStop( Uart, UART_RECEIVE_FRAMING_ERROR);
                  }
                  return;
               }
               if( DataByteIsControl){
                  RxSetStatusAndStop( Uart, UART_RECEIVE_FRAMING_ERROR);
                  return;
               }
               if( Index == MegaviXorDataOffset( UData->Size)){
                  UData->CheckSum = _CalculateCrc( UData->ReceiveBuffer, UData->Size);
                  if( DataByte != (byte) UData->CheckSum){
                     RxSetStatusAndStop( Uart, UART_RECEIVE_CRC_ERROR);
                     return;
                  }
                  break;
               }
               Index -= MEGAVI_COMMAND_CODE_OFFSET;
               UData->ReceiveBuffer[Index] = DataByte;
               break;
         }
         timerRestart( Uart);                         // start intercharacter timeout
         UData->Index++;                              // wait for next character
      }
   }

//------------------------------------------------------------------------------
   // check for Tx data :
   if( usartStatusTx( Status)){
      if( UData->Status == UART_SEND_ACTIVE){
         switch( UData->Index){
            case MEGAVI_LEADER_OFFSET:
               _UsartTxControl( Usart, MEGAVI_LEADER);
               break;

            case MEGAVI_ID_OFFSET:
               _UsartTxData( Usart, UData->SendAddress);   // copy address from incoming command
               break;

            default:
               Index = UData->Index;
               if( Index == MegaviTrailerOffset( UData->Size)){
                  _UsartTxControl( Usart, MEGAVI_TRAILER);
                  usartTxInterruptDisable( Usart);     // stop Tx
                  usartTxDoneInterruptEnable( Usart);  // wait for Tx done
                  break;
               }
               if( Index == MegaviXorDataOffset( UData->Size)){
                  UData->CheckSum = _CalculateCrc( UData->SendBuffer, UData->Size);
                  _UsartTxData( Usart, UData->CheckSum);
                  break;
               }

               Index -= MEGAVI_COMMAND_CODE_OFFSET;
               DataByte = UData->SendBuffer[ Index];
               _UsartTxData( Usart, DataByte);
               break;
         }
         UData->Index++;
      }
   }

//------------------------------------------------------------------------------
   // check for Tx done :
   if( usartStatusTxDone( Status)){
      if( UData->Status == UART_SEND_ACTIVE){
         UData->Status = UART_SEND_DONE;
         UartTxDisable( Uart);                   // RS485 direction to Rx
         usartTxDoneInterruptDisable( Usart);
         _UartReceiveStart( Uart);               // start receiving
      }
   }
} // _UsartMegaviHandler

//------------------------------------------------------------------------------
//   CRC calculation
//------------------------------------------------------------------------------

static byte _CalculateCrc( byte *Data, word Size)
// Calculate XOR from <Data> of <Size> length
{
byte Crc;

   Crc  = 0xFF;
   do {
      Crc ^= *Data;
      Data++;
   } while( --Size);
   return( Crc);
} // CalculateCrc

