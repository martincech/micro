//*****************************************************************************
//
//    UartBinary.c UART binary local functions
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

//------------------------------------------------------------------------------
//   Mode
//------------------------------------------------------------------------------

static void _UartBinaryModeSet( TUartAddress Address, TUartMode Mode)
// Set binary mode
{
const TUartDescriptor *Uart = UartGet( Address);

   usartInterruptDisable( UsartGet(Address));
   InterruptDisable();
   switch( Address){
#if UART_PORTS_COUNT >= 1
      case UART_UART0 :
         CpuIrqAttach( UART0_RX_TX_IRQn, UART0_PRIORITY, _Usart0BinaryHandler);
         CpuIrqAttach( UART0_ERR_IRQn,   UART0_PRIORITY, _Usart0BinaryHandler);
         CpuIrqEnable( UART0_RX_TX_IRQn);
         CpuIrqEnable( UART0_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 2
      case UART_UART1 :
         CpuIrqAttach( UART1_RX_TX_IRQn, UART1_PRIORITY, _Usart1BinaryHandler);
         CpuIrqAttach( UART1_ERR_IRQn,   UART1_PRIORITY, _Usart1BinaryHandler);
         CpuIrqEnable( UART1_RX_TX_IRQn);
         CpuIrqEnable( UART1_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 3
      case UART_UART2 :
         CpuIrqAttach( UART2_RX_TX_IRQn, UART2_PRIORITY, _Usart2BinaryHandler);
         CpuIrqAttach( UART2_ERR_IRQn,   UART2_PRIORITY, _Usart2BinaryHandler);
         CpuIrqEnable( UART2_RX_TX_IRQn);
         CpuIrqEnable( UART2_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 4
      case UART_UART3 :
         CpuIrqAttach( UART3_RX_TX_IRQn, UART3_PRIORITY, _Usart3BinaryHandler);
         CpuIrqAttach( UART3_ERR_IRQn,   UART3_PRIORITY, _Usart3BinaryHandler);
         CpuIrqEnable( UART3_RX_TX_IRQn);
         CpuIrqEnable( UART3_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 5
      case UART_UART4 :
         CpuIrqAttach( UART4_RX_TX_IRQn, UART4_PRIORITY, _Usart4BinaryHandler);
         CpuIrqAttach( UART4_ERR_IRQn,   UART4_PRIORITY, _Usart4BinaryHandler);
         CpuIrqEnable( UART4_RX_TX_IRQn);
         CpuIrqEnable( UART4_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 6
      case UART_UART5 :
         CpuIrqAttach( UART5_RX_TX_IRQn, UART5_PRIORITY, _Usart5BinaryHandler);
         CpuIrqAttach( UART5_ERR_IRQn,   UART5_PRIORITY, _Usart5BinaryHandler);
         CpuIrqEnable( UART5_RX_TX_IRQn);
         CpuIrqEnable( UART5_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 7
      case UART_UART6 :
         CpuIrqAttach( UART6_RX_TX_IRQn, UART6_PRIORITY, _Usart6BinaryHandler);
         CpuIrqAttach( UART6_ERR_IRQn,   UART6_PRIORITY, _Usart6BinaryHandler);
         CpuIrqEnable( UART6_RX_TX_IRQn);
         CpuIrqEnable( UART6_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 8
      case UART_UART7 :
         CpuIrqAttach( UART7_RX_TX_IRQn, UART7_PRIORITY, _Usart7BinaryHandler);
         CpuIrqAttach( UART7_ERR_IRQn,   UART7_PRIORITY, _Usart7BinaryHandler);
         CpuIrqEnable( UART7_RX_TX_IRQn);
         CpuIrqEnable( UART7_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 9
      case UART_UART8 :
         CpuIrqAttach( UART8_RX_TX_IRQn, UART8_PRIORITY, _Usart8BinaryHandler);
         CpuIrqAttach( UART8_ERR_IRQn,   UART8_PRIORITY, _Usart8BinaryHandler);
         CpuIrqEnable( UART8_RX_TX_IRQn);
         CpuIrqEnable( UART8_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 10
      case UART_UART9 :
         CpuIrqAttach( UART9_RX_TX_IRQn, UART9_PRIORITY, _Usart9BinaryHandler);
         CpuIrqAttach( UART9_ERR_IRQn,   UART9_PRIORITY, _Usart9BinaryHandler);
         CpuIrqEnable( UART9_RX_TX_IRQn);
         CpuIrqEnable( UART9_ERR_IRQn);
         break;
#endif
      default :
         break;
   }
   InterruptEnable();
} // _UartBinaryModeSet

//------------------------------------------------------------------------------
//   IRQ Handler
//------------------------------------------------------------------------------

static void __irq _Usart0BinaryHandler( void)
// USART0 binary handler
{
   _UsartBinaryHandler( UartGet(  UART_UART0));
} // _Usart0BinaryHandler

static void __irq _Usart1BinaryHandler( void)
// USART1 binary handler
{
   _UsartBinaryHandler( UartGet(  UART_UART1));
} // _Usart1BinaryHandler

static void __irq _Usart2BinaryHandler( void)
// USART2 binary handler
{
   _UsartBinaryHandler( UartGet(  UART_UART2));
} // _Usart2BinaryHandler

static void __irq _Usart3BinaryHandler( void)
// USART3 binary handler
{
   _UsartBinaryHandler( UartGet(  UART_UART3));
} // _Usart3BinaryHandler

static void __irq _Usart4BinaryHandler( void)
// USART4 binary handler
{
   _UsartBinaryHandler( UartGet(  UART_UART4));
} // _Usart4BinaryHandler

static void __irq _Usart5BinaryHandler( void)
// USART5 binary handler
{
   _UsartBinaryHandler( UartGet(  UART_UART5));
} // _Usart5BinaryHandler

static void __irq _Usart6BinaryHandler( void)
// USART6 binary handler
{
   _UsartBinaryHandler( UartGet(  UART_UART6));
} // _Usar

static void __irq _Usart7BinaryHandler( void)
// USART7 binary handler
{
   _UsartBinaryHandler( UartGet(  UART_UART7));
} // _Usar

static void __irq _Usart8BinaryHandler( void)
// USART8 binary handler
{
   _UsartBinaryHandler( UartGet(  UART_UART8));
} // _Usar

static void __irq _Usart9BinaryHandler( void)
// USART9 binary handler
{
   _UsartBinaryHandler( UartGet(  UART_UART9));
} // _Usar


//------------------------------------------------------------------------------
//   Common binary Handler
//------------------------------------------------------------------------------

static void _UsartBinaryHandler( const TUartDescriptor *Uart)
// Common binary handler
{
TUartData *UData;
TUsart    *Usart;
byte       ch;
unsigned   Status;

   Usart = Uart->Usart;
   UData = Uart->Data;
   Status = usartStatus( Usart);
   // check for Rx errors :
   if( usartStatusRxError( Status)){
      if( UData->Status == UART_RECEIVE_ACTIVE){
         if( usartStatusRxFraming( Status)){
            UData->Status = UART_RECEIVE_FRAMING_ERROR;
            timerClear( Uart);
            usartRxInterruptDisable( Usart);
         }
         if( usartStatusRxOverrun( Status)){
            UData->Status = UART_RECEIVE_OVERRUN_ERROR;
            timerClear( Uart);
            usartRxInterruptDisable( Usart);
         }
      }
      usartStatusReset( Usart);
   }
   // check for Rx data :
   if( usartStatusRx( Status)){
      ch = usartRxData( Usart);
      if( UData->Status == UART_RECEIVE_ACTIVE){
         //erase parity bit from data when parity enabled
         if( Usart->C1 & UART_C1_PE_MASK && !(Usart->C1 & UART_C1_M_MASK)){
            // 8th bit is parity
            ch &= ~0x80;
         }
         if( UData->Status == UART_RECEIVE_ACTIVE){
            UData->ReceiveBuffer[ UData->Index++] = ch;
            UData->Size++;
            timerRestart( Uart);                    // restart intercharacter timeout
            if( UData->Index == UData->ReceiveSizeMax){
               UData->Status = UART_RECEIVE_SIZE_ERROR;
               timerClear( Uart);
               usartRxInterruptDisable( Usart);
            }
         }
      }
   }
   // check for Tx data :
   if( usartStatusTx( Status)){
      if( UData->Status == UART_SEND_ACTIVE){
         usartTxData( Usart, UData->SendBuffer[ UData->Index++]);
         if( UData->Index == UData->Size){
            usartTxInterruptDisable( Usart);     // stop Tx
            usartTxDoneInterruptEnable( Usart);  // wait for Tx done
         }
      }
   }
   // check for Tx done :
   if( usartStatusTxDone( Status)){
      if( UData->Status == UART_SEND_ACTIVE){
         UData->Status = UART_SEND_DONE;
         UartTxDisable( Uart);                   // RS485 direction to Rx
         usartTxDoneInterruptDisable( Usart);
         _UartReceiveStart( Uart);               // start receiving
      }
   }
} // _UsartBinaryHandler
