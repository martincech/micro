//*****************************************************************************
//
//    UartNative.h   UART native local functions
//    Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

static void _UartNativeModeSet( TUartAddress Uart, TUartMode Mode);
// Set native mode

static void __irq _Usart0NativeHandler( void);
// USART0 Native handler

static void __irq _Usart1NativeHandler( void);
// USART1 Native handler

static void __irq _Usart2NativeHandler( void);
// USART2 Native handler

static void __irq _Usart3NativeHandler( void);
// USART3 Native handler

static void __irq _Usart4NativeHandler( void);
// USART4 Native handler

static void __irq _Usart5NativeHandler( void);
// USART5 Native handler

static void __irq _Usart6NativeHandler( void);
// USART6 Native handler

static void __irq _Usart7NativeHandler( void);
// USART7 Native handler

static void __irq _Usart8NativeHandler( void);
// USART8 Native handler

static void __irq _Usart9NativeHandler( void);
// USART9 Native handler


static void _UsartNativeHandler( const TUartDescriptor *Uart);
// Common native handler
