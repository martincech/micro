//*****************************************************************************
//
//    UartAscii.h    UART ASCII local functions
//    Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

static void _UartAsciiModeSet( TUartAddress Uart, TUartMode Mode);
// Set ASCII mode

static void __irq _Usart0AsciiHandler( void);
// USART0 Ascii handler

static void __irq _Usart1AsciiHandler( void);
// USART1 Ascii handler

static void __irq _Usart2AsciiHandler( void);
// USART2 Ascii handler

static void __irq _Usart3AsciiHandler( void);
// USART3 Ascii handler

static void __irq _Usart4AsciiHandler( void);
// USART4 Ascii handler

static void __irq _Usart5AsciiHandler( void);
// USART5 Ascii handler

static void __irq _Usart6AsciiHandler( void);
// USART6 Ascii handler

static void __irq _Usart7AsciiHandler( void);
// USART7 Ascii handler

static void __irq _Usart8AsciiHandler( void);
// USART8 Ascii handler

static void __irq _Usart9AsciiHandler( void);
// USART9 Ascii handler

static void _UsartAsciiHandler( const TUartDescriptor *Uart);
// Common ASCII handler
