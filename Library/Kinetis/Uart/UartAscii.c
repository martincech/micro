//*****************************************************************************
//
//    UartAscii.c  UART ASCII local functions
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

//------------------------------------------------------------------------------
//   Mode
//------------------------------------------------------------------------------

static void _UartAsciiModeSet( TUartAddress Address, TUartMode Mode)
// Set Ascii mode
{
const TUartDescriptor *Uart = UartGet( Address);

   usartInterruptDisable( UsartGet(Address));
   InterruptDisable();
   switch( Address){
#if UART_PORTS_COUNT >= 1
      case UART_UART0 :
         CpuIrqAttach( UART0_RX_TX_IRQn, UART0_PRIORITY, _Usart0AsciiHandler);
         CpuIrqAttach( UART0_ERR_IRQn,   UART0_PRIORITY, _Usart0AsciiHandler);
         CpuIrqEnable( UART0_RX_TX_IRQn);
         CpuIrqEnable( UART0_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 2
      case UART_UART1 :
         CpuIrqAttach( UART1_RX_TX_IRQn, UART1_PRIORITY, _Usart1AsciiHandler);
         CpuIrqAttach( UART1_ERR_IRQn,   UART1_PRIORITY, _Usart1AsciiHandler);
         CpuIrqEnable( UART1_RX_TX_IRQn);
         CpuIrqEnable( UART1_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 3
      case UART_UART2 :
         CpuIrqAttach( UART2_RX_TX_IRQn, UART2_PRIORITY, _Usart2AsciiHandler);
         CpuIrqAttach( UART2_ERR_IRQn,   UART2_PRIORITY, _Usart2AsciiHandler);
         CpuIrqEnable( UART2_RX_TX_IRQn);
         CpuIrqEnable( UART2_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 4
      case UART_UART3 :
         CpuIrqAttach( UART3_RX_TX_IRQn, UART3_PRIORITY, _Usart3AsciiHandler);
         CpuIrqAttach( UART3_ERR_IRQn,   UART3_PRIORITY, _Usart3AsciiHandler);
         CpuIrqEnable( UART3_RX_TX_IRQn);
         CpuIrqEnable( UART3_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 5
      case UART_UART4 :
         CpuIrqAttach( UART4_RX_TX_IRQn, UART4_PRIORITY, _Usart4AsciiHandler);
         CpuIrqAttach( UART4_ERR_IRQn,   UART4_PRIORITY, _Usart4AsciiHandler);
         CpuIrqEnable( UART4_RX_TX_IRQn);
         CpuIrqEnable( UART4_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 6
      case UART_UART5 :
         CpuIrqAttach( UART5_RX_TX_IRQn, UART5_PRIORITY, _Usart5AsciiHandler);
         CpuIrqAttach( UART5_ERR_IRQn,   UART5_PRIORITY, _Usart5AsciiHandler);
         CpuIrqEnable( UART5_RX_TX_IRQn);
         CpuIrqEnable( UART5_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 7
      case UART_UART6 :
         CpuIrqAttach( UART6_RX_TX_IRQn, UART6_PRIORITY, _Usart6AsciiHandler);
         CpuIrqAttach( UART6_ERR_IRQn,   UART6_PRIORITY, _Usart6AsciiHandler);
         CpuIrqEnable( UART6_RX_TX_IRQn);
         CpuIrqEnable( UART6_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 8
      case UART_UART7 :
         CpuIrqAttach( UART7_RX_TX_IRQn, UART7_PRIORITY, _Usart7AsciiHandler);
         CpuIrqAttach( UART7_ERR_IRQn,   UART7_PRIORITY, _Usart7AsciiHandler);
         CpuIrqEnable( UART7_RX_TX_IRQn);
         CpuIrqEnable( UART7_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 9
      case UART_UART8 :
         CpuIrqAttach( UART8_RX_TX_IRQn, UART8_PRIORITY, _Usart8AsciiHandler);
         CpuIrqAttach( UART8_ERR_IRQn,   UART8_PRIORITY, _Usart8AsciiHandler);
         CpuIrqEnable( UART8_RX_TX_IRQn);
         CpuIrqEnable( UART8_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 10
      case UART_UART9 :
         CpuIrqAttach( UART9_RX_TX_IRQn, UART9_PRIORITY, _Usart9AsciiHandler);
         CpuIrqAttach( UART9_ERR_IRQn,   UART9_PRIORITY, _Usart9AsciiHandler);
         CpuIrqEnable( UART9_RX_TX_IRQn);
         CpuIrqEnable( UART9_ERR_IRQn);
         break;
#endif
      default :
         break;
   }
   InterruptEnable();
} // _UartAsciiModeSet

//------------------------------------------------------------------------------
//   IRQ Handler
//------------------------------------------------------------------------------

static void __irq _Usart0AsciiHandler( void)
// USART0 Ascii handler
{
   _UsartAsciiHandler( UartGet(  UART_UART0));
} // _Usart0AsciiHandler

static void __irq _Usart1AsciiHandler( void)
// USART1 Ascii handler
{
   _UsartAsciiHandler( UartGet(  UART_UART1));
} // _Usart1AsciiHandler

static void __irq _Usart2AsciiHandler( void)
// USART2 Ascii handler
{
   _UsartAsciiHandler( UartGet(  UART_UART2));
} // _Usart2AsciiHandler

static void __irq _Usart3AsciiHandler( void)
// USART3 Ascii handler
{
   _UsartAsciiHandler( UartGet(  UART_UART3));
} // _Usart3AsciiHandler

static void __irq _Usart4AsciiHandler( void)
// USART4 Ascii handler
{
   _UsartAsciiHandler( UartGet(  UART_UART4));
} // _Usart4AsciiHandler

static void __irq _Usart5AsciiHandler( void)
// USART5 Ascii handler
{
   _UsartAsciiHandler( UartGet(  UART_UART5));
} // _Usart5AsciiHandler

static void __irq _Usart6AsciiHandler( void)
// USART6 Ascii handler
{
   _UsartAsciiHandler( UartGet(  UART_UART6));
} // _Usar

static void __irq _Usart7AsciiHandler( void)
// USART7 Ascii handler
{
   _UsartAsciiHandler( UartGet(  UART_UART7));
} // _Usar

static void __irq _Usart8AsciiHandler( void)
// USART8 Ascii handler
{
   _UsartAsciiHandler( UartGet(  UART_UART8));
} // _Usar

static void __irq _Usart9AsciiHandler( void)
// USART9 Ascii handler
{
   _UsartAsciiHandler( UartGet(  UART_UART9));
} // _Usar

//------------------------------------------------------------------------------
//   Common ASCII Handler
//------------------------------------------------------------------------------

static void _UsartAsciiHandler( const TUartDescriptor *Uart)
// Common ASCII handler
{
TUartData   *UData;
TUsart      *Usart;
byte        Data;
static byte TrailerIndex = 0;
static byte LeaderIndex = 0;
byte i;
word        Index;
unsigned    Status;

   Status = usartStatus( Uart->Usart);
//------------------------------------------------------------------------------
   // check for Rx errors :
   if( usartStatusRxError( Status)){
      if( Uart->Data->Status == UART_RECEIVE_ACTIVE){
         if( usartStatusRxFraming( Status)){
            RxSetStatusAndStop( Uart, UART_RECEIVE_FRAMING_ERROR);
         }
         if( usartStatusRxOverrun( Status)){
            RxSetStatusAndStop( Uart, UART_RECEIVE_OVERRUN_ERROR);
         }
         usartStatusReset( Uart->Usart);               // clear status
         return;                                 // stop on Rx error
      }
      usartStatusReset( Uart->Usart);
   }
//------------------------------------------------------------------------------
   // check for Rx data :
   if( usartStatusRx( Status)){
      Data = usartRxData( Uart->Usart);
      if( Uart->Data->Status == UART_RECEIVE_ACTIVE){
         //erase parity bit from data when parity enabled
         if( Usart->C1 & UART_C1_PE_MASK && !(Usart->C1 & UART_C1_M_MASK)){
            // 8th bit is parity
            Data &= ~0x80;
         }
         timerRestart( Uart);                    // restart intercharacter timeout
         while(1){
            Index = UData->Index - 1;
            if( UData->Index == 0){
               // waiting for leader
               if( Data == UData->Leader[LeaderIndex]){
                  LeaderIndex++;
                  if(LeaderIndex == UData->LeaderLen){
                     UData->Index++;
                     TrailerIndex = 0;
                     LeaderIndex = 0;
                  }
               } else {
                  LeaderIndex = 0;
               }
            } else if( Index >= UData->ReceiveSizeMax){
               // data size overrun
               RxSetStatusAndStop( Uart, UART_RECEIVE_SIZE_ERROR);
            } else if ( TrailerIndex){
               if( Data == UData->Trailer[TrailerIndex]){
                  if( TrailerIndex == UData->TrailerLen - 1){
                     // frame complete - n trailers
                     UData->Size   = Index;
                     RxSetStatusAndStop( Uart, UART_RECEIVE_FRAME);
                  } else {
                     TrailerIndex++;
                  }
               } else if ( Data == UData->Leader[0]){
                  //start again
                  UData->Index = 0;
                  continue;
               } else {
                  // false alert, data back to frame
                  for( i = 0; i < TrailerIndex; i++){
                     UData->ReceiveBuffer[ Index++] = UData->Trailer[ i];
                     if( Index >= UData->ReceiveSizeMax){
                        // data size overrun
                        RxSetStatusAndStop( Uart, UART_RECEIVE_SIZE_ERROR);
                        return;
                     }
                  }
                  UData->ReceiveBuffer[ Index] = Data;
                  UData->Index += TrailerIndex + 1;
                  TrailerIndex = 0;
               }
            } else if( Data == UData->Trailer[0]){
               TrailerIndex = 1;
               if( UData->TrailerLen == TrailerIndex){
                  // frame complete - 1 trailer
                  UData->Size   = Index;
                  RxSetStatusAndStop( Uart, UART_RECEIVE_FRAME);
               }
            } else {
               // data in the frame
               if( Data == UData->Leader[0]){
                  //leader again, new frame, begin again
                  UData->Index = 0;
                  continue;
               }
               UData->ReceiveBuffer[ Index] = Data;
               UData->Index++;
            }
            break;
         }
      }
   }
//------------------------------------------------------------------------------
   // check for Tx data :
   if( usartStatusTx( Status)){
      if( UData->Status == UART_SEND_ACTIVE){
         Index = UData->Index - 1;
         if( UData->Index == 0){
            usartTxData( Usart, UData->Leader[LeaderIndex]);
            LeaderIndex++;
            if(LeaderIndex == UData->LeaderLen){
               UData->Index++;
               LeaderIndex = 0;
               TrailerIndex = 0;
            }
         } else if( Index < UData->Size){
            usartTxData( Usart, UData->SendBuffer[ Index]);
         } else {
            usartTxData( Usart, UData->Trailer[ TrailerIndex]);
            TrailerIndex++;
            if( UData->TrailerLen == TrailerIndex){
               usartTxInterruptDisable( Usart);     // stop Tx
               usartTxDoneInterruptEnable( Usart);  // wait for Tx done
            }
         }
         Uart->Data->Index++;
      }
   }
//------------------------------------------------------------------------------
   // check for Tx done :
   if( usartStatusTxDone( Status)){
      if( Uart->Data->Status == UART_SEND_ACTIVE){
         Uart->Data->Status = UART_SEND_DONE;
         UartTxDisable( Uart);                   // RS485 direction to Rx
         usartTxDoneInterruptDisable( Uart->Usart);
         _UartReceiveStart( Uart);               // start receiving
      }
   }
} // _UsartAsciiHandler
