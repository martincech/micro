//*****************************************************************************
//
//    UartNative.c UART native local functions
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Uart/NativeDef.h"       // Native protocol definitions

//------------------------------------------------------------------------------
//   Mode
//------------------------------------------------------------------------------

static void _UartNativeModeSet( TUartAddress Address, TUartMode Mode)
// Set Native mode
{
const TUartDescriptor *Uart = UartGet( Address);

   usartInterruptDisable( UsartGet(Address));
   InterruptDisable();
   switch( Address){
#if UART_PORTS_COUNT >= 1
      case UART_UART0 :
         CpuIrqAttach( UART0_RX_TX_IRQn, UART0_PRIORITY, _Usart0NativeHandler);
         CpuIrqAttach( UART0_ERR_IRQn,   UART0_PRIORITY, _Usart0NativeHandler);
         CpuIrqEnable( UART0_RX_TX_IRQn);
         CpuIrqEnable( UART0_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 2
      case UART_UART1 :
         CpuIrqAttach( UART1_RX_TX_IRQn, UART1_PRIORITY, _Usart1NativeHandler);
         CpuIrqAttach( UART1_ERR_IRQn,   UART1_PRIORITY, _Usart1NativeHandler);
         CpuIrqEnable( UART1_RX_TX_IRQn);
         CpuIrqEnable( UART1_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 3
      case UART_UART2 :
         CpuIrqAttach( UART2_RX_TX_IRQn, UART2_PRIORITY, _Usart2NativeHandler);
         CpuIrqAttach( UART2_ERR_IRQn,   UART2_PRIORITY, _Usart2NativeHandler);
         CpuIrqEnable( UART2_RX_TX_IRQn);
         CpuIrqEnable( UART2_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 4
      case UART_UART3 :
         CpuIrqAttach( UART3_RX_TX_IRQn, UART3_PRIORITY, _Usart3NativeHandler);
         CpuIrqAttach( UART3_ERR_IRQn,   UART3_PRIORITY, _Usart3NativeHandler);
         CpuIrqEnable( UART3_RX_TX_IRQn);
         CpuIrqEnable( UART3_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 5
      case UART_UART4 :
         CpuIrqAttach( UART4_RX_TX_IRQn, UART4_PRIORITY, _Usart4NativeHandler);
         CpuIrqAttach( UART4_ERR_IRQn,   UART4_PRIORITY, _Usart4NativeHandler);
         CpuIrqEnable( UART4_RX_TX_IRQn);
         CpuIrqEnable( UART4_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 6
      case UART_UART5 :
         CpuIrqAttach( UART5_RX_TX_IRQn, UART5_PRIORITY, _Usart5NativeHandler);
         CpuIrqAttach( UART5_ERR_IRQn,   UART5_PRIORITY, _Usart5NativeHandler);
         CpuIrqEnable( UART5_RX_TX_IRQn);
         CpuIrqEnable( UART5_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 7
      case UART_UART6 :
         CpuIrqAttach( UART6_RX_TX_IRQn, UART6_PRIORITY, _Usart6NativeHandler);
         CpuIrqAttach( UART6_ERR_IRQn,   UART6_PRIORITY, _Usart6NativeHandler);
         CpuIrqEnable( UART6_RX_TX_IRQn);
         CpuIrqEnable( UART6_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 8
      case UART_UART7 :
         CpuIrqAttach( UART7_RX_TX_IRQn, UART7_PRIORITY, _Usart7NativeHandler);
         CpuIrqAttach( UART7_ERR_IRQn,   UART7_PRIORITY, _Usart7NativeHandler);
         CpuIrqEnable( UART7_RX_TX_IRQn);
         CpuIrqEnable( UART7_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 9
      case UART_UART8 :
         CpuIrqAttach( UART8_RX_TX_IRQn, UART8_PRIORITY, _Usart8NativeHandler);
         CpuIrqAttach( UART8_ERR_IRQn,   UART8_PRIORITY, _Usart8NativeHandler);
         CpuIrqEnable( UART8_RX_TX_IRQn);
         CpuIrqEnable( UART8_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 10
      case UART_UART9 :
         CpuIrqAttach( UART9_RX_TX_IRQn, UART9_PRIORITY, _Usart9NativeHandler);
         CpuIrqAttach( UART9_ERR_IRQn,   UART9_PRIORITY, _Usart9NativeHandler);
         CpuIrqEnable( UART9_RX_TX_IRQn);
         CpuIrqEnable( UART9_ERR_IRQn);
         break;
#endif
      default :
         break;
   }
   InterruptEnable();
} // _UartNativeModeSet

//------------------------------------------------------------------------------
//   IRQ Handler
//------------------------------------------------------------------------------

static void __irq _Usart0NativeHandler( void)
// USART0 Native handler
{
   _UsartNativeHandler( UartGet(  UART_UART0));
} // _Usart0NativeHandler

static void __irq _Usart1NativeHandler( void)
// USART1 Native handler
{
   _UsartNativeHandler( UartGet(  UART_UART1));
} // _Usart1NativeHandler

static void __irq _Usart2NativeHandler( void)
// USART2 Native handler
{
   _UsartNativeHandler( UartGet(  UART_UART2));
} // _Usart2NativeHandler

static void __irq _Usart3NativeHandler( void)
// USART3 Native handler
{
   _UsartNativeHandler( UartGet(  UART_UART3));
} // _Usart3NativeHandler

static void __irq _Usart4NativeHandler( void)
// USART4 Native handler
{
   _UsartNativeHandler( UartGet(  UART_UART4));
} // _Usart4NativeHandler

static void __irq _Usart5NativeHandler( void)
// USART5 Native handler
{
   _UsartNativeHandler( UartGet(  UART_UART5));
} // _Usart5NativeHandler

static void __irq _Usart6NativeHandler( void)
// USART6 Native handler
{
   _UsartNativeHandler( UartGet(  UART_UART6));
} // _Usar

static void __irq _Usart7NativeHandler( void)
// USART7 Native handler
{
   _UsartNativeHandler( UartGet(  UART_UART7));
} // _Usar

static void __irq _Usart8NativeHandler( void)
// USART8 Native handler
{
   _UsartNativeHandler( UartGet(  UART_UART8));
} // _Usar

static void __irq _Usart9NativeHandler( void)
// USART9 Native handler
{
   _UsartNativeHandler( UartGet(  UART_UART9));
} // _Usar

//------------------------------------------------------------------------------
//   Common native Handler
//------------------------------------------------------------------------------

static void _UsartNativeHandler( const TUartDescriptor *Uart)
// Common native handler
{
TUartData *UData;
TUsart    *Usart;
byte       Data;
byte       HeaderCrc;
word       Index;
unsigned   Status;

   Usart = Uart->Usart;
   UData = Uart->Data;
   Status = usartStatus( Usart);
   // check for Rx errors :
   if( usartStatusRxError( Status)){
      if( UData->Status == UART_RECEIVE_ACTIVE){
         if( usartStatusRxFraming( Status)){
            UData->Status = UART_RECEIVE_FRAMING_ERROR;
            timerClear( Uart);
            usartRxInterruptDisable( Usart);
         }
         if( usartStatusRxOverrun( Status)){
            UData->Status = UART_RECEIVE_OVERRUN_ERROR;
            timerClear( Uart);
            usartRxInterruptDisable( Usart);
         }
      }
      usartStatusReset( Usart);
   }
//------------------------------------------------------------------------------
   // check for Rx data :
   if( usartStatusRx( Status)){
      Data = usartRxData( Usart);
      if( UData->Status == UART_RECEIVE_ACTIVE){
         //erase parity bit from data when parity enabled
         if( Usart->C1 & UART_C1_PE_MASK && !(Usart->C1 & UART_C1_M_MASK)){
            // 8th bit is parity
            Data &= ~0x80;
         }
         switch( UData->Index){
            case NATIVE_LEADER_OFFSET :
               if( Data != NATIVE_LEADER){
                  return;
               }
               break;

            case NATIVE_ADDRESS_OFFSET :
               UData->Address = Data;
               break;

            case NATIVE_SIZE_OFFSET :
               UData->Size  = (word)Data;
               break;

            case NATIVE_SIZE_OFFSET + 1 :
               UData->Size |= (word)Data << 8;
               if( UData->Size > UData->ReceiveSizeMax){
                  UData->Status = UART_RECEIVE_SIZE_ERROR;
                  timerClear( Uart);                  // disable timeout
                  usartRxInterruptDisable( Usart);    // disable Rx data interrupt
                  return;
               }
               break;

            case NATIVE_HEADER_CRC_OFFSET :
               HeaderCrc  = UData->Address;
               HeaderCrc += UData->Size & 0xFF;
               HeaderCrc += UData->Size >> 8;
               HeaderCrc  = (byte)-HeaderCrc;
               if( Data != HeaderCrc){
                  UData->Status = UART_RECEIVE_HEADER_CRC_ERROR;
                  timerClear( Uart);                  // disable timeout
                  usartRxInterruptDisable( Usart);    // disable Rx data interrupt
                  return;
               }
               UData->CheckSum = 0;                   // clear data checksum
               break;

            default :
               Index = UData->Index - NATIVE_DATA_OFFSET;
               if( Index < UData->Size){
                  UData->ReceiveBuffer[ Index] = Data;
                  UData->CheckSum += Data;
                  break;
               }
               if( Index == UData->Size){
                  // CRC LSB
                  UData->CheckSum = (word)-UData->CheckSum;
                  if( (byte)(UData->CheckSum & 0xFF) != Data){
                     UData->Status = UART_RECEIVE_CRC_ERROR;
                     timerClear( Uart);               // disable timeout
                     usartRxInterruptDisable( Usart); // disable Rx data interrupt
                     return;
                  }
                  break;
               }
               if( Index == UData->Size + 1){
                  // CRC MSB
                  if( (byte)(UData->CheckSum >> 8) != Data){
                     UData->Status = UART_RECEIVE_CRC_ERROR;
                     timerClear( Uart);               // disable timeout
                     usartRxInterruptDisable( Usart); // disable Rx data interrupt
                     return;
                  }
               }
               UData->Status = UART_RECEIVE_FRAME;
               timerClear( Uart);                     // disable timeout
               usartRxInterruptDisable( Usart);       // disable Rx data interrupt
               return;
         }
         timerRestart( Uart);                         // start intercharacter timeout
         UData->Index++;                              // wait for next character
      }
   }
//------------------------------------------------------------------------------
   // check for Tx data :
   if( usartStatusTx( Status)){
      if( UData->Status == UART_SEND_ACTIVE){
         switch( UData->Index){
            case NATIVE_LEADER_OFFSET :
               usartTxData( Usart, NATIVE_LEADER);
               break;

            case NATIVE_ADDRESS_OFFSET :
               usartTxData( Usart, UData->SendAddress);
               break;

            case NATIVE_SIZE_OFFSET :
               usartTxData( Usart, (byte)(UData->Size & 0xFF));
               break;

            case NATIVE_SIZE_OFFSET + 1 :
               usartTxData( Usart, (byte)(UData->Size >> 8));
               break;

            case NATIVE_HEADER_CRC_OFFSET :
               HeaderCrc  = UData->SendAddress;
               HeaderCrc += UData->Size & 0xFF;
               HeaderCrc += UData->Size >> 8;
               HeaderCrc  = (byte)-HeaderCrc;
               usartTxData( Usart, HeaderCrc);
               UData->CheckSum = 0;                   // clear data checksum
               break;

            default :
               Index = UData->Index - NATIVE_DATA_OFFSET;
               if( Index < UData->Size){
                  // send data
                  Data = UData->SendBuffer[ Index];
                  usartTxData( Usart, Data);
                  UData->CheckSum += Data;
                  break;
               }
               if( Index == UData->Size){
                  // CRC LSB
                  UData->CheckSum = (word)-UData->CheckSum;
                  usartTxData( Usart, (byte)(UData->CheckSum & 0xFF));
                  break;
               }
               if( Index == UData->Size + 1){
                  // CRC MSB
                  usartTxData( Usart, (byte)(UData->CheckSum >> 8));
                  break;
               }
               usartTxInterruptDisable( Usart);     // stop Tx
               usartTxDoneInterruptEnable( Usart);  // wait for Tx done
               break;
         }
         UData->Index++;
      }
   }
   // check for Tx done :
   if( usartStatusTxDone( Status)){
      if( UData->Status == UART_SEND_ACTIVE){
         UData->Status = UART_SEND_DONE;
         UartTxDisable( Uart);                   // RS485 direction to Rx
         usartTxDoneInterruptDisable( Usart);
         _UartReceiveStart( Uart);               // start receiving
      }
   }
} // _UsartNativeHandler
