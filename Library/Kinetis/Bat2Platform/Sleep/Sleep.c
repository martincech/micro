//******************************************************************************
//
//   Sleep.c         Bat2 Low power controller
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "Sleep/Sleep.h"
#include "Hardware.h"
#include "Cpu/Cpu.h"
#include "Usb/Usb.h"
#include "Ads1232/Ads1232.h"

#define K_SLEEP_WAKE_ADC  (int)0xFFFFFFFF
#define LPTMR_PRESCALE        2048         // change prescale also in LPTMR0->PSR
#define TIMER_MAX    (1000 * (2 * LPTMR_PRESCALE - 1))

static int Key;
static word Timer;
static TYesNo Enable;

static void LpTimerEnable( void);
// Low power timer enable

static void LpTimerDisable( void);
// Low power timer disable

void __irq LPTimer_IRQHandler( void);
// Low power timer handler

//-----------------------------------------------------------------------------
// Init
//-----------------------------------------------------------------------------

void SleepInit( void)
// Init
{
   #warning PMPROT is write once register. It is not reset by VLLS reset
   SMC->PMPROT |= SMC_PMPROT_AVLP_MASK;
   Timer = TIMER_WAKE_UP_PERIOD;
   Enable = NO;
} // LpcInit

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

int SleepScheduler( void)
// Sleep scheduler
{
   if(!Enable) {
      return K_IDLE;
   }

   forever {
      Key = K_SLEEP_WAKE_ADC;
      SMC->PMCTRL = SMC_PMCTRL_STOPM(0x2);
      LPTMR0->CSR |= LPTMR_CSR_TCF_MASK;
      Stop();
      break;

      if(Key != K_SLEEP_WAKE_ADC) {
         break;
      }
   }

   return K_IDLE;
} // SleepScheduler

//-----------------------------------------------------------------------------
// Timer set
//-----------------------------------------------------------------------------

void SleepTimerSet(word _Timer)
// Set wake up period
{
   Timer = _Timer;
} // SleepTimerSet

//-----------------------------------------------------------------------------
// Low power enable
//-----------------------------------------------------------------------------

void SleepEnable( TYesNo _Enable)
// <Enable> low power mode
{
#warning Vyresit lepe pripad, kdy neni Sleep pouzito
#ifndef SLEEP_ENABLE
   return;
#endif
   if(Enable == _Enable) {
      return;
   }
   Enable = _Enable;
   if(!Enable) {
      LpTimerDisable();
   } else {
      LpTimerEnable();
   }
} // LpcLowPowerEnable

//******************************************************************************

//-----------------------------------------------------------------------------
// Low power timer
//-----------------------------------------------------------------------------

static void LpTimerEnable( void)
// Low power timer enable
{
word _Timer;

  _Timer = Timer;
   if(_Timer > TIMER_MAX) {
      _Timer = TIMER_MAX;
   }
   if(_Timer == 0) {
      _Timer = 1;
   }
   LpTimerClockEnable();

   LPTMR0->CSR = 0; // reset
   LPTMR0->PSR = LPTMR_PSR_PRESCALE(0xA) | LPTMR_PSR_PCS(0); // MCGIRCLK as source for LPTMR
   LPTMR0->CMR = F_MCGIRCLK * _Timer / LPTMR_PRESCALE / 1000;
   LPTMR0->CSR |= LPTMR_CSR_TCF_MASK; // clear flag
   CpuIrqAttach(LPTimer_IRQn, ADC_INTERRUPT_PRIORITY +1, LPTimer_IRQHandler);
   CpuIrqEnable(LPTimer_IRQn);
   LPTMR0->CSR = LPTMR_CSR_TIE_MASK | LPTMR_CSR_TEN_MASK;
} // LpTimerEnable

static void LpTimerDisable( void)
// Low power timer disable
{
   CpuIrqDisable(LPTimer_IRQn);
   LpTimerClockEnable();
   LPTMR0->CSR |= LPTMR_CSR_TCF_MASK;
   LPTMR0->CSR = 0;
   LpTimerClockDisable();
} // LpTimerDisable

void __irq LPTimer_IRQHandler( void)
// Low power timer handler
{
   Key = K_TIMER_SLOW;
   LPTMR0->CSR |= LPTMR_CSR_TCF_MASK; // clear flag
} // Handler