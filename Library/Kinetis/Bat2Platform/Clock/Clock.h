//******************************************************************************
//
//   Clock.h         Bat2 clock
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef _Uni_H__
   #include "Unisys/Uni.h"
#endif

#include "Hardware.h"

#define F_MCGIRCLK      F_FAST_INTERNAL

#define F_UART0         F_MCGIRCLK

void ClockInit( void);
// Init clock