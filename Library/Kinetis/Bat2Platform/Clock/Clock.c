//******************************************************************************
//
//   Clock.c         Bat2 clock
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "Clock.h"

//------------------------------------------------------------------------------
//  Init
//------------------------------------------------------------------------------

void ClockInit( void)
// Init clock
{
   MCG->C1 |= MCG_C1_IRCLKEN_MASK | MCG_C1_IREFSTEN_MASK;
   MCG->C2 |= MCG_C2_IRCS_MASK;

   MCG->SC &= ~MCG_SC_FCRDIV_MASK;



   /*MCG->C2 |= MCG_C2_FCFTRIM_MASK;
   MCG->C4 |= MCG_C4_FCTRIM_MASK;
   // 14 706*/

   /*MCG->C2 &= ~MCG_C2_FCFTRIM_MASK;
   MCG->C4 &= ~MCG_C4_FCTRIM_MASK;
   // 26 316*/

} // ClockInit
