//******************************************************************************
//
//   Clock.c         Bat2 clock
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "Hardware.h"
#include "Clock/Pll.h"

#define PLL_PREDIVIDER    (F_CRYSTAL / 2000000)
#define PLL_MULTIPLIER    (F_USB / 2000000)

//------------------------------------------------------------------------------
//  Init
//------------------------------------------------------------------------------

void ClockInit( void)
// Init clock
{
} // ClockInit

//------------------------------------------------------------------------------
//  Usb clock start
//------------------------------------------------------------------------------

void ClockUsbFsStart( void)
// Start USB clock
{
   //OSC->CR = OSC_CR_SC2P_MASK | OSC_CR_SC4P_MASK | OSC_CR_SC8P_MASK | OSC_CR_SC16P_MASK;
	PllInit(OSCINIT,   // Initialize the oscillator circuit
			F_CRYSTAL,  // CLKIN0 frequency
			LOW_POWER,     // Set the oscillator for low power mode
			CRYSTAL,     // Crystal or canned oscillator clock input
			PLL_PREDIVIDER,    // PLL predivider value
			PLL_MULTIPLIER,     // PLL multiplier
			PLL_ONLY);
} // UsbFsClockStart

//------------------------------------------------------------------------------
//  Usb clock stop
//------------------------------------------------------------------------------

void ClockUsbFsStop( void)
// Start USB clock
{
   PllShutdown();
} // UsbFsClockStop
