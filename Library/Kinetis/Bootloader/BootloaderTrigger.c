//******************************************************************************
//
//    BootloaderTrigger.c   Bootloader trigger
//    Version 1.0           (c) VEIT Electronics
//
//******************************************************************************

#include "Bootloader.h"
#include "Hardware.h"
#include "Cpu/Cpu.h"

volatile TBootloaderData BootloaderData __attribute__ ((section (".bootloader_data")));;

void Reset( void) {
   BootloaderData.Cmd = BOOT_CMD_APP;
   CpuReset();
}

//-----------------------------------------------------------------------------
// Trigger
//-----------------------------------------------------------------------------

void BootloaderTrigger( void)
// Trigger bootloader
{
   BootloaderData.Cmd = BOOT_CMD_BOOTLOADER;
   CpuReset();
} // BootloaderTrigger