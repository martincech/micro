//******************************************************************************
//
//    HidBootloader.c     HID bootloader
//    Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#include "Bootloader.h"
#include "Memory/Nvm.h"
#include "Memory/NvmLayout.h"
#include "Remote/Rc.h"
#include "Remote/Socket.h"
#include "Remote/SocketIfBootloaderMsd.h"
#include "Usb/UsbModule.h"
#include "Fw/FwStorage.h"

TRcCmd Cmd;
#define SOCKET 0

static byte State;

void _Terminate( void) {
   SocketIfBootloaderMsdClose( SOCKET);
   UsbModuleSwitchOff();
}

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void BootloaderInit(void)
// Initialisation
{
   UsbModuleInit();
   UsbModuleSwitchOn();
   if(!SocketIfBootloaderMsdOpenForReceiveCmd()) {
      State = BOOTLOADER_ERROR;
      return;
   }
   FwStorageInvalidate();
   State = BOOTLOADER_BUSY;
} // HidBootloaderInit

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

void BootloaderExecute( void)
// Execute
{
int CmdSize;
TYesNo Success;

   if(State != BOOTLOADER_BUSY) {
      return;
   }

   if(!SocketIfBootloaderMsdReceive(SOCKET, &Cmd, sizeof(Cmd))) {
      _Terminate();
      State = BOOTLOADER_ERROR;
      return;
   }

   CmdSize = SocketIfBootloaderMsdReceiveSize( SOCKET);

   if(CmdSize == 0) {
      _Terminate();
      State = BOOTLOADER_DONE;
      return;
   }

         Success = NO;
         switch(Cmd.Cmd) {
            case RC_CMD_FILE_SAVE:
               if(CmdSize != RcCmdFileSaveSize( Cmd.Data.FileSave.Size)) {
                  break;
               }
               Success = FwStorageWrite( Cmd.Data.FileSave.Address, Cmd.Data.FileSave.Data, Cmd.Data.FileSave.Size);
               break;

            default:
               Success = YES;
               break;
         }

         if(!Success) {
            _Terminate();
            State = BOOTLOADER_ERROR;
            return;
         }
} // BootloaderExecute

//-----------------------------------------------------------------------------
// State
//-----------------------------------------------------------------------------

int BootloaderState( void)
// State
{
   return State;
} // BootloaderState




