//*****************************************************************************
//
//    EthPhy.h      Ethernet PHY
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "EthPhy.h"
#include "fnet_fec.h"

typedef struct {
   const byte reserved               : 7;
         byte CollisionTest          : 1;
         byte DuplexMode             : 1;
         byte RestartAutoNegotiation : 1;
         byte Isolate                : 1;
         byte PowerDown              : 1;
         byte AutoNegotiationEnable  : 1;
         byte SpeedSelect            : 1;
         byte Loopback               : 1;
         byte Reset                  : 1;
} TBasicControl;

#define BASIC_CONTROL_DUPLEX_MODE_HALF_DUPLEX     0
#define BASIC_CONTROL_DUPLEX_MODE_FULL_DUPLEX     1

#define BASIC_CONTROL_SPEED_SELECT_100MBPS        1
#define BASIC_CONTROL_SPEED_SELECT_10MBPS         0


typedef struct {
   const byte ExtendedCapability     : 1;
   const byte JabberDetect           : 1;
   const byte LinkStatus             : 1;
   const byte AutonegotiationAbility : 1;
   const byte RemoteFault            : 1;
   const byte AutonegotiationComplete: 1;
   const byte NoPreamble             : 1;
   const byte reserved               : 4;
   const byte _10BaseTHalfDuplex     : 1;
   const byte _10BaseTFullDuplex     : 1;
   const byte _100BaseTHalfDuplex    : 1;
   const byte _100BaseTFullDuplex    : 1;
   const byte T4Capable              : 1;
} TBasicStatus;


#define BASIC_STATUS_LINK_STATUS_LINK_IS_UP      1
#define BASIC_STATUS_LINK_STATUS_LINK_IS_DOWN    0

typedef struct {
   const word   PhyIdNumber;
} TPhyIdentifier1;

typedef struct {
   const byte   RevisionNumber         : 4;
   const byte   ModelNumber            : 6;
   const byte   PhyIdNumber            : 6;
} TPhyIdentifier2;


typedef struct {
   TBasicControl   BasicControl;
   TBasicStatus    BasicStatus;
   TPhyIdentifier1 PhyIdentifier1;
   TPhyIdentifier2 PhyIdentifier2;
} TEthPhyMap;

#define SetZero( Register)             {word *Reg = (word *) &Register; *Reg = 0;}
#define AddressOf( Register)           (offsetof( TEthPhyMap, Register) / sizeof(word))

static fnet_fec_if_t *ethif;

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void EthPhyInit( void *EthIf)
// Initialisation
{
   ethif = (fnet_fec_if_t *) EthIf;
} // EthPhyInit

//-----------------------------------------------------------------------------
// Reset
//-----------------------------------------------------------------------------

TYesNo EthPhyReset( void)
// Reset
{
TBasicControl BasicControl;
TPhyIdentifier2 PhyIdentifier2;
word *Data;
   Data = (word *) &BasicControl;

   SetZero(BasicControl);
   BasicControl.Reset = YES;
   fnet_fec_mii_write(ethif, AddressOf( BasicControl), *Data);

   SetZero(BasicControl);
   BasicControl.DuplexMode = BASIC_CONTROL_DUPLEX_MODE_FULL_DUPLEX;
   BasicControl.RestartAutoNegotiation = YES;
   BasicControl.AutoNegotiationEnable = YES;
   fnet_fec_mii_write(ethif, AddressOf( BasicControl), *Data);
   
   
   if(fnet_fec_mii_read(ethif, AddressOf( PhyIdentifier2), (word *)&PhyIdentifier2) != FNET_OK) {
      return NO;
   }
   if( PhyIdentifier2.ModelNumber != 0b010110) {
      return NO;
   }
   return YES;
} // EthPhyReset