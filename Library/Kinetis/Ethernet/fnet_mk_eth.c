/**************************************************************************
*
* Copyright 2005-2011 by Andrey Butok. Freescale Semiconductor, Inc.
*
***************************************************************************
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License Version 3 
* or later (the "LGPL").
*
* As a special exception, the copyright holders of the FNET project give you
* permission to link the FNET sources with independent modules to produce an
* executable, regardless of the license terms of these independent modules,
* and to copy and distribute the resulting executable under terms of your 
* choice, provided that you also meet, for each linked independent module,
* the terms and conditions of the license of that module.
* An independent module is a module which is not derived from or based 
* on this library. 
* If you modify the FNET sources, you may extend this exception 
* to your version of the FNET sources, but you are not obligated 
* to do so. If you do not wish to do so, delete this
* exception statement from your version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*
* You should have received a copy of the GNU General Public License
* and the GNU Lesser General Public License along with this program.
* If not, see <http://www.gnu.org/licenses/>.
*
**********************************************************************/ /*!
*
* @file fnet_mk_eth.c
*
* @author Andrey Butok
*
* @date Aug-2-2012
*
* @version 0.1.9.0
*
* @brief Ethernet driver interafce.
*
***************************************************************************/

#include "fnet_config.h"
#if FNET_MK && (FNET_CFG_CPU_ETH0 ||FNET_CFG_CPU_ETH1)

#include "fnet_fec.h"
#include "Hardware.h"
#include "System/Delay.h"

/************************************************************************
* Ethernet interface structure.
*************************************************************************/
fnet_eth_if_t fnet_mk_eth0_if =
{
    &fnet_fec0_if               /* Points to CPU-specific control data structure of the interface. */
    ,0
    ,fnet_fec_output
#if FNET_CFG_MULTICAST
    ,      
    fnet_fec_multicast_join,
    fnet_fec_multicast_leave,
#endif /* FNET_CFG_MULTICAST */     
};

fnet_netif_t fnet_eth0_if =
{
   0,                          /* Pointer to the next net_if structure.*/
   0,                          /* Pointer to the previous net_if structure.*/
   "eth0",                     /* Network interface name.*/
   FNET_CFG_CPU_ETH0_MTU,      /* Maximum transmission unit.*/
   &fnet_mk_eth0_if,           /* Points to interface specific data structure.*/
   &fnet_fec_api               /* Interface API */
};

/************************************************************************
* NAME: fnet_eth_io_init
*
* DESCRIPTION: Ethernet IO initialization.
*************************************************************************/
void fnet_eth_io_init() 
{
   PowerEthEn();
   SysDelay(100);
   EthPortInit();
   Eth0ClockEnable();
   SysDelay(1);
    /*FSL: allow concurrent access to MPU controller. Example: ENET uDMA to SRAM, otherwise bus error*/
    FNET_MK_MPU_CESR = 0;  /* MPU is disabled. All accesses from all bus masters are allowed.*/
}

/************************************************************************
* NAME: fnet_eth_io_release
*
* DESCRIPTION: Ethernet IO deinitialization.
*************************************************************************/

void fnet_eth_io_release()
{
   EthPortDeinit();
   PowerEthOff();
   Eth0ClockDisable();
}

/************************************************************************
* NAME: fnet_eth_phy_init
*
* DESCRIPTION: Ethernet Physical Transceiver initialization and/or reset.
*************************************************************************/

#include "Ethernet/EthPhy.h"

int fnet_eth_phy_init(fnet_fec_if_t *ethif) 
{
   EthPhyInit( ethif);
   if(!EthPhyReset()) {
      return FNET_ERR;
   }
   return FNET_OK;
}

/************************************************************************
* NAME: fnet_eth_phy_init
*
* DESCRIPTION: Ethernet Physical Transceiver turning off.
*************************************************************************/
void fnet_eth_phy_release()
{
   
}


#endif /* FNET_MK && FNET_CFG_ETH */



