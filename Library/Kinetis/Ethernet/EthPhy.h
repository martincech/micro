//*****************************************************************************
//
//    EthPhy.h      Ethernet PHY
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __EthPhy_H__
   #define __EthPhy_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void EthPhyInit( void *EthIf);
// Initialisation

TYesNo EthPhyReset( void);
// Reset

#endif
