//*****************************************************************************
//
//    SocketInternal.h   Socket
//    Version 1.0        (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __SocketInternal_H__
   #define __SocketInternal_H__
   
#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

typedef byte   (*TSocketPermission)( TSocket *);
typedef byte   (*TSocketState)( TSocket *);
typedef TYesNo (*TSocketReceive)( TSocket *, void *, int);
typedef int    (*TSocketReceiveSize)( TSocket *);
typedef TYesNo (*TSocketSend)( TSocket *, const void *, int);
typedef void   (*TSocketClose)( TSocket *);
typedef TYesNo (*TSocketListen)( TSocket *);

typedef struct {
   TSocketPermission  Permission;
   TSocketState       State;
   TSocketReceive     Receive;
   TSocketReceiveSize ReceiveSize;
   TSocketSend        Send;
   TSocketClose       Close;
   TSocketListen      Listen;
} TSocketInterface;

#endif