//*****************************************************************************
//
//    SocketIfSms.c         Sms socket interface
//    Version 1.0           (c) VEIT Electronics
//
//*****************************************************************************

#include "Remote/SocketIfSms.h"
#include "Message/MGsm.h"
#include "Gsm/GsmChannel.h"
#include "Message/GsmMessage.h"
#include "Message/ContactList.h"            // Contacts
#include <string.h>
#include <assert.h>

typedef enum {
   SOCK_SMS_STATUS_OFF,
   SOCK_SMS_STATUS_OPERATIONAL
} ESocketIfSmsStatus;

typedef struct {
   TSmsChannel SmsChannel;
   byte        SocketState;
   int         MaxSize;
   int         IncSize;
   char        RemoteAddr[GSM_PHONE_NUMBER_SIZE];
} TSmsSocket;

// Local variables
static ESocketIfSmsStatus _SStatus = SOCK_SMS_STATUS_OFF;
static TSmsSocket SmsSocket[SOCKET_SMS_COUNT];

// Local funcitons
static TYesNo _PhoneNumberCheck( char *Number);
// Check for channel accept

//------------------------------------------------------------------------------
//   Permission
//------------------------------------------------------------------------------

byte SocketIfSmsPermission( TSocket *Socket)
// Permission
{
   return FILE_MODE_READ_WRITE;
} // SocketIfSmsPermission

TYesNo SocketIfSmsListen( TSocket *Socket) {
   return NO;
}

//------------------------------------------------------------------------------
//   Init, deinit
//------------------------------------------------------------------------------

static void SocketIfSmsInit( void)
{
int i;

   for( i = 0; i < SOCKET_SMS_COUNT; i++){
      SmsSocket[i].SmsChannel = SMS_INVALID_CHANNEL;
      SmsSocket[i].SocketState = SOCKET_STATE_DISCONNECTED;
   }

   MGsmSmsSocketInit();
   _SStatus = SOCK_SMS_STATUS_OPERATIONAL;
}

static void SocketIfSmsDeinit( void)
{
   MGsmSmsSocketFree();
   _SStatus = SOCK_SMS_STATUS_OFF;
}

//------------------------------------------------------------------------------
//   Executive
//------------------------------------------------------------------------------

void SocketIfSmsExecute( void)
{
int   i, j;
TSmsChannelSendStatus SendStatus;

   if( _SStatus == SOCK_SMS_STATUS_OFF){
      if( !MGsmIsPowerPeriod()){
         return;
      }
      SocketIfSmsInit();
      return;
   }
   if( _SStatus == SOCK_SMS_STATUS_OPERATIONAL){
      if( !MGsmIsPowerPeriod() &&
          !GsmSmsChannelHasLiveChannel()){
         SocketIfSmsDeinit();
         return;
      }
   }
   MGsmSmsSocketExecute();

   // incomming data
   for( i = 0; i < SOCKET_SMS_COUNT; i++){
      if(SmsSocket[i].SmsChannel != SMS_INVALID_CHANNEL){
         // no empty channel
         continue;
      }
      SmsSocket[i].SmsChannel = GsmSmsChannelIncomming(SmsSocket[i].RemoteAddr, &SmsSocket[i].IncSize);
      if(SmsSocket[i].SmsChannel == SMS_INVALID_CHANNEL){
         // no incomming data
         break;
      }
      // check phone number validity
      if( !_PhoneNumberCheck( SmsSocket[i].RemoteAddr)){
         GsmSmsChannelClose( SmsSocket[i].SmsChannel);
         break;
      }
      SmsSocket[i].SocketState = SOCKET_STATE_CONNECTED;
   }
   // outgoing data
   for( i = 0; i < SOCKET_SMS_COUNT; i++){
      SendStatus = GsmSmsChannelSendStatus(SmsSocket[i].SmsChannel);
      switch(SendStatus){
         case GSM_SMS_SEND_OK:
            SmsSocket[i].SocketState = SOCKET_STATE_SEND_DONE;
            SmsSocket[i].SmsChannel = SMS_INVALID_CHANNEL;
            break;

         case GSM_SMS_SEND_ERROR:
         case GSM_SMS_SEND_ERROR_INTERNAL:
         case GSM_SMS_SEND_TIMEOUT:
         case GSM_SMS_SEND_STOPPED:
            SmsSocket[i].SocketState = SOCKET_STATE_SEND_ERROR;
            SmsSocket[i].SmsChannel = SMS_INVALID_CHANNEL;
            break;

         case GSM_SMS_SEND_WAIT_START:
         case GSM_SMS_SEND_WAIT_FIRST_ACK:
         case GSM_SMS_SEND_RESEND_FIRST:
         case GSM_SMS_SEND_SENDING:
         case GSM_SMS_SEND_RESENDING:
         case GSM_SMS_SEND_WAIT_SOME_ACK:
            SmsSocket[i].SocketState = SOCKET_STATE_SEND_ACTIVE;
            break;
         default:
            break;
      }
   }
}

//------------------------------------------------------------------------------
//   Socket API routines
//------------------------------------------------------------------------------

void SocketIfSmsTimer( void)
{
}

byte SocketIfSmsState( TSocket *Socket)
{
   assert(Socket->Id >= SOCKET_SMS_0 && Socket->Id <= SOCKET_SMS_4);
   return( SmsSocket[Socket->Id - SOCKET_SMS_0].SocketState);
}

int SocketIfSmsReceiveSize( TSocket *Socket)
{
int i;

   assert(Socket->Id >= SOCKET_SMS_0 && Socket->Id <= SOCKET_SMS_4);
   i = Socket->Id - SOCKET_SMS_0;
   if( SmsSocket[i].IncSize > SmsSocket[i].MaxSize){
      return SmsSocket[i].MaxSize;
   } else {
      return SmsSocket[i].IncSize;
   }
}

TYesNo SocketIfSmsSend( TSocket *Socket, const void *Buffer, int Size)
{
int i;
   assert(Socket->Id >= SOCKET_SMS_0 && Socket->Id <= SOCKET_SMS_4);
   i = Socket->Id - SOCKET_SMS_0;
   if( SmsSocket[i].SmsChannel == SMS_INVALID_CHANNEL){
      return NO;
   }

   GsmSmsChannelClose(SmsSocket[i].SmsChannel);
   SmsSocket[i].SmsChannel = GsmSmsChannelSend(SmsSocket[i].RemoteAddr, (byte *)Buffer, Size);
   if( SmsSocket[i].SmsChannel == SMS_INVALID_CHANNEL){
      SmsSocket[i].SocketState = SOCKET_STATE_SEND_ERROR;
      return NO;
   }
   SmsSocket[i].SocketState = SOCKET_STATE_SEND_ACTIVE;
   return YES;
}

TYesNo SocketIfSmsReceive( TSocket *Socket, void *Buffer, int Size)
{
int i;

   assert(Socket->Id >= SOCKET_SMS_0 && Socket->Id <= SOCKET_SMS_4);
   i = Socket->Id - SOCKET_SMS_0;
   if( SmsSocket[i].SmsChannel == SMS_INVALID_CHANNEL){
      SmsSocket[i].SocketState = SOCKET_STATE_DISCONNECTED;
      return NO;
   }
   SmsSocket[i].MaxSize = Size;
   if( GsmSmsChannelRead(SmsSocket[i].SmsChannel, Buffer, Size) == SMS_INVALID_CHANNEL){
      SmsSocket[i].SmsChannel = SMS_INVALID_CHANNEL;
      SmsSocket[i].SocketState = SOCKET_STATE_RECEIVE_ERROR;
      return NO;
   }
   SmsSocket[i].SocketState = SOCKET_STATE_RECEIVE_DONE;
   return YES;
}

void SocketIfSmsClose( TSocket *Socket)
// Close socket
{
   assert(Socket->Id >= SOCKET_SMS_0 && Socket->Id <= SOCKET_SMS_4);
   GsmSmsChannelClose(SmsSocket[Socket->Id - SOCKET_SMS_0].SmsChannel);
}


//------------------------------------------------------------------------------
//   Check phone
//------------------------------------------------------------------------------

static TYesNo _PhoneNumberCheck( char *Number)
// Check for channel accept
{
TYesNo Enabled;
TContactList ContactList;

   if( !GsmMessage.SmsDataTransfer.Enabled){
      return( NO);                     // remote control disabled
   }
   if( !GsmMessage.SmsDataTransfer.CheckPhoneNumber){
      return( YES);                    // always accept
   }

   while(!ContactListOpen( &ContactList));
   Enabled = ContactListChannelEnabled( &ContactList, Number);
   ContactListClose( &ContactList);
   return Enabled;
} // _PhoneNumberCheck
