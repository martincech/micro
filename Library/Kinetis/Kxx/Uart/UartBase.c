//*****************************************************************************
//
//    UartBase.c   AVR32 USART base services
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

// Local functions :
static TYesNo _BaudRateSet( TUartAddress Uart, unsigned BaudRate);
// Set baud rate divider

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void UartInit( TUartAddress Uart)
// Communication initialisation
{
   switch( Uart){
      case UART_UART0 :
         Uart0PortInit();
         Uart0ClockEnable();
         break;

#if UART_PORTS_COUNT > 1
      case UART_UART1 :
         Uart1PortInit();
         Uart1ClockEnable();
         break;
#endif
#if UART_PORTS_COUNT > 2
      case UART_UART2 :
         Uart2PortInit();
         Uart2ClockEnable();
         break;
#endif
#if UART_PORTS_COUNT > 3
      case UART_UART3 :
         Uart3PortInit();
         Uart3ClockEnable();
         break;
#endif
#if UART_PORTS_COUNT > 4
      case UART_UART4 :
         Uart4PortInit();
         Uart4ClockEnable();
         break;
#endif
#if UART_PORTS_COUNT > 5
      case UART_UART5 :
         Uart5PortInit();
         Uart5ClockEnable();
         break;
#endif
      default :
         return;
   }
} // UartInit

//-----------------------------------------------------------------------------
// Deinitialisation
//-----------------------------------------------------------------------------

void UartDeinit( TUartAddress Uart)
// Communication deinitialisation
{
   switch( Uart){
      case UART_UART0 :
         Uart0ClockDisable();
         Uart0PortDeinit();
         break;
#if UART_PORTS_COUNT > 1
      case UART_UART1 :
         Uart1ClockDisable();
         Uart1PortDeinit();
         break;
#endif
#if UART_PORTS_COUNT > 2
      case UART_UART2 :
         Uart2ClockDisable();
         Uart2PortDeinit();
         break;
#endif
#if UART_PORTS_COUNT > 3
      case UART_UART3 :
         Uart3ClockDisable();
         Uart3PortDeinit();
         break;
#endif
#if UART_PORTS_COUNT > 4
      case UART_UART4 :
         Uart4ClockDisable();
         Uart4PortDeinit();
         break;
#endif
#if UART_PORTS_COUNT > 5
      case UART_UART5 :
         Uart5ClockDisable();
         Uart5PortDeinit();
         break;
#endif
      default :
         return;
   }
} // UartDeinit


//-----------------------------------------------------------------------------
// Setup
//-----------------------------------------------------------------------------

void UartSetup( TUartAddress Address, unsigned Baud, TUartFormat Format)
// Set communication parameters
{
TUsart *Usart;

   Usart = UsartGet( Address);
   // reset controller :
   Usart->C1 = 0;
   Usart->C2 = 0;
   Usart->C3 = 0;
   Usart->C4 = 0;
   usartStatusReset(Usart);
   // set baud rate dividier :
   if( !_BaudRateSet( Address, Baud)){
      _BaudRateSet( Address, 9600);      // unable set baud rate, set default
   }
   // set bit width, parity and stop bits :
   switch( Format & 0x0F){
      default :    
      case UART_7BIT :       
      case UART_7BIT_SPACE :
      case UART_7BIT_MARK :
      case UART_8BIT :
         break;
      case UART_8BIT_EVEN :
         Usart->C1 |= UART_C1_M_MASK;
      case UART_7BIT_EVEN :
         Usart->C1 |= UART_C1_PE_MASK;
         break;
      case UART_8BIT_ODD :
         Usart->C1 |= UART_C1_M_MASK;
      case UART_7BIT_ODD :
         Usart->C1 |= UART_C1_PE_MASK | UART_C1_PT_MASK;
         break;
      case UART_8BIT_MARK :
         Usart->C1 |= UART_C1_M_MASK;
         Usart->C3 |= UART_C3_T8_MASK;
         break;
      case UART_8BIT_SPACE :
      case UART_9BIT :
         Usart->C1 |= UART_C1_M_MASK;
         break;
   }
   switch( Format & 0xF0){
      case UART_FLOW_CONTROL_HARDWARE:
         Usart->MODEM = UART_MODEM_TXCTSE_MASK | UART_MODEM_RXRTSE_MASK;
         break;
      default:
         Usart->MODEM = 0;
         break;
   }
   // allow internal fifo with interrupt after one byte
  // Usart->PFIFO = UART_PFIFO_RXFE_MASK;
  // Usart->RWFIFO = 1;
   // enable communication :
   usartInterruptDisable( Usart);
   usartRxEnable(Usart);
   usartTxEnable(Usart);
} // UartSetup

//-----------------------------------------------------------------------------
//  Timeout set
//-----------------------------------------------------------------------------

void UartTimeoutSet( TUartAddress Address, unsigned ReplyTimeout, unsigned IntercharacterTimeout)
// Set timeout based communication
{
const TUartDescriptor *Uart = UartGet( Address);

   Uart->Data->IntercharacterTimeout = (word)IntercharacterTimeout;
   Uart->Data->ReplyTimeout          = (word)ReplyTimeout;
} // UartTimeoutSet

//*****************************************************************************

//-----------------------------------------------------------------------------
// Baud rate
//-----------------------------------------------------------------------------

static TYesNo _BaudRateSet( TUartAddress Address, unsigned BaudRate)
// Set baud rate divider
{
unsigned Divisor;
byte FineDivisor;
TUsart *Usart;

   Usart = UsartGet( Address);

   switch(Address) {
      case UART_UART0:
      case UART_UART1:
         Divisor = F_SYSTEM / BaudRate;
         break;
      default:
         Divisor = F_BUS / BaudRate;
         break;
   }

   FineDivisor = (Divisor & (16 - 1)) << 1; // 1/32 increments
   Divisor = Divisor / 16;

   Usart->C4 = UART_C4_BRFA(FineDivisor);
   Usart->BDH = UART_BDH_SBR(Divisor >> 8);
   Usart->BDL = UART_BDL_SBR(Divisor);

   return( YES);
} // _BaudRateSet
