//*****************************************************************************
//
//    UsartReg.h   Kinetis USART register control
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __UsartReg_H__
   #include "Kinetis/Cpu/UsartReg.h"

   #define __UsartReg_H__

#define usartStatusReset( Module)   (void)(Module)->S1;(void)(Module)->D

#endif