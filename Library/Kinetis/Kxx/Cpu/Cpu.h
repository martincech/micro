//*****************************************************************************
//
//    Cpu.h        Kinetis CPU services
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Cpu_H__
   #include "Kinetis/Cpu/Cpu.h"
   #define __Cpu_H__

//-----------------------------------------------------------------------------
// Interrupts
//-----------------------------------------------------------------------------

#define InterruptDisable()  __set_FAULTMASK(1)
#define InterruptEnable()   __set_FAULTMASK(0)

// Conditionally enable/disable
typedef dword TIntState;

static TIntState inline _InterruptConditionallyDisable()
// Disables interrupts and returns previous interrupt enable state
{
TIntState State = __get_FAULTMASK();
   /*__asm__ __volatile__ (
      "mrs %[State], FAULTMASK\n\t" : [State] "=r" (State) :
   );*/
   InterruptDisable();
   return State;
}

static void inline _InterruptConditionallyEnable( TIntState State)
// Conditionally enables interrupts based on <State>
{
  /* __asm__ __volatile__ (
      "msr FAULTMASK, %[State]\n\t" : : [State] "r" (State)
   );*/
   __set_FAULTMASK(State);
}

// Helpers:
#define InterruptConditionallyDisable()   TIntState IntState; IntState = _InterruptConditionallyDisable()
#define InterruptConditionallyEnable()    _InterruptConditionallyEnable( IntState)

#endif
