//-----------------------------------------------------------------------------
//
//    Uni.h  - Universal definitions 32bit GNU
//    Version 1.0  (c) VymOs
//
//-----------------------------------------------------------------------------


#ifndef __Uni_H__
   #include "../../GNU/Unisys/Uni.h"

//-----------------------------------------------------------------------------
// Endian
//-----------------------------------------------------------------------------

#define ENDIAN_CONVERT_WORD( Data)       ((Data & 0xFF00) >> 8) | ((Data & 0x00FF) << 8)
#define ENDIAN_CONVERT_DWORD( Data)      ((Data & 0xFF000000) >> 24) | ((Data & 0x00FF0000) >> 8) | ((Data & 0x0000FF00) << 8) | ((Data & 0x000000FF) << 24)

#define ENDIAN_FROM_BIG_WORD( Data)      ENDIAN_CONVERT_WORD( Data)
#define ENDIAN_FROM_BIG_DWORD( Data)     ENDIAN_CONVERT_DWORD( Data)     


#define ENDIAN_TO_BIG_WORD( Data)        Data =  ENDIAN_CONVERT_WORD( Data)
#define ENDIAN_TO_BIG_DWORD( Data)       Data =  ENDIAN_CONVERT_WORD( Data)

#define ENDIAN_FROM_LITTLE_WORD( Data)   Data
#define ENDIAN_FROM_LITTLE_DWORD( Data)  Data

#define ENDIAN_TO_LITTLE_WORD( Data)     Data = Data
#define ENDIAN_TO_LITTLE_DWORD( Data)    Data = Data
#endif
