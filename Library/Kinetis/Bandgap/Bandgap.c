//*****************************************************************************
//
//    Bandgap.c      VREF bandgap
//    Version 1.0    (c) Veit Electronics
//
//*****************************************************************************

#include "Bandgap.h"

#warning Netestovano !!!

//------------------------------------------------------------------------------
//  Init
//------------------------------------------------------------------------------

void BandgapInit( void)
// Initialization
{
   VrefClockEnable();
   VREF->SC = VREF_SC_VREFEN_MASK;
} // BandgapInit

//------------------------------------------------------------------------------
//  Ready
//------------------------------------------------------------------------------

TYesNo BandgapReady( void)
// Check ready
{
   if(VREF->SC & VREF_SC_VREFST_MASK) {
      return YES;
   }
   
   return NO;
} // BandgapReady

//------------------------------------------------------------------------------
//  Shutdown
//------------------------------------------------------------------------------

void BandgapShutdown( void)
// Shutdown
{
   VREF->SC = 0;
   VrefClockDisable();
} // BandgapShutdown