//*****************************************************************************
//
//    Pll.h        K70 Pll
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************


#ifndef __Pll_H__
   #define __Pll_H__

// Constants for use in pll_init
#define NO_OSCINIT 0
#define OSCINIT 1

#define LOW_POWER 0
#define HIGH_GAIN 1

#define CANNED_OSC  0
#define CRYSTAL 1

#define PLL_ONLY 0
#define MCGOUT 1

#define BLPI 1
#define FBI  2
#define FEI  3
#define FEE  4
#define FBE  5
#define BLPE 6
#define PBE  7
#define PEE  8

// IRC defines
#define SLOW_IRC 0
#define FAST_IRC 1
   
unsigned int PllInit(
		unsigned char init_osc, 
		int crystal_val, 
		unsigned char hgo_val, 
		unsigned char erefs_val, 
		signed char prdiv_val, 
		signed char vdiv_val, 
		unsigned char mcgout_select
);

void PllShutdown( void);

#endif