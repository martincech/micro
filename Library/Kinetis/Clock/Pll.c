//*****************************************************************************
//
//    Pll.h        K70 Pll
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "Pll.h"
#include "Hardware.h"

void PllShutdown( void) {
   MCG->C6 = 0;
   while(MCG->S & MCG_S_PLLST_MASK);
   MCG->C5 &= ~MCG_C5_PLLCLKEN0_MASK;
}

unsigned int PllInit(
		unsigned char init_osc, 
		int crystal_val, 
		unsigned char hgo_val, 
		unsigned char erefs_val, 
		signed char prdiv_val, 
		signed char vdiv_val, 
		unsigned char mcgout_select
)
{
	unsigned char frdiv_val;
	unsigned char temp_reg;
	unsigned char prdiv, vdiv;
	short i;
	int ref_freq;
	int pll_freq;

	// If using the PLL as MCG_OUT must check if the MCG is in FEI mode first
	if (mcgout_select)
	{
		// check if in FEI mode
		if (!((((MCG->S & MCG_S_CLKST_MASK) >> MCG_S_CLKST_SHIFT) == 0x0) && // check CLKS mux has selcted FLL output
				(MCG->S & MCG_S_IREFST_MASK) &&                                  // check FLL ref is internal ref clk
				(!(MCG->S & MCG_S_PLLST_MASK))))                                 // check PLLS mux has selected FLL
		{
			return 0x1;                                                     // return error code
		}
	} // if (mcgout_select)

	// check external frequency is less than the maximum frequency
	if  (crystal_val > 60000000) {return 0x21;}

	// check crystal frequency is within spec. if crystal osc is being used as PLL ref
	if (erefs_val)
	{
		if ((crystal_val < 4000000) || (crystal_val > 32000000)) {return 0x22;} // return 1 if one of the available crystal options is not available
	}

	// make sure HGO will never be greater than 1. Could return an error instead if desired.
	if (hgo_val > 0)
	{
		hgo_val = 1; // force hgo_val to 1 if > 0
	}

	// Check PLL divider settings are within spec.
	if ((prdiv_val < 1) || (prdiv_val > 25)) {return 0x41;}
	if ((vdiv_val < 24) || (vdiv_val > 55)) {return 0x42;}

	// Check PLL reference clock frequency is within spec.
	ref_freq = crystal_val / prdiv_val;
	if ((ref_freq < 2000000) || (ref_freq > 4000000)) {return 0x43;}

	// Check PLL output frequency is within spec.
	/*pll_freq = (crystal_val / prdiv_val) * vdiv_val;
	if ((pll_freq < 180000000) || (pll_freq > 360000000)) {return 0x45;}*/

	// Determine if oscillator needs to be set up
	if (init_osc)
	{
		// Check if the oscillator needs to be configured
			// configure the MCG->C2 register
			// the RANGE value is determined by the external frequency. Since the RANGE parameter affects the FRDIV divide value
			// it still needs to be set correctly even if the oscillator is not being used

			temp_reg = MCG->C2;
			temp_reg &= ~(MCG_C2_RANGE0_MASK | MCG_C2_HGO0_MASK | MCG_C2_EREFS0_MASK); // clear fields before writing new values

			if (crystal_val <= 8000000)
			{
				temp_reg |= (MCG_C2_RANGE0(1) | (hgo_val << MCG_C2_HGO0_SHIFT) | (erefs_val << MCG_C2_EREFS0_SHIFT));
			}
			else
			{
				// On rev. 1.0 of silicon there is an issue where the the input bufferd are enabled when JTAG is connected.
				// This has the affect of sometimes preventing the oscillator from running. To keep the oscillator amplitude
				// low, RANGE = 2 should not be used. This should be removed when fixed silicon is available.
				//temp_reg |= (MCG_C2_RANGE(2) | (hgo_val << MCG_C2_HGO_SHIFT) | (erefs_val << MCG_C2_EREFS_SHIFT));
				temp_reg |= (MCG_C2_RANGE0(1) | (hgo_val << MCG_C2_HGO0_SHIFT) | (erefs_val << MCG_C2_EREFS0_SHIFT));
			}
			MCG->C2 = temp_reg;
	} // if (init_osc)

	if (mcgout_select)
	{
		// determine FRDIV based on reference clock frequency
		// since the external frequency has already been checked only the maximum frequency for each FRDIV value needs to be compared here.
		if (crystal_val <= 1250000) {frdiv_val = 0;}
		else if (crystal_val <= 2500000) {frdiv_val = 1;}
		else if (crystal_val <= 5000000) {frdiv_val = 2;}
		else if (crystal_val <= 10000000) {frdiv_val = 3;}
		else if (crystal_val <= 20000000) {frdiv_val = 4;}
		else {frdiv_val = 5;}

		// Select external oscillator and Reference Divider and clear IREFS to start ext osc
		// If IRCLK is required it must be enabled outside of this driver, existing state will be maintained
		// CLKS=2, FRDIV=frdiv_val, IREFS=0, IRCLKEN=0, IREFSTEN=0
		temp_reg = MCG->C1;
		temp_reg &= ~(MCG_C1_CLKS_MASK | MCG_C1_FRDIV_MASK | MCG_C1_IREFS_MASK); // Clear values in these fields
		temp_reg = MCG_C1_CLKS(2) | MCG_C1_FRDIV(frdiv_val); // Set the required CLKS and FRDIV values
		MCG->C1 = temp_reg;

		// if the external oscillator is used need to wait for OSCINIT to set
		if (erefs_val)
		{
			for (i = 0 ; i < 10000 ; i++)
			{
				if (MCG->S & MCG_S_OSCINIT0_MASK) break; // jump out early if OSCINIT sets before loop finishes
			}
			if (!(MCG->S & MCG_S_OSCINIT0_MASK)) return 0x23; // check bit is really set and return with error if not set
		}

		// wait for Reference clock Status bit to clear
		for (i = 0 ; i < 2000 ; i++)
		{
			if (!(MCG->S & MCG_S_IREFST_MASK)) break; // jump out early if IREFST clears before loop finishes
		}
		if (MCG->S & MCG_S_IREFST_MASK) return 0x11; // check bit is really clear and return with error if not set

		// Wait for clock status bits to show clock source is ext ref clk
		for (i = 0 ; i < 2000 ; i++)
		{
			if (((MCG->S & MCG_S_CLKST_MASK) >> MCG_S_CLKST_SHIFT) == 0x2) break; // jump out early if CLKST shows EXT CLK slected before loop finishes
		}
		if (((MCG->S & MCG_S_CLKST_MASK) >> MCG_S_CLKST_SHIFT) != 0x2) return 0x1A; // check EXT CLK is really selected and return with error if not

		// Now in FBE
		// It is recommended that the clock monitor is enabled when using an external clock as the clock source/reference.
		// It is enabled here but can be removed if this is not required.

MCG->C6 |= MCG_C6_CME0_MASK;

			// Configure PLL0
			// Ensure OSC0 is selected as the reference clock

			// Configure MCG_C5
			// If the PLL is to run in STOP mode then the PLLSTEN bit needs to be OR'ed in here or in user code.
			temp_reg = MCG->C5;
			temp_reg &= ~MCG_C5_PRDIV0_MASK;
			temp_reg |= MCG_C5_PRDIV0(prdiv_val - 1);    //set PLL ref divider
			MCG->C5 = temp_reg;

			// Configure MCG_C6
			// The PLLS bit is set to enable the PLL, MCGOUT still sourced from ext ref clk
			// The loss of lock interrupt can be enabled by seperately OR'ing in the LOLIE bit in MCG_C6
			temp_reg = MCG->C6; // store present C6 value
			temp_reg &= ~MCG_C6_VDIV0_MASK; // clear VDIV settings
			temp_reg |= MCG_C6_PLLS_MASK | MCG_C6_VDIV0(vdiv_val - 24); // write new VDIV and enable PLL
			MCG->C6 = temp_reg; // update MCG_C6

			// wait for PLLST status bit to set
			for (i = 0 ; i < 2000 ; i++)
			{
				if (MCG->S & MCG_S_PLLST_MASK) break; // jump out early if PLLST sets before loop finishes
			}
			if (!(MCG->S & MCG_S_PLLST_MASK)) return 0x16; // check bit is really set and return with error if not set

			// Wait for LOCK bit to set
			for (i = 0 ; i < 2000 ; i++)
			{
				if (MCG->S & MCG_S_LOCK0_MASK) break; // jump out early if LOCK sets before loop finishes
			}
			if (!(MCG->S & MCG_S_LOCK0_MASK)) return 0x44; // check bit is really set and return with error if not set

			// Use actual PLL settings to calculate PLL frequency
			prdiv = ((MCG->C5 & MCG_C5_PRDIV0_MASK) + 1);
			vdiv = ((MCG->C6 & MCG_C6_VDIV0_MASK) + 16);

		// now in PBE

		MCG->C1 &= ~MCG_C1_CLKS_MASK; // clear CLKS to switch CLKS mux to select PLL as MCG_OUT

		// Wait for clock status bits to update
		for (i = 0 ; i < 2000 ; i++)
		{
			if (((MCG->S & MCG_S_CLKST_MASK) >> MCG_S_CLKST_SHIFT) == 0x3) break; // jump out early if CLKST = 3 before loop finishes
		}
		if (((MCG->S & MCG_S_CLKST_MASK) >> MCG_S_CLKST_SHIFT) != 0x3) return 0x1B; // check CLKST is set correctly and return with error if not

		// Now in PEE
	}
	else
	{

			// Setup and enable PLL0

			// Configure MCG_C5
			// If the PLL is to run in STOP mode then the PLLSTEN bit needs to be OR'ed in here or in user code.
			temp_reg = MCG->C5;
			temp_reg &= ~MCG_C5_PRDIV0_MASK;
			temp_reg |= MCG_C5_PRDIV0(prdiv_val - 1);    //set PLL ref divider
			MCG->C5 = temp_reg;

			// Configure MCG_C6
			// The loss of lock interrupt can be enabled by seperately OR'ing in the LOLIE bit in MCG_C6
			temp_reg = MCG->C6; // store present C6 value
			temp_reg &= ~MCG_C6_VDIV0_MASK; // clear VDIV settings
			temp_reg |=  MCG_C6_VDIV0(vdiv_val - 24); // write new VDIV and enable PLL
			MCG->C6 = temp_reg; // update MCG_C6
			// Now enable the PLL
			MCG->C5 |= MCG_C5_PLLCLKEN0_MASK; // Set PLLCLKEN to enable PLL0

			// Wait for LOCK bit to set
			for (i = 0 ; i < 2000 ; i++)
			{
				if (MCG->S & MCG_S_LOCK0_MASK) break; // jump out early if LOCK sets before loop finishes
			}
			if (!(MCG->S & MCG_S_LOCK0_MASK)) return 0x44; // check bit is really set and return with error if not set

			// Use actual PLL settings to calculate PLL frequency
			prdiv = ((MCG->C5 & MCG_C5_PRDIV0_MASK) + 1);
			vdiv = ((MCG->C6 & MCG_C6_VDIV0_MASK) + 16);

	} // if (mcgout_select)

	return (((crystal_val / prdiv) * vdiv) / 2); //MCGOUT equals PLL output frequency/2
} //pll_init