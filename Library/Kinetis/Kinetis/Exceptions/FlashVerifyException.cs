using System;
using System.Runtime.Serialization;

namespace Kinetis
{
   /// <summary>
   /// \brief Custom exception to throw the verify error.
   /// </summary>
   public class FlashVerifyException : ApplicationException
   {
      /// <summary> \brief default constructor. Do nothing /// </summary>
      public FlashVerifyException()
      {
      }

      /// <summary> \brief default constructor. Do nothing /// </summary>
      public FlashVerifyException(string message) : base(message)
      {
      }

      /// <summary> \brief default constructor. Do nothing /// </summary>
      public FlashVerifyException(string message, Exception inner) : base(message, inner)
      {
      }

      /// <summary> \brief default constructor. Do nothing /// </summary>
      protected FlashVerifyException(
         SerializationInfo info,
         StreamingContext context
         )
         : base(info, context)
      {
      }
   }
}