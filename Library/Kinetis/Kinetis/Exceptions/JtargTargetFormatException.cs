using System;
using System.Runtime.Serialization;

namespace Kinetis
{
   /// <summary>
   ///  Custom exception to throw the JTAG definition XML error.
   /// </summary>
   public class JtargTargetFormatException : ApplicationException
   {
      /// <summary>  default constructor. Do nothing 
      /// </summary>
      public JtargTargetFormatException()
      {
      }

      /// <summary>  default constructor. Do nothing 
      /// </summary>
      public JtargTargetFormatException(string message) : base(message)
      {
      }

      /// <summary>  default constructor. Do nothing 
      /// </summary>
      public JtargTargetFormatException(string message, Exception inner) : base(message, inner)
      {
      }

      /// <summary>  default constructor. Do nothing 
      /// </summary>
      protected JtargTargetFormatException(
         SerializationInfo info,
         StreamingContext context
         )
         : base(info, context)
      {
      }
   }
}