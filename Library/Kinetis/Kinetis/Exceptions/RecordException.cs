using System;
using System.Runtime.Serialization;

namespace Kinetis
{
   ///<summary>
   ///\brief Custome Exception to report the Format error.
   ///
   ///This Exception is raised when error is found on the given intel Hex or
   ///Motorola S-format.
   ///</summary>
   public class RecordException : ApplicationException
   {
      ///<summary> \brief default constructor. Do nothing </summary>
      public RecordException()
      {
      }

      ///<summary> \brief default constructor. Do nothing </summary>
      public RecordException(string message) : base(message)
      {
      }

      ///<summary> \brief default constructor. Do nothing </summary>
      public RecordException(string message, Exception inner) : base(message, inner)
      {
      }

      ///<summary> \brief default constructor. Do nothing </summary>
      protected RecordException(
         SerializationInfo info,
         StreamingContext context
         ) : base(info, context)
      {
      }
   };
}