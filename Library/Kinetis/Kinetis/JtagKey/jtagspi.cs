/*
 * Copyright 2008-2009 The Asagao Project. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 *
 *  1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE ASAGAO PROJECT ``AS IS'' AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE ASAGAO PROJECT OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE 
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation 
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of the Asagao Project.
 * 
 * http://sourceforge.net/projects/asagao/
 */
/** 
* \file jtagspi.cs
* \Author Suikan
* \brief Interface for SPI by Amontec JTAG 
* 
* Contains basic interface class for Amontec JTAG Key or its compatible. 
* This file support "JTAG SPI connection" between JTAGKEY, a JTAG aware device and SPI device.
* 
*/

using System;
using System.Text;
using Ftdi.Jtag;
using Utilities;

namespace Kinetis.JtagKey
{
   /**
         * \brief JTAG connection class between Amontec JTAGKEY and SPI devices
         * 
         * This class supports JTAG through connection of JTAG key and SPI slave device.
         * To enable this, this class enable the buffer of JTAGKEY. JTAG connection mean,
         * you can control SPI device whihi is used by ARM processor. Of course the processor
         * BSDL should be published.
         */

   public class JtagSpi : AbstractSpi
   {
      /** 
             * \brief Target JTAG information
             * 
             * This variable hold the JTAG information of target device, which contains safe setting buf 
             * boudary register, instruciton bit string, etc..
             */
      protected TargetDevice Target;

      /**
             * \brief Internal method to handle DLL's error 
             * \param status  returned status from function.
             * 
             * This internal method is used to throw an exception when FTCSPI.DLL or FTCJTAG.DLL returns
             * error status. Status is converted to string and throw away with exception.
             */

      protected override void ThrowError(object status)
      {
         var stat = (JtagStatus) status;

         if (stat == JtagStatus.Success) return;
         var errorMessage = new StringBuilder(255);

         Opened = false;
         FtcJtag.GetErrorCodeString(
            "EN",
            stat,
            errorMessage,
            errorMessage.Capacity);
         throw new ApplicationException(errorMessage + " in JtagSpi module");
      }

      /**
             * \brief keep idcode of device during this object is alive.
             * 
             * This is JTAG only but assigned because of compatibility
            */
      protected int MIdcode;


      public override bool Identified(out string vender, out string name)
      {
         if ((MIdcode & 0x0FFFFFFF) == (Target.RegIdcode & 0x0FFFFFFF))
         {
            vender = Target.Vendor;
            name = Target.Name;
            return true;
         }
         vender = "Doesn't match";
         name = "Doesn't match";
         return false;
      }


      /**
             * \brief return the version string
             * \return A version string of JTAG DLL
             */

      public override string GetDllVersion()
      {
         var s = new StringBuilder(255);

         // get version number.
         var stat = FtcJtag.GetDllVersion(
            s,
            s.Capacity
            );
         ThrowError(stat);
         return (s.ToString());
      }

      /**
             * \brief constructor
             * \param aDivisor  12/(1+aDivisor)/2 MHz.
             * 
             * Setup given parameter into the instans variable to be ready to open the device.
             */

      public JtagSpi(int aDivisor, TargetDevice theTarget)
         : base(aDivisor)
      {
         Target = theTarget;
      }

      /**
             * \brief Open the FT2232 device.
             * \param deviceNumber  is the selected index of ComboBox which has list of devices.
             * 
             * This function open the FT2232 device which is specified by parameter. After opening, 
             * It calls  InitDevice function and transmit the MPSSE instructions to enable the buffer
             * of Amontec JTAGKEY. 
             */

      public override void Open(int deviceNumber)
      {
         if (Opened)
            Close();

         // Open the selected device
         var stat = FtcJtag.OpenEx(
            DeviceNameList[deviceNumber],
            DeviceLocationList[deviceNumber],
            out Handle);
         ThrowError(stat);

         // If success, set up JTAG TAP.
         if (stat != JtagStatus.Success) return;

         Opened = true; // record it
         var index = 0;
         var command = new byte[3]; // command buffer for initialization
         int transfered;

         // initialize JTAG KEY device
         stat = FtcJtag.InitDevice(Handle, Divisor); // devisor 0 => 6MHz.
         ThrowError(stat);

         // enabling buffer by sending MPSSE opcode
         command[index++] = 0x80; // Output GPIO
         command[index++] = 0x08; // BufferEnable, TMS = H, TDI = L
         command[index++] = 0x1B; // GPIOL0, TMS, TDI, CLK is Out, TDO is In.

         Ftdi.Ftdi.Write(Handle, command, index, out transfered);

         TapControl.EnableTap(Handle);

         var tempIdcode = new byte[4];

         // detect idcode
         FtcJtag.Write(
            Handle, // target to control 
            JtagRegType.Instruction, // Instruction protocol
            Target.InstBitlength, // idcode instruction bit length
            Target.InstIdcode, // idcode instruction
            Target.InstIdcode.Length, // length in byte
            JtagTapState.TestLogicState // stay in test mode.
            );

         FtcJtag.Read(
            Handle, // target to control
            JtagRegType.Data, // data protocol
            32, // IDCODE is always 32bit
            tempIdcode, // buffer to receive
            out transfered,
            JtagTapState.TestLogicState // stay in test mode.
            );

         // set the idcode
         MIdcode = (tempIdcode[3] << 24) + (tempIdcode[2] << 16) + (tempIdcode[1] << 8) + tempIdcode[0];
      }

      /**
             * \brief closing FT2232
             * 
             * To close the handle, this routine disable the Amontec JTAG Key buffer. And then
             * FtcJtag.Close() function.
             */

      public override void Close()
      {
         if (Opened)
         {
            var command = new byte[3];
            var index = 0;
            int transfered;

            // disabling buffer
            command[index++] = 0x80; // Output GPIO
            command[index++] = 0x18; // BufferDisable, TMS = H, TDI = L
            command[index++] = 0x1B; // GPIOL0, TMS, TDI, CLK is Out, TDO is In.

            Ftdi.Ftdi.Write(Handle, command, index, out transfered);

            TapControl.DisableTap(Handle);

            // Closing device
            var stat = FtcJtag.Close(Handle);
            ThrowError(stat);
            Opened = false;
         }
      }

      /**
             * \brief write a byte to JTAG KEY
             * \param txBuf Contains a safe setting of JTAG boundary register
             * \param txByte Contains a byte to send
             * 
             * txByte is sent from MSB to LSB by JTAG chain. The SPI clock is 
             * just toggling. So, the initial state of SPICLK bit in the chain
             * decide the clock polality.
             */

      private void WriteByte(byte[] txBuf, byte txByte)
      {
         byte mask = 0x80; // Bit mask to send. First bit is MSB

         // send 8 bits
         for (var i = 0; i < 8; i++)
         {
            // set first bit
            if ((mask & txByte) != 0)
               BitOp.Set(txBuf, Target.SpiMosi);
            else
               BitOp.Clr(txBuf, Target.SpiMosi);

            // update mask
            mask >>= 1;

            // Send first half of cycle.
            var stat = FtcJtag.AddDeviceWriteCmd(
               Handle,
               JtagRegType.Data,
               Target.BoundaryBitlength,
               txBuf,
               txBuf.Length,
               JtagTapState.RunTestIdelState
               );
            ThrowError(stat);

            // toggle clock
            BitOp.Toggle(txBuf, Target.SpiClock);

            // Send second half of cycle.
            stat = FtcJtag.AddDeviceWriteCmd(
               Handle,
               JtagRegType.Data,
               Target.BoundaryBitlength,
               txBuf,
               txBuf.Length,
               JtagTapState.RunTestIdelState
               );
            ThrowError(stat);

            // toggle clock
            BitOp.Toggle(txBuf, Target.SpiClock);
         }
      }

      /**
             * \brief read and write a byte to JTAG KEY
             * \param txBuf Contains a safe setting of JTAG boundary register
             * \param txByte Contains a byte to send
             * 
             * txByte is sent from MSB to LSB by JTAG chain. The SPI clock is 
             * just toggling. So, the initial state of SPICLK bit in the chain
             * decide the clock polality.
             * 
             * The read data is acquired only when you invoke ExecuteCmdSequence().
             */

      private void ReadWriteByte(byte[] txBuf, byte txByte)
      {
         byte mask = 0x80;

         for (var i = 0; i < 8; i++)
         {
            // set first bit
            if ((mask & txByte) != 0)
               BitOp.Set(txBuf, Target.SpiMosi);
            else
               BitOp.Clr(txBuf, Target.SpiMosi);

            // update mask
            mask >>= 1;

            var stat = FtcJtag.AddDeviceWriteCmd(
               Handle,
               JtagRegType.Data,
               Target.BoundaryBitlength,
               txBuf,
               txBuf.Length,
               JtagTapState.RunTestIdelState
               );
            ThrowError(stat);

            // toggle clock
            BitOp.Toggle(txBuf, Target.SpiClock);

            stat = FtcJtag.AddDeviceWriteReadCmd(
               Handle,
               JtagRegType.Data,
               Target.BoundaryBitlength,
               txBuf,
               txBuf.Length,
               JtagTapState.RunTestIdelState
               );
            ThrowError(stat);

            // toggle clock
            BitOp.Toggle(txBuf, Target.SpiClock);
         }
      }

      /**
             * \brief retrieve a bytes from the bit sequence of JTAG chain
             * \param rxBuf The received JTAG boundary registers. 
             * \param byteIndex read pointer. Should be 0 to get first byte. Then, automatically updated inside this funciton.
             * \return received byte
             * 
             * Construct a byte from the JTAG boudary register bit streams. To get one byte, you should scan JTAG chain 8 times. 
             * And all scanned data for a byte should be in a txBuf array. The "byteIndex" is used to mark the position of next
             * time processing. It should be 0 to get first byte. 
             */

      private byte GetByteFromJtag(byte[] rxBuf, ref int byteIndex)
      {
         var bitMask = 0x80;
         var result = 0;

         for (var i = 0; i < 8; i++)
         {
            if (BitOp.Extract(rxBuf, byteIndex, Target.SpiMiso) != 0) // check the value of MISO in a chain.
               result |= bitMask; // if 1, set the corresponding bit
            bitMask >>= 1; // shift mask
            byteIndex += Target.RegBoundary.Length; // update read pointer.
         }
         return (byte) result;
      }

      /**
             * \brief Write/Read data bytes from SPI device.
             * \param txData  transmite data buffer to send to device
             * \param rxData  data buffer to receive data
             * \param LatchingEdge  The edge which MISO samples data. 
             * 
             */

      public override void Transfer(byte[] txData, byte[] rxData, LatchOn latchingEdge)
      {
         // check if given buffer is null. if null, it is ignored, but to be
         // ease of handle, substiute null with zero length buffer.
         if (txData == null)
            txData = new byte[0];
         if (rxData == null)
            rxData = new byte[0];

         var txBuf = new byte[Target.RegBoundary.Length];
         var rxBuf = new byte[rxData.Length*Target.RegBoundary.Length*8];
         var chipSelectPos = Target.SpiCs;
         int transferedLength;
         var txIndex = 0; // index for txData
         var rxIndex = 0; // index for rxData


         // length of data which need to be read/write
         var commonLength = Math.Min(rxData.Length, txData.Length);


         Target.RegBoundary.CopyTo(txBuf, 0); // copy "safe" setting to boundary register to txBuffer

         // create initial state
         BitOp.Set(txBuf, chipSelectPos); // to be sure, set CS H.


         if (latchingEdge == LatchOn.Rising)
            BitOp.Clr(txBuf, Target.SpiClock); // set initial clock pin leve as L
         else
            BitOp.Set(txBuf, Target.SpiClock); // set initial clock pin level as H

         // Set target sample/preload mode
         var stat = FtcJtag.AddDeviceWriteCmd(
            Handle,
            JtagRegType.Instruction,
            Target.InstBitlength,
            Target.InstSample,
            Target.InstSample.Length,
            JtagTapState.RunTestIdelState
            );
         ThrowError(stat);

         // preload initial state
         stat = FtcJtag.AddDeviceWriteCmd(
            Handle,
            JtagRegType.Data,
            Target.BoundaryBitlength,
            txBuf,
            txBuf.Length,
            JtagTapState.RunTestIdelState
            );
         ThrowError(stat);

         // Set target extest mode. Preloaded value is out
         stat = FtcJtag.AddDeviceWriteCmd(
            Handle,
            JtagRegType.Instruction,
            Target.InstBitlength,
            Target.InstExtest,
            Target.InstExtest.Length,
            JtagTapState.RunTestIdelState
            );
         ThrowError(stat);

         // set CS = Low
         BitOp.Clr(txBuf, chipSelectPos);


         // Read/Write command for the first N bytes which has read/write buffer
         if (commonLength > 0)
         {
            while (txIndex < commonLength)
            {
               var iter = Math.Min(8, commonLength - txIndex);

               // create tranxmit JTAG bit stream from txData
               for (var i = 0; i < iter; i++)
               {
                  ReadWriteByte(txBuf, txData[txIndex]);
                  txIndex++;
               }

               stat = FtcJtag.ExecuteCmdSequence(
                  Handle,
                  rxBuf,
                  out transferedLength
                  );
               ThrowError(stat);

               var rxPos = 0; // position in JTAG bit stream;
               for (var i = 0; i < iter; i++)
               {
                  rxData[rxIndex] = GetByteFromJtag(rxBuf, ref rxPos);
                  rxIndex++;
               }
            }
         }

         // Optional Write
         if (txData.Length > commonLength)
         {
            while (txIndex < txData.Length)
            {
               var iter = Math.Min(8, txData.Length - txIndex);

               // create tranxmit JTAG bit stream from txData
               for (var i = 0; i < iter; i++)
               {
                  WriteByte(txBuf, txData[txIndex]);
                  txIndex++;
               }

               stat = FtcJtag.ExecuteCmdSequence(
                  Handle,
                  rxBuf,
                  out transferedLength
                  );
               ThrowError(stat);
            }
         }
            // Optional Read
         else if (rxData.Length > commonLength)
         {
            while (rxIndex < rxData.Length)
            {
               var iter = Math.Min(8, rxData.Length - rxIndex);

               // create dummy jtag bit stream
               for (var i = 0; i < iter; i++)
               {
                  ReadWriteByte(txBuf, 0); // transmit dummy data to generate clock
               }

               stat = FtcJtag.ExecuteCmdSequence(
                  Handle,
                  rxBuf,
                  out transferedLength
                  );
               ThrowError(stat);

               var rxPos = 0; // position in JTAG bit stream;
               for (var i = 0; i < iter; i++)
               {
                  rxData[rxIndex] = GetByteFromJtag(rxBuf, ref rxPos);
                  rxIndex++;
               }
            }
         }

         // Finishing
         BitOp.Set(txBuf, chipSelectPos); // set CS H.
         stat = FtcJtag.AddDeviceWriteCmd(
            Handle,
            JtagRegType.Data,
            Target.BoundaryBitlength,
            txBuf,
            txBuf.Length,
            JtagTapState.RunTestIdelState
            );
         ThrowError(stat);

         stat = FtcJtag.ExecuteCmdSequence(
            Handle,
            rxBuf,
            out transferedLength
            );
         ThrowError(stat);
      }
   }
}