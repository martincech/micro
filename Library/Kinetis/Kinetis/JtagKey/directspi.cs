/*
 * Copyright 2008-2009 The Asagao Project. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 *
 *  1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE ASAGAO PROJECT ``AS IS'' AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE ASAGAO PROJECT OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE 
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation 
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of the Asagao Project.
 * 
 * http://sourceforge.net/projects/asagao/
 */
/** 
* \file directspi.cs
* \Author Suikan
* \brief Interface for SPI by Amontec JTAG 
* 
* Contains basic interface class for Amontec JTAG Key or its compatible. 
* This file support "Dicrect SPI connection" between JTAGKEY and SPI device.
* 
*/

using System;
using System.Text;
using Ftdi.Spi;

namespace Kinetis.JtagKey
{
   /**
         * \brief Direct connection class between Amontec JTAGKEY and SPI devices
         * 
         * This class supports direct connection of JTAG key and SPI slave device.
         * To enable this, this class enable the buffer of JTAGKEY, and provide fully
         * full duplex transfer. 
         */

   public class DirectSpi : AbstractSpi
   {
      /**
             * \brief Internal method to handle DLL's error 
             * \param status  returned status from function.
             * 
             * This internal method is used to throw an exception when FTCSPI.DLL or FTCJTAG.DLL returns
             * error status. Status is converted to string and throw away with exception.
             */

      protected override void ThrowError(object status)
      {
         var stat = (SpiStatus) status;

         if (stat == SpiStatus.Success) return;

         var errorMessage = new StringBuilder(255);   
         Opened = false;
         FtcSpi.GetErrorCodeString(
            "EN",
            stat,
            errorMessage,
            errorMessage.Capacity);
         throw new ApplicationException(errorMessage.ToString());
      }

      /**
             * \brief retur the version string 
             */

      public override string GetDllVersion()
      {
         var s = new StringBuilder(255);

         // get version number.
         FtcSpi.GetDllVersion(
            s,
            s.Capacity
            );
         return (s.ToString());
      }

      /**
             * \brief constructor
             * \param aDivisor  12/(1+aDivisor)/2 MHz.
             * 
             * Setup given parameter into the instans variable to be ready to open the device.
             */

      public DirectSpi(int aDivisor)
         : base(aDivisor)
      {
      }

      /**
             * \brief Open the FT2232 device.
             * \param deviceIndex  is the selected index of ComboBox which has list of devices.
             * 
             * This function open the FT2232 device which is specified by parameter. After opening, 
             * It calls  InitDevice function and transmit the MPSSE instructions to enable the buffer
             * of Amontec JTAGKEY. 
             */

      public override void Open(int deviceIndex)
      {
         if (Opened)
            Close();

         var stat = FtcSpi.OpenEx(
            DeviceNameList[deviceIndex],
            DeviceLocationList[deviceIndex],
            out Handle);
         if (stat == SpiStatus.Success)
         {
            Opened = true;
            var index = 0;
            var command = new byte[3];
            int transfered;

            // initialize device
            stat = FtcSpi.InitDevice(Handle, Divisor); // devisor 0 => 6MHz.
            ThrowError(stat);

            // enabling buffer
            command[index++] = 0x80; // Output GPIO
            command[index++] = 0x08; // BufferEnable, TMS = H, TDI = L
            command[index++] = 0x1B; // GPIOL0, TMS, TDI, CLK is Out, TDO is In.

            Ftdi.Ftdi.Write(Handle, command, index, out transfered);
         }
         else
            ThrowError(stat);
      }

      /**
             * \brief closing FT2232
             * 
             * To close the handle, this routine disable the Amontec JTAG Key buffer. And then
             * SpiDll.Close() function.
             */

      public override void Close()
      {
         if (!Opened) return;

         var command = new byte[3];
         var index = 0;
         int transfered;

         // disabling buffer
         command[index++] = 0x80; // Output GPIO
         command[index++] = 0x18; // BufferDisable, TMS = H, TDI = L
         command[index++] = 0x1B; // GPIOL0, TMS, TDI, CLK is Out, TDO is In.

         Ftdi.Ftdi.Write(Handle, command, index, out transfered);

         Opened = false;

         var stat = FtcSpi.Close(Handle);
         ThrowError(stat);
      }

      /**
             * \brief Write/Read data bytes from SPI device.
             * \param txData  transmite data buffer to send to device
             * \param rxData  data buffer to receive data
             * \param LatchingEdge  The edge which MISO samples data. 
             * \param chipSelect specify which chip select is used. Note that this value is ignored.
             * 
             * Transmit the data in txData through the SPI protocol. Simultenously, receive the data
             * into the rxData. The transmit / receive data size is decidec by the size of txData / rxData, respectively.
             * The size of txData and rxData can be different. In case of the length is different, shorter buffer
             * stops transfering at first.
             * 
             * This function control the JTAG KEY device as SPI direct protocol. Then, you can connect SPI
             * slave to the JTAG KEY directly.
             */

      public override void Transfer(byte[] txData, byte[] rxData, LatchOn latchingEdge)
      {
         // check if given buffer is null. if null, it is ignored, but to be
         // ease of handle, substiute null with zero length buffer.
         if (txData == null)
            txData = new byte[0];
         if (rxData == null)
            rxData = new byte[0];

         // 15 in the command length is a magic number. where :
         // 3 for initializing (set GPIO command)
         // 3 for TMS assert (set GPIO command)
         // 3 for Data in/out (except data itself).
         // 3 for TMS deassert (set GPIO command)
         // 3 for optional Data in/out (except data itself )
         var command = new byte[txData.Length + 16];
         var index = 0; // current index of command buffer
         int transfered; // how much data is transfered

         byte initClock; // clock bit which specify initial state.
         byte clockDataByteInOutInst; // read write
         byte clockDataByteInInst; // read
         byte clockDataByteOutInst; // write


         // length of data which need to be read/write
         var commonLength = Math.Min(rxData.Length, txData.Length);

         if (latchingEdge == LatchOn.Falling)
         {
            initClock = 0x01; // initial level of clock signal is H
            clockDataByteOutInst = 0x10; // Clock Data Byte Shift Out on ve+, MSB first
            clockDataByteInInst = 0x20; // Clock Data Byte Shift In on ve+, MSB first
            clockDataByteInOutInst = 0x30; // Clock Data Byte Shift In/Out on ve+, MSB first
         }
         else
         {
            initClock = 0x00; // initial level of clock signal is L
            clockDataByteOutInst = 0x15; // Clock Data Byte Shift Out on ve-, MSB first
            clockDataByteInInst = 0x25; // Clock Data Byte Shift In on ve-, MSB first
            clockDataByteInOutInst = 0x35; // Clock Data Byte Shift In/Out on ve-, MSB first
         }

         // construct instruction stream for MPSSE
         // Set start condition
         command[index++] = 0x80; // Output GPIO
         command[index++] = (byte) (0x08 | initClock); // BufferEnable, TMS = H, TDI = L
         command[index++] = 0x1B; // GPIOL0, TMS, TDI, CLK is Out, TDO is In.

         // Assert TMS
         command[index++] = 0x80; // Output GPIO
         command[index++] = (byte) (0x00 | initClock); // BufferEnable, TMS = L, TDI = L
         command[index++] = 0x1B; // GPIOL0, TMS, TDI, CLK is Out, TDO is In.

         // Read/Write command for the first N bytes which has read/write buffer
         if (commonLength > 0)
         {
            // Output Data
            command[index++] = clockDataByteInOutInst; // Clock Data Byte In/Out, MSB first
            command[index++] = BitConverter.GetBytes(commonLength - 1)[0]; // lower byte of length
            command[index++] = BitConverter.GetBytes(commonLength - 1)[1]; // upper byte of length

            for (var i = 0; i < commonLength; i++)
               command[index++] = txData[i]; // copy transmit data
         }

         // Optional Write
         if (txData.Length > commonLength)
         {
            // Output Data
            command[index++] = clockDataByteOutInst; // Clock Data Byte Out, MSB first
            command[index++] = BitConverter.GetBytes(txData.Length - commonLength - 1)[0]; // lower byte of length
            command[index++] = BitConverter.GetBytes(txData.Length - commonLength - 1)[1]; // upper byte of length

            for (var i = commonLength; i < txData.Length; i++)
               command[index++] = txData[i]; // copy transmit data
         }
         else if (rxData.Length > commonLength)
         {
            // Optional Read
            // input data
            command[index++] = clockDataByteInInst; // Clock Data Byte In, MSB first
            command[index++] = BitConverter.GetBytes(rxData.Length - commonLength - 1)[0]; // lower byte of length
            command[index++] = BitConverter.GetBytes(rxData.Length - commonLength - 1)[1]; // upper byte of length
         }

         // Deassert TMS
         command[index++] = 0x80; // Output GPIO
         command[index++] = (byte) (0x08 | initClock); // BufferEnable, TMS = H, TDI = L
         command[index++] = 0x1B; // GPIOL0, TMS, TDI, CLK is Out, TDO is In.


         Ftdi.Ftdi.Write(Handle, command, index, out transfered);
        
         if (rxData.Length != 0)
            Ftdi.Ftdi.Read(Handle, rxData, rxData.Length, out transfered);
      }

      /**
             * \brief Set the SRST pin as "L"
             */

      public override void AssertSRST()
      {
         var command = new byte[16];
         var index = 0; // current index of command buffer
         int transfered; // how much data is transfered

         // Deassert TMS
         command[index++] = 0x82; // Output GPIO High Byte
         command[index++] = 0x00; // nTRST=nSRST=L, nTRST=HiZ, nSRST=Out
         //command[index++] = 0x04;                    // nTRST=nSRST=L, nTRST=HiZ, nSRST=Out
         command[index++] = 0x0F; // GPIOL0, All bits are out.

         Ftdi.Ftdi.Write(Handle, command, index, out transfered);
      }

      /**
             * \brief Set the SRST pin as "H"
             */

      public override void DeassertSRST()
      {
         var command = new byte[16];
         var index = 0; // current index of command buffer
         int transfered; // how much data is transfered

         // Deassert TMS
         command[index++] = 0x82; // Output GPIO High Byte
         command[index++] = 0x02; // nTRST=nSRST=L, nTRST=nSRST=HiZ
         //command[index++] = 0x0C;                    // nTRST=nSRST=L, nTRST=nSRST=HiZ
         command[index++] = 0x0F; // GPIOL0, All bits are out.

         Ftdi.Ftdi.Write(Handle, command, index, out transfered);
      }
   }
}