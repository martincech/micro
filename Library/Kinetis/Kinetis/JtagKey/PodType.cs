namespace Kinetis.JtagKey
{
   /// <summary> \brief JTAG POD type definition /// </summary>
   public enum PodType
   {
      AmontecCompatible,
      GnIce,
// ReSharper disable once InconsistentNaming
      OOCDlinkS
   };
}