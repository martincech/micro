using Ftdi;

namespace Kinetis.JtagKey
{
   // ReSharper disable InconsistentNaming
   /// <summary> 
   /// interface for reset pin controls. 
   /// </summary>
   public interface IResetControl
   {
      void EnableTap(ref InputOutputPins gpioH);
      void DisableTap(ref InputOutputPins gpioH);
      void AssertSRST(ref InputOutputPins gpioH);

      void DeassertSRST(ref InputOutputPins gpioH);
      void AssertTRST(ref InputOutputPins gpioH);
      void DeassertTRST(ref InputOutputPins gpioH);
   }
}