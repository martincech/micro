using Ftdi;

namespace Kinetis.JtagKey
{
// ReSharper disable once InconsistentNaming
   public class OOCDlinkResetControl : IResetControl
   {
      public void EnableTap(ref InputOutputPins gpioH)
      {
         gpioH.Pin1 = LogicLevel.H; // deassert TRST;
         gpioH.Pin0 = LogicLevel.L; // enable TRST

         gpioH.Pin3 = LogicLevel.L; // keep it L always
         gpioH.Pin2 = LogicLevel.H; // Hi-Z  SRST
      }

      public void DisableTap(ref InputOutputPins gpioH)
      {
         gpioH.Pin1 = LogicLevel.L; // eassert TRST;
         gpioH.Pin0 = LogicLevel.H; // disable TRST

         gpioH.Pin3 = LogicLevel.L; // keep it L always
         gpioH.Pin2 = LogicLevel.H; // Hi-Z  SRST
      }

      public void AssertSRST(ref InputOutputPins gpioH)
      {
         // state of pin1 is unchanged.
         gpioH.Pin0 = LogicLevel.L; // enable TRST

         gpioH.Pin3 = LogicLevel.L; // SRST = L 
         gpioH.Pin2 = LogicLevel.L; // SRST is output (L)
      }

      public void DeassertSRST(ref InputOutputPins gpioH)
      {
         // state of pin1 is unchanged.
         gpioH.Pin0 = LogicLevel.L; // enable TRST

         gpioH.Pin3 = LogicLevel.L; // Keep it L
         gpioH.Pin2 = LogicLevel.H; // Hi-Z SRST
      }

      public void AssertTRST(ref InputOutputPins gpioH)
      {
         gpioH.Pin1 = LogicLevel.L; // Assert
         gpioH.Pin0 = LogicLevel.L; // enable TRST

         gpioH.Pin3 = LogicLevel.L; // keep it L
         // state of pin2 is unchanged.
      }

      public void DeassertTRST(ref InputOutputPins gpioH)
      {
         gpioH.Pin1 = LogicLevel.H; // Deassert
         gpioH.Pin0 = LogicLevel.L; // enable TRST

         gpioH.Pin3 = LogicLevel.L; // keep it L always
         // state of pin2 is unchanged.
      }
   }
}