using Ftdi;

namespace Kinetis.JtagKey
{
// ReSharper disable InconsistentNaming
   public class GnICEResetControl : AmontecResetControl
   {
      public new void EnableTap(ref InputOutputPins gpioH)
      {
         gpioH.Pin0 = LogicLevel.H; // deassert TRST;
         gpioH.Pin2 = LogicLevel.L; // enable TRST

         gpioH.Pin1 = LogicLevel.H; // This pin is used as nTRST. deassert it.
         gpioH.Pin3 = LogicLevel.H; // Hi-Z  SRST
      }

      public new void DeassertTRST(ref InputOutputPins gpioH)
      {
         gpioH.Pin0 = LogicLevel.H; // Deassert
         gpioH.Pin2 = LogicLevel.L; // enable TRST

         gpioH.Pin1 = LogicLevel.H; // This pin is used as nTRST. deassert it.
         // state of pin3 is unchanged.
      }
   }
}