namespace Kinetis.JtagKey
{
   public class ResetControlFactory
   {
      #region Private fields

      private static ResetControlFactory _self;
      private readonly AmontecResetControl amontec;
      private readonly GnICEResetControl gnice;
      private readonly OOCDlinkResetControl oocdlinkS;

      #endregion


      /// <summary> \brief declare as private. This class is singleton. /// </summary>
      private ResetControlFactory()
      {
         amontec = new AmontecResetControl();
         gnice = new GnICEResetControl();
         oocdlinkS = new OOCDlinkResetControl();
      }

      public IResetControl GetResetControl(PodType aPod)
      {
         switch (aPod)
         {
            case PodType.AmontecCompatible:
               return amontec;
            case PodType.GnIce:
               return gnice;
            case PodType.OOCDlinkS:
               return oocdlinkS;
         }

         return amontec;
      }

      /// <summary> \brief get singnleton instance. /// </summary>
      public static ResetControlFactory Instance()
      {
         return _self ?? (_self = new ResetControlFactory());
      }
   }
}