namespace Kinetis.RomWriter
{
   ///<summary>
   /// \brief basic class for block stracture of Flash
   /// 
   /// This struct hold the size, start address and end adress of each block of flash ROM.
   ///</summary>
   public struct BlockInfo
   {
      public int size;

      ///< size of block in byte
      public int startAddress;

      ///< start byte address
      public int endAddress; ///< end byte address
   }
}