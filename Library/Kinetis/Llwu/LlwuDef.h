//*****************************************************************************
//
//    LlwuDef.h        Llwu K60 definitions
//    Version 1.0      (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __LlwuDef_H__
   #define __LlwuDef_H__

typedef enum {
   LLWU_PTE1,
   LLWU_PTE2,
   LLWU_PTE4,
   LLWU_PTA4,
   LLWU_PTA13,
   LLWU_PTB0,
   LLWU_PTC1,
   LLWU_PTC3,
   LLWU_PTC4,
   LLWU_PTC5,
   LLWU_PTC6,
   LLWU_PTC11,
   LLWU_PTD0,
   LLWU_PTD2,
   LLWU_PTD4,
   LLWU_PTD6,
   LLWU_LPTMR,
   LLWU_CMP0,
   LLWU_CMP1,
   LLWU_CMP2,
   LLWU_RTC_ALARM,
   LLWU_RESERVED,
   LLWU_RTC_SECONDS
} ELlwuPinK60;

#endif