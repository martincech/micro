//*****************************************************************************
//
//    Lptmr.c          Low power timer
//    Version 1.0      (c) VEIT Electronics
//
//*****************************************************************************

#include "Lptmr.h"
#include "Cpu/Cpu.h"
#include "Hardware.h"

void __irq LPTimer_IRQHandler( void);
// LLWU handler

//*****************************************************************************
// Init
//*****************************************************************************

void LptmrInit( void)
// Initialize module
{
   SIM->SCGC5 |= SIM_SCGC5_LPTIMER_MASK;
   SIM->SOPT1 &= ~SIM_SOPT1_OSC32KSEL_MASK; // RTC as ERCLK32K
   SIM->SOPT1 |= SIM_SOPT1_OSC32KSEL(2); // RTC as ERCLK32K

   LPTMR0->CSR = 0; // reset
   LPTMR0->PSR = LPTMR_PSR_PRESCALE(4) | LPTMR_PSR_PCS(2); // ERCLK32K as source for LPTMR
   LPTMR0->CMR = 32678ull * LPTMR_PERIOD / 32 / 1000;
   LPTMR0->CSR |= LPTMR_CSR_TCF_MASK; // clear flag
   CpuIrqAttach(LPTimer_IRQn, LPTIMER_INTERRUPT_PRITORITY, LPTimer_IRQHandler);
   CpuIrqEnable(LPTimer_IRQn);
   LPTMR0->CSR = LPTMR_CSR_TIE_MASK | LPTMR_CSR_TEN_MASK;
} // LlwuInit

//-----------------------------------------------------------------------------

//*****************************************************************************
// IRQ Handler
//*****************************************************************************

void __irq LPTimer_IRQHandler( void)
// LPTimer handler
{
   LPTMR0->CSR |= LPTMR_CSR_TCF_MASK; // clear flag
   LPTmrEvent();
} // Handler