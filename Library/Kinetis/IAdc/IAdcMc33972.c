//*****************************************************************************
//
//    IAdcMc33972.c     Internal A/D convertor using Mc33972 analog mux
//    Version 1.0  	(c) Veit Electronics
//
//*****************************************************************************

#include "IAdc/IAdc.h"
#include "Mc33972/Mc33972DaisyChain.h"
#include "Hardware.h"
#include "System/System.h"

#define MUX_STABILISATION_TIME_US   10

#define RESOLUTION_8BIT   0
#define RESOLUTION_10BIT  2
#define RESOLUTION_12BIT  1
#define RESOLUTION_16BIT  3

#define AdcEnableClock()      Adc0EnableClock()
#define ADC                   ADC0

#define Adc0EnableClock()   SIM->SCGC6 |= SIM_SCGC6_ADC0_MASK;
#define Adc1EnableClock()   SIM->SCGC3 |= SIM_SCGC3_ADC1_MASK;


#define Resolution( Value)   ADC_CFG1_MODE( Value)
#define ConfigModule( Value) ADC->CFG1 = (Value)

#define SelectChannel( Value) ADC->SC1[0] = (Value)

#define StartConversion()   SelectChannel( IADC_CHANNEL);
#define ConversionDone()    (ADC->SC1[0] & ADC_SC1_COCO_MASK)
#define ConversionResult()  (ADC->R[0] & ADC_R_D_MASK)

static inline void MuxSelect( byte Channel);
// Mc33972 mux select

//-----------------------------------------------------------------------------
// Init
//-----------------------------------------------------------------------------

void IAdcInit( void)
// Initialize convertor
{
   Mc33972Init( IADC_MC33972_CHANNEL);

   // Reset
   Mc33972DaisyChainStart( DIN_MC33972_CHANNEL);
   Mc33972Reset( DIN_MC33972_CHANNEL);
   Mc33972Reset( DIN_MC33972_CHANNEL);
   Mc33972DaisyChainStop( DIN_MC33972_CHANNEL);

   AdcEnableClock();
   ConfigModule( Resolution( RESOLUTION_16BIT));
}

//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------

word IAdcRead( byte Channel)
// Returns measured <Channel> value
{
   MuxSelect( Channel);
   SysUDelay( MUX_STABILISATION_TIME_US);
   StartConversion();
   while(!ConversionDone());
   return ConversionResult();
}

//******************************************************************************

//-----------------------------------------------------------------------------
// Mux select
//-----------------------------------------------------------------------------

static inline void MuxSelect( byte Channel)
// Mux select
{
   Mc33972DaisyChainStart( IADC_MC33972_CHANNEL);

   if(Channel < 1 * MC33972_CHANNELS_COUNT) {
      Mc33972AnalogSelect( IADC_MC33972_CHANNEL, Channel - 0 * MC33972_CHANNELS_COUNT);
      Mc33972Read( IADC_MC33972_CHANNEL);
   } else {
      Mc33972Read( IADC_MC33972_CHANNEL);
      Mc33972AnalogSelect( IADC_MC33972_CHANNEL, Channel - 1 * MC33972_CHANNELS_COUNT);
   }

   Mc33972DaisyChainStop( IADC_MC33972_CHANNEL);
}