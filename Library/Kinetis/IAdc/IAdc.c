//*****************************************************************************
//
//    IAdcMc33972.c     Internal A/D convertor using Mc33972 analog mux
//    Version 1.0  	(c) Veit Electronics
//
//*****************************************************************************

#include "IAdc/IAdc.h"

#define F_ADC_INPUT  (F_BUS / 2)
#define F_ADC        12000000ull

#define RESOLUTION_8BIT   0
#define RESOLUTION_10BIT  2
#define RESOLUTION_12BIT  1
#define RESOLUTION_16BIT  3

#define F_DIVIDER_1       0
#define F_DIVIDER_2       1
#define F_DIVIDER_4       2
#define F_DIVIDER_8       3

#define F_INPUT_BUS       0
#define F_INPUT_BUS2      1
#define F_INPUT_ATLCLK    2
#define F_INPUT_ADACK     3

#define AdcEnableClock()      Adc0EnableClock()
#define ADC                   ADC0

#define Resolution( Value)   ADC_CFG1_MODE( Value)
#define FInput( Value)       ADC_CFG1_ADICLK( Value)
#define FDivider( Value)     ADC_CFG1_ADIV( Value)
#define ConfigModule( Value) ADC->CFG1 = (Value)

#define SelectChannel( Value) ADC->SC1[0] = (Value)

#define StartConversion( Channel)   SelectChannel( Channel);
#define ConversionDone()    (ADC->SC1[0] & ADC_SC1_COCO_MASK)
#define ConversionResult()  (ADC->R[0] & ADC_R_D_MASK)

#define F_DIVIDER    F_BUS / F_ADC

//-----------------------------------------------------------------------------
// Init
//-----------------------------------------------------------------------------

void IAdcInit( void)
// Initialize convertor
{
   AdcEnableClock();
   ConfigModule( Resolution( RESOLUTION_16BIT) | FInput(F_INPUT_BUS2) | FDivider(F_DIVIDER_2));
}

//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------

word IAdcRead( byte Channel)
// Returns measured <Channel> value
{
   StartConversion(Channel);
   while(!ConversionDone());
   return ConversionResult();
}