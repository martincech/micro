//*****************************************************************************
//
//    UsartReg.h   Kinetis USART register control
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __UsartReg_H__
   #define __UsartReg_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

typedef volatile UART_Type TUsart;

//-----------------------------------------------------------------------------
//  Constants
//-----------------------------------------------------------------------------

#define UART_S1_ERROR_MASK         (UART_S1_OR_MASK | UART_S1_FE_MASK | UART_S1_PF_MASK)
#define UART_C3_IE_MASK         (UART_C3_ORIE_MASK | UART_C3_FEIE_MASK | UART_C3_PEIE_MASK)
#define UART_C2_IE_MASK         (UART_C2_TIE_MASK | UART_C2_TCIE_MASK | UART_C2_RIE_MASK)

//-----------------------------------------------------------------------------
//  Functions
//-----------------------------------------------------------------------------

// basic operations :
#define usartRxReady( Module)      ((Module)->S1 & UART_S1_RDRF_MASK)
#define usartRxError( Module)      ((Module)->S1 & UART_S1_ERROR_MASK)
#define usartTxReady( Module)      ((Module)->S1 & UART_S1_TDRE_MASK)
#define usartReset( Module)         usartStatusReset( Module)

// data buffers :
#define usartTxData( Module, c)     (Module)->D = (((uint8_t)(((uint8_t)(c))<<UART_D_RT_SHIFT))&UART_D_RT_MASK)
#define usartTxDataBit8( Module, b) if(b){(Module)->C3 |= UART_C3_T8_MASK;}else{(Module)->C3 &= ~(UART_C3_T8_MASK);}
#define usartRxData( Module)        (uint8_t)(((uint8_t)((Module)->D & UART_D_RT_MASK)) >> UART_D_RT_SHIFT)
#define usartRxDataBit8( Module)    (uint8_t)(((Module)->C3 & UART_C3_R8_MASK) >> UART_C3_R8_SHIFT)

#define usartRxFifoSize( Usart)    UART_PFIFO_RXFIFOSIZE( Usart->PFIFO)
// interrupts :
#define usartInterruptDisable( Module)       (Module)->C3 &= ~UART_C3_IE_MASK; (Module)->C2 &= ~(UART_C2_IE_MASK)

#define usartRxInterruptEnable( Module)      (Module)->C2 |= UART_C2_RIE_MASK; (Module)->C3 |= UART_C3_IE_MASK
#define usartRxInterruptDisable( Module)     (Module)->C2 &= ~UART_C2_RIE_MASK; (Module)->C3 &= ~UART_C3_IE_MASK

#define usartTxInterruptEnable( Module)      (Module)->C2 |= UART_C2_TIE_MASK
#define usartTxInterruptDisable( Module)     (Module)->C2 &= ~UART_C2_TIE_MASK

#define usartTxDoneInterruptEnable( Module)  (Module)->C2 |= UART_C2_TCIE_MASK
#define usartTxDoneInterruptDisable(Module)  (Module)->C2 &= ~UART_C2_TCIE_MASK

// status register :
#define usartStatus( Module)            ((Module)->S1) & (((Module)->C2 & UART_C2_IE_MASK) | ((Module)->C3 & UART_C3_IE_MASK))
#define usartStatusRx( Module)         ((Status) & UART_S1_RDRF_MASK)
#define usartStatusRxError( Status)    ((Status) & UART_S1_ERROR_MASK)
#define usartStatusRxFraming( Status)  ((Status) & (UART_S1_FE_MASK | UART_S1_PF_MASK))
#define usartStatusRxOverrun( Status)  ((Status) & UART_S1_OR_MASK)
#define usartStatusTimeout( Status)    

#define usartStatusTx( Status)         ((Status) & UART_S1_TDRE_MASK)
#define usartStatusTxDone( Status)     ((Status) & UART_S1_TC_MASK)

// receive & transmit enable/disable
#define usartRxEnable( Module)            (Module)->C2 |= UART_C2_RE_MASK
#define usartRxDisable( Module)           (Module)->C2 &= ~UART_C2_RE_MASK
#define usartTxEnable( Module)            (Module)->C2 |= UART_C2_TE_MASK
#define usartTxDisable( Module)           (Module)->C2 &= ~UART_C2_TE_MASK

#endif
