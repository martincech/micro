//*****************************************************************************
//
//    Io.h         Freescale Kinetis IO services
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Io_H__
   #define __Io_H__

#include "Hardware.h"
#include "Cpu/uCommon.h"
#include "Unisys/Uni.h"

typedef byte TPin;
typedef byte TFunction;

typedef struct {
   TPin Pin;
   TFunction Function;
} TPinFunction;

//-----------------------------------------------------------------------------
// Port control
//-----------------------------------------------------------------------------

// Pin function
#define PinFunction( PinNumber, Function) \
                                 PORT( N2PORT( PinNumber))->PCR[N2PIN( PinNumber)] &= ~PORT_PCR_MUX_MASK; \
                                 PORT( N2PORT( PinNumber))->PCR[N2PIN( PinNumber)] |= PORT_PCR_MUX(Function)
// Pull
#define PinPullup( PinNumber)    PORT( N2PORT( PinNumber))->PCR[N2PIN( PinNumber)] |= PORT_PCR_PS_MASK | PORT_PCR_PE_MASK;
#define PinPulldown( PinNumber)  PORT( N2PORT( PinNumber))->PCR[N2PIN( PinNumber)] |= PORT_PCR_PS_MASK; \
                                 PORT( N2PORT( PinNumber))->PCR[N2PIN( PinNumber)] &= ~PORT_PCR_PE_MASK
#define PinNoPull(PinNumber)     PORT( N2PORT( PinNumber))->PCR[N2PIN( PinNumber)] &= ~PORT_PCR_PS_MASK

// Slew
#define PinSlewRateSlowEnable(PinNumber)  PORT( N2PORT( PinNumber))->PCR[N2PIN( PinNumber)] |= PORT_PCR_SRE_MASK
#define PinSlewRateSlowDisable(PinNumber) PORT( N2PORT( PinNumber))->PCR[N2PIN( PinNumber)] &= ~PORT_PCR_SRE_MASK

// Slew
#define PinOpenDrainEnable(PinNumber)  PORT( N2PORT( PinNumber))->PCR[N2PIN( PinNumber)] |= PORT_PCR_ODE_MASK
#define PinOpenDrainDisable(PinNumber) PORT( N2PORT( PinNumber))->PCR[N2PIN( PinNumber)] &= ~PORT_PCR_ODE_MASK

//-----------------------------------------------------------------------------
// GPIO
//-----------------------------------------------------------------------------

// Register getters
#define GpioRegister( PinNumber, Register) PT( N2PORT( PinNumber))->Register
#define GpioRegisterSet( PinNumber)        GpioRegister( PinNumber, PSOR)
#define GpioRegisterClr( PinNumber)        GpioRegister( PinNumber, PCOR)
#define GpioRegisterToggle( PinNumber)     GpioRegister( PinNumber, PTOR)
#define GpioRegisterGet( PinNumber)        GpioRegister( PinNumber, PDIR)
#define GpioMask( PinNumber)               PIN_MASK( N2PIN( PinNumber))

// Direction
#define GpioInput( PinNumber)     PT( N2PORT( PinNumber))->PDDR &= ~GPIO_PDDR_PDD( GpioMask( PinNumber))
#define GpioOutput( PinNumber)    PT( N2PORT( PinNumber))->PDDR |= GPIO_PDDR_PDD( GpioMask( PinNumber))

// Gpio state
#define GpioSet( PinNumber)       PT( N2PORT( PinNumber))->PSOR = GPIO_PSOR_PTSO( GpioMask( PinNumber))
#define GpioClr( PinNumber)       PT( N2PORT( PinNumber))->PCOR = GPIO_PCOR_PTCO( GpioMask( PinNumber))
#define GpioToggle( PinNumber)    PT( N2PORT( PinNumber))->PTOR = GPIO_PTOR_PTTO( GpioMask( PinNumber))
#define GpioGet( PinNumber)       (PT( N2PORT( PinNumber))->PDIR & GPIO_PDIR_PDI(  GpioMask( PinNumber)))

//-----------------------------------------------------------------------------
// GPIO Bus
//-----------------------------------------------------------------------------

#define GpioBusEnable( PinNumber, Mask)    PORT( N2PORT( PinNumber))->GPCLR =  (((Mask << N2PIN( PinNumber)) & 0xFF) << 16) | PORT_PCR_MUX(KINETIS_GPIO_FUNCTION); \
                                           PORT( N2PORT( PinNumber))->GPCHR =  ((Mask << N2PIN( PinNumber)) & 0xFF00)       | PORT_PCR_MUX(KINETIS_GPIO_FUNCTION)

#define GpioBusInput( PinNumber, Mask)     PT( N2PORT( PinNumber))->PDDR &= ~GPIO_PDDR_PDD( Mask << N2PIN( PinNumber))
#define GpioBusOutput( PinNumber, Mask)    PT( N2PORT( PinNumber))->PDDR |=  GPIO_PDDR_PDD( Mask << N2PIN( PinNumber))

#define GpioBusPullup( PinNumber, Mask)    PORT( N2PORT( PinNumber))->GPCLR =  (((Mask << N2PIN( PinNumber)) & 0xFF) << 16) | PORT_PCR_MUX(KINETIS_GPIO_FUNCTION) | PORT_PCR_PS_MASK | PORT_PCR_PE_MASK; \
                                      PORT( N2PORT( PinNumber))->GPCHR =  ((Mask << N2PIN( PinNumber)) & 0xFF00)       | PORT_PCR_MUX(KINETIS_GPIO_FUNCTION) | PORT_PCR_PS_MASK | PORT_PCR_PE_MASK

#define GpioBusNoPullup( PinNumber, Mask)  GpioBusEnable( Gpio, Mask)

#define GpioBusSet( PinNumber, Mask)       PT( N2PORT( PinNumber))->PSOR = GPIO_PSOR_PTSO( Mask << N2PIN( PinNumber)); Nop();Nop();Nop();Nop(); Nop();Nop();Nop();Nop()
#define GpioBusClr( PinNumber, Mask)       PT( N2PORT( PinNumber))->PCOR = GPIO_PCOR_PTCO( Mask << N2PIN( PinNumber)); Nop();Nop();Nop();Nop(); Nop();Nop();Nop();Nop()
#define GpioBusToggle( PinNumber, Mask)    PT( N2PORT( PinNumber))->PTOR = GPIO_PTOR_PTTO( Mask << N2PIN( PinNumber))
#define GpioBusGet( PinNumber, Mask)       ((PT( N2PORT( PinNumber))->PDIR & GPIO_PDIR_PDI( Mask << N2PIN( PinNumber))) >> N2PIN( PinNumber))

#include "Cpu/Eint.h"

#endif
