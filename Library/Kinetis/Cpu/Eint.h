//*****************************************************************************
//
//    Eint.h       External interrupt services
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Eint_H__
   #define __Eint_H__

#ifndef __uCommon_H__
   #include "Cpu/uCommon.h"
#endif

// External interrupt sense :
#define EINT_SENSE_LOW_LEVEL    0x8
#define EINT_SENSE_RISING_EDGE  0x9
#define EINT_SENSE_FALLING_EDGE 0xA
#define EINT_SENSE_ANY_EDGE     0xB
#define EINT_SENSE_HIGH_LEVEL   0xC

// External interrupt commands :
//#define EintDisableAll()        
#define EintDisable( PinNumber)         PORT( N2PORT( PinNumber))->PCR[N2PIN( PinNumber)] &= ~PORT_PCR_IRQC_MASK
#define EintEnable( PinNumber, Sense)   EintDisable( PinNumber);PORT( N2PORT( PinNumber))->PCR[N2PIN( PinNumber)] |=  PORT_PCR_IRQC(Sense) 
//#define EintSense( n, Sense)
#define EintClearFlag( PinNumber)               PORT( N2PORT( PinNumber))->PCR[N2PIN( PinNumber)] |= PORT_PCR_ISF_MASK
#define EintGetFlag( PinNumber)                 (PORT( N2PORT( PinNumber))->PCR[N2PIN( PinNumber)] & PORT_PCR_ISF_MASK)

#endif
