//******************************************************************************
//
//   Pwm.c          PWM controller
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __Pwm_H__
   #define __Pwm_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

//-----------------------------------------------------------------------------

// PWM channels :
typedef enum {
   PWM_CHANNEL_0,
   PWM_CHANNEL_1,
   _PWM_CHANNEL_COUNT
} TPwmChannel;

//-----------------------------------------------------------------------------

void PwmInit( unsigned FrequencyKHz);
// Initialisation

void PwmStart( int Channel);
// Start PWM

void PwmDutySet( int Channel, unsigned Duty);
// Set PWM <Duty> cycle

void PwmStop( int Channel);
// Stop PWM

#endif
