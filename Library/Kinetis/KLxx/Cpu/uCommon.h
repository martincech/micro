//*****************************************************************************
//
//    uCommon.h   Kinetis definitions
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef __uCommon_H__
   #include "Kinetis/Cpu/uCommon.h"
   #define __uCommon_H__

#include "FtflTranslation.h"

#define PORT_PCR_ODE_MASK           0

#define MCU_FLASH_SECTOR_SIZE     1024

#define CLOCK_DIVIDER_MAX        16 // SIM_CLKDIV max value
#define PLL_MULTIPLIER_MIN       24 // 

// Enable port clocks
#define PortsEnable()   SIM->SCGC5 |=   SIM_SCGC5_PORTA_MASK | \
                                        SIM_SCGC5_PORTB_MASK
// Disable port clocks
#define PortsDisable()  SIM->SCGC5 &= ~(SIM_SCGC5_PORTA_MASK | \
                                        SIM_SCGC5_PORTB_MASK

#define Uart0ClockEnable()    SIM->SOPT2 |= SIM_SOPT2_UART0SRC(3); \
                              SIM->SCGC4 |= SIM_SCGC4_UART0_MASK
#define Uart0ClockDisable()   SIM->SOPT2 &= ~SIM_SOPT2_UART0SRC_MASK; \
                              SIM->SCGC4 &= ~SIM_SCGC4_UART0_MASK

#define LpTimerClockEnable()       SIM->SCGC5 |= SIM_SCGC5_LPTMR_MASK
#define LpTimerClockDisable()      SIM->SCGC5 &= ~SIM_SCGC5_LPTMR_MASK

#define Iic0ClockEnable()       SIM->SCGC4 |= SIM_SCGC4_I2C0_MASK
#define Iic1ClockEnable()       SIM->SCGC4 |= SIM_SCGC4_I2C1_MASK


#define LptmrClockEnable() SIM->SCGC5 |= SIM_SCGC5_LPTMR_MASK
#define LptmrClockDisable() SIM->SCGC5 &= ~SIM_SCGC5_LPTMR_MASK

#define LptmrClockEnable() SIM->SCGC5 |= SIM_SCGC5_LPTMR_MASK
#define LptmrClockDisable() SIM->SCGC5 &= ~SIM_SCGC5_LPTMR_MASK

#define Adc0EnableClock()  SIM->SCGC6 |= SIM_SCGC6_ADC0_MASK
#define Adc0DisableClock() SIM->SCGC6 &= ~SIM_SCGC6_ADC0_MASK

#endif