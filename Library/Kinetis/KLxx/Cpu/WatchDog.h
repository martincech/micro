//*****************************************************************************
//
//    WatchDog.h     KLxx WatchDog
//    Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __WatchDog_H__
   #define __WatchDog_H__

#include "Unisys/Uni.h"

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void WatchDogInit( void);
// Initialize watchdog

void WatchDog( void);
// Refresh watchdog

#endif