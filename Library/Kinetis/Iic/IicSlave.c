//*****************************************************************************
//
//   IicSlave.c   Kinetis I2C slave interface
//   Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Iic/IicSlave.h"
#include "Hardware.h"
#include "Cpu/Cpu.h"

typedef enum {
   IDLE,
   STOP,
   ADDRESSED,
   TRANSFER
} TIicEvent;

static byte Event;
static TYesNo Busy;

#define IIC       I2C0

#define Reset()                      IIC->C1 = 0
#define ConfigureModule( Val)        IIC->C1 = Val
#define Enable()                     I2C_C1_IICEN_MASK
#define WakeupEnable()               I2C_C1_WUEN_MASK
#define SetAddress( Address)         IIC->A1 = Address << 1
#define IicInterruptEnable( )        IIC->C1 |= I2C_C1_IICIE_MASK
#define IicInterruptDisable( )       IIC->C1 &= ~I2C_C1_IICIE_MASK
#define FastAckEnable()              IIC->SMB | I2C_SMB_FACK_MASK
#define StopInterruptEnable()        IIC->FLT |= I2C_FLT_STOPIE_MASK
#define StopInterruptDisable()       IIC->FLT &= ~I2C_FLT_STOPIE_MASK
#define SetTransmitMode()            IIC->C1 |= I2C_C1_TX_MASK
#define SetReceiveMode()             IIC->C1 &= ~I2C_C1_TX_MASK
#define ConfigureBaudRate( Mult, Prescaler)  (IIC->F = I2C_F_MULT(Mult) | I2C_F_ICR(Prescaler))

#define Addressed()              (IIC->S   & I2C_S_IAAS_MASK)
#define TransferComplete()       (IIC->S   & I2C_S_TCF_MASK)
#define StopFlag()               (IIC->FLT & I2C_FLT_STOPF_MASK)
#define ArbitrationLost( )       (IIC->S   & I2C_S_ARBL_MASK)

#define ArbitrationLostClear( )   IIC->S |= I2C_S_ARBL_MASK
#define Flag()                   (IIC->S & I2C_S_IICIF_MASK)
#define ClearFlag()               IIC->S |= I2C_S_IICIF_MASK
#define ClearStopFlag()           IIC->FLT |= I2C_FLT_STOPF_MASK

#define ArbitrationLost( )       (IIC->S & (I2C_S_ARBL_MASK))
#define ArbitrationLostClear( )  (IIC->S |= I2C_S_ARBL_MASK)

#define ReadWrite()              (IIC->S & I2C_S_SRW_MASK) ? YES : NO
#define Acknowledge()             IIC->C1 &= ~I2C_C1_TXAK_MASK 
#define NotAcknowledge()          IIC->C1 |= I2C_C1_TXAK_MASK
#define Acknowledged()          !(IIC->S & I2C_S_RXAK_MASK)
#define LineBusy()               (IIC->S & I2C_S_BUSY_MASK)

#define Send( Val)               (IIC->D = Val)
#define Read( )                  (IIC->D)

void __irq I2C0_IRQHandler( void);
// ISR

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void IicSlaveInit( void)
// Initialize bus
{
   
   PinFunction(IIC_SLAVE_SDA_PIN, IIC_SLAVE_SDA_PIN_FUNCTION);
   PinFunction(IIC_SLAVE_SCL_PIN, IIC_SLAVE_SCL_PIN_FUNCTION);
   PinPullup(IIC_SLAVE_SDA_PIN);
   PinPullup(IIC_SLAVE_SCL_PIN);
/*
   Iic0ClockEnable();
   Reset();

   SetAddress( IIC_SLAVE_ADDRESS);
   ConfigureBaudRate(2, 20);
   ConfigureModule( Enable() | WakeupEnable());
   //StopInterruptEnable();
   IicInterruptEnable();
   IIC->FLT |= I2C_FLT_SHEN_MASK;
   CpuIrqAttach(I2C0_IRQn, 0, I2C0_IRQHandler);
   CpuIrqEnable( I2C0_IRQn);*/
   

   SIM->SCGC4 |= SIM_SCGC4_I2C0_MASK;
   I2C0->C1 = 0x00U;
   I2C0->FLT = (I2C_FLT_STOPF_MASK | I2C_FLT_FLT(0x00));
   I2C0->S = I2C_S_IICIF_MASK;

   CpuIrqAttach(I2C0_IRQn, 0, I2C0_IRQHandler);
   CpuIrqEnable( I2C0_IRQn);

   I2C0->A1 = I2C_A1_AD(0x55);
   I2C0->C2 = I2C_C2_AD(0x00);
   I2C0->FLT = I2C_FLT_FLT(0x00);
   I2C0->SMB = I2C_SMB_SLTF_MASK;
   I2C0->F = (I2C_F_MULT(0x02) | I2C_F_ICR(0x00));

   I2C0->C1 |= I2C_C1_IICEN_MASK;
   I2C0->C1 |= I2C_C1_IICIE_MASK;
   I2C0->C1 |= I2C_C1_WUEN_MASK;

/*

	SIM->SCGC4 |= SIM_SCGC4_I2C0_MASK;
	
	
	I2C0->A1 |= (0x55 << 1);
	
	I2C0->C1 |= (I2C_C1_IICEN_MASK | I2C_C1_IICIE_MASK);
	StopInterruptEnable();
        I2C0->C1 |= I2C_C1_WUEN_MASK;
	CpuIrqAttach(I2C0_IRQn, 0, I2C0_IRQHandler);
   CpuIrqEnable( I2C0_IRQn);*/
} // IicSlaveInit

//-----------------------------------------------------------------------------
// Busy
//-----------------------------------------------------------------------------

TYesNo IicSlaveBusy( void)
// Check for busy
{
   //PinFunction(KINETIS_PIN_PORTB19, KINETIS_GPIO_FUNCTION);
   GpioOutput(KINETIS_PIN_PORTB19);
   if(Busy) {
      GpioClr(KINETIS_PIN_PORTB19);
   } else {
      GpioSet(KINETIS_PIN_PORTB19);
   } 

   return Busy;
} // IicSlaveBusy

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

void IicSlaveExecute( void)
// Execute
{/*
InterruptDisable();
if(IIC->FLT & I2C_FLT_STOPF_MASK) {
      IIC->FLT &= I2C_FLT_STOPF_MASK;
      Busy = NO;
   }
InterruptEnable();*/

byte Data;
   return;
   if(!Flag()) {
      return;
   }

   if(IIC->FLT & 16) {
      IIC->FLT |= 16;
      ClearFlag();
   }
   if(ArbitrationLost()) {
      ArbitrationLostClear();
      ClearFlag();
   }
   if(StopFlag()) {
      ClearStopFlag();
      ClearFlag();
      Acknowledge();
      SetReceiveMode();
      Read();
      IicSlaveStopped();
      Busy = NO;
   } else if(Addressed()) {
      
      ClearFlag();
      IicSlaveAddressed( !ReadWrite());
      Acknowledge();

      if(ReadWrite()) {
         SetTransmitMode();
         if(!IicSlaveByteGet( &Data)) {
            Data = 0xFF;
         }
         Send( Data);
      } else {
         SetReceiveMode();
         Read();
      }
   } else if(TransferComplete()) {
      IIC->S |= I2C_S_TCF_MASK;
      ClearFlag();
      if(ReadWrite()) {
         if(Acknowledged()) { // ACK, Tx byte
            if(!IicSlaveByteGet( &Data)) {
               Data = 0xFF;
            }
            Send( Data);
         } else { // NACK, turn off Tx
            SetReceiveMode();
            Read();
         }
      } else {
         Acknowledge();
         Data = Read();
         IicSlaveByteProcess(Data);
      }
   }

   CpuIrqEnable( I2C0_IRQn);
} // IicSlaveExecute

//*****************************************************************************

//-----------------------------------------------------------------------------
// ISR
//-----------------------------------------------------------------------------

void __irq I2C0_IRQHandler( void)
// ISR
{
volatile byte Status;
volatile byte Status2;
IIC->C1 &= ~I2C_C1_TXAK_MASK;
  Status = IIC->S;
  IIC->S |= (I2C_S_IICIF_MASK | I2C_S_ARBL_MASK);

   if(Status & I2C_S_IAAS_MASK) {
      if((Status & I2C_S_ARBL_MASK) != 0) {
      }
      if((Status & I2C_S_SRW_MASK) != 0) {
         IIC->C1 |= I2C_C1_TX_MASK;
         IIC->D = 0x55;
      } else {
         IIC->C1 &= ~I2C_C1_TX_MASK;
         (void) IIC->D;
      }
   } else {
      if((Status & I2C_S_ARBL_MASK) != 0) {
      } else {
         if((IIC->C1 & I2C_C1_TX_MASK) != 0) {
            if((Status & I2C_S_RXAK_MASK) != 0) {
               IIC->C1 &= ~I2C_C1_TX_MASK;
               (void) IIC->D;
            } else {
               IIC->D = 0x55;
            }
         } else {
            (void) IIC->D;
         }
      }
   }



/*
byte Data;

   if(IIC->FLT & 16) {
      IIC->FLT |= 16;
      ClearFlag();
   }
   if(ArbitrationLost()) {
      ArbitrationLostClear();
      ClearFlag();
   }
   if(StopFlag()) {
      ClearStopFlag();
      ClearFlag();
      Acknowledge();
      SetReceiveMode();
      Read();
      Send(0xFF);
      IicSlaveStopped();
      StopInterruptDisable();
      Busy = NO;
   } else if(Addressed()) {
      Busy = YES;
   PinFunction(KINETIS_PIN_PORTB18, KINETIS_GPIO_FUNCTION);
   GpioOutput(KINETIS_PIN_PORTB18);
   GpioClr(KINETIS_PIN_PORTB18);
   SysUDelay(50000)
   GpioSet(KINETIS_PIN_PORTB18);
      StopInterruptEnable();
      ClearFlag();
      IicSlaveAddressed( !ReadWrite());
      Acknowledge();

      if(ReadWrite()) {
         SetTransmitMode();
         if(!IicSlaveByteGet( &Data)) {
            Data = 0xFF;
         }
         Send( Data);
      } else {
         SetReceiveMode();
         Read();
      }
   } else if(TransferComplete()) {
      IIC->S |= I2C_S_TCF_MASK;
      ClearFlag();
      if(ReadWrite()) {
         if(Acknowledged()) { // ACK, Tx byte
            if(!IicSlaveByteGet( &Data)) {
               Data = 0xFF;
            }
            Send( Data);
         } else { // NACK, turn off Tx
            SetReceiveMode();
            Read();
         }
      } else {
         Acknowledge();
         Data = Read();
         IicSlaveByteProcess(Data);
      }
   }
*/
   //CpuIrqDisable( I2C0_IRQn);
} // I2C0_IRQHandler