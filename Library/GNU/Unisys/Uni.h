//-----------------------------------------------------------------------------
//
//    Uni.h        GNU C/C++ Universal definitions
//    Version 1.0  (c) VEIT Electronics
//
//-----------------------------------------------------------------------------

#ifndef __Uni_H__
   #define __Uni_H__

#include <stddef.h>           // macro offsetof


// exact size Intel-like data types :
typedef unsigned char      byte;
typedef unsigned short     word;
typedef unsigned int       dword;
typedef unsigned long long qword;

// exact size signed types :
typedef signed char        int8;       // signed byte
typedef short              int16;      // signed word
typedef int                int32;      // signed dword
typedef long long          int64;      // signed qword

// exact size unsigned types :
typedef unsigned char      uint8;      // byte
typedef unsigned short     uint16;     // word
typedef unsigned int       uint32;     // dword
typedef unsigned long long uint64;     // qword

// logical data types :
typedef dword         TYesNo;          // portable boolean

typedef enum {
   NO,
   YES
} TYesNoEnum;

// infinite loop :
#ifndef forever
   #define forever     for(;;)
#endif

// mnemonic function aliases :
#define strequ( s1, s2)      !strcmp( s1, s2)
#define striequ( s1, s2)     !stricmp( s1, s2)
#define strnequ( s1, s2, n)  !strncmp( s1, s2, n)
#define strnequi( s1, s2, n) !strncmpi( s1, s2, n)
#define memequ( m1, m2, l)   !memcmp( m1, m2, l)

// string cut copy :
#define strncpyx( s1, s2, n) strncpy( s1, s2, n); (s1)[ n] = '\0';

// sizeof by type definition :
#define TSizeOf( type, item)   sizeof(((type *)0)->item)

// TYesNo coding  ( TYesNo is integer, any nonzero value == YES) :
#define YesNoSet( Item, Value)   Item = NO; if( Value) Item = YES
#define YesNoGet( Value)       ((Value) ? YES : NO)

// Fixed decimal point division :
#define DivCover( n, d)      (((n) + (d) - 1) / (d))    // division n / d, result * d >= n
#define DivInterior( n, d)   ((n) / (d))                // division n / d, result * d <= n
#define IntegerRound( n, m)  (((n) / (m)) * (m))        // integer rounding n to modulus m

// Array operations :
#define ArrayItemSize( Array)   (sizeof( Array[ 0]))                     // array item size
#define ArrayItemCount( Array)  (sizeof( Array) / ArrayItemSize( Array)) // array items count

// structure/union alignment :
#define __packed __attribute__ ((packed))

#define DllImportExport

// prevent compiler warnings about unused function argument
#define UNUSED_ARG(x)   (void)(x)
#endif
