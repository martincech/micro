//******************************************************************************
//
//    Bootloader.c        Uart bootloader
//    Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#include "Bootloader.h"
#include "BootloaderEnterExit.h" // application specific
#include "BootloaderDef.h"       // platform specific
#include "ApplicationTrigger.h"
#include "Flash/IFlash.h"

#include "Fw/Fw.h"
#include <string.h>


typedef enum {
   BOOTLOADER_IDLE,
   BOOTLOADER_DOWNLOADING,
   BOOTLOADER_READY_TO_START_APP,
   BOOTLOADER_START_APP,
   BOOTLOADER_ERROR
} EBootloaderState;

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void BootloaderInit(void)
// Initialisation
{
   if(!BootloaderTriggered() &&
      !BootloaderEntryCondition() && 
       FwValidate()) {
       ApplicationTrigger();
   }

   IFlashInit();
   BootloaderVersion.SerialNumber = IFlashReadOnce(60, &BootloaderVersion.SerialNumber, sizeof(BootloaderVersion.SerialNumber));
   BootloaderVersion.Software = 0x0100;

   BootloaderOnEnter();
} // HidBootloaderInit

//-----------------------------------------------------------------------------
// Shutdown
//-----------------------------------------------------------------------------

void BootloaderShutdown(void)
// Shutdown
{
   State = BOOTLOADER_IDLE;
} // HidBootloaderInit

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

void BootloaderExecute( void)
// Execute
{
   TBootloaderCmd Cmd;
   
   if(BootloaderExitCondition() && FwValidate()) { // check exit condition
      BootloaderShutdown();
      BootloaderOnExit();
      ApplicationTrigger();
   }

   switch(State) {
      case BOOTLOADER_DOWNLOADING:
         switch(FwExecute()) {
            case FW_STATUS_PROCESSING:
               break;

            case FW_STATUS_DONE:
               _State = BOOTLOADER_IDLE;
               break;

            default:
               _State = BOOTLOADER_ERROR;
               break;
         }
         break;

      case BOOTLOADER_START_APP:
         BootloaderShutdown();
         BootloaderOnExit();
         ApplicationTrigger();
         break;

      default:
         break;
   }
} // BootloaderExecute

//-----------------------------------------------------------------------------
// Receive
//-----------------------------------------------------------------------------

void BootloaderReceive(void *Data, word MaxLength)
// Receive up to <MaxLength> bytes
{
   if(State != BOOTLOADER_DOWNLOADING) {
      Status = BOOTLOADER_RECEIVE_ERROR;
      return;
   }
   if(MaxLength < HBOOTLOADER_DATA_LENGTH) {
      Status = BOOTLOADER_RECEIVE_ERROR;
      return;
   }

   ReceiveLength = 0;
   
   if(!UartRxChar( &Ch)) {
      Status = BOOTLOADER_RECEIVE_ERROR;
      return;
   }
   if(Ch != 0x55) {
      Status = BOOTLOADER_RECEIVE_ERROR;
      return;
   }
   
   while(MaxLength--) {
      if(!UartRxChar( &Ch)) {
         Status = BOOTLOADER_RECEIVE_DONE;
      }
      *Data = Ch;
      Data++;
      ReceiveLength++;
   }
} // BootloaderReceive

//-----------------------------------------------------------------------------
// Receive status
//-----------------------------------------------------------------------------

byte BootloaderReceiveStatus( void)
// Receive status
{
   return Status;
} // BootloaderReceiveStatus

//-----------------------------------------------------------------------------
// Receive length
//-----------------------------------------------------------------------------

byte BootloaderReceiveLength( void)
// Received length
{
   return ReceiveLength;
} // BootloaderReceiveLength

//-----------------------------------------------------------------------------
// Timer
//-----------------------------------------------------------------------------

void BootloaderTimer( void)
// Timer
{
} // BootloaderTimer