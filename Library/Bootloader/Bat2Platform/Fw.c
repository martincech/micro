//*****************************************************************************
//
//    Fw.c           Firmware
//    Version 1.0    (c) Veit Electronics
//
//*****************************************************************************

#include "Fw.h"
#include "Bootloader.h"
#include "BootloaderDef.h"
#include "Crypt/Crypt.h"
#include "Fw/FwDef.h"

static TYesNo Done;        // Fw processing done
static TFwDataCrc DataCrc; // Fw Crc
static TApplicationHeader ApplicationHeader; // Application header

static TYesNo CmdRead(TFwCmd *Cmd, word *CmdLength);
// Read <Cmd>, returns success

static void BufferInit( void);
// Init buffer

static TYesNo BufferCheckForNewData( void);
// Checks for new data and stores them to buffer

static dword BufferRead( void *_Data, dword Length);
// Read buffer to <Data> with <Length>, returns bytes read

void MagicValueGet( byte *Magic);
// Get magic value

static TYesNo MagicSet( void);
// Set magic in flash

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void FwInit( void)
// Inits processing
{
   memset((byte *)&ApplicationHeader, 0xFF, sizeof(ApplicationHeader));
   BufferInit();
   FwDataCrcStart( &DataCrc);
   Done = NO;
} // FwInit

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

#warning Pokud byl Cmd na stacku tak to delalo bordel
static TFwCmd Cmd;

byte FwExecute( void)
// Exexute, returns status
{
word i;
word CmdLength;
TApplicationHeader *Header;
dword DataHeaderStart;
dword HeaderStart;
dword HeaderCount;

   if(Done) {
      return FW_STATUS_DONE;
   }

   if(!CmdRead( &Cmd, &CmdLength)) {
      return NO;
   }

   switch(Cmd.Cmd) {
      case FW_DATA:
         //Cmd.Data.Data.Count = 0x10101010;
         //a = FwSizeOfData( Cmd.Data.Data.Count);

  /* __asm__ __volatile__ ("movw r1, 0x82a0\n\t"
                         "movt r1, 0x1fff\n\t"
                         "ldr.w r0, [r1, #1]\n\t");

   __asm__ __volatile__ ("ldr.w r0, [sp, #1]\n\t");*/

         if(CmdLength != FwSizeOfData( Cmd.Data.Data.Count)) {
            return NO;
         }
         if(Cmd.Data.Data.Address < OFFSETOF_APPLICATION || Cmd.Data.Data.Address >  OFFSETOF_APPLICATION + APPLICATION_SIZE - 1 - Cmd.Data.Data.Count) {
            return NO;
         }
         if(Cmd.Data.Data.Address % 8 != 0) {
            return NO;
         }

         for(i = 0 ; i < Cmd.Data.Data.Count ; i++) {
            FwDataCrcProcess(&DataCrc, Cmd.Data.Data.Data[i]);
         }

         if(Cmd.Data.Data.Address > OFFSETOF_APPLICATION_HEADER) {
            DataHeaderStart = 0;
            HeaderStart = Cmd.Data.Data.Address - OFFSETOF_APPLICATION_HEADER;
         } else { 
            DataHeaderStart = Cmd.Data.Data.Address - OFFSETOF_APPLICATION_HEADER;
            HeaderStart = 0;
         }

         if(HeaderStart >= sizeof(TApplicationHeader)) {
         } else if(DataHeaderStart >= Cmd.Data.Data.Count) {
         } else {

            if(Cmd.Data.Data.Count - DataHeaderStart < sizeof(TApplicationHeader) - HeaderStart) {
               HeaderCount = Cmd.Data.Data.Count - DataHeaderStart;
            } else {
               HeaderCount = sizeof(TApplicationHeader) - HeaderStart;
            }
            memcpy((byte *)&ApplicationHeader + HeaderStart, (byte *)Cmd.Data.Data.Data + DataHeaderStart, HeaderCount);
            memset((byte *)Cmd.Data.Data.Data + DataHeaderStart, 0xFF, HeaderCount);
         }

         if(!IFlashProgram(Cmd.Data.Data.Address, Cmd.Data.Data.Data, Cmd.Data.Data.Count)) {
            return NO;
         }
         break;

      case FW_ERASE:
         if(CmdLength != FwSizeOf( Erase)) {
            return NO;
         }
         if(!IFlashErase(OFFSETOF_APPLICATION, OFFSETOF_APPLICATION + APPLICATION_SIZE - 1)) {
            return NO;
         }
         break;

      case FW_NONSENSE:
         if(CmdLength != FwSizeOf( NonSense)) {
            return NO;
         }
         break;

      case FW_TERMINATOR:
         if(CmdLength != FwSizeOf( Terminator)) {
            return NO;
         }
         Header = (TApplicationHeader *) OFFSETOF_APPLICATION_HEADER;
         ApplicationHeader.Version.SerialNumber = BootloaderVersion.SerialNumber;
         if(!IFlashProgram((byte *)(Header) + offsetof(TApplicationHeader, Version), (byte *)(&ApplicationHeader) + offsetof(TApplicationHeader, Version), sizeof(TApplicationHeader) - offsetof(TApplicationHeader, Version))) {
            return NO;
         }
         if(Header->Version.Class != BootloaderVersion.Class) {
            return NO;
         }
         if(Header->Version.Modification != BootloaderVersion.Modification) {
            return NO;
         }
         if(Header->Version.Hardware != BootloaderVersion.Hardware) {
            return NO;
         }
         FwDataCrcEnd( &DataCrc);
         if(DataCrc != Cmd.Data.Terminator.Crc) {
            return NO;
         }
         if(!MagicSet()) {
            return NO;
         }
         Done = YES;
         return FW_STATUS_DONE;

      default:
         return NO;
   }

   return FW_STATUS_PROCESSING;
} // FwExecute

//-----------------------------------------------------------------------------
// Validate
//-----------------------------------------------------------------------------

TYesNo FwValidate( void)
// Validate Fw 
{
byte Magic[BOOTLOADER_MAGIC_SIZE];
byte *MagicFlash = (byte *)OFFSETOF_APPLICATION_HEADER + offsetof(TApplicationHeader, Magic);
   MagicValueGet(Magic);

   if(memcmp(Magic, MagicFlash, BOOTLOADER_MAGIC_SIZE)) {
      return NO;
   }

   return YES;
} // FwValidate

//*****************************************************************************

//-----------------------------------------------------------------------------
// Cmd read
//-----------------------------------------------------------------------------

static TYesNo CmdRead(TFwCmd *Cmd, word *CmdLength)
// Read <Cmd>, returns success
{
byte *Data;
TFwCrc Crc;
TFwCrc CrcReceived;
word i;
   if(BufferRead(CmdLength, sizeof(*CmdLength)) != sizeof(*CmdLength)) {
      return NO;
   }
   if(*CmdLength > sizeof(TFwCmd)) {
      return NO;
   }
   if(BufferRead(Cmd, *CmdLength) != *CmdLength) {
      return NO;
   }
   if(BufferRead(&CrcReceived, sizeof(CrcReceived)) != sizeof(CrcReceived)) {
      return NO;
   }

   Data = (byte *)Cmd;
   FwCrcStart(&Crc);
   for(i = 0; i < *CmdLength; i++) {
      FwCrcProcess( &Crc, *Data);
      Data++;
   }
   FwCrcEnd(&Crc);

   if(Crc != CrcReceived) {
      return NO;
   }

   return YES;
} // CmdRead

//-----------------------------------------------------------------------------
// Magic
//-----------------------------------------------------------------------------

void MagicValueGet( byte *Magic)
// Get magic value
{
byte i;
   for(i = 0 ; i < BOOTLOADER_MAGIC_SIZE ; i++) {
      Magic[i] = (i << 4) | (i & 0x0F);
   }
} // MagicValueGet

static TYesNo MagicSet( void)
// Set magic in flash
{
byte Magic[BOOTLOADER_MAGIC_SIZE];
   MagicValueGet(Magic);
   if(!IFlashProgram(OFFSETOF_APPLICATION_HEADER + offsetof(TApplicationHeader, Magic), Magic, BOOTLOADER_MAGIC_SIZE)) {
      return NO;
   }

   return YES;
} // MagicSet

//-----------------------------------------------------------------------------
// Buffer
//-----------------------------------------------------------------------------

#define DATA_SIZE    64 // transmission size
#define BUFFER_SIZE  (20 * DATA_SIZE)

static byte Data[DATA_SIZE];
static byte Buffer[BUFFER_SIZE];
static word WritePtr;
static word ReadPtr;
static TYesNo ReceiveInited;
static void BufferInit( void)
// Init buffer
{
   WritePtr = 0;
   ReadPtr = 0;
   ReceiveInited = NO;
   CryptInit();
   BufferCheckForNewData();
} // BufferInit

static TYesNo BufferCheckForNewData( void)
// Checks for new data and stores them to buffer
{
   if(!ReceiveInited) {
      dword FreeSpace;
      // Init receive only when free space available
      if(WritePtr < ReadPtr) {
         FreeSpace = ReadPtr - WritePtr;
      } else {
         FreeSpace = BUFFER_SIZE - (WritePtr - ReadPtr);
      }

      if(FreeSpace >= DATA_SIZE) {
         BootloaderReceive(Data, DATA_SIZE);
         ReceiveInited = YES;
      }
   }

   switch(BootloaderReceiveStatus()) {
      case BOOTLOADER_RECEIVE_DONE:
         break;
      case BOOTLOADER_RECEIVE_ACTIVE:
         return YES;
      default:
         return NO;
   }
   ReceiveInited = NO;
   word ReceiveSize = BootloaderReceiveLength();

   #warning Error if whole crypted block not received
   if(EncryptSize(ReceiveSize) > ReceiveSize) {
      return NO;
   }

   Decrypt((byte *)Data, ReceiveSize);

   byte i;
   for( i = 0 ; i < ReceiveSize ; i++) {
      Buffer[WritePtr++] = Data[i];
      if(WritePtr == BUFFER_SIZE) {
         WritePtr = 0;
      }
   }
   return YES;
} // BufferCheckForNewData

static dword BufferRead( void *_Data, dword Length)
// Read buffer to <Data> with <Length>, returns bytes read
{
dword Read = 0;
byte *Data = (byte *) _Data;
word Timeout;
   if(Length == 0) {
      return 0;
   }

   while(Length) {
      Timeout = 0;
      forever {
         if(WritePtr != ReadPtr) {
            break;
         }
         if(!BufferCheckForNewData()) {
            return Read;
         }
      }

      *Data = Buffer[ReadPtr++];
      if(ReadPtr == BUFFER_SIZE) {
         ReadPtr = 0;
      }
      Read++;
      Data++;
      Length--;
   }

   return Read;
} // BufferRead