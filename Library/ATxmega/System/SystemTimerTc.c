//******************************************************************************
//
//    SystemTimerTc.c  System timer
//    Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#include "Hardware.h"
#include "System/System.h"

#define PRESCALER    64

#define PERIOD    (TIMER_PERIOD * F_PER / 64 / 1000)

// timer interrupt :
#define timerEnable()        TCC0.CTRLA = TC_CLKSEL_DIV64_gc
#define timerDisable()       TCC0.CTRLA = 0

// timer initialization :
#define timerInit()          sysclk_enable_module(SYSCLK_PORT_C, SYSCLK_TC0); TCC0.INTCTRLA = TC_OVFINTLVL_LO_gc; TCC0.PER = PERIOD; timerEnable()

// run user executive
#define timerExecute()       SysTimerExecute()                            

// timer handler header :
#define timerHandler()       ISR(TCC0_OVF_vect)

// timer interrupt confirm :
#define timerInterruptConfirm()
