//*****************************************************************************
//
//   IicSlave.h   Avr I2C slave interface
//   Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Iic/IicSlave.h"
#include "Hardware.h"
#include "Cpu/Cpu.h"

#define TWI_ADDRESS        0x55

#define TWI_BASE        (TWIC.SLAVE)
#define TWI_vect        TWIC_TWIS_vect

#define TwicClockEnable()     PR.PRPC &= ~PR_TWI_bm

volatile TYesNo Busy;

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void IicSlaveInit( void)
// Initialize bus
{
   TwicClockEnable();

   
   
   TWI_BASE.CTRLA =  TWI_SLAVE_INTLVL_LO_gc |
                     TWI_SLAVE_DIEN_bm |
                     TWI_SLAVE_APIEN_bm |
                     TWI_SLAVE_PIEN_bm |
                     TWI_SLAVE_ENABLE_bm;
   TWI_BASE.ADDR = TWI_ADDRESS << 1;
   
   PORTC.PIN0CTRL = PORTC.PIN1CTRL = PORT_SRLEN_bm | PORT_OPC_PULLUP_gc;
   
   PORTC.PIN2CTRL = PORTC.PIN3CTRL = PORT_SRLEN_bm | PORT_INVEN_bm;
TWIC.CTRL = TWI_EDIEN_bm;
   Busy = NO;
} // IicSlaveInit

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

void IicSlaveExecute( void)
//
{
byte Data;
byte Status = TWI_BASE.STATUS;

   if ((Status & (TWI_SLAVE_DIF_bm | TWI_SLAVE_APIF_bm | TWI_SLAVE_BUSERR_bm)) == 0x00) {
      TWI_BASE.CTRLA = TWI_SLAVE_INTLVL_LO_gc |
         TWI_SLAVE_DIEN_bm |
         TWI_SLAVE_APIEN_bm |
         TWI_SLAVE_PIEN_bm |
         TWI_SLAVE_ENABLE_bm;
      return;
   }
   
   if ((Status & (TWI_SLAVE_DIF_bm | TWI_SLAVE_DIR_bm)) == TWI_SLAVE_DIF_bm) {
      Data = TWI_BASE.DATA;  
      IicSlaveByteProcess(Data);
      /* ACK */
      TWI_BASE.CTRLB = TWI_SLAVE_CMD_RESPONSE_gc;
      TWI_BASE.STATUS |= TWI_SLAVE_DIF_bm;
   } else if ((Status & (TWI_SLAVE_DIF_bm | TWI_SLAVE_DIR_bm)) == (TWI_SLAVE_DIF_bm | TWI_SLAVE_DIR_bm)) {
      /*if ((status & TWI_SLAVE_RXACK_bm) == TWI_SLAVE_RXACK_bm) {
         TWI_BASE.CTRLB = TWI_SLAVE_CMD_COMPTRANS_gc;
      } else {*/
      if(!IicSlaveByteGet( &Data)) {
         Data = 0xFF;
      }
      
      TWI_BASE.DATA = Data;//twi_read_reg(reg_address);
      TWI_BASE.CTRLB = TWI_SLAVE_CMD_RESPONSE_gc;
      //}
      
      TWI_BASE.STATUS |= TWI_SLAVE_DIF_bm;
   } else if ((Status & (TWI_SLAVE_APIF_bm | TWI_SLAVE_AP_bm)) == TWI_SLAVE_APIF_bm) {
      /* Stop condition */
      IicSlaveStopped();
      TWI_BASE.STATUS |= TWI_SLAVE_APIF_bm;
      Busy = NO;
   } else if ((Status & (TWI_SLAVE_APIF_bm | TWI_SLAVE_AP_bm)) == (TWI_SLAVE_APIF_bm | TWI_SLAVE_AP_bm)) {
      /*Address match - send ack */
      IicSlaveAddressed( !(Status & TWI_SLAVE_DIR_bm));
      TWI_BASE.CTRLB = TWI_SLAVE_CMD_RESPONSE_gc;
      TWI_BASE.STATUS |= TWI_SLAVE_APIF_bm;
   } else {
      /* Unknown state or error. */
      if ((Status & (TWI_SLAVE_BUSERR_bm | TWI_SLAVE_COLL_bm)) != 0x00) {
         /* Error or collision... */
      }
   }
   
   TWI_BASE.CTRLA =  TWI_SLAVE_INTLVL_LO_gc |
                     TWI_SLAVE_DIEN_bm |
                     TWI_SLAVE_APIEN_bm |
                     TWI_SLAVE_PIEN_bm |
                     TWI_SLAVE_ENABLE_bm;
} // IicSlaveExecute

TYesNo IicSlaveBusy( void)
// Busy
{
TYesNo Ret;

   TWI_BASE.CTRLA &= ~TWI_SLAVE_INTLVL_gm; // disable interrupts
   Ret = Busy;
   TWI_BASE.CTRLA |= TWI_SLAVE_INTLVL_LO_gc; // enable interrupts
   return Ret;
} // IicSlaveBusy

//*****************************************************************************

//-----------------------------------------------------------------------------
// ISR
//-----------------------------------------------------------------------------

ISR(TWI_vect)
{
   TWI_BASE.CTRLA = TWI_SLAVE_ENABLE_bm; // disable interrupts
   Busy = YES;
   IicSlaveEvent();
}