//*****************************************************************************
//
//    Multitasking.c       Dummy Multitasking
//    Version 1.0          (c) Veit Electronics
//
//*****************************************************************************

#include "Multitasking/Multitasking.h"
#include "Hardware.h"
#include "Worker.h"
#include <QThread>
#include <QApplication>
#include <QMutex>
#include "debug/uDebug.h"
#include <QDebug>
#include <QTime>

Worker *MoveToNextWorker();
#include <QHash>
static QHash<TMutex *, QMutex *> Mutexes;
static TTask mainTask{

};


extern "C" void GuiTaskFn( TTask *Task);
// Gui task

extern "C" void WeighingTaskFn( TTask *Task);
// Weighing task

extern "C" void DataPublicationTaskFn( TTask *Task);
// Sms task

extern "C" void RemoteTaskFn( TTask *Task);
// Remote task

extern "C" void GsmTaskFn( TTask *Task);
// Gsm task

extern "C" void TCPIPTaskFn( TTask *Task);
// TCP/IP task
extern "C" void SMSTaskFn(TTask *Task);
// SMS send/Receive task

static char* GetTaskName(Worker *worker)
{
   if( worker->Task->EntryPoint == &GuiTaskFn){
      return "GuiTaskFn";
   }
   if( worker->Task->EntryPoint == &WeighingTaskFn){
      return "WeighingTaskFn";
   }
   if( worker->Task->EntryPoint == &DataPublicationTaskFn){
      return "DataPublicationTaskFn";
   }
   if( worker->Task->EntryPoint == &RemoteTaskFn){
      return "RemoteTaskFn";
   }
   if( worker->Task->EntryPoint == &GsmTaskFn){
      return "GsmTaskFn";
   }

   if( worker->Task->EntryPoint == &TCPIPTaskFn){
      return "TCPIPTaskFn";
   }

   if( worker->Task->EntryPoint == &SMSTaskFn){
      return "SMSTaskFn";
   }
   return "MainTask";
}


void MultitaskingInit( void)
// Initialization
{
   mainTask.Task = QThread::currentThread();
   Worker *worker = new Worker(&mainTask);
   Worker::AllWorkers.append(worker);
}

void MultitaskingTaskRun( TTask *Task)
// Start task
{
  Task->Event = 0;
  Task->Task = new QThread();
  ((QThread *)Task->Task)->setStackSize(Task->StackSize);
  Worker *worker = new Worker(Task);
  Worker::AllWorkers.append(worker);
  worker->moveToThread(((QThread *)Task->Task));
  ((QThread *)Task->Task)->start();
  QObject::connect(((QThread *)Task->Task), SIGNAL(finished()), worker, SLOT(deleteLater()));
  QMetaObject::invokeMethod(worker, "doWork", Qt::QueuedConnection);
  Task->Idle = NO;
}


void MultitaskingTaskExit( TTask *Task)
// Terminates task, waits for task exit
{
   if(Task->Task == NULL){
      return;
   }
   MultitaskingEventSet( Task, K_SHUTDOWN);
   Task->Idle = YES;
   ((QThread *)Task->Task)->quit();
//   MoveToNextWorker();
}


void MultitaskingEventSet( TTask *Task, int Event)
// Set event 0 - 31
{
   Task->Idle = NO;
   if(Event > 31) {
      Task->EventLong = Event;
   } else {
      Task->Event |= 1 << Event;
   }
}

TYesNo MultitaskingTaskIdle( TTask *Task)
// Checks for task idle
{
   return Task->Idle;
}

int MultitaskingTaskScheduler( TTask *Task)
// Task scheduler
{
   int Key;

      if(!Task->EventLong && !Task->Event) {
         Task->Idle = YES;
         return K_IDLE;
      }

      if(Task->EventLong) {
         Key = Task->EventLong;
         Task->EventLong = 0;
         return Key;
      }

      Key = 0;
      forever {
         if(Task->Event & (1 << Key)) {
            Task->Event &= ~(1 << Key);
            return Key;
         }
         Key++;
      }
}
//-----------------------------------------------------------------------------
// Reschedule
//-----------------------------------------------------------------------------
Worker *MoveToNextWorker()
{
   Worker *thisWorker = NULL;
   Worker *nextWorker = NULL;
   int workerCount = Worker::AllWorkers.length();
   for(int i = 0; i < workerCount; i++){
      nextWorker = Worker::AllWorkers[i];
      if(((QThread *)nextWorker->Task->Task) == QThread::currentThread()){
         thisWorker = nextWorker;
         if(i == workerCount - 1){
            nextWorker = Worker::AllWorkers[0];
         } else {
            nextWorker = Worker::AllWorkers[i+1];
         }
         break;
      }
   }
   Q_ASSERT(thisWorker != NULL);
   Q_ASSERT(nextWorker != NULL);

//   qDebug()<< "Reschedule from"<<GetTaskName(thisWorker)<< " to " <<  GetTaskName(nextWorker) << "( " << QTime::currentTime().toString("hh:mm:ss:zzz")<< ")";
//   nextWorker->CanRun = true;
//   thisWorker->CanRun = false;

   if(thisWorker == nextWorker){
      return NULL;
   }
   nextWorker->CanRun.wakeOne();
   return thisWorker;
}

void MultitaskingReschedule( void)
// Reschedule
{
   Worker::RunMutex.lock();
   Worker *thisWorker = MoveToNextWorker();
   if(thisWorker != NULL){
      thisWorker->CanRun.wait(&Worker::RunMutex);
   } else {
      thisWorker = Worker::AllWorkers.at(0);
      delete(thisWorker);
   }
   Worker::RunMutex.unlock();


//   while(!thisWorker->CanRun && Worker::AllWorkers.count() > 1){
//      // wait until can continue
//      QThread::yieldCurrentThread();
//   }
//   qDebug()<< "Task "<<GetTaskName(thisWorker)<<" continues";
}

//-----------------------------------------------------------------------------
// Mutex
//-----------------------------------------------------------------------------

void MultitaskingMutexInit(TMutex *Mutex)
// Init mutex
{
   if(Mutexes.contains(Mutex)){
      return;
   }
   Mutexes.insert(Mutex, new QMutex());
} // MultitaskingMutexInit

void MultitaskingMutexSet( TMutex *Mutex)
// Set mutex
{
   forever {
      if(!MultitaskingMutexTrySet(Mutex)){
         MultitaskingReschedule();
         continue;
      }
      break;
   }
} // MultitaskingMutexSet

TYesNo MultitaskingMutexTrySet( TMutex *Mutex)
// Set mutex
{
   if(!Mutexes.contains(Mutex)){
      return NO;
   }
   if(Mutexes[Mutex]->tryLock()){
      return YES;
   }
   return NO;
} // MultitaskingMutexTrySet

void MultitaskingMutexRelease( TMutex *Mutex)
// Release mutex
{
   if(!Mutexes.contains(Mutex)){
      return;
   }
   Mutexes[Mutex]->unlock();
} // MultitaskingMutexRelease
