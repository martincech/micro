#ifndef WORKER_H
#define WORKER_H

#include "Multitasking/Multitasking.h"
#include <QObject>
#include <QWaitCondition>
#include <QMutex>

class Worker : public QObject
{
   Q_OBJECT
public:
   Worker(TTask *Task);
   ~Worker();
   TTask *Task;

   static QList<Worker*> AllWorkers;
   static QMutex RunMutex;
   QWaitCondition CanRun;
signals:

public slots:
    void doWork();
};

#endif // WORKER_H
