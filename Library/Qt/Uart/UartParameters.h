//******************************************************************************
//
//   UartParameters.h Uart parameters dialog
//   Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#ifndef UARTPARAMETERS_H
#define UARTPARAMETERS_H

#include <QDialog>
#include <QDataStream>
#include "Uart/WinUart.h"

namespace Ui {
class UartParameters;
}

class UartParameters : public QDialog
{
   Q_OBJECT

public:
   explicit UartParameters(QWidget *parent = 0);
   explicit UartParameters(WinUart * uart, QWidget *parent = 0);
   ~UartParameters();
   //---------------------------------------------------------------------------

   QString toString();
   // Returns parameters as string

   TYesNo toUart( WinUart *uart);
   // Set parameters to <uart>
   void fromUart( WinUart *uart);
   // Set parameters from <uart>

   //---------------------------------------------------------------------------
   QString                 getPortName();
   int                     getBaudRate();
   int                     getDataBits();
   WinUart::EParity        getParity();
   WinUart::EStopBits      getStopBits();
   WinUart::EFlowControl   getFlowControl();
   //---------------------------------------------------------------------------
   void setPortName( QString portName);
   void setBaudRate( int baudRate);
   void setDataBits( int dataBits);
   void setParity( WinUart::EParity parity);
   void setStopBits( WinUart::EStopBits stopBits);
   void setFlowControl( WinUart::EFlowControl flowControl);
   //---------------------------------------------------------------------------
public slots:
   TYesNo execute();
   // Execute dialog

private:
   Ui::UartParameters *ui;
   QString                 _portName;
   int                     _baudRate;
   int                     _dataBits;
   WinUart::EParity        _parity;
   WinUart::EStopBits      _stopBits;
   WinUart::EFlowControl   _flowControl;

   void setup();
   void update();
}; // UartParameters

QDataStream &operator<<(QDataStream &out, WinUart *parameters);
QDataStream &operator>>(QDataStream &in, WinUart *parameters);

#endif // UARTPARAMETERS_H
