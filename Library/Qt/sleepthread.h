#ifndef __SLEEPTHREAD_H__
#define __SLEEPTHREAD_H__

#include <QThread>
class SleepThread : public QThread
{
    Q_OBJECT
public:
    static void  sleep(unsigned long secs) { QThread::sleep(secs);}
};

#endif // __SLEEPTHREAD_H__
