#include "cputimer.h"
#include <QDebug>
#include <QTimer>
extern "C"{
#include "fnet.h"
}

void CpuTimerWorker::tick()
{
   fnet_isr_handler(FNET_CFG_CPU_TIMER_VECTOR_NUMBER);
}
CpuTimer::CpuTimer(int msec)
   : QThread()
{
   timeout = msec;
   start();
}

CpuTimer::~CpuTimer()
{
   this->exit();
   while(this->isRunning()){
      currentThread()->yieldCurrentThread();
   }
}

void CpuTimer::run()
{
    QTimer *timer = new QTimer();
    CpuTimerWorker worker;
    connect(timer, SIGNAL(timeout()), &worker, SLOT(tick()));
    timer->start(timeout);

    exec();
    timer->deleteLater();
}
