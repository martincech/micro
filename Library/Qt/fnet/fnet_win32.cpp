extern "C"{
#include "fnet.h"
#include "fnet_eth_prv.h"
}

#include "fnet_win32.h"
#include "fnet_win32_prv.h"
#include "FNetLogWindow.h"
#include "Hardware.h"
#include "System/Delay.h"
#include <stdio.h>
#include <QTimer>
#include <QList>


#include "cputimer.h"
static void fnet_cpu_timer_handler_top( void *)
{
    /* Update RTC counter.
     */
    fnet_timer_ticks_inc();
}


/************************************************************************
*     types and defines
*************************************************************************/
#define FNET_PCAP_RX_BUF_NUM  2
#define FNET_PCAP_BUF_SIZE    (((FNET_CFG_CPU_ETH0_MTU>FNET_CFG_CPU_ETH1_MTU)?FNET_CFG_CPU_ETH0_MTU:FNET_CFG_CPU_ETH1_MTU)+FNET_ETH_HDR_SIZE+FNET_ETH_CRC_SIZE+16)
//#define fnet_eth_trace(str, eth_hdr)

typedef struct
{
   eth_if_t *if_ptr;               /* Pointer to the eth registers. */
   QList<QByteArray> rx_buf;
} fnet_pcap_if_t;

/************************************************************************
*     methods
*************************************************************************/
static int  fnet_pcap_init(fnet_netif_t *netif);
static void fnet_pcap_release(fnet_netif_t *netif);
static int  fnet_pcap_get_hw_addr(fnet_netif_t *netif, unsigned char * hw_addr);
static int  fnet_pcap_is_connected(fnet_netif_t *netif);
static int  fnet_pcap_get_statistics(struct fnet_netif *netif, struct fnet_netif_statistics * statistics);
static void fnet_pcap_output(fnet_netif_t *netif, unsigned short type, const fnet_mac_addr_t dest_addr, fnet_netbuf_t* nb);

static void fnet_pcap_input(eth_if_t *ethif, const unsigned char *data, const int size);

/************************************************************************
*     variables
*************************************************************************/
static CpuTimer *timer = NULL;

static const fnet_netif_api_t fnet_pcap_api =
{
   FNET_NETIF_TYPE_ETHERNET,       /* Data-link type. */
   sizeof(fnet_mac_addr_t),
   fnet_pcap_init,                  /* Initialization function.*/
   fnet_pcap_release,               /* Shutdown function.*/
   #if FNET_CFG_IP4
   fnet_eth_output_ip4,            /* IPv4 Transmit function.*/
   #endif
   fnet_eth_change_addr_notify,    /* Address change notification function.*/
   fnet_eth_drain,                 /* Drain function.*/
   fnet_pcap_get_hw_addr,
   0,
   fnet_pcap_is_connected,
   fnet_pcap_get_statistics
   #if FNET_CFG_MULTICAST
   #if FNET_CFG_IP4
   ,fnet_eth_multicast_join_ip4
   ,fnet_eth_multicast_leave_ip4
   #endif
   #if FNET_CFG_IP6
   ,fnet_eth_multicast_join_ip6
   ,fnet_eth_multicast_leave_ip6
   #endif
   #endif
   #if FNET_CFG_IP6
   ,fnet_eth_output_ip6            /* IPv6 Transmit function.*/
   #endif
};


#if FNET_CFG_CPU_ETH0
static fnet_pcap_if_t fnet_pcap0_if;  /*eth0*/
// ethernet interface
static fnet_eth_if_t fnet_lo_eth0_if =
{
   &fnet_pcap0_if
   ,0
   ,fnet_pcap_output
   #if FNET_CFG_MULTICAST
   ,
   fnet_pcap_multicast_join,
   fnet_pcap_multicast_leave,
   #endif /* FNET_CFG_MULTICAST */
};

fnet_netif_t fnet_eth0_if =
{
   0,                          /* Pointer to the next net_if structure.*/
   0,                          /* Pointer to the previous net_if structure.*/
   "eth0",                     /* Network interface name.*/
   FNET_CFG_CPU_ETH0_MTU,      /* Maximum transmission unit.*/
   &fnet_lo_eth0_if,           /* Points to interface specific data structure.*/
   &fnet_pcap_api              /* Interface API */
};
#endif

#if FNET_CFG_CPU_ETH1
static fnet_pcap_if_t fnet_pcap1_if;  /*eth1*/
// ethernet interface
static fnet_eth_if_t fnet_lo_eth1_if =
{
   &fnet_pcap1_if
   ,0
   ,fnet_pcap_output
   #if FNET_CFG_MULTICAST
   ,
   fnet_pcap_multicast_join,
   fnet_pcap_multicast_leave,
   #endif /* FNET_CFG_MULTICAST */
};

fnet_netif_t fnet_eth1_if =
{
   0,                          /* Pointer to the next net_if structure.*/
   0,                          /* Pointer to the previous net_if structure.*/
   "eth1",                     /* Network interface name.*/
   FNET_CFG_CPU_ETH1_MTU,      /* Maximum transmission unit.*/
   &fnet_lo_eth1_if,           /* Points to interface specific data structure.*/
   &fnet_pcap_api              /* Interface API */
};
#endif


/************************************************************************
*     extern methods implementation
*************************************************************************/

int fnet_cpu_isr_install(unsigned int vector_number, unsigned int priority)
{
   if(vector_number == FNET_CFG_CPU_TIMER_VECTOR_NUMBER){
      return FNET_OK;
   }
   return FNET_ERR;
}

int fnet_cpu_timer_init( unsigned int period_ms )
{
   int result = fnet_isr_vector_init(FNET_CFG_CPU_TIMER_VECTOR_NUMBER, fnet_cpu_timer_handler_top,
                                             fnet_timer_handler_bottom, FNET_CFG_CPU_TIMER_VECTOR_PRIORITY, 0);
   if(result == FNET_OK)
   {
      timer = new CpuTimer(period_ms);
      return FNET_OK;
   }
   return FNET_ERR;
}

void fnet_cpu_timer_release( void )
{
   fnet_isr_vector_release(FNET_CFG_CPU_TIMER_VECTOR_NUMBER);
   delete timer;
}

void fnet_cpu_serial_init(long, unsigned long)
{

}

void fnet_cpu_serial_putchar (long, int character)
{
   FNetLogWindow::putchar((char)character);
}

int fnet_cpu_serial_getchar (long)
{
   return getchar();
}

/************************************************************************
*     internal methods implementation
*************************************************************************/

static int fnet_pcap_init(fnet_netif_t *netif)
{
   fnet_pcap_if_t *ethif;
   int i;

#if FNET_CFG_CPU_ETH0
   if(netif == &fnet_eth0_if){
      ((fnet_pcap_if_t*)((fnet_eth_if_t*)netif->if_ptr)->if_cpu_ptr)->if_ptr = winApiInit(&fnet_pcap_input);
   }
#endif
#if FNET_CFG_CPU_ETH1
   if(netif == &fnet_eth1_if){
      ((fnet_pcap_if_t*)((fnet_eth_if_t*)netif->if_ptr)->if_cpu_ptr)->if_ptr = winApiInit(&fnet_pcap_input);
   }
#endif
   ethif = (fnet_pcap_if_t*)((fnet_eth_if_t*)netif->if_ptr)->if_cpu_ptr;
   if( ethif== NULL){
      return  FNET_ERR;
   }

   return FNET_OK;
}

static void fnet_pcap_release(fnet_netif_t *netif)
{
   eth_if_t *ethif = (eth_if_t *)((fnet_pcap_if_t*)((fnet_eth_if_t*)netif->if_ptr)->if_cpu_ptr)->if_ptr;
   winApiFree(ethif);
}


static void fnet_pcap_output(fnet_netif_t *netif, unsigned short type, const fnet_mac_addr_t dest_addr, fnet_netbuf_t* nb)
{
   eth_if_t *ethif = (eth_if_t *)((fnet_pcap_if_t*)((fnet_eth_if_t*)netif->if_ptr)->if_cpu_ptr)->if_ptr;
   fnet_eth_header_t *ethheader = (fnet_eth_header_t *)malloc(netif->mtu + FNET_ETH_HDR_SIZE);
   int length;

   if((nb!=0) && (nb->total_length<=netif->mtu))
   {
      fnet_netbuf_to_buf(nb, 0, FNET_NETBUF_COPYALL, (void *)((unsigned long)ethheader + FNET_ETH_HDR_SIZE));
      fnet_memcpy (ethheader->destination_addr, dest_addr, sizeof(fnet_mac_addr_t));
      winApiGetMacAddr(ethif, (char *)&ethheader->source_addr);
      ethheader->type=fnet_htons(type);
      length = (FNET_ETH_HDR_SIZE + nb->total_length);
#if FNET_CFG_DEBUG_TRACE_ETH
      fnet_eth_trace("\nTX", ethheader); /* Print ETH header.*/
#endif
      if( winApiOutput( ethif, (const unsigned char *) ethheader, length)){
         ((fnet_eth_if_t *)(netif->if_ptr))->statistics.tx_packet++;
         FNetLogWindow::countersChanged();
      }
      free(ethheader);
   }
   fnet_netbuf_free_chain(nb);
}

int fnet_pcap_get_ip_addr(fnet_netif_t *netif, fnet_ip4_addr_t * ip_addr)
{
   eth_if_t *ethif = (eth_if_t *)((fnet_pcap_if_t*)((fnet_eth_if_t*)netif->if_ptr)->if_cpu_ptr)->if_ptr;
   if( ethif == FNET_NULL){
      return FNET_ERR;
   }
   char ip_str[FNET_IP4_ADDR_STR_SIZE];
   winApiGetIpAddr(ethif, ip_str);
   if(strlen(ip_str) == 0){
      return FNET_ERR;
   }
   fnet_inet_aton(ip_str, (struct in_addr *)ip_addr);
   return FNET_OK;
}

static int fnet_pcap_get_hw_addr(fnet_netif_t *netif, unsigned char * hw_addr)
{
   eth_if_t *ethif ;
   int result;

   if(netif && (netif->api->type==FNET_NETIF_TYPE_ETHERNET)
         && ((ethif = (eth_if_t *)((fnet_pcap_if_t*)((fnet_eth_if_t*)netif->if_ptr)->if_cpu_ptr)->if_ptr) != FNET_NULL)
         && (hw_addr) )
   {
      winApiGetMacAddr(ethif, (char*)hw_addr);
      result = FNET_OK;
   }
   else
   {
      result = FNET_ERR;
   }

   return result;
}

static int fnet_pcap_is_connected(fnet_netif_t *netif)
{
   eth_if_t *ethif ;

   if(netif && (netif->api->type==FNET_NETIF_TYPE_ETHERNET)
         && ((ethif = (eth_if_t *)((fnet_pcap_if_t*)((fnet_eth_if_t*)netif->if_ptr)->if_cpu_ptr)->if_ptr) != FNET_NULL))
   {
      return winApiIsConnected(ethif)? 1 : 0;
   }
   else
   {
      return FNET_ERR;
   }
}

static int fnet_pcap_get_statistics(struct fnet_netif *netif, struct fnet_netif_statistics * statistics)
{
   int result;

   if(netif && (netif->api->type == FNET_NETIF_TYPE_ETHERNET))
   {
      *statistics = ((fnet_eth_if_t *)(netif->if_ptr))->statistics;
      result = FNET_OK;
   }
   else
   {
      result = FNET_ERR;
   }
   return result;
}

#include <QtConcurrentRun>
static void fnet_pcap_input(eth_if_t *ethif, const unsigned char *data, const int size)
{
   fnet_netif_t *netif = (fnet_netif_t *)ethif;
   fnet_eth_header_t * ethheader;
   fnet_netbuf_t * nb=0;
   fnet_mac_addr_t local_mac_addr;
#if FNET_CFG_CPU_ETH0
   if(fnet_pcap0_if.if_ptr == ethif){
      netif = &fnet_eth0_if;
   }
#endif
#if FNET_CFG_CPU_ETH1
   if(fnet_pcap1_if.if_ptr == ethif){
      netif = &fnet_eth1_if;
   }
#endif

   if(size < (int)sizeof(fnet_eth_header_t) ){
      // not even header arrived
      return;
   }
   ethheader = (fnet_eth_header_t *)data;

   /* Just ignore our own "bounced" packets.*/
   winApiGetMacAddr(ethif, (char *)&local_mac_addr);
   if(!fnet_memcmp(ethheader->source_addr, local_mac_addr, sizeof(local_mac_addr)))
   {
      return;
   }
#if !FNET_CFG_CPU_ETH_MIB
   ((fnet_eth_if_t *)(netif->if_ptr))->statistics.rx_packet++;
   FNetLogWindow::countersChanged();
#endif
#if FNET_CFG_DEBUG_TRACE_ETH
   fnet_eth_trace((char*)"\nRX", ethheader); /* Print ETH header.*/
#endif

   nb = fnet_netbuf_from_buf((void *)((unsigned long)ethheader + sizeof(fnet_eth_header_t)),
                               (int)(size - sizeof(fnet_eth_header_t)), FNET_TRUE );
   if(nb)
   {
       if(!fnet_memcmp(ethheader->destination_addr, fnet_eth_broadcast, sizeof(fnet_eth_broadcast)))    /* Broadcast */
       {
           nb->flags|=FNET_NETBUF_FLAG_BROADCAST;
       }

       if (ethheader->destination_addr[0] == 0x01 &&
           ethheader->destination_addr[1] == 0x00 &&
           ethheader->destination_addr[2] == 0x5E) /* Multicast */
       {
           nb->flags|=FNET_NETBUF_FLAG_MULTICAST;
       }
       /* Network-layer input.*/
//       QtConcurrent::run(&fnet_eth_prot_input, netif, nb, ethheader->type);
       fnet_eth_prot_input( netif, nb, ethheader->type );
   }
}

