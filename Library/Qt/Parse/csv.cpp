//******************************************************************************
//
//   Csv.cpp      CSV parser
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "csv.h"
#include <QFile>
#include <QTextStream>

#define UNICODE_CODEC_NAME "UTF-16"

#define CSV_LINE_MAX        2000       // maximum characters per line

#define CSV_ANSI_QUOTATION_LEFT  ((char)0x84)       // left quotation mark symbol
#define CSV_ANSI_QUOTATION_RIGHT ((char)0x93        // right quotation mark symbol

#define CSV_QUOTATION_LEFT  ((wchar_t)0x201E)       // left quotation mark symbol
#define CSV_QUOTATION_RIGHT ((wchar_t)0x201C)       // right quotation mark symbol

#define CSV_APOSTROPHE_LEFT  ((wchar_t)0x201A)      // left apostrophe symbol
#define CSV_APOSTROPHE_RIGHT ((wchar_t)0x2018)      // right apostrophe symbol

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------

Csv::Csv( int columns)
{
   _array         = new ArrayModel( 0, columns);
   _itemSeparator = QChar( ',');
   _textDelimiter = QChar( '"');
   _isUnicode     = false;
   QString text;
   for( int column = 0; column < columns; column++){
      text.sprintf( "%c", (char)(column + 'A'));
      _array->setTitle( column, text);
   }
} // Csv

//------------------------------------------------------------------------------
//  Destructor
//------------------------------------------------------------------------------

Csv::~Csv()
{
   if( _array){
      delete _array;
   }
} // ~Csv

//------------------------------------------------------------------------------
//  Load
//------------------------------------------------------------------------------

bool Csv::load( QString fileName)
// load CSV <fileName> to current array
{
   QFile file( fileName);
   if( !file.open( QIODevice::ReadOnly | QIODevice::Text)){
      return( false);
   }
   QTextStream stream( &file);
   stream.setAutoDetectUnicode( false);        // disable auto detection
   if( _isUnicode){
      stream.setCodec( UNICODE_CODEC_NAME);
   }
   QString     line;
   QStringList items;
   while( !stream.atEnd()){
      line = stream.readLine( CSV_LINE_MAX);
      // split to items :
      items.clear();
      parseLine( line, items);
      _array->addRow( items);
   }
   return( true);
} // load

//------------------------------------------------------------------------------
//  Save
//------------------------------------------------------------------------------

bool Csv::save( QString fileName)
// save current array as <fileName>
{
   QFile file( fileName);
   if( !file.open( QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text)){
      return( false);
   }
   QTextStream stream( &file);
   if( _isUnicode){
      stream.setCodec( UNICODE_CODEC_NAME);
      stream << QChar( QChar::ByteOrderMark);
   }
   int rows    = _array->getRowsCount();
   int columns = _array->getColumnsCount();
   QString    item;
   for( int row = 0; row < rows; row++){
      bool firstItem = true;
      for( int column = 0; column < columns; column++){
         if( !firstItem){
            stream << _itemSeparator;
         }
         item = _array->getItem( row, column);
         item.replace( "\"", QString( QChar( CSV_QUOTATION_LEFT)));
         stream << _textDelimiter;
         stream << item;
         stream << _textDelimiter;
         firstItem = false;
      }
      stream << QChar( '\n');
   }
   return( true);
} // save

//------------------------------------------------------------------------------
//  Unicode
//------------------------------------------------------------------------------

void Csv::setUnicode( bool isUnicode)
// set current <isUnicode> encoding
{
   _isUnicode = isUnicode;
} // setUnicode

//------------------------------------------------------------------------------
//  Item separator
//------------------------------------------------------------------------------

void Csv::setItemSeparator( QChar itemSeparator)
// set items separator character
{
   _itemSeparator = itemSeparator;
} // setItemSeparator

//------------------------------------------------------------------------------
//  Text delimiter
//------------------------------------------------------------------------------

void Csv::setTextDelimiter( QChar textDelimiter)
// set text delimiter character
{
   _textDelimiter = textDelimiter;
} // setTextDelimiter

//------------------------------------------------------------------------------
//  Array
//------------------------------------------------------------------------------

ArrayModel *Csv::array()
// returns current array of strings
{
   return( _array);
} // array

//******************************************************************************

//------------------------------------------------------------------------------
//  Parse
//------------------------------------------------------------------------------

void Csv::parseLine( QString line, QStringList &items)
// parse <line>, returns <items> list
{
   QString item;
   int columns = line.size();
   for( int i = 0; i < columns; i++){
      if( line[i].isSpace()){
         continue;                     // skip whitespace
      }
      item.clear();
      if( !_textDelimiter.isNull() && (line[ i] == _textDelimiter)){
         // word with delimiter
         i++;                          // skip delimiter
         // read word :
         while( i < columns){
            if( line[i] == _textDelimiter){
               i++;                    // skip delimiter
               break;                  // stop at delimiter
            }
            item.append( line[ i]);
            i++;
         }
      } else {
         // word without delimiter
         while( i < columns){
            if( _itemSeparator.isSpace()){
               // separate with spaces
               if( line[ i].isSpace()){
                  break;               // stop at separator
               }
            } else {
               // separate with punctuation character
               if( line[i] == _itemSeparator){
                  break;               // stop at separator
               }
            }
            item.append( line[ i]);
            i++;
         }
      }
      // find item separator :
      while( i < columns){
         if( line[i] == _itemSeparator){
            break;                     // stop at separator
         }
         i++;
      }
      items.append( item);
   }
   // update items :
   QMutableStringListIterator i( items);
   while( i.hasNext()){
      item = i.next();
      item = item.trimmed();           // remove leading & trailing spaces
      // replace quotation marks with ASCII quote :
      item.replace( QChar( CSV_QUOTATION_LEFT),  QChar( '"'));
      item.replace( QChar( CSV_QUOTATION_RIGHT), QChar( '"'));
      // replace apostrophe with ASCII value :
      item.replace( QChar( CSV_APOSTROPHE_LEFT),  QChar( '\''));
      item.replace( QChar( CSV_APOSTROPHE_RIGHT), QChar( '\''));
      i.value() = item;
   }
} // parseLine
