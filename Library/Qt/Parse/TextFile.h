//******************************************************************************
//
//   TextFile.h   Text file utility
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef TEXTFILE_H
   #define TEXTFILE_H

#include <QString>

class TextFile
{
public:
   TextFile();
   TextFile( QString fileName);

   void setFileName( QString fileName);
   // set current <fileName>

   void setUnicode( bool isUnicode);
   // set current <isUnicode> encoding

   QString load();
   // load current file (with current unicode encoding)

   bool save( QString text);
   // save current file (by current unicode encoding)

private :
   QString _fileName;
   bool    _isUnicode;
}; // TextFile

#endif // TEXTFILE_H
