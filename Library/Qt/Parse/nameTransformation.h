//******************************************************************************
//
//   NameTransformation.h  Object name transformation
//   Version 1.0           (c) VEIT Electronics
//
//******************************************************************************

#ifndef NAMETRANSFORMATION_H
   #define NAMETRANSFORMATION_H

#include <QString>

//------------------------------------------------------------------------------

namespace OName
{

   QString toUpperCase( QString name);
   // replace spaces with underscore, all letters in upper case

   QString toLowerCase( QString name);
   // single space only, all letters lowcase

   QString toCamelCase( QString name);
   // remove spaces, first word letter in upcase

   QString toObjectCase( QString name);
   // remove spaces, first word as lowcase remainder as camel case

   QString toText( QString name);
   // single space only, all letters lowcase, first letter upcase

   QString fromUpperCase( QString name);
   // replace underscore with space, first letter upcase, remainder lowcase

   QString fromCamelCase( QString name);
   // insert space before upcase letter, first letter upcase, remainder lowcase

} // OName

//------------------------------------------------------------------------------

#endif // NAMETRANSFORMATION_H
