//******************************************************************************
//
//   Lexer.h      Lexical analyser
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef LEXER_H
   #define LEXER_H

#include <QString>
#include <QVector>

//------------------------------------------------------------------------------
//   Lexical types
//------------------------------------------------------------------------------

typedef enum {
   LEX_UNDEFINED,
   LEX_SPACE,
   LEX_NEWLINE,
   LEX_PUNCTUATION,
   LEX_SYMBOL,
   LEX_WORD,
   LEX_NUMBER,
   LEX_TEXT,
   LEX_COMMENT,
   LEX_END,
   LEX_ERROR,
   _LEX_LAST
} ELexicalType;

//------------------------------------------------------------------------------
//   Lexical units
//------------------------------------------------------------------------------

typedef struct {
   int     type;
   QString value;
   int     line;
   int     column;
} LexicalUnit;

//------------------------------------------------------------------------------
//   Lexical units
//------------------------------------------------------------------------------

typedef QVector<LexicalUnit> LexicalUnits;

//------------------------------------------------------------------------------
//   Lexer
//------------------------------------------------------------------------------

class Lexer
{
public:
   Lexer();
   Lexer( QString text);

   void setText( QString text);
   // set current <text>

   void parse();
   // parse current text

   int count();
   // returns lexical units count

   const LexicalUnit *at( int index);
   // returns units at <index>

   QString view( LexicalUnit &unit);
   // convert <unit> to diagnostic text

//------------------------------------------------------------------------------
private :
   QString      _text;
   LexicalUnits _units;
   int          _line;
   int          _column;
   LexicalUnit  _errorUnit;

   LexicalUnit parseWord( int &index);
   // parse text word at <index>

   LexicalUnit parseNumber( int &index);
   // parse number at <index>

   LexicalUnit parseSpace( int &index);
   // parse spaces

   LexicalUnit parseComment( int &index);
   // parse comment

   LexicalUnit parseText( int &index);
   // parse text string

   LexicalUnit parseOther( int &index);
   // parse punctuation and symbols
}; // TextFile

#endif // LEXER_H
