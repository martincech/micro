//******************************************************************************
//
//   SocketIfEthernet.cpp    Ethernet socket
//   Version 1.0             (c) Veit Electronics
//
//******************************************************************************

#include "SocketIfEthernetQ.h"
#include "Remote/Frame.h"

#define CHUNK_SIZE   1024

//******************************************************************************
// Constructor
//******************************************************************************

SocketIfEthernet::SocketIfEthernet()
   : TcpRawServer()
{
   _State = SOCKET_STATE_DISCONNECTED;
}

#ifdef __WIN32__
SocketIfEthernet::SocketIfEthernet(CrtDump *logger)
   : TcpRawServer()
{
   _State = SOCKET_STATE_DISCONNECTED;
   this->setLogger(logger);
}
#endif
//******************************************************************************
// Destructor
//******************************************************************************

SocketIfEthernet::~SocketIfEthernet()
// Destructor
{
}


byte SocketIfEthernet::State( void)
// Gets state of socket
{
   return _State;
}

TYesNo SocketIfEthernet::Receive( void *Buffer, int Size)
// Receive into <Buffer> with <Size>
{
byte ChunkBuffer[CHUNK_SIZE];
int ReceiveSize;
TFrameState FrameState;
   FrameInit( &FrameState);
   FrameReceiveStart( &FrameState, Buffer, Size);

   forever {
      ReceiveSize = receive( ChunkBuffer, CHUNK_SIZE);
      if(!ReceiveSize) {
         return NO;
      }
      if(!FrameReceiveProccess( &FrameState, ChunkBuffer, ReceiveSize)) {
         return NO;
      }
      switch(FrameState.Status) {
         case FRAME_STATE_RECEIVE_ACTIVE:
            break;
         case FRAME_STATE_RECEIVE_FRAME:
            ReplySize = FrameState.SizeProccessed;
            _State = SOCKET_STATE_RECEIVE_DONE;
            return YES;
         default:
            _State = SOCKET_STATE_RECEIVE_ERROR;
            return NO;
      }
   }
}

int SocketIfEthernet::ReceiveSize( void)
// Gets number of received bytes
{
   return ReplySize;
}

TYesNo SocketIfEthernet::Send( const void *Buffer, int Size)
// Send <Buffer> with <Size>
{
int ChunkSize;
byte ChunkBuffer[CHUNK_SIZE];
TFrameState FrameState;
   FrameInit( &FrameState);
   FrameSendInit( &FrameState, Buffer, Size);

   forever {
      if(!FrameSendProccess( &FrameState, ChunkBuffer, CHUNK_SIZE, &ChunkSize)) {
         return NO;
      }
      if(!send( ChunkBuffer, ChunkSize)) {
         return NO;
      }
      switch(FrameState.Status) {
         case FRAME_STATE_SEND_ACTIVE:
            break;

         case FRAME_STATE_SEND_DONE:
            _State = SOCKET_STATE_SEND_DONE;
            return YES;
            
         default:
            _State = SOCKET_STATE_SEND_ERROR;
            return NO;
      }
   }
}

void SocketIfEthernet::Close( void)
// Close socket
{
   TcpRawServer::close();
}

#include "Memory/File.h"

byte SocketIfEthernet::Permission( void)
//
{
   return FILE_MODE_READ_WRITE;
}

/*
//******************************************************************************
// Connected
//******************************************************************************

bool Bat2TcpRemote::Connected()
// Get connection state
{
   return( _socket && _socket->state() != QAbstractSocket::UnconnectedState);
} // Connected
*/
