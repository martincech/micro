//******************************************************************************
//
//   SocketIfMsd.h         MSD socket
//   Version 1.0           (c) Veit Electronics
//
//******************************************************************************

#ifndef _SocketIfMsdQ_H__
   #define _SocketIfMsdQ_H__

#include "SocketIf.h"

class SocketIfMsd : virtual public SocketIf
{
public :
   SocketIfMsd( void);
   // Constructor
   
   ~SocketIfMsd();
   // Destructor
   
   TYesNo OpenForReceiveCmd( void);
   // Open socket for cmd receive

   TYesNo OpenForSendCmd( void);
   // Open socket for send cmd

   byte State( void);
   // Gets state of socket

   TYesNo Receive( void *Buffer, int Size);
   // Receive into <Buffer> with <Size>

   int ReceiveSize( void);
   // Gets number of received bytes

   TYesNo Send( const void *Buffer, int Size);
   // Send <Buffer> with <Size>

   void Close( void);
   // Close socket
   
   byte Permission( void);
   //

private:
   byte _State;
};

#endif // _SocketIfMsd_H__
