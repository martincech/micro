//*****************************************************************************
//
//   SocketDef.h      Qt socket definitions
//   Version 1.0     (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __SocketDef_H__
   #define __SocketDef_H__

#include "Unisys/Uni.h"

typedef int TSocketDescriptor;

#define SOCKET_COUNT    1

typedef void TSocket;

#endif
