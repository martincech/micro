//******************************************************************************
//
//   SocketIfUart.h   Bat2 remote access through COM port
//   Version 1.0        (c) Veit Electronics
//
//******************************************************************************

#ifndef __SOCKET_IF_UART_H__
   #define __SOCKET_IF_UART_H__

#include "SocketIf.h"
#include "Uart/WinUart.h"

class SocketIfUart : virtual public SocketIf, public WinUart
{
public:
   SocketIfUart();
   // Constructor

   virtual ~SocketIfUart();
   // destructor

   bool connected();

   byte State( void);
   // Gets state of socket

   TYesNo Receive( void *Buffer, int Size);
   // Receive into <Buffer> with <Size>

   int ReceiveSize( void);
   // Gets number of received bytes

   TYesNo Send( const void *Buffer, int Size);
   // Send <Buffer> with <Size>

   void Close( void);
   // Close socket

   TYesNo WriteOnly( void);
   //
private:
   byte _State;
   int ReplySize;
};

#endif // SocketIfUart_H
