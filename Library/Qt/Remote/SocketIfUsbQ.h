//******************************************************************************
//
//   SocketIfUsbQ.h    Bat2 USB
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

#ifndef __SocketIfUsb_H__
   #define __SocketIfUsb_H__

#include "SocketIf.h"
#include "Usb/HidNative/HidNative.h"

//******************************************************************************
// SocketIfUsb
//******************************************************************************

class SocketIfUsb : public SocketIf {
public:
   SocketIfUsb(THid *hid);
   // Constructor

   virtual ~SocketIfUsb();
   // Destructor

   byte State(void);
   // Gets state of socket

   TYesNo Receive(void *Buffer, int Size);
   // Receive into <Buffer> with <Size>

   int ReceiveSize(void);
   // Gets number of received bytes

   TYesNo Send(const void *Buffer, int Size);
   // Send <Buffer> with <Size>

   void Close(void);
   // Close socket

   byte Permission(void);
   //
private:
   int ReplySize;
   THidNative *HidNative;
};

#endif

