//*****************************************************************************
//
//    Socket.cpp         Qt Socket
//    Version 1.0        (c) VEIT Electronics
//
//*****************************************************************************

#include "Remote/Socket.h"
#include "Remote/SocketIf.h"

//------------------------------------------------------------------------------
//  Write only
//------------------------------------------------------------------------------

byte SocketPermission( TSocket *Socket)
// Write only
{
   return ((SocketIf *)Socket)->Permission();
} // SocketWriteOnly

//------------------------------------------------------------------------------
//  State
//------------------------------------------------------------------------------

byte SocketState( TSocket *Socket)
// Gets state of socket
{
   return ((SocketIf *)Socket)->State();
} // SocketState

//------------------------------------------------------------------------------
//  Receive
//------------------------------------------------------------------------------

TYesNo SocketReceive( TSocket *Socket, void *Buffer, int Size)
// Receive into <Buffer> with <Size>
{
   return ((SocketIf *)Socket)->Receive(Buffer, Size);
} // SocketReceive

//------------------------------------------------------------------------------
//  Receive size
//------------------------------------------------------------------------------

int SocketReceiveSize( TSocket *Socket)
// Gets number of received bytes
{
   return ((SocketIf *)Socket)->ReceiveSize();
} // SocketReceiveSize

//------------------------------------------------------------------------------
//  Send
//------------------------------------------------------------------------------

TYesNo SocketSend( TSocket *Socket, const void *Buffer, int Size)
// Send <Buffer> with <Size>
{
   return ((SocketIf *)Socket)->Send(Buffer, Size);
} // SocketSend

//------------------------------------------------------------------------------
//  Close
//------------------------------------------------------------------------------

void SocketClose( TSocket *Socket)
// Close socket
{
   return ((SocketIf *)Socket)->Close();
} // SocketClose
