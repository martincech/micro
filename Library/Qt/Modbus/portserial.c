/* ----------------------- Platform includes --------------------------------*/
#include <stdlib.h>
#include "port.h"
#include "Hardware.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"

// TODO - dummy implementace

void vMBPortSerialEnable( BOOL xRxEnable, BOOL xTxEnable )
{
}

BOOL xMBPortSerialInit( UCHAR ucPORT, ULONG ulBaudRate, UCHAR ucDataBits, eMBParity eParity )
{

   return TRUE;
}

void vMBPortSerialClose( void )
{

}

BOOL xMBPortSerialPutByte( CHAR ucByte )
{
   return TRUE;
}

BOOL xMBPortSerialGetByte( CHAR * pucByte )
{
   pucByte = 0;
   return TRUE;
}

//------------------------------------------------------------------------------
//   Common Modbus Handler
//------------------------------------------------------------------------------

//static void _UsartModbusHandler( TUartAddress Address)
//// Common Modbus handler
//{

//TUartData   *UData;
//TUsart      *Usart;
//TUartDescriptor       *Uart;
//byte        Data;
//unsigned    Status;

//   Uart = UartGet( Address);
//   uartAddress = Address;
//   Usart = UsartGet(Address);
//   Status = usartStatus( Usart);
////------------------------------------------------------------------------------
//   // check for Rx errors :
//   if( usartStatusRxError( Status)){
//      usartStatusReset( Usart);
//      return;
//   }

//   if( usartStatusRx( Status)){
//      pxMBFrameCBByteReceived();
//   }

//   if( usartStatusTx( Status))
//   {
//      pxMBFrameCBTransmitterEmpty(  );
//   }

//   if( usartStatusTxDone( Status))
//   {
//      if( Uart->Data->Status == UART_SEND_ACTIVE){
//         Uart->Data->Status = UART_SEND_DONE;
//         UartTxDisable( Uart);                   // RS485 direction to Rx
//         usartTxDoneInterruptDisable( Usart);
//         UartReceive(uartAddress);
//      }
//   }
//}
