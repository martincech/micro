#ifndef MODELPARSER_H
#define MODELPARSER_H

#include "Model/ArrayModel.h"
#include "ARegister.h"
#include "GeneratorDef.h"

class ModelParser
{
private:
   QStringList GetVersion( ArrayModel *model);
   // get version as string list (major, minor)
   int ParseGroup( RegisterGroup *group, ArrayModel *model, int fromRow);
   // parse register group and return last row of of group
   void updateRangeTo( Register *reg, QString &regValueRangeField);
   //update range to
   void decodeRange( Register *reg, QString &regValueRangeField);
public:
   ModelParser( ArrayModel *model){this->model = model;}
   ARegister *Parse();
   ArrayModel *GetModel(ARegister *reg);
   ArrayModel *GetModel(){return model;}
   QString GetVersionMajor();
   QString GetVersionMinor();
   void SetModelTitle(ArrayModel *model);

private:
   ArrayModel *model;
};

#endif // MODELPARSER_H
