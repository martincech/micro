#include "CodeGenerator.h"
#include "Parse/nameTransformation.h"
#include <typeinfo>       // std::bad_cast
//------------------------------------------------------------------------------
//  Abstract class
//------------------------------------------------------------------------------

CodeGenerator::CodeGenerator(CodeCreator *builder)
{
   SetBuilder(builder);
}

void CodeGenerator::SetBuilder(CodeCreator *builder)
{
   Builder = builder;
}

//------------------------------------------------------------------------------
//  GroupSourceGenerator
//------------------------------------------------------------------------------
QString GroupSourceGenerator::groupMapping( const RegisterGroup *group)
// helper function for group generation - create group mapping definition function
{
Iterator *it;
QString                    functionBody;
int                        regCount;
ARegister                  *reg;
ARegister                  *prevReg;


   prevReg = NULL;
   it = new ConditionalIterator((RegisterGroup*)forGroup, true, true, ~ARegister::REGISTER_STRING_BUFFER);
   while(it->hasNext()){
      reg = it->Next();
      if(!prevReg || prevReg.Number() + 1 != reg->Number()){

      }
   }
   for( int j = 0; j < regCount; j++){
      reg = group->item.at(j);
      if(reg.write && reg.read && (reg.registerType != REGISTER_STRING_BUFFER) && reg.dataName.toLower() != KEYWORD_GLOBAL){
         if( !regSubGroup.isEmpty() && prevReg.num + 1 != reg.num){
            regSubGroups.append(regSubGroup);
            regSubGroup.clear();
         }
         regSubGroup.append( reg);
         prevReg = reg;
      }
   }
   regSubGroups.append(regSubGroup);
   functionBody = "int i;\n";
   for( int i = 0; i < regSubGroups.count(); i++){
      regSubGroup = regSubGroups.at(i);
      if( i > 0){
         functionBody += "   else if(";
      } else {
         functionBody += "   if(";
      }
      functionBody += " R <= " + CodeHeader::constantIdentifier("modbus reg", group->name, regSubGroup.last().name, "")
                      + "){\n";
      functionBody += "      i = R - " + CodeHeader::constantIdentifier("modbus reg", group->name, regSubGroup.first().name, "");
      if( i > 0){
         functionBody += " + 1 \n            + ";
      }
      for(int j = i-1; j >= 0; j--){
         functionBody += CodeHeader::constantIdentifier("modbus reg", group->name,  regSubGroups.at(j).last().name, "") +
                         "\n            - " + CodeHeader::constantIdentifier("modbus reg", group->name,  regSubGroups.at(j).first().name, "");
         if( j != 0){
            functionBody += "\n            + ";
         }
      }
      functionBody += ";\n   }\n";
   }
   functionBody += "\n   return i;\n";
   return CodeSource::functionDefinition("static int", "_GetRegisterMapping", "EModbusRegNum R", "return mapping of discontinuous register number to contiuous array", functionBody);
}

QString GroupSourceGenerator::Generate(ARegister *forGroup)
{
QStringList slist;
QStringList sslist;
IteratorPtr *it;
ARegister   *areg;
Register    *reg;
QString tmpString;

   // name of file
   Builder->SetCodeName("Modbus"+OName::toCamelCase(forGroup->Name()) + "Group");

   //-------------------------------------------------------------------------------------
   // header files includes
   it = new IteratorPtr(forGroup->CreateIterator());
   while((*it)->hasNext()){
      areg = (*it)->Next();
      slist << areg->Header();
   }
   delete( it);
   slist.removeDuplicates();
   slist.removeAll("");
   for(QStringList::iterator i = slist.begin(); i < slist.end(); i++){
      *i = Builder->RepresentInclude("", *i);
   }
   Builder->CreateIncludeFiles(slist);
   slist.clear();
   //-------------------------------------------------------------------------------------
   //Global static variables declaration
   try{
      it = new IteratorPtr(new GlobalRegsGroupIterator(dynamic_cast<RegisterGroup*>(forGroup)));
      while((*it)->hasNext()){
         reg = (Register*)(*it)->Next();
         tmpString = forGroup->Name() + " " + reg->Name();
         slist << Builder->RepresentGlobalVariableDefinition(
                        reg->Locals(),
                        tmpString,
                        reg->DefaultValue(),
                        reg->Description()
                        );
//         reg->SetDataName( reg->DataName().replace(reg->DataName(), tmpString));
      }
      delete(it);
      Builder->CreateVariables(slist);
      slist.clear();
   } catch( std::bad_cast &){
   }
   //-------------------------------------------------------------------------------------
   //local functions declarations && definitions
   it = new IteratorPtr(new ConditionalIterator((RegisterGroup*)forGroup, true, true, ~ARegister::REGISTER_STRING_BUFFER));
   while((*it)->hasNext()){
      areg = (*it)->Next();
      if( areg && !areg->ChildCount() && ((Register *)areg)->DataName().toLower() != KEYWORD_GLOBAL){
         QStringList params;params << "EModbusRegNum R";
         slist << Builder->RepresentFunctionDeclaration("int", "_GetRegisterMapping", params, "return mapping of discontinuous register number to contiuous array");


         break;
      }
   }
   delete(it);
   it = new IteratorPtr(new TypeGroupIterator((RegisterGroup*)forGroup, ARegister::REGISTER_COMMAND));
   while((*it)->hasNext()){
      areg = (*it)->Next();
      if( areg && !areg->ChildCount() &&
          ((Register *)areg)->Name().contains("save")||
          ((Register *)areg)->Name().contains("start")){
         QString SaveExecutiveName =  OName::toCamelCase("_" + OName::fromUpperCase(areg->Name()) + " command executive");
         slist << Builder->RepresentFunctionDeclaration("void", SaveExecutiveName, QStringList(), "execute "+ areg->Name() + " command - apply all parameteres");

//         sslist
      }
   }
   delete(it);
   Builder->CreateFunctions(slist, sslist);


   return Builder->GetResult();
}

//------------------------------------------------------------------------------
//  GroupHeaderGenerator
//------------------------------------------------------------------------------

QString GroupHeaderGenerator::Generate(ARegister *forGroup)
{
QStringList slist;
QStringList sslist;
IteratorPtr *it;

   // name of file
   Builder->SetCodeName("Modbus"+OName::toCamelCase(forGroup->Name()) + "Group");

   // header files includes
   slist << Builder->RepresentInclude("", "Hardware", false);
   slist << Builder->RepresentInclude("", MODBUS_REG_FILENAME);
   Builder->CreateIncludeFiles(slist);
   slist.clear();

   try{
      // function declarations
      it = new IteratorPtr(new RGroupIterator( dynamic_cast<RegisterGroup*>(forGroup)));
      if((*it)->hasNext()){
         QStringList params;params << "EModbusRegNum R";
         slist << Builder->RepresentFunctionDeclaration("word",
                                                        OName::toCamelCase( QString("modbus reg read ") + forGroup->Name()),
                                                        params,
                                                        "Read "+ forGroup->Name() +" register group\n");
      }
      delete( it);
      it = new IteratorPtr(new WGroupIterator( dynamic_cast<RegisterGroup*>(forGroup)));
      if((*it)->hasNext()){
         QStringList params;params << "EModbusRegNum R" << "word D";
         slist << Builder->RepresentFunctionDeclaration("TYesNo",
                                                        OName::toCamelCase( QString("modbus reg write ") + forGroup->Name()),
                                                        params,
                                                        "Write "+ forGroup->Name() +" register group\n");
      }
      delete( it);
   } catch( std::bad_cast &){
   }

   sslist.clear();
   Builder->CreateFunctions(slist, sslist);
   return Builder->GetResult();
}
