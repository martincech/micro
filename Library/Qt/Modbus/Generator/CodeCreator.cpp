//******************************************************************************
//
//   CodeSource.c     Source code generator
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "CodeCreator.h"
#include "Parse/nameTransformation.h"
#include "uGenerator/CodeHeader.h"
//------------------------------------------------------------------------------
//  Helper functions for source creation
//------------------------------------------------------------------------------

namespace CodeSource
{
   //---------------------------------------------------------------------------
   QString includeFileLocal( QString filePath, QString fileName);
   // generate local include statement for <filePath> and <fileName>
   QString includeFileGlobal( QString filePath, QString fileName);
   // generate global include statement for <filePath> and <fileName>
   //---------------------------------------------------------------------------
   QString functionDefinition( QString returnType, QString name, QStringList parameters, QString comment, QString body);
   QString functionDefinition( QString returnType, QString name, QString parameters, QString comment, QString body);
   QString functionDefinition( QString returnType, QString name, const char * parameters, QString comment, QString body);
   // generate function with body
   QString functionDeclaration(QString returnType, QString name, QStringList parameters, QString comment);
   QString functionDeclaration(QString returnType, QString name, QString parameters, QString comment);
   QString functionDeclaration(QString returnType, QString name, const char * parameters, QString comment);
   // generate function declaration
   QString functionCall( QString name, QStringList parameters);
   QString functionCall( QString name, QString     parameters);
   QString functionCall( QString name, const char *parameters);
   // generate function call
   //---------------------------------------------------------------------------
   QString globalVariableDefinition( QString type, QString name, QString defaultValue = "0", QString description = "");
   // generate global variable definition:  type _CamelCaseName; //description
   QString globalVariable( QString name);
   // generate access string to global variable
}

//------------------------------------------------------------------------------
//  Abstract builder constructor
//------------------------------------------------------------------------------

CodeCreator::CodeCreator(QString templateFile)
{
   sourceTemplateFileName = templateFile;
   output.clear();
}

QString CodeCreator::indentation(int times)
{
QString ret;

   for(int i = 0; i < times; i++){
      ret += "   ";
   }
   return ret;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//  C source creator
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Represents
//------------------------------------------------------------------------------

QString CSourceCreator::RepresentInclude(QString Path, QString name, bool local)
{
QString ret;

   if(local){
      ret += CodeSource::includeFileLocal(Path, name);
   } else {
      ret += CodeSource::includeFileGlobal(Path, name);
   }
   return ret;
}

QString CSourceCreator::RepresentFunctionDeclaration(QString returnType, QString name, QStringList parameters, QString comment)
{
   return CodeSource::functionDeclaration(returnType, name, parameters, comment);
}

QString CSourceCreator::RepresentFunctionDefinition( QString returnType, QString name, QStringList parameters, QString body, QString comment)
{
   return CodeSource::functionDefinition(returnType, name, parameters, comment, body);
}

QString CSourceCreator::RepresentFunctionCall( QString name, QStringList parameters)
{
   return CodeSource::functionCall(name, parameters);
}

QString CSourceCreator::RepresentGlobalVariableDefinition( QString type, QString name, QString defaultValue, QString description)
{
   return "static " + CodeSource::globalVariableDefinition(type, name, defaultValue, description);
}

QString CSourceCreator::RepresentGlobalVariableUse(QString name)
{
   return CodeSource::globalVariable(name);
}

//------------------------------------------------------------------------------
//  creates
//------------------------------------------------------------------------------

void CSourceCreator::CreateIncludeFiles(QStringList &FilesList)
{
   includes += FilesList.join("");
}

void CSourceCreator::CreateFunctions(QStringList &Declarations, QStringList &Definitions)
{
   functionsDecl += Declarations.join("\n");
   functionsDef += Definitions.join("\n");
}

void CSourceCreator::SetCodeName(QString name)
{
   this->name = name;
}

void CSourceCreator::CreateVariables(QStringList &vars)
{
   this->variables += vars.join("\n");
}

//------------------------------------------------------------------------------
//  result
//------------------------------------------------------------------------------

QString CSourceCreator::GetResult()
{
   sourceTemplate.setFileName(sourceTemplateFileName);
   sourceTemplate.setParameter("NAME", name);
   sourceTemplate.setParameter("INCLUDE", includes);
   sourceTemplate.setParameter( "GLOBALS", variables);
   sourceTemplate.setParameter( "LOCAL_DECLARATIONS", functionsDecl);
   sourceTemplate.setParameter( "LOCAL_DEFINITIONS", functionsDef);
   return sourceTemplate.toString();
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//  C header creator
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

CHeaderCreator::CHeaderCreator()
   : CSourceCreator()
{
   sourceTemplateFileName = QString(HEADER_FILE_NAME);
}

QString CHeaderCreator::GetResult()
{
   sourceTemplate.setFileName(sourceTemplateFileName);
   sourceTemplate.setParameter("NAME", name);
   sourceTemplate.setParameter("INCLUDE", includes);
   sourceTemplate.setParameter( "CONSTANTS", constants);
   sourceTemplate.setParameter( "ENUM", enums);
   sourceTemplate.setParameter( "DATA_TYPES", dataTypes);
   sourceTemplate.setParameter( "DATA", data);
   sourceTemplate.setParameter( "FUNCTIONS", functionsDecl);
   return sourceTemplate.toString();
}




//------------------------------------------------------------------------------
//  Include local file
//------------------------------------------------------------------------------

QString CodeSource::includeFileLocal( QString filePath, QString fileName)
// generate local include statement for <filePath> and <fileName>
{
QString text;

   if( filePath.isEmpty()){
      text +=  QString( "#include \"%1.h\"\n").arg( fileName);
   } else {
      text +=  QString( "#include \"%1/%2.h\"\n").arg( filePath).arg( fileName);
   }
   return( text);
}

//------------------------------------------------------------------------------
//  Include global file
//------------------------------------------------------------------------------

QString CodeSource::includeFileGlobal( QString filePath, QString fileName)
// generate global include statement for <filePath> and <fileName>
{
QString text;

   if( filePath.isEmpty()){
      text +=  QString( "#include <%1.h>\n").arg( fileName);
   } else {
      text +=  QString( "#include <%1/%2.h>\n").arg( filePath).arg( fileName);
   }
   return( text);
}

//------------------------------------------------------------------------------
//  Function definition
//------------------------------------------------------------------------------
QString CodeSource::functionDefinition( QString returnType, QString name, QString parameters, QString comment, QString body)
{
   return functionDefinition(returnType, name, QStringList(parameters), comment, body);
}

QString CodeSource::functionDefinition( QString returnType, QString name, const char * parameters, QString comment, QString body)
{
   return functionDefinition(returnType, name, QString(parameters), comment, body);
}

QString CodeSource::functionDefinition( QString returnType, QString name, QStringList parameters, QString comment, QString body)
// generate function with body
{
QString text;

   text = returnType + " " + name + "( ";
   if( parameters.count() == 0 || (parameters.count() == 1 && parameters.first() == "")){
      text += "void";
   } else {
      for( int i = 0; i < parameters.count() - 1; i++){
         text += parameters[ i] + ", ";
      }
      text += parameters.last();
   }
   text += ")\n// " + comment + "\n{\n";
   text += body;
   text += "}";
   return text;
}

//------------------------------------------------------------------------------
//  Function declaration
//------------------------------------------------------------------------------
QString CodeSource::functionDeclaration(QString returnType, QString name, QString parameters, QString comment)
{
   return functionDeclaration(returnType, name, QStringList(parameters), comment);
}

QString CodeSource::functionDeclaration(QString returnType, QString name, const char * parameters, QString comment)
{
   return functionDeclaration(returnType, name, QString(parameters), comment);
}

QString CodeSource::functionDeclaration(QString returnType, QString name, QStringList parameters, QString comment)
// generate function declaration
{
QString text;

   text = returnType + " " + name + "( ";
   if( parameters.count() == 0 || (parameters.count() == 1 && parameters.first() == "")){
      text += "void";
   } else {
      for( int i = 0; i < parameters.count() - 1; i++){
         text += parameters[ i] + ", ";
      }
      text += parameters.last();
   }
   text += ");\n// " + comment + "";
   return text;
}

//------------------------------------------------------------------------------
//  Function call
//------------------------------------------------------------------------------
QString CodeSource::functionCall( QString name, QString     parameters)
{
   return functionCall( name, QStringList(parameters));
}

QString CodeSource::functionCall( QString name, const char *parameters)
{
   return functionCall( name, QString(parameters));
}

QString CodeSource::functionCall( QString name, QStringList parameters)
// generate function call
{
QString text;

   text = name;
   text += "(";

   if( parameters.count() != 0 && parameters.first() != ""){
      text += " ";
      for( int i = 0; i < parameters.count() - 1; i++){
         text += parameters[ i] + ", ";
      }
      text += parameters.last();
   }
   text += ");";
   return text;
}

//------------------------------------------------------------------------------
//  Global variable
//------------------------------------------------------------------------------
QString CodeSource::globalVariableDefinition( QString type, QString name, QString defaultValue, QString description)
// generate global variable definition:  type _CamelCaseName; //description
{
QString text;

   if( type.isEmpty()){
      text += "int";
   } else {
      text += type;
   }
   text += "   _" + OName::toCamelCase(OName::fromUpperCase(name));
   text += " = "+defaultValue+";";
   if( description.isEmpty()){
      text += "\n";
   } else {
      text += " // "+ description + "\n";
   }
   return text;
}

QString CodeSource::globalVariable( QString name)
// generate access string to global variable
{
QString text;
   text = "_" + OName::toCamelCase(OName::fromUpperCase(name));
   return text;
}
