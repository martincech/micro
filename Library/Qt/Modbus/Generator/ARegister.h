#ifndef AREGISTER_H
#define AREGISTER_H

#include <QString>
#include <QStringList>
#include <QVector>
#include <QVectorIterator>

// declarations of classes below
class Iterator;
class NullIterator;
class GroupIterator;

//------------------------------------------------------------------------------
//  Abstract class representing register or register group
//------------------------------------------------------------------------------

class ARegister
{
public:
   ARegister(): registerType(REGISTER_REGISTER){}

   /* Bitfield masks */
   typedef enum {
      REGISTER_REGISTER = 1,
      REGISTER_COMMAND  = 2,
      REGISTER_STRING   = 4,
      REGISTER_STRING_BUFFER = 8
   } ERegisterType;

   virtual QString        Name() = 0;
   virtual int            Number() = 0;
   virtual bool           Readable(ERegisterType Type) = 0;
   virtual bool           Writable(ERegisterType Type) = 0;
   virtual QStringList    Header() = 0;

   virtual bool           Readable(){return Readable((ERegisterType)0xf);}
   virtual bool           Writable(){return Writable((ERegisterType)0xf);}

   virtual void           Add(ARegister *){}
   virtual void           Remove(ARegister *){}
   virtual int            ChildCount(){return 0;}
   virtual Iterator       *CreateIterator();

   int  Type(){ return registerType;}
   static ERegisterType decodeType( QString &typeString);
   static QString encodeType(int type);

protected:
   int  registerType;              // type of register for generation purposes
};

//------------------------------------------------------------------------------
//  Leaf - concrete one register without childs
//------------------------------------------------------------------------------

class Register : public ARegister
{
private:
   QString        name;                      // register name
   int            num;                       // register number
   bool           read;                      // is for read
   bool           write;                     // is for write
   QString        from;                      // range from as string
   QString        to;                        // range to as string
   QString        defaultValue;              // default value
   QString        dataName;                  // name of data which this register is for
   QStringList    headerFile;                // name of data file with declaration of this data
   QString        dataGetter;                // getter for data represented by this register
   QString        dataSetter;                // setter of data represented by this register
   QString        dataGSLocals;              // local function values needed for setter/getter
   QString        description;               // data description

public:
   Register( QString &Name, ERegisterType Type, int Number, const QStringList &Header, bool Readable = true, bool Writable = false);
   // ARegister methods
   QString        Name(){return name;}
   int            Number(){return num;}
   bool           Readable(){return read;}
   bool           Writable(){return write;}
   bool           Readable(ERegisterType Type){return registerType & Type? Readable() : false;}
   bool           Writable(ERegisterType Type){return registerType & Type? Writable() : false;}
   QStringList    Header(){return headerFile;}
   // Own methods
   void SetName( QString Name){name = Name;}
   void SetNumber( int Number){num = Number;}
   void SetReadable(){read = true;}
   void SetWritable(){write = true;}
   void SetRange(int From, int To);
   void SetRange(QString From, QString To){from = From; to = To;}
   void SetFrom(int From);
   void SetFrom(QString From){from = From;}
   void SetTo(int To);
   void SetTo(QString To){to = To;}
   void SetDefaultValue( QString Value){defaultValue = Value;}
   QString DefaultValue(){return defaultValue;}
   void SetDescription( QString Desc){description = Desc;}
   QString Description(){return description;}
   void SetGetter( QString getter){dataGetter = getter;}
   QString Getter(){return dataGetter;}
   void SetSetter( QString setter){dataSetter = setter;}
   QString Setter(){return dataSetter;}
   void SetLocals( QString locals){dataGSLocals = locals;}
   QString Locals(){return dataGSLocals;}
   void SetDataName( QString dname){dataName = dname;}
   QString DataName(){return dataName;}
};

//------------------------------------------------------------------------------
//  Composite - group of ARegister children
//------------------------------------------------------------------------------

class RegisterGroup : public ARegister
{
private:
   QVector<ARegister *> Registers;
   QString              name;                      // group name
   int                  base;                      // group base
   QStringList          variableTypes;             // list of variables types
   QStringList          variableNames;             // list of variables names

public:
   RegisterGroup( QString Name, QStringList &GroupVariables, int Base);
   ~RegisterGroup();

   // ARegister methods
   QString        Name(){return name;}
   int            Number(){return base;}
   bool           Readable(ERegisterType Type);
   bool           Writable(ERegisterType Type);
   QStringList    Header();

   void           Add(ARegister *Reg);
   void           Remove(ARegister *Reg);
   int            ChildCount(){return Registers.count();}
   Iterator       *CreateIterator();

   // Own methods
   bool Contains(ARegister *Reg);
   void SetName( QString Name);  // set group name
   void SetNumber( int Number);  // set group base

private:
   friend class GroupIterator;
};

//------------------------------------------------------------------------------
//  Iterator abstract class
//------------------------------------------------------------------------------

class Iterator
{
protected:
   Iterator(){}
   Iterator(const Iterator &){}

public:
   virtual bool hasNext() = 0;
   virtual ARegister *Next() = 0;
};

//------------------------------------------------------------------------------
//  Iterator Proxy pointer
//------------------------------------------------------------------------------

class IteratorPtr
{
public:
   IteratorPtr(Iterator *i):_i(i){}
   ~IteratorPtr(){
      delete _i;
   }

   Iterator *operator->(){return _i;}
   Iterator &operator*(){ return *_i;}
private:
   IteratorPtr(const IteratorPtr&);
   IteratorPtr& operator=(const IteratorPtr&);
private:
   Iterator *_i;
};

//------------------------------------------------------------------------------
//  Empty iterator
//------------------------------------------------------------------------------

class NullIterator : public Iterator
{
public:
   NullIterator():Iterator(){}
   NullIterator(const NullIterator &copy): Iterator(copy){}
   bool hasNext(){return false;}
   ARegister *Next(){return NULL;}
};

//------------------------------------------------------------------------------
//  Group iterator
//------------------------------------------------------------------------------

class GroupIterator: public Iterator
{
public:
   GroupIterator(RegisterGroup *reg);
   GroupIterator(const GroupIterator &copy);
   ~GroupIterator();
   bool hasNext();
   ARegister *Next();

protected:
   QVectorIterator<ARegister*> *_it;

   void SetItToNewLocation( RegisterGroup *reg);
};

//------------------------------------------------------------------------------
//  Read registers iterator
//------------------------------------------------------------------------------

class RGroupIterator: public GroupIterator
{
public:
   RGroupIterator(RegisterGroup *reg):GroupIterator(reg){}
   RGroupIterator(const RGroupIterator &copy):GroupIterator(copy){}
   bool hasNext();
   ARegister *Next();
};

//------------------------------------------------------------------------------
//  Write registers iterator
//------------------------------------------------------------------------------

class WGroupIterator: public RGroupIterator
{
public:
   WGroupIterator(RegisterGroup *reg):RGroupIterator(reg){}
   WGroupIterator(const WGroupIterator &copy):RGroupIterator(copy){}
   ARegister *Next();
};

//------------------------------------------------------------------------------
//  R/W registers iterator
//------------------------------------------------------------------------------

class RWGroupIterator: public RGroupIterator
{
public:
   RWGroupIterator(RegisterGroup *reg):RGroupIterator(reg){}
   RWGroupIterator(const RWGroupIterator &copy):RGroupIterator(copy){}
   ARegister *Next();
};

//------------------------------------------------------------------------------
//  Type registers iterator
//------------------------------------------------------------------------------

class TypeGroupIterator: public RGroupIterator
{
public:
   TypeGroupIterator(RegisterGroup *reg, ARegister::ERegisterType type):RGroupIterator(reg), itType(type){}
   TypeGroupIterator(const TypeGroupIterator &copy):RGroupIterator(copy){}
   ARegister *Next();

protected:
   ARegister::ERegisterType itType;
};

//------------------------------------------------------------------------------
//  Type registers iterator
//------------------------------------------------------------------------------

class GlobalRegsGroupIterator: public GroupIterator
{
public:
   GlobalRegsGroupIterator(RegisterGroup *reg);
   GlobalRegsGroupIterator(const GlobalRegsGroupIterator &copy);
   ~GlobalRegsGroupIterator();

   bool hasNext();
   ARegister *Next();

protected:
   QVector<QVectorIterator<ARegister*> *> _itStack;
};

//------------------------------------------------------------------------------
//  R/W type iterator
//------------------------------------------------------------------------------

class ConditionalIterator: public RGroupIterator
{
public:
   ConditionalIterator(RegisterGroup *reg);
   ConditionalIterator(RegisterGroup *reg, bool r, bool w, int type = 0xFF);
   ConditionalIterator(const ConditionalIterator &copy);

   void setCondition(bool r, bool w, int type = 0xFF);
   ARegister *Next();

protected:
   bool R;
   bool W;
   int type;
};

#endif // AREGISTER_H
