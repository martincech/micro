#ifndef CODEGENERATOR_H
#define CODEGENERATOR_H

#include "CodeCreator.h"
#include "Modbus/Generator/ARegister.h"
#include <QFile>

//------------------------------------------------------------------------------
//  Abstract generator
//------------------------------------------------------------------------------

class CodeGenerator
{
protected:
   CodeGenerator(CodeCreator *builder);

public:
   void SetBuilder(CodeCreator *builder);
   virtual QString Generate(ARegister *forGroup) = 0;

protected:
   CodeCreator *Builder;
};

//------------------------------------------------------------------------------
//  Generator for register groups source files
//------------------------------------------------------------------------------

class GroupSourceGenerator : public CodeGenerator
{
protected:
   QString groupMapping( const RegisterGroup *group);
public:
   GroupSourceGenerator(CodeCreator *builder):CodeGenerator(builder){}
   QString Generate(ARegister *forGroup);
};

//------------------------------------------------------------------------------
//  Generator for register groups header files
//------------------------------------------------------------------------------

class GroupHeaderGenerator : public CodeGenerator
{
public:
   GroupHeaderGenerator(CodeCreator *builder):CodeGenerator(builder){}
   QString Generate(ARegister *forGroup);
};


#endif // CODEGENERATOR_H
