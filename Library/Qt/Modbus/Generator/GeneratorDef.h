//******************************************************************************
//
//   GeneratorDef.h     Modbus code generator definitions
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __GeneratorDef_H__
   #define __GeneratorDef_H__

#ifndef UGENERATORDEF_H
   #include "uGenerator/GeneratorDef.h"
#endif
//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------
#define MODBUS_GENERATOR_TEMPLATE_PATH    GENERATOR_TEMPLATE_PATH"Modbus/"

#define HEADER_FILE_NAME          MODBUS_GENERATOR_TEMPLATE_PATH "HeaderWithFunc.h"
#define SOURCE_FILE_NAME          MODBUS_GENERATOR_TEMPLATE_PATH "Source.c"

#define MODBUS_RANGE_CHECK_FILENAME "ModbusRegRangeCheck"
#define MODBUS_REG_FILENAME "ModbusReg"
//------------------------------------------------------------------------------
//  Keywords
//------------------------------------------------------------------------------

#define KEYWORD_VERSION      "version"
#define KEYWORD_GROUP        "registergroup"
#define KEYWORD_GLOBAL       "global"

#endif // __GeneratorDef_H__


