//******************************************************************************
//
//    Graph.h      Basic graph
//    Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef GRAPH_H
#define GRAPH_H

#include <qwt_plot.h>
#include <qwt_plot_curve.h>

class QwtPlotCurve;

class Graph: public QwtPlot
{
    Q_OBJECT

public:
    Graph(QWidget *parent);
    // constructor

    QwtPlotCurve *newCurve( const char *title, const QColor color);
    // create new curve

    void moveX( double offset, double width);
    // move graph horizontally, start at <offset> with window <width>

    void moveY( double offset, double height);
    // move graph vertically, start at <offset> with window <height>

    void fit();
    // autoscale graph to fit all curves

}; // Graph

#endif
