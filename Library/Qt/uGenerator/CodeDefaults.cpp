//******************************************************************************
//
//   CodeDefaults.cpp  Source code default values
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#include "CodeDefaults.h"
#include "uGenerator/SourceTemplate.h"
#include "uGenerator/CodeHeader.h"
#include "uGenerator/CodeSeparator.h"
#include "uGenerator/CodeNumber.h"
#include "Parse/nameTransformation.h"

#define DEFAULT_SUFFIX  "Default"

//------------------------------------------------------------------------------
//  Source code
//------------------------------------------------------------------------------

QString CodeDefaults::sourceCode( DataParser *dataParser, DictionaryParser *dictionaryParser)
// generate full source code by <dataParser> and <dictionaryParser>
{
   QString text;
   // generate global variables list :
   text += globalDeclaration( dataParser);
   // generate by data definitions :
   for( int i = 0; i < dataParser->itemsCount(); i++){
      text += CodeDefaults::structDeclaration( dataParser->definitionAt( i));
   }
   return( text);
} // sourceCode

//------------------------------------------------------------------------------
//  Global declaration
//------------------------------------------------------------------------------

QString CodeDefaults::globalDeclaration( DataParser *dataParser)
// generate global variables list
{
   QString text;
   const DataDefinition *definition;
   for( int i = 0; i < dataParser->itemsCount(); i++){
      definition = dataParser->definitionAt( i);
      if( definition->menu){
         continue;                     // skip menu definition
      }
      QString name = OName::toCamelCase( definition->prefix + QChar( ' ') + definition->name);
      text += CodeDefaults::globalItemDeclaration( name);
   }
   text += QChar( '\n');
   return( text);
} // globalDeclaration

//------------------------------------------------------------------------------
//  Global item declaration
//------------------------------------------------------------------------------

QString CodeDefaults::globalItemDeclaration( QString name)
// generate global variable with <name>
{
   return( QString( "T%1 %2;\n").arg(name).arg(name));
} // globalItemDeclaration

//------------------------------------------------------------------------------
//  Struct declaration
//------------------------------------------------------------------------------

QString CodeDefaults::structDeclaration( const DataDefinition *definition)
// generate struct list by <definition> items
{
   if( definition->menu){
      return( QString());             // skip menu definition
   }
   QString text;
   QString name = OName::toCamelCase( definition->prefix + QChar( ' ') + definition->name); // short struct name
   // title :
   text  = CodeSeparator::section( OName::toText( definition->prefix + QChar( ' ') + definition->name)) + QChar( '\n');
   text += QString( "const T%1 %2 = {\n").arg( name).arg( name + DEFAULT_SUFFIX);
   // data items :
   const DataItem *item;
   for( int i = 0; i < definition->item.count(); i++){
      item = &definition->item[ i];
      // check for data type :
      switch( item->dataType){
         case DATA_TYPE_CUSTOM :
         case DATA_TYPE_STRUCT :
            break;

         default :
            continue;                  // don't generate item
      }
      // check for class type :
      switch( item->classType){
         case CLASS_TYPE_YESNO :
         case CLASS_TYPE_SPARE :
         case CLASS_TYPE_IP :
            // direct constant
            text += CodeDefaults::structItemDeclaration( item->name, item->defaultValue);
            break;

         case CLASS_TYPE_UINT :
         case CLASS_TYPE_INT :
         case CLASS_TYPE_FLOAT :
         case CLASS_TYPE_EMBEDDED :
         case CLASS_TYPE_FIXED :
         case CLASS_TYPE_CHAR :
         case CLASS_TYPE_STRING :
         case CLASS_TYPE_PASSWORD :
            // symbolic constant
            text += CodeDefaults::structItemDeclaration( item->name,
                        CodeHeader::constantDefaultValue( definition->prefix, definition->name, item->name));
            break;

         case CLASS_TYPE_ARRAY :
            // fake definition for array
            text  += CodeDefaults::structItemDeclaration( item->name, QString( "Array?"));
            break;

         case CLASS_TYPE_ENUM :
            // enum constant
            text  += CodeDefaults::structEnumItemDeclaration( item->name, item->className,
                                                              item->defaultValue);
            break;

         case CLASS_TYPE_CUSTOM :
            // fake struct definition :
            text += CodeDefaults::structItemDeclaration( OName::toCamelCase( item->className),
                                                         OName::toCamelCase( item->className) + QChar( '?'));
            break;

         default :
            // fake definition for undefined type
            text  += CodeDefaults::structItemDeclaration( item->name, QString( "?"));
            break;
      }
   }
   text.remove( text.size() - 2, 1);   // remove last comma (let new line only)
   text += QString( "};\n");
   text += QChar( '\n');               // space after definition
   return( text);
} // structDeclaration

//------------------------------------------------------------------------------
//  Struct Item
//------------------------------------------------------------------------------

QString CodeDefaults::structItemDeclaration( QString item, QString value)
// generate struct <item> with <value>
{
   QString text;
   text = QString( "   /* %1 */ %2,\n").arg( OName::toCamelCase( item)).arg( value);
   return( text);
} // structItemDeclaration

//------------------------------------------------------------------------------
//  Struct Item by enum
//------------------------------------------------------------------------------

QString CodeDefaults::structEnumItemDeclaration( QString item, QString enumName, QString value)
// generate struct <item> with <value> for <enumName>
{
   QString text;
   QString enumConstant = OName::toUpperCase( enumName + QChar( ' ') + value);
   text = QString( "   /* %1 */ %2,\n").arg( OName::toCamelCase( item)).arg( enumConstant);
   return( text);
} // structEnumItemDeclaration
