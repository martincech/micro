//******************************************************************************
//
//   uGenerator.cpp  Micro code generator
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "uGenerator.h"
#include "Parse/nameTransformation.h"
#include "uGenerator/SourceTemplate.h"
#include "Parse/Enum.h"
#include "uGenerator/GeneratorDef.h"
#include "uGenerator/CodeSeparator.h"
#include "uGenerator/CodeHeader.h"
#include "uGenerator/CodeStrings.h"
#include "uGenerator/CodeDefaults.h"
#include "uGenerator/CodeMenu.h"

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------

uGenerator::uGenerator()
{
   _dictionary      = 0;
   _embeddedTypes   = 0;
   _dataDefinitions = 0;
} // uGenerator

//------------------------------------------------------------------------------
//  Destructor
//------------------------------------------------------------------------------

uGenerator::~uGenerator()
{
   if( _dictionary){
      delete _dictionary;
   }
   if( _embeddedTypes){
      delete _embeddedTypes;
   }
   if( _dataDefinitions){
      delete _dataDefinitions;
   }
} // ~uGenerator

//------------------------------------------------------------------------------
//  Parse
//------------------------------------------------------------------------------

bool uGenerator::parse( ArrayModel *objects, ArrayModel *dictionary)
// Parse <objects> and <dictionary>
{
   // delete old data :
   if( _dictionary){
      delete _dictionary;
   }
   if( _embeddedTypes){
      delete _embeddedTypes;
   }
   if( _dataDefinitions){
      delete _dataDefinitions;
   }
   // parser source arrays :
   _dictionary      = new DictionaryParser( dictionary);
   _embeddedTypes   = new EmbeddedParser( GENERATOR_TEMPLATE_PATH "EmbeddedTypes.csv");
   _dataDefinitions = new DataParser( objects, _dictionary, _embeddedTypes);
   return( true);
} // parse

//------------------------------------------------------------------------------
//  Header
//------------------------------------------------------------------------------

QString uGenerator::headerSource()
// returns global header file
{
   return( CodeHeader::sourceCode( _dataDefinitions, _dictionary));
} // headerSource

//------------------------------------------------------------------------------
//  Defaults
//------------------------------------------------------------------------------

QString uGenerator::defaultsSource()
// returns global data default values
{
   return( CodeDefaults::sourceCode( _dataDefinitions, _dictionary));
} // defaultsSource

//------------------------------------------------------------------------------
//  Menu
//------------------------------------------------------------------------------

QStringList uGenerator::menuSource()
// returns menu source files
{
   return( CodeMenu::sourceCode( _dataDefinitions, _dictionary));
} // menuSource

//------------------------------------------------------------------------------
//  Menu header
//------------------------------------------------------------------------------

QStringList uGenerator::menuHeader()
// returns menu header files
{
   return( CodeMenu::headerCode( _dataDefinitions, _dictionary));
} // menuHeader

//------------------------------------------------------------------------------
//  Menu file names
//------------------------------------------------------------------------------

QStringList uGenerator::menuFileNames()
// returns menu file names list (without extension and path)
{
   return( CodeMenu::sourceFiles( _dataDefinitions));
} // menuFileNames

//------------------------------------------------------------------------------
//  Strings
//------------------------------------------------------------------------------

QString uGenerator::stringsSource()
// returns strings definition file
{
   return( Dacs5CodeString::sourceCode( _dataDefinitions, _dictionary));
} // stringsSource
