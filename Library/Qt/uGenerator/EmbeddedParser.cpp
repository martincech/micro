//******************************************************************************
//
//   EmbeddedParser.cpp   Embeded data definitions parser
//   Version 1.0   (c)    VEIT Electronics
//
//******************************************************************************

#include "EmbeddedParser.h"
#include "uGenerator/GeneratorDef.h"
#include "Parse/Csv.h"

// fixed column numbers :
#define NAME_COLUMN     0
#define TYPE_COLUMN     1
#define CLASS_COLUMN    2
#define UNITS_COLUMN    3
#define DECIMALS_COLUMN 4
#define LOW_COLUMN      5
#define HIGH_COLUMN     6
#define DEFAULT_COLUMN  7
#define EDITOR_COLUMN   8
#define COMMENT_COLUMN  9

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------

EmbeddedParser::EmbeddedParser( QString fileName) :
                                _dataTypeEnum( ENUM_DATA_TYPE), _classTypeEnum( ENUM_CLASS_TYPE)
// create data definitions from <fileName>
{
   Csv *csvEmbedded = new Csv( EMBEDDED_COLUMNS_MAX);  // create embedded table
   if( !csvEmbedded->load( fileName)){
      return;
   }
   ArrayModel *dataArray = csvEmbedded->array();
   int     rows;
   QString name;
   EmbeddedDefinition definition;
   rows = dataArray->getRowsCount();
   for( int row = 0; row < rows; row++){
      name = dataArray->getItem( row, NAME_COLUMN);
      if( name.isEmpty()){
         continue;
      }
      if( name == QString( KEYWORD_NAME)){
         // copy title row to column header :
         for( int col = 0; col < OBJECT_COLUMNS_MAX; col++){
            dataArray->setTitle( col, dataArray->getItem( row, col));
         }
         // skip column header
         continue;
      }
      // embedded definition :
      definition.name         = name;
      definition.dataName     = dataArray->getItem( row, TYPE_COLUMN);
      definition.dataType     = dataType( definition.dataName);
      definition.className    = dataArray->getItem( row, CLASS_COLUMN);
      definition.classType    = classType( definition.className);
      definition.units        = dataArray->getItem( row, UNITS_COLUMN);
      definition.decimals     = dataArray->getItem( row, DECIMALS_COLUMN).toInt();
      definition.lowLimit     = dataArray->getItem( row, LOW_COLUMN).toDouble();
      definition.highLimit    = dataArray->getItem( row, HIGH_COLUMN).toDouble();
      definition.defaultValue = dataArray->getItem( row, DEFAULT_COLUMN);
      definition.editor       = dataArray->getItem( row, EDITOR_COLUMN);
      definition.comment      = dataArray->getItem( row, COMMENT_COLUMN);
      _embeddedList.append( definition);
   }
} // EmbeddedParser

//------------------------------------------------------------------------------
//  Destructor
//------------------------------------------------------------------------------

EmbeddedParser::~EmbeddedParser()
// destructor
{
} // EmbeddedParser

//------------------------------------------------------------------------------
//  Count
//------------------------------------------------------------------------------

int EmbeddedParser::itemsCount()
// returns number of items
{
   return( _embeddedList.count());
} // itemsCount

//------------------------------------------------------------------------------
//  Item by name
//------------------------------------------------------------------------------

const EmbeddedDefinition *EmbeddedParser::getDefinition( QString name)
// returns definition by <name>
{
   for( int i = 0; i < _embeddedList.count(); i++){
      if( _embeddedList.at( i).name == name){
         return( &_embeddedList.at( i));
      }
   }
   return( 0);
} // getDefinition

//------------------------------------------------------------------------------
//  Name by index
//------------------------------------------------------------------------------

const EmbeddedDefinition *EmbeddedParser::definitionAt( int index)
// returns definition at <index> position
{
   if( index >= _embeddedList.count()){
      return( 0);
   }
   return( &_embeddedList.at( index));
} // definitionAt

//------------------------------------------------------------------------------
//  Has name
//------------------------------------------------------------------------------

bool EmbeddedParser::hasName( QString name)
// check for <name>
{
   for( int i = 0; i < _embeddedList.count(); i++){
      if( _embeddedList.at( i).name == name){
         return( true);
      }
   }
   return( false);
} // hasName

//------------------------------------------------------------------------------
//  Data type
//------------------------------------------------------------------------------

int EmbeddedParser::dataType( QString dataTypeName)
// returns data type by <dataTypeName>
{
   if( dataTypeName.isEmpty()){
      return( DATA_TYPE_UNDEFINED);
   }
   if( !_dataTypeEnum.hasString( dataTypeName.toAscii())){
      return( DATA_TYPE_CUSTOM);
   }
   return( _dataTypeEnum.toCode( dataTypeName.toAscii()));
} // dataType

//------------------------------------------------------------------------------
//  Class type
//------------------------------------------------------------------------------

int EmbeddedParser::classType( QString className)
// returns class type by <className>
{
   if( className.isEmpty()){
      return( CLASS_TYPE_UNDEFINED);
   }
   if( !_classTypeEnum.hasString( className.toAscii())){
      return( CLASS_TYPE_UNDEFINED);
   }
   return( _classTypeEnum.toCode( className.toAscii()));
} // classType
