//******************************************************************************
//
//   CodeStrings.cpp Source code Stringss
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "CodeStrings.h"
#include "uGenerator/SourceTemplate.h"
#include "uGenerator/CodeSeparator.h"
#include "Parse/nameTransformation.h"

//------------------------------------------------------------------------------
//  Source code
//------------------------------------------------------------------------------

QString Dacs5CodeString::sourceCode( DataParser *dataParser, DictionaryParser *dictionaryParser)
// generate full source code by <dataParser> and <dictionaryParser>
{
   QString strings;
   // file title :
   strings  = CodeSeparator::title( dataParser->fileName() + QString(".str"), dataParser->title());
   // generate enum strings :
   const DictionaryDefinition *definition;
   for( int i = 0; i < dictionaryParser->itemsCount(); i++){
      definition = dictionaryParser->definitionAt( i);
      if( definition->type != DICTIONARY_TYPE_ENUM){
         continue;
      }
      strings += Dacs5CodeString::enumList( definition->name, definition->itemsList);
   }
   // generate data definitions strings :
   for( int i = 0; i < dataParser->itemsCount(); i++){
      strings += Dacs5CodeString::dataList( dataParser->definitionAt( i));
   }
   return( strings);
} // sourceCode

//------------------------------------------------------------------------------
//  Enum list
//------------------------------------------------------------------------------

QString Dacs5CodeString::enumList( QString enumName, Enum *enumDefinition)
// generate strings list for <enumName> by <enumDefinition>
{
   QString text;
   // title :
   text  = CodeSeparator::section( OName::toText( enumName) + QString( " enum")) + QChar( '\n');
   // enum list constant :
   text += enumConstant( enumName) + QChar( '\n');
   // enum items :
   for( int i = 0; i < enumDefinition->itemsCount(); i++){
      text += itemDeclaration( enumDefinition->toString( i));
   }
   text += QChar( '\n');               // space after definition
   return( text);
} // enumList

//------------------------------------------------------------------------------
//  Data items list
//------------------------------------------------------------------------------

QString Dacs5CodeString::dataList( const DataDefinition *definition)
// generate strings list by <definition> items
{
   QString text;
   // title :
   text  = CodeSeparator::section( OName::toText( definition->name)) + QChar( '\n');
   text += itemDeclaration( definition->name);             // menu title
   // data items :
   const DataItem *item;
   for( int i = 0; i < definition->item.count(); i++){
      item = &definition->item[ i];
      if( item->classType == CLASS_TYPE_SPARE){
         continue;                                         // skip spare item
      }
      text += itemDeclaration( item->name);                // menu item
   }
   text += QChar( '\n');         // space after definition
   return( text);
} // dataList

//------------------------------------------------------------------------------
//  Item declaration
//------------------------------------------------------------------------------

QString Dacs5CodeString::itemDeclaration( QString name)
// generate <name> declaration row
{
   QString text;
   text  = itemConstant( name);        // constant identifier
   text += QString( " \"")  + OName::toText( name) + QString( "\"\n"); // string
   return( text);
} // itemDeclaration

//------------------------------------------------------------------------------
//  Constant
//------------------------------------------------------------------------------

QString Dacs5CodeString::itemConstant( QString name)
// generate string constant by <name>
{
   return( QString( "STR_") + OName::toUpperCase( name.replace(QString("'"), QString(""))));
} // itemConstant

//------------------------------------------------------------------------------
//  Enum constant
//------------------------------------------------------------------------------

QString Dacs5CodeString::enumConstant( QString name)
// generate enum constant by <name>
{
   return( QString( "ENUM_") + OName::toUpperCase( name));
} // enumConstant
