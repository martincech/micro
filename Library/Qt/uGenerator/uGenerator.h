//******************************************************************************
//
//   uGenerator.h  Micro code generator
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef UGENERATOR_H
   #define UGENERATOR_H

#include <QString>
#include <QStringList>
#include "Model/ArrayModel.h"
#include "uGenerator/DataParser.h"

class uGenerator
{
public:
   uGenerator();
   ~uGenerator();

   bool parse( ArrayModel *objects, ArrayModel *dictionary);
   // Parse <objects> and <dictionary>

   //---------------------------------------------------------------------------
   QString headerSource();
   // returns global header file

   QString defaultsSource();
   // returns global data default values

   QStringList menuSource();
   // returns menu source files

   QStringList menuHeader();
   // returns menu header files

   QStringList menuFileNames();
   // returns menu file names list (without extension and path)

   QString stringsSource();
   // returns strings definition file

private :
   DictionaryParser *_dictionary;
   EmbeddedParser   *_embeddedTypes;
   DataParser       *_dataDefinitions;
}; // uGenerator

//------------------------------------------------------------------------------

#endif // UGENERATOR_H
