//******************************************************************************
//
//   CodeStrings.h  Source code Stringss
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef CODESTRINGS_H
   #define CODESTRINGS_H

#include <QString>
#include "uGenerator/DataParser.h"
#include "uGenerator/DictionaryParser.h"

namespace Dacs5CodeString
{

   QString sourceCode( DataParser *dataParser, DictionaryParser *dictionaryParser);
   // generate full source code by <dataParser> and <dictionaryParser>

   QString enumList( QString enumName, Enum *enumDefinition);
   // generate strings list for <enumName> by <enumDefinition>

   QString dataList( const DataDefinition *definition);
   // generate strings list by <definition> items

   QString itemDeclaration( QString name);
   // generate <name> declaration row

   QString itemConstant( QString name);
   // generate string constant by <name>

   QString enumConstant( QString name);
   // generate enum constant by <name>

} // CodeStrings

#endif // CODESTRINGS_H
