//******************************************************************************
//
//   CodeMenu.cpp    Source code menu
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "CodeMenu.h"
#include "uGenerator/SourceTemplate.h"
#include "uGenerator/CodeSeparator.h"
#include "uGenerator/CodeStrings.h"
#include "uGenerator/CodeHeader.h"
#include "uGenerator/CodeDefaults.h"
#include "uGenerator/CodeNumber.h"
#include "Parse/nameTransformation.h"

#define MENU_BASIC_FILE_NAME        GENERATOR_TEMPLATE_PATH "MenuBasic.c"
#define MENU_DIALOG_FILE_NAME       GENERATOR_TEMPLATE_PATH "MenuDialog.c"
#define MENU_HEADER_FILE_NAME       GENERATOR_TEMPLATE_PATH "Menu.h"

//------------------------------------------------------------------------------
//  Source code
//------------------------------------------------------------------------------

QStringList CodeMenu::sourceCode( DataParser *dataParser, DictionaryParser *dictionaryParser)
// generate full source code by <dataParser> and <dictionaryParser>
{
   // generate by data definitions :
   QStringList list;
   for( int i = 0; i < dataParser->itemsCount(); i++){
      if( dataParser->definitionAt( i)->data){
         continue;                     // don't generate menu
      }
      list.append( CodeMenu::menuSource( dataParser->fileName(), dataParser->definitionAt( i)));
   }
   return( list);
} // sourceCode

//------------------------------------------------------------------------------
//  Header code
//------------------------------------------------------------------------------

QStringList CodeMenu::headerCode(  DataParser *dataParser, DictionaryParser *dictionaryParser)
// generate headers source code
{
   // generate by data definitions :
   QStringList list;
   for( int i = 0; i < dataParser->itemsCount(); i++){
      if( dataParser->definitionAt( i)->data){
         continue;                     // don't generate menu
      }
      list.append( CodeMenu::menuHeader( dataParser->fileName(), dataParser->definitionAt( i)));
   }
   return( list);
} // headerCode

//------------------------------------------------------------------------------
//  Header code
//------------------------------------------------------------------------------

QStringList CodeMenu::sourceFiles( DataParser *dataParser)
// returns source file names
{
   // generate by data definitions :
   QStringList list;
   QString     fileName;
   const DataDefinition *definition;
   for( int i = 0; i < dataParser->itemsCount(); i++){
      definition = dataParser->definitionAt( i);
      if( definition->data){
         continue;                     // don't generate menu
      }
      fileName  = QString( "Menu ");
      fileName += definition->menuPrefix;
      fileName += QChar( ' ');
      fileName += definition->name;
      fileName = OName::toCamelCase( fileName);
      list.append( fileName);
   }
   return( list);
} // sourceFiles

//------------------------------------------------------------------------------
//  Menu source
//------------------------------------------------------------------------------

QString CodeMenu::menuSource( QString fileName, const DataDefinition *definition)
// generate menu source by <definition> with main <fileName> header
{
   QString        description = definition->prefix + QChar( ' ') + definition->name + QString( " menu");
   SourceTemplate menuTemplate;
   if( definition->menu){
      // menu without parameters
      menuTemplate.setFileName( MENU_BASIC_FILE_NAME);
   } else {
      // dialog menu
      menuTemplate.setFileName( MENU_DIALOG_FILE_NAME);
      menuTemplate.setParameter( "PARAMETERS",       CodeMenu::parametersList( definition));
      menuTemplate.setParameter( "DATA_DECLARATION", CodeMenu::dataList( definition));
   }
   // file title :
   menuTemplate.setParameter( "NAME",        OName::toCamelCase( definition->menuPrefix + QChar( ' ') + definition->name));  // menu name
   menuTemplate.setParameter( "UNAME",       OName::toUpperCase( definition->name));                                         // menu title
   menuTemplate.setParameter( "LNAME",       OName::toLowerCase( definition->menuPrefix + QChar( ' ') + definition->name));  // menu comment
   menuTemplate.setParameter( "DNAME",       OName::toCamelCase( definition->prefix     + QChar( ' ') + definition->name));  // menu data name
   menuTemplate.setParameter( "DESCRIPTION", OName::toText(      definition->menuPrefix + QChar( ' ') + definition->name + QString( " menu")));
   menuTemplate.setParameter( "INCLUDE",     CodeMenu::includeList( fileName, definition));
   // update lists
   menuTemplate.setParameter( "STRINGS",     CodeMenu::stringsList( definition));
   menuTemplate.setParameter( "ENUM",        CodeMenu::enumList( definition));
   menuTemplate.setParameter( "CASE",        CodeMenu::caseList( definition));
   return( menuTemplate.toString());
} // menuSource

//------------------------------------------------------------------------------
//  Include files
//------------------------------------------------------------------------------

QString CodeMenu::includeList( QString fileName, const DataDefinition *definition)
// generate menu include list with main <fileName> by <definition>
{
   QString text;
   text  = QString( "#include \"") + fileName + QString( ".h\"\n");  // main header
   // add menu / method files :
   const DataItem *item;
   for( int i = 0; i < definition->item.count(); i++){
      item =  &definition->item[ i];
      // check for data type :
      switch( item->dataType){
         case DATA_TYPE_MENU :
         case DATA_TYPE_STRUCT :
            if( item->className.isEmpty()){
               text += QString( "#include \"Menu") + OName::toCamelCase( definition->name + QChar( ' ') + item->name);
            } else {
               text += QString( "#include \"Menu") + OName::toCamelCase( item->className);
            }
            break;

         case DATA_TYPE_METHOD :
            text += QString( "#include \"") + OName::toCamelCase( definition->name);         // data structure name
            break;

         default :
            continue;                  // don't generate item
      }
      text += QString( ".h\"\n");
   }
   return( text);
} // includeList

//------------------------------------------------------------------------------
//  Strings
//------------------------------------------------------------------------------

QString CodeMenu::stringsList( const DataDefinition *definition)
// generate menu strings list
{
   QString text;
   // data items :
   const DataItem *item;
   for( int i = 0; i < definition->item.count(); i++){
      item = &definition->item[ i];
      if( item->classType == CLASS_TYPE_SPARE){
         continue;                     // skip spare item
      }
      text += QString( "   ") + Dacs5CodeString::itemConstant( item->name);
      if( i < (definition->item.count() - 1)){
         text +=  QString( ",\n");     // not last item - add comma and newline
      } else {
         text +=  QChar( ',');         // add comma after last item also
      }
   }
   return( text);
} // stringsList

//------------------------------------------------------------------------------
//  Enum
//------------------------------------------------------------------------------

QString CodeMenu::enumList( const DataDefinition *definition)
// generate menu enum list
{
   QString text;
   // data items :
   const DataItem *item;
   for( int i = 0; i < definition->item.count(); i++){
      item = &definition->item[ i];
      if( item->classType == CLASS_TYPE_SPARE){
         continue;                     // skip spare item
      }
      text += QString( "   ") + CodeMenu::enumConstant( item->name);
      if( i < (definition->item.count() - 1)){
         text +=  QString( ",\n");     // not last item - add comma
      }
   }
   return( text);
} // enumList

//------------------------------------------------------------------------------
//  Data declaration
//------------------------------------------------------------------------------

QString CodeMenu::dataList( const DataDefinition *definition)
// generate menu data definition list
{
   // buffer declaration for strings data
   QString text;
   // data items :
   const DataItem *item;
   for( int i = 0; i < definition->item.count(); i++){
      item =  &definition->item[ i];
      // check for data type :
      if( item->dataType != DATA_TYPE_CUSTOM){
         continue;
      }
      if( item->classType != CLASS_TYPE_STRING && item->classType != CLASS_TYPE_PASSWORD){
         continue;
      }
      text += QString( "char %1[ %2 + 1];\n")
              .arg( OName::toCamelCase( item->name))
              .arg( CodeHeader::constantIdentifier( definition->prefix, definition->name, item->name, QString( "Size")));
   }
   return( text);
} // dataList

//------------------------------------------------------------------------------
//  Case
//------------------------------------------------------------------------------

QString CodeMenu::caseList( const DataDefinition *definition)
// generate menu case list
{
   QString        text;
   QString        dataName = OName::toCamelCase( definition->prefix + QChar( ' ') + definition->name); // short struct name
   SourceTemplate itemTemplate;
   // data items :
   const DataItem *item;
   for( int i = 0; i < definition->item.count(); i++){
      item =  &definition->item[ i];
      QString itemConstant = CodeMenu::enumConstant( item->name);
      QString itemName     = OName::toCamelCase( item->name);
      // check for data type :
      switch( item->dataType){
         case DATA_TYPE_CUSTOM :
         case DATA_TYPE_STRUCT :
            break;

         case DATA_TYPE_ALIAS :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "EditAlias.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            text += itemTemplate.toString();
            continue;

         case DATA_TYPE_MENU :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "EditMethod.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            if( item->className.isEmpty()){
               itemTemplate.setParameter( "METHOD",    QString( "Menu") + OName::toCamelCase( definition->name + QChar( ' ') + item->name));
            } else {
               itemTemplate.setParameter( "METHOD",    QString( "Menu") + OName::toCamelCase( item->className));
            }
            text += itemTemplate.toString();
            continue;

         case DATA_TYPE_METHOD :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "EditMethod.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            if( item->className.isEmpty()){
               itemTemplate.setParameter( "METHOD",    OName::toCamelCase( definition->name + QChar( ' ') + item->name));
            } else {
               itemTemplate.setParameter( "METHOD",    OName::toCamelCase( item->className));
            }
            text += itemTemplate.toString();
            continue;

         default :
            continue;                  // don't generate item
      }
      // check for class type :
      switch( item->classType){
         case CLASS_TYPE_YESNO :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "EditYesNo.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            itemTemplate.setParameter( "DATA", dataName);
            itemTemplate.setParameter( "ITEM", itemName);
            text += itemTemplate.toString();
            break;

         case CLASS_TYPE_UINT :
         case CLASS_TYPE_INT :
         case CLASS_TYPE_FIXED :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "EditNumber.c");
            itemTemplate.setParameter( "MENU_ITEM",  itemConstant);
            itemTemplate.setParameter( "DATA",       dataName);
            itemTemplate.setParameter( "ITEM",       itemName);
            itemTemplate.setParameter( "TYPE",       item->dataName);
            itemTemplate.setParameter( "WIDTH",      QString::number( item->width));
            itemTemplate.setParameter( "DECIMALS",   QString::number( item->decimals));
            itemTemplate.setParameter( "LOW_LIMIT",  CodeHeader::constantLowLimit( definition->prefix, definition->name, item->name));
            itemTemplate.setParameter( "HIGH_LIMIT", CodeHeader::constantHighLimit( definition->prefix, definition->name, item->name));
            itemTemplate.setParameter( "UNITS",      CodeMenu::itemUnits( item));
            text += itemTemplate.toString();
            break;

          case CLASS_TYPE_IP :
             itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "EditIp.c");
             itemTemplate.setParameter( "MENU_ITEM",  itemConstant);
             itemTemplate.setParameter( "DATA",       dataName);
             itemTemplate.setParameter( "ITEM",       itemName);
             itemTemplate.setParameter( "UITEM", OName::toUpperCase( item->name));
             text += itemTemplate.toString();
             break;

         case CLASS_TYPE_FLOAT :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "EditFloat.c");
            itemTemplate.setParameter( "MENU_ITEM",  itemConstant);
            itemTemplate.setParameter( "DATA",       dataName);
            itemTemplate.setParameter( "ITEM",       itemName);
            itemTemplate.setParameter( "TYPE",       item->dataName);
            itemTemplate.setParameter( "WIDTH",      QString::number( item->width));
            itemTemplate.setParameter( "DECIMALS",   QString::number( item->decimals));
            itemTemplate.setParameter( "LOW_LIMIT",  CodeHeader::constantLowLimit( definition->prefix, definition->name, item->name));
            itemTemplate.setParameter( "HIGH_LIMIT", CodeHeader::constantHighLimit( definition->prefix, definition->name, item->name));
            itemTemplate.setParameter( "UNITS",      CodeMenu::itemUnits( item));
            text += itemTemplate.toString();
            break;

         case CLASS_TYPE_CHAR :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "EditLetter.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            itemTemplate.setParameter( "DATA", dataName);
            itemTemplate.setParameter( "ITEM", itemName);
            text += itemTemplate.toString();
            break;

         case CLASS_TYPE_STRING :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "EditText.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            itemTemplate.setParameter( "DATA",  dataName);
            itemTemplate.setParameter( "ITEM",  itemName);
            itemTemplate.setParameter( "UITEM", OName::toUpperCase( item->name));
            itemTemplate.setParameter( "TEXT",  itemName);
            itemTemplate.setParameter( "TEXT_LENGTH",  CodeHeader::constantIdentifier( definition->prefix, definition->name, item->name, QString( "Size")));
            text += itemTemplate.toString();
            break;

         case CLASS_TYPE_PASSWORD :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "EditPassword.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            itemTemplate.setParameter( "DATA",  dataName);
            itemTemplate.setParameter( "ITEM",  itemName);
            itemTemplate.setParameter( "UITEM", OName::toUpperCase( item->name));
            itemTemplate.setParameter( "TEXT",  itemName);
            itemTemplate.setParameter( "TEXT_LENGTH",  CodeHeader::constantIdentifier( definition->prefix, definition->name, item->name, QString( "Size")));
            text += itemTemplate.toString();
            break;

         case CLASS_TYPE_ARRAY :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "EditArray.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            itemTemplate.setParameter( "DATA",  dataName);
            itemTemplate.setParameter( "ITEM",  itemName);
            itemTemplate.setParameter( "UITEM", OName::toUpperCase( item->name));
            itemTemplate.setParameter( "ARRAY_SIZE",  CodeHeader::constantIdentifier( definition->prefix, definition->name, item->name, QString( "Size")));
            text += itemTemplate.toString();
            break;

         case CLASS_TYPE_ENUM :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "EditEnum.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            itemTemplate.setParameter( "DATA", dataName);
            itemTemplate.setParameter( "ITEM", itemName);
            itemTemplate.setParameter( "ENUM_STRINGS", Dacs5CodeString::enumConstant( item->className));
            itemTemplate.setParameter( "ENUM_LAST", CodeHeader::enumConstantLast( item->className));
            text += itemTemplate.toString();
            break;

         case CLASS_TYPE_EMBEDDED :
            itemTemplate.setFileName( QString( GENERATOR_TEMPLATE_PATH) + QString( "Edit") + item->editor + QString(".c"));
            itemTemplate.setParameter( "MENU_ITEM",  itemConstant);
            itemTemplate.setParameter( "DATA",       dataName);
            itemTemplate.setParameter( "ITEM",       itemName);
            itemTemplate.setParameter( "TYPE",       item->dataName);
            itemTemplate.setParameter( "WIDTH",      QString::number( item->width));
            itemTemplate.setParameter( "DECIMALS",   QString::number( item->decimals));
            itemTemplate.setParameter( "LOW_LIMIT",  CodeHeader::constantLowLimit( definition->prefix, definition->name, item->name));
            itemTemplate.setParameter( "HIGH_LIMIT", CodeHeader::constantHighLimit( definition->prefix, definition->name, item->name));
            itemTemplate.setParameter( "UNITS",      CodeMenu::itemUnits( item));
            text += itemTemplate.toString();
            break;

         case CLASS_TYPE_CUSTOM :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "EditMethod.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            itemTemplate.setParameter( "METHOD",    QString( "Menu") + item->editor);
            text += itemTemplate.toString();
            break;

         default :
            break;
      }
   }
   if( !text.isEmpty()){
      text.remove( text.size() - 1, 1);          // remove last linefeed
   }
   return( text);
} // caseList

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

QString CodeMenu::parametersList( const DataDefinition *definition)
// generate menu parameters list
{
   QString        text;
   SourceTemplate itemTemplate;
   // data items :
   const DataItem *item;
   for( int i = 0; i < definition->item.count(); i++){
      item = &definition->item[ i];
      QString        itemConstant = CodeMenu::enumConstant( item->name);
      QString        itemName     = OName::toCamelCase( item->name);
      // check for data type :
      switch( item->dataType){
         case DATA_TYPE_CUSTOM :
         case DATA_TYPE_STRUCT :
            break;

         case DATA_TYPE_MENU :
         case DATA_TYPE_METHOD :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "DisplayMethod.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            text += itemTemplate.toString();
            continue;

         case DATA_TYPE_ALIAS :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "DisplayAlias.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            text += itemTemplate.toString();
            continue;

         default :
            continue;                  // don't generate item
      }
      // check for class type :
      switch( item->classType){
         case CLASS_TYPE_YESNO :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "DisplayYesNo.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            itemTemplate.setParameter( "ITEM", itemName);
            text += itemTemplate.toString();
            break;

         case CLASS_TYPE_IP :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "DisplayIp.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            itemTemplate.setParameter( "ITEM", itemName);
            text += itemTemplate.toString();
            break;

         case CLASS_TYPE_UINT :
         case CLASS_TYPE_INT :
         case CLASS_TYPE_FIXED :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "DisplayNumber.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            itemTemplate.setParameter( "ITEM", itemName);
            if( item->units.isEmpty()){
               if( item->classType == CLASS_TYPE_FIXED){
                  itemTemplate.setParameter( "FORMAT", QString( "%%1.%2f").arg( item->width).arg( item->decimals));
               } else {
                  itemTemplate.setParameter( "FORMAT", QString( "%d"));
               }
            } else {
               if( item->classType == CLASS_TYPE_FIXED){
                  itemTemplate.setParameter( "FORMAT", QString( "%%1.%2f %s").arg( item->width).arg( item->decimals));
               } else {
                  itemTemplate.setParameter( "FORMAT", QString( "%d %s"));
               }
            }
            itemTemplate.setParameter( "UNITS", CodeMenu::itemUnits( item));
            text += itemTemplate.toString();
            break;

         case CLASS_TYPE_FLOAT :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "DisplayFloat.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            itemTemplate.setParameter( "ITEM", itemName);
            if( item->units.isEmpty()){
               itemTemplate.setParameter( "FORMAT", QString( "%%1.%2g").arg( item->width).arg( item->decimals));
            } else {
               itemTemplate.setParameter( "FORMAT", QString( "%%1.%2g %s").arg( item->width).arg( item->decimals));
            }
            itemTemplate.setParameter( "UNITS", CodeMenu::itemUnits( item));
            text += itemTemplate.toString();
            break;

         case CLASS_TYPE_CHAR :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "DisplayLetter.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            itemTemplate.setParameter( "ITEM", itemName);
            text += itemTemplate.toString();
            break;

         case CLASS_TYPE_STRING :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "DisplayText.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            itemTemplate.setParameter( "ITEM", itemName);
            text += itemTemplate.toString();
            break;

         case CLASS_TYPE_PASSWORD :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "DisplayPassword.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            itemTemplate.setParameter( "ITEM", itemName);
            text += itemTemplate.toString();
            break;

         case CLASS_TYPE_ARRAY :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "DisplayArray.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            text += itemTemplate.toString();
            break;

         case CLASS_TYPE_ENUM :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "DisplayEnum.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            itemTemplate.setParameter( "ITEM", itemName);
            itemTemplate.setParameter( "ENUM_STRINGS", Dacs5CodeString::enumConstant( item->className));
            text += itemTemplate.toString();
            break;

         case CLASS_TYPE_EMBEDDED :
            itemTemplate.setFileName( QString( GENERATOR_TEMPLATE_PATH) + QString( "Display") + item->editor + QString(".c"));
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            itemTemplate.setParameter( "ITEM",     itemName);
            itemTemplate.setParameter( "CLASS",    OName::toCamelCase( item->className));
            itemTemplate.setParameter( "WIDTH",    QString::number( item->width));
            itemTemplate.setParameter( "DECIMALS", QString::number( item->decimals));
            itemTemplate.setParameter( "UNITS",    CodeMenu::itemUnits( item));
            text += itemTemplate.toString();
            break;

         case CLASS_TYPE_CUSTOM :
            itemTemplate.setFileName( GENERATOR_TEMPLATE_PATH "DisplayMethod.c");
            itemTemplate.setParameter( "MENU_ITEM", itemConstant);
            text += itemTemplate.toString();
            break;

         default :
            break;
      }
   }
   if( !text.isEmpty()){
      text.remove( text.size() - 1, 1);          // remove last linefeed
   }
   return( text);
} // parametersList

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

QString CodeMenu::enumConstant( QString name)
// generate menu item constant
{
   return( QString( "MI_") + OName::toUpperCase( name));
} // enumConstant

//------------------------------------------------------------------------------
//  Units
//------------------------------------------------------------------------------

QString CodeMenu::itemUnits( const DataItem *item)
// generate units format string
{
   if( item->units.isEmpty()){
      return( QString( "0"));
   }
   return( QChar( '"') + item->units + QChar( '"'));
} // itemUnits

//------------------------------------------------------------------------------
//  Menu header
//------------------------------------------------------------------------------

QString CodeMenu::menuHeader( QString fileName, const DataDefinition *definition)
// generate menu header source by <definition> with main <fileName> header
{
   SourceTemplate headerTemplate;
   headerTemplate.setFileName( MENU_HEADER_FILE_NAME);
   headerTemplate.setParameter( "INCLUDE",     CodeHeader::includeFile( QString(), fileName)); // main header
   // file title :
   headerTemplate.setParameter( "NAME",        OName::toCamelCase( definition->menuPrefix + QChar( ' ') + definition->name));
   headerTemplate.setParameter( "LNAME",       OName::toLowerCase( definition->menuPrefix + QChar( ' ') + definition->name));
   headerTemplate.setParameter( "DESCRIPTION", OName::toText(      definition->menuPrefix + QChar( ' ') + definition->name + QString( " menu")));
   return( headerTemplate.toString());
} // menuHeader
