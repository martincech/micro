//******************************************************************************
//
//   EmbeddedParser.h     Embeded data definitions parser
//   Version 1.0   (c)    VEIT Electronics
//
//******************************************************************************

#ifndef EMBEDEDPARSER_H
   #define EMBEDEDPARSER_H

#include <QString>
#include <QVector>
#include "Model/ArrayModel.h"
#include "uGenerator/GeneratorDef.h"

typedef QVector<EmbeddedDefinition> EmbeddedDefinitonList;

class EmbeddedParser
{
public:
   EmbeddedParser( QString fileName);
   // create data definitions from <fileName>

   ~EmbeddedParser();
   // destructor

   int itemsCount();
   // returns number of items

   const EmbeddedDefinition *getDefinition( QString name);
   // returns definition by <name>

   const EmbeddedDefinition *definitionAt( int index);
   // returns definition at <index> position

   bool hasName( QString name);
   // check for <name>

   int dataType( QString dataTypeName);
   // returns data type by <dataTypeName>

   int classType( QString className);
   // returns class type by <className>

private :
   EmbeddedDefinitonList _embeddedList;
   Enum                  _dataTypeEnum;
   Enum                  _classTypeEnum;
}; // EmbeddedParser

#endif // EMBEDEDPARSER_H
