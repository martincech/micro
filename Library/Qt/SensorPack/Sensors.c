#include "SensorPack/Sensors/Sensors.h"


void SensorsInit( void)
{

}

int8 TemperatureRead( void)
{
   return 25;
}

uint8 HumidityRead( void)
{
   return 53;
}

uint16 CO2Read( void)
{
   return 365;
}


int8 TemperatureAverageRead( void)
// Read average, starts new averaging period
{
   return TemperatureRead();
}

uint8 HumidityAverageRead( void)
// Read average, starts new averaging period
{
   return HumidityRead();
}

uint16 CO2AverageRead( void)
// Read average, starts new averaging period
{
   return CO2Read();
}


void SensorsSetFromModbus( TSensorsModbusRegisters *Sensors)
{

}
