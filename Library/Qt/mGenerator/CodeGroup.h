//******************************************************************************
//
//   CodeGroup.c        Source/header code generator for every register group
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef CODEGROUP_H
   #define CODEGROUP_H

#include "mGenerator/GeneratorDef.h"
#include <QString>

namespace CodeGroup
{
   QString sourceCode( RegisterGroup *group);
   // generate full source code for this <group>

   QString headerCode( const RegisterGroup *group);
   // generate header for this <group>

   QString fileName(  const RegisterGroup *group);
   // return file name for this group (without .c or .h)

   //------------------------------------------------------------------------------
   QString groupMapping(const QHash<QString, RegisterGroup> bufferedGroup, QString groupName);
   // helper function for group generation - create group mapping definition function

   QString saveExecutive(RegisterGroup group, const int groupOffset = 0);
   // helper function for group generation - create save executive

   QString arrayTypeRegistersRead(QList<Registers> regs, QString groupName);
   // get read for array type registers
   QString nonArrayTypeRegistersRead(Registers regs, QString groupName);
   // get read for non array type registers
}

#endif // CODEGROUP_H
