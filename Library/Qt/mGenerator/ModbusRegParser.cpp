#include "ModbusRegParser.h"
#include <QMessageBox>

#define GROUP_COLUMN          0
#define ADDRESS_COLUMN        1
#define NAME_COLUMN           2
#define RW_COLUMN             3
#define GENERATOR_TYPE_COLUMN 4
#define RANGE_COLUMN          5
#define DEFAULT_VALUE_COLUMN  6
#define DATA_NAME_COLUMN      8
#define DATA_FILE_COLUMN      9
#define DATA_GETTER_COLUMN    10
#define DATA_SETTER_COLUMN    11
#define DATA_GS_LOCALS_COLUMN 12
#define DESCRIPTION_COLUMN    13

#define CHAR_THREE_DOTS  ((wchar_t)0x2026) //"�"

#define ARRAY_MODEL_COLUMNS  8
#define ARRAY_MODEL_TITLE_COLUMN_1 "Name"
#define ARRAY_MODEL_TITLE_COLUMN_2 "Number"
#define ARRAY_MODEL_TITLE_COLUMN_3 "Readable"
#define ARRAY_MODEL_TITLE_COLUMN_4 "Writable"
#define ARRAY_MODEL_TITLE_COLUMN_5 "Range from"
#define ARRAY_MODEL_TITLE_COLUMN_6 "Range to"
#define ARRAY_MODEL_TITLE_COLUMN_7 "Data name"
#define ARRAY_MODEL_TITLE_COLUMN_8 "Data file"

ModbusRegParser::ModbusRegParser(ArrayModel *registerArray)
{
int            rows;
int            titleRow;
int            cols;
int            currentAddress;
bool           arrayType;
int            arrayTypeCount;
QString        groupName;
QString        groupValue;
QString        groupVars;
QString        loGroupName;
QString        regAddress;
QString        regRW;
QString        regValueRange;
QString        regName;
QString        regDataName;
QString        regDataFile;
QString        regDataGetter;
QString        regDataSetter;
QString        regDataGSLocal;
QString        regType;
QString        regDesc;
QString        regValueDefault;
QStringList    stringList;
QStringList    slist;
RegisterGroup  group;

   titleRow = -1;
   rows = registerArray->getRowsCount();
   for( int row = 0; row < rows; row++){
      groupName = registerArray->getItem(row, GROUP_COLUMN);
      groupValue = registerArray->getItem(row, ADDRESS_COLUMN);
      groupVars = registerArray->getItem(row, NAME_COLUMN);
      if( !groupName.isEmpty()){
         // new group name or keyword,
         // check for keywords
         loGroupName = groupName.toLower();
         loGroupName.remove(" ");
         if( loGroupName == QString( KEYWORD_VERSION)){
            // version
            if( groupValue.isEmpty()){
               continue;
            }
            stringList = groupValue.split(".");
            _versionMajor = stringList.at(0).toInt();
            _versionMinor = stringList.at(1).toInt();
            continue;
         }

         if( loGroupName == QString( KEYWORD_GROUP)){
            // data header
            cols = registerArray->getColumnsCount();
            for( int col = 0; col < cols; col++){
               registerArray->setTitle(col, registerArray->getItem(row, col));
            }
            titleRow = row;
            continue;
         }
         // no keyword, group name
         group.item.clear();
         group.name = groupName;
         group.base = groupValue.toInt();
         group.variableTypes.clear();
         group.variables.clear();
         if( !groupVars.isEmpty()){
            stringList = groupVars.split(";");
            stringList.removeAll("");
            for( int i = 0; i < stringList.count(); i++){
               loGroupName = stringList[ i];
               slist = loGroupName.split(QRegExp("\\s+"));
               if(slist.count()< 2){
                  continue;
               }
               group.variableTypes << slist[0];
               group.variables << slist[1];
            }
         }
      } else {
         continue;
      }

      currentAddress = group.base;
      arrayType = false;
      arrayTypeCount = 0;
      while(row < rows){
         row++;
         //process registers from this group
         if( !registerArray->getItem(row, GROUP_COLUMN).isEmpty()){
            // new group
            row--;//return index to previous value
            break;
         }

         regRW          = registerArray->getItem(row, RW_COLUMN).toLower();
         regValueRange  = registerArray->getItem(row, RANGE_COLUMN);
         regName        = registerArray->getItem(row, NAME_COLUMN);
         regAddress     = registerArray->getItem(row, ADDRESS_COLUMN);
         regDataName    = registerArray->getItem(row, DATA_NAME_COLUMN);
         regDataFile    = registerArray->getItem(row, DATA_FILE_COLUMN);
         regDataGetter  = registerArray->getItem(row, DATA_GETTER_COLUMN);
         regDataSetter  = registerArray->getItem(row, DATA_SETTER_COLUMN);
         regDataGSLocal = registerArray->getItem(row, DATA_GS_LOCALS_COLUMN);
         regDataGetter.replace(QRegExp(";[ ]*"), QString(";"));
         regDataSetter.replace(QRegExp(";[ ]*"), QString(";"));
         regDataGSLocal.replace(QRegExp(";[ ]*"), QString(";"));
         regDataGetter.remove(QRegExp(";[ ]*$"));
         regDataSetter.remove(QRegExp(";[ ]*$"));
         regDataGSLocal.remove(QRegExp(";[ ]*$"));
         regType        = registerArray->getItem(row, GENERATOR_TYPE_COLUMN);
         regDesc        = registerArray->getItem(row, DESCRIPTION_COLUMN);
         regValueDefault= registerArray->getItem(row, DEFAULT_VALUE_COLUMN);

         if( !regAddress.isEmpty()){
            // just subgroup separator
            currentAddress = regAddress.toInt() + group.base;
            continue;
         }
         if( regName.isEmpty() && !regValueRange.isEmpty()){
            //just range to
            updateRangeTo(&group, group.item.count() - 1, regValueRange);
            continue;
         }

         if( !regName.isEmpty() && (( regName == "...") || ( regName.unicode()[0] == CHAR_THREE_DOTS))){
            //array definition
            updateArrayType( &group, group.item.count() - 1, ++arrayTypeCount);
            arrayType = true;
            continue;
         }
         if( regName.isEmpty()){
            //empty line or comment line
            continue;
         }
         Register reg;
         if(arrayType){
            // end of array register
            reg = group.item.last();
            currentAddress = addArrayRegisters( &group, &reg, regName.split("_").last().toInt());
            arrayType = false;
            continue;
         }
         //regular register or begining of array
         reg.name = regName;
         reg.num = currentAddress++;
         reg.read = regRW.contains("r");
         reg.write = regRW.contains("w");
         reg.stringLen = "";
         //reg.from and reg.to
         decodeRange( &reg, regValueRange);
         reg.dataName = regDataName;
         reg.dataFile = regDataFile;
         reg.dataGetter = regDataGetter;
         reg.dataSetter = regDataSetter;
         reg.dataGSLocals = regDataGSLocal;
         reg.arrayType = 0;
         reg.registerType = decodeType( regType);
         if(reg.registerType == REGISTER_STRING && reg.stringLen.isEmpty()){
            reg.stringLen = QString::number( reg.from);
         }
         reg.description = regDesc;
         reg.defaultValue = regValueDefault;
         group.item.append(reg);
      }
      _registerList.append(group);
   }

   if( titleRow >= 0){
      registerArray->removeRows( titleRow, 1);
   }

   // check for string buffer if some string register exists
   bool stringExists = false;
   bool stringBufferExists = false;
   foreach (RegisterGroup group, _registerList) {
      foreach (Register reg, group.item) {
         if(reg.registerType == REGISTER_STRING){
            stringExists = true;
         }
         if(reg.registerType == REGISTER_STRING_BUFFER){
            stringBufferExists = true;
         }
      }
   }
   if(stringExists && !stringBufferExists){
      QMessageBox::warning(NULL, "Missing string buffer", "String register type found but string buffer not! This may cause problems in result code. Please see documentation.");
   }
   regModel = NULL;
}

//------------------------------------------------------------------------------
//  Destructor
//------------------------------------------------------------------------------

ModbusRegParser::~ModbusRegParser()
// destructor
{
} // ModbusRegParser

//------------------------------------------------------------------------------
//  Count
//------------------------------------------------------------------------------

int ModbusRegParser::itemsCount()
// returns number of items
{
   return( _registerList.count());
} // itemsCount

//------------------------------------------------------------------------------
//  Item by name
//------------------------------------------------------------------------------

const RegisterGroup *ModbusRegParser::getGroup( QString name)
// returns definition by <name>
{
   for( int i = 0; i < _registerList.count(); i++){
      if( _registerList.at( i).name == name){
         return( &_registerList.at( i));
      }
   }
   return( NULL);
} // getDefinition

//------------------------------------------------------------------------------
//  Name by index
//------------------------------------------------------------------------------

RegisterGroup *ModbusRegParser::groupAt( int index)
// returns definition at <index> position
{
   if( index >= _registerList.count()){
      return( NULL);
   }
   return( const_cast<RegisterGroup *>(&_registerList.at( index)));
} // definitionAt

//------------------------------------------------------------------------------
//  Has name
//------------------------------------------------------------------------------

bool ModbusRegParser::hasName( QString name)
// check for <name>
{
   for( int i = 0; i < _registerList.count(); i++){
      if( _registerList.at( i).name == name){
         return( true);
      }
   }
   return( false);
} // hasName

//------------------------------------------------------------------------------
//  Add all array registers to group
//------------------------------------------------------------------------------

int ModbusRegParser::addArrayRegisters( RegisterGroup *toGroup, Register *first, int to)
{
QStringList nameList;
Register    newReg;
bool        TwoNumbers;
int         num;

   TwoNumbers = false;
   num = first->num;
   nameList = first->name.split("_");
   if( nameList.count() - 2 >= 0){
      nameList.at(nameList.count() - 2).toInt(&TwoNumbers);
   }

   for( int i = nameList.last().toInt() + 1; i <= to; TwoNumbers? i+=2 : i++){
      newReg = *first;
      nameList.removeLast();
      if( TwoNumbers){
         nameList.removeLast();
      }
      nameList.append(QString::number(i));
      if( TwoNumbers){
         nameList.append(QString::number(i+1));
      }
      newReg.name = nameList.join("_");
      newReg.num = ++num;
      toGroup->item.append(newReg);
   }
   return ++num;
}

//------------------------------------------------------------------------------
//  Update range to for register at index
//------------------------------------------------------------------------------

void ModbusRegParser::updateRangeTo( RegisterGroup *group, int index, QString &regValueRangeField)
{
Register reg;
bool     isInt;

   reg  = group->item.at(index);
   reg.to = regValueRangeField.toInt(&isInt);
   if( !isInt){
      //range not numeric but character
      reg.to = regValueRangeField.toAscii().at(0);
   }
   group->item.replace(index, reg);
}

//------------------------------------------------------------------------------
//  Update range to for register at index
//------------------------------------------------------------------------------

void ModbusRegParser::updateArrayType( RegisterGroup *group, int index, int type)
{
Register reg;

   reg  = group->item.at(index);
   reg.arrayType = type;
   group->item.replace(index, reg);
}

//------------------------------------------------------------------------------
//  Decode range from range field
//------------------------------------------------------------------------------

void ModbusRegParser::decodeRange( Register *reg, QString &regValueRangeField)
{
bool           isInt;
QStringList    stringList;
QByteArray     byteArray;

   if( regValueRangeField.isEmpty()){
      reg->from = 0;
      reg->to = 0xffff;
      return;
   }

   stringList = regValueRangeField.split("..");

   if( regValueRangeField.contains("/")){
      stringList = regValueRangeField.split("/");
   }
   reg->from = stringList.at(0).toInt(&isInt);
   if( !isInt){
      if( stringList.at(0).length() > 1){
         //range not numeric but string
         reg->stringLen = stringList.at(0);
         reg->from = 0;
      } else {
         //range not numeric but character
         byteArray = stringList.at(0).toAscii();
         if( byteArray.isEmpty()){
            reg->from = ' ';
         } else {
            reg->from = byteArray.at(0);
         }
      }
   }
   if( stringList.count() <= 1){
      reg->to = 0;
      return;
   }
   reg->to = stringList.at(1).toInt(&isInt);
   if( !isInt){
      //range not numeric but character
      byteArray = stringList.at(1).toAscii();
      if( byteArray.isEmpty()){
         reg->to = ' ';
      } else {
         reg->to = byteArray.at(0);
      }
   }
}

//------------------------------------------------------------------------------
//  Decode type from type field
//------------------------------------------------------------------------------

ERegisterType ModbusRegParser::decodeType( QString &typeField)
{
   typeField.remove(" ");
   typeField = typeField.toLower();

   if( typeField == "stringbuffer"){
      return REGISTER_STRING_BUFFER;
   }
   if( typeField == "command"){
      return REGISTER_COMMAND;
   }
   if( typeField == "string"){
      return REGISTER_STRING;
   }

   return REGISTER_REGISTER;
}

const ArrayModel *ModbusRegParser::regGroupToArrayModel( int index)
{
   return regGroupToArrayModel( groupAt(index));
}

const ArrayModel *ModbusRegParser::regGroupToArrayModel( QString name)
{
   return regGroupToArrayModel( getGroup(name));
}

const ArrayModel *ModbusRegParser::regGroupToArrayModel( const RegisterGroup *group)
// returns array model of all registers
{
int          rows;
QStringList  items;
Register     reg;

   if( !group){
      return NULL;
   }

   if( regModel){
      delete regModel;
   }
   rows = group->item.count();
   regModel = new ArrayModel( rows, ARRAY_MODEL_COLUMNS);
   regModel->setTitle(0, ARRAY_MODEL_TITLE_COLUMN_1);
   regModel->setTitle(1, ARRAY_MODEL_TITLE_COLUMN_2);
   regModel->setTitle(2, ARRAY_MODEL_TITLE_COLUMN_3);
   regModel->setTitle(3, ARRAY_MODEL_TITLE_COLUMN_4);
   regModel->setTitle(4, ARRAY_MODEL_TITLE_COLUMN_5);
   regModel->setTitle(5, ARRAY_MODEL_TITLE_COLUMN_6);
   regModel->setTitle(6, ARRAY_MODEL_TITLE_COLUMN_7);
   regModel->setTitle(7, ARRAY_MODEL_TITLE_COLUMN_8);

   for( int row = 0; row < rows; row++){
      reg = group->item.at(row);
      items.clear();
      items.append(reg.name);
      items.append(QString::number(reg.num));
      items.append(reg.read? QString("YES"): QString("NO"));
      items.append(reg.write? QString("YES"): QString("NO"));
      items.append(QString::number(reg.from));
      items.append(QString::number(reg.to));
      items.append(reg.dataName);
      items.append(reg.dataFile);
      regModel->setRow(row, items);
   }
   return regModel;
}

