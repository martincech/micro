//******************************************************************************
//
//   GeneratorDef.h     Modbus code generator definitions
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __GeneratorDef_H__
   #define __GeneratorDef_H__

#include <QString>
#include <QVector>
#include <QStringList>
#include <QHash>

#ifndef UGENERATORDEF_H
   #include "uGenerator/GeneratorDef.h"
#endif
//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------
#define REGISTER_COLUMNS_MAX   15     // register columns
#define M_GENERATOR_TEMPLATE_PATH     GENERATOR_TEMPLATE_PATH"Modbus/"

#define HEADER_FILE_NAME          M_GENERATOR_TEMPLATE_PATH "HeaderWithFunc.h"
#define SOURCE_FILE_NAME          M_GENERATOR_TEMPLATE_PATH "Source.c"

#define RANGE_CHECK_DECLARATION   M_GENERATOR_TEMPLATE_PATH "RangeCheckDeclaration.c"
#define RANGE_CHECK_DEFINITION    M_GENERATOR_TEMPLATE_PATH "RangeCheckDefinition.c"
#define RANGE_CHECK_SWITCH        M_GENERATOR_TEMPLATE_PATH "RangeCheckSwitch.c"
#define RANGE_CHECK_CASE          M_GENERATOR_TEMPLATE_PATH "RangeCheckCase.c"
#define RANGE_CHECK_ARRAY         M_GENERATOR_TEMPLATE_PATH "RangeCheckArray.c"

#define REG_BUFFER                M_GENERATOR_TEMPLATE_PATH "RegBufferType.c"
#define REG_BUFFER_STRUCT         M_GENERATOR_TEMPLATE_PATH "RegBufferTypeStruct.c"
#define REG_BUFFER_NON_UNION      M_GENERATOR_TEMPLATE_PATH "RegBufferTypeNonUnion.c"
#define REG_CORRECT_NUM           M_GENERATOR_TEMPLATE_PATH "RegCorrectNum.c"
#define REG_CORRECT_NUM_CONDITION M_GENERATOR_TEMPLATE_PATH "RegCorrectNumCondition.c"
#define REG_GROUP_CALL            M_GENERATOR_TEMPLATE_PATH "RegGroupCallIf.c"

#define GROUP_READ_SWITCH         M_GENERATOR_TEMPLATE_PATH "GroupReadSwitch.c"
#define GROUP_READ_CASE           M_GENERATOR_TEMPLATE_PATH "GroupReadCase.c"
#define GROUP_READ_ARRAY          M_GENERATOR_TEMPLATE_PATH "GroupReadArray.c"
#define GROUP_WRITE_BUFFERED      M_GENERATOR_TEMPLATE_PATH "GroupWriteBuffered.c"
#define GROUP_WRITE               M_GENERATOR_TEMPLATE_PATH "GroupWrite.c"
#define GROUP_WRITE_ARRAY         M_GENERATOR_TEMPLATE_PATH "GroupWriteArray.c"
#define GROUP_WRITE_STRING_BUFFER M_GENERATOR_TEMPLATE_PATH "GroupWriteStringBuffer.c"
#define GROUP_WRITE_COMMAND       M_GENERATOR_TEMPLATE_PATH "GroupWriteCommand.c"
#define GROUP_WRITE_STRING        M_GENERATOR_TEMPLATE_PATH "GroupWriteString.c"
#define GROUP_WRITE_GLOBAL        M_GENERATOR_TEMPLATE_PATH "GroupWriteGlobal.c"
#define GROUP_SAVE                M_GENERATOR_TEMPLATE_PATH "GroupSave.c"
#define GROUP_SAVE_ARRAY          M_GENERATOR_TEMPLATE_PATH "GroupSaveArray.c"

#define MODBUS_STACK_PATH         M_GENERATOR_TEMPLATE_PATH "Stack"

#define MODBUS_RANGE_CHECK_FILENAME "ModbusRegRangeCheck"
#define MODBUS_REG_FILENAME "ModbusReg"
//------------------------------------------------------------------------------
//  Keywords
//------------------------------------------------------------------------------

#define KEYWORD_VERSION      "version"
#define KEYWORD_GROUP        "registergroup"
#define KEYWORD_GLOBAL       "global"
//------------------------------------------------------------------------------
//  Register definition
//------------------------------------------------------------------------------
typedef enum {
   REGISTER_REGISTER,
   REGISTER_COMMAND,
   REGISTER_STRING,
   REGISTER_STRING_BUFFER
} ERegisterType;

typedef enum {
   REGISTER_R = 1,
   REGISTER_W = 2,
   REGISTER_RW = REGISTER_R | REGISTER_W,
} ERegisterRWType;

class Register {
public:
   QString        name;                      // register name
   int            num;                       // register number
   bool           read;                      // is for read
   bool           write;                     // is for write
   int            from;                      // range from
   int            to;                        // range to
   QString        defaultValue;              // default value
   int            arrayType;                 // this register is part of registers in array
   ERegisterType  registerType;              // type of register for generation purposes
   QString        dataName;                  // name of data which this register is for
   QString        dataFile;                  // name of data file with declaration of this data
   QString        dataGetter;                // getter for data represented by this register
   QString        dataSetter;                // setter of data represented by this register
   QString        dataGSLocals;              // local function values needed for setter/getter
   QString        stringLen;                 // length of string, valid only when registerType==REGISTER_STRING
   QString        description;               // data description


   bool operator==(const Register &other) const
   {
    return this->name == other.name && this->num == other.num;
   }

   void IndexReplacement();
   QStringList Includes();
   ERegisterRWType Type();
   QString ArrayName();
};

//------------------------------------------------------------------------------
//  Register group definition
//------------------------------------------------------------------------------

class Registers : public QVector<Register>
{
public:
   Registers(){}
   Registers(const Registers& d): QVector<Register>(d)
   {
   }
   Registers Exclude(Registers other) const;
   Registers OfType(int rwType, bool includeArrayTypes = true, bool includeGlobal = false) const;
   Registers OfType(ERegisterRWType rwType, bool includeArrayTypes = true, bool includeGlobal = false) const{
      return OfType((int) rwType, includeArrayTypes, includeGlobal);
   }
   Registers OfType(ERegisterType type, bool includeArrayTypes = true, bool includeGlobal = false) const;
   Registers OfType(ERegisterType type, int rwType, bool includeArrayTypes = true, bool includeGlobal = false) const;
   Registers OfType(ERegisterType type, ERegisterRWType rwType, bool includeArrayTypes = true, bool includeGlobal = false){
      return OfType(type, (int) rwType, includeArrayTypes, includeGlobal);
   }

   QList<Registers> ArrayTypeOfType(ERegisterType type, int rwType, bool includeGlobal = false) const;
   QList<Registers> OfArrayType() const;
   Registers OfNonArrayType() const;
   Registers OfTheSameArrayType(Register reg) const;
   Registers GlobalRegisters() const;


   Registers& operator=(const QVector<Register>& d)
   {
       QVector<Register>::operator=(d);
       return *this;
   }
};

class RegisterGroup {
public:
   RegisterGroup();
   ~RegisterGroup();
   QString     name;
   QStringList variableTypes;
   QStringList variables;
   int         base;
   Registers   item;

   int Count() const;

   const QHash<QString, RegisterGroup> *BufferedSubgroups();
   bool BufferedSubgroupsContainsReg(Register &reg);
   Register BufferedFirst();
   Register BufferedLast();


   Registers RegistersOfType(int rwType, bool includeArrayTypes = true, bool includeGlobal = false) const;
   Registers RegistersOfType(ERegisterType type, bool includeArrayTypes = true, bool includeGlobal = false) const;
   Registers RegistersOfType(ERegisterType type, int rwType, bool includeArrayTypes = true, bool includeGlobal = false) const;
   Registers RegistersOfTheSameArrayType(Register reg) const;
   Registers GlobalRegisters() const;
   QList<Registers> ArrayTypeRegistersOfType(ERegisterType type, int rwType, bool includeGlobal = false) const;

   void IndexReplacement();
   QStringList Includes() const;

private:
   QHash<QString, RegisterGroup> bufferedSubgroups;
   bool bufferedSubgroupsCreated;

};


#endif // __GeneratorDef_H__


