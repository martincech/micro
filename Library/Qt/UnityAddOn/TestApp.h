#include <QtCore/QCoreApplication>

class TestApp : public QCoreApplication
{
   Q_OBJECT

public:
   TestApp( int argc, char *argv[]);

   int exec( void);
};
