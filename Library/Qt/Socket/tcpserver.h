//******************************************************************************
//
//   TcpServer.h  TCP socket server
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QTcpServer>
#include <QTcpSocket>

#ifndef CRTDUMP_H
   #include "Crt/CrtDump.h"
#endif

#include "Socket/SocketDef.h"

//------------------------------------------------------------------------------
//   Socket server
//------------------------------------------------------------------------------

class TcpServer : public QTcpServer
{
   Q_OBJECT
public :
   TcpServer();
   TcpServer( QObject *parent);
   ~TcpServer();

   void setLogger( CrtDump *logger);
   // Set data visualisation <logger>

   bool start(QString name);
   // Start server with <name>
   bool start(QString name, int port);
   // Start server with <name> and port

   bool send( void *data, int size);
   // Send data to client

   int receive( void *data, int size);
   // Receive <data> with total <size>, returns size received

signals :
   void dataReceived();
   // Data received
   void clientDisconnected();
   // client disconnected

private slots :
   void connectClient();
   // New client connected

   void disconnectClient();
   // Client disconnected

   void dataReady();
   // Data received
   
protected :
   QTcpSocket   *_socket;
   CrtDump      *_logger;
   quint16       _receivedSize;
   quint8        _message[ SOCKET_MESSAGE_SIZE_MAX + sizeof( SocketMessageSize)];
}; // TcpServer

#endif // TCPSERVER_H
