#ifndef SOCKETPARAMETERS_H
#define SOCKETPARAMETERS_H

#include <QDialog>
#include "tcpclient.h"

namespace Ui {
   class SocketParameters;
}

class SocketParameters : public QDialog
{
   Q_OBJECT
   
public:
   explicit SocketParameters(QWidget *parent = 0);
   ~SocketParameters();

private slots:
   void on_pushButton_clicked();

signals:
   void connected(TcpClient &to);
   void quit();

private:
   Ui::SocketParameters *ui;
   TcpClient *socket;
};

#endif // SOCKETPARAMETERS_H
