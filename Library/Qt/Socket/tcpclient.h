//******************************************************************************
//
//   TcpClient.h     TCP socket client
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include <QTcpSocket>

#ifndef CRTDUMP_H
   #include "Crt/CrtDump.h"
#endif

#include "Socket/SocketDef.h"

//------------------------------------------------------------------------------
//   Socket server
//------------------------------------------------------------------------------

class TcpClient
{
public :
   TcpClient();
   TcpClient( const TcpClient &copy);
   virtual ~TcpClient();

   void setLogger( CrtDump *logger);
   // Set data visualisation <logger>

   virtual bool connect( QString name);
   // Connect server with <name>

   virtual bool connect( QString IPv4, quint16 port );
   // Connect server with <Ip> and <port>

   void disconnect();
   // Disconnect server

   virtual bool send( void *data, int size);
   // Send data to client

   virtual int receive( void *data, int size);
   // Receive <data> with total <size>, returns size received

protected :
   QTcpSocket   *_socket;
   CrtDump      *_logger;
   quint16       _receivedSize;
   quint8        _message[ SOCKET_MESSAGE_SIZE_MAX + sizeof( SocketMessageSize)];
}; // TcpClient

#endif // TCPCLIENT_H
