#include "SocketParameters.h"
#include "ui_SocketParameters.h"

SocketParameters::SocketParameters(QWidget *parent) :
   QDialog(parent),
   ui(new Ui::SocketParameters)
{
   ui->setupUi(this);
   socket = NULL;
}

SocketParameters::~SocketParameters()
{
   if( socket){
      delete socket;
      socket = NULL;
   }
   delete ui;
}

void SocketParameters::on_pushButton_clicked()
{
   if( socket){
      socket->disconnect();
      delete socket;
      socket = NULL;
   }
   socket = new TcpClient();
   CrtDump *crtdump = new CrtDump(ui->logger);
   socket->setLogger(crtdump);
   QString text = ui->IpEdit->text();
   if(!socket->connect(text, (quint16)ui->PortEdit->value())){
      socket->setLogger(NULL);
      delete(crtdump);
      return;
   }
   socket->setLogger(NULL);
   delete(crtdump);
   emit(connected(*socket));
}
