//******************************************************************************
//
//   TcpClient.cpp  TCP socket client
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "tcpclient.h"
#include <QHostAddress>
#include <string.h>

#define LoggerTx( frame, size)       if( _logger) _logger->show( CrtDump::TX, frame, size)
#define LoggerRx( frame, size)       if( _logger) _logger->show( CrtDump::RX, frame, size)
#define LoggerGarbage( frame, size)  if( _logger) _logger->show( CrtDump::GARBAGE, frame, size)
#define LoggerReport( txt, ...)      if( _logger) _logger->printf( txt, ##__VA_ARGS__)


#define SOCKET_RX_TIMEOUT 3000

//------------------------------------------------------------------------------
//   Constructor
//------------------------------------------------------------------------------

TcpClient::TcpClient()
{
   this->_socket       = NULL;
   _logger       = NULL;
   _receivedSize = 0;
} // TcpClient

TcpClient::TcpClient( const TcpClient &copy)
{
   this->_socket = new QTcpSocket(NULL);
   this->_socket->setSocketDescriptor(copy._socket->socketDescriptor());
   this->_logger = 0;
   this->_receivedSize = copy._receivedSize;
   memcpy(this->_message, copy._message, SOCKET_MESSAGE_SIZE_MAX + sizeof( SocketMessageSize));
}

//------------------------------------------------------------------------------
//   Destructor
//------------------------------------------------------------------------------

TcpClient::~TcpClient()
{
   if( !_socket){
      return;
   }
   _socket->abort();
   _socket->deleteLater();
} // ~TcpClient

//------------------------------------------------------------------------------
//   Connect
//------------------------------------------------------------------------------

bool TcpClient::connect( QString name)
// Connect server with <name>
{
   if( _socket){
      _socket->abort();
      _socket->deleteLater();
   }
   _socket = new QTcpSocket( NULL);
   _socket->connectToHost( name, SOCKET_PORT);
   if( !_socket->waitForConnected( SOCKET_RX_TIMEOUT)){
      LoggerReport("Wait for connect: %d -- %s\n", _socket->error(), _socket->errorString().toStdString().c_str());
      return( false);
   }
   return( true);
} // start

bool TcpClient::connect(QString IPv4, quint16 port )
// Connect server with <Ip> and <port>
{
   if( this->_socket){
      _socket->abort();
      _socket->deleteLater();
   }
   QHostAddress addr;
   if( !addr.setAddress(IPv4)){
      LoggerReport( "%s wrong IP\n", IPv4.toStdString().c_str());
      return NO;
   }
   _socket = new QTcpSocket( NULL);
   _socket->connectToHost( addr, port);
   if( !_socket->waitForConnected( SOCKET_RX_TIMEOUT)){
      LoggerReport("Wait for connect: %d -- %s\n", _socket->error(), _socket->errorString().toStdString().c_str());
      return( false);
   }
   return( true);
}

//------------------------------------------------------------------------------
//   Disconnect
//------------------------------------------------------------------------------

void TcpClient::disconnect()
// Disconnect server
{
   if( !_socket){
      return;
   }
   _socket->disconnectFromHost();
   if (_socket->state() != QAbstractSocket::UnconnectedState){
      _socket->waitForDisconnected( SOCKET_RX_TIMEOUT);
   }
} // disconnect

//------------------------------------------------------------------------------
//  Set logger
//------------------------------------------------------------------------------

void TcpClient::setLogger( CrtDump *logger)
// Set data visualisation <logger>
{
   _logger = logger;
} // setLogger

//------------------------------------------------------------------------------
//   Send
//------------------------------------------------------------------------------

bool TcpClient::send( void *data, int size)
// Send data to client
{
   if( size > SOCKET_MESSAGE_SIZE_MAX){
      return( false);                  // too long message
   }
   if( !_socket){
      return( false);                  // disconnected
   }
   _message[ 0] = size & 0xFF;
   _message[ 1] = (size >> 8) & 0xFF;
   memcpy( &_message[ 2], data, size);
   qint64 sizeSend = SocketMessageLength( size);
   qint64 sizeWritten;
   sizeWritten = _socket->write( (const char *)_message, sizeSend);
   if( sizeWritten != sizeSend){
      LoggerReport( "Send size mismatch\n");
      return( false);
   }
   _socket->flush();
   _receivedSize = 0;                  // wait for next command
   LoggerTx( _message, sizeWritten);
   return( true);
} // send

//------------------------------------------------------------------------------
//   Receive
//------------------------------------------------------------------------------

int TcpClient::receive( void *data, int size)
// Receive <data> with total <size>, returns size received
{
   if( !_socket){
      return( 0);                      // disconnected
   }
   if( !_socket->waitForReadyRead( SOCKET_RX_TIMEOUT)){
      return( 0);
   }
   qint64 availableSize;
   availableSize = _socket->bytesAvailable();
   if( availableSize == 0){
      return( 0);                      // no data received
   }
   qint64  readRequest;
   qint64  readSize;
   // read header :
   if( _receivedSize == 0){
      if( availableSize < (qint64)SocketHeaderLenght()){
         LoggerReport( "Short header\n");
         return( 0);                   // too short for header
      }
      readRequest = SocketHeaderLenght();
      readSize    = _socket->read( (char *)_message, readRequest);
      if( readSize != readRequest){
         LoggerReport( "Header read failed\n");
         return( 0);                   // read failed
      }
      availableSize -= readSize;
      _receivedSize  = (SocketMessageSize)_message[ 0] | ((SocketMessageSize)_message[ 1] << 8);
      LoggerReport( "Header read ok\n");
   }
   // read message body :
   if( _receivedSize > availableSize){
      LoggerReport( "Message data short\n");
      return( 0);                      // incomplete message
   }
   readRequest = _receivedSize;
   readSize = _socket->read( (char *)&_message[ 2], readRequest);
   if( readSize != readRequest){
      LoggerReport( "Message body size mismatch\n");
      _receivedSize = 0;               // start new reception
      return( 0);                      // read failed
   }
   if( _receivedSize > size){
      LoggerReport( "Message too large\n");
      _receivedSize = 0;               // start new reception
      return( 0);                      // message too long
   }
   memcpy( data, &_message[ 2], (size_t)readSize);
   LoggerRx( _message, SocketMessageLength( readSize));
   _receivedSize = 0;                  // start new reception
   return( (int)readSize);             // return size read
} // receive
