//******************************************************************************
//
//   LocalServer.h   Localhost socket server
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef LOCALSERVER_H
#define LOCALSERVER_H

#include <qlocalserver.h>
#include <qlocalsocket.h>

#ifndef CRTDUMP_H
   #include "Crt/CrtDump.h"
#endif

#include "Socket/SocketDef.h"

//------------------------------------------------------------------------------
//   Socket server
//------------------------------------------------------------------------------

class LocalServer : public QLocalServer
{
   Q_OBJECT
public :
   LocalServer( QObject *parent);
   ~LocalServer();

   void setLogger( CrtDump *logger);
   // Set data visualisation <logger>

   bool start( QString name);
   // Start server with <name>

   bool send( void *data, int size);
   // Send data to client

   int receive( void *data, int size);
   // Receive <data> with total <size>, returns size received

signals :
   void dataReceived();
   // Data received

private slots :
   void connectClient();
   // New client connected

   void disconnectClient();
   // Client disconnected

   void dataReady();
   // Data received
   
private :
   QLocalSocket *_socket;
   CrtDump      *_logger;
   quint16       _receivedSize;
   quint8        _message[ SOCKET_MESSAGE_SIZE_MAX + sizeof( SocketMessageSize)];
}; // SocketServer

#endif // LOCALSERVER_H
