//******************************************************************************
//
//   LocalClient.h   Localhost socket client
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef LOCALCLIENT_H
#define LOCALCLIENT_H

#include <qlocalsocket.h>

#ifndef CRTDUMP_H
   #include "Crt/CrtDump.h"
#endif

#include "Socket/SocketDef.h"

//------------------------------------------------------------------------------
//   Socket server
//------------------------------------------------------------------------------

class LocalClient : public QObject
{
   Q_OBJECT
public :
   LocalClient( QObject *parent);
   ~LocalClient();

   void setLogger( CrtDump *logger);
   // Set data visualisation <logger>

   bool connect( QString name);
   // Connect server with <name>

   void disconnect();
   // Disconnect server

   bool send( void *data, int size);
   // Send data to client

   int receive( void *data, int size);
   // Receive <data> with total <size>, returns size received

private slots :
   
private :
   QLocalSocket *_socket;
   CrtDump      *_logger;
   quint16       _receivedSize;
   quint8        _message[ SOCKET_MESSAGE_SIZE_MAX + sizeof( SocketMessageSize)];
}; // LocalClient

#endif // LOCALCLIENT_H
