//*****************************************************************************
//
//   LcdDisplay.cpp  LCD display simulator
//   Version 1.0     (c) VymOs
//
//*****************************************************************************

#include "lcddisplay.h"
#include <QPen>
#include <QBrush>
#include <QPixmap>
#include <QPainter>
#include <QClipboard>
#include <QApplication>
#include <QPaintEvent>

#define LCD_ZOOM   2                   // screen pixels to LCD pixel

// Color planes :
//            White   Light gray   Dark Gray Black
// Plane 0    0       1            0         1
// Plane 1    0       0            1         1

// Color transformation :
#define RGB_BLACK      (QRgb)0x00000000
#define RGB_WHITE      (QRgb)0x00FFFFFF
#define RGB_LIGHTGRAY  (QRgb)0x00C0C0C0
#define RGB_DARKGRAY   (QRgb)0x00606060

TGraphicBuffer GBuffer;

static LcdDisplay *mLcdDisplay;

//-----------------------------------------------------------------------------
// C - access functions
//-----------------------------------------------------------------------------

void GpuInit( void)
// Initialisation
{
   mLcdDisplay->initializeBuffer();
} // GpuInit

void GpuFlush( void)
// Redraw buffer
{
   mLcdDisplay->updateBuffer();
} // GpuFlush

void GpuContrast( unsigned Value)
// Set display contrast
{
} // GpuContrast

void GpuShutdown( void)
// Power off sequence
{
} // GpuShutdown

//*****************************************************************************

//-----------------------------------------------------------------------------
// Constructor
//-----------------------------------------------------------------------------

LcdDisplay::LcdDisplay( QWidget *parent) : QWidget(parent)
// Constructor
{
   setAttribute( Qt::WA_StaticContents);
   setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed);

   resize( LCD_ZOOM * G_WIDTH, LCD_ZOOM * G_HEIGHT);
   // initialize pixmap :
   mPixmap = new QPixmap( LCD_ZOOM * G_WIDTH, LCD_ZOOM * G_HEIGHT);
   mPixmap->fill( QColor( RGB_WHITE));
   // initialize buffer :
   memset( &GBuffer, 0, sizeof( GBuffer));
   initializeBuffer();
   mLcdDisplay = this;
   QObject::connect(this, SIGNAL(repaintSignal()), this, SLOT(repaint()), Qt::QueuedConnection);
} // LcdDisplay

//-----------------------------------------------------------------------------
// Destructor
//-----------------------------------------------------------------------------

LcdDisplay::~LcdDisplay()
// Destructor
{
   if( mPixmap){
      delete mPixmap;
   }
} // ~LcdDisplay

//-----------------------------------------------------------------------------
// Initialize
//-----------------------------------------------------------------------------

void LcdDisplay::initializeBuffer()
// initialize buffer
{
   GBuffer.MinX = G_WIDTH;
   GBuffer.MaxX = 0;
   GBuffer.MinY = G_HEIGHT;
   GBuffer.MaxY = 0;
} // initializeBuffer

//-----------------------------------------------------------------------------
// Clear
//-----------------------------------------------------------------------------

void LcdDisplay::clearBuffer()
// Clear buffer
{
   memset( &GBuffer, 0, sizeof( GBuffer));
   initializeBuffer();
   mPixmap->fill( QColor( RGB_WHITE));
   initializeBuffer();
   emit repaintSignal();
} // clearBuffer

//-----------------------------------------------------------------------------
// Update
//-----------------------------------------------------------------------------

void LcdDisplay::updateBuffer()
// Update buffer
{
   QPainter painter( mPixmap);
   QRgb     color;
   for( int x = GBuffer.MinX; x <= GBuffer.MaxX; x++){
      for( int y = GBuffer.MinY; y <= GBuffer.MaxY; y++){
         int Yaddr = y / 8;
         int Ymask = 1 << (y % 8);
         // plane 0 :
         if( GBuffer.Buffer[ 0][ Yaddr][ x] & Ymask){
            // plane 1 :
            if( GBuffer.Buffer[ 1][ Yaddr][x] & Ymask){
               color = RGB_BLACK;
            } else {
               color = RGB_LIGHTGRAY;
            }
         } else {
            // plane 1 :
            if( GBuffer.Buffer[ 1][ Yaddr][x] & Ymask){
               color = RGB_DARKGRAY;
            } else {
               color = RGB_WHITE;
            }
         }
         painter.fillRect( x * LCD_ZOOM, y * LCD_ZOOM, LCD_ZOOM, LCD_ZOOM, QColor( color));
      }
   }
//   QRect area( LCD_ZOOM *  GBuffer.MinX, LCD_ZOOM * GBuffer.MinY,
//               LCD_ZOOM * (GBuffer.MaxX - GBuffer.MinX + 1), LCD_ZOOM * (GBuffer.MaxY - GBuffer.MinY + 1));
   initializeBuffer();
   emit repaintSignal();
//   qApp->postEvent(
//      this,
//      new QPaintEvent(area));
////   repaint( area);
} // updateBuffer

//-----------------------------------------------------------------------------
// Size hint
//-----------------------------------------------------------------------------

QSize LcdDisplay::sizeHint() const
// Returns requested size
{
   QSize size( LCD_ZOOM * G_WIDTH, LCD_ZOOM * G_HEIGHT);
   return( size);
} // sizeHint

//-----------------------------------------------------------------------------
// Copy
//-----------------------------------------------------------------------------

void LcdDisplay::copy()
// Copy display to clipboard
{
   QClipboard *clipboard = QApplication::clipboard();
   clipboard->setPixmap( *mPixmap);
} // copy

//*****************************************************************************

//-----------------------------------------------------------------------------
// Redraw
//-----------------------------------------------------------------------------
#include <QDebug>
void LcdDisplay::paintEvent( QPaintEvent *event)
// Redraw
{
   event->accept();
   QPainter painter(this);
   painter.drawPixmap(QPoint(0,0), *mPixmap, event->rect());
} // paintEvent
