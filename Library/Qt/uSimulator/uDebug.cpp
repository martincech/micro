//*****************************************************************************
//
//    uDebug.cpp    Debug trace utilities
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Debug/uDebug.h"
#include <QTextStream>
#include <stdio.h>
#include <stdarg.h>

//------------------------------------------------------------------------------
//  Printf
//------------------------------------------------------------------------------

void dbgPrintf( const char *Format, ...)
// Debug printf
{
va_list arg;
char    Buffer[ 1024];

   va_start( arg, Format);
   vsprintf( Buffer, Format, arg);
   va_end( arg);
   QTextStream(stderr) << Buffer;
   fflush(stderr);
} // dbgPrintf

//------------------------------------------------------------------------------
//  Putchar
//------------------------------------------------------------------------------

int dbgPutchar( int Ch)
// Debug putchar
{
   QTextStream(stderr) << (char) Ch;
   return Ch;
} // dbgPutchar
