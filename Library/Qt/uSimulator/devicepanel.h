//*****************************************************************************
//
//   DevicePanel.h   Device Simulator panel
//   Version 1.0     (c) VymOs
//
//*****************************************************************************

#ifndef DEVICEPANEL_H
#define DEVICEPANEL_H

#include <QWidget>
#include <QMutex>

namespace Ui {
    class DevicePanel;
}

//-----------------------------------------------------------------------------
// Device panel
//-----------------------------------------------------------------------------

class DevicePanel : public QWidget
{
   Q_OBJECT

public:
   explicit DevicePanel(QWidget *parent = 0);
   ~DevicePanel();

   int  scheduler();
   // Scheduler loop

   void timeoutReset();
   // Reset inactivity timeout

   void timeoutEnable();
   // Enable inactivity timeout

   void timeoutDisable();
   // Disable inactivity timeout

   void flashReset();
   // Restart flashing

   QSize sizeHint() const;
   // Widget size

public slots:
   void powerOnSwitch();
   // Activate scheduler loop

   void powerOffSwitch();
   // Stop scheduler loop

   void copy();
   // Copy display to clipboard

private slots :
   void powerStart();
   // Power on after application start

protected:
   void keyPressEvent( QKeyEvent *event);
   void keyReleaseEvent(QKeyEvent *event);

private slots:
   void on_buttonEnter_pressed();
   void on_buttonEnter_released();

   void on_buttonEsc_pressed();
   void on_buttonEsc_released();

   void on_button0_pressed();
   void on_button0_released();
   void on_button1_pressed();
   void on_button1_released();
   void on_button2_pressed();
   void on_button2_released();
   void on_button3_pressed();
   void on_button3_released();
   void on_button4_pressed();
   void on_button4_released();
   void on_button5_pressed();
   void on_button5_released();
   void on_button6_pressed();
   void on_button6_released();
   void on_button7_pressed();
   void on_button7_released();
   void on_button8_pressed();
   void on_button8_released();
   void on_button9_pressed();
   void on_button9_released();

   void on_powerOn_clicked();

   void timerFastTimeout();
   void timerSlowTimeout();
   void timerKeyboardTimeout();

   void on_pushButton_pressed();

   void stopTimers();
   void startTimers();

signals:
   void startKeyboardTimer(int);
   void stopKeyboardTimer();

   void startFastTimer(int);
   void stopFastTimer();

   void startSlowTimer(int);
   void stopSlowTimer();
private:
   void AppMainExecutive();
   Ui::DevicePanel *ui;

   QTimer *timerFast;
   QTimer *timerSlow;
   QTimer *timerKeyboard;
   QMutex *schedulerMutex;


   bool timerFastTick;
   bool timerSlowTick;
   bool timerKeyboardTick;

   int  counterTimeout;
   int  counterFlash;

   bool powerOn;
   int  currentKey;
   int  lastKey;
}; // DevicePanel

#endif // DEVICEPANEL_H
