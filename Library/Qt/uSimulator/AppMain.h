//******************************************************************************
//
//   AppMain.h    Application Main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef AppMainH
   #define AppMainH

#ifdef __cplusplus
   extern "C" {
#endif

void AppMain( void);

#ifdef __cplusplus
   }
#endif

#endif
