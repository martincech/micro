//*****************************************************************************
//
//   DevicePanel.cpp Device Simulator panel
//   Version 1.0     (c) VymOs
//
//*****************************************************************************

#include "devicepanel.h"
#include "ui_devicepanel.h"
#include <QKeyEvent>
#include <QTimer>
#include "uSimulator/AppMain.h"
#include "uSimulator/lcddisplay.h"
#include "Graphic/Graphic.h"
#include "Console/conio.h"
#include "Utility/Performance.h"
#include "System/System.h"
#include "Time/uClock.h"
#include "Fonts.h"
#include "Debug/uDebug.h"
#include <QThread>
#include <QtConcurrentRun>

#define START_DELAY   300      // power on start delay

#define PANEL_WIDTH   500
#define PANEL_HEIGHT  630

// timer settings :
#define timerTimeoutSet()       counterTimeout  = (TIMER_TIMEOUT * 1000) / TIMER_SLOW_PERIOD
#define timerTimeoutClear()     counterTimeout  = 0
#define timerFlashSet()         counterFlash    = (TIMER_FLASH1 + TIMER_FLASH2) / TIMER_FAST_PERIOD
#define timerFlash1()           (counterFlash == TIMER_FLASH1 / TIMER_FAST_PERIOD)

static DevicePanel *mDevicePanel;

//-----------------------------------------------------------------------------
// Constructor
//-----------------------------------------------------------------------------

DevicePanel::DevicePanel(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DevicePanel)
{
   ui->setupUi(this);
   setFocus();
   setAttribute( Qt::WA_StaticContents);
   setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed);
   resize( PANEL_WIDTH, PANEL_HEIGHT);

   // create timers :
   timerFast     = new QTimer( this);
   timerSlow     = new QTimer( this);
   timerKeyboard = new QTimer( this);
   connect( timerFast,     SIGNAL( timeout()), this, SLOT( timerFastTimeout()), Qt::QueuedConnection);
   connect( timerSlow,     SIGNAL( timeout()), this, SLOT( timerSlowTimeout()), Qt::QueuedConnection);
   connect( timerKeyboard, SIGNAL( timeout()), this, SLOT( timerKeyboardTimeout()), Qt::QueuedConnection);

   connect(this, SIGNAL(startKeyboardTimer(int)), timerKeyboard, SLOT(start(int)), Qt::QueuedConnection);
   connect(this, SIGNAL(stopKeyboardTimer()), timerKeyboard, SLOT(stop()), Qt::QueuedConnection);

   connect(this, SIGNAL(startFastTimer(int)), timerFast, SLOT(start(int)), Qt::QueuedConnection);
   connect(this, SIGNAL(stopFastTimer()), timerFast, SLOT(stop()), Qt::QueuedConnection);

   connect(this, SIGNAL(startSlowTimer(int)), timerSlow, SLOT(start(int)), Qt::QueuedConnection);
   connect(this, SIGNAL(stopSlowTimer()), timerSlow, SLOT(stop()), Qt::QueuedConnection);

   //  prepare LCD display :
   ui->lcdDisplay->clearBuffer();
   // intialize private data :
   counterTimeout    = 0;
   counterFlash      = 0;
   timerFastTick     = false;
   timerSlowTick     = false;
   timerKeyboardTick = false;
   powerOn           = false;
   currentKey        = K_RELEASED;
   lastKey           = K_RELEASED;
   mDevicePanel      = this;
   schedulerMutex    = new QMutex();
   QTimer::singleShot( START_DELAY, this, SLOT( powerStart()));
} // DevicePanel

//-----------------------------------------------------------------------------
// Destructor
//-----------------------------------------------------------------------------

DevicePanel::~DevicePanel()
{
   delete schedulerMutex;
   delete ui;
} // ~DevicePanel

//-----------------------------------------------------------------------------
// Size hint
//-----------------------------------------------------------------------------

QSize DevicePanel::sizeHint() const
// Returns requested size
{
   QSize size( PANEL_WIDTH, PANEL_HEIGHT);
   return( size);
} // sizeHint

//-----------------------------------------------------------------------------
// Key press
//-----------------------------------------------------------------------------

void DevicePanel::keyPressEvent( QKeyEvent *event)
// Check for keyboard pressed
{
   if( event->isAutoRepeat()){
      QWidget::keyPressEvent( event);
      return;
   }
   switch( event->key()){
      case Qt::Key_Return :
      case Qt::Key_Enter :
         on_buttonEnter_pressed();
         break;
      case Qt::Key_Escape :
         on_buttonEsc_pressed();
         break;
      case Qt::Key_4 :
      case Qt::Key_Left :
         on_button4_pressed();
         break;
      case Qt::Key_6 :
      case Qt::Key_Right :
         on_button6_pressed();
         break;
      case Qt::Key_2 :
      case Qt::Key_Up :
         on_button2_pressed();
         break;
      case Qt::Key_8 :
      case Qt::Key_Down :
         on_button8_pressed();
         break;
      case Qt::Key_0 :
         on_button0_pressed();
         break;
      case Qt::Key_1 :
         on_button1_pressed();
         break;
      case Qt::Key_3 :
         on_button3_pressed();
         break;
      case Qt::Key_5 :
         on_button5_pressed();
         break;
      case Qt::Key_Delete:
      case Qt::Key_7 :
         on_button7_pressed();
         break;
      case Qt::Key_Backspace:
      case Qt::Key_9 :
         on_button9_pressed();
         break;
      default :
         QWidget::keyPressEvent( event);
         break;
   }
} // keyPressEvent

//-----------------------------------------------------------------------------
// Key release
//-----------------------------------------------------------------------------

void DevicePanel::keyReleaseEvent(QKeyEvent *event)
// Check for keyboard released
{
   if( event->isAutoRepeat()){
      QWidget::keyReleaseEvent( event);
      return;
   }
   switch( event->key()){
      case Qt::Key_Return :
         on_buttonEnter_released();
         break;
      case Qt::Key_Escape :
         on_buttonEsc_released();
         break;
      case Qt::Key_4 :
      case Qt::Key_Left :
         on_button4_released();
         break;
      case Qt::Key_6 :
      case Qt::Key_Right :
         on_button6_released();
         break;
      case Qt::Key_2 :
      case Qt::Key_Up :
         on_button2_released();
         break;
      case Qt::Key_8 :
      case Qt::Key_Down :
         on_button8_released();
         break;
      case Qt::Key_0 :
         on_button0_released();
         break;
      case Qt::Key_1 :
         on_button1_released();
         break;
      case Qt::Key_3 :
         on_button3_released();
         break;
      case Qt::Key_5 :
         on_button5_released();
         break;
      case Qt::Key_7 :
         on_button7_released();
         break;
      case Qt::Key_9 :
         on_button9_released();
         break;
      default :
         QWidget::keyReleaseEvent( event);
         break;
   }
} // keyReleaseEvent

//-----------------------------------------------------------------------------
// Enter
//-----------------------------------------------------------------------------

void DevicePanel::on_buttonEnter_pressed()
{
   TRACE( "Enter pressed");
   currentKey = K_ENTER;
} // on_buttonEnter_pressed

void DevicePanel::on_buttonEnter_released()
{
   TRACE( "Enter released");
   currentKey = K_RELEASED;
} // on_buttonEnter_released

//-----------------------------------------------------------------------------
// Esc
//-----------------------------------------------------------------------------

void DevicePanel::on_buttonEsc_pressed()
{
   TRACE( "Esc pressed");
   currentKey = K_ESC;
} // on_buttonEsc_pressed


void DevicePanel::on_buttonEsc_released()
{
   TRACE( "Esc released");
   currentKey = K_RELEASED;
} // on_buttonEsc_released

//-----------------------------------------------------------------------------
// 0
//-----------------------------------------------------------------------------
void DevicePanel::on_button0_pressed()
{
   currentKey = K_0;
}
void DevicePanel::on_button0_released()
{
   currentKey = K_RELEASED;
}

//-----------------------------------------------------------------------------
// 1
//-----------------------------------------------------------------------------
void DevicePanel::on_button1_pressed()
{
   currentKey = K_1;
}
void DevicePanel::on_button1_released()
{
   currentKey = K_RELEASED;
}

//-----------------------------------------------------------------------------
// 2
//-----------------------------------------------------------------------------

void DevicePanel::on_button2_pressed()
{
   TRACE( "2 pressed");
   currentKey = K_2;
} // on_buttonUp_pressed

void DevicePanel::on_button2_released()
{
   TRACE( "2 released");
   currentKey = K_RELEASED;
} // on_buttonUp_released

//-----------------------------------------------------------------------------
// 3
//-----------------------------------------------------------------------------

void DevicePanel::on_button3_pressed()
{
   currentKey = K_3;
} // on_buttonUp_pressed

void DevicePanel::on_button3_released()
{
   currentKey = K_RELEASED;
} // on_buttonUp_released

//-----------------------------------------------------------------------------
// 4
//-----------------------------------------------------------------------------

void DevicePanel::on_button4_pressed()
{
   TRACE( "4 pressed");
   currentKey = K_4;
} // on_buttonLeft_pressed

void DevicePanel::on_button4_released()
{
   TRACE( "4 released");
   currentKey = K_RELEASED;
} // on_buttonLeft_released

//-----------------------------------------------------------------------------
// 5
//-----------------------------------------------------------------------------

void DevicePanel::on_button5_pressed()
{
   currentKey = K_5;
} // on_buttonUp_pressed

void DevicePanel::on_button5_released()
{
   currentKey = K_RELEASED;
} // on_buttonUp_released


//-----------------------------------------------------------------------------
// Right
//-----------------------------------------------------------------------------

void DevicePanel::on_button6_pressed()
{
   TRACE( "6 pressed");
   currentKey = K_6;
} // on_buttonRight_pressed

void DevicePanel::on_button6_released()
{
   TRACE( "6 released");
   currentKey = K_RELEASED;
} // on_buttonRight_released

//-----------------------------------------------------------------------------
// 7
//-----------------------------------------------------------------------------

void DevicePanel::on_button7_pressed()
{
   currentKey = K_7;
} // on_buttonUp_pressed

void DevicePanel::on_button7_released()
{
   currentKey = K_RELEASED;
} // on_buttonUp_released

//-----------------------------------------------------------------------------
// Down
//-----------------------------------------------------------------------------

void DevicePanel::on_button8_pressed()
{
   TRACE( "8 pressed");
   currentKey = K_8;
} // on_buttonDown_pressed

void DevicePanel::on_button8_released()
{
   TRACE( "8 relased");
   currentKey = K_RELEASED;
} // on_buttonDown_released

//-----------------------------------------------------------------------------
// 9
//-----------------------------------------------------------------------------

void DevicePanel::on_button9_pressed()
{
   currentKey = K_9;
} // on_buttonUp_pressed

void DevicePanel::on_button9_released()
{
   currentKey = K_RELEASED;
} // on_buttonUp_released

#include "Weighing/WeighingConfiguration.h"
#include "Scheduler/WeighingScheduler.h"
#include <QMessageBox>
void DevicePanel::on_pushButton_pressed()
{
   if( WeighingStatus() != WEIGHING_STATUS_WEIGHING){
       return;
   }
   if( QMessageBox::question(this, "Confirm next day", "Realy switch to next day", QMessageBox::Ok, QMessageBox::Cancel) != QMessageBox::Ok){
      return;
   }
   WeighingSchedulerDayNext();
}

//-----------------------------------------------------------------------------
// Power On
//-----------------------------------------------------------------------------

void DevicePanel::on_powerOn_clicked()
{
   if( ui->powerOn->isChecked()){
      powerOnSwitch();
   } else {
      powerOffSwitch();
   }
} // on_powerOn_clicked

//-----------------------------------------------------------------------------
// Timers
//-----------------------------------------------------------------------------
#include <QTime>
void DevicePanel::timerFastTimeout()
{
   schedulerMutex->lock();
//   TRACES("Fast timer", QTime::currentTime().toString("hh:mm:ss:zzz").toStdString().c_str())
   timerFastTick = true;
   schedulerMutex->unlock();
} // timerFastTimeut

void DevicePanel::timerSlowTimeout()
{
   schedulerMutex->lock();
//   TRACES("Slow timer", QTime::currentTime().toString("hh:mm:ss:zzz").toStdString().c_str())
   timerSlowTick = true;
   schedulerMutex->unlock();
} // timerSlowTimeut

void DevicePanel::timerKeyboardTimeout()
{
   schedulerMutex->lock();
//   TRACES("Keyboard timer", QTime::currentTime().toString("hh:mm:ss:zzz").toStdString().c_str())
   timerKeyboardTick = true;
   schedulerMutex->unlock();
} // timerKeyboardTimeut

//-----------------------------------------------------------------------------
// Scheduler
//-----------------------------------------------------------------------------

int DevicePanel::scheduler()
{
int retVal;

   // process Windows messages :
   if( !powerOn){
      return K_SHUTDOWN;             // terminate scheduler loop
   }
   qApp->processEvents();
   schedulerMutex->lock();
//   TRACE("Scheduler");
   do{
      // check for power off :
      if( !powerOn){
         retVal = K_SHUTDOWN;             // terminate scheduler loop
         break;
      }
      // check for key pressed :
      if( currentKey != K_RELEASED && lastKey == K_RELEASED){
         lastKey           = currentKey;
         timerKeyboardTick = false;
         emit startKeyboardTimer(KBD_AUTOREPEAT_START);
         timeoutReset();
         retVal = lastKey;
         break;
      }
      // check for key released :
      if( currentKey == K_RELEASED && lastKey != K_RELEASED){
         int key = lastKey;
         lastKey = K_RELEASED;
         emit stopKeyboardTimer();
         timeoutReset();
         retVal = key | K_RELEASED;
         break;
      }
      // check for key autorepeat :
      if( lastKey != K_RELEASED && timerKeyboardTick){
         timerKeyboardTick = false;
         emit startKeyboardTimer( KBD_AUTOREPEAT_SPEED);
         timeoutReset();
         retVal =  lastKey | K_REPEAT;
         break;
      }

      // check for slow timer :
      if( timerSlowTick){
          timerSlowTick = false;
         if( counterTimeout){
            if( !(--counterTimeout)){
               timerTimeoutSet();         // count again
               retVal =  K_TIMEOUT;        // inactivity timeout occured
               break;
            }
         }
         retVal =  K_TIMER_SLOW;
         break;
      }
      // check for fast timer :
      if( timerFastTick){
         timerFastTick = false;
         counterFlash--;
         if( timerFlash1()){
//            TRACE("Flash1 timer");
            retVal =  K_FLASH1;            // first limit reached
            break;
         }
         if( !counterFlash){
//            TRACE("Flash2 timer");
            timerFlashSet();              // count again
            retVal = K_FLASH2;            // flash period reached
            break;
         }
         retVal = K_TIMER_FAST;
         break;
      }
      retVal = K_IDLE;
   }while(0);

   schedulerMutex->unlock();
   return( retVal);
} // scheduler

//-----------------------------------------------------------------------------
// Timeout
//-----------------------------------------------------------------------------

void DevicePanel::timeoutReset()
{
   if( !counterTimeout){
      return;                          // timer stopped
   }
   timerTimeoutSet();                  // restart countdown
} // timeoutReset

void DevicePanel::timeoutEnable()
{
   timerTimeoutSet();                  // start countdown
} // timeoutEnable

void DevicePanel::timeoutDisable()
{
   timerTimeoutClear();                // stop countdown
} // timeoutDisable

void DevicePanel::flashReset()
{
   timerFlashSet();                    // restart countdown
} // flashReset

//-----------------------------------------------------------------------------
// Power
//-----------------------------------------------------------------------------
#include "Menu/MenuExit.h"
void DevicePanel::AppMainExecutive(){
   AppMain();
   MenuExitSet(0);
   TRACE( "Power Off");
   stopTimers();
}

void DevicePanel::powerOnSwitch()
{
   if( powerOn){
      return;                          // already on
   }
   TRACE( "Power On");
   powerOn = true;                     // switch power on
   // start timers :
   startTimers();
   // run device main :
   QtConcurrent::run(this, &DevicePanel::AppMainExecutive);
} // powerOnSwitch

void DevicePanel::startTimers()
{
   timerTimeoutSet();
   timerFlashSet();
   emit startFastTimer( TIMER_FAST_PERIOD);
   emit startSlowTimer( TIMER_SLOW_PERIOD);
   emit stopKeyboardTimer();
   timerFastTick     = false;
   timerSlowTick     = false;
   timerKeyboardTick = false;
}

void DevicePanel::stopTimers()
{
   // stop timers :
   emit stopFastTimer();
   emit stopSlowTimer();
   emit stopKeyboardTimer();
}

void DevicePanel::powerOffSwitch()
{
   if( !powerOn){
      return;                          // already off
   }
   TRACE( "Power Off clicked");
   powerOn = false;                    // switch power off only
   MenuExitSet(MENU_EXIT_POWER_OFF);
} // powerOffSwitch

//------------------------------------------------------------------------------
//   Power start
//------------------------------------------------------------------------------


void DevicePanel::powerStart()
// Power on after application start
{
   ui->powerOn->setChecked( true);
   on_powerOn_clicked();
} // powerStart

//-----------------------------------------------------------------------------
// Copy
//-----------------------------------------------------------------------------

void DevicePanel::copy()
// Copy display to clipboard
{
   ui->lcdDisplay->copy();
} // copy

//*****************************************************************************
// C - access functions
//*****************************************************************************

//-----------------------------------------------------------------------------
// Initialize
//-----------------------------------------------------------------------------

void SysInit( void)
// Initialisation
{
} // SysInit

//-----------------------------------------------------------------------------
// Scheduler
//-----------------------------------------------------------------------------

int SysSchedulerDefault( void)
// Scheduler cycle, returns any event
{
   return( mDevicePanel->scheduler());
} // SysSchedulerDefault

void SysEnable( TYesNo Enable) {

}

//-----------------------------------------------------------------------------
// Wait event
//-----------------------------------------------------------------------------

int SysEventWait( void)
// Scheduler loop, returns nonempty event
{
int Key;

   forever {
      Key = SysScheduler();            // scheduler step
      if( Key != K_IDLE) {
          return( Key);                // nonempty event
      }
   }
} // SysEventWait

//------------------------------------------------------------------------------
//   Timeout
//------------------------------------------------------------------------------

void SysTimeoutDisable( void)
// Disable timeout
{
   mDevicePanel->timeoutDisable();
} // SysTimeoutDisable

void SysTimeoutEnable( void)
// Enable timeout
{
   mDevicePanel->timeoutEnable();
} // SysTimeoutEnable

void SysTimeoutReset( void)
// Reset timeout
{
   mDevicePanel->timeoutReset();
} // SysTimeoutReset

void SysFlashReset( void)
// Restart flashing
{
   mDevicePanel->flashReset();
} // SysFlashReset

//-----------------------------------------------------------------------------
// Clock & Delay
//-----------------------------------------------------------------------------

//#include "System/SysClock.c"

#include "Rtc/Rtc.h"

void RtcInit( void) {}

//-----------------------------------------------------------------------------
// Kbd
//-----------------------------------------------------------------------------

#include "Kbd/Kbd.h"

void KbdInit( void) {}
