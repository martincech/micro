//*****************************************************************************
//
//    Power.cpp    Power management
//    Version 1.2  (c) VEIT Electronics
//
//*****************************************************************************

#include "Power/Power.h"
#include "System/System.h"
#include "Memory/Nvm.h"
#include <QApplication>
#include <stdlib.h>
dword  i;
TYesNo Failure;

//#define SIMULATE_POWER_FAILURE
#define TIMEOUT 10

//------------------------------------------------------------------------------
//  Initialization
//------------------------------------------------------------------------------

void PowerInit( void)
// Initialize
{
   i = SysClock() + TIMEOUT;
   Failure = NO;
} // PowerInit

//------------------------------------------------------------------------------
//  Execute
//------------------------------------------------------------------------------

void PowerExecute( void)
// Task
{

} // PowerExecute

//------------------------------------------------------------------------------
//  Failure
//------------------------------------------------------------------------------

TYesNo PowerFailure( void)
// Check for power failure
{
#ifdef SIMULATE_POWER_FAILURE
   if( SysClock() > i){
      i = SysClock() + TIMEOUT;
      Failure = !Failure;
      return( Failure);
   }
   return( Failure);
#else
   return NO;
#endif
} // PowerFailure

//------------------------------------------------------------------------------
//  External
//------------------------------------------------------------------------------

TYesNo PowerExternal( void)
// External
{
   return NO;
} // PowerExternal

//------------------------------------------------------------------------------
//  Charger status
//------------------------------------------------------------------------------

byte PowerChargerStatus( void)
// Charger status
{
   return 0;
} // PowerChargerStatus

//------------------------------------------------------------------------------
//  Power for GSM
//------------------------------------------------------------------------------

TYesNo PowerEnoughForGsm( void)
// Query enough power for GSM
{
   return YES;
} // PowerEnoughForGsm

//------------------------------------------------------------------------------
//  Rs485 power
//------------------------------------------------------------------------------

TYesNo PowerRs485( void)
// Rs485 power present
{
   return NO;
} // PowerRs485

//------------------------------------------------------------------------------
//  Accu present
//------------------------------------------------------------------------------

TYesNo PowerAccuPresent( void)
// Accu present
{
   return NO;
} // PowerAccuPresent

//------------------------------------------------------------------------------
//  Shutdown
//------------------------------------------------------------------------------

void PowerShutdown( void)
// Shutdown
{
} // PowerShutdown

//------------------------------------------------------------------------------
//  ESD
//------------------------------------------------------------------------------

TYesNo PowerEsdEvent( void)
// Esd event
{
   return NO;
} // PowerEsdEvent

//------------------------------------------------------------------------------
//  User switched
//------------------------------------------------------------------------------

TYesNo PowerSwitchedOnByUser( void)
// User switched
{
   return YES;
} // PowerSwitchedOnByUser

//------------------------------------------------------------------------------
//  Power off
//------------------------------------------------------------------------------

void PowerOff( void)
// Safely power off
{
//   qApp->exit(1);
}
