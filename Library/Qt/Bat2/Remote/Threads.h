#ifndef THREADS_H
#define THREADS_H

#include <QThread>
#include <QDateTime>
#include <QString>
#include <string.h>
#include "Gsm/Gsm.h"
#include "Gsm/GsmDefPrv.h"
class SleepThread : public QThread
{
    Q_OBJECT
public:
    SleepThread(QObject *parent) :  QThread(parent) {}
    static void  sleep(unsigned long secs) { QThread::sleep(secs);}
};

class Thread: public QObject
{
    Q_OBJECT
public slots:
   virtual void doWork() = 0;
   void stop();

signals:
   void done();
   void progressRange(int from, int to);
   void progress(int i);

protected:
    void sleep(int secs);
    void finish();

    bool terminate;
};

class InitThread: public Thread
{
   Q_OBJECT
public:
   InitThread();
   QString getOperator(){ return QString(operatorName);}
   int getSignalStrength(){return signalStrength;}
   bool getRegistered(){ return registered;}
   bool getSmsServiceReady(){return smsInited;}
public slots:
   void doWork();

private:
   char operatorName[255];
   int  signalStrength;
   bool registered;
   bool smsInited;
};

class SmsSendThread: public Thread
{
   Q_OBJECT
public:
   SmsSendThread();
   void setText(QString text){
      strncpy( smsText, text.toStdString().c_str(), 160);
      smsText[160] = '\0';
   }
   void setPhone(QString text){
      strncpy( phoneNumber, text.toStdString().c_str(), 20);
      phoneNumber[20] = '\0';
   }

public slots:
   void doWork();

private:
   char smsText[161];
   char phoneNumber[20];
};

class SmsReadThread: public Thread
{
   Q_OBJECT
public:
   SmsReadThread();
   QString getText(){ return QString(smsText);}
   QString getPhone(){ return QString(phoneNumber);}
   QDateTime getTimeStamp(){ return timestamp;}
   TSmsReadStatus getStatus(){return status;}
public slots:
   void doWork();

private:
   char smsText[161];
   char phoneNumber[20];
   QDateTime timestamp;
   TSmsReadStatus status;
};

class StatusReadThread: public Thread
{
   Q_OBJECT
public:
   StatusReadThread();
   QString getText(){ return QString(smsText);}
   QString getPhone(){ return QString(phoneNumber);}
   QDateTime getTimeStamp(){ return timestamp;}
   QString getStatus();
   int getMessageReference(){ return mr;}

public slots:
   void doWork();

private:
   QDateTime timestamp;
   ESmsStatus status;
   char phoneNumber[20];
   char smsText[161];
   int mr;
};

class CommandSendThread: public Thread
{
   Q_OBJECT
public:
   CommandSendThread();
   void setPhone(QString text){
      strncpy( phoneNumber, text.toStdString().c_str(), 20);
      phoneNumber[20] = '\0';
   }

   void setMessageReference(int mr){ this->mr = mr;}
   void setCommandType(ESmsCommandType ct){this->ct = ct;}

public slots:
   void doWork();

private:
   char phoneNumber[20];
   int mr;
   ESmsCommandType ct;
};

#include "Model/arraymodel.h"

//executive with data read from channel
class SmsChannelExecutive: public Thread
{
   Q_OBJECT
public:
   SmsChannelExecutive();
   ~SmsChannelExecutive();

   bool RegisterStatusCheck(TSmsChannel chan);
   QAbstractItemModel *getOutputChannelModel(){return OutChannelsModel;}
   QAbstractItemModel *getInputChannelModel(){return IncChannelsModel;}
   QAbstractItemModel *getChannelsStatusModel(){return ChannelsModel;}
public slots:
   void doWork();

private:
   QList<TSmsChannel>            channels;
   QList<bool>                   channelsAlive;
   QList<TSmsChannelSendStatus>  channelsStatus;
   QList<TSmsChannel>            OChannels;
   ArrayModel            *OutChannelsModel;
   ArrayModel            *IncChannelsModel;
   ArrayModel            *ChannelsModel;
};

// just executive without read and writes
class SmsChannelSimpleExecutive: public Thread
{
   Q_OBJECT
public:
   SmsChannelSimpleExecutive();
   ~SmsChannelSimpleExecutive();

   QAbstractItemModel *getChannelsModel(){return ChannelsModel;}
public slots:
   void doWork();

private:
   ArrayModel            *ChannelsModel;
};

#endif // THREADS_H
