#include "Bat2SmsRemote.h"
#include "Gsm/Gsm.h"
#include "Uart/UartModem.h"
#include <QApplication>
#include <QThreadPool>

#define LoggerTx( frame, size)       if( _logger) _logger->show( CrtDump::TX, frame, size)
#define LoggerRx( frame, size)       if( _logger) _logger->show( CrtDump::RX, frame, size)
#define LoggerGarbage( frame, size)  if( _logger) _logger->show( CrtDump::GARBAGE, frame, size)
#define LoggerReport( txt, ...)      if( _logger) _logger->printf( txt, ##__VA_ARGS__)

//------------------------------------------------------------------------------
//  Constructors
//------------------------------------------------------------------------------

Bat2SmsRemote::Bat2SmsRemote(char *Phone, CrtDump *log, QObject *parent) :
   QObject(parent),
   TBat2RemoteInterface(),
   WinUart()
{

   this->_logger = log;
   this->smsChannelThread = NULL;
   workerThread = new QThread();
   UartModemSetup( this, _logger);
   strcpy(this->phone, Phone);
   GsmInit();
   init = new InitThread();
   init->moveToThread(workerThread);
   QObject::connect(this, SIGNAL(start()), init, SLOT(doWork()));
   QObject::connect(init, SIGNAL(done()), this, SLOT(initDone()));
   workerThread->start();
   while(!workerThread->isRunning());
   emit start();
}

Bat2SmsRemote::Bat2SmsRemote(char *Phone, QObject *parent)
{
   Bat2SmsRemote(Phone, parent);
}



bool Bat2SmsRemote::connected(){
   return checkOpen();
}
bool Bat2SmsRemote::inited(){
   return smsChannelThread != NULL;
}

QAbstractItemModel *Bat2SmsRemote::channels()
{
   if( smsChannelThread){
      return smsChannelThread->getChannelsModel();
   }
   return NULL;
}

//------------------------------------------------------------------------------
//  Destructor
//------------------------------------------------------------------------------

Bat2SmsRemote::~Bat2SmsRemote()
// destructor
{
   if( init){
      init->stop();
      init->deleteLater();
      init = NULL;
   }
   if( smsChannelThread){
      smsChannelThread->stop();
      smsChannelThread->deleteLater();
      smsChannelThread = NULL;
   }
   while( workerThread->isRunning()){
      qApp->processEvents();
      QThread::currentThread()->yieldCurrentThread();
   }
   QThreadPool::globalInstance()->waitForDone();
   workerThread->deleteLater();
   GsmDeinit();
   WinUart::disconnect();
}

//------------------------------------------------------------------------------
//  init done
//------------------------------------------------------------------------------
void Bat2SmsRemote::initDone()
{
   while( workerThread->isRunning());
   delete init; init = NULL;
   smsChannelThread = new SmsChannelSimpleExecutive();
   smsChannelThread->moveToThread(workerThread);
   QObject::connect(workerThread, SIGNAL(started()), smsChannelThread, SLOT(doWork()));
   workerThread->start();
}

//******************************************************************************
// Send command and receive reply
//******************************************************************************
SendAndReceiveEx::SendAndReceiveEx( Bat2SmsRemote *master)
{
   this->master = master;
   this->_logger = master->_logger;
   this->setAutoDelete(true);
}

void SendAndReceiveEx::run()
{
   if( !master->connected()){
      LoggerReport("Unconnected - set parameters first.");
      master->result = NO; return;
   }
   if( !master->workerThread->isRunning()){
      LoggerReport("Modem problem!");
      master->result = NO; return;
   }

   if(!master->SendCommand( master->Cmd, master->CmdSize)) {
      LoggerReport("Send command problem!\n");
      master->result = NO; return;
   }
   if(!master->ReceiveReply( master->Reply, master->ReplySize)) {
      LoggerReport("Receive reply problem!\n");
      master->result = NO; return;
   }

   if(master->Reply->Reply != RcReply(master->Cmd->Cmd, YES)) {
      LoggerReport("Reply error received!\n");
      master->result = NO; return;
   }
   if(master->DesiredReplySize && *master->ReplySize != master->DesiredReplySize) {
      LoggerReport("Wrong reply size received!\n");
      master->result = NO; return;
   }
   master->result = YES; return;
}

TYesNo Bat2SmsRemote::SendCommandAndReceiveReply( TRcCmd *Cmd, int CmdSize, TRcReply *Reply, int *ReplySize, int DesiredReplySize)
// Send command and receive reply
{

   this->Cmd = Cmd;
   this->CmdSize = CmdSize;
   this->Reply = Reply;
   this->ReplySize = ReplySize;
   this->DesiredReplySize = DesiredReplySize;
   QThreadPool::globalInstance()->start(new SendAndReceiveEx(this));
   while( QThreadPool::globalInstance()->activeThreadCount() > 0){
      qApp->processEvents();
   }
   return this->result;
}

#include "Gsm/Gsm.h"
SendCommandEx::SendCommandEx(Bat2SmsRemote *master)
{
   this->master = master;
   this->setAutoDelete(true);
}

void SendCommandEx::run()
{
TSmsChannelSendStatus status;
TSmsChannel channel;

   channel = GsmSmsChannelSend(master->phone, (byte*)master->Cmd, master->CmdSize);
   if( channel == SMS_INVALID_CHANNEL){
      master->result = NO; return;
   }
   forever{
      status = GsmSmsChannelSendStatus( channel);
      if( !master->workerThread->isRunning()){
         GsmSmsChannelClose( channel);
         master->result = NO; return;
      }
      switch( status){
         case GSM_SMS_SEND_OK:
            master->result = YES; return;

         case GSM_SMS_SEND_ERROR:
         case GSM_SMS_SEND_ERROR_INTERNAL:
         case GSM_SMS_SEND_TIMEOUT:
         case GSM_SMS_SEND_STOPPED:
         case GSM_SMS_SEND_WRONG_CHANNEL_DESC:
            master->result = NO; return;

         default:
            QThread::currentThread()->yieldCurrentThread();
            break;
      }
   }
}

TYesNo Bat2SmsRemote::SendCommand( TRcCmd *Cmd, int CmdSize)
// Send command
{
   this->Cmd = Cmd;
   this->CmdSize = CmdSize;
   QThreadPool::globalInstance()->start(new SendCommandEx(this));
   while( QThreadPool::globalInstance()->activeThreadCount() > 1){
      QThread::yieldCurrentThread();
   }
   return this->result;
}

ReceiveReplyEx::ReceiveReplyEx( Bat2SmsRemote *master)
{
   this->master = master;
   this->setAutoDelete(true);
}
#include "Timer/Timer.h"
void ReceiveReplyEx::run()
{
char pn[20];
int  dataSize;
TSmsChannel channel;

   timer = SysClock() + 8*TIME_MIN;
   forever{
      if( !master->workerThread->isRunning() || TimeAfter(SysClock(), timer)){
         master->result = NO; return;
      }
      QThread::currentThread()->yieldCurrentThread();

      channel = GsmSmsChannelIncomming(pn, &dataSize);
      if( channel == SMS_INVALID_CHANNEL){
         continue;
      }
      if( !strequ(pn, master->phone)){
         GsmSmsChannelClose(channel);
         continue;
      }
      QByteArray ba;
      ba.resize(dataSize);
      GsmSmsChannelRead( channel, (byte *)ba.data(), dataSize);
      memcpy(master->Reply, ba.data(), (unsigned)dataSize > sizeof(TRcReply)? sizeof(TRcReply): dataSize);
      *master->ReplySize = dataSize;
      master->result = YES; return;
   }
}

TYesNo Bat2SmsRemote::ReceiveReply( TRcReply *Reply, int *ReplySize)
// receive reply
{
   this->Reply = Reply;
   this->ReplySize = ReplySize;
   QThreadPool::globalInstance()->start(new ReceiveReplyEx(this));
   while( QThreadPool::globalInstance()->activeThreadCount() > 1){
      QThread::yieldCurrentThread();
   }
   return this->result;
}
