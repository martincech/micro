//******************************************************************************
//
//   CrtDump.cpp  CRT based data dump
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include "crtdump.h"
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>

#define BUFFER_SIZE  265

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------

CrtDump::CrtDump( Crt *crt)
{
   _crt          = crt;
   _dumpMode     = BINARY;
   _enable       = true;
   _enableOffset = true;
   _enableHeader = true;
} // CrtDump

//------------------------------------------------------------------------------
//  Mode
//------------------------------------------------------------------------------

void CrtDump::setMode( DumpMode mode)
{
   _dumpMode = mode;
} // setMode

//------------------------------------------------------------------------------
//  Enable
//------------------------------------------------------------------------------

void CrtDump::setEnable( bool enable)
{
   _enable = enable;
} // setEnable

//------------------------------------------------------------------------------
//  Enable offset
//------------------------------------------------------------------------------

void CrtDump::setEnableOffset( bool enable)
{
   _enableOffset = enable;
} // setEnableOffset

//------------------------------------------------------------------------------
//  Enable header offset
//------------------------------------------------------------------------------
void CrtDump::setEnableHeader( bool enableHeader)
// enable/disable rx/tx text
{
   _enableHeader = enableHeader;
}

//------------------------------------------------------------------------------
//  puts
//------------------------------------------------------------------------------

void CrtDump::puts( const char *string)
{
   if( !_enable){
      return;                          // disable dump
   }
   _crt->puts( string);
} // puts

//------------------------------------------------------------------------------
//  printf
//------------------------------------------------------------------------------

void CrtDump::printf( const char *format, ...)
{
va_list arg;

   if( !_enable){
      return;                          // disable dump
   }
   va_start( arg, format);
   _crt->vprintf( format, arg);
   va_end( arg);
} // printf

//------------------------------------------------------------------------------
//  Write
//------------------------------------------------------------------------------

void CrtDump::show( DumpType type, const void *data, const int size)
{
   if( !_enable){
      return;                          // disable dump
   }
   // print header :
   if( _enableHeader){
      switch( type){
         case NO_TYPE :
            break;
         case RX :
            _crt->puts( "RX :\n");
            break;
         case TX :
            _crt->puts( "TX :\n");
            break;
         case GARBAGE :
            _crt->puts( "RX GARBAGE:\n");
            break;
         default :
            return;
      }
   } else if (type == RX){
      _crt->puts("  ");
   }
   // print data :
   switch( _dumpMode){
      case BINARY :
         binary( (const byte *)data, size);
         break;

      case TEXT :
         text( (const byte *)data, size);
         break;

      case MIXED :
         mixed( (const byte *)data, size);
         break;

      default :
         return;
   }
} // show

//------------------------------------------------------------------------------
//  Binary dump
//------------------------------------------------------------------------------

void CrtDump::binary( const void *data, int size)
{
   if( !_enable){
      return;                          // disable dump
   }
   int columns;
   int rows      = DivCover( size, _crt->width()/_crt->fontMetrics().maxWidth());
   const byte *p = (const byte *)data;
   for( int row = 0; row < rows; row++){
      columns = size;
      if( columns > _crt->width()/_crt->fontMetrics().maxWidth()){
         columns = _crt->width()/_crt->fontMetrics().maxWidth();
      }
      if( _enableOffset){
         _crt->printf( "%04X: ", row * _crt->width()/_crt->fontMetrics().maxWidth());
      }
      binaryLine( p, columns);
      p    += columns;
      size -= columns;
      _crt->putch( '\n');
   }
} // binary

//------------------------------------------------------------------------------
//  Text dump
//------------------------------------------------------------------------------

void CrtDump::text( const void *data, int size)
{
   if( !_enable){
      return;                          // disable dump
   }
   int columns;
   int rows      = DivCover( size, _crt->width()/_crt->fontMetrics().maxWidth());
   const byte *p = (const byte *)data;
   for( int row = 0; row < rows; row++){
      columns = size;
      if( columns > _crt->width()/_crt->fontMetrics().maxWidth()){
         columns = _crt->width()/_crt->fontMetrics().maxWidth();
      }
      if( _enableOffset){
         _crt->printf( "%04X: ", row * _crt->width()/_crt->fontMetrics().maxWidth());
      }
      textLine( p, columns);
      _crt->putch( '\n');
      p    += columns;
      size -= columns;
   }
} // text

//------------------------------------------------------------------------------
//  Mixed dump
//------------------------------------------------------------------------------

void CrtDump::mixed( const void *data, int size)
{
   if( !_enable){
      return;                          // disable dump
   }
   int columns;
   int rows      = DivCover( size, _crt->width()/_crt->fontMetrics().maxWidth());
   const byte *p = (const byte *)data;
   for( int row = 0; row < rows; row++){
      columns = size;
      if( columns > _crt->width()/_crt->fontMetrics().maxWidth()){
         columns = _crt->width()/_crt->fontMetrics().maxWidth();
      }
      if( _enableOffset){
         _crt->printf( "%04X: ", row * _crt->width()/_crt->fontMetrics().maxWidth());
      }
      binaryLine( p, columns);
      _crt->puts( "| ");
      textLine( p, columns);
      _crt->putch( '\n');
      p    += columns;
      size -= columns;
   }
} // mixed

//------------------------------------------------------------------------------
//  Binary line
//------------------------------------------------------------------------------

void CrtDump::binaryLine( const void *data, int size)
{
   if( !_enable){
      return;                          // disable dump
   }
   char buffer[ BUFFER_SIZE];
   int  offset;
   const byte *array = (const byte *)data;
   offset = 0;
   for( int i = 0; i < size; i++){
      sprintf( &buffer[ offset], "%02X ", array[ i]);
      offset += 3;
   }
   buffer[ offset++] = '\0';
   _crt->puts( buffer);
} // binaryLine

//------------------------------------------------------------------------------
//  Text line
//------------------------------------------------------------------------------

void CrtDump::textLine( const void *data, int size)
{
   if( !_enable){
      return;                          // disable dump
   }
   char buffer[ BUFFER_SIZE];
   int  offset;
   const byte *array = (const byte *)data;
   offset = 0;
   for( int i = 0; i < size; i++){
      if( array[ i] == 0){
         buffer[ offset++] = '.';       // zero as nonprintable
      } else if( !isprint( array[ i])){
         buffer[ offset++] = '.';       // nonprintable character
      } else {
         buffer[ offset++] = array[ i]; // ascii character
      }
   }
   buffer[ offset++] = '\0';
   _crt->puts( buffer);
} // textLine
