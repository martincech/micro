//*****************************************************************************
//
//    IFlash.cpp       Internal flash programming
//    Version 1.0    (c) Veit Electronics
//
//*****************************************************************************

#include "Flash/IFlash.h"
#include "Hardware.h"
#include <QFile>
#include <QDir>

#define WRITE_BLOCK_SIZE   4

byte IFlash[MCU_FLASH_SIZE]  __attribute__ ((aligned (MCU_FLASH_SECTOR_SIZE)));
const QString FILE_NAME = QDir::currentPath() + "/IFlash.bin";

//-----------------------------------------------------------------------------
// Init
//-----------------------------------------------------------------------------

void IFlashInit( void)
// Init
{
   memset( IFlash, 0xFF, MCU_FLASH_SIZE);          // default contents
   QFile file( FILE_NAME);
   if( !file.open( QIODevice::ReadOnly)){
      return;
   }
   int size;
   size = file.read( (char *)IFlash, MCU_FLASH_SIZE);
   file.close();
   if(size != MCU_FLASH_SIZE) {
      memset( IFlash, 0xFF, MCU_FLASH_SIZE);          // default contents
   }
}

//-----------------------------------------------------------------------------
// Sector erase
//-----------------------------------------------------------------------------

TYesNo IFlashErase( dword StartSectorAddress, dword EndSectorAddress)
// Erase <SectorAddress> sector
{
dword Address;
   StartSectorAddress = StartSectorAddress & ~(MCU_FLASH_SECTOR_SIZE - 1);
   EndSectorAddress = (EndSectorAddress + MCU_FLASH_SECTOR_SIZE) & ~(MCU_FLASH_SECTOR_SIZE - 1);
   Address = StartSectorAddress;
   while(Address < EndSectorAddress) {
      memset( (void *)Address, 0xFF, MCU_FLASH_SECTOR_SIZE);          // default contents
      Address += MCU_FLASH_SECTOR_SIZE;
   }
   QFile file( FILE_NAME);
   if( !file.open( QIODevice::WriteOnly | QIODevice::Truncate)){
      return NO;
   }
   file.write((char *)IFlash, MCU_FLASH_SIZE);
   file.close();
   return YES;
} // IFlashSectorErase

//-----------------------------------------------------------------------------
// Program
//-----------------------------------------------------------------------------

TYesNo IFlashProgram( dword Address, byte *Data, dword Size)
// Program <Address> by <Data> with <Size>
// <Address>, <Size> aligned to 4/8 multiplies depending on flash configuration
{
byte *Flash;
   Address &= ~(WRITE_BLOCK_SIZE - 1); // Must be 64-bit aligned (Flash address [2:0] = 000)
   Flash = (byte *)Address;
   while(Size--) {
      *Flash &= *Data + 2;
      Flash++;
      Data++;
   }

   QFile file( FILE_NAME);
   if( !file.open( QIODevice::WriteOnly | QIODevice::Truncate)){
      return NO;
   }
   file.write((char *)IFlash, MCU_FLASH_SIZE);
   file.close();

   return YES;
} // IFlashProgram
