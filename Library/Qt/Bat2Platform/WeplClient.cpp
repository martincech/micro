//******************************************************************************
//
//   Wepl.cpp       Weighing platform communication
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "WeplClient.h"
#include "Platform/PlatformRpc.h"

#define PLATFORM_NAME "Wepl"

#define DumpPuts( s)    if( _crt) _crt->puts( s)

static LocalClient *_client;
static Crt         *_crt;

// command buffer :
static TPlatformCommand Command;
static TPlatformReply   Reply;
static int              RxSize;

#define CommandClear()  memset( &Command, 0, sizeof( TPlatformCommand))

static TYesNo CommandSend( int Size, int ReplySize);
// Send <Command> with <Size>, <ReplySize> is requested reply size

//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

void WeplClientInit( LocalClient *client, Crt *crt)
// Initialize client
{
   _client = client;
   _crt    = crt;
} // WeplClientInit

//------------------------------------------------------------------------------
//  Address
//------------------------------------------------------------------------------

void WeplAddressSet( TPlatformAddress Address)
// Set current platform <Address>
{
} // WeplAddressSet

//------------------------------------------------------------------------------
//  Version
//------------------------------------------------------------------------------

TYesNo WeplVersionGet( TPlatformVersion *Version)
// Get device version info
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command = PLATFORM_CMD_VERSION_GET;
   CommandSize = PlatformSimpleCommandSize();
   ReplySize   = PlatformReplySize( Version);
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   *Version = Reply.Data.Version;
   return( YES);
} // WeplVersion

//------------------------------------------------------------------------------
//  Status
//------------------------------------------------------------------------------

TYesNo WeplStatusGet( TPlatformStatus *Status)
// Get device status
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command = PLATFORM_CMD_STATUS_GET;
   CommandSize = PlatformSimpleCommandSize();
   ReplySize   = PlatformReplySize( Status);
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   *Status = Reply.Data.Status;
   return( YES);
} // WeplStatusGet

//------------------------------------------------------------------------------
//  Clock
//------------------------------------------------------------------------------

TYesNo WeplClockSet( TPlatformClock Clock)
// Set device clock
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command    = PLATFORM_CMD_CLOCK_SET;
   Command.Data.Clock = Clock;
   CommandSize = PlatformCommandSize( Clock);
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplClockSet

//------------------------------------------------------------------------------
//  Picostrain
//------------------------------------------------------------------------------

TYesNo WeplPicostrainSet( TPlatformPicostrain *Picostrain)
// Set picostrain configuration
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command         = PLATFORM_CMD_PICOSTRAIN_SET;
   Command.Data.Picostrain = *Picostrain;
   CommandSize = PlatformCommandSize( Picostrain);
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplPicostrainSet

//------------------------------------------------------------------------------
//  Sigma Delta
//------------------------------------------------------------------------------

TYesNo WeplSigmaDeltaSet( TPlatformSigmaDelta *SigmaDelta)
// Set sigma-delta configuration
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command         = PLATFORM_CMD_SIGMA_DELTA_SET;
   Command.Data.SigmaDelta = *SigmaDelta;
   CommandSize = PlatformCommandSize( SigmaDelta);
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplSigmaDeltaSet

//------------------------------------------------------------------------------
//  Detection
//------------------------------------------------------------------------------

TYesNo WeplDetectionSet( TPlatformDetection *Detection)
// Set detection parameters
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command         = PLATFORM_CMD_DETECTION_SET;
   Command.Data.Detection = *Detection;
   CommandSize = PlatformCommandSize( Detection);
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplDetectionSet

//------------------------------------------------------------------------------
//  Acceptance
//------------------------------------------------------------------------------

TYesNo WeplAcceptanceSet( TPlatformAcceptance *Acceptance)
// Set acceptance parameters
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command         = PLATFORM_CMD_ACCEPTANCE_SET;
   Command.Data.Acceptance = *Acceptance;
   CommandSize = PlatformCommandSize( Acceptance);
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplAcceptanceSet

//------------------------------------------------------------------------------
//  Calibration
//------------------------------------------------------------------------------

TYesNo WeplCalibrationSet( TPlatformCalibration *Calibration)
// Set calibration parameters
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command          = PLATFORM_CMD_CALIBRATION_SET;
   Command.Data.Calibration = *Calibration;
   CommandSize = PlatformCommandSize( Calibration);
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplCalibrationSet

//------------------------------------------------------------------------------
//  Start
//------------------------------------------------------------------------------

TYesNo WeplWeighingStart( void)
// Start weighing
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command = PLATFORM_CMD_WEIGHING_START;
   CommandSize = PlatformSimpleCommandSize();
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplWeighingStart

//------------------------------------------------------------------------------
//  Diagnostic start
//------------------------------------------------------------------------------

TYesNo WeplDiagnosticStart( void)
// Start diagnostics
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command = PLATFORM_CMD_DIAGNOSTIC_START;
   CommandSize = PlatformSimpleCommandSize();
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplDiagnosticStart

//------------------------------------------------------------------------------
//  Stop
//------------------------------------------------------------------------------

TYesNo WeplStop( void)
// Stop weighing/diagnostics
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command = PLATFORM_CMD_STOP;
   CommandSize = PlatformSimpleCommandSize();
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplStop

//------------------------------------------------------------------------------
//  Saved weight
//------------------------------------------------------------------------------

TYesNo WeplSavedWeightGet( word *Count, TPlatformWeight *Sample)
// Returns saved <Sample> with <Count>
{
int CommandSize;
int ReplyCount;

   CommandClear();
   Command.Command = PLATFORM_CMD_SAVED_WEIGHT_GET;
   CommandSize = PlatformSimpleCommandSize();
   if( !CommandSend( CommandSize, 0)){
      return( NO);
   }
   ReplyCount = Reply.Data.SavedWeight.Count;
   if( ReplyCount > PLATFORM_SAVED_WEIGHT_COUNT){
      DumpPuts( "Wrong saved weight count\n");
      return( NO);
   }
   if( (int)PlatformSavedWeightReplySize( ReplyCount) != RxSize){
      DumpPuts( "Wrong saved weight size\n");
      return( NO);
   }
   *Count = ReplyCount;
   if( ReplyCount == 0){
      return( YES);
   }
   memcpy( Sample, &Reply.Data.SavedWeight.Weight, ReplyCount * sizeof( TPlatformWeight));
   return( YES);
} // WeplSavedWeightGet

//------------------------------------------------------------------------------
//  Confirm saved weight
//------------------------------------------------------------------------------

TYesNo WeplSavedWeightConfirm( word Count)
// Confirm <Count> saved weights
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command = PLATFORM_CMD_SAVED_WEIGHT_CONFIRM;
   Command.Data.Confirm.Count = Count;
   CommandSize = PlatformCommandSize( SavedWeightConfirm);
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplSavedWeightConfirm

//------------------------------------------------------------------------------
//   Communication set
//------------------------------------------------------------------------------

TYesNo WeplCommunicationSet( TPlatformCommunication *Communication)
// Set communication parameters (warning : parameters are set immediately)
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command            = PLATFORM_CMD_COMMUNICATION_SET;
   Command.Data.Communication = *Communication;
   CommandSize = PlatformCommandSize( Communication);
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplCommunicationSet

//------------------------------------------------------------------------------
//   Communication status get
//------------------------------------------------------------------------------

TYesNo WeplCommunicationStatusGet( TPlatformCommunicationStatus *CommunicationStatus)
// Get communication status
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command = PLATFORM_CMD_COMMUNICATION_STATUS_GET;
   CommandSize = PlatformSimpleCommandSize();
   ReplySize   = PlatformReplySize( CommunicationStatus);
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   *CommunicationStatus = Reply.Data.CommunicationStatus;
   return( YES);
} // WeplCommunicationStatusGet

//******************************************************************************

//------------------------------------------------------------------------------
//  Send
//------------------------------------------------------------------------------

static TYesNo CommandSend( int Size,  int ReplySize)
// Send <Command> with <Size>, <ReplySize> is requested reply size
{
   if( !_client->send( &Command, Size)){
      // unable send, try connect server :
      if( !_client->connect( PLATFORM_NAME)){
         DumpPuts( "Unable send\n");
         return( NO);                  // server not found
      }
      // server connected retry send :
      if( !_client->send( &Command, Size)){
         DumpPuts( "Unable send\n");
         _client->disconnect();
         return( NO);                  // server failure
      }
   }
   DumpPuts( "Message sent\n");
   RxSize = _client->receive( &Reply, SOCKET_MESSAGE_SIZE_MAX);
   if( !RxSize){
      DumpPuts( "Receive timeout\n");
      return( NO);
   }
   DumpPuts( "Reply received\n");
   // check for reply :
   if( Reply.Reply != PlatformReply( Command.Command)){
      DumpPuts( "Wrong reply code\n");
      _client->disconnect();           // reconnect to flush data
      return( NO);
   }
   if( ReplySize == 0){
      return( YES);                    // don't check size
   }
   if( ReplySize != RxSize){
      DumpPuts( "Reply size mismatch\n");
      _client->disconnect();           // reconnect to flush data
      return( NO);
   }
   return( YES);
} // CommandSend
