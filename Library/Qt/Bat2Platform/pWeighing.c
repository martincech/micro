//******************************************************************************
//
//   Weighing.c    Weighing utilities
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "Weighing/Weighing.h"
#include "Bat2Platform/WeighingSimulation.h"
#include "System/System.h"            // Operating system
#include "Platform/Platform.h"        // Platform data
#include "Fifo/Fifo.h"                // Samples FIFO
#include "Filter/FilterRelative.h"    // Samples filter
#include "Platform/Acceptance.h"      // Acceptance filter

#define SAMPLES_STEP_DEFAULT   10     // step 0.1s

// Local data :

static TWeightGauge  _ActualWeight;         // currently measured weight
static TWeightSample *_Samples;             // ADC samples array
static int            _SamplesCount;        // ADC samples count
static int            _SamplesIndex;        // ADC actual index
static int            _SamplesStep;         // ADC samples per step
static TYesNo         _StaticMode;          // static simulation
static UDateTimeGauge _DateTime;

// Local functions :

static void SetupFilter( void);
// Setup weight filering

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------

void pWeighingInit( void)
// Initialize weighing
{
   FifoInit();
   FilterStop();
   _ActualWeight = 0;
   _Samples      = 0;
   _SamplesCount = 0;
   _SamplesIndex = 0;
   _SamplesStep  = SAMPLES_STEP_DEFAULT;
   _StaticMode   = NO;
   _DateTime = SysDateTime();
} // WeighingInit

//------------------------------------------------------------------------------
// Start
//------------------------------------------------------------------------------

void pWeighingStart( void)
// Power up start weighing
{
   _ActualWeight = 0;
   FifoInit();                         // clear samples FIFO
   SetupFilter();                      // set parameters
   FilterStart();                      // start filter
} // WeighingStart

//------------------------------------------------------------------------------
// Stop
//------------------------------------------------------------------------------

void pWeighingStop( void)
// Power down stop weighing
{
   _ActualWeight = 0;
   FilterStop();
} // WeighingStop

//------------------------------------------------------------------------------
// Diagnostic
//------------------------------------------------------------------------------

void WeighingDiagnosticStart( void)
// Start diagnostics
{
} // WeighingDiagnosticStart

//------------------------------------------------------------------------------
// Execute
//------------------------------------------------------------------------------

void pWeighingExecute( void)
// Execute automatic weighing
{
TWeightSample *Sample;
TWeightGauge   Weight;
byte           Flags;
int            i;

   if( PlatformOperation() != PLATFORM_OPERATION_WEIGHING){
      return;
   }
   // check for samples database :
   if( _SamplesCount == 0){
      _ActualWeight = 0;
      return;                          // empty samples
   }
   // execute samples step :
   for( i = 0; i < _SamplesStep; i++){
      if( _SamplesIndex >= _SamplesCount){
         return;                      // samples array exhausted
      }
      Sample = &_Samples[ _SamplesIndex++];
      FilterNextSample( Sample->Weight);                    // filter sample
      SysDateTimeSet( _DateTime + TimestampClock( Sample->Timestamp));  // fake time evaluation
      // set actual value :
      Weight        = FilterRecord.LowPass;
      _ActualWeight = Weight;
      // check for stable value :
      if( !FilterRead( &Weight)){
         continue;
      }
      if( !AcceptWeight( &Weight, &Flags)){
         continue;
      }
      // accept stable value :
      FifoPut( Weight, Flags);
   }
} // WeighingExecute

//------------------------------------------------------------------------------
// Raw weight
//------------------------------------------------------------------------------

TRawWeight WeighingRaw( void)
// Get actual value
{
   if( _SamplesCount == 0){
      return( 0);
   }
   return( _Samples[ _SamplesIndex].Weight);
} // WeighingRaw

//------------------------------------------------------------------------------
// Weight
//------------------------------------------------------------------------------

TWeightGauge WeighingWeight( void)
// Returns actual weight
{
   return( _ActualWeight);
} // WeighingWeight

//------------------------------------------------------------------------------
// Simulation stop
//------------------------------------------------------------------------------

void WeighingSimulationStop( void)
// Stop simulation
{
   pWeighingStop();
   PlatformOperationSet( PLATFORM_OPERATION_STOP);
} // WeighingSimulationStop

//------------------------------------------------------------------------------
// Samples
//------------------------------------------------------------------------------

void WeighingSamplesSet( TWeightSample *Samples, int SamplesCount)
// Set ADC <Samples> with <SamplesCount>
{
   _Samples      = Samples;
   _SamplesCount = SamplesCount;
   _SamplesIndex = 0;
} // WeighingSamplesSet

//------------------------------------------------------------------------------
// Step
//------------------------------------------------------------------------------

void WeighingStepSet( int WeighingStep)
// Set <WeighingStep> for single execute call
{
   _SamplesStep = WeighingStep;
} // WeighingStepSet

//------------------------------------------------------------------------------
// Samples index
//------------------------------------------------------------------------------

int WeighingSamplesIndex( void)
// return Samples Index
{
   return( _SamplesIndex);
} // WeighingSamplesIndex

void WeighingSamplesIndexSet( int SamplesIndex)
// Set <SamplesIndex>
{
   if( SamplesIndex >= _SamplesCount){
      _SamplesIndex = _SamplesCount;
      return;
   }
   if( SamplesIndex < 0){
      _SamplesIndex = 0;
      return;
   }
   _SamplesIndex = SamplesIndex;
} // WeighingSamplesIndexSet

//------------------------------------------------------------------------------
// End
//------------------------------------------------------------------------------

TYesNo WeighingEnd( void)
// Weighing samples exhausted
{
   return( _SamplesIndex >= _SamplesCount);
} // WeighingEnd

//------------------------------------------------------------------------------
//   Static mode
//------------------------------------------------------------------------------

void WeighingStaticModeSet( TYesNo Static)
// Set <Static> mode of simulation
{
   _StaticMode = Static;
} // WeighingStaticModeSet

//------------------------------------------------------------------------------
//   Static mode accept
//------------------------------------------------------------------------------

void WeighingWeightAccept( TWeightGauge Weight)
// set <Weight> as accepted weight
{
byte Flags;

   if( !_StaticMode){
      return;
   }
   if( PlatformOperation() != PLATFORM_OPERATION_WEIGHING){
      return;
   }
   _ActualWeight = Weight;
   // check for stable value :
   if( !AcceptWeight( &Weight, &Flags)){
      return;
   }
   FifoPut( Weight, Flags);
} // WeighingWeightAccept

//------------------------------------------------------------------------------
//   Diagnostics
//------------------------------------------------------------------------------

#include "Diagnostic/pDiagnostic.h"

TYesNo pDiagnosticFrameGet( dword IdIn, dword *IdOut, TDiagnosticFrame *Frame)
// Get frame
{
   static int LastId;
   TWeightSample *Sample;
   int   DiagnosticSampleIndex;
   int   i;

   if( PlatformOperation() != PLATFORM_OPERATION_DIAGNOSTICS){
      return NO;
   }

   if(LastId == IdIn) {
      _SamplesIndex += DIAGNOSTIC_BURST_MAX;
   }
   DiagnosticSampleIndex = _SamplesIndex;

   Frame->Count = 0;

   for( i = 0; i < DIAGNOSTIC_BURST_MAX; i++){
      if( DiagnosticSampleIndex >= _SamplesCount){
         break;                      // samples array exhausted
      }
      Sample = &_Samples[ DiagnosticSampleIndex++];
      DiagnosticWeightCompose(&Frame->Weight[i], Sample->Weight);
      Frame->Count++;
      if(i == 0) {
         Frame->Timestamp = Sample->Timestamp;
      }
   }
   LastId = IdIn + 1;
   *IdOut = LastId;

   return YES;
}

//******************************************************************************

//------------------------------------------------------------------------------
// Setup filter
//------------------------------------------------------------------------------

static void SetupFilter( void)
// Setup weight filering
{
   FilterRecord.Inversion       = NO;
   FilterRecord.AveragingWindow = PlatformDetection.Fine.AveragingWindow;
   FilterRecord.StableWindow    = PlatformDetection.Fine.StableWindow;
   FilterRecord.ZeroWeight      = 0;
   // calculate stable range :
   FilterRecord.StableRange     = PlatformDetection.Fine.AbsoluteRange;
   FilterRecord.MaxWeight       = uConstDwordGet( pPlatformVersion.WeightMax);
} // SetupFilter
