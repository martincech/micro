//*****************************************************************************
//
//    Rcs.cpp      Remote control server
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Rcs.h"
#include "System/System.h"             // Operating system
#include "Remote/RcsCommand.h"         // Remote command processing
#include "Uart/WinNative.h"

#define UART_TIMEOUT       10        // reply timeout [ms]

#define BUFFER_SIZE NATIVE_DATA_SIZE

static byte      _buffer[ BUFFER_SIZE];
static WinNative *_server;

//-----------------------------------------------------------------------------
// Initialize
//-----------------------------------------------------------------------------

void RcsInit( CrtDump *dump, UartParameters *parameters)
// Initialize
{
   RcsCommandInit();
   if(_server) {
      delete _server;
   }
   _server = new WinNative();
   parameters->toUart(_server);
   _server->setLogger(dump);
   _server->setReceiveTimeout( UART_TIMEOUT, 1);
   _server->setRxTimeoutShow(NO);
} // RcsInit

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

void RcsExecute( void)
// Server executive
{
int  rxSize;
TNativeAddress Address;
   if(!_server->receive(&Address, _buffer, &rxSize)) {
      return;
   }
   if( !rxSize){
      return;
   }
   word ReplySize;
   if( !RcsCommand( Address, _buffer, (word)rxSize, _buffer, &ReplySize)){
      return;
   }
   _server->send( Address, _buffer, ReplySize);
} // RcsExecute
