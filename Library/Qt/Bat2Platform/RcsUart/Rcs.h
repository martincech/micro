//*****************************************************************************
//
//    Rcs.h        Remote control server
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Rcs_H__
   #define __Rcs_H__

#include "Uart/UartParameters.h"
#include "Crt/CrtDump.h"

void RcsInit( CrtDump *dump, UartParameters *server);
// Initialize

void RcsExecute( void);
// Server executive

#endif
