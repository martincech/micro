//******************************************************************************
//
//   PlatformSimulator.cpp  Bat2Platform simulator window
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "PlatformSimulator.h"
#include "ui_PlatformSimulator.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QDir>
#include <QFile>

#include "Bat2Platform/WeighingSimulation.h"
#include "Weighing/pWeighing.h"
#include "Time/uTime.h"

#define TIMER_SLOW_TIME      1000

#define PLAYBACK_STEP        600
#define PLAYBACK_LARGE_STEP  36000

#define updateStatus( s)     ui->statusBar->showMessage( s)

//------------------------------------------------------------------------------
//   Constructor
//------------------------------------------------------------------------------

PlatformSimulator::PlatformSimulator(QWidget *parent) :
   QMainWindow(parent),
   ui(new Ui::PlatformSimulator)
{
   ui->setupUi(this);
   setWindowTitle( "Bat2 Platform Simulator");
   // create slow timer :
   _timerSlow = new QTimer( this);
   connect( _timerSlow, SIGNAL( timeout()), this, SLOT( timerSlowTimeout()));
   _timerSlow->start( TIMER_SLOW_TIME);
   // initialize database :
   _samples   = new Samples();
   _playback  = false;                 // stop playback
   _suspended = true;                  // stop sampling
   updateStatus( "Stop");
   // initialize playback speed :
   ui->playbackSpeed->addItem( "1x",    10);
   ui->playbackSpeed->addItem( "10x",   100);
   ui->playbackSpeed->addItem( "60x",   600);
   ui->playbackSpeed->addItem( "3600x", 36000);
   ui->playbackSpeed->setCurrentIndex( 0);
   // initialize platform :
   pWeighingInit();
} // PlatformSimulator

//------------------------------------------------------------------------------
//   Destructor
//------------------------------------------------------------------------------

PlatformSimulator::~PlatformSimulator()
{
   delete ui;
   if( _timerSlow){
      delete _timerSlow;
   }
   if( _samples){
      delete _samples;
   }
} // ~PlatformSimulator


//------------------------------------------------------------------------------
//  Timer
//------------------------------------------------------------------------------

void PlatformSimulator::timerSlowTimeout()
{
   updateTime( WeighingSamplesIndex());

   if( _suspended){
      return;
   }
   pWeighingExecute();
} // timerSlowTimeout

//------------------------------------------------------------------------------
//   Open
//------------------------------------------------------------------------------

void PlatformSimulator::on_actionOpen_triggered()
{
   QString fileName;
   fileName = QFileDialog::getOpenFileName( this, "Open file",
                                            QDir::currentPath(),
                                            "CSV files (*.csv);;All files (*.*)");
   if( fileName.isNull()){
      return;
   }
   QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));    // hourglass cursor
   WeighingSimulationStop();                                    // stop weighing simulation
   if( !_samples->load( fileName)){
      ui->totalTime->setText( samplesToTime( 0));               // total samples count
      ui->samplesGauge->setMaximum( 0);
      QString msg;
      msg.sprintf( "Unable parse file '%s'", qPrintable( fileName));
      QMessageBox::warning( this, "Error", msg, QMessageBox::Ok);
      QApplication::restoreOverrideCursor();                    // default cursor
      return;
   }
   WeighingSamplesSet( _samples->samples(), _samples->count());    // set weighing samples
   QApplication::restoreOverrideCursor();                          // default cursor
   ui->samplesGauge->setMaximum( _samples->count() - 1);
   ui->totalTime->setText( samplesToTime( _samples->count()- 1));  // end time
   on_actionStop_triggered();                                      // stop playback
} // on_actionOpen_triggered

//------------------------------------------------------------------------------
//   Fixed mode
//------------------------------------------------------------------------------

void PlatformSimulator::on_actionFixed_triggered()
{
   if( ui->actionFixed->isChecked()){
      // stop playback :
      on_actionStop_triggered();
      updateTime( 0);
      WeighingStaticModeSet( YES);
      // disable controllers :
      ui->actionFastBackward->setEnabled( false);
      ui->actionBackward->setEnabled( false);
      ui->actionForward->setEnabled( false);
      ui->actionFastForward->setEnabled( false);
      ui->actionPlay->setEnabled( false);
      ui->actionStop->setEnabled( false);
      ui->actionPause->setEnabled( false);
      // enable fixed value :
      ui->labelValue->setEnabled( true);
      ui->editValue->setEnabled( true);
      ui->buttonSet->setEnabled( true);
   } else {
      // stop fixed simulation :
      WeighingStaticModeSet( NO);
      // disable fixed value :
      ui->labelValue->setEnabled( false);
      ui->editValue->setEnabled( false);
      ui->buttonSet->setEnabled( false);
      // enable controllers :
      ui->actionFastBackward->setEnabled( true);
      ui->actionBackward->setEnabled( true);
      ui->actionForward->setEnabled( true);
      ui->actionFastForward->setEnabled( true);
      ui->actionPlay->setEnabled( true);
      ui->actionStop->setEnabled( true);
      ui->actionPause->setEnabled( true);
   }
} // on_actionFixed_triggered

//------------------------------------------------------------------------------
//   Set value
//------------------------------------------------------------------------------

void PlatformSimulator::on_buttonSet_clicked()
{
double Weight;

   Weight  = ui->editValue->text().toDouble();
   Weight *= 1000;                     // kg -> g
   Weight *= WEIGHT_UNITS_FACTOR;      // g -> weight gauge
   if( Weight > 0){
      Weight += 0.5;
   } else {
      Weight -= 0.5;
   }
   WeighingWeightAccept( (TWeightGauge)Weight);
} // on_buttonSet_clicked()

//------------------------------------------------------------------------------
//   Speed changed
//------------------------------------------------------------------------------

void PlatformSimulator::on_playbackSpeed_currentIndexChanged( int index)
{
   int speed = ui->playbackSpeed->itemData( index).toInt();
   WeighingStepSet( speed);
} // on_playbackSpeed_currentIndexChanged

//------------------------------------------------------------------------------
//   Slider
//------------------------------------------------------------------------------

void PlatformSimulator::on_samplesGauge_sliderPressed()
{
   _suspended = true;
} // on_samplesGauge_sliderPressed

void PlatformSimulator::on_samplesGauge_sliderMoved( int position)
{
   if( _samples->count() == 0){
      ui->currentTime->setText( samplesToTime( 0));
      return;
   }
   ui->currentTime->setText( samplesToTime( position)) ;
} // on_samplesGauge_sliderMoved

void PlatformSimulator::on_samplesGauge_sliderReleased()
{
   WeighingSamplesIndexSet( ui->samplesGauge->value());
   if( _playback){
      _suspended = false;              // continue playback
   }
} // on_samplesGauge_sliderReleased

//------------------------------------------------------------------------------
//   Playback
//------------------------------------------------------------------------------

void PlatformSimulator::on_actionPlay_triggered()
{
   if( _samples->count() == 0){
      return;
   }
   _playback  = true;                 // run playback
   _suspended = false;                // run sampling
   updateStatus( "Play");
} // on_actionPlay_triggered

void PlatformSimulator::on_actionPause_triggered()
{
   if( _samples->count() == 0){
      return;
   }
   _playback  = false;                 // stop playback
   _suspended = true;                  // stop sampling
   updateStatus( "Pause");
} // on_actionPause_triggered

void PlatformSimulator::on_actionStop_triggered()
{
   if( _samples->count() == 0){
      return;
   }
   _playback  = false;                 // stop playback
   _suspended = true;                  // stop sampling
   WeighingSamplesIndexSet( 0);
   updateTime( 0);
   updateStatus( "Stop");
} // on_actionStop_triggered

//------------------------------------------------------------------------------
//   Move
//------------------------------------------------------------------------------

void PlatformSimulator::on_actionFastBackward_triggered()
{
   if( _samples->count() == 0){
      return;
   }
   int index = WeighingSamplesIndex();
   if( index < PLAYBACK_LARGE_STEP){
      index  = 0;
   } else {
      index -= PLAYBACK_LARGE_STEP;
   }
   WeighingSamplesIndexSet( index);
   updateTime( index);
} // on_actionFastBackward_triggered

void PlatformSimulator::on_actionBackward_triggered()
{
   if( _samples->count() == 0){
      return;
   }
   int index = WeighingSamplesIndex();
   if( index < PLAYBACK_STEP){
      index  = 0;
   } else {
      index -= PLAYBACK_STEP;
   }
   WeighingSamplesIndexSet( index);
   updateTime( index);
} // on_actionBackward_triggered

void PlatformSimulator::on_actionForward_triggered()
{
   if( _samples->count() == 0){
      return;
   }
   int index = WeighingSamplesIndex();
   index    += PLAYBACK_STEP;
   if( index >= _samples->count()){
      index  = _samples->count() - 1;
   }
   WeighingSamplesIndexSet( index);
   updateTime( index);
} // on_actionForward_triggered

void PlatformSimulator::on_actionFastForward_triggered()
{
   if( _samples->count() == 0){
      return;
   }
   int index = WeighingSamplesIndex();
   index    += PLAYBACK_LARGE_STEP;
   if( index >= _samples->count()){
      index  = _samples->count() - 1;
   }
   WeighingSamplesIndexSet( index);
   updateTime( index);
} // on_actionFastForward_triggered


//------------------------------------------------------------------------------
//   Update time
//------------------------------------------------------------------------------

void PlatformSimulator::updateTime( int index)
{
   ui->currentTime->setText( samplesToTime( index)) ;
   if( _samples->count() == 0){
      ui->samplesGauge->setValue( 0);
   } else {
      ui->samplesGauge->setValue( index);
   }
} // updateTime

//------------------------------------------------------------------------------
//   Samples to time
//------------------------------------------------------------------------------

QString PlatformSimulator::samplesToTime( int index)
{
   TWeightSample Sample;
   Sample = _samples->samples()[ index];
   UTime time;
   uTime( &time, TimestampClock( Sample.Timestamp));
   QString timeText;
   timeText.sprintf( "%02d:%02d:%02d", time.Hour, time.Min, time.Sec);
   return( timeText);
} // samplesToTime
