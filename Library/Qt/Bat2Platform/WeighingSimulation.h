//******************************************************************************
//
//   WeighingSimulation.h   Weighing simulator
//   Version 1.0            (c) VEIT Electronics
//
//******************************************************************************

#ifndef __WeighingSimulation_H__
   #define __WeighingSimulation_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeightDef_H__
   #include "Weight/WeightDef.h"
#endif

// weight samples record :
typedef struct {
   TWeightGauge Weight;             // weight    [0.1g]
   int64        Timestamp;          // timestamp [ms]
} TWeightSample;

// samples timestamp :
#define TimestampClock( ts)   (dword)((ts) / 1000)

#ifdef __cplusplus
   extern "C" {
#endif

//------------------------------------------------------------------------------
// Simulator
//------------------------------------------------------------------------------

void WeighingSimulationStop( void);
// Stop simulation

void WeighingSamplesSet( TWeightSample *Samples, int SamplesCount);
// Set ADC <Samples> with <SamplesCount>

void WeighingStepSet( int WeighingStep);
// Set <WeighingStep> for single execute call

int WeighingSamplesIndex( void);
// returns Samples Index

void WeighingSamplesIndexSet( int SamplesIndex);
// Set <SamplesIndex>

TYesNo WeighingEnd( void);
// Weighing samples exhausted

//------------------------------------------------------------------------------
// Simulator static
//------------------------------------------------------------------------------

void WeighingStaticModeSet( TYesNo Static);
// Set <Static> mode of simulation

void WeighingWeightAccept( TWeightGauge Weight);
// set <Weight> as accepted weight

//------------------------------------------------------------------------------

#ifdef __cplusplus
   }
#endif

#endif
