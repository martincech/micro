//*****************************************************************************
//
//    uListDef.h   List data description
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __uListDef_H__
   #define __uListDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __File_H__
   #include "Memory/File.h"
#endif

//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#define ULIST_INDEX_INVALID  0xFF

//------------------------------------------------------------------------------
//  Data types
//------------------------------------------------------------------------------

typedef byte  UListIndex;             // list items count/index
typedef word  UListCrc;               // list CRC
typedef word  UListItemSize;          // list item size
typedef dword UListAddress;           // nonvolatile memory address

//------------------------------------------------------------------------------
//  List header
//------------------------------------------------------------------------------

typedef struct {
   UListIndex    Capacity;             // total list items count
   UListIndex    Count;                // actual list items count
   UListItemSize ItemSize;             // list item size
   UListCrc      Crc;                  // checksum
} UListHeader;

//------------------------------------------------------------------------------
//  List layout
//------------------------------------------------------------------------------

typedef struct {
   UListHeader Header;            // list descriptor
// UListItem   Item[];            // list items
} UListLayout;

//------------------------------------------------------------------------------
//  List descriptor
//------------------------------------------------------------------------------

typedef struct {
   UListIndex        Capacity;         // total list items count
   UListItemSize     ItemSize;         // list item size
} UListDescriptor;

//------------------------------------------------------------------------------
//  List
//------------------------------------------------------------------------------

typedef struct {
   const UListDescriptor *Descriptor;
   TFile File;
} UList;

#endif
