//*****************************************************************************
//
//    uFifo.c      FIFO storage utility
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Data/uFifo.h"
#include "Memory/File.h"
#include <string.h>

#define FIFO_ID_INVALID    (-1)

static TYesNo _FifoPointerSave( UFifoDescriptor *FifoDescriptor);
// Save pointer data

static UFifoAddress _FifoAddress( UFifoDescriptor *FifoDescriptor, UFifoIndex Index);
// Returns address of <Index> item

static TYesNo _PointerLoad( UFifoDescriptor *Descriptor);
// Load pointer

static TYesNo _PointerCheck( UFifoDescriptor *Descriptor);
// Check pointer

TYesNo uFifoCopy( const UFifoConfiguration *ConfigurationDest, const UFifoConfiguration *ConfigurationSrc)
// Copy fifo 
{
TFileAddress Address;
TFileAddress MaxAddress;
int Size;
UFifo FifoSrcInt;
UFifo *FifoSrc = &FifoSrcInt;
UFifoDescriptor DescriptorSrcInt;
UFifoDescriptor * DescriptorSrc = &DescriptorSrcInt;
UFifo FifoDestInt;
UFifo *FifoDest = &FifoDestInt;
UFifoDescriptor DescriptorDestInt;
UFifoDescriptor * DescriptorDest = &DescriptorDestInt;
TYesNo UpdateOnly;   // Check for range of data to update
//UFifoIndex StartMoved;

   DescriptorDest->Configuration = ConfigurationDest;
   DescriptorSrc->Configuration = ConfigurationSrc;

   FifoSrc->Descriptor = DescriptorSrc;
   FifoDest->Descriptor = DescriptorDest;

   if(!FileOpen(&FifoSrc->File, DescriptorSrc->Configuration->Filename, FILE_MODE_READ_ONLY)) {
      return NO;
   }

   if(FileIsOffline(&FifoSrc->File)) {
      FileClose( &FifoSrc->File);
      return NO;
   }

   if(!FileOpen(&FifoDest->File, DescriptorDest->Configuration->Filename, FILE_MODE_WRITE_ONLY)) {
      FileClose( &FifoSrc->File);
      return NO;
   }

   // Load Pointer from SRC
   if( !_PointerLoad(FifoSrc->Descriptor)) {
      FileClose( &FifoSrc->File);
      FileClose( &FifoDest->File);
      return NO;
   }

   UpdateOnly = NO;
/*
   if(_PointerLoad( FifoDest->Descriptor) &&
      FifoDest->Descriptor->Pointer.Id == FifoSrc->Descriptor->Pointer.Id &&
      FifoDest->Descriptor->Pointer.StartMoved <= FifoSrc->Descriptor->Pointer.StartMoved
      ) {
      StartMoved = FifoSrc->Descriptor->Pointer.StartMoved - FifoDest->Descriptor->Pointer.StartMoved;

      if(StartMoved < uFifoCount( FifoDest->Descriptor)) {
         UpdateOnly = YES;
         Address = _FifoAddress( FifoDest->Descriptor, FifoDest->Descriptor->Pointer.End);
         Size = (uFifoCount( FifoSrc->Descriptor) - (uFifoCount( FifoDest->Descriptor) - StartMoved)) * (FifoDest->Descriptor->Configuration->Size);
      }
   }
*/

   if(!UpdateOnly) {
      Address = _FifoAddress( FifoSrc->Descriptor, FifoSrc->Descriptor->Pointer.Start);
      Size = uFifoCount( FifoSrc->Descriptor) * FifoSrc->Descriptor->Configuration->Size;
   }

   if(Size) {
      //TODO Pokud prepisujeme oblast fifo, kde jsou platne zaznamy, pak by chtelo patricne upravit pointer, aby v pripade chyby pri zapisu neukazoval na neplatna data
      MaxAddress = _FifoAddress( FifoDest->Descriptor, FifoDest->Descriptor->Configuration->Capacity);
      // Update data
      if(Address + Size > MaxAddress) {
         if(!FileClone( &FifoDest->File,  &FifoSrc->File, Address, MaxAddress - Address)) {
            FileClose( &FifoSrc->File);
            FileClose( &FifoDest->File);
            return NO;
         }
	 if(!FileClone( &FifoDest->File,  &FifoSrc->File, _FifoAddress( FifoDest->Descriptor, 0), Size - (MaxAddress - Address))) {
            FileClose( &FifoSrc->File);
            FileClose( &FifoDest->File);
            return NO;
         }
      } else {
         if(!FileClone( &FifoDest->File,  &FifoSrc->File, Address, Size)) {
            FileClose( &FifoSrc->File);
            FileClose( &FifoDest->File);
            return NO;
         }
      }
   }

   // Save pointer
   memcpy( &FifoDest->Descriptor->Pointer, &FifoSrc->Descriptor->Pointer, sizeof(FifoSrc->Descriptor->Pointer));
   if(!_FifoPointerSave( FifoDest->Descriptor)) {
      FileClose( &FifoSrc->File);
      FileClose( &FifoDest->File);
      return NO;
   }
   FileClose( &FifoSrc->File);
   FileClose( &FifoDest->File);
   return YES;
} // uFifoCopy

//------------------------------------------------------------------------------
//  Initialization
//------------------------------------------------------------------------------

void uFifoInit( UFifoDescriptor *FifoDescriptor, const UFifoConfiguration *Configuration)
// Initialize
{
UFifo FifoInt;
UFifo *Fifo = &FifoInt;

   FifoDescriptor->Configuration = Configuration;
   Fifo->Descriptor = FifoDescriptor;

   if( _PointerLoad(Fifo->Descriptor)){
      return;
   }
   if( !uFifoOpen(Fifo, Fifo->Descriptor)) {
      return;
   }
   uFifoClear( Fifo);
   uFifoClose( Fifo);
} // uFifoInit

//------------------------------------------------------------------------------
//  Open
//------------------------------------------------------------------------------

TYesNo uFifoOpen( UFifo *Fifo, UFifoDescriptor *FifoDescriptor)
// Open <Fifo> with descriptor <FifoDescriptor>
{
   if(!FileOpen(&Fifo->File, FifoDescriptor->Configuration->Filename, FILE_MODE_READ_WRITE)) {
      return NO;
   }

   Fifo->Descriptor = FifoDescriptor;
   return YES;
} // uFifoOpen

//------------------------------------------------------------------------------
//  Close
//------------------------------------------------------------------------------

void uFifoClose( UFifo *Fifo)
// Open <Fifo>
{
   FileClose(&Fifo->File);
}

//------------------------------------------------------------------------------
//  Clear
//------------------------------------------------------------------------------

void uFifoClear( UFifo *Fifo)
// Clear - delete all items
{
   Fifo->Descriptor->Pointer.End %= Fifo->Descriptor->Configuration->Capacity;
   Fifo->Descriptor->Pointer.Start = Fifo->Descriptor->Pointer.End;
   Fifo->Descriptor->Pointer.Count = 0;
   Fifo->Descriptor->Pointer.StartMoved = 0;
   Fifo->Descriptor->Pointer.Id++;
   if(Fifo->Descriptor->Pointer.Id == FIFO_ID_INVALID) {
      Fifo->Descriptor->Pointer.Id++;
   }
   _FifoPointerSave( Fifo->Descriptor);
} // uFifoClear

//------------------------------------------------------------------------------
//  Count
//------------------------------------------------------------------------------

UFifoIndex uFifoCount( UFifoDescriptor *FifoDescriptor)
// Returns current items count
{
   return( FifoDescriptor->Pointer.Count);
} // uFifoCount

//------------------------------------------------------------------------------
//  Append
//------------------------------------------------------------------------------

TYesNo uFifoAppend( UFifo *Fifo, void *Data)
// Append <Data> at end
{
UFifoIndex Index;
   if( uFifoFull( Fifo->Descriptor)){
      if( !Fifo->Descriptor->Configuration->Overwrite){
         return( NO);                  // capacity reached
      }
      // delete first item :
      Fifo->Descriptor->Pointer.Start++;
      Fifo->Descriptor->Pointer.StartMoved++;
      Fifo->Descriptor->Pointer.Start %= Fifo->Descriptor->Configuration->Capacity;
      // same Count
   } else {
      Fifo->Descriptor->Pointer.Count++;           // append item
   }
   Index  = Fifo->Descriptor->Pointer.End;
   // save data :
   if(!FileSave( &Fifo->File, _FifoAddress( Fifo->Descriptor, Index), Data, Fifo->Descriptor->Configuration->Size)) {
      return NO;
   }
   // move index :
   Index++;
   Index %= Fifo->Descriptor->Configuration->Capacity;
   Fifo->Descriptor->Pointer.End = Index;
   // update pointers :
   _FifoPointerSave( Fifo->Descriptor);
   return( YES);
} // uFifoAppend

//------------------------------------------------------------------------------
//  Remove
//------------------------------------------------------------------------------

TYesNo uFifoRemove( UFifo *Fifo, void *Data)
// Read & remove first <Data> item
{
UFifoIndex Index;
   if( Fifo->Descriptor->Pointer.Count == 0){
      return( NO);
   }
   // get item index :
   Index  = Fifo->Descriptor->Pointer.Start;
   if( Data){
      // load data :
      if(!FileLoad( &Fifo->File, _FifoAddress( Fifo->Descriptor, Index), Data, Fifo->Descriptor->Configuration->Size)) {
         return NO;
      }
   }
   // move index :
   Index++;
   Index %= Fifo->Descriptor->Configuration->Capacity;
   Fifo->Descriptor->Pointer.Start = Index;
   Fifo->Descriptor->Pointer.StartMoved++;
   Fifo->Descriptor->Pointer.Count--;
   // update pointers :
   _FifoPointerSave( Fifo->Descriptor);
   return( YES);
} // uFifoRemove

//------------------------------------------------------------------------------
//  Get
//------------------------------------------------------------------------------

TYesNo uFifoGet( UFifo *Fifo, UFifoIndex Index, void *Data)
// Returns <Data> at <Index>
{
   if( Index >= Fifo->Descriptor->Pointer.Count){
      return( NO);
   }
   // get item index :
   Index += Fifo->Descriptor->Pointer.Start;
   Index %= Fifo->Descriptor->Configuration->Capacity;
   // load data :
   return FileLoad( &Fifo->File, _FifoAddress( Fifo->Descriptor, Index), Data, Fifo->Descriptor->Configuration->Size);
} // uFifoGet

//------------------------------------------------------------------------------
//  Empty
//------------------------------------------------------------------------------

TYesNo uFifoEmpty( UFifoDescriptor *FifoDescriptor)
// Returns YES on empty <Fifo>
{
   return( FifoDescriptor->Pointer.Count == 0);
} // uFifoEmpty

//------------------------------------------------------------------------------
//  Full
//------------------------------------------------------------------------------

TYesNo uFifoFull( UFifoDescriptor *FifoDescriptor)
// Returns YES on <Fifo> full
{
   return( FifoDescriptor->Pointer.Count == FifoDescriptor->Configuration->Capacity);
} // uFifoFull

//******************************************************************************

//------------------------------------------------------------------------------
//  Pointer save
//------------------------------------------------------------------------------

static TYesNo _FifoPointerSave( UFifoDescriptor *FifoDescriptor)
// Save pointer data
{
   TFile FileFifo;
   if( !FileOpen( &FileFifo, FifoDescriptor->Configuration->PointerFilename, FILE_MODE_WRITE_ONLY)) {
      return NO;
   }
   if(!FileSave( &FileFifo, FifoDescriptor->Configuration->Pointer, &FifoDescriptor->Pointer, UFIFO_POINTER_SIZE)) {
      FileClose( &FileFifo);
      return NO;
   }
   FileClose( &FileFifo);
   return YES;
} // _FifoPointerSave

//------------------------------------------------------------------------------
//  Address
//------------------------------------------------------------------------------

static UFifoAddress _FifoAddress( UFifoDescriptor *FifoDescriptor, UFifoIndex Index)
// Returns address of <Index> item
{
UFifoAddress Address;

   Address = Index * FifoDescriptor->Configuration->Size;
   return( Address);
} // _FifoAddress

//------------------------------------------------------------------------------
//  Load pointer
//------------------------------------------------------------------------------

static TYesNo _PointerLoad( UFifoDescriptor *Descriptor)
// Load pointer
{
TFile File;
   if(!FileOpen(&File, Descriptor->Configuration->PointerFilename, FILE_MODE_READ_ONLY)) {
      return NO;
   }
   if(!FileLoad(&File, Descriptor->Configuration->Pointer, &Descriptor->Pointer, UFIFO_POINTER_SIZE)) {
      FileClose(&File);
      return NO;
   }
   FileClose(&File);

   if( !_PointerCheck(Descriptor)){
      return NO;
   }

   return YES;
} // _PointerLoad

//------------------------------------------------------------------------------
//  Check pointer
//------------------------------------------------------------------------------

static TYesNo _PointerCheck( UFifoDescriptor *Descriptor)
// Check pointer
{
UFifoIndex Index;
   if( Descriptor->Pointer.Id == FIFO_ID_INVALID) {
      return NO;
   }
   if( Descriptor->Pointer.Count > Descriptor->Configuration->Capacity) {
      return NO;
   }
   Index  = Descriptor->Pointer.Start + Descriptor->Pointer.Count;
   Index %= Descriptor->Configuration->Capacity;
   if( Index != Descriptor->Pointer.End) {
      return NO;
   }
   return YES;
} // _PointerCheck
