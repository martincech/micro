//*****************************************************************************
//
//    uList.c       Sequential list utility
//    Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#include "Data/uList.h"
#include "Memory/Nvm.h"
#include <string.h>


// Local functions :

static TNvmAddress _ItemAddress( UList *List, UListIndex Index);
// Calculate item address by <Index>

static void _ItemAppend( UList *List, UListIndex Index, void *Item);
// Append item at <Index>

static void _ItemInsert( UList *List, UListIndex Index, UListIndex Count);
// Insert space at <Index>

static void _ItemRemove( UList *List, UListIndex Index, UListIndex Count);
// Remove item at <Index>

static void _ItemClear( UList *List, UListIndex Index);
// Clear <List> item at <Index>

static TYesNo _HeaderCheck( const UListDescriptor *Descriptor, UListHeader *Header);
// Check header

//------------------------------------------------------------------------------
//  Remote load
//------------------------------------------------------------------------------

TYesNo uListCopy(const UListDescriptor *Descriptor, TFileName FilenameDest, TFileName FilenameSrc)
// Load from remote device
{
UList ListSrcInt;
UList *ListSrc = &ListSrcInt;
UList ListDestInt;
UList *ListDest = &ListDestInt;
UListHeader Header;
TFileAddress Address;
dword Size;

   ListSrc->Descriptor = Descriptor;
   ListDest->Descriptor = Descriptor;

   if(!FileOpen(&ListSrc->File, FilenameSrc, FILE_MODE_READ_ONLY)) {
      return NO;
   }

   if(FileIsOffline(&ListSrc->File)) {
      Address = 0;
      Size = uListSize(ListSrc->Descriptor->Capacity, ListSrc->Descriptor->ItemSize);
      FileLoad(&ListSrc->File, Address, 0, Size);
      FileClose(&ListSrc->File);
      return YES;
   }

   if(!FileOpen(&ListDest->File, FilenameDest, FILE_MODE_WRITE_ONLY | FILE_MODE_SANDBOX)) {
      FileClose( &ListSrc->File);
      return NO;
   }

   if(!uListHeaderGet( ListSrc, &Header)) {
      FileClose( &ListSrc->File);
      FileClose( &ListDest->File);
      return NO;
   }

   Address = _ItemAddress( ListDest, 0);
   Size = Header.Count * ListDest->Descriptor->ItemSize;

   if(!FileClone( &ListDest->File, &ListSrc->File, Address, Size)) {
      FileClose( &ListSrc->File);
      FileClose( &ListDest->File);
      return NO;
   }

   if(!uListHeaderSet( ListDest, &Header)) {
      FileClose( &ListSrc->File);
      FileClose( &ListDest->File);
      return NO;
   }
   FileCommit( &ListDest->File);
   FileClose( &ListSrc->File);
   FileClose( &ListDest->File);
   return YES;
} // uListRemoteLoad

//------------------------------------------------------------------------------
//  Initialization
//------------------------------------------------------------------------------

void uListInit( const UListDescriptor *Descriptor, TFileName FileName)
// Initialize <List>
{
UListHeader Header;
UList ListInt;
UList *List = &ListInt;

   List->Descriptor = Descriptor;

   uListOpen( List, Descriptor, FileName);

   if(uListHeaderGet( List, &Header)) {
      uListClose( List);
      return;
   }

   // create default :
   memset( &Header, 0, sizeof( UListHeader));
   Header.Capacity = List->Descriptor->Capacity;
   Header.ItemSize = List->Descriptor->ItemSize;
   uListHeaderSet( List, &Header);
   FileFill( &List->File, _ItemAddress( List, 0), 0xFF, uListSize( List->Descriptor->Capacity, List->Descriptor->ItemSize));
   uListClose( List);
} // uListInit

//------------------------------------------------------------------------------
//   Open
//------------------------------------------------------------------------------

TYesNo uListOpen( UList *List, const UListDescriptor *Descriptor, TFileName FileName)
// Open
{
   if(!FileOpen(&List->File, FileName, FILE_MODE_READ_WRITE)) {
      return NO;
   }
   List->Descriptor = Descriptor;
   return YES;
} // uListOpen

//------------------------------------------------------------------------------
//   Close
//------------------------------------------------------------------------------

void uListClose( UList *List)
// Close
{
   FileClose( &List->File);
} // uListClose

//------------------------------------------------------------------------------
//   Add
//------------------------------------------------------------------------------

UListIndex uListAppend( UList *List, void *Item)
// Add <Item> to <List>
{
UListHeader Header;
UListIndex  Index;

   uListHeaderGet( List, &Header);
   // search a position for the item :
   Index = Header.Count;
   _ItemAppend( List, Index, Item);
   // update count :
   Header.Count++;
   uListHeaderSet( List, &Header);
   return( Index);
} // uListAppend

//------------------------------------------------------------------------------
//   Insert
//------------------------------------------------------------------------------

void uListInsert( UList *List, UListIndex Index, void *Item)
// Insert <Item> at <Index> of <List>
{
UListHeader Header;

   uListHeaderGet( List, &Header);
   // space for the item :
   _ItemInsert( List, Index, Header.Count);
   // save new item :
   uListItemSave( List, Index, Item);
   // update count :
   Header.Count++;
   uListHeaderSet( List, &Header);
} // uListInsert

//------------------------------------------------------------------------------
//   Delete
//------------------------------------------------------------------------------

void uListDelete( UList *List, UListIndex Index)
// Delete item at <Index> from <List>
{
UListHeader Header;

   uListHeaderGet( List, &Header);
   _ItemRemove( List, Index, Header.Count);
   // update count :
   Header.Count--;
   uListHeaderSet( List, &Header);
} // uListDelete

//------------------------------------------------------------------------------
//   Get
//------------------------------------------------------------------------------

void uListItemLoad( UList *List, UListIndex Index, void *Item)
// Load <Item> from <Index> of <List>
{
   FileLoad( &List->File, _ItemAddress( List, Index), Item, List->Descriptor->ItemSize);
} // uListItemLoad

//------------------------------------------------------------------------------
//   Set
//------------------------------------------------------------------------------

void uListItemSave( UList *List, UListIndex Index, void *Item)
// Save <Item> at <Index> of <List>
{
TNvmAddress Address;

   Address = _ItemAddress( List, Index);
   if( FileMatch( &List->File, Address, Item, List->Descriptor->ItemSize)){
      return;                          // don't save - same data
   }
   FileSave( &List->File, Address, Item, List->Descriptor->ItemSize);
} // uListSave

//------------------------------------------------------------------------------
//   Get Header
//------------------------------------------------------------------------------

TYesNo uListHeaderGet( UList *List, UListHeader *Header)
// Returns <List> <Header>
{
   if(!FileLoad( &List->File, 0, Header, sizeof( UListHeader))) {
      return NO;
   }
   if(!_HeaderCheck(List->Descriptor, Header)) {
      return NO;
   }
   return YES;
} // uListHeaderGet

//------------------------------------------------------------------------------
//   Set Header
//------------------------------------------------------------------------------

TYesNo uListHeaderSet( UList *List, const UListHeader *Header)
// Sets <List> <Header>
{
   if( FileMatch( &List->File, 0, Header, sizeof( UListHeader))){
      return YES;                          // don't save - same data
   }
   return FileSave( &List->File, 0, Header, sizeof( UListHeader));
} // uListHeaderSet

//------------------------------------------------------------------------------
//   Count
//------------------------------------------------------------------------------

int uListCount( UList *List)
// Returns <List> items count
{
   return( FileByteRead( &List->File, 0 + offsetof( UListHeader, Count)));
} // uListCount

//------------------------------------------------------------------------------
//   Capacity
//------------------------------------------------------------------------------

int uListCapacity( const UListDescriptor *Descriptor)
// Returns <List> capacity
{
   return( Descriptor->Capacity);
} // uListCapacity

//******************************************************************************

//------------------------------------------------------------------------------
//   Item address
//------------------------------------------------------------------------------

static TNvmAddress _ItemAddress( UList *List, UListIndex Index)
// Calculate item address by <Index>
{
   return( sizeof( UListHeader) + Index * List->Descriptor->ItemSize);
} // _ItemAddress

//------------------------------------------------------------------------------
//   Append item
//------------------------------------------------------------------------------

static void _ItemAppend( UList *List, UListIndex Index, void *Item)
// Append item at <Index>
{
   FileSave( &List->File, _ItemAddress( List, Index), Item, List->Descriptor->ItemSize);
} // _ItemInsert

//------------------------------------------------------------------------------
//   Insert
//------------------------------------------------------------------------------

static void _ItemInsert( UList *List, UListIndex Index, UListIndex Count)
// Insert space at <Index>
{
int Size;

   Size  = (Count + 1) - (Index + 1);       // use items count with the new item
   Size *= List->Descriptor->ItemSize;
   if( Size){
      FileMove( &List->File, _ItemAddress( List, Index + 1), _ItemAddress( List, Index), Size);
   }
} // _ItemInsert

//------------------------------------------------------------------------------
//   Remove item
//------------------------------------------------------------------------------

static void _ItemRemove( UList *List, UListIndex Index, UListIndex Count)
// Remove space at <Index>
{
int Size;

   Size  = Count - (Index + 1);        // items count of remaining items
   Size *= List->Descriptor->ItemSize;
   if( Size){
      // move items up :
      FileMove( &List->File, _ItemAddress( List, Index), _ItemAddress( List, Index + 1), Size);
   }
   _ItemClear( List, Count - 1);       // clear last moved item
} // _ItemRemove

//------------------------------------------------------------------------------
//   Clear item
//------------------------------------------------------------------------------

static void _ItemClear( UList *List, UListIndex Index)
// Clear <List> item at <Index>
{
   FileFill( &List->File, _ItemAddress( List, Index), 0xFF, List->Descriptor->ItemSize);
} // _ItemClear

//------------------------------------------------------------------------------
//   Header check
//------------------------------------------------------------------------------

static TYesNo _HeaderCheck( const UListDescriptor *Descriptor, UListHeader *Header)
// Check header
{
   if( Header->Capacity != Descriptor->Capacity ||
      Header->ItemSize != Descriptor->ItemSize){
      return NO;
   }
   return YES;
} // _HeaderCheck
