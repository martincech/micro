//*****************************************************************************
//
//    uFifoDef.h   FIFO data description
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __uFifoDef_H__
   #define __uFifoDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __File_H__
   #include "Memory/File.h"
#endif

//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#define UFIFO_POINTER_SIZE   sizeof( UFifoPointer)

//------------------------------------------------------------------------------
//  Data types
//------------------------------------------------------------------------------

// FIFO memory address :
typedef dword UFifoAddress;

// FIFO item index :
typedef dword UFifoIndex;

// FIFO item size :
typedef word  UFifoItemSize;

//------------------------------------------------------------------------------
//  Configuration
//------------------------------------------------------------------------------

typedef struct {
   UFifoAddress  Pointer;              // FIFO pointers address
   UFifoIndex    Capacity;             // maximum items count
   UFifoItemSize Size;                 // item size
   byte          Overwrite;            // enable items overwrite - TYesNo
   byte          Filename;
   byte          PointerFilename;
} UFifoConfiguration;

//------------------------------------------------------------------------------
//  Pointers
//------------------------------------------------------------------------------

typedef struct {
   UFifoIndex Start;                   // FIFO start index
   UFifoIndex End;                     // FIFO end index
   UFifoIndex Count;                   // FIFO items count
   UFifoIndex StartMoved;
   word       Id;
} UFifoPointer;

//------------------------------------------------------------------------------
//  Descriptor
//------------------------------------------------------------------------------

typedef struct {
   UFifoPointer              Pointer;       // FIFO pointers
   const UFifoConfiguration *Configuration; // FIFO configuration
} UFifoDescriptor;

//------------------------------------------------------------------------------
//  Fifo
//------------------------------------------------------------------------------

typedef struct {
   UFifoDescriptor *Descriptor;
   TFile File;
} UFifo;

#endif
