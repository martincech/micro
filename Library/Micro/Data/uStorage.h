//*****************************************************************************
//
//    uStorage.h    Persistent storage utility
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __uStorage_H__
   #define __uStorage_H__

#ifndef __uStorageDef_H__
   #include "Data/uStorageDef.h"
#endif   

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------
TYesNo uStorageCopy( const UStorageDescriptor *Descriptor, TFileName FilenameDest, TFileName FilenameSrc);
// Load from remote device

void uStorageInit( const UStorageDescriptor *Descriptor, TFileName FileName);
// Initialize <Storage>

TYesNo uStorageOpen( UStorage *Storage, const UStorageDescriptor *Descriptor, TFileName FileName);
// Open

void uStorageClose( UStorage *Storage);
// Close

TYesNo uStorageCheck( UStorage *Storage);
// Check persistent data integrity

void uStorageRestore( UStorage *Storage);
// restores persistent data

TYesNo uStorageLoad( UStorage *Storage);
// Load all persistent data

TYesNo uStorageSave( UStorage *Storage);
// Save all persistent data

void uStorageDefault(const UStorageDescriptor *Descriptor);
// Initialize all persistent data with defaults

//------------------------------------------------------------------------------

TYesNo uStorageItemLoad( UStorage *Storage, UStorageIndex Index);
// Load persistent data at item <Index>

TYesNo uStorageItemSave( UStorage *Storage, UStorageIndex Index);
// Save persistent data at item <Index>

void uStorageItemDefault( const UStorageDescriptor *Descriptor, UStorageIndex Index);
// Initialize persistent data at item <Index> with defaults

TYesNo uStorageItemChanged( UStorage *Storage, UStorageIndex Index);
// Returns YES if persistent data don't match actual data

#endif
