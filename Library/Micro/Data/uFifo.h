//*****************************************************************************
//
//    uFifo.h      FIFO storage utility
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __uFifo_H__
   #define __uFifo_H__

#ifndef __uFifoDef_H__
   #include "Data/uFifoDef.h"
#endif   

//------------------------------------------------------------------------------
//  Declaration
//------------------------------------------------------------------------------

#define uFifoAlloc( Name, PointerAddress, Capacity, ItemSize, Overwrite, FileName, PointerFilename) \
   static const UFifoConfiguration Name = {\
      PointerAddress,\
      Capacity,\
      ItemSize,\
      Overwrite,\
      FileName,\
      PointerFilename\
   };

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

TYesNo uFifoCopy( const UFifoConfiguration *ConfigurationDest, const UFifoConfiguration *ConfigurationSrc);
// Copy fifo

void uFifoInit( UFifoDescriptor *FifoDescriptor, const UFifoConfiguration *Configuration);
// Initialize

TYesNo uFifoOpen( UFifo *Fifo, UFifoDescriptor *FifoDescriptor);
// Open <Fifo> with descriptor <FifoDescriptor>

void uFifoClose( UFifo *Fifo);
// Open <Fifo>

void uFifoClear( UFifo *Fifo);
// Clear - delete all items

UFifoIndex uFifoCount( UFifoDescriptor *FifoDescriptor);
// Returns current items count

TYesNo uFifoAppend( UFifo *Fifo, void *Data);
// Append <Data> at end

TYesNo uFifoRemove( UFifo *Fifo, void *Data);
// Read & remove first <Data> item

TYesNo uFifoGet( UFifo *Fifo, UFifoIndex Index, void *Data);
// Returns <Data> at <Index>

TYesNo uFifoEmpty( UFifoDescriptor *FifoDescriptor);
// Returns YES on empty <Fifo>

TYesNo uFifoFull( UFifoDescriptor *FifoDescriptor);
// Returns YES on <Fifo> full

#endif
