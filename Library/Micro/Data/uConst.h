//*****************************************************************************
//
//    uConst.h     Constant data handling
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __uConst_H__
   #define __uConst_H__

   #include <string.h>
  
   #define UCONST_MEMORY
   #define uConstDeclare( c)           const c
   #define uConstByteGet( c)           c
   #define uConstWordGet( c)           c
   #define uConstDwordGet( c)          c
   #define uConstCopy( dst, src, size) memcpy( dst, src, size)
#endif

