//*****************************************************************************
//
//    uDirectory.c  Directory utility
//    Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#include "Data/uDirectory.h"
#include "Memory/Nvm.h"
#include <string.h>


// Rem: maybe replace uDirectoryHeaderGet & change count uDirectoryHeaderSet
// with uDirectoryCount & _CountSet local function

// Local functions :

static TNvmAddress _ItemAddress( UDirectoryIndex Index);
// Calculate item address by <Index>

static void _ItemRead( UDirectory *Directory, UDirectoryIndex Index, UDirectoryItem *Item);
// Read <Directory> <Item> at <Index>

static void _ItemWrite( UDirectory *Directory, UDirectoryIndex Index, const UDirectoryItem *Item);
// Write <Directory> <Item> to <Index>

static void _ItemClear( UDirectory *Directory, UDirectoryIndex Index);
// Clear <Directory> item at <Index>

static void _ItemInsert( UDirectory *Directory, UDirectoryIndex Index, UDirectoryIndex Count);
// Insert space at <Index>

static void _ItemRemove( UDirectory *Directory, UDirectoryIndex Index, UDirectoryIndex Count);
// Remove space at <Index>

static UDirectoryIndex _ItemSearch( UDirectory *Directory, const char *Name, UDirectoryIndex Count);
// Returns nearest position for <Name>

static TYesNo _HeaderCheck( const UDirectoryDescriptor *Descriptor, UDirectoryHeader *Header);
// Check header

static TYesNo _uDirectoryOpen( UDirectory *Directory, const UDirectoryDescriptor *Descriptor, TFileName Filename);
// Open

TYesNo uDirectoryCopy( UDirectory *DirectoryDest, UDirectory *DirectorySrc)
// Load from remote device
{
UDirectoryHeader Header;
TFileAddress Address;
dword Size;

   if(!uDirectoryHeaderGet( DirectorySrc, &Header)) {
      return NO;
   }

   Address = _ItemAddress( 0);
   Size = Header.Capacity * sizeof(UDirectoryItem);

   if(!FileClone( &DirectoryDest->File, &DirectorySrc->File, Address, Size)) {
      return NO;
   }

   if(!uDirectoryHeaderSet( DirectoryDest, &Header)) {
      return NO;
   }

   return YES;
} // uDirectoryCopy
/*
//------------------------------------------------------------------------------
//  Remote load
//------------------------------------------------------------------------------

TYesNo uDirectoryRemoteLoad( UDirectoryDescriptor *Descriptor)
// Load from remote device
{
TFile File;
UDirectory DirectoryInt;
UDirectory *Directory = &DirectoryInt;
UDirectoryHeader Header;
TFileAddress Address;
dword Size;
dword FragmentSize;
byte Buffer[BUFFER_SIZE];

   Directory->Descriptor = Descriptor;

   if(!FileRemoteOpen(&File, Descriptor->Filename, FILE_MODE_READ_WRITE)) {
      return NO;
   }
   if(!FileRemoteLoad( &File, 0, &Header, sizeof( UDirectoryHeader))) {
      FileRemoteClose(&File);
      return NO;
   }
   if(!_HeaderCheck( Descriptor, &Header)) {
      FileRemoteClose(&File);
      return NO;
   }

   if(!uDirectoryOpen( Directory, Descriptor)) {
      FileRemoteClose(&File);
      return NO;
   }

   Address = _ItemAddress( 0);
   Size = Header.Capacity * sizeof(UDirectoryItem);

   while(Size) {
      if(Size > BUFFER_SIZE) {
         FragmentSize = BUFFER_SIZE;
      } else {
         FragmentSize = Size;
      }
      if(!FileRemoteLoad( &File, Address, Buffer, FragmentSize)) {
         uDirectoryClose( Directory);
         FileRemoteClose(&File);
         return NO;
      }
      if(!FileSave( &Directory->File, Address, Buffer, FragmentSize)) {
         uDirectoryClose( Directory);
         FileRemoteClose(&File);
         return NO;
      }
      Address += FragmentSize;
      Size -= FragmentSize;
   }

   uDirectoryHeaderSet( Directory, &Header);
   uDirectoryClose( Directory);
   FileRemoteClose(&File);
   return YES;
} // uDirectoryRemoteLoad
*/
//------------------------------------------------------------------------------
//  Initialization
//------------------------------------------------------------------------------

void uDirectoryInit( UDirectory *Directory)
// Initialize <Directory>
{
UDirectoryHeader Header;

   uDirectoryHeaderGet( Directory, &Header);
   if( _HeaderCheck(Directory->Descriptor, &Header)){
      return;
   }
   // create default :
   memset( &Header, 0, sizeof( UDirectoryHeader));
   Header.Capacity = Directory->Descriptor->Capacity;
   Header.ItemSize = Directory->Descriptor->ItemSize;
   uDirectoryHeaderSet( Directory, &Header);
   FileFill( &Directory->File, _ItemAddress( 0), 0xFF,
            Directory->Descriptor->Capacity * sizeof( UDirectoryItem));
} // uDirectoryInit

//------------------------------------------------------------------------------
//   Add
//------------------------------------------------------------------------------

UDirectoryIndex uDirectoryAdd( UDirectory *Directory, const char *Name, UDirectoryIdentifier Identifier)
// Add item with <Name> and <Index> to <Directory>
{
UDirectoryHeader Header;
UDirectoryItem   Item;
UDirectoryIndex  Index;

   uDirectoryHeaderGet( Directory, &Header);
   // search a position for the item :
   Index = _ItemSearch( Directory, Name, Header.Count);
   _ItemInsert( Directory, Index, Header.Count);
   // save new item :
   memset( &Item, 0, sizeof( UDirectoryItem));
   strncpyx( Item.Name, Name, UDIRECTORY_NAME_SIZE);
   Item.Identifier = Identifier;
   _ItemWrite( Directory, Index, &Item);
   // update count :
   Header.Count++;
   uDirectoryHeaderSet( Directory, &Header);
   return( Index);
} // uDirectoryAdd

//------------------------------------------------------------------------------
//   Delete
//------------------------------------------------------------------------------

void uDirectoryDelete( UDirectory *Directory, UDirectoryIndex Index)
// Delete item at <Index> from <Directory>
{
UDirectoryHeader Header;

   uDirectoryHeaderGet( Directory, &Header);
   _ItemRemove( Directory, Index, Header.Count);
   // update count :
   Header.Count--;
   uDirectoryHeaderSet( Directory, &Header);
} // uDirectoryDelete

//------------------------------------------------------------------------------
//   Rename
//------------------------------------------------------------------------------

UDirectoryIndex uDirectoryRename( UDirectory *Directory, UDirectoryIndex Index, const char *NewName)
// Rename item at <Index> with <NewName>
{
UDirectoryItem  Item;
UDirectoryIndex Count;

   Count = uDirectoryCount( Directory);
   // read and rename item :
   _ItemRead( Directory, Index, &Item);
   memset( Item.Name, 0, UDIRECTORY_NAME_SIZE + 1);
   strncpyx( Item.Name, NewName, UDIRECTORY_NAME_SIZE);
   // remove item :
   _ItemRemove( Directory, Index, Count);
   Count--;
   // search a position for the item :
   Index = _ItemSearch( Directory, Item.Name, Count);
   _ItemInsert( Directory, Index, Count);
   _ItemWrite( Directory, Index, &Item);
   // count is the same, don't modify header
   return( Index);
} // uDirectoryRename


//------------------------------------------------------------------------------
//   Get Header
//------------------------------------------------------------------------------

TYesNo uDirectoryHeaderGet( UDirectory *Directory, UDirectoryHeader *Header)
// Returns <Directory> <Header>
{
   if(!FileLoad( &Directory->File, 0, Header, sizeof( UDirectoryHeader))) {
      return NO;
   }
   if(!_HeaderCheck( Directory->Descriptor, Header)) {
      return NO;
   }
   return YES;
} // uDirectoryHeaderGet

//------------------------------------------------------------------------------
//   Set Header
//------------------------------------------------------------------------------

TYesNo uDirectoryHeaderSet( UDirectory *Directory, const UDirectoryHeader *Header)
// Sets <Directory> <Header>
{
   if( FileMatch(  &Directory->File, 0, Header, sizeof( UDirectoryHeader))){
      return YES;                          // don't save - same data
   }
   FileSave( &Directory->File, 0, Header, sizeof( UDirectoryHeader));
   return YES;
} // uDirectoryHeaderSet

//------------------------------------------------------------------------------
//   Count
//------------------------------------------------------------------------------

int uDirectoryCount( UDirectory *Directory)
// Returns <Directory> items count
{
   return( FileByteRead( &Directory->File, offsetof( UDirectoryHeader, Count)));
} // uDirectoryCount

//------------------------------------------------------------------------------
//   Capacity
//------------------------------------------------------------------------------

int uDirectoryCapacity( const UDirectoryDescriptor *Descriptor)
// Returns <Directory> capacity
{
   return( Descriptor->Capacity);
} // uDirectoryCapacity

//------------------------------------------------------------------------------
//   Item name
//------------------------------------------------------------------------------

void uDirectoryItemName( UDirectory *Directory, UDirectoryIndex Index, char *Name)
// Returns <Name> of directory item at <Index>
{
   FileLoad( &Directory->File, _ItemAddress( Index) + offsetof( UDirectoryItem, Name), Name, UDIRECTORY_NAME_SIZE + 1);
} // uDirectoryItemName

//------------------------------------------------------------------------------
//   Index
//------------------------------------------------------------------------------

UDirectoryIdentifier uDirectoryItemIdentifier( UDirectory *Directory, UDirectoryIndex Index)
// Returns list identifier of directory item at <Index>
{
   return( FileByteRead( &Directory->File, _ItemAddress( Index) + offsetof( UDirectoryItem, Identifier)));
} // uDirectoryItemIdentifier

//------------------------------------------------------------------------------
//   Find
//------------------------------------------------------------------------------

UDirectoryIndex uDirectoryFind( UDirectory *Directory, const char *Name)
// Find <Directory> item by <Name>
{
int  i, Count;
char ItemName[ UDIRECTORY_NAME_SIZE + 1];

   Count = uDirectoryCount( Directory);
   for( i = 0; i < Count; i++){
      uDirectoryItemName( Directory, i, ItemName);
      if( strequ( ItemName, Name)){
         return( i);
      }
   }
   return( UDIRECTORY_INDEX_INVALID);
} // uDirectoryFind

//------------------------------------------------------------------------------
//   Find index
//------------------------------------------------------------------------------

UDirectoryIndex uDirectoryFindIdentifier( UDirectory *Directory, UDirectoryIdentifier Identifier)
// Find <Directory> item by <Identifier>
{
int i, Count;

   if( Identifier == ULIST_IDENTIFIER_INVALID){
      return( UDIRECTORY_INDEX_INVALID);
   }
   Count = uDirectoryCount( Directory);
   for( i = 0; i < Count; i++){
      if( uDirectoryItemIdentifier( Directory, i) == Identifier){
         return( i);
      }
   }
   return( UDIRECTORY_INDEX_INVALID);
} // uDirectoryFindIndex

//------------------------------------------------------------------------------
//   Free index
//------------------------------------------------------------------------------

UDirectoryIdentifier uDirectoryIdentifierFree( UDirectory *Directory)
// Find first free list identifier
{
UDirectoryHeader Header;
UDirectoryIdentifier  Identifier;
int              i;

   uDirectoryHeaderGet( Directory, &Header);
   Identifier = Header.Last;
   if( Identifier < Directory->Descriptor->Capacity){
      // capacity not reached
      Header.Last++;
      uDirectoryHeaderSet( Directory, &Header);
      return( Identifier);
   }
   // search for deleted item :
   for( i = 0; i < Directory->Descriptor->Capacity; i++){
      if( uDirectoryFindIdentifier( Directory, i) == ULIST_IDENTIFIER_INVALID){
         return( i);
      }
   }
   return( Directory->Descriptor->Capacity - 1);
} // uDirectoryIdentifierFree

//------------------------------------------------------------------------------
//   Directory address next
//------------------------------------------------------------------------------

UDirectoryAddress uDirectoryAddressNext( const UDirectoryDescriptor *Descriptor)
// Returns <Directory> address after directory data
{
   return( uDirectorySize( Descriptor->Capacity));
} // uDirectoryAddressNext

//******************************************************************************

//------------------------------------------------------------------------------
//   Item address
//------------------------------------------------------------------------------

static TNvmAddress _ItemAddress( UDirectoryIndex Index)
// Calculate item address by <Index>
{
   return( sizeof( UDirectoryHeader) + Index * sizeof( UDirectoryItem));
} // _ItemAddress

//------------------------------------------------------------------------------
//   Read item
//------------------------------------------------------------------------------

static void _ItemRead( UDirectory *Directory, UDirectoryIndex Index, UDirectoryItem *Item)
// Read <Directory> <Item> at <Index>
{
   FileLoad( &Directory->File, _ItemAddress( Index), Item, sizeof( UDirectoryItem));
} // _ItemRead

//------------------------------------------------------------------------------
//   Write item
//------------------------------------------------------------------------------

static void _ItemWrite( UDirectory *Directory, UDirectoryIndex Index, const UDirectoryItem *Item)
// Write <Directory> <Item> to <Index>
{
   if( FileMatch( &Directory->File, _ItemAddress( Index), Item, sizeof( UDirectoryItem))){
      return;                          // don't save - same data
   }
   FileSave( &Directory->File, _ItemAddress( Index), Item, sizeof( UDirectoryItem));
} // _ItemWrite

//------------------------------------------------------------------------------
//   Clear item
//------------------------------------------------------------------------------

static void _ItemClear( UDirectory *Directory, UDirectoryIndex Index)
// Clear <Directory> item at <Index>
{
   FileFill( &Directory->File, _ItemAddress( Index), 0xFF, sizeof( UDirectoryItem));
} // _ItemClear

//------------------------------------------------------------------------------
//   Insert item
//------------------------------------------------------------------------------

static void _ItemInsert( UDirectory *Directory, UDirectoryIndex Index, UDirectoryIndex Count)
// Insert space at <Index>
{
int Size;

   Size  = (Count + 1) - (Index + 1);       // use items count with the new item
   Size *= sizeof( UDirectoryItem);
   if( Size){
      FileMove( &Directory->File, _ItemAddress( Index + 1), _ItemAddress( Index), Size);
   }
} // _ItemInsert

//------------------------------------------------------------------------------
//   Remove item
//------------------------------------------------------------------------------

static void _ItemRemove( UDirectory *Directory, UDirectoryIndex Index, UDirectoryIndex Count)
// Remove space at <Index>
{
int Size;

   Size  = Count - (Index + 1);             // items count of remaining items
   Size *= sizeof( UDirectoryItem);
   if( Size){
      // move items up :
      FileMove( &Directory->File, _ItemAddress( Index), _ItemAddress( Index + 1), Size);
   }
   _ItemClear( Directory, Count - 1);       // clear last moved item
} // _ItemRemove

//------------------------------------------------------------------------------
//   Search item
//------------------------------------------------------------------------------

static UDirectoryIndex _ItemSearch( UDirectory *Directory, const char *Name, UDirectoryIndex Count)
// Returns nearest position for <Name>
{
int  i;
char ItemName[ UDIRECTORY_NAME_SIZE + 1];

   for( i = 0; i < Count; i++){
      uDirectoryItemName( Directory, i, ItemName);
      if( strcmp( ItemName, Name) > 0){
         return( i);
      }
   }
   return( Count);
} // _ItemSearch

//------------------------------------------------------------------------------
//   Header check
//------------------------------------------------------------------------------

static TYesNo _HeaderCheck( const UDirectoryDescriptor *Descriptor, UDirectoryHeader *Header)
// Check header
{
   if( Header->Capacity != Descriptor->Capacity ||
      Header->ItemSize != Descriptor->ItemSize){
      return NO;
   }
   return YES;
} // _HeaderCheck
