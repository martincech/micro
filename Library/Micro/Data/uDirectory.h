//*****************************************************************************
//
//    uDirectory.h  Directory utility
//    Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __uDirectory_H__
   #define __uDirectory_H__

#ifndef __uDirectoryDef_H__
   #include "Data/uDirectoryDef.h"
#endif   

//------------------------------------------------------------------------------
//  Declaration
//------------------------------------------------------------------------------

#define uDirectoryAlloc( Name, Capacity, ItemSize) \
   const UDirectoryDescriptor Name = {\
      Capacity,\
      ItemSize\
   };

#define uDirectorySize( Capacity) \
                      (sizeof( UDirectoryHeader) + (Capacity) * sizeof( UDirectoryItem))

#ifdef __cplusplus
   extern "C" {
#endif

TYesNo uDirectoryCopy(UDirectory *DirectoryDest, UDirectory *DirectorySrc);
   // Load from remote device
//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void uDirectoryInit( UDirectory *Directory);
// Initialize <Directory>

UDirectoryIndex uDirectoryAdd( UDirectory *Directory, const char *Name, UDirectoryIdentifier Identifier);
// Add item with <Name> and <Index> to <Directory>

void uDirectoryDelete( UDirectory *Directory, UDirectoryIndex Index);
// Delete item at <Index> from <Directory>

UDirectoryIndex uDirectoryRename( UDirectory *Directory, UDirectoryIndex Index, const char *NewName);
// Rename item at <Index> with <NewName>, returns new index

//------------------------------------------------------------------------------

TYesNo uDirectoryHeaderGet( UDirectory *Directory, UDirectoryHeader *Header);
// Returns <Directory> <Header>

TYesNo uDirectoryHeaderSet( UDirectory *Directory, const UDirectoryHeader *Header);
// Sets <Directory> <Header>

int uDirectoryCount( UDirectory *Directory);
// Returns <Directory> items count

int uDirectoryCapacity( const UDirectoryDescriptor *Descriptor);
// Returns <Directory> capacity

void uDirectoryItemName( UDirectory *Directory, UDirectoryIndex Index, char *Name);
// Returns <Name> of directory item at <Index>

UDirectoryIdentifier uDirectoryItemIdentifier( UDirectory *Directory, UDirectoryIndex Index);
// Returns list identifier of directory item at <Index>

UDirectoryIndex uDirectoryFind( UDirectory *Directory, const char *Name);
// Find <Directory> item by <Name>

UDirectoryIndex uDirectoryFindIdentifier( UDirectory *Directory, UDirectoryIdentifier Identifier);
// Find <Directory> item by <Identifier>

UDirectoryIdentifier uDirectoryIdentifierFree( UDirectory *Directory);
// Find first free list identifier

UDirectoryAddress uDirectoryAddressNext( const UDirectoryDescriptor *Descriptor);
// Returns <Directory> address after directory data

#ifdef __cplusplus
   }
#endif

#endif
