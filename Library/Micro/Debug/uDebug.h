//******************************************************************************
//
//    uDebug.h      Debugger utility
//    Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __uDebug_H__
   #define __uDebug_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif

void dbgPrintf( const char *Format,...);
// Debug printf

int dbgPutchar( int Ch);
// Debug putchar

#ifdef __UDEBUG__
   #define TRACE_INIT()
   #define TRACE(   msg)      dbgPrintf( "%s\n",     msg);
   #define TRACEC(  msg, c)   dbgPrintf( "%s %c\n",  msg, c);
   #define TRACES(  msg, s)   dbgPrintf( "%s %s\n",  msg, s);
   #define TRACED(  msg, n)   dbgPrintf( "%s %d\n",  msg, n);
   #define TRACEX(  msg, n)   dbgPrintf( "%s %X\n",  msg, n);
   #define TRACEXX( msg, n)   dbgPrintf( "%s %lX\n", msg, n);
#else // nodebug
   #define TRACE_INIT()
   #define TRACE( msg)
   #define TRACEC(  msg, c)
   #define TRACES(  msg, s)
   #define TRACED( msg, n)
   #define TRACEX( msg, n)
   #define TRACEXX( msg, n)
#endif

#ifdef __cplusplus
   }
#endif

#endif
