//******************************************************************************
//
//   uTime.c      Time utilities
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "uTime.h"

//------------------------------------------------------------------------------
// Encode
//------------------------------------------------------------------------------

UTimeGauge uTimeGauge( UTime *Time)
// Encode <Time> to internal representation
{
UTimeGauge TimeGauge;

   TimeGauge  = (byte)Time->Sec   * TIME_SEC;
   TimeGauge += (word)Time->Min   * TIME_MIN;
   TimeGauge += (dword)Time->Hour * TIME_HOUR;
   return( TimeGauge);
} // uTimeGauge

//------------------------------------------------------------------------------
// Decode
//------------------------------------------------------------------------------

void uTime( UTime *Time, UTimeGauge TimeGauge)
// Decode <Time> from <TimeGauge>
{
   Time->Sec   = TimeGauge % 60;       // seconds
   TimeGauge  /= 60;                   // remove seconds
   Time->Min   = TimeGauge % 60;       // minutes
   TimeGauge  /= 60;                   // remove minutes
   Time->Hour  = TimeGauge % 24;       // hours
} // uTime

//------------------------------------------------------------------------------
// Validity
//------------------------------------------------------------------------------

ETimeError uTimeValid( UTime *Time)
// Check for time validity
{
   if( Time->Sec > 59){
      return( TIME_WRONG_SEC);
   }
   if( Time->Min > 59){
      return( TIME_WRONG_MIN);
   }
   if( Time->Hour > 23){
#ifdef TIME_ACCEPT_MIDNIGHT   
      if( (Time->Hour == 24) && (Time->Min == 0) && (Time->Sec == 0)){
         return( TIME_OK);
      }
#endif // TIME_ACCEPT_MIDNIGHT      
      return( TIME_WRONG_HOUR);
   }
   return( TIME_OK);
} // uTimeValid

//------------------------------------------------------------------------------
//  Time range
//------------------------------------------------------------------------------

const TTimeRange TimeRangeDefault = {
   /* From */ TIME_RANGE_FROM_DEFAULT,
   /* To */   TIME_RANGE_TO_DEFAULT
};
