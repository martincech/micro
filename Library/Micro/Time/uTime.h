//******************************************************************************
//
//   uTime.h      Time utilities
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __uTime_H__
   #define __uTime_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

//------------------------------------------------------------------------------
// Data types
//------------------------------------------------------------------------------

// Linear time representation (LSB = 1s) :
typedef int32 UTimeGauge;

// Time :
typedef byte UHour;          // 0..23
typedef byte UMin;           // 0..59
typedef byte USec;           // 0..59

// Time structure :
typedef struct
{
   USec  Sec;
   UMin  Min;
   UHour Hour;
} UTime;

// Time range

typedef struct {
   UTimeGauge From;                 // Range from
   UTimeGauge To;                   // Range to
} TTimeRange;

extern const TTimeRange TimeRangeDefault;
#define TIME_RANGE_FROM_DEFAULT  0
#define TIME_RANGE_TO_DEFAULT    (24*3600-1)

//------------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------------

// Linear time orders :
#define TIME_SEC  1
#define TIME_MIN  60
#define TIME_HOUR 3600
#define TIME_DAY  ((UTimeGauge)24 * (UTimeGauge)3600)

// Field errors :
typedef enum {
   TIME_OK,                       // time OK
   TIME_WRONG_SEC,                // wrong second
   TIME_WRONG_MIN,                // wrong minute
   TIME_WRONG_HOUR,               // wrong hour
   _TIME_WRONG_LAST
} ETimeError;


#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
// Functions
//------------------------------------------------------------------------------

UTimeGauge uTimeGauge( UTime *Time);
// Encode <Time> to internal representation

void uTime( UTime *Time, UTimeGauge TimeGauge);
// Decode <Time> from <TimeGauge>

ETimeError uTimeValid( UTime *Time);
// Check for time validity

#ifdef __cplusplus
}
#endif

#endif
