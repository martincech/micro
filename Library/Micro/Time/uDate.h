//******************************************************************************
//
//   uDate.h      Date utilities
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __uDate_H__
   #define __uDate_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

//------------------------------------------------------------------------------
// Data types
//------------------------------------------------------------------------------

// Linear date representation (LSB = 1day) :
typedef int16 UDateGauge;    // days from 1.1.2000

// Date :
typedef byte UYear;          // <Real Year> - 2000
typedef byte UMonth;         // 1..12
typedef byte UDay;           // 1..31

typedef byte UDow;           // day of week 0..6
typedef byte UWeek;          // week of month number 1..5

// Year representation :
#define uYearGet( y)       ((y) + 2000)   // from UYear to display
#define uYearSet( y)       ((y) - 2000)   // from display to UYear

// Date structure :
typedef struct
{
   UDay   Day;
   UMonth Month;
   UYear  Year;
} UDate;

//------------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------------

#define YEAR_MAX  67         // 2067 is maximum year with signed 32bit date&time gauge

// Months :
typedef enum {
   MONTH_JANUARY = 1,
   MONTH_FEBRUARY,
   MONTH_MARCH,
   MONTH_APRIL,
   MONTH_MAY,
   MONTH_JUNE,
   MONTH_JULE,
   MONTH_AUGUST,
   MONTH_SEPTEMBER,
   MONTH_OCTOBER,
   MONTH_NOVEMBER,
   MONTH_DECEMBER,
   _MONTH_COUNT = MONTH_DECEMBER
} EMonth;

// Day of week :
typedef enum {
   DOW_MONDAY,
   DOW_TUESDAY,
   DOW_WEDNESDAY,
   DOW_THURSDAY,
   DOW_FRIDAY,
   DOW_SATURDAY,
   DOW_SUNDAY,
   _DOW_COUNT
} EDayOfWeek;

// Field errors :
typedef enum {
   DATE_OK,                    // date OK
   DATE_WRONG_DAY,             // wrong day
   DATE_WRONG_MONTH,           // wrong month
   DATE_WRONG_YEAR,            // wrong year
   _DATE_WRONG_LAST
} EDateError;

#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
// Functions
//------------------------------------------------------------------------------

UDateGauge uDateGauge( UDate *Date);
// Encode <Date> to internal representation

void uDate( UDate *Date, UDateGauge DateGauge);
// Decode <Date> from <DateGauge>

EDateError uDateValid( UDate *Date);
// Check for date validity

//------------------------------------------------------------------------------
// Calculations
//------------------------------------------------------------------------------

TYesNo uYearLeap( UYear Year);
// Returns YES on leap <Year>

UDay uMonthDays( UYear Year, UMonth Month);
// Returns number of <Month> days in the <Year>

UDow uDateDow( UDateGauge DateGauge);
// Returns day of week of <DateGauge>

#ifdef __cplusplus
}
#endif

#endif
