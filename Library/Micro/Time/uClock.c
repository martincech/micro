//******************************************************************************
//
//   uClock.c  Date & Time with daylight saving
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "uClock.h"
#include "Data/uConst.h"          // Const data

// daylight saving offset :
#define DAYLIGHT_SAVING_BIAS  3600     // internal representation of DST bias

// transition day descriptor :
typedef struct {
   UMonth Month;
   UWeek  Week;
   UDow   Dow;
   UHour  Hour;
   UMin   Min;
} UTransitionDay;

// transition descriptor :
typedef struct {
   UTransitionDay Start;
   UTransitionDay End;
} UDaylightSavingTransition;

// transition table :
static uConstDeclare( UDaylightSavingTransition DaylightSavingTransition[ _DAYLIGHT_SAVING_COUNT - 1]) = {
// EU
{
   { MONTH_MARCH,   5, DOW_SUNDAY, 2, 0},
   { MONTH_OCTOBER, 5, DOW_SUNDAY, 2, 0}
},
// US
{
   { MONTH_MARCH,    2, DOW_SUNDAY, 2, 0},
   { MONTH_NOVEMBER, 1, DOW_SUNDAY, 2, 0}
}
};

// Persistent data :
#ifndef UCLOCK_PARENT // singleton
   uPersistentAlloc( ClockDaylightSavingType);
#endif   

// Local functions :
static UDay TransitionDayGet( UYear Year, const UTransitionDay *Transition);
// Returns transition day of month

//------------------------------------------------------------------------------
// Encode
//------------------------------------------------------------------------------

UClockGauge uClockGauge( UDateTime *DateTime)
// Encode <DateTime> to internal representation
{
UDateTimeGauge DateTimeGauge;

   DateTimeGauge = uDateTimeGauge( DateTime);    // wall clock date & time gauge
   if( !uClockDaylightSaving( DateTime)){
      return( (UClockGauge)DateTimeGauge);       // winter time
   }
   // summer time
   return( (UClockGauge)(DateTimeGauge - DAYLIGHT_SAVING_BIAS));
} // uClockGauge

//------------------------------------------------------------------------------
// Decode
//------------------------------------------------------------------------------

void uClock(UDateTime *DateTime, UClockGauge ClockGauge)
// Decode <DateTime> from <ClockGauge>
{
   uDateTime( DateTime, (UDateTimeGauge)ClockGauge);  // convert as winter time
   if( !uClockDaylightSaving( DateTime)){
      return;                                         // winter time
   }
   ClockGauge += DAYLIGHT_SAVING_BIAS;                // summertime offset
   uDateTime( DateTime, (UDateTimeGauge)ClockGauge);  // convert as summer time
} // uClock

//------------------------------------------------------------------------------
// Convert to Date & Time
//------------------------------------------------------------------------------

UDateTimeGauge uClockDateTime( UClockGauge ClockGauge)
// Returns DateTimeGauge of <ClockGauge>
{
UDateTime DateTime;

   uDateTime( &DateTime, (UDateTimeGauge)ClockGauge); // split to winter time
   if( !uClockDaylightSaving( &DateTime)){
      return( (UDateTimeGauge)ClockGauge);            // winter time
   }
   // summer time
   return( (UDateTimeGauge)(ClockGauge + DAYLIGHT_SAVING_BIAS));
} // uClockDateTime

//------------------------------------------------------------------------------
// Convert from Date & Time
//------------------------------------------------------------------------------

UClockGauge uDateTimeClock( UDateTimeGauge DateTimeGauge)
// Returns ClockGauge of <DateTimeGauge>
{
UDateTime DateTime;

   uDateTime( &DateTime, DateTimeGauge);
   if( !uClockDaylightSaving( &DateTime)){
      return( (UClockGauge)DateTimeGauge);            // winter time
   }
   // summer time
   return( (UClockGauge)(DateTimeGauge - DAYLIGHT_SAVING_BIAS));
} // uDateTimeClock

//------------------------------------------------------------------------------
// Day of week
//------------------------------------------------------------------------------

UDow uClockDow( UClockGauge ClockGauge)
// Returns day of week of <ClockGauge>
{
UDateTimeGauge DateTimeGauge;

   DateTimeGauge = uClockDateTime( ClockGauge);
   return( uDateTimeDow( DateTimeGauge));
} // uClockDow

//------------------------------------------------------------------------------
// Daylight saving
//------------------------------------------------------------------------------

// Remark : Works exactly if <DateTime> is internal representation
// of the date & time (winter time). Works usefull if <Local> is wall clock
// time (in forbidden date/time areas returns invalid results, some
// internal times are inaccessible)

TYesNo uClockDaylightSaving( UDateTime *DateTime)
// Returns YES if the <DateTime> is in summertime
{
byte                  DstType;
int                   StartDay;
const UTransitionDay *Start, *End;

   DstType = uClockDaylightSavingType();    // get persistent value
   if( DstType == DAYLIGHT_SAVING_OFF){
      return( NO);                          // daylight saving mode disabled
   }
   // get transition points :
   DstType--;                          // transition array starts at DstType == 1
   Start = &DaylightSavingTransition[ DstType].Start;
   End   = &DaylightSavingTransition[ DstType].End;
   // check for months out of transition areas :
   if( DateTime->Date.Month < uConstByteGet( Start->Month) || DateTime->Date.Month > uConstByteGet( End->Month)){
      return( NO);       // out of summer time
   }
   if( DateTime->Date.Month > uConstByteGet( Start->Month) && DateTime->Date.Month < uConstByteGet( End->Month)){
      return( YES);      // surely summer time
   }
   // solve transition months :
   if( DateTime->Date.Month == uConstByteGet( Start->Month)){
      // start of summer time :
      StartDay = TransitionDayGet( DateTime->Date.Year, Start);
      if( DateTime->Date.Day > StartDay){
         return( YES);   // day after start
      }
      if( DateTime->Date.Day < StartDay){
         return( NO);    // day before start
      }
      // transition day :
      if( DateTime->Time.Hour > uConstByteGet( Start->Hour)){
         return( YES);   // hour after transition
      }
      if( DateTime->Time.Hour < uConstByteGet( Start->Hour)){
         return( NO);
      }
      // transition hour :
      if( DateTime->Time.Min  >= uConstByteGet( Start->Min)){
         return( YES);   // min after transition
      } else {
         return( NO);    // min before transition
      }
   } else { // Month == DST_END_MONTH
      // end of summertime
      StartDay = TransitionDayGet( DateTime->Date.Year, End);
      if( DateTime->Date.Day > StartDay){
         return( NO);    // day after end
      }
      if( DateTime->Date.Day < StartDay){
         return( YES);   // day before end
      }
      // transition day :
      if( DateTime->Time.Hour > uConstByteGet( End->Hour)){
         return( NO);    // hour after transition
      }
      if( DateTime->Time.Hour < uConstByteGet( End->Hour)){
         return( YES);   // hour before transition
      }
      // transition hour :
      if( DateTime->Time.Min >= uConstByteGet( End->Min)){
         return( NO);    // min after transition
      } else {
         return( YES);   // min before transition
      }
   }
} // uClockDaylightSaving

//******************************************************************************

//------------------------------------------------------------------------------
//  Transition day
//------------------------------------------------------------------------------

static UDay TransitionDayGet( UYear Year, const UTransitionDay *Transition)
// Returns transition day of month
{
UDay       LastDay;
UDay       Day;
UDate      Date;
UDateGauge DateGauge;
UDow       Dow;

   LastDay = uMonthDays( Year, uConstByteGet( Transition->Month));
   Day     = uConstByteGet( Transition->Week) * _DOW_COUNT;    // get end of week day
   if( Day > LastDay){
      Day = LastDay;                                           // month days saturation
   }
   // encode date :
   Date.Year  = Year;
   Date.Month = uConstByteGet( Transition->Month);
   Date.Day   = Day;
   DateGauge  = uDateGauge( &Date);              // date internal representation
   // get day of week :
   Dow        = uDateDow( DateGauge);            // day of week
   if( Dow < uConstByteGet( Transition->Dow)){
      Dow += _DOW_COUNT;                         // transition day is before Dow - rool back one week
   }
   Day  -= Dow;                                  // monday
   Day  += uConstByteGet( Transition->Dow);      // this day of week
   return( Day);
} // TransitionDayGet
