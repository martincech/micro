//******************************************************************************
//
//   uDate.c      Date utilities
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "uDate.h"
#include "Data/uConst.h"          // Const data

// month days :
static uConstDeclare( byte _MDay[12]) = {
  31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

// month days from start of year :
static uConstDeclare( word _YDay[12]) = {
  0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334
};

// helper macros :
#define MonthDay( m)     uConstByteGet( _MDay[ (m) - 1])  // days of month
#define YearDay( m)      uConstWordGet( _YDay[ (m) - 1])  // days from begin of year

// constants :
#define DOW_1_1_2000      DOW_SATURDAY                // dow for 1.1.2000
#define FOUR_YEAR_DAYS    1461                        // days of four years

//------------------------------------------------------------------------------
// Encode
//------------------------------------------------------------------------------

UDateGauge uDateGauge( UDate *Date)
// Encode <Date> to internal representation
{
int  Leaps;                   // count of leap days
int  Tmp;

   Leaps = (Date->Year + 4) / 4;       // 2000 (0) is first leap
   if( uYearLeap( Date->Year) && Date->Month < MONTH_MARCH){
      Leaps--;                         // this year is leap, but low date
   }
   Tmp = Leaps + Date->Day - 1;        // day of month started at 0
   return( Tmp + (UDateGauge)Date->Year * 365 +  YearDay( Date->Month));
} // uDateGauge

//------------------------------------------------------------------------------
// Decode
//------------------------------------------------------------------------------

void uDate( UDate *Date, UDateGauge DateGauge)
// Decode <Date> from <DateGauge>
{
int        Tmp;
UDay       D;
UMonth     M;
UDateGauge Y;
UDateGauge YDay;   // remaining days of year
UDateGauge YDays;  // total days of year

   Tmp  = DateGauge / FOUR_YEAR_DAYS;     // count of four years
   YDay = DateGauge % FOUR_YEAR_DAYS;     // offset by begin of four years
   Y    = Tmp * 4;                   // near low start of four years
   // correct year :
   forever {
      YDays = uYearLeap((UYear)Y) ? 366 : 365;
      if( YDay < YDays){
         break;                      // ofset inside of year
      }
      Y++;                           // next year
      YDay -= YDays;
   }
   Date->Year = (UYear)Y;
   // YDay is day of year, convert to MM:DD
   YDay++;                           // date starts at 1
   if (uYearLeap((UYear)Y)){
      if( YDay > (31 + 29)){
         YDay--;                     // short by leap day
      } else if( YDay == (31 + 29)){
         Date->Day   = 29;           // is leap day now
         Date->Month = MONTH_FEBRUARY;
         return;
      }
   }
   M = MONTH_JANUARY;
   forever {
      D = MonthDay( M);              // days of month
      if( D >= YDay){
         break;
      }
      YDay -= D;                     // substract days of month
      M++;                           // next month
   }
   Date->Month = M;
   Date->Day   = (UDay)YDay;
} // uDate

//------------------------------------------------------------------------------
// Date Validity
//------------------------------------------------------------------------------

EDateError uDateValid( UDate *Date)
// Check for date validity
{
UDay LastDay;

   // check for date :
   if( Date->Month < MONTH_JANUARY || Date->Month > MONTH_DECEMBER){
      return( DATE_WRONG_MONTH);
   }
   if( Date->Year > YEAR_MAX){
      return( DATE_WRONG_YEAR);
   }
   LastDay = uMonthDays( Date->Year, Date->Month);
   if( Date->Day < 1 || Date->Day > LastDay){
      return( DATE_WRONG_DAY);
   }
   return( DATE_OK);
} // uDateValid

//------------------------------------------------------------------------------
// Leap Year
//------------------------------------------------------------------------------

TYesNo uYearLeap( UYear Year)
// Returns YES on leap <Year>
{
   if( (Year & 0x03) != 0){
      return( NO);                     // year undivisible by 4 is not leap
   }
   // Year divisible by 1000 is leap
   // Year divisible by 100  is not leap
   // Other years divisible by 4 are leap
   return( YES);
} // uYearLeap

//------------------------------------------------------------------------------
// Month days
//------------------------------------------------------------------------------

UDay uMonthDays( UYear Year, UMonth Month)
// Returns number of <Month> days in the <Year>
{
   if( Month == MONTH_FEBRUARY && uYearLeap( Year)){
      // february & leap year
      return( 29);
   } // other months, inclusive normal february
   return( MonthDay( Month));
} // uMonthDays

//------------------------------------------------------------------------------
// Day of week
//------------------------------------------------------------------------------

UDow uDateDow( UDateGauge DateGauge)
// Returns day of week of <DateGauge>
{
   return( (UDow)((DateGauge + DOW_1_1_2000) % 7));
} // uDow

