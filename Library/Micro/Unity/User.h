//*****************************************************************************
//
//    User.h        User interface for Unity
//    Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#ifndef __User_H__
#define __User_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

void UserConfirm( void);
// Prompts user to confirm previous action

void UserContinue( const char *Message);
// Continue when user press anything + \n

#endif