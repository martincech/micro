//******************************************************************************
//
//   Crc.h     Crc 16 calculation
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Crc_H__
#define __Crc_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif


word CalculateCrc16( byte *Buffer, word BufferLen, word Residue);
// calculate 16 with initial value set to Residue

byte CalculateXorLrc( byte *Buffer, char BufferLen, TYesNo FinalXor);
// calculate LRC as xor of bytes with additional xoring by 0xFF(if FinalXor set)
#ifdef __cplusplus
   }
#endif

#endif // __Crc_H__


