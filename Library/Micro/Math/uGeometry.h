//******************************************************************************
//
//   uGeometry.h   Geometry functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __uGeometry_H__
   #define __uGeometry_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

typedef int16 UCoordinateShort;
typedef int32 UCoordinate;
typedef int64 UCoordinateLong;

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

UCoordinate uInterpolation( UCoordinate x1, UCoordinate x2,
                            UCoordinate y1, UCoordinate y2,
                            UCoordinate x);
// Returns interpolated Y value for <x>

UCoordinateShort uInterpolationShort( UCoordinateShort x1, UCoordinateShort x2,
                                      UCoordinateShort y1, UCoordinateShort y2,
                                      UCoordinateShort x);
// Returns interpolated Y value for <x>

#endif
