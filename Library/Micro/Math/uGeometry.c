//******************************************************************************
//
//   uGeometry.c   Geometry functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "uGeometry.h"

//------------------------------------------------------------------------------
//   Interpolation
//------------------------------------------------------------------------------

UCoordinate uInterpolation( UCoordinate x1, UCoordinate x2,
                            UCoordinate y1, UCoordinate y2,
                            UCoordinate x)
// Returns interpolated Y value for <x>
{
UCoordinateLong tmp;

   // check for bounds :
   if( x == x1){
      return( y1);
   }
   if( x == x2){
      return( y2);
   }
   // linear interpolation :
   tmp  = (x - x1) * (y2 - y1);
   tmp /= (x2 - x1);
   return( (UCoordinate)tmp + y1);
} // _uInterpolation

//------------------------------------------------------------------------------
//   Short interpolation
//------------------------------------------------------------------------------

UCoordinateShort uInterpolationShort( UCoordinateShort x1, UCoordinateShort x2,
                                      UCoordinateShort y1, UCoordinateShort y2,
                                      UCoordinateShort x)
// Returns interpolated Y value for <x>
{
UCoordinate tmp;

   // check for bounds :
   if( x == x1){
      return( y1);
   }
   if( x == x2){
      return( y2);
   }
   // linear interpolation :
   tmp  = (x - x1) * (y2 - y1);
   tmp /= (x2 - x1);
   return( (UCoordinateShort)tmp + y1);
} // _uInterpolationShort
