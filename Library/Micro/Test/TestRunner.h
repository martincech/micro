//*****************************************************************************
//
//    TestRunner.h    Unity test runner
//    Version 1.0     (c) Veit Electronics
//
//*****************************************************************************

#ifndef __TestRunner_H__
   #define __TestRunner_H__

#ifdef __cplusplus
   extern "C" {
#endif

#include "Unity/unity_fixture.h"

void TestRunner( void);

#ifdef __cplusplus
   }
#endif

#endif
