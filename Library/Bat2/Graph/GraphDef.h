//******************************************************************************
//
//   GraphDef.h  Graph data select
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __GraphDef_H__
   #define __GraphDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __SexDef_H__
   #include "Weight/SexDef.h"
#endif

#define GRAPH_LINE_COUNT 2
//------------------------------------------------------------------------------
//  Graph data lines
//------------------------------------------------------------------------------

typedef enum {
   _GRAPH_DATA_LINES_FIRST,
   GRAPH_DATA_LINES_AVERAGE_WEIGHT = _GRAPH_DATA_LINES_FIRST,
   GRAPH_DATA_LINES_TARGET_WEIGHT,
   GRAPH_DATA_LINES_CV,
   GRAPH_DATA_LINES_COUNT,
   GRAPH_DATA_LINES_UNIFORMITY,
   _GRAPH_DATA_LINES_LAST
} EGraphDataLines;

#define GRAPH_GENDER_MASK                 1u
#define GRAPH_GENDER_SHIFT                15
#define GRAPH_DATA_MASK                   (~((GRAPH_GENDER_MASK << GRAPH_GENDER_SHIFT)))
#define GRAPH_DATA_SHIFT                  0

#define GraphDataSetGender( id, gender)   ((id) = (id & ~(GRAPH_GENDER_MASK << GRAPH_GENDER_SHIFT)) | ((gender == SEX_FEMALE) ? (GRAPH_GENDER_MASK << GRAPH_GENDER_SHIFT) : 0))
#define GraphDataGetGender( id)           (((id) & (GRAPH_GENDER_MASK << GRAPH_GENDER_SHIFT))? SEX_FEMALE : SEX_MALE)

#define GraphDataGetType( id)             ((id) & (GRAPH_DATA_MASK << GRAPH_DATA_SHIFT))
#define GraphDataSetType( id, type)       ((id) = (id & ~(GRAPH_DATA_MASK << GRAPH_DATA_SHIFT)) | (type & (GRAPH_DATA_MASK << GRAPH_DATA_SHIFT)))

typedef int TGraphDataLineType;

//------------------------------------------------------------------------------


#endif
