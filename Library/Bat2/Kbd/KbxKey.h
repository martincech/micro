//*****************************************************************************
//
//    KbxKey.h     Bat2 Platform keyboard codes
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __KbxKey_H__
   #ifndef _MANAGED
   #define __KbxKey_H__
   #endif

#ifndef __Hardware_H__
   #include "Hardware.h"               // keycodes
#endif

// key definition :
#ifdef _MANAGED
namespace Bat2Library{
   public enum class KeyboardKeyE{
#else
typedef enum {
#endif
   K_NULL  = 0,                        // menu & window excluded
   // keyboard :
   K_ENTER = _K_KEYBOARD_BASE,         // Enter
   K_ESC,                              // Esc
   K_0,
   K_1,
   K_2,
   K_UP = K_2,                         // Up arrow
   K_3,
   K_4,
   K_LEFT = K_4,                       // Left arrow
   K_5,
   K_6,
   K_RIGHT = K_6,                      // Right arrow
   K_7,
   K_DELETE = K_7,                     // Delete
   K_8,
   K_DOWN = K_8,                       // Down arrow
   K_9,
   K_BACKSPACE = K_9,                  // Backspace

   K_BOOT,                             // Boot composed key
   // events :
   K_REDRAW,                           // 1s redraw
   
//-------------------------------------------------------------------   
   // system keys/flags :
   K_REPEAT   = 0x2000,                // autorepeat flag
   K_RELEASED = 0x4000,                // key released
   K_ASCII    = 0x8000,                // key as ascii code
#ifndef _MANAGED
   _K_KEY_LAST = 0xFF
} EKeyboardKey;
#else
   };
}
#endif

#ifdef _MANAGED
   #undef _MANAGED
   #include "KbxKey.h"
   #define _MANAGED
#endif


#endif
