//*****************************************************************************
//
//    KbxBat2Rot.c   Bat2 keyboard, matrix
//    Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#include "Kbd/Kbx.h"         // Keyboard matrix
#include "System/System.h"

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void KbxInit( void)
// Initialization
{
   KbdPortInit();
} // KbdInit

//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------
#if KBD_NUMERIC
   int KbxRead( void)
   // Read keyboard
   {
      if( KbdR0()){
         return( K_1);
      }	
      if( KbdR1()){
         return( K_4);		
      }
      if( KbdR2()){
         return( K_7);		
      }
      if( KbdR3()){
         return( K_ESC);		
      }

      KbdC1Set();
      SysUDelay(1);
      if( KbdR0()){
         KbdC1Release();
         return( K_2);
      }
      if( KbdR1()){
         KbdC1Release();
         return( K_5);
      }
      if( KbdR2()){
         KbdC1Release();
         return( K_8);
      }
      if( KbdR3()){
         KbdC1Release();
         return( K_0);
      }
      KbdC1Release();

      KbdC2Set();
      SysUDelay(1);
      if( KbdR0()){	
         KbdC2Release();
         return( K_3);
      }
      if( KbdR1()){
         KbdC2Release();
         return( K_6);
      }
      if( KbdR2()){
         KbdC2Release();
         return( K_9);
      }
      if( KbdR3()){
         KbdC2Release();
         return( K_ENTER);
      }

      KbdC2Release();
      return( K_RELEASED);
   } // KbxRead
#else
   int KbxRead( void)
   // Read keyboard
   {
      if( KbdR0() && KbdR1()){
         return( K_BOOT);
      }	

      if( KbdR0()){
         return( K_ESC);
      }	
      if( KbdR1()){
         return( K_LEFT);		
      }

      KbdC1Set();
      SysUDelay(2);
      if( KbdR0()){
         KbdC1Release();
         return( K_UP);
      }
      if( KbdR1()){
         KbdC1Release();
         return( K_DOWN);
      }
      KbdC1Release();

      KbdC2Set();
      SysUDelay(2);
      if( KbdR0()){	
         KbdC2Release();
         return( K_ENTER);
      }
      if( KbdR1()){
         KbdC2Release();
         return( K_RIGHT);
      }
      KbdC2Release();

      return( K_RELEASED);
   } // KbxRead
#endif