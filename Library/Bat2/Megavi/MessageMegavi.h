//******************************************************************************
//
//   MessageMegavi.h  Megavi message management
//   Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MessageMegavi_H__
   #define __MessageMegavi_H__

#ifndef __MegaviDef_H__
   #include "Megavi/MegaviDef.h"
#endif


//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------
void MessageMegaviInit( void);
// Initialize

void MessageMegaviSetSmsRequest( TMegaviSmsRequest SmsRequest, char *PhoneNumber);
// Set sms request field to be this one

TMegaviSmsRequest MessageMegaviGetSmsRequest( void);
// Get actual sms request field

TSmsGateSlotNumber MessageMegaviSmsSend( TMegaviSmsTarget SmsTarget, char *SmsText);
// Send sms to SmsTarget

ESmsGateSmsStatus MessageMegaviSmsStatus( TSmsGateSlotNumber SlotNumber);
// Get status of SMS on slot

#endif //__MessageMegavi_H__
