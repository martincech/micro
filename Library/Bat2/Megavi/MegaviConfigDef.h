//******************************************************************************
//
//   MegaviConfigDef.h Megavi messaging options
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MegaviConfigDef_H__
   #ifndef _MANAGED
      #define __MegaviConfigDef_H__
   #endif

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

//------------------------------------------------------------------------------
//  Megavi power options
//------------------------------------------------------------------------------
#ifndef _MANAGED
typedef struct {
   //  ID = (module_number << 4) | module_code where
   //  module_number = RS485 address
   //  module_code = defined by SODALEC, default MEGAVI_MODULE_CODE(0x0A)
   byte                    ID;
} TMegaviModuleOptions;


#define MEGAVI_MEGAVI_MODULE_ADDRESS_MIN 0
#define MEGAVI_MEGAVI_MODULE_ADDRESS_MAX 15
#define MEGAVI_MEGAVI_MODULE_ADDRESS_DEFAULT 1

#define MEGAVI_MEGAVI_MODULE_CODE_MIN 0
#define MEGAVI_MEGAVI_MODULE_CODE_MAX 15
#define MEGAVI_MEGAVI_MODULE_CODE_DEFAULT 10
#endif

#ifdef _MANAGED
   #undef _MANAGED
   #include "MegaviConfigDef.h"
   #define _MANAGED
   namespace Bat2Library{
      public ref class MegaviConfigurationC abstract sealed{
      public:
         literal int MEGAVI_MODULE_ADDRESS_MIN = MEGAVI_MEGAVI_MODULE_ADDRESS_MIN;
         literal int MEGAVI_MODULE_ADDRESS_MAX = MEGAVI_MEGAVI_MODULE_ADDRESS_MAX;
         literal int MEGAVI_MODULE_ADDRESS_DEFAULT = MEGAVI_MEGAVI_MODULE_ADDRESS_DEFAULT;

         literal int MEGAVI_MODULE_CODE_MIN = MEGAVI_MEGAVI_MODULE_CODE_MIN;
         literal int MEGAVI_MODULE_CODE_MAX = MEGAVI_MEGAVI_MODULE_CODE_MAX;
         literal int MEGAVI_MODULE_CODE_DEFAULT = MEGAVI_MEGAVI_MODULE_CODE_DEFAULT;
      };
   }
#endif

#endif // __MegaviConfigDef_H__
