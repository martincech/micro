//******************************************************************************
//
//   MegaviTask.h  Megavi task routines
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************
#ifndef __MegaviTask_H__
   #define __MegaviTask_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __MegaviDef_H__
   #include "Megavi/MegaviDef.h"
#endif

#define MEGAVI_SMS_REQUEST_PERIOD 60  // SmsRequest max validity after setup [s]

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void MegaviTaskInit( int uartPort);
// Initialize

void MegaviTaskFree( void);
// Deinit - free resources

void MegaviTaskExecute( void);
// Execute

void MegaviTaskExecuteSmsRequestClear( void);
// Executive for SmsRequest field time

void MegaviTaskSetSmsRequest( char *PhoneNumber, TMegaviSmsRequest SmsRequest);
// Set sms request field for data reply,
// valid for MEGAVI_SMS_REQUEST_PERIOD s after calling this function or
// until data reply send


#endif // __MegaviTask_H__
