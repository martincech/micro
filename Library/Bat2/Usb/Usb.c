//*****************************************************************************
//
//    Usb.c        Usb
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "Usb.h"
#include "System/System.h"
#include "Usb/UsbCharger.h"
#include "Usb/UsbModule.h" // Low level USB module
#include "Hardware.h"
#include "IAdc/IAdc.h"
#include "Power/Power.h"
#include "Usb/UsbDevice.h"
#include "Remote/SocketIfUsb.h"

typedef enum {
   EDGE_RISING,
   EDGE_FALLING,
   LEVEL_HIGH,
   LEVEL_LOW
} EState;

static void EnableDevice( void);
// Enable device (switch)

static void EnableHost( void);
// Enable device (switch & power)

static void DeinitDeviceIfNeeded( void);
// USB device deinitilization, if needed

static byte _UsbPower( void);
// Usb power state

static byte UsbState;
static TYesNo IsExclusive;
static TYesNo EnumerationEnabled;

//------------------------------------------------------------------------------
//  Initialisation
//------------------------------------------------------------------------------

void UsbInit( void)
// Initialisation
{
   UsbModuleInit();
   UsbPortInit();
   UsbState = USB_STATE_OFF;
   IsExclusive = NO;
   EnumerationEnabled = NO;
} // UsbInit

//------------------------------------------------------------------------------
//  Enumeration enable
//------------------------------------------------------------------------------

void UsbEnumerationEnable( TYesNo Enable)
// Enable device enumeration
{
   EnumerationEnabled = Enable;
} // UsbEnumerationEnable

TYesNo UsbIdle( void) {
   return UsbState == USB_STATE_OFF;
}

//------------------------------------------------------------------------------
//  USB execute
//------------------------------------------------------------------------------

void UsbExecute( void)
// Usb task
{
static TYesNo ConnectedToHost;
TChargerType Charger;
   switch(_UsbPower()) {
      case EDGE_RISING:
         if(UsbState != USB_STATE_HOST_ENABLED && UsbState != USB_STATE_HOST_CONNECTED) {
            UsbShutdown(); // make sure to reset USB (partially disconnected USB connector doesn't trigger EDGE_FALLING)
         }
         UsbState = USB_STATE_OFF;

      case LEVEL_HIGH:
         break;

      case EDGE_FALLING:
         if(UsbState != USB_STATE_HOST_ENABLED && UsbState != USB_STATE_HOST_CONNECTED) {
            UsbShutdown();
         }
         UsbState = USB_STATE_OFF;
      case LEVEL_LOW:
         return;
      
      default:
         return;
   }

   // proccess USB device
   switch(UsbState) {
      case USB_STATE_OFF:
         UsbModuleSwitchOn();
         EnableDevice();
         UsbState = USB_STATE_DEVICE_CHARGER_DETECTION;
         UsbChargerDetectionStart();
         break;
         
      case USB_STATE_DEVICE_CHARGER_DETECTION:
         if(!UsbChargerDetectionCompleted()) {
            break;
         }

         Charger = UsbChargerCharger();
         
         
         switch(Charger) {
            case CHARGER_STANDARD_HOST:
               ConnectedToHost = YES;
               if(EnumerationEnabled) {
                  UsbState = USB_STATE_DEVICE_ATTACHED;
                  UsbDeviceInit();
                  return;
               }   
               break;

            case CHARGER_CHARGING_PORT:
               ConnectedToHost = YES;
               if(EnumerationEnabled) {
                  UsbState = USB_STATE_DEVICE_ATTACHED;
                  UsbDeviceInit();
                  return;
               }               
               break;

            case CHARGER_DEDICATED:
               ConnectedToHost = NO;
               break;

            default:
               ConnectedToHost = NO;
               break;
         }
         UsbModuleSwitchOff();
         UsbState = USB_STATE_CHARGER;
         break;

      case USB_STATE_CHARGER:
         if(EnumerationEnabled && ConnectedToHost) {
            UsbState = USB_STATE_DEVICE_ATTACHED;
            UsbModuleSwitchOn();
            EnableDevice();
            UsbDeviceInit();
         }
         break;

      case USB_STATE_DEVICE_ATTACHED:
         if(!EnumerationEnabled) {
            UsbDeviceDeinit();
            UsbModuleSwitchOff();
            UsbState = USB_STATE_CHARGER;
         }
         if(UsbDeviceIsConnected()) {
            UsbState = USB_STATE_DEVICE_CONNECTED;
         }
         break;

      case USB_STATE_DEVICE_CONNECTED:
         if(!EnumerationEnabled) {
            UsbDeviceDeinit();
            UsbModuleSwitchOff();
            UsbState = USB_STATE_CHARGER;
         }
         if(!UsbDeviceIsConnected()) {
            UsbState = USB_STATE_DEVICE_ATTACHED;
            break;
         }
         break;

      default:
         break;
   }
} // UsbExecute

//------------------------------------------------------------------------------
//  Status
//------------------------------------------------------------------------------

TUsbState UsbStatusGet( void)
// Usb state
{
   return UsbState;
} // UsbStatusGet;

//------------------------------------------------------------------------------
//  Host enable
//------------------------------------------------------------------------------

TYesNo UsbHostEnable(TYesNo Enable)
// Enable Host
{
   if(Enable && SocketIfUsbConnected()) {
      return NO;
   }
   UsbShutdown();

   if(!Enable) {
      SysDelay(200);
      return YES;
   }

   UsbModuleSwitchOn();
   UsbModulePullDownEnable(YES);
   EnableHost();
   UsbState = USB_STATE_HOST_ENABLED;
   return YES;
} // UsbHostEnable

//------------------------------------------------------------------------------
//  Shutdown
//------------------------------------------------------------------------------

void UsbShutdown( void)
// Shutdown USB
{
   DeinitDeviceIfNeeded();
   UsbModuleSwitchOff();
   UsbSwitchDisable();
   PowerUsbHostOff();
   UsbState = USB_STATE_OFF;
} // UsbShutdown

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  Enable device
//------------------------------------------------------------------------------

static void EnableDevice( void)
// Enable device (switch)
{
   UsbSwitchEnable();
   UsbSwitchSelectDevice();
} // EnableDevice

//------------------------------------------------------------------------------
//  Enable host
//------------------------------------------------------------------------------

static void EnableHost( void)
// Enable device (switch & power)
{
   PowerUsbHostEn();
   UsbSwitchEnable();
   UsbSwitchSelectHost();
} // EnableHost

//------------------------------------------------------------------------------
//  Device deinitilization
//------------------------------------------------------------------------------

static void DeinitDeviceIfNeeded( void)
// USB device deinitilization, if needed
{
   if(UsbState == USB_STATE_DEVICE_ATTACHED || UsbState == USB_STATE_DEVICE_CONNECTED) {
      UsbDeviceDeinit();
   }
} // DeinitDeviceIfNeeded

//------------------------------------------------------------------------------
//  Usb power
//------------------------------------------------------------------------------

static byte _UsbPower( void)
// Usb power state
{
static TYesNo LastPower;
TYesNo PowerChanged;
TYesNo PowerState;

   PowerState = PowerUsbDeviceOn();
   PowerChanged = PowerState ^ LastPower;
   LastPower = PowerState;

   if(PowerChanged) {
      if(PowerState) {
         return EDGE_RISING;
      } else {
         return EDGE_FALLING;
      }
   } else {
      if(PowerState) {
         return LEVEL_HIGH;
      } else {
         return LEVEL_LOW;
      }
   }
} // _UsbPower

//------------------------------------------------------------------------------
//  Memory Dump to USB
//------------------------------------------------------------------------------

#include "Scheduler/WeighingPlanList.h"
#include "Message/ContactList.h"
#include "Predefined/PredefinedList.h"
#include "Curve/CurveList.h"
#include "Curve/CorrectionList.h"
#include "Archive/Archive.h"
#include "Log/Log.h"
#include "Storage/Sample.h"
#include "File/Efs.h"
#include "Remote/SocketIfMsd.h"
#include "Remote/Rc.h"
#include "Config/Config.h"
#include "Config/Context.h"
#include "Memory/FileRemote.h"

static void ReportProgress(ProgressBar progressreport, byte percent);

TYesNo MemoryMsdDump( ProgressBar progressreport)
// Memory Dump to USB
{
TSocket Socket;

   if(!SocketIfMsdOpenForSendCmd( &Socket)) {
      return NO;
   }
   FileRemoteSetup(&Socket);
   if(!ConfigRemoteSave()) {
      SocketIfMsdClose(&Socket);
      return NO;
   }
   if(!ContextRemoteSave()) {
      SocketIfMsdClose(&Socket);
      return NO;
   }
   if(!WeighingPlanListRemoteSave()) {
      SocketIfMsdClose(&Socket);
      return NO;
   }
   if(!ContactListRemoteSave()) {
      SocketIfMsdClose(&Socket);
      return NO;
   }
   if(!PredefinedListRemoteSave()) {
      SocketIfMsdClose(&Socket);
      return NO;
   }
   if(!CurveListRemoteSave()) {
      SocketIfMsdClose(&Socket);
      return NO;
   }
   if(!CorrectionListRemoteSave()) {
      SocketIfMsdClose(&Socket);
      return NO;
   }
   if(!ArchiveRemoteSave()) {
      SocketIfMsdClose(&Socket);
      return NO;
   }
   if(!SampleRemoteSave()) {
      SocketIfMsdClose(&Socket);
      return NO;
   }
   SocketIfMsdClose(&Socket);
   return YES;
} // MemoryDump

#define BUFFER_SIZE (sizeof(TRcReply) > sizeof(TRcCmd)? sizeof(TRcReply) : sizeof(TRcCmd))

TYesNo MsdMemoryDump( ProgressBar progressreport)
// Memory Dump from USB (load data from USB)
{
TSocket Socket;
TRcId RcId;
byte Buffer[ BUFFER_SIZE];
TRcCmd *Command = (TRcCmd *)Buffer;
TYesNo Processed;
dword ReceiveSize = 0;
dword TotalSize;
word ReplyLength;

   if(!SocketIfMsdOpenForReceiveCmd()) {
      
      return NO;
   }
   TotalSize = SocketIfMsdTotalReceiveSize();
   RcInit();
   RcId.SessionId = RcSessionStart();
   forever{
      if( !SocketIfMsdReceive(&Socket, Buffer, BUFFER_SIZE)){
         break;
      }
      ReceiveSize += SocketIfMsdReceiveSize(&Socket);
      ReportProgress(progressreport, ReceiveSize*100/TotalSize);
      
      if( Command->Cmd < _RC_CMD_NO_SESSION){
         RcCommand( &RcId, Command, SocketIfMsdReceiveSize(&Socket),(TRcReply *) Buffer, &ReplyLength);
      } else {
         RcNoSessionCommand( Command, SocketIfMsdReceiveSize(&Socket),(TRcReply *) Buffer, &ReplyLength);
      }
      if( SocketIfMsdState( &Socket) == SOCKET_STATE_DISCONNECTED){
         break;
      }
      MultitaskingReschedule();
   }

   ReportProgress(progressreport, 100);
   RcSessionTerminate(RcId.SessionId);
   SocketIfMsdClose(&Socket);   
   return YES;
}

static void ReportProgress(ProgressBar progressreport, byte percent){
   if(progressreport != NULL){
      progressreport(percent);
   }
}