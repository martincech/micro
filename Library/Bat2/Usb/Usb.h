//*****************************************************************************
//
//    Usb.h         Usb
//    Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Usb_H__
   #define __Usb_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

// Usb state
typedef enum {
   USB_STATE_OFF,
   USB_STATE_DEVICE_CHARGER_DETECTION,
   USB_STATE_CHARGER,
   USB_STATE_DEVICE_ATTACHED,
   USB_STATE_DEVICE_CONNECTED,
   USB_STATE_HOST_ENABLED,
   USB_STATE_HOST_CONNECTED
} EUsbState;

typedef byte TUsbState;

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void UsbInit( void);
// Initialisation

void UsbEnumerationEnable( TYesNo Enable);
// Enable device enumeration

void UsbExecute( void);
// Usb task

TYesNo UsbIsExclusive( void);
// Exclusive access requested/not requested

TUsbState UsbStatusGet( void);
// Usb state

TYesNo UsbHostEnable(TYesNo Enable);
// Enable Host

void UsbShutdown( void);
// Shutdown USB

TYesNo UsbIdle( void);

typedef void (*ProgressBar)(byte Percent);

TYesNo MemoryMsdDump( ProgressBar progressreport);
// Memory Dump to USB

TYesNo MsdMemoryDump( ProgressBar progressreport);
// Memory Dump from USB (load data from USB)

#endif
