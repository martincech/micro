//******************************************************************************
//
//   UsbDevice.h    Usb Device
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __UsbDevice_H__
   #define __UsbDevice_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

void UsbDeviceInit( void);
// Initialisation

void UsbDeviceDeinit( void);
// Deinitialisation

TYesNo UsbDeviceIsConnected( void);
// Returns YES if connected to host

#endif