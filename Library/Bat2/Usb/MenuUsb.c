//******************************************************************************
//
//   MenuUsb.c               Usb menu
//   Version 1.0             (c) VEIT Electronics
//
//******************************************************************************

#include "MenuUsb.h"
#include "System/System.h"
#include "Graphic/Graphic.h"      // graphic
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DProgress.h"         // Display menu
#include "Gadget/DMsg.h"          // Message box
#include "Str.h"                  // Strings
#include "Weighing/Memory.h"
#include "Menu/Menu.h"
#include "Config/ConfigDef.h"
#include "Config/Config.h"
#include "Remote/Socket.h"
#include "Weighing/Memory.h"
#include "Usb/Usb.h"
#include "Gadget/DEvent.h"                  // Display events

#include "Remote/SocketIfMsd.h"
#include "Remote/Rc.h"

static DefMenu( UsbMenu)
   STR_LOAD_CONFIGURATION,
   STR_SAVE_CONFIGURATION,
   STR_TEST_MEMORY,
   STR_FORMAT,
EndMenu()

typedef enum {
   MI_LOAD_CONFIGURATION,
   MI_SAVE_CONFIGURATION,
   MI_TEST_MEMORY,
   MI_FORMAT,
} EUsbMenu;

static void Progress(byte Percent);
// Progress

//------------------------------------------------------------------------------
//  Menu Usb
//------------------------------------------------------------------------------

void MenuUsb( void)
// Menu Usb
{
TMenuData MData;

   DMenuClear( MData);
   forever {
      DEventDiscard();
      // selection :
      if( !DMenu( STR_USB, UsbMenu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_LOAD_CONFIGURATION :
            if( !DMsgYesNo( STR_CONFIRMATION, STR_PROCEED_CONFIRM, 0)){
               break;
            }
            DMsgWait();
            if( MsdMemoryDump( Progress)){
               DMsgOk( STR_USB, STR_DONE, 0);
               // load new configuration for the cases, when something was changed to update it
               MenuExitSet(MENU_EXIT_REBOOT);
            } else {
               DMsgOk( STR_USB, STR_USB_ERROR, 0);
            } 
            break;

         case MI_SAVE_CONFIGURATION :
            if( !DMsgYesNo( STR_CONFIRMATION, STR_PROCEED_CONFIRM, 0)){
               break;
            }
            DMsgWait();
            if(MemoryMsdDump(Progress)) {
               DMsgOk( STR_USB, STR_DONE, 0);
            } else {
               DMsgOk( STR_USB, STR_USB_ERROR, 0);
            }
            break;

         case MI_TEST_MEMORY :
            DMsgWait();
            if(!SocketIfMsdTest()) {
               DMsgOk( STR_USB, STR_USB_ERROR, 0);
            } else {
               DMsgOk( STR_USB, STR_USB_OK, 0);
            }
            break;

         case MI_FORMAT:
            if(!DMsgYesNo( STR_USB, STR_ALL_DATA_LOST, 0)) {
               return;
            }
            DMsgWait();
            if(!SocketIfMsdFormat()) {
               DMsgOk( STR_USB, STR_USB_NOT_PRESENT_INCOMPATIBLE, 0);
               return;
            }
            DMsgOk( STR_USB, STR_DONE, 0);
            break;
      }
   }
} // MenuUsb

//******************************************************************************

//------------------------------------------------------------------------------
//  Progress
//------------------------------------------------------------------------------

static void Progress(byte Percent)
// Progress
{
   DProgress(Percent, 0, 0, G_WIDTH, 20);
   GFlush();
} // Progress
