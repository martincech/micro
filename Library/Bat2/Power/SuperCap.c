//*****************************************************************************
//
//    SuperCap.c    Bat2 supercap utility
//    Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#include "SuperCap.h"
#include "Hardware.h"
#include "IAdc/IAdc.h"
#include "Power/Power.h"

typedef enum {
   SUPERCAP_STATE_CHARGING,
   SUPERCAP_STATE_CHARGED
} TSupercapState;

static int VoltageGet( byte Channel);
// Get <Channel> voltage 

static int CapVoltage( void);
// Measure supercap voltage

static TYesNo CapVoltageLow( void);
// Check supercap's low level

static TYesNo CapVoltageHigh( void);
// Check supercap's high level

static void ChargingStart( void);
// Start charging

static void ChargingStop( void);
// Stop charging

static byte _State;
static int ReferenceVoltage;
static TYesNo ReferenceValid;

//------------------------------------------------------------------------------
//  Init
//------------------------------------------------------------------------------

void SuperCapInit( void)
// Init
{
   SuperCapPortInit();
   IAdcInit();
   ChargingStart();
   _State = SUPERCAP_STATE_CHARGING;
} // SuperCapInit

//------------------------------------------------------------------------------
//  Execute
//------------------------------------------------------------------------------

void SuperCapExecute( void)
// Execute
{
   switch( _State) {
      case SUPERCAP_STATE_CHARGING:
         ChargingStart();
         if(!CapVoltageHigh()) {
            break;
         }
         ChargingStop();
         _State = SUPERCAP_STATE_CHARGED;
         break;
      
      case SUPERCAP_STATE_CHARGED:
         if(!CapVoltageLow()) {
            break;
         }
         ChargingStart();
         _State = SUPERCAP_STATE_CHARGING;
         break;
         
      default:
         break;
   }
} // SupercapExecute

//******************************************************************************

//------------------------------------------------------------------------------
//  Voltage get
//------------------------------------------------------------------------------

static int VoltageGet( byte Channel)
// Get <Channel> voltage 
{
   word BandgapRaw = IAdcRead( IADC_BANDGAP_CHANNEL);
   int ReferenceVoltage = IADC_RAW_MAX * IADC_BANDGAP_VOLTAGE / BandgapRaw;
   word Raw = IAdcRead( Channel);
   int Voltage = Raw * ReferenceVoltage / IADC_RAW_MAX;
   return Voltage;
} // VoltageGet

//------------------------------------------------------------------------------
//  Supercap voltage
//------------------------------------------------------------------------------

static int CapVoltage( void)
// Measure supercap voltage
{
int AdcVoltage = 0;;
   SuperCapMeasOn();
   SysUDelay(1);
   AdcVoltage += VoltageGet( IADC_SUPERCAP_CHANNEL);
   AdcVoltage += VoltageGet( IADC_SUPERCAP_CHANNEL);
   AdcVoltage += VoltageGet( IADC_SUPERCAP_CHANNEL);
   AdcVoltage += VoltageGet( IADC_SUPERCAP_CHANNEL);
   AdcVoltage += VoltageGet( IADC_SUPERCAP_CHANNEL);
   AdcVoltage += VoltageGet( IADC_SUPERCAP_CHANNEL);
   AdcVoltage += VoltageGet( IADC_SUPERCAP_CHANNEL);
   AdcVoltage += VoltageGet( IADC_SUPERCAP_CHANNEL);
   AdcVoltage += VoltageGet( IADC_SUPERCAP_CHANNEL);
   AdcVoltage += VoltageGet( IADC_SUPERCAP_CHANNEL);
   AdcVoltage += VoltageGet( IADC_SUPERCAP_CHANNEL);
   AdcVoltage += VoltageGet( IADC_SUPERCAP_CHANNEL);
   AdcVoltage += VoltageGet( IADC_SUPERCAP_CHANNEL);
   AdcVoltage += VoltageGet( IADC_SUPERCAP_CHANNEL);
   AdcVoltage += VoltageGet( IADC_SUPERCAP_CHANNEL);
   AdcVoltage += VoltageGet( IADC_SUPERCAP_CHANNEL);
   AdcVoltage /= 16;

   int Voltage = AdcVoltage * (SUPERCAP_VOLTAGE_R_UPPER + SUPERCAP_VOLTAGE_R_LOWER) / SUPERCAP_VOLTAGE_R_LOWER;
   SuperCapMeasOff();
   /*GClear();
   GTextAt(0, 0);
   cprintf("ADC: %d, CAP: %d", AdcVoltage, Voltage);
   GFlush();*/
   return Voltage;
} // CapVoltage

//------------------------------------------------------------------------------
//  Supercap voltage low
//------------------------------------------------------------------------------

static TYesNo CapVoltageLow( void)
// Check supercap's low level
{
   int Voltage = CapVoltage();
   if(Voltage < SUPERCAP_VOLTAGE_MIN) {
      return YES;
   }
   return NO;
} // CapVoltageLow

//------------------------------------------------------------------------------
//  Supercap voltage high
//------------------------------------------------------------------------------

static TYesNo CapVoltageHigh( void)
// Check supercap's high level
{
   int Voltage = CapVoltage();
   if(Voltage >= SUPERCAP_VOLTAGE_MAX) {
      return YES;
   }
   return NO;
} // CapVoltageHigh

//------------------------------------------------------------------------------
//  Start charging
//------------------------------------------------------------------------------

static void ChargingStart( void)
// Start charging
{
   SuperCapChargingEnable();
   if(PowerExternal()) {
      return;
   }
   PowerBankDisable();
   PowerBankOnEnable();
   PowerBankOnLow();
   SysUDelay(100);
   PowerBankOnHigh();
   SysUDelay(100);
   PowerBankOnLow();
   SysUDelay(100);
   PowerBankOnDisable();
   PowerBankEnable();
} // ChargingStart

//------------------------------------------------------------------------------
//  Stop charging
//------------------------------------------------------------------------------

static void ChargingStop( void)
// Stop charging
{
   SuperCapChargingDisable();
   PowerBankDisable();
} // ChargingStop