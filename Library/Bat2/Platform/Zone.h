//******************************************************************************
//
//   Zone.h         Weighing zone control
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Zone_H__
   #define __Zone_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __PlatformDef_H__
   #include "Platform/PlatformDef.h"
#endif

#ifndef __StatisticDef_H__
   #include "Statistic/StatisticDef.h"
#endif

// platform calibration data :
extern       TPlatformVersion     PlatformVersion;
extern       TPlatformSigmaDelta  PlatformSigmaDelta;
extern const TPlatformSigmaDelta  PlatformSigmaDeltaDefault;
extern       TPlatformPicostrain  PlatformPicostrain;
extern const TPlatformPicostrain  PlatformPicostrainDefault;
extern       TPlatformDetection   PlatformDetection;
extern       TPlatformAcceptance  PlatformAcceptance;

extern       TPlatformCalibration PlatformCalibration;
extern const TPlatformCalibration PlatformCalibrationDefault;

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void ZoneInit( void);
// Initialize

void ZoneResume( void);
// Resume weighing data after power on

void ZoneExecute( void);
// Execute weighing

TYesNo ZoneStart( void);
// Start weighing

TYesNo ZoneStop( void);
// Stop weighing

TYesNo ZoneCalibration( void);
// Start calibration

TYesNo ZoneDiagnostics( void);
// Run diagnostics

void ZoneClear( void);
// Clear samples FIFO and statistics

//------------------------------------------------------------------------------

byte ZoneOperation( void);
// Returns platform current operation

byte ZoneError( void);
// Returns platform last error

TWeightGauge ZoneActualWeight( void);
// Returns platform actual weight

int ZoneSamplesCount( void);
// Returns platform saved samples count

TWeightGauge ZoneLastWeight( void);
// Returns platform last saved weight

byte ZoneLastFlag( void);
// Returns platform last saved weight flag

TStatistic *ZoneStatistic( void);
// Returns current statistics

THistogram *ZoneHistogram( void);
// Returns current histogram

TStatistic *ZoneStatisticFemale( void);
// Returns current statistics

THistogram *ZoneHistogramFemale( void);
// Returns current histogram

#endif
