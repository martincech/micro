//******************************************************************************
//
//   Wepl.c        Weighing platform communication
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "Platform/Wepl.h"
#include "Platform/PlatformRpc.h"
#include "Uart/Uart.h"
#include "Uart/NativeDef.h"
#ifdef ENDIAN_BIG
   #include "Convert/uEndian.h"
#endif
#include <string.h>

#define WEPL_TIMEOUT       200        // reply timeout [ms]
#define WEPL_TX_DELAY      2          // Tx delay [ms]

// command buffer :
static TPlatformCommand Command;
static TPlatformReply   Reply;
static int              RxSize;

#define CommandClear()  memset( &Command, 0, sizeof( TPlatformCommand))

static TYesNo CommandSend( int Size, int ReplySize);
// Send <Command> with <Size>, <ReplySize> is requested reply size

//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

TYesNo WeplInit( void)
// Initialize
{
   UartInit( WEPL_UART);
   UartSetup( WEPL_UART, 19200, UART_8BIT);
   UartTimeoutSet( WEPL_UART, WEPL_TIMEOUT, 50);
   UartSendDelaySet( WEPL_UART, WEPL_TX_DELAY);
   UartBufferSet( WEPL_UART, &Reply, sizeof( TPlatformReply));
   UartModeSet( WEPL_UART, UART_MODE_NATIVE_MASTER);
   return( YES);
} // WeplInit

//------------------------------------------------------------------------------
//  Address
//------------------------------------------------------------------------------

void WeplAddressSet( TPlatformAddress Address)
// Set current platform <Address>
{
   UartSendAddressSet( WEPL_UART, Address);
} // WeplAddressSet

TYesNo WeplDiscover( TDeviceVersion *Version)
// Discover new device
{
int CommandSize;
int ReplySize;
TCommonCommand CommonCmd;
   WeplAddressSet( BROADCAST_ADDRESS);
   CommandClear();
   Command.Command = COMMON_CMD_DISCOVER;
   CommandSize = CommonSimpleCommandSize();
   ReplySize   = CommonReplySize( DiscoverReply);
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   *Version = Reply.Data.Discover.Version;
   return YES;
} // WeplDiscover

//------------------------------------------------------------------------------
//  
//------------------------------------------------------------------------------

TYesNo WeplAddressAssign( TPlatformAddress Address, TDeviceVersion *Version)
// Assign new <Address> by <Version>
{
int CommandSize;
int ReplySize;
   WeplAddressSet( BROADCAST_ADDRESS);
   CommandClear();
   Command.Command = COMMON_CMD_ADDRESS_ASSIGN;
   Command.Data.AddressAssign.Address = Address;
   Command.Data.AddressAssign.Version = *Version;
   CommandSize = CommonCommandSize( AddressAssign);;
   ReplySize   = CommonSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return YES;
} // WeplDiscover



//------------------------------------------------------------------------------
//  Version
//------------------------------------------------------------------------------

TYesNo WeplVersionGet( TPlatformVersion *Version)
// Get device version info
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command = PLATFORM_CMD_VERSION_GET;
   CommandSize = PlatformSimpleCommandSize();
   ReplySize   = PlatformReplySize( Version);
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
#ifdef ENDIAN_BIG
   Reply.Data.Version.Device.Class        = uEndianWordSwap( Reply.Data.Version.Device.Class);
   Reply.Data.Version.Device.Modification = uEndianWordSwap( Reply.Data.Version.Device.Modification);
   Reply.Data.Version.Device.Software     = uEndianWordSwap( Reply.Data.Version.Device.Software);
   Reply.Data.Version.Device.Hardware     = uEndianWordSwap( Reply.Data.Version.Device.Hardware);
   Reply.Data.Version.Device.SerialNumber = uEndianDwordSwap( Reply.Data.Version.Device.SerialNumber);
   Reply.Data.Version.WeightMax           = uEndianDwordSwap( Reply.Data.Version.WeightMax);
   Reply.Data.Version.CalibrationTime     = uEndianDwordSwap( Reply.Data.Version.CalibrationTime);
   Reply.Data.Version.SamplesCount        = uEndianWordSwap( Reply.Data.Version.SamplesCount);
   Reply.Data.Version.SensorInversion     = uEndianByteSwap( Reply.Data.Version.SensorInversion);
#endif
   *Version = Reply.Data.Version;
   return( YES);
} // WeplVersion

//------------------------------------------------------------------------------
//  Status
//------------------------------------------------------------------------------

TYesNo WeplStatusGet( TPlatformStatus *Status)
// Get device status
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command = PLATFORM_CMD_STATUS_GET;
   CommandSize = PlatformSimpleCommandSize();
   ReplySize   = PlatformReplySize( Status);
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
#ifdef ENDIAN_BIG
   Reply.Data.Status.Operation    = uEndianByteSwap( Reply.Data.Status.Operation);
   Reply.Data.Status.Error        = uEndianByteSwap( Reply.Data.Status.Error);
   Reply.Data.Status.Power        = uEndianByteSwap( Reply.Data.Status.Power);
   Reply.Data.Status.SamplesCount = uEndianWordSwap( Reply.Data.Status.SamplesCount);
   Reply.Data.Status.Weight       = uEndianDwordSwap( Reply.Data.Status.Weight);
#endif
   *Status = Reply.Data.Status;
   return( YES);
} // WeplStatusGet

//------------------------------------------------------------------------------
//  Clock
//------------------------------------------------------------------------------

TYesNo WeplClockSet( TPlatformClock Clock)
// Set device clock
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command    = PLATFORM_CMD_CLOCK_SET;
#ifdef ENDIAN_BIG
   Command.Data.Clock = uEndianDwordSwap( Clock);
#else
   Command.Data.Clock = Clock;
#endif
   CommandSize = PlatformCommandSize( Clock);
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplClockSet

//------------------------------------------------------------------------------
//  Picostrain
//------------------------------------------------------------------------------

TYesNo WeplPicostrainSet( TPlatformPicostrain *Picostrain)
// Set picostrain configuration
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command         = PLATFORM_CMD_PICOSTRAIN_SET;
   Command.Data.Picostrain = *Picostrain;
#ifdef ENDIAN_BIG
   Command.Data.Picostrain.Rate           = uEndianWordSwap( Command.Data.Picostrain.Rate);
   Command.Data.Picostrain.Prefilter      = uEndianWordSwap( Command.Data.Picostrain.Prefilter);
   Command.Data.Picostrain.Filter         = uEndianByteSwap( Command.Data.Picostrain.Filter);
   Command.Data.Picostrain.Mode           = uEndianByteSwap( Command.Data.Picostrain.Mode);
   Command.Data.Picostrain.AccuracyFine   = uEndianByteSwap( Command.Data.Picostrain.AccuracyFine);
   Command.Data.Picostrain.AccuracyCoarse = uEndianByteSwap( Command.Data.Picostrain.AccuracyCoarse);
#endif
   CommandSize = PlatformCommandSize( Picostrain);
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplPicostrainSet

//------------------------------------------------------------------------------
//  Sigma Delta
//------------------------------------------------------------------------------

TYesNo WeplSigmaDeltaSet( TPlatformSigmaDelta *SigmaDelta)
// Set sigma-delta configuration
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command         = PLATFORM_CMD_SIGMA_DELTA_SET;
   Command.Data.SigmaDelta = *SigmaDelta;
#ifdef ENDIAN_BIG
   Command.Data.SigmaDelta.Rate      = uEndianWordSwap( Command.Data.SigmaDelta.Rate);
   Command.Data.SigmaDelta.Prefilter = uEndianWordSwap( Command.Data.SigmaDelta.Prefilter);
   Command.Data.SigmaDelta.Filter    = uEndianByteSwap( Command.Data.SigmaDelta.Filter);
   Command.Data.SigmaDelta.Chop      = uEndianByteSwap( Command.Data.SigmaDelta.Chop);
#endif
   CommandSize = PlatformCommandSize( SigmaDelta);
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplSigmaDeltaSet

//------------------------------------------------------------------------------
//  Detection
//------------------------------------------------------------------------------

TYesNo WeplDetectionSet( TPlatformDetection *Detection)
// Set detection parameters
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command         = PLATFORM_CMD_DETECTION_SET;
   Command.Data.Detection = *Detection;
#ifdef ENDIAN_BIG
   Command.Data.Detection.Fine.AveragingWindow   = uEndianByteSwap(  Command.Data.Detection.Fine.AveragingWindow);
   Command.Data.Detection.Fine.StableWindow      = uEndianByteSwap(  Command.Data.Detection.Fine.StableWindow);
   Command.Data.Detection.Fine.AbsoluteRange     = uEndianDwordSwap( Command.Data.Detection.Fine.AbsoluteRange);
   Command.Data.Detection.Fine.SwitchoverRange   = uEndianDwordSwap( Command.Data.Detection.Fine.SwitchoverRange);
   Command.Data.Detection.Coarse.AveragingWindow = uEndianByteSwap(  Command.Data.Detection.Coarse.AveragingWindow);
   Command.Data.Detection.Coarse.StableWindow    = uEndianByteSwap(  Command.Data.Detection.Coarse.StableWindow);
   Command.Data.Detection.Coarse.AbsoluteRange   = uEndianDwordSwap( Command.Data.Detection.Coarse.AbsoluteRange);
   Command.Data.Detection.Coarse.SwitchoverRange = uEndianDwordSwap( Command.Data.Detection.Coarse.SwitchoverRange);
#endif
   CommandSize = PlatformCommandSize( Detection);
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplDetectionSet

//------------------------------------------------------------------------------
//  Acceptance
//------------------------------------------------------------------------------

TYesNo WeplAcceptanceSet( TPlatformAcceptance *Acceptance)
// Set acceptance parameters
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command         = PLATFORM_CMD_ACCEPTANCE_SET;
   Command.Data.Acceptance = *Acceptance;
#ifdef ENDIAN_BIG
   Command.Data.Acceptance.Mode            = uEndianByteSwap( Command.Data.Acceptance.Mode);
   Command.Data.Acceptance.Sex             = uEndianByteSwap( Command.Data.Acceptance.Sex);
   Command.Data.Acceptance.Step            = uEndianByteSwap( Command.Data.Acceptance.Step);
   Command.Data.Acceptance.LowLimit        = uEndianDwordSwap( Command.Data.Acceptance.LowLimit);
   Command.Data.Acceptance.HighLimit       = uEndianDwordSwap( Command.Data.Acceptance.HighLimit);
   Command.Data.Acceptance.LowLimitFemale  = uEndianDwordSwap( Command.Data.Acceptance.LowLimitFemale);
   Command.Data.Acceptance.HighLimitFemale = uEndianDwordSwap( Command.Data.Acceptance.HighLimitFemale);
#endif
   CommandSize = PlatformCommandSize( Acceptance);
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplAcceptanceSet

//------------------------------------------------------------------------------
//  Calibration
//------------------------------------------------------------------------------

TYesNo WeplCalibrationSet( TPlatformCalibration *Calibration)
// Set calibration parameters
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command          = PLATFORM_CMD_CALIBRATION_SET;
   Command.Data.Calibration = *Calibration;
#ifdef ENDIAN_BIG
   Command.Data.Calibration.FullRange = uEndianDwordSwap( Command.Data.Calibration.FullRange);
   Command.Data.Calibration.Delay     = uEndianWordSwap( Command.Data.Calibration.Delay);
   Command.Data.Calibration.Duration  = uEndianWordSwap( Command.Data.Calibration.Duration);
#endif
   CommandSize = PlatformCommandSize( Calibration);
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplCalibrationSet

//------------------------------------------------------------------------------
//  Start
//------------------------------------------------------------------------------

TYesNo WeplWeighingStart( void)
// Start weighing
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command = PLATFORM_CMD_WEIGHING_START;
   CommandSize = PlatformSimpleCommandSize();
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplWeighingStart

//------------------------------------------------------------------------------
//  Diagnostic start
//------------------------------------------------------------------------------

TYesNo WeplDiagnosticStart( void)
// Start diagnostics
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command = PLATFORM_CMD_DIAGNOSTIC_START;
   CommandSize = PlatformSimpleCommandSize();
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplDiagnosticStart

//------------------------------------------------------------------------------
// Diagnostics get
//------------------------------------------------------------------------------

TYesNo WeplDiagnosticWeightGet( dword IdIn, dword *IdOut, TDiagnosticFrame *Frame)
// Get diagnostic data
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command = PLATFORM_CMD_DIAGNOSTIC_WEIGHT_GET;
   Command.Data.Diagnostic.Id = IdIn;
   CommandSize = PlatformCommandSize(DiagnosticWeightGet);
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, 0)){
      return( NO);
   }
   if( Reply.Data.Diagnostic.Frame.Count > DIAGNOSTIC_BURST_MAX){
      return( NO);
   }
   if( (int)PlatformDiagnosticWeightReplySize( &Reply.Data.Diagnostic.Frame) != RxSize){
      return( NO);
   }
   *IdOut = Reply.Data.Diagnostic.Id;
   memcpy( Frame, &Reply.Data.Diagnostic.Frame, DiagnosticFrameSize(&Reply.Data.Diagnostic.Frame));
   return( YES);
} // WeplDiagnosticWeightGet

//------------------------------------------------------------------------------
//  Stop
//------------------------------------------------------------------------------

TYesNo WeplStop( void)
// Stop weighing/diagnostics
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command = PLATFORM_CMD_STOP;
   CommandSize = PlatformSimpleCommandSize();
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplStop

//------------------------------------------------------------------------------
//  Saved weight
//------------------------------------------------------------------------------

TYesNo WeplSavedWeightGet( word *Count, TPlatformWeight *Sample)
// Returns saved <Sample> with <Count>
{
int CommandSize;
int ReplyCount;
#ifdef ENDIAN_BIG
   int              i;
   TPlatformWeight *w;
#endif

   CommandClear();
   Command.Command = PLATFORM_CMD_SAVED_WEIGHT_GET;
   CommandSize = PlatformSimpleCommandSize();
   if( !CommandSend( CommandSize, 0)){
      return( NO);
   }
   ReplyCount = Reply.Data.SavedWeight.Count;
   if( ReplyCount > PLATFORM_SAVED_WEIGHT_COUNT){
      return( NO);
   }
   if( (int)PlatformSavedWeightReplySize( ReplyCount) != RxSize){
      return( NO);
   }
   *Count = ReplyCount;
   if( ReplyCount == 0){
      return( YES);
   }
#ifdef ENDIAN_BIG
   i = ReplyCount;
   w = Reply.Data.SavedWeight.Weight;
   while( i--){
      w->Weight    = uEndianDwordSwap( w->Weight);
      w->Timestamp = uEndianWordSwap( w->Timestamp);
      w++;
   }
#endif
   memcpy( Sample, &Reply.Data.SavedWeight.Weight, ReplyCount * sizeof( TPlatformWeight));
   return( YES);
} // WeplSavedWeightGet

//------------------------------------------------------------------------------
//  Confirm saved weight
//------------------------------------------------------------------------------

TYesNo WeplSavedWeightConfirm( word Count)
// Confirm <Count> saved weights
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command = PLATFORM_CMD_SAVED_WEIGHT_CONFIRM;
   Command.Data.Confirm.Count = Count;
   CommandSize = PlatformCommandSize( SavedWeightConfirm);
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplSavedWeightConfirm

//------------------------------------------------------------------------------
//   Communication set
//------------------------------------------------------------------------------

TYesNo WeplCommunicationSet( TPlatformCommunication *Communication)
// Set communication parameters (warning : parameters are set immediately)
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command            = PLATFORM_CMD_COMMUNICATION_SET;
   Command.Data.Communication = *Communication;
#ifdef ENDIAN_BIG
   Command.Data.Communication.BaudRate              = uEndianDwordSwap( Command.Data.Communication.BaudRate);
   Command.Data.Communication.BusAddress            = uEndianByteSwap( Command.Data.Communication.BusAddress);
   Command.Data.Communication.Format                = uEndianByteSwap( Command.Data.Communication.Format);
   Command.Data.Communication.IntercharacterTimeout = uEndianWordSwap( Command.Data.Communication.IntercharacterTimeout);
   Command.Data.Communication.TransmitterDelay      = uEndianWordSwap( Command.Data.Communication.TransmitterDelay);
#endif   
   CommandSize = PlatformCommandSize( Communication);
   ReplySize   = PlatformSimpleReplySize();
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
   return( YES);
} // WeplCommunicationSet

//------------------------------------------------------------------------------
//   Communication status get
//------------------------------------------------------------------------------

TYesNo WeplCommunicationStatusGet( TPlatformCommunicationStatus *CommunicationStatus)
// Get communication status
{
int CommandSize;
int ReplySize;

   CommandClear();
   Command.Command = PLATFORM_CMD_COMMUNICATION_STATUS_GET;
   CommandSize = PlatformSimpleCommandSize();
   ReplySize   = PlatformReplySize( CommunicationStatus);
   if( !CommandSend( CommandSize, ReplySize)){
      return( NO);
   }
#ifdef ENDIAN_BIG
   Reply.Data.CommunicationStatus.FrameCount          = uEndianWordSwap( Reply.Data.CommunicationStatus.FrameCount);
   Reply.Data.CommunicationStatus.WrongCharacterCount = uEndianWordSwap( Reply.Data.CommunicationStatus.WrongCharacterCount);
   Reply.Data.CommunicationStatus.WrongFrameCount     = uEndianWordSwap( Reply.Data.CommunicationStatus.WrongFrameCount);
   Reply.Data.CommunicationStatus.OtherErrors         = uEndianWordSwap( Reply.Data.CommunicationStatus.OtherErrors);
#endif
   *CommunicationStatus = Reply.Data.CommunicationStatus;
   return( YES);
} // WeplCommunicationStatusGet

//******************************************************************************

//------------------------------------------------------------------------------
//  Send
//------------------------------------------------------------------------------

static TYesNo CommandSend( int Size,  int ReplySize)
// Send <Command> with <Size>, <ReplySize> is requested reply size
{
TNativeAddress ReplyAddress;
byte           r;

   // send command :
   UartSend( WEPL_UART, &Command, Size);
   do {
      MultitaskingReschedule();
      r = UartSendStatus( WEPL_UART);	   
   } while( r != UART_SEND_DONE);
   // receive reply :
   forever {
      switch( UartReceiveStatus( WEPL_UART)){
         case UART_RECEIVE_ACTIVE :
            MultitaskingReschedule();
            continue;

         case UART_RECEIVE_FRAME :
            RxSize = UartReceiveSize( WEPL_UART);
            ReplyAddress = UartReceiveAddress( WEPL_UART);
            break;
            
         case UART_RECEIVE_FRAMING_ERROR :
         case UART_RECEIVE_OVERRUN_ERROR :
         case UART_RECEIVE_SIZE_ERROR :
         case UART_RECEIVE_REPLY_TIMEOUT :
         case UART_RECEIVE_TIMEOUT :
         case UART_RECEIVE_HEADER_CRC_ERROR : 
         case UART_RECEIVE_CRC_ERROR :
         default :
            return( NO);
      }
      break;
   }
   if( Reply.Reply != PlatformReply( Command.Command)){
      return( NO);
   }
   if( ReplySize == 0){
      return( YES);                    // don't check size
   }
   if( ReplySize != RxSize){
      return( NO);
   }
   return( YES);
} // CommandSend
