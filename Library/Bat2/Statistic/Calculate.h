//*****************************************************************************
//
//    Calculate.h  Statistic calculations
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Calculate_H__
   #define __Calculate_H__

#ifndef __StatistiDef_H__
   #include "Statistic/StatisticDef.h"
#endif

// calculation data :
typedef struct {
   byte            Sex;
   TWeightGauge    TargetWeight;
   TWeightGauge    LastAverage;
   byte            HourFrom;
   byte            HourTo;
   TStatisticGauge Statistic;
   THistogram      Histogram;
} TCalculate;

//-----------------------------------------------------------------------------

void CalculateInit( TCalculate *Calculate, byte Sex, TWeightGauge TargetWeight, TWeightGauge LastAverage, byte HourFrom, byte HourTo);
// Initialize calculation

void CalculateClear( TCalculate *Calculate);
// Clear results

void CalculateAll( TCalculate *Calculate);
// Calculate all data

void CalculateAppend( TCalculate *Calculate, TWeightGauge Weight);
// Append sample

void CalculateUpdate( TCalculate *Calculate);
// Recalculate

void CalculateStatistic( TCalculate *Calculate, TStatistic *Statistic);
// Calculate <Statistic>

THistogram *CalculateHistogram( TCalculate *Calculate);
// Returns <Calculate> histogram

#endif
