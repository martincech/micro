//******************************************************************************
//                                                                            
//   MenuStatisticParameters.c  Statistic Parameters menu
//   Version 1.0                (c) VEIT Electronics
//
//******************************************************************************

#include "MenuStatisticParameters.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label

#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display weight

#include "Str.h"                  // Strings

static DefMenu( StatisticParametersMenu)
   STR_UNIFORMITY_RANGE,
   STR_HISTOGRAM_MODE,
   STR_HISTOGRAM_RANGE,
   STR_HISTOGRAM_STEP,
   STR_SHORT_PERIOD,
   STR_SHORT_TYPE,
EndMenu()

typedef enum {
   MI_UNIFORMITY_RANGE,
   MI_HISTOGRAM_MODE,
   MI_HISTOGRAM_RANGE,
   MI_HISTOGRAM_STEP,
   MI_SHORT_PERIOD,
   MI_SHORT_TYPE
} EStatisticParametersMenu;

// Local functions :

static void StatisticParametersParameters( int Index, int y, TStatisticParameters *StatisticParameters);
// Display statistic parameters

//------------------------------------------------------------------------------
//  Statistic parameters menu
//------------------------------------------------------------------------------

void MenuStatisticParameters( TStatisticParameters *StatisticParameters)
// Edit statistic parameters
{
TMenuData MData;
int       i;

   DMenuClear( MData);
   forever {
      MData.Mask = 0;
      if(StatisticParameters->ShortPeriod == 0) {
         MData.Mask |= (1 << MI_SHORT_TYPE);
      }
      if( StatisticParameters->Histogram.Mode == HISTOGRAM_MODE_RANGE){
         MData.Mask |= (1 << MI_HISTOGRAM_STEP);       // hide step
      } else {
         MData.Mask |= (1 << MI_HISTOGRAM_RANGE);      // hide range
      }
      if( !DMenu( STR_STATISTICS, StatisticParametersMenu, (TMenuItemCb *)StatisticParametersParameters, StatisticParameters, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_UNIFORMITY_RANGE :
            i = StatisticParameters->UniformityRange;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, STATISTIC_UNIFORMITY_RANGE_MIN, STATISTIC_UNIFORMITY_RANGE_MAX, "%")){
               break;
            }
            StatisticParameters->UniformityRange = i;
            break;

         case MI_HISTOGRAM_MODE :
            i = StatisticParameters->Histogram.Mode;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, STR_HISTOGRAM_MODE_RANGE, _HISTOGRAM_MODE_COUNT)){
               break;
            }
            StatisticParameters->Histogram.Mode = i;
            break;

        case MI_HISTOGRAM_RANGE :
            i = StatisticParameters->Histogram.Range;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, HISTOGRAM_RANGE_MIN, HISTOGRAM_RANGE_MAX, "%")){
               break;
            }
            StatisticParameters->Histogram.Range = i;
            break;

        case MI_HISTOGRAM_STEP :
            i = StatisticParameters->Histogram.Step;
            if( !DInputWeightRange( STR_HISTOGRAM_STEP, "", &i, STATISTIC_HISTOGRAM_STEP_MIN, STATISTIC_HISTOGRAM_STEP_MAX)){
               break;
            }
            StatisticParameters->Histogram.Step  = i;
            break;

         case MI_SHORT_PERIOD :
             i = StatisticParameters->ShortPeriod;
             if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, STATISTIC_SHORT_PERIOD_MIN, STATISTIC_SHORT_PERIOD_MAX, "h")){
                break;
             }
             StatisticParameters->ShortPeriod = i;
             break;

         case MI_SHORT_TYPE :
            i = StatisticParameters->ShortType;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, STR_STATISTIC_SHORT_TYPE_INTERVAL, _STATISTIC_SHORT_TYPE_COUNT)){
               break;
            }
            StatisticParameters->ShortType = i;
            break;
      }
   }
} // MenuLimits

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void StatisticParametersParameters( int Index, int y, TStatisticParameters *StatisticParameters)
// Display statistic parameters
{
   switch( Index){
      case MI_UNIFORMITY_RANGE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %%", StatisticParameters->UniformityRange);
         break;

      case MI_HISTOGRAM_MODE :
         DLabelEnum( StatisticParameters->Histogram.Mode, STR_HISTOGRAM_MODE_RANGE, DMENU_PARAMETERS_X, y);
         break;

      case MI_HISTOGRAM_RANGE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %%", StatisticParameters->Histogram.Range);
         break;

      case MI_HISTOGRAM_STEP :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, StatisticParameters->Histogram.Step);
         break;

      case MI_SHORT_PERIOD :
         if(StatisticParameters->ShortPeriod == 0) {
            DLabelNarrow( STR_SHORT_PERIOD_DISABLED, DMENU_PARAMETERS_X, y);
            break;
         }
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d h", StatisticParameters->ShortPeriod);
         break;

      case MI_SHORT_TYPE :
         DLabelEnum( StatisticParameters->ShortType, STR_STATISTIC_SHORT_TYPE_INTERVAL, DMENU_PARAMETERS_X, y);
         break;
   }
} // StatisticParametersParameters
