//******************************************************************************
//                                                                            
//    DStatistic.h   Display statistics
//    Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __DStatistic_H__
   #define __DStatistic_H__

#ifndef __Calculate_H__
   #include "Statistic/Calculate.h"
#endif


void DStatistic( TStatistic *StatisticCalculate);
// Display <Statistic>. Returns YES on Enter

#endif
