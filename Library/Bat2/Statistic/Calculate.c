//*****************************************************************************
//
//    Calculate.c  Statistic calculations
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Calculate.h"
#include "Storage/Sample.h"
#include "Statistic/Statistic.h"
#include "Statistic/Histogram.h"
#include "Weighing/WeighingConfiguration.h"
#include "Multitasking/Multitasking.h"

// Local functions :
static void _CalculatePrimary( TCalculate *Calculate);
// Calculate primary statistics

static void _CalculateSecondary( TCalculate *Calculate);
// Calculate secondary statistics

static TYesNo _CheckSex( TCalculate *Calculate, byte WeightFlag);
// Check <WeightFlag> for <Calculate> sex

//-----------------------------------------------------------------------------
// Initialize
//-----------------------------------------------------------------------------

void CalculateInit( TCalculate *Calculate, byte Sex, TWeightGauge TargetWeight, TWeightGauge LastAverage, byte HourFrom, byte HourTo)
// Initialize calculation
{
   Calculate->HourFrom     = HourFrom;
   Calculate->HourTo       = HourTo;
   Calculate->Sex          = Sex;
   Calculate->LastAverage  = LastAverage;
   Calculate->TargetWeight = TargetWeight;
} // CalculateInit

//-----------------------------------------------------------------------------
// Clear
//-----------------------------------------------------------------------------

void CalculateClear( TCalculate *Calculate)
// Clear results
{
   StatisticClear( &Calculate->Statistic);
   HistogramClear( &Calculate->Histogram);
} // CalculateClear

//-----------------------------------------------------------------------------
// Statistics
//-----------------------------------------------------------------------------

void CalculateAll( TCalculate *Calculate)
// Calculate all data
{
   CalculateClear( Calculate);
   _CalculatePrimary( Calculate);
   _CalculateSecondary( Calculate);
} // CalculateAll

//-----------------------------------------------------------------------------
// Append
//-----------------------------------------------------------------------------

void CalculateAppend( TCalculate *Calculate, TWeightGauge Weight)
// Append sample
{
   StatisticAppend( &Calculate->Statistic, Weight);
} // CalculateAppend

//-----------------------------------------------------------------------------
// Update
//-----------------------------------------------------------------------------

void CalculateUpdate( TCalculate *Calculate)
// Recalculate
{
   _CalculateSecondary( Calculate);
} // CalculateUpdate

//-----------------------------------------------------------------------------
// Statistic
//-----------------------------------------------------------------------------

void CalculateStatistic( TCalculate *Calculate, TStatistic *Statistic)
// Calculate <Statistic>
{
TStatisticNumber Sigma, Average;

   Average = StatisticAverage( &Calculate->Statistic);
   Sigma   = StatisticSigma( &Calculate->Statistic);
   Statistic->Target      = Calculate->TargetWeight;
   Statistic->Sex         = Calculate->Sex;
   Statistic->Count       = StatisticCount( &Calculate->Statistic);
   Statistic->Average     = (TWeightGauge)Average;
   Statistic->LastAverage = Calculate->LastAverage;
   Statistic->Gain        = 0;
   if( Calculate->LastAverage != WEIGHT_INVALID){
      Statistic->Gain     = Average - Calculate->LastAverage;
   }
   Statistic->Sigma       = (TWeightGauge)Sigma;
   Statistic->Cv          = (word)(StatisticVariation( Average, Sigma) * 10);
   Statistic->Uniformity  = (word)(StatisticUniformity( &Calculate->Statistic)  * 10);
} // CalculateStatistic

//-----------------------------------------------------------------------------
// Histogram
//-----------------------------------------------------------------------------

THistogram *CalculateHistogram( TCalculate *Calculate)
// Returns <Calculate> histogram
{
   return( &Calculate->Histogram);
} // CalculateHistogram

//*****************************************************************************

//-----------------------------------------------------------------------------
// Primary statistics
//-----------------------------------------------------------------------------
#include <stdio.h>
static void _CalculatePrimary( TCalculate *Calculate)
// Calculate primary statistics
{
TSampleIndex Count, i;
TSample      Sample;
TSamples     Samples;
byte         Hour;
   // scan samples :
   while( !SampleOpen( &Samples));
   Count = SampleCount();
   for(i = 0 ; i < Count ; i++) {
      if(!SampleGet( &Samples, i, &Sample)) {
         break;
      }
      if( !_CheckSex( Calculate, SampleFlag( &Sample))){
          continue;
      }
      Hour = WeighingHourNormalize( SampleTimestamp( &Sample));
      if(Hour < Calculate->HourFrom || Hour > Calculate->HourTo) {
         continue;
      }
      StatisticAppend( &Calculate->Statistic, SampleWeight( &Sample));
   }
   SampleClose( &Samples);
} // _CalculatePrimary

//-----------------------------------------------------------------------------
// Secondary statistics
//-----------------------------------------------------------------------------

static void _CalculateSecondary( TCalculate *Calculate)
// Calculate secondary statistics
{
TSampleIndex Count, i;
TSample      Sample;
TWeightGauge Weight;
THistogram   Histogram;
TSamples Samples;
byte Hour;
   // clear uniformity & histogram :
   StatisticUniformityClear( &Calculate->Statistic, WeighingConfiguration.Statistic.UniformityRange);
   HistogramClear( &Histogram);
   HistogramCenterSet( &Histogram, StatisticAverage( &Calculate->Statistic));
   if( WeighingConfiguration.Statistic.Histogram.Mode == HISTOGRAM_MODE_RANGE) {
      HistogramRangeSet( &Histogram, WeighingConfiguration.Statistic.Histogram.Range);  // by range
   } else {
      HistogramStepSet( &Histogram,  WeighingConfiguration.Statistic.Histogram.Step);   // by step
   }
   // scan samples :
   while( !SampleOpen( &Samples));
   Count = SampleCount();
   for(i = 0 ; i < Count ; i++) {
      if(!SampleGet( &Samples, i, &Sample)) {
         break;
      }
      if( !_CheckSex( Calculate, SampleFlag( &Sample))){
          continue;
      }
      Hour = WeighingHourNormalize( SampleTimestamp( &Sample));
      if(Hour < Calculate->HourFrom || Hour > Calculate->HourTo) {
         continue;
      }
      Weight = SampleWeight( &Sample);
      StatisticUniformityAppend( &Calculate->Statistic, Weight);   // update uniformity
      HistogramAppend( &Histogram, Weight);             // update histogram
      if(i % 256 == 0) {
         MultitaskingReschedule();
      }
   }
   SampleClose( &Samples);
   Calculate->Histogram = Histogram;
} // _CalculateSecondary

//-----------------------------------------------------------------------------
// Check sex
//-----------------------------------------------------------------------------

static TYesNo _CheckSex( TCalculate *Calculate, byte WeightFlag)
// Check <WeightFlag> for <Calculate> sex
{

   switch( Calculate->Sex){
      case SEX_MALE :
         return( WeightFlag & WEIGHT_FLAG_MALE);

      case SEX_FEMALE :
         return( WeightFlag & WEIGHT_FLAG_FEMALE);

      case SEX_UNDEFINED :
         return( (WeightFlag & WEIGHT_FLAG_SEX) == 0);

      case SEX_INVALID :
         return( YES);                 // always accept
   }
   return( NO);
} // _CheckSex
