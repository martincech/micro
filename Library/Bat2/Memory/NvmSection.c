//*****************************************************************************
//
//    NvmSection.c  Bat2 nonvolatile memory sections
//    Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#include "Memory/Nvm.h"
#include "Memory/NvmLayout.h"     // project memory layout

#if defined(__AT25DF641__)
   #include "Flash/At25dfxx.h"
#else
   #error "Unknown NOR Flash chip"
#endif

//------------------------------------------------------------------------------
//   Sections data
//------------------------------------------------------------------------------

// sections count :
#define SECTION_COUNT   13

// R/W section cache data :
static TNvmCache _Cache[ SECTION_COUNT];

//------------------------------------------------------------------------------
//   Sections
//------------------------------------------------------------------------------

// Read-only section data :
static const TNvmSection _Section[] = {
   // cached Flash memory :
   {
      /* StartAddress */ NVM_CONFIG_START,
      /* EndAddress   */ NVM_CONFIG_END,
      /* CacheAddress */ NVM_PAGE_CONFIG_START,
      /* PageSize     */ NVM_PAGE_CONFIG_PAGE,
      /* Mode         */ NVM_CONFIG_MODE,
      /* Cache        */ &_Cache[ 0]
   },
   {
      /* StartAddress */ NVM_CONTEXT_START,
      /* EndAddress   */ NVM_CONTEXT_END,
      /* CacheAddress */ NVM_PAGE_CONTEXT_START,
      /* PageSize     */ NVM_PAGE_CONTEXT_PAGE,
      /* Mode         */ NVM_CONTEXT_MODE,
      /* Cache        */ &_Cache[ 1]
   },
   {
      /* StartAddress */ NVM_SANDBOX_START,
      /* EndAddress   */ NVM_SANDBOX_END,
      /* CacheAddress */ NVM_PAGE_SANDBOX_START,
      /* PageSize     */ NVM_PAGE_SANDBOX_PAGE,
      /* Mode         */ NVM_SANDBOX_MODE,
      /* Cache        */ &_Cache[ 2]
   },
   {
      /* StartAddress */ NVM_DIAGNOSTIC_START,
      /* EndAddress   */ NVM_DIAGNOSTIC_END,
      /* CacheAddress */ NVM_PAGE_DIAGNOSTIC_START,
      /* PageSize     */ NVM_PAGE_DIAGNOSTIC_PAGE,
      /* Mode         */ NVM_DIAGNOSTIC_MODE,
      /* Cache        */ &_Cache[ 3]
   },
   {
      /* StartAddress */ NVM_SAMPLES_START,
      /* EndAddress   */ NVM_SAMPLES_END,
      /* CacheAddress */ NVM_PAGE_SAMPLES_START,
      /* PageSize     */ NVM_PAGE_SAMPLES_PAGE,
      /* Mode         */ NVM_SAMPLES_MODE,
      /* Cache        */ &_Cache[ 4]
   },
   {
      /* StartAddress */ NVM_SAMPLES_BACKUP_START,
      /* EndAddress   */ NVM_SAMPLES_BACKUP_END,
      /* CacheAddress */ NVM_PAGE_SAMPLES_BACKUP_START,
      /* PageSize     */ NVM_PAGE_SAMPLES_BACKUP_PAGE,
      /* Mode         */ NVM_SAMPLES_BACKUP_MODE,
      /* Cache        */ &_Cache[ 5]
   },
   {
      /* StartAddress */ NVM_ARCHIVE_START,
      /* EndAddress   */ NVM_ARCHIVE_END,
      /* CacheAddress */ NVM_PAGE_ARCHIVE_START,
      /* PageSize     */ NVM_PAGE_ARCHIVE_PAGE,
      /* Mode         */ NVM_ARCHIVE_MODE,
      /* Cache        */ &_Cache[ 6]
   },
   {
      /* StartAddress */ NVM_LOG_START,
      /* EndAddress   */ NVM_LOG_END,
      /* CacheAddress */ NVM_PAGE_LOG_START,
      /* PageSize     */ NVM_PAGE_LOG_PAGE,
      /* Mode         */ NVM_LOG_MODE,
      /* Cache        */ &_Cache[ 7]
   },
   {
      /* StartAddress */ NVM_FIFO_START,
      /* EndAddress   */ NVM_FIFO_END,
      /* CacheAddress */ NVM_PAGE_FIFO_START,
      /* PageSize     */ NVM_PAGE_FIFO_PAGE,
      /* Mode         */ NVM_FIFO_MODE,
      /* Cache        */ &_Cache[ 8]
   },
   {
      /* StartAddress */ NVM_SERVICE_START,
      /* EndAddress   */ NVM_SERVICE_END,
      /* CacheAddress */ 0,
      /* PageSize     */ 0,
      /* Mode         */ NVM_SERVICE_MODE,
      /* Cache        */ &_Cache[ 9]
   },
   {
      /* StartAddress */ NVM_STATE_START,
      /* EndAddress   */ NVM_STATE_END,
      /* CacheAddress */ 0,
      /* PageSize     */ 0,
      /* Mode         */ NVM_STATE_MODE,
      /* Cache        */ &_Cache[ 10]
   },
   // direct FRAM access :
   {
      /* StartAddress */ NVM_FRAM_START,
      /* EndAddress   */ NVM_FRAM_END,
      /* CacheAddress */ 0,
      /* PageSize     */ 0,
      /* Mode         */ NVM_FRAM_MODE,
      /* Cache        */ &_Cache[ 11]
   },
   // direct FLASH access :
   {
      /* StartAddress */ NVM_FLASH_START,
      /* EndAddress   */ NVM_FLASH_END,
      /* CacheAddress */ NVM_PAGE_FLASH_START,
      /* PageSize     */ NVM_PAGE_FLASH_PAGE,
      /* Mode         */ NVM_FLASH_MODE,
      /* Cache        */ &_Cache[ 12]
   }
};
