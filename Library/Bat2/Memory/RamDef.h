//******************************************************************************
//
//   RamDef.h     Bat2 RAM definition
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __RamDef_H__
   #define __RamDef_H__

#ifndef __NvmLayout_H__
   #include "Memory/NvmLayout.h"
#endif

#ifndef __StateDef_H__
   #include "Config/StateDef.h"
#endif

typedef struct {
   union {
      byte   _state[NVM_STATE_SIZE];
      TState State;
   };
} TRam;


extern TRam Ram;

#endif
