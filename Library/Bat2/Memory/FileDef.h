//*****************************************************************************
//
//   FileDef.h       Bat2 file definitions
//   Version 1.0     (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __FileDef_H__
   #ifndef _MANAGED
      #define __FileDef_H__
   #endif

#ifndef __Uni_H__
#include "Unisys/Uni.h"
#endif

#ifdef _MANAGED
namespace Bat2Library{
   public enum class FileModeE{
#else
typedef enum {
#endif
   FILE_MODE_READ_ONLY = 0x01,
   FILE_MODE_WRITE_ONLY = 0x02,
   FILE_MODE_READ_WRITE = FILE_MODE_READ_ONLY | FILE_MODE_WRITE_ONLY,
   FILE_MODE_SANDBOX = 0x80
#ifndef _MANAGED
} EFileMode;
#else
   };
}
#endif

#ifndef _MANAGED
typedef byte TFileName;

typedef byte TFileMode;

typedef struct {
   TFileName Name;
   byte Interface;
} TFileItem;

typedef struct {
   TFileItem Item;
   TFileMode Mode;
   TFileName Name;
} TFile;

typedef dword TFileAddress;

typedef dword TFileSum;

typedef enum {
   FILE_INTERFACE_LOCAL,
   FILE_INTERFACE_REMOTE,
   _FILE_INTERFACE_COUNT
} EFileInterface;

typedef enum {
   FILE_SANDBOX,
   FILE_CONFIG,
   FILE_SAMPLES,
   FILE_SAMPLES_BACKUP,
   FILE_ARCHIVE,
   FILE_FIFO,
   FILE_CONTACT_LIST,
   FILE_WEIGHING_PLAN,
   FILE_CONTEXT,
   FILE_PREDEFINED_WEIGHING,
   FILE_GROWTH_CURVE,
   FILE_CORRECTION_CURVE,
   FILE_LOG,
   FILE_STATE,
   FILE_FIRMWARE,
   _FILE_COUNT
} EFileDescriptor;


typedef enum {
   FILE_SANDBOX_LOCAL,
   FILE_CONFIG_LOCAL,
   FILE_SAMPLES_LOCAL,
   FILE_SAMPLES_BACKUP_LOCAL,
   FILE_ARCHIVE_LOCAL,
   FILE_FIFO_LOCAL,
   FILE_CONTACT_LIST_LOCAL,
   FILE_WEIGHING_PLAN_LOCAL,
   FILE_CONTEXT_LOCAL,
   FILE_PREDEFINED_WEIGHING_LOCAL,
   FILE_GROWTH_CURVE_LOCAL,
   FILE_CORRECTION_CURVE_LOCAL,
   FILE_LOG_LOCAL,
   FILE_STATE_LOCAL,
   FILE_FIRMWARE_LOCAL,

   FILE_CONFIG_REMOTE,
   FILE_SAMPLES_REMOTE,
   FILE_SAMPLES_BACKUP_REMOTE,
   FILE_ARCHIVE_REMOTE,
   FILE_FIFO_REMOTE,
   FILE_CONTACT_LIST_REMOTE,
   FILE_WEIGHING_PLAN_REMOTE,
   FILE_CONTEXT_REMOTE,
   FILE_PREDEFINED_WEIGHING_REMOTE,
   FILE_GROWTH_CURVE_REMOTE,
   FILE_CORRECTION_CURVE_REMOTE,
   FILE_LOG_REMOTE,
   FILE_STATE_REMOTE,
   FILE_FIRMWARE_REMOTE,
   _FILE_LIST_COUNT
} EFileList;

#include "Memory/FileInternal.h"
extern const TFileItem FileItem[_FILE_LIST_COUNT];
extern const TFileInterface FileInterface[_FILE_INTERFACE_COUNT];

#endif
#ifdef _MANAGED
   #undef _MANAGED
   #include "FileDef.h"
   #define _MANAGED
#endif

#endif
