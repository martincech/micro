//******************************************************************************
//
//   ModbusConfig.h     Modbus protocol configuration
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "ModbusConfig.h"
#include "mb.h"
#include "uart/uart.h"

TModbusModuleOptions ModbusOptions;

const TModbusModuleOptions ModbusOptionsDefault = {
   /* Mode */           MB_RTU_MASTER,
   /* Address */        1,
   /* BaudRate */       19200,
   /* Parity */         MB_PAR_EVEN,
};
