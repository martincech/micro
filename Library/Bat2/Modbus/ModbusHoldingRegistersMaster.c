//******************************************************************************
//
//   ModbusHoldingRegistersMaster.c     Modbus process reply - Holding registers commands
//   Version 1.0                       (c) VEIT Electronics
//
//******************************************************************************

#include "mb.h"
#include "SensorPack/Modbus/ModbusReg.h"
#include "Multitasking/Multitasking.h"
#include "ModbusHoldingRegistersMaster.h"
#include "mbutils.h"
#include <string.h>

static USHORT *oBuffer;
static TYesNo responseReceived = NO;
static eMBErrorCode status;
//-----------------------------------------------------------------------------
// Read holding register(s)
//-----------------------------------------------------------------------------
eMBErrorCode eMBMReadHoldingRegistersPolled( UCHAR ucSlaveAddress, USHORT usRegStartAddress, UCHAR ubNRegs, USHORT *arusBufferOut){ 
   oBuffer = arusBufferOut;
   status = eMBMReadHoldingRegisters(ucSlaveAddress, usRegStartAddress, ubNRegs);
   if(status != MB_ENOERR){
      return status;
   } 
   responseReceived = NO;
   while(responseReceived != YES){
      MultitaskingReschedule();
   }
   return status;
}

void eMBMRegHoldingCB(USHORT *pucRegBuffer, USHORT usAddress, USHORT usNRegs, eMBRegisterMode eMode)
{
word i;
word Data;

   switch ( eMode )
   {
      case MB_REG_READ:
         for( i = 0; i < usNRegs; i++){
            oBuffer[i] = ENDIAN_FROM_BIG_WORD(pucRegBuffer[i]);
         }
      break;

      case MB_REG_WRITE:
         break;
   }
   responseReceived = YES;
}

void eMBMExceptionCB(UCHAR ucRcvAddress, UCHAR ucFunctionCode, eMBException excstatus)
{
   status = prveMBException2Error(excstatus);
   responseReceived = YES;
}