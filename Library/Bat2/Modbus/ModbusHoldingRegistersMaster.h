//******************************************************************************
//
//   ModbusHoldingRegistersMaster.h     Additional functions for master
//   Version 1.0                       (c) VEIT Electronics
//
//******************************************************************************

#ifndef _MODBUS_HOLDING_REGISTERS_MASTER_H_
#define _MODBUS_HOLDING_REGISTERS_MASTER_H_

#include "mb.h"
/*! \ingroup modbus_master
 * \brief Read Holding Registers from a slave using the MODBUS function code <b>0x03</b>. 
 *        Waits until slave response (or timeout) and return the response in \c arusBufferOut.
 *        check return value for error codes! 
 *
 * \param ucSlaveAddress Slave address. Note that a broadcast address is not allowed for a function which expects a response.
 * \param usRegStartAddress The first holding register to be read. We use protocol addresses starting at zero.
 * \param ubNRegs           Number of registers to read.
 * \param arusBufferOut     An array of USHORT values of at least ubNRegs elements.
 * \return eMBErrorCode::MB_ENOERR if the packet was queued to be send successfully. Any other errors are IO errors.
 */
eMBErrorCode eMBMReadHoldingRegistersPolled( UCHAR ucSlaveAddress, USHORT usRegStartAddress, UCHAR ubNRegs, USHORT *arusBufferOut);

#endif //_MODBUS_HOLDING_REGISTERS_MASTER_H_
