//******************************************************************************
//
//   ModbusConfig.h     Modbus protocol configuration
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ModbusConfig_H__
#define __ModbusConfig_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __ModbusConfigDef_H__
   #include "ModbusConfigDef.h"
#endif

#include "Modbus/mb.h"

#ifdef __cplusplus
   extern "C" {
#endif

extern         TModbusModuleOptions ModbusOptions;
extern const   TModbusModuleOptions ModbusOptionsDefault;

#ifdef __cplusplus
   }
#endif

#endif // __ModbusConfig_H__


