//******************************************************************************
//
//   ModbusPredefinedWeighingsGroup.c      Modbus Predefined weighings register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "ModbusPredefinedWeighingsGroup.h"
#include "Predefined/PredefinedWeighingDef.h"
#include "Predefined/PredefinedList.h"
#include "Data/uNamedList.h"
#include "Weighing/WeighingConfiguration.h"
#include "ModbusRegRangeCheck.h"
#include <string.h>


static TPredefinedWeighingIndex   _PredefinedWeighingsPredefId = 0; // Predefined weighing numeric identifier - 0 means no predefined weighing
static TPredefinedWeighing Predefined;


// Locals :


//------------------------------------------------------------------------------
//  Read Predefined weighings register
//------------------------------------------------------------------------------
word ModbusRegReadPredefinedWeighings( EModbusRegNum R)
// Read Predefined weighings register group
{
TPredefinedList PredefList;
UDateTime DateTime;

   switch ( R){
      case MODBUS_REG_PREDEFINED_WEIGHINGS_MENU_CHANGES :
      {
         return Predefined.MenuMask;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_INITIALDAYNUMBER :
      {
         return Predefined.InitialDay;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_DAYSTART :
      {
         uTime( &DateTime.Time, Predefined.DayStart);
         return DateTime.Time.Hour;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_USEBOTHGENDERS :
      {
         return Predefined.TargetWeights.SexDifferentiation;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_INITIALWFEMALES :
      {
         return Predefined.Female.InitialWeight;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_INITIALWMALES :
      {
         return Predefined.Male.InitialWeight;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_USECURVES :
      {
         return Predefined.TargetWeights.Mode;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_MODE :
      {
         return Predefined.TargetWeights.Growth;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_WEIGH_PLAN_PREDEFINED :
      {
         return Predefined.WeighingPlan;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_CURVE_F_PREDEFINED :
      {
         return Predefined.Female.GrowthCurve;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_CURVE_M_PREDEFINED :
      {
         return Predefined.Male.GrowthCurve;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_FILTER :
      {
         return Predefined.Detection.Filter;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_STABILIZATIONRANGE :
      {
         return Predefined.Detection.StabilizationRange;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_STABILIZATIONTIME :
      {
         return Predefined.Detection.StabilizationTime;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_JUMPMODE :
      {
         return Predefined.Detection.Step;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_ABOVE_FEMALES :
      {
         return Predefined.Acceptance.Female.MarginAbove;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_ABOVE_MALES :
      {
         return Predefined.Acceptance.Male.MarginAbove;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_BELOW_FEMALES :
      {
         return Predefined.Acceptance.Female.MarginBelow;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_BELOW_MALES :
      {
         return Predefined.Acceptance.Male.MarginBelow;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_UNIFORMITYRANGE :
      {
         return Predefined.Statistic.UniformityRange;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_HISTOGRAMRANGE :
      {
         return Predefined.Statistic.Histogram.Range;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_NAME :
      {
         strncpy( (void *)WriteValuesBuffer.CharacterBuffer, Predefined.Name, WEIGHING_CONFIGURATION_NAME_SIZE + 1);
         return 1;
      }
      case MODBUS_REG_PREDEFINED_WEIGHINGS_FLOCK_NAME :
      {
         strncpy( (void *)WriteValuesBuffer.CharacterBuffer, Predefined.Flock, WEIGHING_CONFIGURATION_FLOCK_SIZE + 1);
         return 1;
      }

      default :
         return 0;
   }

   return 0;
}

//------------------------------------------------------------------------------
//  Write Predefined weighings register
//------------------------------------------------------------------------------
TYesNo ModbusRegWritePredefinedWeighings( EModbusRegNum R, word D)
// Write Predefined weighings register group
{
TPredefinedList PredefList;
UDateTime DateTime;

   // Check value correctness
   if( !ModbusRegRangeCheckPredefinedWeighings( R, D)){
      return NO;
   }
   // String NAME
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_NAME){
      strncpy( Predefined.Name, (void *)WriteValuesBuffer.CharacterBuffer, WEIGHING_CONFIGURATION_NAME_SIZE + 1);
      return YES;
   }
   // String FLOCK_NAME
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_FLOCK_NAME){
      strncpy( Predefined.Flock, (void *)WriteValuesBuffer.CharacterBuffer, WEIGHING_CONFIGURATION_FLOCK_SIZE + 1);
      return YES;
   }
   // Command CREATE
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_CREATE){
      if( !(uDirectoryCount( uNamedListDirectory( &PredefList)) < uDirectoryCapacity( uNamedListDirectory( &PredefList)) && uDirectoryFind( uNamedListDirectory( &PredefList), Predefined.Name) == UDIRECTORY_INDEX_INVALID)){
         return NO;
      }
      while( PredefinedListOpen( &PredefList));
      _PredefinedWeighingsPredefId = uNamedListAdd( &PredefList, Predefined.Name,  (char *)&Predefined + PREDEFINED_WEIGHING_NAME_SIZE + 1);
      PredefinedListClose( &PredefList);

      return YES;
   }
   // Command SAVE
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_SAVE){
      if( !(_PredefinedWeighingsPredefId > 0)){
         return NO;
      }
      while( PredefinedListOpen( &PredefList));
      PredefinedListSave( &PredefList, &Predefined, _PredefinedWeighingsPredefId);
      PredefinedListClose( &PredefList);

      return YES;
   }
   // Command DELETE
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_DELETE){
      if( !(_PredefinedWeighingsPredefId > 0)){
         return NO;
      }
      while( PredefinedListOpen( &PredefList));
      uNamedListDelete( &PredefList, _PredefinedWeighingsPredefId);
      PredefinedListClose( &PredefList);

      return YES;
   }
   // Command SET_DEFAULT
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_SET_DEFAULT){
      if( !(_PredefinedWeighingsPredefId > 0)){
         return NO;
      }
      while( PredefinedListOpen( &PredefList));
      DefaultWeighingSet(&PredefList, _PredefinedWeighingsPredefId);
      PredefinedListClose( &PredefList);

      return YES;
   }
   // Command DISABLE_DEFAULT
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_DISABLE_DEFAULT){
      if( !(DefaultWeighingGet() != ULIST_IDENTIFIER_INVALID)){
         return NO;
      }
      while( PredefinedListOpen( &PredefList));
      DefaultWeighingSet(&PredefList,  ULIST_IDENTIFIER_INVALID);
      PredefinedListClose( &PredefList);

      return YES;
   }
   // Global variable _PredefinedWeighingsPredefId
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_PREDEF_ID){
      if( !(D < uDirectoryCount( uNamedListDirectory( &PredefList)))){
         return NO;
      }
      _PredefinedWeighingsPredefId = D;
      while( PredefinedListOpen( &PredefList));
      PredefinedListLoad(  &PredefList, &Predefined, _PredefinedWeighingsPredefId);
      PredefinedListClose( &PredefList);
      return YES;
   }
   // R/W registers 
   // register MENU_CHANGES
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_MENU_CHANGES){    
            
      Predefined.MenuMask = D;      
      
      return YES;
   }
   // register INITIALDAYNUMBER
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_INITIALDAYNUMBER){    
            
      Predefined.InitialDay = D;      
      
      return YES;
   }
   // register DAYSTART
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_DAYSTART){    
      uTime( &DateTime.Time, Predefined.DayStart);      
      DateTime.Time.Hour = D;      
      Predefined.DayStart = uTimeGauge( &DateTime.Time);
      return YES;
   }
   // register USEBOTHGENDERS
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_USEBOTHGENDERS){    
            
      Predefined.TargetWeights.SexDifferentiation = D;      
      
      return YES;
   }
   // register INITIALWFEMALES
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_INITIALWFEMALES){    
            
      Predefined.Female.InitialWeight = D;      
      
      return YES;
   }
   // register INITIALWMALES
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_INITIALWMALES){    
            
      Predefined.Male.InitialWeight = D;      
      
      return YES;
   }
   // register USECURVES
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_USECURVES){    
            
      Predefined.TargetWeights.Mode = D;      
      
      return YES;
   }
   // register MODE
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_MODE){    
            
      Predefined.TargetWeights.Growth = D;      
      
      return YES;
   }
   // register WEIGH_PLAN_PREDEFINED
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_WEIGH_PLAN_PREDEFINED){    
            
      Predefined.WeighingPlan = D;      
      
      return YES;
   }
   // register CURVE_F_PREDEFINED
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_CURVE_F_PREDEFINED){    
            
      Predefined.Female.GrowthCurve = D;      
      
      return YES;
   }
   // register CURVE_M_PREDEFINED
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_CURVE_M_PREDEFINED){    
            
      Predefined.Male.GrowthCurve = D;      
      
      return YES;
   }
   // register FILTER
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_FILTER){    
            
      Predefined.Detection.Filter = D;      
      
      return YES;
   }
   // register STABILIZATIONRANGE
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_STABILIZATIONRANGE){    
            
      Predefined.Detection.StabilizationRange = D;      
      
      return YES;
   }
   // register STABILIZATIONTIME
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_STABILIZATIONTIME){    
            
      Predefined.Detection.StabilizationTime = D;      
      
      return YES;
   }
   // register JUMPMODE
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_JUMPMODE){    
            
      Predefined.Detection.Step = D;      
      
      return YES;
   }
   // register MARGIN_ABOVE_FEMALES
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_ABOVE_FEMALES){    
            
      Predefined.Acceptance.Female.MarginAbove = D;      
      
      return YES;
   }
   // register MARGIN_ABOVE_MALES
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_ABOVE_MALES){    
            
      Predefined.Acceptance.Male.MarginAbove = D;      
      
      return YES;
   }
   // register MARGIN_BELOW_FEMALES
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_BELOW_FEMALES){    
            
      Predefined.Acceptance.Female.MarginBelow = D;      
      
      return YES;
   }
   // register MARGIN_BELOW_MALES
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_BELOW_MALES){    
            
      Predefined.Acceptance.Male.MarginBelow = D;      
      
      return YES;
   }
   // register UNIFORMITYRANGE
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_UNIFORMITYRANGE){    
            
      Predefined.Statistic.UniformityRange = D;      
      
      return YES;
   }
   // register HISTOGRAMRANGE
   if( R == MODBUS_REG_PREDEFINED_WEIGHINGS_HISTOGRAMRANGE){    
            
      Predefined.Statistic.Histogram.Range = D;      
      
      return YES;
   }
   return NO;
}




