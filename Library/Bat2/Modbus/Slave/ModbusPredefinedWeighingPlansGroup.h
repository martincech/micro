//******************************************************************************
//
//   ModbusPredefinedWeighingPlansGroup.h  Modbus Predefined Weighing Plans register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ModbusPredefinedWeighingPlansGroup_H__
   #define __ModbusPredefinedWeighingPlansGroup_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif
#ifndef __ModbusReg_H__
   #include "ModbusReg.h"
#endif










//------------------------------------------------------------------------------
word ModbusRegReadPredefinedWeighingPlans( EModbusRegNum R);
// Read Predefined Weighing Plans register group


TYesNo ModbusRegWritePredefinedWeighingPlans( EModbusRegNum R, word D);
// Write Predefined Weighing Plans register group



#endif
