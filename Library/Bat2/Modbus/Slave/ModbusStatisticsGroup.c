//******************************************************************************
//
//   ModbusStatisticsGroup.c      Modbus Statistics register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "ModbusStatisticsGroup.h"
#include "Archive/Archive.h"
#include "ModbusRegRangeCheck.h"


static TDayNumber   _StatisticsDaynumber = 0; // Current statistic day number
static TStatistic StatisticMale;
static TStatistic StatisticFemale;
static THistogram HistogramMale;
static THistogram HistogramFemale;
static TArchiveIndex MarkerIndex;
static TArchive Archive;
static TArchiveItem ArchiveItem;


// Locals :


//------------------------------------------------------------------------------
//  Read Statistics register
//------------------------------------------------------------------------------
word ModbusRegReadStatistics( EModbusRegNum R)
// Read Statistics register group
{
int i;
   if( R >= MODBUS_REG_STATISTICS_HIST_F_COUNT_1 && R <= MODBUS_REG_STATISTICS_HIST_F_COUNT_39){
      i = R - MODBUS_REG_STATISTICS_HIST_F_COUNT_1;
      return HistogramFemale.Slot[ i];
   }
   if( R >= MODBUS_REG_STATISTICS_HIST_M_COUNT_1 && R <= MODBUS_REG_STATISTICS_HIST_M_COUNT_39){
      i = R - MODBUS_REG_STATISTICS_HIST_M_COUNT_1;
      return HistogramMale.Slot[ i];
   }
   switch ( R){
      case MODBUS_REG_STATISTICS_STAT_F_COUNT :
      {
         return StatisticFemale.Count;
      }
      case MODBUS_REG_STATISTICS_STAT_M_COUNT :
      {
         return StatisticMale.Count;
      }
      case MODBUS_REG_STATISTICS_LASTAVERAGE_F :
      {
         return StatisticFemale.LastAverage;
      }
      case MODBUS_REG_STATISTICS_LASTAVERAGE_M :
      {
         return StatisticMale.LastAverage;
      }
      case MODBUS_REG_STATISTICS_TARGETWEIGHT_F :
      {
         return StatisticFemale.Target;
      }
      case MODBUS_REG_STATISTICS_TARGETWEIGHT_M :
      {
         return StatisticMale.Target;
      }
      case MODBUS_REG_STATISTICS_REALUNIFORMITY_F :
      {
         return StatisticFemale.Uniformity;
      }
      case MODBUS_REG_STATISTICS_REALUNIFORMITY_M :
      {
         return StatisticMale.Uniformity;
      }
      case MODBUS_REG_STATISTICS_HIST_F_CENTER_HI :
      {
         return HistogramFemale.Center >> 16;
      }
      case MODBUS_REG_STATISTICS_HIST_F_CENTER_LO :
      {
         return HistogramFemale.Center;
      }
      case MODBUS_REG_STATISTICS_HIST_F_STEP_HI :
      {
         return HistogramFemale.Step >> 16;
      }
      case MODBUS_REG_STATISTICS_HIST_F_STEP_LO :
      {
         return HistogramFemale.Step;
      }
      case MODBUS_REG_STATISTICS_HIST_M_CENTER_HI :
      {
         return HistogramMale.Center >> 16;
      }
      case MODBUS_REG_STATISTICS_HIST_M_CENTER_LO :
      {
         return HistogramMale.Center;
      }
      case MODBUS_REG_STATISTICS_HIST_M_STEP_HI :
      {
         return HistogramMale.Step >> 16;
      }
      case MODBUS_REG_STATISTICS_HIST_M_STEP_LO :
      {
         return HistogramMale.Step;
      }

      default :
         return 0;
   }

   return 0;
}

//------------------------------------------------------------------------------
//  Write Statistics register
//------------------------------------------------------------------------------
TYesNo ModbusRegWriteStatistics( EModbusRegNum R, word D)
// Write Statistics register group
{
   // Check value correctness
   if( !ModbusRegRangeCheckStatistics( R, D)){
      return NO;
   }
   // Global variable _StatisticsDaynumber
   if( R == MODBUS_REG_STATISTICS_DAYNUMBER){
      if( !(ArchiveSearch( &Archive, D) != ARCHIVE_INDEX_INVALID)){
         return NO;
      }
      _StatisticsDaynumber = D;
      while(!ArchiveOpen( &Archive));
      MarkerIndex = ArchiveSearch( &Archive, _StatisticsDaynumber);
      ArchiveGet( &Archive,  MarkerIndex + 1, &ArchiveItem);
      ArchiveStatistic( &StatisticMale, &ArchiveItem);
      ArchiveHistogram( &HistogramMale, &ArchiveItem);
      if( !ArchiveGet( &Archive,  MarkerIndex + 2, &ArchiveItem) || ArchiveIsMarker(&ArchiveItem)){memset( &StatisticFemale, 0, sizeof(TStatistic));
      memset( &HistogramFemale, 0, sizeof(THistogram));
      } else {ArchiveStatistic( &StatisticFemale, &ArchiveItem);
      ArchiveHistogram( &HistogramFemale, &ArchiveItem);
      }ArchiveGet( &Archive,  MarkerIndex, &ArchiveItem);
      StatisticMale.Target = ArchiveItem.Marker.TargetWeight;
      StatisticFemale.Target = ArchiveItem.Marker.TargetWeightFemale;
      ArchiveClose( &Archive);
      return YES;
   }
   return NO;
}




