//******************************************************************************
//
//   ModbusConfigurationGroup.h  Modbus Configuration register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ModbusConfigurationGroup_H__
   #define __ModbusConfigurationGroup_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif
#ifndef __ModbusReg_H__
   #include "ModbusReg.h"
#endif










//------------------------------------------------------------------------------
word ModbusRegReadConfiguration( EModbusRegNum R);
// Read Configuration register group


TYesNo ModbusRegWriteConfiguration( EModbusRegNum R, word D);
// Write Configuration register group



#endif
