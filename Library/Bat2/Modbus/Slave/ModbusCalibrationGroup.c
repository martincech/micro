//******************************************************************************
//
//   ModbusCalibrationGroup.c      Modbus Calibration register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "ModbusCalibrationGroup.h"
#include "Calibration/Calibration.h"
#include "Weight/Weight.h"
#include "ModbusRegRangeCheck.h"




// Locals :


//------------------------------------------------------------------------------
//  Read Calibration register
//------------------------------------------------------------------------------
word ModbusRegReadCalibration( EModbusRegNum R)
// Read Calibration register group
{
   switch ( R){
      case MODBUS_REG_CALIBRATION_ZEROCAL_HI :
      {
         return 0;
      }
      case MODBUS_REG_CALIBRATION_ZEROCAL_LO :
      {
         return 0;
      }
      case MODBUS_REG_CALIBRATION_RANGECAL_HI :
      {
         return 0;
      }
      case MODBUS_REG_CALIBRATION_RANGECAL_LO :
      {
         return 0;
      }
      case MODBUS_REG_CALIBRATION_RANGE_HI :
      {
         return 0;
      }
      case MODBUS_REG_CALIBRATION_RANGE_LO :
      {
         return 0;
      }
      case MODBUS_REG_CALIBRATION_DIVISION :
      {
         return WeightUnits.Division;
      }

      default :
         return 0;
   }

   return 0;
}

//------------------------------------------------------------------------------
//  Write Calibration register
//------------------------------------------------------------------------------
TYesNo ModbusRegWriteCalibration( EModbusRegNum R, word D)
// Write Calibration register group
{
   return NO;
}




