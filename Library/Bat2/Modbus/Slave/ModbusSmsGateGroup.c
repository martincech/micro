//******************************************************************************
//
//   ModbusSmsGateGroup.c      Modbus SMS Gate register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "ModbusSmsGateGroup.h"
#include "SmsGate/SmsGateDef.h"
#include "SmsGate/SmsGate.h"
#include "Gsm/GsmDef.h"
#include "ModbusRegRangeCheck.h"
#include <string.h>


static TSmsGateSlotNumber   _SmsGateSmsSlotNumber = 0; // Read slot number of last sended sms, write sms slot number for which status will be read
static char Sms[GSM_SMS_SIZE];
static char PhoneNumber[GSM_PHONE_NUMBER_SIZE];


// Locals :


//------------------------------------------------------------------------------
//  Read SMS Gate register
//------------------------------------------------------------------------------
word ModbusRegReadSmsGate( EModbusRegNum R)
// Read SMS Gate register group
{
ESmsGateSmsStatus Status;

   switch ( R){
      case MODBUS_REG_SMS_GATE_SMS_STATUS :
      {
         Status = SmsGateSmsStatus( _SmsGateSmsSlotNumber);
         return Status;
      }

      default :
         return 0;
   }

   return 0;
}

//------------------------------------------------------------------------------
//  Write SMS Gate register
//------------------------------------------------------------------------------
TYesNo ModbusRegWriteSmsGate( EModbusRegNum R, word D)
// Write SMS Gate register group
{
   // Check value correctness
   if( !ModbusRegRangeCheckSmsGate( R, D)){
      return NO;
   }
   // String SMS_TEXT
   if( R == MODBUS_REG_SMS_GATE_SMS_TEXT){
      strncpy( Sms, (void *)WriteValuesBuffer.CharacterBuffer, GSM_SMS_SIZE);
      return YES;
   }
   // String SMS_PHONE_NUMBER
   if( R == MODBUS_REG_SMS_GATE_SMS_PHONE_NUMBER){
      strncpy( PhoneNumber, (void *)WriteValuesBuffer.CharacterBuffer, GSM_PHONE_NUMBER_SIZE);
      return YES;
   }
   // Command SEND
   if( R == MODBUS_REG_SMS_GATE_SEND){
      if( !(strlen( Sms) > 0)){
         return NO;
      }
      _SmsGateSmsSlotNumber = SmsGateSmsSend( Sms, NULL);

      return YES;
   }
   // Command SENDTO
   if( R == MODBUS_REG_SMS_GATE_SENDTO){
      if( !(strlen( PhoneNumber) > 0 && strlen( Sms) > 0)){
         return NO;
      }
      _SmsGateSmsSlotNumber = SmsGateSmsSend( Sms, PhoneNumber);

      return YES;
   }
   // Global variable _SmsGateSmsSlotNumber
   if( R == MODBUS_REG_SMS_GATE_SMS_SLOT_NUMBER){
      if( !(D < SMS_GATE_SMS_SLOTS)){
         return NO;
      }
      _SmsGateSmsSlotNumber = D;
      return YES;
   }
   return NO;
}




