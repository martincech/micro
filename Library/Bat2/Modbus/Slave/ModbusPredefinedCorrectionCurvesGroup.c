//******************************************************************************
//
//   ModbusPredefinedCorrectionCurvesGroup.c      Modbus Predefined Correction Curves register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "ModbusPredefinedCorrectionCurvesGroup.h"
#include "Curve/CorrectionCurveDef.h"
#include "Curve/CorrectionList.h"
#include "Data/uNamedList.h"
#include "ModbusRegRangeCheck.h"
#include <string.h>


static TCorrectionIndex   _PredefinedCorrectionCurvesCurveIndex = 0; // This curve index (create assign new index)
static TCorrectionCurve Curve;


// Locals :


//------------------------------------------------------------------------------
//  Read Predefined Correction Curves register
//------------------------------------------------------------------------------
word ModbusRegReadPredefinedCorrectionCurves( EModbusRegNum R)
// Read Predefined Correction Curves register group
{
int i;
TCorrectionList CorrectionList;

   if( R >= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_DAY_1 && R <= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_DAY_10){
      i = R - MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_DAY_1;
      return Curve.Point[ i].Day;
   }
   if( R >= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_FACTOR_1 && R <= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_FACTOR_10){
      i = R - MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_FACTOR_1;
      return Curve.Point[ i].Factor;
   }
   switch ( R){
      case MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_NAME :
      {
         strncpy( (void *)WriteValuesBuffer.CharacterBuffer, Curve.Name, CORRECTION_CURVE_NAME_SIZE);
         return 1;
      }

      default :
         return 0;
   }

   return 0;
}

//------------------------------------------------------------------------------
//  Write Predefined Correction Curves register
//------------------------------------------------------------------------------
TYesNo ModbusRegWritePredefinedCorrectionCurves( EModbusRegNum R, word D)
// Write Predefined Correction Curves register group
{
int i;
TCorrectionList CorrectionList;

   // Check value correctness
   if( !ModbusRegRangeCheckPredefinedCorrectionCurves( R, D)){
      return NO;
   }
   // String CURVE_NAME
   if( R == MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_NAME){
      strncpy( Curve.Name, (void *)WriteValuesBuffer.CharacterBuffer, CORRECTION_CURVE_NAME_SIZE);
      return YES;
   }
   // Command CREATE
   if( R == MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CREATE){
      if( !(uDirectoryCount( uNamedListDirectory( &CorrectionList)) < uDirectoryCapacity( uNamedListDirectory( &CorrectionList)) && uDirectoryFind( uNamedListDirectory( &CorrectionList), Curve.Name) == UDIRECTORY_INDEX_INVALID)){
         return NO;
      }
      while( !CorrectionListOpen( &CorrectionList));
      _PredefinedCorrectionCurvesCurveIndex = uNamedListAdd( &CorrectionList, Curve.Name,  (char *)&Curve + CORRECTION_CURVE_NAME_SIZE + 1);
      CorrectionListClose(&CorrectionList);

      return YES;
   }
   // Command SAVE
   if( R == MODBUS_REG_PREDEFINED_CORRECTION_CURVES_SAVE){
      if( !(_PredefinedCorrectionCurvesCurveIndex > 0)){
         return NO;
      }
      while( !CorrectionListOpen( &CorrectionList));
      CorrectionListSave( &CorrectionList, &Curve, _PredefinedCorrectionCurvesCurveIndex);
      CorrectionListClose(&CorrectionList);

      return YES;
   }
   // Command DELETE
   if( R == MODBUS_REG_PREDEFINED_CORRECTION_CURVES_DELETE){
      if( !(_PredefinedCorrectionCurvesCurveIndex > 0)){
         return NO;
      }
      while( !CorrectionListOpen( &CorrectionList));
      uNamedListDelete( &CorrectionList,  _PredefinedCorrectionCurvesCurveIndex);
      CorrectionListClose( &CorrectionList);

      return YES;
   }
   // Global variable _PredefinedCorrectionCurvesCurveIndex
   if( R == MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_INDEX){
      if( !(D < uDirectoryCount( uNamedListDirectory( &CorrectionList)))){
         return NO;
      }
      _PredefinedCorrectionCurvesCurveIndex = D;
      while( !CorrectionListOpen( &CorrectionList));
      CorrectionListLoad( &CorrectionList, &Curve, _PredefinedCorrectionCurvesCurveIndex);
      CorrectionListClose( &CorrectionList);
      return YES;
   }
   // R/W registers 
   // register array CURVE_DAY
   if( R >= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_DAY_1 && R <= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_DAY_10){
      i = R - MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_DAY_1; 
      
      Curve.Point[ i].Day = D;
      
      return YES;
   }
   // register array CURVE_FACTOR
   if( R >= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_FACTOR_1 && R <= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_FACTOR_10){
      i = R - MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_FACTOR_1; 
      
      Curve.Point[ i].Factor = D;
      
      return YES;
   }
   return NO;
}




