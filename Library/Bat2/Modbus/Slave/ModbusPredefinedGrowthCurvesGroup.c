//******************************************************************************
//
//   ModbusPredefinedGrowthCurvesGroup.c      Modbus Predefined Growth Curves register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "ModbusPredefinedGrowthCurvesGroup.h"
#include "Curve/GrowthCurveDef.h"
#include "Curve/CurveList.h"
#include "Data/uNamedList.h"
#include "ModbusRegRangeCheck.h"
#include <string.h>


static TCurveIndex   _PredefinedGrowthCurvesCurveIndex = 0; // This curve index (create assign new index)
static TGrowthCurve Curve;


// Locals :


//------------------------------------------------------------------------------
//  Read Predefined Growth Curves register
//------------------------------------------------------------------------------
word ModbusRegReadPredefinedGrowthCurves( EModbusRegNum R)
// Read Predefined Growth Curves register group
{
int i;
TCurveList CurveList;

   if( R >= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_DAY_1 && R <= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_DAY_30){
      i = R - MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_DAY_1;
      return Curve.Point[ i].Day;
   }
   if( R >= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_WEIGHT_1 && R <= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_WEIGHT_30){
      i = R - MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_WEIGHT_1;
      return Curve.Point[ i].Weight;
   }
   switch ( R){
      case MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_NAME :
      {
         strncpy( (void *)WriteValuesBuffer.CharacterBuffer, Curve.Name, GROWTH_CURVE_NAME_SIZE);
         return 1;
      }

      default :
         return 0;
   }

   return 0;
}

//------------------------------------------------------------------------------
//  Write Predefined Growth Curves register
//------------------------------------------------------------------------------
TYesNo ModbusRegWritePredefinedGrowthCurves( EModbusRegNum R, word D)
// Write Predefined Growth Curves register group
{
int i;
TCurveList CurveList;

   // Check value correctness
   if( !ModbusRegRangeCheckPredefinedGrowthCurves( R, D)){
      return NO;
   }
   // String CURVE_NAME
   if( R == MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_NAME){
      strncpy( Curve.Name, (void *)WriteValuesBuffer.CharacterBuffer, GROWTH_CURVE_NAME_SIZE);
      return YES;
   }
   // Command CREATE
   if( R == MODBUS_REG_PREDEFINED_GROWTH_CURVES_CREATE){
      if( !(uDirectoryCount( uNamedListDirectory( &CurveList)) < uDirectoryCapacity( uNamedListDirectory( &CurveList)) && uDirectoryFind( uNamedListDirectory( &CurveList), Curve.Name) == UDIRECTORY_INDEX_INVALID)){
         return NO;
      }
      while( !CurveListOpen( &CurveList));
      _PredefinedGrowthCurvesCurveIndex = uNamedListAdd( &CurveList, Curve.Name,  (char *)&Curve + GROWTH_CURVE_NAME_SIZE + 1);
      CurveListClose( &CurveList);

      return YES;
   }
   // Command SAVE
   if( R == MODBUS_REG_PREDEFINED_GROWTH_CURVES_SAVE){
      if( !(_PredefinedGrowthCurvesCurveIndex > 0)){
         return NO;
      }
      while( !CurveListOpen( &CurveList));
      CurveListSave(  &CurveList, &Curve, _PredefinedGrowthCurvesCurveIndex);
      CurveListClose( &CurveList);

      return YES;
   }
   // Command DELETE
   if( R == MODBUS_REG_PREDEFINED_GROWTH_CURVES_DELETE){
      if( !(_PredefinedGrowthCurvesCurveIndex > 0)){
         return NO;
      }
      while( !CurveListOpen( &CurveList));
      uNamedListDelete(&CurveList,  _PredefinedGrowthCurvesCurveIndex);
      CurveListClose( &CurveList);

      return YES;
   }
   // Global variable _PredefinedGrowthCurvesCurveIndex
   if( R == MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_INDEX){
      if( !(D < uDirectoryCount( uNamedListDirectory( &CurveList)))){
         return NO;
      }
      _PredefinedGrowthCurvesCurveIndex = D;
      while( !CurveListOpen( &CurveList));
      CurveListLoad(  &CurveList, &Curve, _PredefinedGrowthCurvesCurveIndex);
      CurveListClose( &CurveList);
      return YES;
   }
   // R/W registers 
   // register array CURVE_DAY
   if( R >= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_DAY_1 && R <= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_DAY_30){
      i = R - MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_DAY_1; 
      
      Curve.Point[ i].Day = D;
      
      return YES;
   }
   // register array CURVE_WEIGHT
   if( R >= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_WEIGHT_1 && R <= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_WEIGHT_30){
      i = R - MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_WEIGHT_1; 
      
      Curve.Point[ i].Weight = D;
      
      return YES;
   }
   return NO;
}




