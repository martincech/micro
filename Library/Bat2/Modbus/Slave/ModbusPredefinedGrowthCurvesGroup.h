//******************************************************************************
//
//   ModbusPredefinedGrowthCurvesGroup.h  Modbus Predefined Growth Curves register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ModbusPredefinedGrowthCurvesGroup_H__
   #define __ModbusPredefinedGrowthCurvesGroup_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif
#ifndef __ModbusReg_H__
   #include "ModbusReg.h"
#endif










//------------------------------------------------------------------------------
word ModbusRegReadPredefinedGrowthCurves( EModbusRegNum R);
// Read Predefined Growth Curves register group


TYesNo ModbusRegWritePredefinedGrowthCurves( EModbusRegNum R, word D);
// Write Predefined Growth Curves register group



#endif
