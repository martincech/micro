//******************************************************************************
//
//   ModbusGsmContactsGroup.h  Modbus Gsm Contacts register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ModbusGsmContactsGroup_H__
   #define __ModbusGsmContactsGroup_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif
#ifndef __ModbusReg_H__
   #include "ModbusReg.h"
#endif










//------------------------------------------------------------------------------
word ModbusRegReadGsmContacts( EModbusRegNum R);
// Read Gsm Contacts register group


TYesNo ModbusRegWriteGsmContacts( EModbusRegNum R, word D);
// Write Gsm Contacts register group



#endif
