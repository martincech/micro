//******************************************************************************
//
//   ModbusCalibrationGroup.h  Modbus Calibration register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ModbusCalibrationGroup_H__
   #define __ModbusCalibrationGroup_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif
#ifndef __ModbusReg_H__
   #include "ModbusReg.h"
#endif










//------------------------------------------------------------------------------
word ModbusRegReadCalibration( EModbusRegNum R);
// Read Calibration register group



#endif
