//******************************************************************************
//
//   ModbusConfigurationGroup.c      Modbus Configuration register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "ModbusConfigurationGroup.h"
#include "Config/Config.h"
#include "ModbusReg.h"
#include "Display/DisplayConfiguration.h"
#include "Weight/Weight.h"
#include "Country/Country.h"
#include "Str.h"
#include "Modbus/ModbusConfig.h"
#include "System/System.h"
#include "Message/MGsm.h"
#include "Message/GsmMessage.h"
#include "ModbusRegRangeCheck.h"
#include <string.h>




// Locals :
static void _WeighingSaveCommandExecutive( void);
// execute  command - apply all parameteres

static void _GsmSaveCommandExecutive( void);
// execute  command - apply all parameteres

static void _DisplaySaveCommandExecutive( void);
// execute  command - apply all parameteres

static void _DateTimeSaveCommandExecutive( void);
// execute  command - apply all parameteres

static void _LocalizationSaveCommandExecutive( void);
// execute  command - apply all parameteres

static int _GetRegisterMapping( EModbusRegNum R);
// return mapping of discontinuous register number to contiuous array



//------------------------------------------------------------------------------
//  Read Configuration register
//------------------------------------------------------------------------------
word ModbusRegReadConfiguration( EModbusRegNum R)
// Read Configuration register group
{
int i;
UDateTimeGauge Local;
UDateTime DateTime;
char   Operator[ GSM_OPERATOR_SIZE + 1];
UTime Time;

   if( R >= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_HOUR_1 && R <= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_HOUR_10){
      i = R - MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_HOUR_1;
      uTime( &Time, GsmMessage.PowerOptions.Times[ i].From);
      return Time.Hour;
   }
   if( R >= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_MIN_1 && R <= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_MIN_10){
      i = R - MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_MIN_1;
      uTime( &Time, GsmMessage.PowerOptions.Times[ i].From);
      return Time.Min;
   }
   if( R >= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_HOUR_1 && R <= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_HOUR_10){
      i = R - MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_HOUR_1;
      uTime( &Time, GsmMessage.PowerOptions.Times[ i].To);
      return Time.Hour;
   }
   if( R >= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_MIN_1 && R <= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_MIN_10){
      i = R - MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_MIN_1;
      uTime( &Time, GsmMessage.PowerOptions.Times[ i].To);
      return Time.Min;
   }
   switch ( R){
      case MODBUS_REG_CONFIGURATION_VERSION :
      {
         return Bat2Version.Software;
      }
      case MODBUS_REG_CONFIGURATION_MODIFICATION :
      {
         return Bat2Version.Modification;
      }
      case MODBUS_REG_CONFIGURATION_CLASS :
      {
         return Bat2Version.Class;
      }
      case MODBUS_REG_CONFIGURATION_HWVERSION :
      {
         return Bat2Version.Hardware;
      }
      case MODBUS_REG_CONFIGURATION_MODBUS_VERSION_MAJOR :
      {
         return ModbusVersion.Major;
      }
      case MODBUS_REG_CONFIGURATION_MODBUS_VERSION_MINOR :
      {
         return ModbusVersion.Minor;
      }
      case MODBUS_REG_CONFIGURATION_SN_HI :
      {
         return ConfigurationSerialNumber();
      }
      case MODBUS_REG_CONFIGURATION_SN_LO :
      {
         return ConfigurationSerialNumber();
      }
      case MODBUS_REG_CONFIGURATION_DISPLAY_BACKLIGHT :
      {
         return DisplayConfiguration.Backlight.Intensity;
      }
      case MODBUS_REG_CONFIGURATION_DISPLAY_CONTRAST :
      {
         return DisplayConfiguration.Contrast;
      }
      case MODBUS_REG_CONFIGURATION_DISPLAY_MODE :
      {
         return DisplayConfiguration.Mode;
      }
      case MODBUS_REG_CONFIGURATION_WEIGHING_UNITS :
      {
         return WeightUnits.Units;
      }
      case MODBUS_REG_CONFIGURATION_WEIGHING_DIVISION :
      {
         return WeightUnits.Division;
      }
      case MODBUS_REG_CONFIGURATION_WEIGHING_RANGE :
      {
         return WeightUnits.Range;
      }
      case MODBUS_REG_CONFIGURATION_LOCALIZATION_LANGUAGE :
      {
         return Country.Language;
      }
      case MODBUS_REG_CONFIGURATION_LOCALIZATION_COUNTRY :
      {
         return Country.Country;
      }
      case MODBUS_REG_CONFIGURATION_LOCALIZATION_DATE_FORMAT :
      {
         return Country.Locale.DateFormat;
      }
      case MODBUS_REG_CONFIGURATION_LOCALIZATION_DATE_SEPARATOR_FIRST :
      {
         return Country.Locale.DateSeparator1;
      }
      case MODBUS_REG_CONFIGURATION_LOCALIZATION_DATE_SEPARATOR_SECOND :
      {
         return Country.Locale.DateSeparator2;
      }
      case MODBUS_REG_CONFIGURATION_LOCALIZATION_TIME_FORMAT :
      {
         return Country.Locale.TimeFormat;
      }
      case MODBUS_REG_CONFIGURATION_LOCALIZATION_TIME_SEPARATOR :
      {
         return Country.Locale.TimeSeparator;
      }
      case MODBUS_REG_CONFIGURATION_LOCALIZATION_DAYLIGHT_SAVING :
      {
         return Country.Locale.DaylightSavingType;
      }
      case MODBUS_REG_CONFIGURATION_RS485_ADDRESS :
      {
         return ModbusOptions.Address;
      }
      case MODBUS_REG_CONFIGURATION_RS485_SPEED_HI :
      {
         return ModbusOptions.BaudRate;
      }
      case MODBUS_REG_CONFIGURATION_RS485_SPEED_LO :
      {
         return ModbusOptions.BaudRate;
      }
      case MODBUS_REG_CONFIGURATION_RS485_PARITY :
      {
         return ModbusOptions.Parity;
      }
      case MODBUS_REG_CONFIGURATION_RS485_PROTOCOL :
      {
         return ModbusOptions.Mode;
      }
      case MODBUS_REG_CONFIGURATION_DATE_TIME_DAY :
      {
         Local = SysDateTime();
         uDateTime( &DateTime, Local);
         return DateTime.Date.Day;
      }
      case MODBUS_REG_CONFIGURATION_DATE_TIME_MONTH :
      {
         Local = SysDateTime();
         uDateTime( &DateTime, Local);
         return DateTime.Date.Month;
      }
      case MODBUS_REG_CONFIGURATION_DATE_TIME_YEAR :
      {
         Local = SysDateTime();
         uDateTime( &DateTime, Local);
         return DateTime.Date.Year;
      }
      case MODBUS_REG_CONFIGURATION_DATE_TIME_HOUR :
      {
         Local = SysDateTime();
         uDateTime( &DateTime, Local);
         return DateTime.Time.Hour;
      }
      case MODBUS_REG_CONFIGURATION_DATE_TIME_MIN :
      {
         Local = SysDateTime();
         uDateTime( &DateTime, Local);
         return DateTime.Time.Min;
      }
      case MODBUS_REG_CONFIGURATION_GSM_INFO_PIN_STATE :
      {
         return MGsmPinReady() == YES? 1 : 0;
      }
      case MODBUS_REG_CONFIGURATION_GSM_INFO_REGISTERED :
      {
         return MGsmRegistered() == YES? 1 : 0;
      }
      case MODBUS_REG_CONFIGURATION_GSM_INFO_SIGNAL_STRENGTH :
      {
         return MGsmSignalStrength();
      }
      case MODBUS_REG_CONFIGURATION_GSM_POWER_OPTIONS :
      {
         return GsmMessage.PowerOptions.Mode;
      }
      case MODBUS_REG_CONFIGURATION_GSM_POWER_SWITCH_ON_PERIOD :
      {
         return GsmMessage.PowerOptions.SwitchOnPeriod;
      }
      case MODBUS_REG_CONFIGURATION_GSM_POWER_SWITCH_ON_DURATION :
      {
         return GsmMessage.PowerOptions.SwitchOnDuration;
      }
      case MODBUS_REG_CONFIGURATION_GSM_POWER_TIMES_COUNT :
      {
         return GsmMessage.PowerOptions.TimesCount;
      }
      case MODBUS_REG_CONFIGURATION_GSM_EVENTS_EVENT_MASK :
      {
         return GsmMessage.Events.EventMask;
      }
      case MODBUS_REG_CONFIGURATION_SCALE_NAME :
      {
         strncpy( (void *)WriteValuesBuffer.CharacterBuffer, Bat2Device.Name, DEVICE_NAME_SIZE);
         return 1;
      }
      case MODBUS_REG_CONFIGURATION_LOCALIZATION_COUNTRY_NAME :
      {
         strncpy( (void *)WriteValuesBuffer.CharacterBuffer, ENUM_COUNTRY + Country.Country, 30);
         return 1;
      }
      case MODBUS_REG_CONFIGURATION_GSM_INFO_OPERATOR :
      {
         MGsmOperator( Operator);
         strncpy( (void *)WriteValuesBuffer.CharacterBuffer, Operator, GSM_OPERATOR_SIZE);
         return 1;
      }

      default :
         return 0;
   }

   return 0;
}

//------------------------------------------------------------------------------
//  Write Configuration register
//------------------------------------------------------------------------------
TYesNo ModbusRegWriteConfiguration( EModbusRegNum R, word D)
// Write Configuration register group
{
int i;
UDateTimeGauge Local;
UDateTime DateTime;
UTime Time;

   // Check value correctness
   if( !ModbusRegRangeCheckConfiguration( R, D)){
      return NO;
   }
   // String SCALE_NAME
   if( R == MODBUS_REG_CONFIGURATION_SCALE_NAME){
      strncpy( Bat2Device.Name, (void *)WriteValuesBuffer.CharacterBuffer, DEVICE_NAME_SIZE);
      return YES;
   }
   // Command DISPLAY_SAVE
   if( R == MODBUS_REG_CONFIGURATION_DISPLAY_SAVE){
      _DisplaySaveCommandExecutive();
      return YES;
   }
   // Command WEIGHING_SAVE
   if( R == MODBUS_REG_CONFIGURATION_WEIGHING_SAVE){
      _WeighingSaveCommandExecutive();
      return YES;
   }
   // Command LOCALIZATION_SAVE
   if( R == MODBUS_REG_CONFIGURATION_LOCALIZATION_SAVE){
      _LocalizationSaveCommandExecutive();
      return YES;
   }
   // Command DATE_TIME_SAVE
   if( R == MODBUS_REG_CONFIGURATION_DATE_TIME_SAVE){
      _DateTimeSaveCommandExecutive();
      return YES;
   }
   // Command GSM_SAVE
   if( R == MODBUS_REG_CONFIGURATION_GSM_SAVE){
      _GsmSaveCommandExecutive();
      return YES;
   }
   // R/W registers 
   // buffered registers
   if( R >=  MODBUS_REG_CONFIGURATION_DISPLAY_BACKLIGHT && R <= MODBUS_REG_CONFIGURATION_GSM_EVENTS_EVENT_MASK){
     i = _GetRegisterMapping( R);
     if( i < 0){ return NO; }
     WriteValuesBuffer.ConfigurationChange[ i >> 3] |= 1 << (7 - (i & 7));
     WriteValuesBuffer.Configuration[ i] = D;
     return YES;
   }
   return NO;
}



//------------------------------------------------------------------------------
//  COMMAND WEIGHING_SAVE executive
//------------------------------------------------------------------------------
static void _WeighingSaveCommandExecutive( void)
// execute  command - apply all parameteres
{
   // WEIGHING_UNITS
   if( WriteValuesBuffer.ConfigurationChange[ 0] & (1 << 7)){
      WeightUnits.Units = WriteValuesBuffer.Configuration[ 0];
      WriteValuesBuffer.ConfigurationChange[ 0] &= ~(1 << 7);
   }
   // WEIGHING_DIVISION
   if( WriteValuesBuffer.ConfigurationChange[ 0] & (1 << 6)){
      WeightUnits.Division = WriteValuesBuffer.Configuration[ 1];
      WriteValuesBuffer.ConfigurationChange[ 0] &= ~(1 << 6);
   }
   // WEIGHING_RANGE
   if( WriteValuesBuffer.ConfigurationChange[ 0] & (1 << 5)){
      WeightUnits.Range = WriteValuesBuffer.Configuration[ 2];
      WriteValuesBuffer.ConfigurationChange[ 0] &= ~(1 << 5);
   }
}

//------------------------------------------------------------------------------
//  COMMAND GSM_SAVE executive
//------------------------------------------------------------------------------
static void _GsmSaveCommandExecutive( void)
// execute  command - apply all parameteres
{
int i;
UTime Time;

   uTime( &Time, GsmMessage.PowerOptions.Times[i].From);
   // GSM_POWER_TIME_FROM_HOUR
   for( i = 0; i < 10; i++){
     int _ai = (3 + i >> 3) + 0;
     int _bi = 1 << (5 - ((i+1) & 7) & 7);
     if( WriteValuesBuffer.ConfigurationChange[ _ai] & _bi){
        Time.Hour = WriteValuesBuffer.Configuration[ i + 3];
        WriteValuesBuffer.ConfigurationChange[ _ai] &= ~_bi;
     }
   }
   // GSM_POWER_TIME_FROM_MIN
   for( i = 0; i < 10; i++){
     int _ai = (5 + i >> 3) + 1;
     int _bi = 1 << (3 - ((i+1) & 7) & 7);
     if( WriteValuesBuffer.ConfigurationChange[ _ai] & _bi){
        Time.Min = WriteValuesBuffer.Configuration[ i + 13];
        WriteValuesBuffer.ConfigurationChange[ _ai] &= ~_bi;
     }
   }
   uTime( &Time, GsmMessage.PowerOptions.Times[i].To);
   // GSM_POWER_TIME_TO_HOUR
   for( i = 0; i < 10; i++){
     int _ai = (7 + i >> 3) + 2;
     int _bi = 1 << (1 - ((i+1) & 7) & 7);
     if( WriteValuesBuffer.ConfigurationChange[ _ai] & _bi){
        Time.Hour = WriteValuesBuffer.Configuration[ i + 23];
        WriteValuesBuffer.ConfigurationChange[ _ai] &= ~_bi;
     }
   }
   // GSM_POWER_TIME_TO_MIN
   for( i = 0; i < 10; i++){
     int _ai = (1 + i >> 3) + 4;
     int _bi = 1 << (7 - ((i+1) & 7) & 7);
     if( WriteValuesBuffer.ConfigurationChange[ _ai] & _bi){
        Time.Min = WriteValuesBuffer.Configuration[ i + 33];
        WriteValuesBuffer.ConfigurationChange[ _ai] &= ~_bi;
     }
   }
   // GSM_POWER_OPTIONS
   if( WriteValuesBuffer.ConfigurationChange[ 5] & (1 << 4)){
      GsmMessage.PowerOptions.Mode = WriteValuesBuffer.Configuration[ 43];
      WriteValuesBuffer.ConfigurationChange[ 5] &= ~(1 << 4);
   }
   // GSM_POWER_SWITCH_ON_PERIOD
   if( WriteValuesBuffer.ConfigurationChange[ 5] & (1 << 3)){
      GsmMessage.PowerOptions.SwitchOnPeriod = WriteValuesBuffer.Configuration[ 44];
      WriteValuesBuffer.ConfigurationChange[ 5] &= ~(1 << 3);
   }
   // GSM_POWER_SWITCH_ON_DURATION
   if( WriteValuesBuffer.ConfigurationChange[ 5] & (1 << 2)){
      GsmMessage.PowerOptions.SwitchOnDuration = WriteValuesBuffer.Configuration[ 45];
      WriteValuesBuffer.ConfigurationChange[ 5] &= ~(1 << 2);
   }
   // GSM_POWER_TIMES_COUNT
   if( WriteValuesBuffer.ConfigurationChange[ 5] & (1 << 1)){
      GsmMessage.PowerOptions.TimesCount = WriteValuesBuffer.Configuration[ 46];
      WriteValuesBuffer.ConfigurationChange[ 5] &= ~(1 << 1);
   }
   // GSM_EVENTS_EVENT_MASK
   if( WriteValuesBuffer.ConfigurationChange[ 5] & (1 << 0)){
      GsmMessage.Events.EventMask = WriteValuesBuffer.Configuration[ 47];
      WriteValuesBuffer.ConfigurationChange[ 5] &= ~(1 << 0);
   }
}

//------------------------------------------------------------------------------
//  COMMAND DISPLAY_SAVE executive
//------------------------------------------------------------------------------
static void _DisplaySaveCommandExecutive( void)
// execute  command - apply all parameteres
{
   // DISPLAY_BACKLIGHT
   if( WriteValuesBuffer.ConfigurationChange[ 6] & (1 << 7)){
      DisplayConfiguration.Backlight.Intensity = WriteValuesBuffer.Configuration[ 48];
      WriteValuesBuffer.ConfigurationChange[ 6] &= ~(1 << 7);
   }
   // DISPLAY_CONTRAST
   if( WriteValuesBuffer.ConfigurationChange[ 6] & (1 << 6)){
      DisplayConfiguration.Contrast = WriteValuesBuffer.Configuration[ 49];
      WriteValuesBuffer.ConfigurationChange[ 6] &= ~(1 << 6);
   }
   // DISPLAY_MODE
   if( WriteValuesBuffer.ConfigurationChange[ 6] & (1 << 5)){
      DisplayConfiguration.Mode = WriteValuesBuffer.Configuration[ 50];
      WriteValuesBuffer.ConfigurationChange[ 6] &= ~(1 << 5);
   }
}

//------------------------------------------------------------------------------
//  COMMAND DATE_TIME_SAVE executive
//------------------------------------------------------------------------------
static void _DateTimeSaveCommandExecutive( void)
// execute  command - apply all parameteres
{
UDateTimeGauge Local;
UDateTime DateTime;

   Local = SysDateTime();
   uDateTime( &DateTime, Local);
   // DATE_TIME_DAY
   if( WriteValuesBuffer.ConfigurationChange[ 6] & (1 << 4)){
      DateTime.Date.Day = WriteValuesBuffer.Configuration[ 51];
      WriteValuesBuffer.ConfigurationChange[ 6] &= ~(1 << 4);
   }
   // DATE_TIME_MONTH
   if( WriteValuesBuffer.ConfigurationChange[ 6] & (1 << 3)){
      DateTime.Date.Month = WriteValuesBuffer.Configuration[ 52];
      WriteValuesBuffer.ConfigurationChange[ 6] &= ~(1 << 3);
   }
   // DATE_TIME_YEAR
   if( WriteValuesBuffer.ConfigurationChange[ 6] & (1 << 2)){
      DateTime.Date.Year = WriteValuesBuffer.Configuration[ 53];
      WriteValuesBuffer.ConfigurationChange[ 6] &= ~(1 << 2);
   }
   // DATE_TIME_HOUR
   if( WriteValuesBuffer.ConfigurationChange[ 6] & (1 << 1)){
      DateTime.Time.Hour = WriteValuesBuffer.Configuration[ 54];
      WriteValuesBuffer.ConfigurationChange[ 6] &= ~(1 << 1);
   }
   // DATE_TIME_MIN
   if( WriteValuesBuffer.ConfigurationChange[ 6] & (1 << 0)){
      DateTime.Time.Min = WriteValuesBuffer.Configuration[ 55];
      WriteValuesBuffer.ConfigurationChange[ 6] &= ~(1 << 0);
   }
   Local = uDateTimeGauge( &DateTime);
   SysDateTimeSet( Local);
}

//------------------------------------------------------------------------------
//  COMMAND LOCALIZATION_SAVE executive
//------------------------------------------------------------------------------
static void _LocalizationSaveCommandExecutive( void)
// execute  command - apply all parameteres
{
   // LOCALIZATION_LANGUAGE
   if( WriteValuesBuffer.ConfigurationChange[ 7] & (1 << 7)){
      Country.Language = WriteValuesBuffer.Configuration[ 56];
      WriteValuesBuffer.ConfigurationChange[ 7] &= ~(1 << 7);
   }
   // LOCALIZATION_COUNTRY
   if( WriteValuesBuffer.ConfigurationChange[ 7] & (1 << 6)){
      Country.Country = WriteValuesBuffer.Configuration[ 57];
      WriteValuesBuffer.ConfigurationChange[ 7] &= ~(1 << 6);
   }
   // LOCALIZATION_DATE_FORMAT
   if( WriteValuesBuffer.ConfigurationChange[ 7] & (1 << 5)){
      Country.Locale.DateFormat = WriteValuesBuffer.Configuration[ 58];
      WriteValuesBuffer.ConfigurationChange[ 7] &= ~(1 << 5);
   }
   // LOCALIZATION_DATE_SEPARATOR_FIRST
   if( WriteValuesBuffer.ConfigurationChange[ 7] & (1 << 4)){
      Country.Locale.DateSeparator1 = WriteValuesBuffer.Configuration[ 59];
      WriteValuesBuffer.ConfigurationChange[ 7] &= ~(1 << 4);
   }
   // LOCALIZATION_DATE_SEPARATOR_SECOND
   if( WriteValuesBuffer.ConfigurationChange[ 7] & (1 << 3)){
      Country.Locale.DateSeparator2 = WriteValuesBuffer.Configuration[ 60];
      WriteValuesBuffer.ConfigurationChange[ 7] &= ~(1 << 3);
   }
   // LOCALIZATION_TIME_FORMAT
   if( WriteValuesBuffer.ConfigurationChange[ 7] & (1 << 2)){
      Country.Locale.TimeFormat = WriteValuesBuffer.Configuration[ 61];
      WriteValuesBuffer.ConfigurationChange[ 7] &= ~(1 << 2);
   }
   // LOCALIZATION_TIME_SEPARATOR
   if( WriteValuesBuffer.ConfigurationChange[ 7] & (1 << 1)){
      Country.Locale.TimeSeparator = WriteValuesBuffer.Configuration[ 62];
      WriteValuesBuffer.ConfigurationChange[ 7] &= ~(1 << 1);
   }
   // LOCALIZATION_DAYLIGHT_SAVING
   if( WriteValuesBuffer.ConfigurationChange[ 7] & (1 << 0)){
      Country.Locale.DaylightSavingType = WriteValuesBuffer.Configuration[ 63];
      WriteValuesBuffer.ConfigurationChange[ 7] &= ~(1 << 0);
   }
}

//------------------------------------------------------------------------------
//  Register mapping
//------------------------------------------------------------------------------
static int _GetRegisterMapping( EModbusRegNum R)
// return mapping of discontinuous register number to contiuous array
{
int i;
   if( R >= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_HOUR_1 && R <= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_HOUR_10){
      i = R - MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_HOUR_1;
      return MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_HOUR_1 + 3;
   }
   if( R >= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_MIN_1 && R <= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_MIN_10){
      i = R - MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_MIN_1;
      return MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_MIN_1 + 13;
   }
   if( R >= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_HOUR_1 && R <= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_HOUR_10){
      i = R - MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_HOUR_1;
      return MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_HOUR_1 + 23;
   }
   if( R >= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_MIN_1 && R <= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_MIN_10){
      i = R - MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_MIN_1;
      return MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_MIN_1 + 33;
   }
   switch ( R){
      case MODBUS_REG_CONFIGURATION_WEIGHING_UNITS :
      {
         return 0;
      }
      case MODBUS_REG_CONFIGURATION_WEIGHING_DIVISION :
      {
         return 1;
      }
      case MODBUS_REG_CONFIGURATION_WEIGHING_RANGE :
      {
         return 2;
      }
      case MODBUS_REG_CONFIGURATION_GSM_POWER_OPTIONS :
      {
         return 43;
      }
      case MODBUS_REG_CONFIGURATION_GSM_POWER_SWITCH_ON_PERIOD :
      {
         return 44;
      }
      case MODBUS_REG_CONFIGURATION_GSM_POWER_SWITCH_ON_DURATION :
      {
         return 45;
      }
      case MODBUS_REG_CONFIGURATION_GSM_POWER_TIMES_COUNT :
      {
         return 46;
      }
      case MODBUS_REG_CONFIGURATION_GSM_EVENTS_EVENT_MASK :
      {
         return 47;
      }
      case MODBUS_REG_CONFIGURATION_DISPLAY_BACKLIGHT :
      {
         return 48;
      }
      case MODBUS_REG_CONFIGURATION_DISPLAY_CONTRAST :
      {
         return 49;
      }
      case MODBUS_REG_CONFIGURATION_DISPLAY_MODE :
      {
         return 50;
      }
      case MODBUS_REG_CONFIGURATION_DATE_TIME_DAY :
      {
         return 51;
      }
      case MODBUS_REG_CONFIGURATION_DATE_TIME_MONTH :
      {
         return 52;
      }
      case MODBUS_REG_CONFIGURATION_DATE_TIME_YEAR :
      {
         return 53;
      }
      case MODBUS_REG_CONFIGURATION_DATE_TIME_HOUR :
      {
         return 54;
      }
      case MODBUS_REG_CONFIGURATION_DATE_TIME_MIN :
      {
         return 55;
      }
      case MODBUS_REG_CONFIGURATION_LOCALIZATION_LANGUAGE :
      {
         return 56;
      }
      case MODBUS_REG_CONFIGURATION_LOCALIZATION_COUNTRY :
      {
         return 57;
      }
      case MODBUS_REG_CONFIGURATION_LOCALIZATION_DATE_FORMAT :
      {
         return 58;
      }
      case MODBUS_REG_CONFIGURATION_LOCALIZATION_DATE_SEPARATOR_FIRST :
      {
         return 59;
      }
      case MODBUS_REG_CONFIGURATION_LOCALIZATION_DATE_SEPARATOR_SECOND :
      {
         return 60;
      }
      case MODBUS_REG_CONFIGURATION_LOCALIZATION_TIME_FORMAT :
      {
         return 61;
      }
      case MODBUS_REG_CONFIGURATION_LOCALIZATION_TIME_SEPARATOR :
      {
         return 62;
      }
      case MODBUS_REG_CONFIGURATION_LOCALIZATION_DAYLIGHT_SAVING :
      {
         return 63;
      }

      default :
         return -1;
   }


   return -1;
}


