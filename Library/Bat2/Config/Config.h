//******************************************************************************
//
//   Config.h     Bat2 configuration
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Config_H__
   #define __Config_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __VersionDef_H__
   #include "Device/VersionDef.h"
#endif

#ifndef __DeviceDef_H__
   #include "Config/DeviceDef.h"
#endif

#ifndef __Context_H__
   #include "Config/Context.h"
#endif

//------------------------------------------------------------------------------
//  Data
//------------------------------------------------------------------------------

extern       TDeviceVersion Bat2Version;
extern const TDeviceVersion Bat2VersionDefault;

extern       TBat2Device    Bat2Device;
extern const TBat2Device    Bat2DeviceDefault;


   #ifndef __uStorageDef_H__
      #include "Data/uStorageDef.h"
   #endif
#ifdef __WIN32__
   DllImportExport
#endif
extern const UStorageDescriptor ConfigDescriptor;

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

#ifdef __cplusplus
   extern "C" {
#endif

TYesNo ConfigRemoteLoad( void);
// Load from remote device

TYesNo ConfigRemoteSave( void);
// Save to remote device

void ConfigLoad( void);
// Load configuration

void ConfigSave( void);
// Save configuration

void ConfigFactory( void);
// Set factory config on demand

#ifdef __cplusplus
   }
#endif

//------------------------------------------------------------------------------

void ConfigDeviceSave( void);
// Save device configuration

void ConfigCountrySave( void);
// Save country configuration

void ConfigWeightUnitsSave( void);
// Save weight units

void ConfigDisplayConfigurationSave( void);
// Save display configuration

void ConfigWeighingConfigurationSave( void);
// Save weighing configuration

void ConfigWeighingContextSave( void);
// Save weighing context

void ConfigGsmMessageSave( void);
// Save GSM messages configuration

void ConfigGsmContextSave( void);
// Save GSM subsystem context

void ConfigMegaviOptionsSave( void);
// Save Megavi settings

void ConfigModbusOptionsSave( void);
// Save Modbus settings

void ConfigSmsGateOptionsSave( void);
// Save Sms gate settings

void ConfigDacsOptionsSave( void);
// Save Dacs settings

void ConfigRs485OptionsSave( void);
// Save RS485 settings

void ConfigEthernetOptionsSave( void);
// Save Ethernet settings

void ConfigWeighingPlanSave( void);
// Save weighing plan

void ConfigDataPublicationSave( void);
// Save Data publication

void ConfigCellularDataSave( void);
// Save Cellular data

void ConfigEthernetSave( void);
// Save Ethernet

void ConfigMenuContextSave( void);
// Save Menu context

#endif
