//******************************************************************************
//
//   Context.h     Bat2 context
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Context_H__
   #define __Context_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __VersionDef_H__
   #include "Device/VersionDef.h"
#endif

#ifndef __DeviceDef_H__
   #include "Config/DeviceDef.h"
#endif

#ifdef __WIN32__
   #ifndef __uStorageDef_H__
   #include "Data/uStorageDef.h"
   #endif
   DllImportExport extern const UStorageDescriptor ContextDescriptor;
#endif

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

#ifdef __cplusplus
   extern "C" {
#endif

TYesNo ContextRemoteLoad( void);
// Load from remote device

TYesNo ContextRemoteSave( void);
// Save to remote device

void ContextLoad( void);
// Load context

void ContextFactory( void);

#ifdef __cplusplus
   }
#endif

//------------------------------------------------------------------------------

void ContextWeighingContextSave( void);
// Save weighing context

void ContextCommunicationSave( void);
// Save Communication subsystem context

void ContextSmsGateSave( void);
// Save SMS gate subsystem context

void ContextMenuContextSave( void);
// Save Menu context

void ContextCalibrationCoefficientsSave(void);
// Save Calibration cefficients context

void ContextCalibrationSave( void);
// Save Calibration configuration


#endif
