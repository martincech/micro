//******************************************************************************
//
//   ConfigCheck.h  Bat2 configuration check
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ConfigCheck_H__
   #define __ConfigCheck_H__

#include "Config/ConfigDef.h"
#include "Data/uFifoDef.h"
#include "Storage/SampleDef.h"
#include "Archive/ArchiveDef.h"

// check for FIFO pointer size :
#if (NVM_FIFO_POINTER_SIZE < UFIFO_POINTER_SIZE)
   #error "NvmLayout.h FIFO pointer size mismatch"
#endif   

// check for NVM end address :
#if (NVM_FIFO_END >= FLASH_SIZE)
   #error "NvmLayout.h logical sections mismatch"
#endif

// check for configuration size :
#if (GROWTH_CURVE_END > NVM_CONFIG_END)
   #error "NvmLayout.h wrong configuration size"
#endif

// check for samples FIFO size :     
#if ((SAMPLES_CAPACITY * sizeof( TSample)) > NVM_SAMPLES_SIZE)
   #error "NvmLayout.h wrong samples FIFO size"
#endif

// check for archive FIFO size :     
#if ((ARCHIVE_CAPACITY * sizeof( TArchiveItem)) > NVM_ARCHIVE_SIZE)
   #error "NvmLayout.h wrong archive FIFO size"
#endif

#endif
