//******************************************************************************
//
//   Bat2Def.h     Bat2 configuration data
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Bat2Def_H__
   #define __Bat2Def_H__

#ifndef __NvmLayout_H__
   #include "Memory/NvmLayout.h"
#endif

#ifndef __FileDef_H__
   #include "Memory/FileDef.h"
#endif

#include "Config.h"
//------------------------------------------------------------------------------
// Version info
//------------------------------------------------------------------------------

#define BAT2_SW_VERSION   DeviceVersionSet(1, 0, 0)
// #define BAT2_HW_VERSION ---> Hardware.h

//------------------------------------------------------------------------------
// Common data types
//------------------------------------------------------------------------------

// technological day :
typedef word TDayNumber;

#define DAY_NUMBER_MIN      0
#define DAY_NUMBER_MAX      999
#define DAY_NUMBER_DEFAULT  1
#define DAY_NUMBER_INVALID  0xFFFF

//------------------------------------------------------------------------------
// Configuration storage
//------------------------------------------------------------------------------

#define CONFIG_ADDRESS            NVM_CONFIG_START
#define CONFIG_SIZE              (8*1024)
#define CONFIG_END               (CONFIG_ADDRESS + CONFIG_SIZE - 1)

//------------------------------------------------------------------------------
// Growth curve storage
//------------------------------------------------------------------------------

#define GROWTH_CURVE_ADDRESS     (CONFIG_END + 1)
#define GROWTH_CURVE_COUNT        10
#define GROWTH_CURVE_SIZE        (3 * 1024)
#define GROWTH_CURVE_END         (GROWTH_CURVE_ADDRESS + GROWTH_CURVE_SIZE - 1)

//------------------------------------------------------------------------------
// Correction curve storage
//------------------------------------------------------------------------------

#define CORRECTION_CURVE_ADDRESS (GROWTH_CURVE_END + 1)
#define CORRECTION_CURVE_COUNT    10
#define CORRECTION_CURVE_SIZE    (1 * 1024)
#define CORRECTION_CURVE_END     (CORRECTION_CURVE_ADDRESS + CORRECTION_CURVE_SIZE - 1)

//------------------------------------------------------------------------------
// Phone number list
//------------------------------------------------------------------------------

#define CONTACT_ADDRESS          (CORRECTION_CURVE_END + 1)
#define CONTACT_COUNT             20
#define CONTACT_SIZE             (1 * 1024)
#define CONTACT_END              (CONTACT_ADDRESS + CONTACT_SIZE - 1)

//------------------------------------------------------------------------------
// Predefined weighings list
//------------------------------------------------------------------------------

#define PREDEFINED_WEIGHING_ADDRESS (CONTACT_END + 1)
#define PREDEFINED_WEIGHING_COUNT    10
#define PREDEFINED_WEIGHING_SIZE    (1 * 1024)
#define PREDEFINED_WEIGHING_END     (PREDEFINED_WEIGHING_ADDRESS + PREDEFINED_WEIGHING_SIZE - 1)

//------------------------------------------------------------------------------
// Predefined weighing plans
//------------------------------------------------------------------------------

#define WEIGHING_PLAN_ADDRESS       (PREDEFINED_WEIGHING_END + 1)
#define WEIGHING_PLAN_COUNT          10
#define WEIGHING_PLAN_SIZE          (2 * 1024)
#define WEIGHING_PLAN_END           (WEIGHING_PLAN_ADDRESS + WEIGHING_PLAN_SIZE - 1)
#define WEIGHING_TIME_COUNT          20

//------------------------------------------------------------------------------
// Device list
//------------------------------------------------------------------------------

#define DEVICE_LIST_ADDRESS         (WEIGHING_PLAN_END + 1)
#define DEVICE_LIST_COUNT            253
#define DEVICE_LIST_SIZE            (8 * 1024)
#define DEVICE_LIST_END             (DEVICE_LIST_ADDRESS + DEVICE_LIST_SIZE - 1)

#if (( CONFIG_SIZE +                 \
      GROWTH_CURVE_SIZE +           \
      CORRECTION_CURVE_SIZE +       \
      CONTACT_SIZE +                \
      PREDEFINED_WEIGHING_SIZE +    \
      WEIGHING_PLAN_SIZE +          \
      DEVICE_LIST_SIZE)             \
      > NVM_CONFIG_SIZE)
#error "NVM_CONFIG_SIZE smaller than size of its items"
#endif
//------------------------------------------------------------------------------
// Context storage
//------------------------------------------------------------------------------

#define CONTEXT_ADDRESS           NVM_CONTEXT_START
#define CONTEXT_SIZE              NVM_CONTEXT_SIZE         // size with spare
#define CONTEXT_END              (CONTEXT_ADDRESS + CONTEXT_SIZE - 1)

//------------------------------------------------------------------------------
// Diagnostic storage
//------------------------------------------------------------------------------

#define DIAGNOSTIC_DATA_ADDRESS      NVM_DIAGNOSTIC_START

//------------------------------------------------------------------------------
// Samples storage
//------------------------------------------------------------------------------

#define SAMPLES_POINTER_ADDRESS   (0)
#define SAMPLES_CAPACITY          (100 * 1024)

#define SAMPLES_BACKUP_POINTER_ADDRESS   SAMPLES_POINTER_ADDRESS + NVM_FIFO_POINTER_SIZE

//------------------------------------------------------------------------------
// Archive storage
//------------------------------------------------------------------------------

#define ARCHIVE_POINTER_ADDRESS   SAMPLES_BACKUP_POINTER_ADDRESS + NVM_FIFO_POINTER_SIZE
#define ARCHIVE_CAPACITY          (60 * 1024)

//------------------------------------------------------------------------------
// Log storage
//------------------------------------------------------------------------------

#define LOG_POINTER_ADDRESS      ARCHIVE_POINTER_ADDRESS + NVM_FIFO_POINTER_SIZE
#define LOG_CAPACITY             (4 * 1024)

#endif
