//******************************************************************************
//
//   State.h      Bat2 state
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __State_H__
   #define __State_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __RamDef_H__
   #include "Memory/RamDef.h"
#endif

#define State        Ram.State

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

#ifdef __cplusplus
   extern "C" {
#endif

TYesNo StateRemoteLoad( void);
// Load from remote device

TYesNo StateRemoteSave( void);
// Save to remote device

#ifdef __cplusplus
   }
#endif

#endif
