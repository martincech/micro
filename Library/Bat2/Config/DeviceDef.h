//******************************************************************************
//
//   DeviceDef.h   Bat2 device data
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __DeviceDef_H__
   #ifndef _MANAGED
      #define __DeviceDef_H__
   #endif

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

//------------------------------------------------------------------------------
//  Device data
//------------------------------------------------------------------------------

#ifndef _MANAGED

#define DEVICE_NAME_SIZE    15
#define PASSWORD_SIZE       4

typedef struct {
   char Name[ DEVICE_NAME_SIZE + 1];
   char Password[ PASSWORD_SIZE];
} TBat2Device;
   
#endif

#ifdef _MANAGED
   #undef _MANAGED
   #include "DeviceDef.h"
   #define _MANAGED
   namespace Bat2Library{
      public ref class DeviceC abstract sealed{
      public:
         /// <summary>
         /// Maximum device name length
         /// </summary>
         literal int NAME_SIZE_MAX = DEVICE_NAME_SIZE;
         /// <summary>
         /// Required password size
         /// </summary>
         literal int PASSWORD_LENGTH = PASSWORD_SIZE;
      };
   }
#endif

#endif
