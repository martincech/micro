//*****************************************************************************
//
//    Bat2Hardware.h    Bat2 hardware definitions
//    Version 1.0      (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Bat2Hardware_H__
   #define __Bat2Hardware_H__

#define BAT2_HW_VERSION    DeviceVersionSet(4,6,0)

#include "Cpu/uMK60DN512VLQ10.h"
#include "Cpu/Io.h"
#include "Unisys/Uni.h"

#include "Llwu/Llwu.h"

#define WIFI_EN                  KINETIS_PIN_PORTD01

#define CHARGER_SDA              KINETIS_PIN_PORTE00
#define CHARGER_SDA_FUNCTION     KINETIS_PIN_I2C1_SDA_PORTE00_FUNCTION
#define CHARGER_SCL              KINETIS_PIN_PORTE01
#define CHARGER_SCL_FUNCTION     KINETIS_PIN_I2C1_SCL_PORTE01_FUNCTION

#define KBD_R1                   KINETIS_PIN_PORTE02
#define KBD_R1_LLWU              LLWU_PTE2
#define KBD_R1_LLWU_FLAG         LLWU_FALLING_EDGE

#define USB_POWER_EN             KINETIS_PIN_PORTE03

#define KBD_R0                   KINETIS_PIN_PORTE04
#define KBD_R0_LLWU              LLWU_PTE4
#define KBD_R0_LLWU_FLAG         LLWU_FALLING_EDGE

#define POWER_USB_HOST_EN        KINETIS_PIN_PORTE05

#define GSM_UART                 UART_UART5
#define GSM_TX                   KINETIS_PIN_PORTE08
#define GSM_TX_FUNCTION          KINETIS_PIN_UART5_TX_PORTE08_FUNCTION
#define GSM_RX                   KINETIS_PIN_PORTE09
#define GSM_RX_FUNCTION          KINETIS_PIN_UART5_RX_PORTE09_FUNCTION
#define GSM_CTS                  KINETIS_PIN_PORTE10
#define GSM_CTS_FUNCTION         KINETIS_PIN_UART5_CTS_b_PORTE10_FUNCTION
#define GSM_RTS                  KINETIS_PIN_PORTE11
#define GSM_RTS_FUNCTION         KINETIS_PIN_UART5_RTS_b_PORTE11_FUNCTION

#define RS485_1_DIR              KINETIS_PIN_PORTE12

#define USB_SWITCH_EN            KINETIS_PIN_PORTE25
#define USB_SWITCH_SELECT_DEVICE KINETIS_PIN_PORTE24
#define USB_HOST_D_PLUS          KINETIS_PIN_PORTE26
#define USB_HOST_D_MINUS         KINETIS_PIN_PORTE27

#define ETH_EN                   KINETIS_PIN_PORTE28

#define FUEL_GAUGE_ALERT         KINETIS_PIN_PORTA28

#define FUELGAUGE_IIC_CHANNEL    IIC_IIC0
#define FUELGAUGE_SCL            KINETIS_PIN_PORTB02
#define FUELGAUGE_SCL_FUNCTION   KINETIS_PIN_I2C0_SCL_PORTB02_FUNCTION
#define FUELGAUGE_SDA            KINETIS_PIN_PORTB03
#define FUELGAUGE_SDA_FUNCTION   KINETIS_PIN_I2C0_SDA_PORTB03_FUNCTION

#define CHARGER_INTERRUPT_HANDLER       PORTB_IRQHandler
#define CHARGER_INTERRUPT_NUMBER        PORTB_IRQn

#define CHARGER_CE               KINETIS_PIN_PORTB04
#define CHARGER_PG               KINETIS_PIN_PORTB05

#define POWER_HIGH_EN            KINETIS_PIN_PORTB06
#define POWER_GOOD               KINETIS_PIN_PORTB07

#define ADC_SCLK                 KINETIS_PIN_PORTB08
#define ADC_POWERDOWN            KINETIS_PIN_PORTB09

#define FLASH_CS                 KINETIS_PIN_PORTB10
#define FLASH_SCK                KINETIS_PIN_PORTB11
#define FLASH_SCK_FUNCTION       KINETIS_PIN_SPI1_SCK_PORTB11_FUNCTION
#define FLASH_MOSI               KINETIS_PIN_PORTB16
#define FLASH_MOSI_FUNCTION      KINETIS_PIN_SPI1_SOUT_PORTB16_FUNCTION
#define FLASH_MISO               KINETIS_PIN_PORTB17
#define FLASH_MISO_FUNCTION      KINETIS_PIN_SPI1_SIN_PORTB17_FUNCTION

#define POWER_ADC_EN             KINETIS_PIN_PORTB18

#define POWER_BACKLIGHT_PWM_CHANNEL  PWM_CHANNEL_1
#define POWER_BACKLIGHT_PWM          KINETIS_PIN_PORTB19
#define POWER_BACKLIGHT_PWM_FUNCTION KINETIS_PIN_FTM2_CH1_PORTB19_FUNCTION

#define FRAM_CS              KINETIS_PIN_PORTB20
#define FRAM_SCK             KINETIS_PIN_PORTB21
#define FRAM_SCK_FUNCTION    KINETIS_PIN_SPI2_SCK_PORTB21_FUNCTION
#define FRAM_MOSI            KINETIS_PIN_PORTB22
#define FRAM_MOSI_FUNCTION   KINETIS_PIN_SPI2_SOUT_PORTB22_FUNCTION
#define FRAM_MISO            KINETIS_PIN_PORTB23
#define FRAM_MISO_FUNCTION   KINETIS_PIN_SPI2_SIN_PORTB23_FUNCTION

#define LCD_DATA                 KINETIS_PIN_PORTC00
#define LCD_CS                   KINETIS_PIN_PORTC13
#define LCD_A0                   KINETIS_PIN_PORTC10
#define LCD_WR                   KINETIS_PIN_PORTC09
#define LCD_RES                  KINETIS_PIN_PORTC08

#define ADC_DOUT_RDY             KINETIS_PIN_PORTC11
#define ADC_DOUT_RDY_LLWU        LLWU_PTC11
#define ADC_DOUT_RDY_LLWU_FLAG   LLWU_FALLING_EDGE
#define ADC_INTERRUPT_HANDLER    PORTC_IRQHandler
#define ADC_INTERRUPT_NUMBER     PORTC_IRQn

#define PLATFORM_UART            UART_UART4
#define PLATFORM_DIR             KINETIS_PIN_PORTC12
#define PLATFORM_RX              KINETIS_PIN_PORTC14
#define PLATFORM_RX_FUNCTION     KINETIS_PIN_UART4_RX_PORTC14_FUNCTION
#define PLATFORM_TX              KINETIS_PIN_PORTC15
#define PLATFORM_TX_FUNCTION     KINETIS_PIN_UART4_TX_PORTC15_FUNCTION

#define RS485_1_UART             UART_UART3
#define RS485_1_RX               KINETIS_PIN_PORTC16
#define RS485_1_RX_FUNCTION      KINETIS_PIN_UART3_RX_PORTC16_FUNCTION
#define RS485_1_TX               KINETIS_PIN_PORTC17
#define RS485_1_TX_FUNCTION      KINETIS_PIN_UART3_TX_PORTC17_FUNCTION

#define POWER_RS485_1_ON         KINETIS_PIN_PORTC18

#define POWER_RS485_0_ON         KINETIS_PIN_PORTD00
#define POWER_RS485_0_ON_LLWU    LLWU_PTD0
#define POWER_RS485_0_ON_LLWU_FLAG LLWU_FALLING_EDGE

#define POWER_ETH_ON             KINETIS_PIN_PORTD02
#define POWER_ETH_ON_LLWU        LLWU_PTD2
#define POWER_ETH_ON_LLWU_FLAG   LLWU_FALLING_EDGE

#define GSM_EN                   KINETIS_PIN_PORTD03

#define POWER_USB_DEVICE_ON      KINETIS_PIN_PORTD04
#define POWER_USB_DEVICE_ON_LLWU LLWU_PTD4
#define POWER_USB_DEVICE_ON_LLWU_FLAG LLWU_FALLING_EDGE

#define RS485_0_UART             UART_UART0
#define RS485_0_DIR              KINETIS_PIN_PORTD05
#define RS485_0_RX               KINETIS_PIN_PORTD06
#define RS485_0_RX_FUNCTION      KINETIS_PIN_UART0_RX_PORTD06_FUNCTION
#define RS485_0_TX               KINETIS_PIN_PORTD07
#define RS485_0_TX_FUNCTION      KINETIS_PIN_UART0_TX_PORTD07_FUNCTION

#define KBD_C1                   KINETIS_PIN_PORTD11
#define KBD_C2                   KINETIS_PIN_PORTD12

#define USB_POWER_STAT           KINETIS_PIN_PORTD15
#define POWERBANK_START_PULLDOWN KINETIS_PIN_PORTD14
#define POWERBANK_START_PULLUP   KINETIS_PIN_PORTD13

//------------------------------------------------------------------------------
//   Clock
//------------------------------------------------------------------------------

#define F_CRYSTAL  4000000ull
#define F_USB     48000000ull

//-----------------------------------------------------------------------------
// UART0
//-----------------------------------------------------------------------------

#define Uart0PortInit()       PinFunction( RS485_0_RX, RS485_0_RX_FUNCTION); \
                              PinFunction( RS485_0_TX, RS485_0_TX_FUNCTION); \
                              PinFunction( RS485_0_DIR, KINETIS_GPIO_FUNCTION);\
                              GpioOutput( RS485_0_DIR)

#define Uart0PortDeinit()     PinFunction( RS485_0_RX, KINETIS_GPIO_FUNCTION); \
                              PinFunction( RS485_0_TX, KINETIS_GPIO_FUNCTION); \
                              GpioInput( RS485_0_DIR)

#define UART0_BAUD            9600           // baud rate
#define UART0_FORMAT          UART_8BIT      // default format
#define UART0_TIMEOUT         10             // intercharacter timeout [ms]

#define UART0_TX_ENABLE_REGISTER   &GpioRegisterSet( RS485_0_DIR)
#define UART0_TX_DISABLE_REGISTER  &GpioRegisterClr( RS485_0_DIR)
#define UART0_TX_ENABLE_MASK       GpioMask( RS485_0_DIR)

//-----------------------------------------------------------------------------
// UART1
//-----------------------------------------------------------------------------

#define UART1_RTS_PIN        0
#define UART1_CTS_PIN        0

#define Uart1PortInit()

#define Uart1PortDeinit()

#define UART1_BAUD           9600           // baud rate
#define UART1_FORMAT         UART_8BIT      // default format
#define UART1_TIMEOUT        10             // intercharacter timeout [ms]
#define UART1_REPLY_TIMEOUT  3000           // reply timeout [ms]

#define UART1_TX_ENABLE_REGISTER     0
#define UART1_TX_DISABLE_REGISTER    0
#define UART1_TX_ENABLE_MASK         0

//-----------------------------------------------------------------------------
// UART2
//-----------------------------------------------------------------------------

#define UART2_RTS_PIN         0
#define UART2_CTS_PIN         0

#define Uart2PortInit()

#define Uart2PortDeinit()

#define UART2_BAUD           9600           // baud rate
#define UART2_FORMAT         UART_8BIT      // default format
#define UART2_TIMEOUT        10             // intercharacter timeout [ms]

// TODO !!!! switched clr and set
#define UART2_TX_ENABLE_REGISTER   0
#define UART2_TX_DISABLE_REGISTER  0
#define UART2_TX_ENABLE_MASK       0

//-----------------------------------------------------------------------------
// UART3
//-----------------------------------------------------------------------------

#define Uart3PortInit()       PinFunction( RS485_1_RX, RS485_1_RX_FUNCTION); \
                              PinFunction( RS485_1_TX, RS485_1_TX_FUNCTION); \
                              PinFunction( RS485_1_DIR, KINETIS_GPIO_FUNCTION);\
                              GpioOutput( RS485_1_DIR)

#define Uart3PortDeinit()     PinFunction( RS485_1_RX, KINETIS_GPIO_FUNCTION); \
                              PinFunction( RS485_1_TX, KINETIS_GPIO_FUNCTION); \
                              GpioInput( RS485_1_DIR)

#define UART3_BAUD           9600           // baud rate
#define UART3_FORMAT         UART_8BIT      // default format
#define UART3_TIMEOUT        10             // intercharacter timeout [ms]

#define UART3_TX_ENABLE_REGISTER   &GpioRegisterSet( RS485_1_DIR)
#define UART3_TX_DISABLE_REGISTER  &GpioRegisterClr( RS485_1_DIR)
#define UART3_TX_ENABLE_MASK       GpioMask( RS485_1_DIR)

//-----------------------------------------------------------------------------
// UART4
//-----------------------------------------------------------------------------

#define Uart4PortInit()       PinFunction( PLATFORM_RX, PLATFORM_RX_FUNCTION); \
                              PinFunction( PLATFORM_TX, PLATFORM_TX_FUNCTION)

#define Uart4PortDeinit()     PinFunction( PLATFORM_RX, KINETIS_GPIO_FUNCTION); \
                              PinFunction( PLATFORM_TX, KINETIS_GPIO_FUNCTION)

#define UART4_BAUD           9600           // baud rate
#define UART4_FORMAT         UART_8BIT      // default format
#define UART4_TIMEOUT        10             // intercharacter timeout [ms]

#define UART4_TX_ENABLE_REGISTER   0
#define UART4_TX_DISABLE_REGISTER  0
#define UART4_TX_ENABLE_MASK       0

//-----------------------------------------------------------------------------
// UART5
//-----------------------------------------------------------------------------

#define Uart5PortInit()      PinFunction( GSM_RX, GSM_RX_FUNCTION);\
                             PinFunction( GSM_TX, GSM_TX_FUNCTION);\
                             PinFunction( GSM_CTS, GSM_CTS_FUNCTION);\
                             PinFunction( GSM_RTS, GSM_RTS_FUNCTION)

#define Uart5PortDeinit()    PinFunction( GSM_RX, KINETIS_GPIO_FUNCTION);\
                             PinFunction( GSM_TX, KINETIS_GPIO_FUNCTION);\
                             PinFunction( GSM_CTS, KINETIS_GPIO_FUNCTION);\
                             PinFunction( GSM_RTS, KINETIS_GPIO_FUNCTION)

#define UART5_TX_ENABLE_REGISTER   0
#define UART5_TX_DISABLE_REGISTER  0
#define UART5_TX_ENABLE_MASK       0


//------------------------------------------------------------------------------
// Keyboard
//------------------------------------------------------------------------------

#define KBD_NUMERIC 0
#define K_POWER_ON_OFF   K_ESC

#define KBD_K0	    KBD_R0
#define KBD_K1      KBD_C2
#define KBD_K2      KBD_C1

#define KbdPortInit()   PinFunction(KBD_K0, KINETIS_GPIO_FUNCTION); \
                        PinFunction(KBD_K1, KINETIS_GPIO_FUNCTION); \
                        PinFunction(KBD_K2, KINETIS_GPIO_FUNCTION); \
                        GpioInput( KBD_K0);  GpioInput( KBD_K1);  GpioInput( KBD_K2);\
                        PinPullup( KBD_K0);  PinPullup( KBD_K1);  PinPullup( KBD_K2);\
         GpioClr( KBD_K0);    GpioClr( KBD_K1);    GpioClr( KBD_K2)

#define KbdK0Set()      GpioOutput( KBD_K0)
#define KbdK0Release()  GpioInput( KBD_K0)
#define KbdK1Set()      GpioOutput( KBD_K1)
#define KbdK1Release()  GpioInput( KBD_K1)
#define KbdK2Set()      GpioOutput( KBD_K2)
#define KbdK2Release()  GpioInput( KBD_K2)

#define KbdK0()         (!GpioGet( KBD_K0))
#define KbdK1()         (!GpioGet( KBD_K1))
#define KbdK2()         (!GpioGet( KBD_K2))

//------------------------------------------------------------------------------

// keyboard/event codes :
#define _K_SYSTEM_BASE            1    // skip K_NULL
#include "System/SystemKey.h"          // system events
#define _K_USB_BASE               _K_SYSTEM_LAST
#include "Usb/UsbKey.h"
#define _K_KEYBOARD_BASE          64
#include "Kbd/KbxKey.h"                // keyboard and user events


// timing constants [ms] :
#define KBD_DEBOUNCE           20      // delay after first touch
#define KBD_AUTOREPEAT_START   300     // autorepeat delay
#define KBD_AUTOREPEAT_SPEED   200     // autorepeat repeat speed

#define SYSTEM_KEYBOARD        1       // service keyboard with default scheduler

//------------------------------------------------------------------------------
//   USB
//------------------------------------------------------------------------------

#define UsbPortInit()            UsbSwitchDisable();     \
                                 GpioOutput(USB_SWITCH_EN); \
                                 GpioOutput(USB_SWITCH_SELECT_DEVICE); \
                                 PinFunction(USB_HOST_D_PLUS, KINETIS_GPIO_FUNCTION); \
                                 PinFunction(USB_HOST_D_MINUS, KINETIS_GPIO_FUNCTION); \
                                 PinFunction(USB_SWITCH_EN, KINETIS_GPIO_FUNCTION); \
                                 PinFunction(USB_SWITCH_SELECT_DEVICE, KINETIS_GPIO_FUNCTION)

#define UsbSwitchEnable()        GpioClr(USB_SWITCH_EN)
#define UsbSwitchDisable()       GpioSet(USB_SWITCH_EN)

#define UsbSwitchSelectDevice()  GpioSet(USB_SWITCH_SELECT_DEVICE)
#define UsbSwitchSelectHost()    GpioClr(USB_SWITCH_SELECT_DEVICE)

static inline TYesNo UsbHostReady( void) {
TYesNo Ready;
   PinPulldown( USB_HOST_D_PLUS);
   PinPulldown( USB_HOST_D_MINUS);
   if(GpioGet(USB_HOST_D_PLUS) || GpioGet(USB_HOST_D_MINUS)) {
      Ready = YES;
   } else {
      Ready = NO;
   }
   PinNoPull( USB_HOST_D_PLUS);
   PinNoPull( USB_HOST_D_MINUS);
   return Ready;;
}

#define USB_POWER            500
#define USB_MANUFACTURER     "V\0E\0I\0T\0 \0E\0l\0e\0c\0t\0r\0o\0n\0i\0c\0s\0"
#define USB_PRODUCT          "B\0A\0T\0""2\0"
#define USB_VID              0xA600
#define USB_PID              0xE2A0

//------------------------------------------------------------------------------
// Display controller ST7529
//------------------------------------------------------------------------------

// pins :
#define GPU_DATA_OFFSET  LCD_DATA   // LSB bus D0..D7
#define GPU_CS           LCD_CS   // /CS
#define GPU_A0           LCD_A0   //  A0/RS
#define GPU_WR           LCD_WR   // /WR
#define GPU_RES          LCD_RES   // /RES  08

#define GpuPortInit()   PinFunction(GPU_CS, KINETIS_GPIO_FUNCTION); \
                        PinFunction(GPU_A0, KINETIS_GPIO_FUNCTION); \
                        PinFunction(GPU_WR, KINETIS_GPIO_FUNCTION); \
                        PinFunction(GPU_RES, KINETIS_GPIO_FUNCTION); \
                        GpioOutput( GPU_CS); GpioOutput( GPU_A0); \
                        GpioOutput( GPU_WR); GpioOutput( GPU_RES)

#define GpuClearAll()   GpuResClr(); GpuDeselect(); GpuWrSet(); GpuRsSet()

// chipselect :
#define _GpuSelect()     GpioClr( GPU_CS)
#define _GpuDeselect()   GpioSet( GPU_CS)
// control signals :
#define _GpuResSet()     GpioSet( GPU_RES)
#define _GpuResClr()     GpioClr( GPU_RES)
#define _GpuWrSet()      GpioSet( GPU_WR)
#define _GpuWrClr()      GpioClr( GPU_WR)
#define _GpuRsSet()      GpioSet( GPU_A0)
#define _GpuRsClr()      GpioClr( GPU_A0)

// Settings :
#define GPU_LF                 0x00                   // line cycles
#define GPU_EC_BASE            0x120                  // electronic contrast base
#define GPU_ANASET1            GPU_ANASET1_193
#define GPU_ANASET2            GPU_ANASET2_3K
#define GPU_ANASET3            GPU_ANASET3_12
#define GPU_DATSDR1            GPU_DATSDR1_CI
#define GPU_DATSDR2            GPU_DATSDR2_CLR

#define CPU_COLUMN_OFFSSET     5

// Color palette (31..0) :
#define G_INTENSITY_BLACK      31
#define G_INTENSITY_DARKGRAY   20
#define G_INTENSITY_LIGHTGRAY  10
#define G_INTENSITY_WHITE      0

//------------------------------------------------------------------------------
// Graphics
//------------------------------------------------------------------------------

#define G_WIDTH          240           // display width (X)
#define G_HEIGHT         160           // display height (Y)
#define G_PLANES           2           // color planes count

#define GRAPHIC_CONIO            1     // enable conio.h
#define PRINTF_STRING            1     // enable string resource
#define PRINTF_BUFFER            1     // use bprintf
#define GRAPHIC_LETTER_CENTERING 1     // fixed text centering
#define GRAPHIC_TEXT_INDENTATION 1     // indentation

//-----------------------------------------------------------------------------
//  BackLight
//-----------------------------------------------------------------------------

#define BACKLIGHT_CHANNEL        POWER_BACKLIGHT_PWM_CHANNEL
#define BACKLIGHT_PWM_FREQUENCY  755             // backlight PWM period [Hz]
#define PwmPortInit()            PinFunction(POWER_BACKLIGHT_PWM, POWER_BACKLIGHT_PWM_FUNCTION)

//-----------------------------------------------------------------------------
// Ads1232
//-----------------------------------------------------------------------------

#define ADC_INTERRUPT_PRIORITY      5

#define AdcInterruptHandler()       void __irq ADC_INTERRUPT_HANDLER( void)
#define AdcInitInt()                CpuIrqAttach(ADC_INTERRUPT_NUMBER, ADC_INTERRUPT_PRIORITY, ADC_INTERRUPT_HANDLER); \
                                    CpuIrqEnable(ADC_INTERRUPT_NUMBER)
#define AdcClearInt()               EintClearFlag( ADC_DOUT_RDY)
#define AdcEnableInt()              EintEnable( ADC_DOUT_RDY, EINT_SENSE_FALLING_EDGE)
#define AdcDisableInt()             EintDisable( ADC_DOUT_RDY)

#define AdcPortInit()      PinFunction(ADC_SCLK, KINETIS_GPIO_FUNCTION); \
                           PinFunction(ADC_DOUT_RDY, KINETIS_GPIO_FUNCTION); \
                           PinFunction(ADC_POWERDOWN, KINETIS_GPIO_FUNCTION); \
                           GpioOutput(ADC_SCLK); \
                           GpioOutput(ADC_POWERDOWN); \
                           GpioInput(ADC_DOUT_RDY);

#define AdcSetSCLK()     GpioSet(ADC_SCLK)
#define AdcClrSCLK()     GpioClr(ADC_SCLK)

#define AdcPowerDownDisable()     GpioSet(ADC_POWERDOWN)
#define AdcPowerDownEnable()     GpioClr(ADC_POWERDOWN)

#define AdcGetDOUT()     GpioGet(ADC_DOUT_RDY)
#define AdcGetRDY()      !GpioGet(ADC_DOUT_RDY)

// Parameters :
//#define ADC_CONVERSION_RATE   10      // default conversion rate [Hz]
//------------------------------------------------------------------------------
//  Power control
//------------------------------------------------------------------------------

#define PowerPortInit()      PowerGsmOff(); \
                             PinFunction(GSM_EN, KINETIS_GPIO_FUNCTION); \
                             GpioOutput( GSM_EN); \
                             PowerUsbHostOff(); \
                             PinFunction(POWER_USB_HOST_EN, KINETIS_GPIO_FUNCTION); \
                             GpioOutput( POWER_USB_HOST_EN); \
                             PowerAdcOff(); \
                             PinFunction(POWER_ADC_EN, KINETIS_GPIO_FUNCTION); \
                             GpioOutput( POWER_ADC_EN); \
                             PowerEthOff(); \
                             PinFunction( ETH_EN, KINETIS_GPIO_FUNCTION); \
                             GpioOutput( ETH_EN); \
                             PinPullup(POWER_USB_DEVICE_ON); \
                             PinFunction(POWER_USB_DEVICE_ON, KINETIS_GPIO_FUNCTION); \
                             PinPullup(POWER_ETH_ON); \
                             PinFunction(POWER_ETH_ON, KINETIS_GPIO_FUNCTION); \
                             PinPullup(POWER_RS485_0_ON); \
                             PinFunction(POWER_RS485_0_ON, KINETIS_GPIO_FUNCTION); \
                             PinPullup(POWER_RS485_1_ON); \
                             PinFunction(POWER_RS485_1_ON, KINETIS_GPIO_FUNCTION); \
                             PinFunction(POWER_HIGH_EN, KINETIS_GPIO_FUNCTION); \
                             PinFunction(POWER_GOOD, KINETIS_GPIO_FUNCTION); \
                             PinPullup(POWER_GOOD); \
                             GpioClr( POWER_HIGH_EN); \
                             UsbPowerEnable(); \
                             PinFunction(USB_POWER_EN, KINETIS_GPIO_FUNCTION); \
                             GpioOutput(USB_POWER_EN); \
                             PinFunction(USB_POWER_STAT, KINETIS_GPIO_FUNCTION); \
                             PowerBankStartPullupLow(); \
                             GpioOutput( POWERBANK_START_PULLUP); \
                             PinFunction(POWERBANK_START_PULLUP, KINETIS_GPIO_FUNCTION); \
                             PowerBankStartPulldownDisable(); \
                             GpioOutput( POWERBANK_START_PULLDOWN); \
                             PinFunction(POWERBANK_START_PULLDOWN, KINETIS_GPIO_FUNCTION); \
                             WifiDisable(); \
                             PinFunction(WIFI_EN, KINETIS_GPIO_FUNCTION); \
                             GpioOutput( WIFI_EN)

#define WifiEnable()         GpioClr( WIFI_EN)
#define WifiDisable()        GpioSet( WIFI_EN)

#define PowerGsmEn()         PowerHighEnable(); GpioSet( GSM_EN)
#define PowerGsmOff()        PowerHighDisable(); GpioClr( GSM_EN)

#define PowerUsbHostEn()     GpioSet( POWER_USB_HOST_EN)
#define PowerUsbHostOff()    GpioClr( POWER_USB_HOST_EN)

#define PowerAdcEn()         GpioSet( POWER_ADC_EN)
#define PowerAdcOff()        GpioClr( POWER_ADC_EN)

#define PowerEthEn()         GpioSet( ETH_EN)
#define PowerEthOff()        GpioClr( ETH_EN)

#define PowerHighEnable()     GpioOutput( POWER_HIGH_EN)
#define PowerHighDisable()    GpioInput( POWER_HIGH_EN)

#define PowerUsbDeviceOn()  (GpioGet( POWER_USB_DEVICE_ON) ? NO : YES)
#define PowerEthOn()        (GpioGet( POWER_ETH_ON) ? NO : YES)
#define PowerRs4850On()     (GpioGet( POWER_RS485_0_ON) ? NO : YES)
#define PowerRs4851On()     (GpioGet( POWER_RS485_1_ON) ? NO : YES)

#define UsbPowerEnable()        GpioClr( USB_POWER_EN)
#define UsbPowerDisable()       GpioSet( USB_POWER_EN)

#define UsbPowerStat()     (GpioGet( USB_POWER_STAT) ? YES : NO)

#define PowerBankStartPullupLow()   GpioClr( POWERBANK_START_PULLUP)
#define PowerBankStartPullupHigh()  GpioSet( POWERBANK_START_PULLUP)

#define PowerBankStartPulldownEnable()   GpioSet( POWERBANK_START_PULLDOWN)
#define PowerBankStartPulldownDisable()  GpioClr( POWERBANK_START_PULLDOWN)

#define PowerGood()              (GpioGet( POWER_GOOD) ? NO : YES)
//------------------------------------------------------------------------------
//   Bq2425x
//------------------------------------------------------------------------------

#define BQ2425X_INTERRUPT_HANDLER       CHARGER_INTERRUPT_HANDLER
#define BQ2425X_INTERRUPT_NUMBER        CHARGER_INTERRUPT_NUMBER
#define BQ2425X_INTERRUPT_PRIORITY      5

#define Bq2425xIrqHandler()   void __irq BQ2425X_INTERRUPT_HANDLER( void)
#define Bq2425xIrqInit()      CpuIrqAttach(BQ2425X_INTERRUPT_NUMBER, BQ2425X_INTERRUPT_PRIORITY, BQ2425X_INTERRUPT_HANDLER); \
                              CpuIrqEnable(BQ2425X_INTERRUPT_NUMBER)
#define Bq2425xIrqClear()     EintClearFlag( CHARGER_PG)
#define Bq2425xIrqEnable()    EintEnable( CHARGER_PG, EINT_SENSE_ANY_EDGE)
#define Bq2425xIrqDisable()   EintDisable( CHARGER_PG)

#define Bq2425xPg()           (GpioGet( CHARGER_PG) ? YES : NO)

#define Bq2425xPortInit()     PinFunction(CHARGER_CE, KINETIS_GPIO_FUNCTION); \
                              Bq2425xCeDeassert(); \
                              GpioOutput( CHARGER_CE); \
                              PinFunction(CHARGER_PG, KINETIS_GPIO_FUNCTION); \
                              PinPullup(CHARGER_PG)


#define Bq2425xVinHandler()  ISR(PORTA_INT0_vect)

#define Bq2425xCeAssert()    GpioClr( CHARGER_CE)
#define Bq2425xCeDeassert()  GpioSet( CHARGER_CE)

//-----------------------------------------------------------------------------
// Platform
//-----------------------------------------------------------------------------

#define OPTION_SIGMA_DELTA
#define OPTION_PICOSTRAIN

//-----------------------------------------------------------------------------
// IIC0
//-----------------------------------------------------------------------------

#define MAX17047_IIC_CHANNEL     FUELGAUGE_IIC_CHANNEL
#define BQ2425X_IIC_CHANNEL      FUELGAUGE_IIC_CHANNEL

#define Iic0PortInit()           PinOpenDrainEnable( FUELGAUGE_SCL); \
                                 PinOpenDrainEnable( FUELGAUGE_SDA); \
                                 PinFunction( FUELGAUGE_SCL, FUELGAUGE_SCL_FUNCTION); \
                                 PinFunction( FUELGAUGE_SDA, FUELGAUGE_SDA_FUNCTION)
#define Iic0PortDeinit()
#define IIC0_CLOCK               50000

//-----------------------------------------------------------------------------
// IIC1
//-----------------------------------------------------------------------------

#define Iic1PortInit()
#define Iic1PortDeinit()
#define IIC1_CLOCK               0

//-----------------------------------------------------------------------------
// SMBus
//-----------------------------------------------------------------------------

#define SMBUS_IIC_CHANNEL     ACCU_IIC

//-----------------------------------------------------------------------------
// SPI0 module
//-----------------------------------------------------------------------------

#define SPI_M0_CLK_PIN            0//KINETIS_PIN_SPI0_SCK_PORTD01
#define SPI_M0_CLK_PIN_FUNCTION   0//KINETIS_PIN_SPI0_SCK_PORTD01_FUNCTION
#define SPI_M0_DIN_PIN            0//KINETIS_PIN_SPI0_SIN_PORTD03
#define SPI_M0_DIN_PIN_FUNCTION   0//KINETIS_PIN_SPI0_SIN_PORTD03_FUNCTION
#define SPI_M0_DOUT_PIN           0//0//KINETIS_PIN_SPI0_SOUT_PORTD02
#define SPI_M0_DOUT_PIN_FUNCTION  0//KINETIS_PIN_SPI0_SOUT_PORTD02_FUNCTION
#define SPI_M0_S0_CLOCK           5000000
#define SPI_M0_S1_CLOCK           5000000
#define Spi0PortInit()            PinPullup( SPI_M0_DIN_PIN)

//-----------------------------------------------------------------------------
// SPI1 module
//-----------------------------------------------------------------------------

#define SPI_M1_CLK_PIN            FLASH_SCK
#define SPI_M1_CLK_PIN_FUNCTION   FLASH_SCK_FUNCTION
#define SPI_M1_DIN_PIN            FLASH_MISO
#define SPI_M1_DIN_PIN_FUNCTION   FLASH_MISO_FUNCTION
#define SPI_M1_DOUT_PIN           FLASH_MOSI
#define SPI_M1_DOUT_PIN_FUNCTION  FLASH_MOSI_FUNCTION
#define SPI_M1_S0_CLOCK           5000000
#define SPI_M1_S1_CLOCK           5000000
#define Spi1PortInit()            PinPullup( SPI_M1_DIN_PIN)

//-----------------------------------------------------------------------------
// SPI2 module
//-----------------------------------------------------------------------------

#define SPI_M2_CLK_PIN            FRAM_SCK
#define SPI_M2_CLK_PIN_FUNCTION   FRAM_SCK_FUNCTION
#define SPI_M2_DIN_PIN            FRAM_MISO
#define SPI_M2_DIN_PIN_FUNCTION   FRAM_MISO_FUNCTION
#define SPI_M2_DOUT_PIN           FRAM_MOSI
#define SPI_M2_DOUT_PIN_FUNCTION  FRAM_MOSI_FUNCTION
#define SPI_M2_S0_CLOCK           5000000
#define SPI_M2_S1_CLOCK           5000000
#define Spi2PortInit()            PinPullup( SPI_M2_DIN_PIN)

#define SPI_CHANNELS_PER_SUBMODULE  1

#define SPI3_CS_PIN     FLASH_CS
#define SPI5_CS_PIN     FRAM_CS


//-----------------------------------------------------------------------------
//  Data Flash AT25DF641
//-----------------------------------------------------------------------------

// SPI1, NPCS0
#define SPI_FLASH_CHANNEL         SPI_SPI3

#define __AT25DF641__             1              // NOR Flash chip type
#define FLASH_TIMEOUT             500            // NOR Flash write (sector erase) timeout [ms]
#define FLASH_ERASE_TIMEOUT       120000         // NOR Flash chip erase timeout [ms]
#define FLASH_SIZE                (8 * 1024 * 1024L)   // memory total capacity

//-----------------------------------------------------------------------------
// FRAM FM25V10
//-----------------------------------------------------------------------------

// SPI2, NPCS0
#define SPI_FRAM_CHANNEL          SPI_SPI5

#define FRAM_SIZE                (256 * 1024L)   // memory total capacity

// conditional compilation :
#define FRAM_PRESENT              1              // check for presence
#define FRAM_BYTE_READ            1              // read single byte
#define FRAM_BYTE_WRITE           1              // write single byte

//-----------------------------------------------------------------------------
// RAM
//-----------------------------------------------------------------------------

#define RAM_SIZE  128

//------------------------------------------------------------------------------
//  GSM module
//------------------------------------------------------------------------------

#define GSM_BAUD_RATE                      115200
#define UART_MODEM_CHANNEL                 GSM_UART
#define UART_MODEM_FORMAT                  (UART_8BIT | UART_FLOW_CONTROL_HARDWARE)
#define UART_MODEM_INTERCHARACTER_TIMEOUT  150

// SMS expiration :
#define GsmSmsExpirationMins( m)   ((m) / 5)               // min 0  max 720 (0..143)
#define GsmSmsExpirationHours( h)  (((h) - 12) * 2 + 143)  // min 13 max 24  (144..167)
#define GsmSmsExpirationDays( d)   ((d) + 166)             // min 2  max 30  (168..196)
#define GsmSmsExpirationWeeks( w)  ((w) + 192)             // min 5  max 58  (197..255)
#define GsmSmsExpirationMax()      255                     // maximum expiration

#define GSM_SMS_EXPIRATION    GsmSmsExpirationDays( 4)     // requested expiration

#define GsmPowerOn()        PowerGsmEn()
#define GsmPowerOff()       PowerGsmOff()

//------------------------------------------------------------------------------
//  RS485
//------------------------------------------------------------------------------
#define RS485_0_PHYSICAL_DEVICE        RS485_0_UART
#define RS485_1_PHYSICAL_DEVICE        RS485_1_UART

//------------------------------------------------------------------------------
//  Ethernet module
//------------------------------------------------------------------------------

#define EthPortRxInit()  PinFunction( KINETIS_PIN_RMII0_RXD0_MII0_RXD0_PORTA13, KINETIS_PIN_RMII0_RXD0_MII0_RXD0_PORTA13_FUNCTION); \
                         PinFunction( KINETIS_PIN_RMII0_RXD1_MII0_RXD1_PORTA12, KINETIS_PIN_RMII0_RXD1_MII0_RXD1_PORTA12_FUNCTION); \
                         PinFunction( KINETIS_PIN_MII0_RXD2_PORTA10, KINETIS_PIN_MII0_RXD2_PORTA10_FUNCTION); \
                         PinFunction( KINETIS_PIN_MII0_RXD3_PORTA09, KINETIS_PIN_MII0_RXD3_PORTA09_FUNCTION); \
                         PinFunction( KINETIS_PIN_MII0_RXCLK_PORTA11, KINETIS_PIN_MII0_RXCLK_PORTA11_FUNCTION); \
                         PinFunction( KINETIS_PIN_RMII0_CRS_DV_MII0_RXDV_PORTA14, KINETIS_PIN_RMII0_CRS_DV_MII0_RXDV_PORTA14_FUNCTION); \
                         PinFunction( KINETIS_PIN_RMII0_RXER_MII0_RXER_PORTA05, KINETIS_PIN_RMII0_RXER_MII0_RXER_PORTA05_FUNCTION)

#define EthPortRxDeInit() PinFunction( KINETIS_PIN_RMII0_RXD0_MII0_RXD0_PORTA13, KINETIS_GPIO_FUNCTION); \
                          PinFunction( KINETIS_PIN_RMII0_RXD1_MII0_RXD1_PORTA12, KINETIS_GPIO_FUNCTION); \
                          PinFunction( KINETIS_PIN_MII0_RXD2_PORTA10, KINETIS_GPIO_FUNCTION); \
                          PinFunction( KINETIS_PIN_MII0_RXD3_PORTA09, KINETIS_GPIO_FUNCTION); \
                          PinFunction( KINETIS_PIN_MII0_RXCLK_PORTA11, KINETIS_GPIO_FUNCTION); \
                          PinFunction( KINETIS_PIN_RMII0_CRS_DV_MII0_RXDV_PORTA14, KINETIS_GPIO_FUNCTION); \
                          PinFunction( KINETIS_PIN_RMII0_RXER_MII0_RXER_PORTA05, KINETIS_GPIO_FUNCTION)

#define EthPortTxInit()  PinFunction( KINETIS_PIN_RMII0_TXD0_MII0_TXD0_PORTA16, KINETIS_PIN_RMII0_TXD0_MII0_TXD0_PORTA16_FUNCTION); \
                         PinFunction( KINETIS_PIN_RMII0_TXD1_MII0_TXD1_PORTA17, KINETIS_PIN_RMII0_TXD1_MII0_TXD1_PORTA17_FUNCTION); \
                         PinFunction( KINETIS_PIN_MII0_TXD2_PORTA24, KINETIS_PIN_MII0_TXD2_PORTA24_FUNCTION); \
                         PinFunction( KINETIS_PIN_MII0_TXD3_PORTA26, KINETIS_PIN_MII0_TXD3_PORTA26_FUNCTION); \
                         PinFunction( KINETIS_PIN_MII0_TXCLK_PORTA25, KINETIS_PIN_MII0_TXCLK_PORTA25_FUNCTION); \
                         PinFunction( KINETIS_PIN_RMII0_TXEN_MII0_TXEN_PORTA15, KINETIS_PIN_RMII0_TXEN_MII0_TXEN_PORTA15_FUNCTION); \
                         PinFunction( KINETIS_PIN_MII0_TXER_PORTA28, KINETIS_PIN_MII0_TXER_PORTA28_FUNCTION)

#define EthPortTxDeInit() PinFunction( KINETIS_PIN_RMII0_TXD0_MII0_TXD0_PORTA16, KINETIS_GPIO_FUNCTION); \
                          PinFunction( KINETIS_PIN_RMII0_TXD1_MII0_TXD1_PORTA17, KINETIS_GPIO_FUNCTION); \
                          PinFunction( KINETIS_PIN_MII0_TXD2_PORTA24, KINETIS_GPIO_FUNCTION); \
                          PinFunction( KINETIS_PIN_MII0_TXD3_PORTA26, KINETIS_GPIO_FUNCTION); \
                          PinFunction( KINETIS_PIN_MII0_TXCLK_PORTA25, KINETIS_GPIO_FUNCTION); \
                          PinFunction( KINETIS_PIN_RMII0_TXEN_MII0_TXEN_PORTA15, KINETIS_GPIO_FUNCTION); \
                          PinFunction( KINETIS_PIN_MII0_TXER_PORTA28, KINETIS_GPIO_FUNCTION)

#define EthPortMDIOInit()  \
                         PinFunction( KINETIS_PIN_RMII0_MDIO_MII0_MDIO_PORTB00, KINETIS_PIN_RMII0_MDIO_MII0_MDIO_PORTB00_FUNCTION); \
                         PinFunction( KINETIS_PIN_RMII0_MDC_MII0_MDC_PORTB01, KINETIS_PIN_RMII0_MDC_MII0_MDC_PORTB01_FUNCTION)

#define EthPortMDIODeInit()  \
                         PinFunction( KINETIS_PIN_RMII0_MDIO_MII0_MDIO_PORTB00, KINETIS_GPIO_FUNCTION); \
                         PinFunction( KINETIS_PIN_RMII0_MDC_MII0_MDC_PORTB01, KINETIS_GPIO_FUNCTION)

#define EthPortConfigAndStatusInit() \
                         PinFunction( KINETIS_PIN_MII0_CRS_PORTA27, KINETIS_PIN_MII0_CRS_PORTA27_FUNCTION); \
                         PinFunction( KINETIS_PIN_MII0_COL_PORTA29, KINETIS_PIN_MII0_COL_PORTA29_FUNCTION)

#define EthPortConfigAndStatusDeInit() \
                         PinFunction( KINETIS_PIN_MII0_CRS_PORTA27, KINETIS_GPIO_FUNCTION); \
                         PinFunction( KINETIS_PIN_MII0_COL_PORTA29, KINETIS_GPIO_FUNCTION)

#define EthPortInit()   EthPortRxInit();\
                        EthPortTxInit();\
                        EthPortMDIOInit();\
                        EthPortConfigAndStatusInit()

#define EthPortDeinit() EthPortRxDeInit();\
                        EthPortTxDeInit();\
                        EthPortMDIODeInit();\
                        EthPortConfigAndStatusDeInit()

//-----------------------------------------------------------------------------
// Calibration
//-----------------------------------------------------------------------------

#include "Platform/PlatformDef.h"

#define CALIBRATION_POINTS_MAX   PLATFORM_CALIBRATION_POINTS_MAX

#endif


#define DATE_YEAR_YYYY


#define ACCU_BACKUP_PERIOD       1
