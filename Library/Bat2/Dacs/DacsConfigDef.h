//******************************************************************************
//
//   DacsConfigDef.h     Dacs messaging options
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __DacsConfigDef_H__
   #ifndef _MANAGED
      #define __DacsConfigDef_H__
   #endif

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifdef _MANAGED
namespace Bat2Library{
   public enum class DacsVersionE{
#else
typedef enum {
#endif
   DACS_VERSION_6,
#ifndef _MANAGED
   _DACS_VERSION_LAST
} EDacsVersion;
#else
   };
}
#endif

#ifndef _MANAGED
typedef struct {
   byte Version;
   byte Address;
} TDacsModuleOptions;

#define DACS_ADDRESS_MIN 0
#define DACS_ADDRESS_MAX 255
#define DACS_ADDRESS_DEFAULT 65
#endif

#ifdef _MANAGED
   #undef _MANAGED
   #include "DacsConfigDef.h"
   #define _MANAGED
   namespace Bat2Library{
      public ref class DacsConfigurationC abstract sealed{
      public:
         literal int ADDRESS_MIN = DACS_ADDRESS_MIN;
         literal int ADDRESS_MAX = DACS_ADDRESS_MAX;
         literal int ADDRESS_DEFAULT = DACS_ADDRESS_DEFAULT;
      };
   }
#endif
#endif // __DacsConfigDef_H__


