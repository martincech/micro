//******************************************************************************
//
//   DacsConfig.h     Dacs module configuration
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __DacsConfig_H__
#define __DacsConfig_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __DacsConfigDef_H__
   #include "DacsConfigDef.h"
#endif

extern         TDacsModuleOptions DacsOptions;
extern const   TDacsModuleOptions DacsOptionsDefault;

#endif // __DacsConfig_H__


