//******************************************************************************
//
//   DacsTask.h     Dacs service task routines
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __DacsTask_H__
#define __DacsTask_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void DacsTaskInit( int uartPort);
// Init

void DacsTaskFree( void);
// Deinit - free resources

void DacsTaskExecute( void);
// Executive


#endif // __DacsTask_H__


