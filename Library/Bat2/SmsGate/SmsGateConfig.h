//******************************************************************************
//
//   SmsGateConfig.h     Sms gate configuration
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __SmsGateConfig_H__
#define __SmsGateConfig_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __SmsGateDef_H__
   #include "SmsGateDef.h"
#endif

#ifndef __SmsGateConfigDef_H__
   #include "SmsGateConfigDef.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif

extern         TSmsGateModuleOptions SmsGateOptions;
extern const   TSmsGateModuleOptions SmsGateOptionsDefault;

extern TSmsQueue         SmsGateQueue;
extern const TSmsQueue   SmsGateQueueDefault;

#ifdef __cplusplus
   }
#endif

#endif // __SmsGateConfig_H__


