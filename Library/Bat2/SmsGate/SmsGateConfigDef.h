//******************************************************************************
//
//   SmsGateConfigDef.h     Sms gate configuration definitions
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __SmsGateConfigDef_H__
   #define __SmsGateConfigDef_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

typedef struct {
   byte                    DeviceAddress;
   dword                   BaudRate;
   byte                    DataBits;
   byte                    Parity;
} TSmsGateModuleOptions;


#endif // __SmsGateConfigDef_H__


