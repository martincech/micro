//******************************************************************************
//
//   SmsGateConfig.h     Sms gate configuration
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "SmsGateConfig.h"

#include "PreprocLoop/loopstart.h"
#define LOOP_END SMS_GATE_SMS_SLOTS
#define MACRO(i) \
{\
/* SmsText*/               "",\
/* SmsPhoneNumber*/        "",\
/* _Dummy1*/               0,\
/* SlotStatus*/            SMS_GATE_SMS_STATUS_EMPTY,\
/* Contact*/               CONTACT_INDEX_INVALID,\
/* Broadcast*/             NO,\
/* _Dummy2*/               0,\
/* SlotReadStartTime*/     -1,\
/* BinarySize*/            0,\
/* _Dummy3*/               0,\
},\

TSmsGateModuleOptions SmsGateOptions;
const   TSmsGateModuleOptions SmsGateOptionsDefault = {
   /* DeviceAddress */  RS485_1_PHYSICAL_DEVICE,
   /* BaudRate */       SMS_GATE_BAUD_RATE_DEFAULT,
   /* DataBits */       SMS_GATE_DATA_BITS_DEFAULT,
   /* Parity */         SMS_GATE_PARITY_DEFAULT
};

TSmsQueue         SmsGateQueue;
const TSmsQueue   SmsGateQueueDefault = {
   {
      #include "PreprocLoop/loop.h"
   },
   0
};

