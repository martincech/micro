//******************************************************************************
//
//   SmsGate.c     Sms gate service
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "SmsGate.h"
#include "Message/ContactList.h"           // Phonebook
#include "Message/MGsm.h"                  // Sms sending
#include "System/System.h"                 // Sysclock
#include "Timer/Timer.h"                   // Systimer
#include <string.h>                        // string routines
#include <ctype.h>                         // valid number check
#include "Config/Context.h"

static dword                     _SmsSendStartTime;
static TYesNo                    _InitDone = NO;  // for synchronization purposes
static TYesNo                    _ModulePowerOn;

#define SMS_SEND_TIMEOUT            300  // s (5 mins)
#define SMS_SEND_TIMEOUT_INVALID -1
#define _SmsSendTimeout()        (!_SmsSendTimeoutRunning() || TimeAfter(SysTime(), _SmsSendStartTime))
#define _SmsSendTimeoutSet()     (_SmsSendStartTime = SysTime() + SMS_SEND_TIMEOUT)
#define _SmsSendTimeoutReset()   (_SmsSendStartTime = SMS_SEND_TIMEOUT_INVALID)
#define _SmsSendTimeoutRunning() (_SmsSendStartTime != SMS_SEND_TIMEOUT_INVALID)

#define SLOT_READ_TIMEOUT           120  // s (2 mins)
#define _SlotReadTimeout(i)        (!isSlotPending(i) || !_SlotReadTimeoutRunning(i) || TimeAfter(SysTime(), SmsGateQueue.SmsSlots[i].SlotReadStartTime))
#define _SlotReadTimeoutSet(i)     (SmsGateQueue.SmsSlots[i].SlotReadStartTime = SysTime() + SLOT_READ_TIMEOUT)
#define _SlotReadTimeoutReset(i)   (SmsGateQueue.SmsSlots[i].SlotReadStartTime = SMS_SEND_TIMEOUT_INVALID)
#define _SlotReadTimeoutRunning(i) (SmsGateQueue.SmsSlots[i].SlotReadStartTime != SMS_SEND_TIMEOUT_INVALID)

//------------------------------------------------------------------------------
//  Sms slots
//------------------------------------------------------------------------------

#define getSlotStatus( SlotNum)           SmsGateQueue.SmsSlots[SlotNum].SlotStatus
#define isSlotEmpty( SlotNum)             (getSlotStatus( SlotNum) == SMS_GATE_SMS_STATUS_EMPTY)
#define isSlotError( SlotNum)             (getSlotStatus( SlotNum) == SMS_GATE_SMS_STATUS_ERROR)
#define isSlotSended( SlotNum)            (getSlotStatus( SlotNum) == SMS_GATE_SMS_STATUS_SENDED)
#define isSlotPending( SlotNum)           (getSlotStatus( SlotNum) == SMS_GATE_SMS_STATUS_PENDING)
#define setSlotStatus( SlotNum, Status)   SmsGateQueue.SmsSlots[SlotNum].SlotStatus = Status

// Local functions :

static TYesNo _Send( void);
//try to send sms on active slot
static void _NextSlot( ESmsGateSmsStatus CurrentSlotStatus);
// Sets current slot status and moves to next slot

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void SmsGateInit( void)
// initialize
{
   _SmsSendTimeoutReset();
   _InitDone = YES;
   _ModulePowerOn = NO;
}

void SmsGateExecute( void)
// Execute - communicate with GSM module etc
{
TContactList ContactList;
int          Count;

   if( getSlotStatus(SmsGateQueue.ActiveSlot) != SMS_GATE_SMS_STATUS_PENDING){
      if( _ModulePowerOn){
         MGsmPowerResume();
         _ModulePowerOn = NO;
      }
      return;
   }
   if( !_ModulePowerOn){
      MGsmPowerOn();
      _ModulePowerOn = YES;
   }

   // start timer if not running
   if(!_SmsSendTimeoutRunning()) {
      _SmsSendTimeoutSet();
   }
   if( !_Send()){
      while(!ContactListOpen( &ContactList)){}
      Count = ContactListCount( &ContactList);
      ContactListClose(  &ContactList);

      // timed out or contact error
      if(_SmsSendTimeout() ||
            (SmsGateQueue.SmsSlots[SmsGateQueue.ActiveSlot].Broadcast &&
             SmsGateQueue.SmsSlots[SmsGateQueue.ActiveSlot].Contact >= Count)){
         _SmsSendTimeoutReset();
         _NextSlot(SMS_GATE_SMS_STATUS_ERROR);
         ContextSmsGateSave();
      }
      return;
   }

   _SmsSendTimeoutReset();
   ContactListOpen( &ContactList);
   if( SmsGateQueue.SmsSlots[SmsGateQueue.ActiveSlot].Broadcast &&
       SmsGateQueue.SmsSlots[SmsGateQueue.ActiveSlot].Contact < (ContactListCount( &ContactList) - 1))
   { // jump to next contact
      SmsGateQueue.SmsSlots[SmsGateQueue.ActiveSlot].Contact++;
      ContactListClose(  &ContactList);
      ContextSmsGateSave();
      return;
   }
   ContactListClose(  &ContactList);
   _NextSlot(SMS_GATE_SMS_STATUS_SENDED);
   ContextSmsGateSave();
}


static TSmsGateSlotNumber SmsGateDataSend( char *SmsText, int size, char *PhoneNumber, TYesNo binary)
// Send sms to phonenumber, when PhoneNumber is NULL, send to whole contact list
{
byte SlotIndex;
byte i;
TSmsSlot *EmptySlot;
byte PhoneLen;
TContactList ContactList;

   if( !_InitDone){
      return SMS_GATE_SLOT_ERROR;
   }

   EmptySlot = NULL;
   SlotIndex = SmsGateQueue.ActiveSlot;
   for( i = 0; i < SMS_GATE_SMS_SLOTS; i++ ){
      if( isSlotEmpty( SlotIndex)){
         EmptySlot = &SmsGateQueue.SmsSlots[SlotIndex];
         break;
      }
      SlotIndex++;
      if(SlotIndex < SMS_GATE_SMS_SLOTS){
         continue;
      }
      SlotIndex = 0;
   }
   if( !EmptySlot){
      SlotIndex = SmsGateQueue.ActiveSlot;
      for( i = 0; i < SMS_GATE_SMS_SLOTS; i++ ){
      // check whether there is some dead slot
         if( _SlotReadTimeout(i)){
            EmptySlot = &SmsGateQueue.SmsSlots[SlotIndex];
            _SlotReadTimeoutReset(i);
            break;
         }
         SlotIndex++;
         if(SlotIndex < SMS_GATE_SMS_SLOTS){
            continue;
         }
         SlotIndex = 0;
      }
      if( !EmptySlot){
         return SMS_GATE_SLOT_ERROR;
      }
   }

   if( PhoneNumber){
      strncpy(EmptySlot->SmsPhoneNumber, PhoneNumber, GSM_PHONE_NUMBER_SIZE);
      EmptySlot->SmsPhoneNumber[GSM_PHONE_NUMBER_SIZE] = '\0';
      PhoneLen = strlen(EmptySlot->SmsPhoneNumber);
      if( PhoneLen < 1){
         return SMS_GATE_PHONENUMBER_ERROR;
      }
      if( EmptySlot->SmsPhoneNumber[0] == '+'){
         i = 1;
      } else {
         i = 0;
      }
      for(; i < PhoneLen; i++){
         if( !isdigit(EmptySlot->SmsPhoneNumber[i])){
            return SMS_GATE_PHONENUMBER_ERROR;
         }
      }
      EmptySlot->Broadcast = NO;
      EmptySlot->Contact = CONTACT_INDEX_INVALID;
   } else {
      ContactListOpen( &ContactList);
      if( ContactListCount( &ContactList) == 0){
         ContactListClose( &ContactList);
         return SMS_GATE_PHONEBOOK_ERROR;
      }
      ContactListClose( &ContactList);
      EmptySlot->Contact = 0;
      EmptySlot->Broadcast = YES;
   }
   size = size > GSM_SMS_SIZE? GSM_SMS_SIZE : size;
   memcpy(EmptySlot->SmsText, SmsText, size);
   EmptySlot->SmsText[size] = '\0';
   EmptySlot->BinarySize = binary ? size : 0;
   EmptySlot->SlotStatus = SMS_GATE_SMS_STATUS_PENDING;
   ContextSmsGateSave();
   return SlotIndex;
}

TSmsGateSlotNumber SmsGateSmsSend( char *SmsText, char *PhoneNumber)
// Send sms to phonenumber, when PhoneNumber is NULL, send to whole contact list
{
   int len;
   len = strlen(SmsText);
   return SmsGateDataSend(SmsText, strlen(SmsText), PhoneNumber, NO);
}

TSmsGateSlotNumber SmsGateSmsSendBinary( char *SmsData, int dataLen, char *PhoneNumber)
// Send binary sms to phonenumber, when PhoneNumber is NULL, send to whole contact list
{
   return SmsGateDataSend(SmsData, dataLen, PhoneNumber, YES);
}

ESmsGateSmsStatus SmsGateSmsStatus( TSmsGateSlotNumber SlotNumber)
// Get status of SMS on slot
{
   if( !_InitDone){
      return SMS_GATE_SMS_STATUS_ERROR;
   }
   if( SlotNumber >= SMS_GATE_SMS_SLOTS){
      return SMS_GATE_SMS_STATUS_ERROR;
   }
   if( isSlotError(SlotNumber)){
       setSlotStatus( SlotNumber, SMS_GATE_SMS_STATUS_EMPTY);
       _SlotReadTimeoutReset(SlotNumber);
       return SMS_GATE_SMS_STATUS_ERROR;
   }
   if( isSlotSended(SlotNumber)){
       setSlotStatus( SlotNumber, SMS_GATE_SMS_STATUS_EMPTY);
       _SlotReadTimeoutReset(SlotNumber);
      return SMS_GATE_SMS_STATUS_SENDED;
   }
   if( isSlotEmpty(SlotNumber)){
      _SlotReadTimeoutReset(SlotNumber);
      return SMS_GATE_SMS_STATUS_EMPTY;
   }
   _SlotReadTimeoutSet(SlotNumber);
   return getSlotStatus( SlotNumber);
}

//------------------------------------------------------------------------------
//  Actual send
//------------------------------------------------------------------------------

static TYesNo _Send( void)
//try to send sms on active slot
{
TGsmContact ListContact;
TContactList ContactList;

   if( SmsGateQueue.SmsSlots[SmsGateQueue.ActiveSlot].Contact != CONTACT_INDEX_INVALID){
      ContactListOpen( &ContactList);
      if( SmsGateQueue.SmsSlots[SmsGateQueue.ActiveSlot].Contact >= ContactListCount( &ContactList)){
         ContactListClose( &ContactList);
         return NO;
      }
      ContactListLoad( &ContactList, &ListContact, SmsGateQueue.SmsSlots[SmsGateQueue.ActiveSlot].Contact);
      ContactListClose( &ContactList);
      strncpy(SmsGateQueue.SmsSlots[SmsGateQueue.ActiveSlot].SmsPhoneNumber, ListContact.PhoneNumber, GSM_PHONE_NUMBER_SIZE);
      SmsGateQueue.SmsSlots[SmsGateQueue.ActiveSlot].SmsPhoneNumber[GSM_PHONE_NUMBER_SIZE] = '\0';
   }

   if(SmsGateQueue.SmsSlots[SmsGateQueue.ActiveSlot].BinarySize != 0){
      // binary sms
      return MGsmSendBinary( SmsGateQueue.SmsSlots[SmsGateQueue.ActiveSlot].SmsPhoneNumber, SmsGateQueue.SmsSlots[SmsGateQueue.ActiveSlot].SmsText, SmsGateQueue.SmsSlots[SmsGateQueue.ActiveSlot].BinarySize);
   } else {
      // textual sms
      return MGsmSend( SmsGateQueue.SmsSlots[SmsGateQueue.ActiveSlot].SmsPhoneNumber, SmsGateQueue.SmsSlots[SmsGateQueue.ActiveSlot].SmsText);
   }
}

//------------------------------------------------------------------------------
//  Go to next slot
//------------------------------------------------------------------------------

static void _NextSlot( ESmsGateSmsStatus CurrentSlotStatus)
// Sets current slot status and moves to next slot
{
   setSlotStatus(SmsGateQueue.ActiveSlot, CurrentSlotStatus);
   _SlotReadTimeoutSet( SmsGateQueue.ActiveSlot);
   SmsGateQueue.ActiveSlot++;
   if( SmsGateQueue.ActiveSlot < SMS_GATE_SMS_SLOTS){
      return;
   }
   SmsGateQueue.ActiveSlot = 0;
}
