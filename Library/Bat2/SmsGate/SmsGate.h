//******************************************************************************
//
//   SmsGate.h     Sms gate service
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __SMS_GATE_H__
   #define __SMS_GATE_H__

#include "SmsGateDef.h"
#include "SmsGateConfig.h"
//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------
void SmsGateInit( void);
// initialize

void SmsGateExecute( void);
// Execute - communicate with GSM module etc

TSmsGateSlotNumber SmsGateSmsSend( char *SmsText, char *PhoneNumber);
// Send sms to phonenumber, when PhoneNumber is NULL, send to whole contact list

TSmsGateSlotNumber SmsGateSmsSendBinary( char *SmsData, int dataLen, char *PhoneNumber);
// Send binary sms to phonenumber, when PhoneNumber is NULL, send to whole contact list

ESmsGateSmsStatus SmsGateSmsStatus( TSmsGateSlotNumber SlotNumber);
// Get status of SMS on slot

#endif // __SMS_GATE_H__
