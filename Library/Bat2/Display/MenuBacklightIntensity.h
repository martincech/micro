//******************************************************************************
//
//   MenuBacklightIntensity.h  Menu backlight intensity
//   Version 1.0               (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuBacklightIntensity_H__
   #define __MenuBacklightIntensity_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuBacklightIntensity( int y);
// Menu backlight intensity at row <y>

#endif
