//******************************************************************************
//
//   MenuContrast.h   Menu contrast intensity
//   Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuContrast_H__
   #define __MenuContrast_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuContrast( int y);
// Menu contrast intensity at row <y>

#endif
