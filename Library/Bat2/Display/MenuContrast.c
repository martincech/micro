//******************************************************************************
//
//   MenuContrast.c   Menu contrast intensity
//   Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#include "MenuContrast.h"
#include "Gadget/DEdit.h"                  // Display edit value
#include "Display/Contrast.h"              // Contrast control
#include "Display/DisplayConfiguration.h"  // Display configuration

//------------------------------------------------------------------------------
//  Menu contrast
//------------------------------------------------------------------------------

void MenuContrast( int y)
// Menu contrast intensity at row <y>
{
int Contrast;

   Contrast = DisplayConfiguration.Contrast;
   if( !DEditSpin( DMENU_EDIT_X, y, &Contrast, CONTRAST_MIN, CONTRAST_MAX, ContrastTest)){
      ContrastSet();          // restore contrast
      return;
   }
   DisplayConfiguration.Contrast = Contrast;
} // MenuContrast
