//*****************************************************************************
//
//    Backlight.c   Backlight functions
//    Version 1.1   (c) VEIT Electronics
//
//*****************************************************************************

/*
   August 22, 2013 - BacklighStop() and BacklightIsOn() added
*/

#include "Backlight.h"
#include "System/System.h"                  // timer
#include "Display/BacklightPwm.h"           // Backlight PWM control
#include "Display/DisplayConfiguration.h"   // Display configuration

#define _BacklightTimerSet()        _BacklightTimer = TimerSlowCount( DisplayConfiguration.Backlight.Duration)
#define _BacklightTimerStop()       _BacklightTimer = 0

static word _BacklightTimer;
static TYesNo _On;

// Local functions :
static void _BacklightOn( void);
// Switch backlight by intensity

static void _BacklightOff( void);
// Switch off

//------------------------------------------------------------------------------
//  Initialization
//------------------------------------------------------------------------------

void BacklightInit( void)
// Initialization
{
   BacklightPwmInit();
   BacklighStop();
} // BacklightInit

//------------------------------------------------------------------------------
//  Normal
//------------------------------------------------------------------------------

void BacklightStart( void)
// Set normal mode
{
   _BacklightTimerStop();
   switch( DisplayConfiguration.Backlight.Mode){
      case BACKLIGHT_MODE_ON :
         _BacklightOn();               // always on
         break;
		  
      case BACKLIGHT_MODE_AUTO :
         _BacklightTimerSet();         // start off timeout
         _BacklightOn();               // switch on
         break;
		 
      case BACKLIGHT_MODE_OFF :
         _BacklightOff();             // always off
         break;
   }
} // BacklightStart

//------------------------------------------------------------------------------
//  On
//------------------------------------------------------------------------------

void BacklightOn( void)
// Conditionaly on
{
   _BacklightTimerStop();
   if( DisplayConfiguration.Backlight.Mode != BACKLIGHT_MODE_AUTO){
      return;                          // always on/off
   }
   _BacklightTimerSet();               // start off timeout
   _BacklightOn();                     // switch on
} // BacklightOn

//------------------------------------------------------------------------------
//  Off
//------------------------------------------------------------------------------

void BacklightOff( void)
// Conditionaly off
{
   _BacklightTimerStop();
   if( DisplayConfiguration.Backlight.Mode != BACKLIGHT_MODE_AUTO){
      return;                          // always on/off
   }
   _BacklightOff();                  // switch off
} // BacklightOff

//------------------------------------------------------------------------------
//  Stop
//------------------------------------------------------------------------------

void BacklighStop( void)
// Stop backlight
{
   _BacklightTimerStop();
   _BacklightOff();                  // switch off
} // BacklighShutdown

//------------------------------------------------------------------------------
//  Is on
//------------------------------------------------------------------------------

TYesNo BacklightIsOn( void)
// Check state
{
   return _On;
} // BacklighIsOn

//------------------------------------------------------------------------------
//  Test
//------------------------------------------------------------------------------

void BacklightTest( int Intensity)
// Test backlight intensity
{
   BacklightPwmOn( Intensity);
} // BacklightTest

//------------------------------------------------------------------------------
//  Timer
//------------------------------------------------------------------------------

void BacklightTimer( void)
// Backlight timer tick
{
   if( !_BacklightTimer){
      return;
   }
   if( --_BacklightTimer){
      return;
   }
   _BacklightOff();
} // BacklightTimer

//*****************************************************************************

//------------------------------------------------------------------------------
//  Set intensity
//------------------------------------------------------------------------------

static void _BacklightOn( void)
// Switch backlight by intensity
{
   BacklightPwmOn( DisplayConfiguration.Backlight.Intensity);
   _On = YES;
} // _BacklightOn

//------------------------------------------------------------------------------
//  Switch off
//------------------------------------------------------------------------------

static void _BacklightOff( void)
// Switch off
{
   BacklightPwmOff();
   _On = NO;
}