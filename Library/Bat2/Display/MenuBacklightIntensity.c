//******************************************************************************
//
//   MenuBacklightIntensity.c  Menu backlight intensity
//   Version 1.0               (c) VEIT Electronics
//
//******************************************************************************

#include "MenuBacklightIntensity.h"
#include "Gadget/DEdit.h"                  // Display edit value
#include "Display/Backlight.h"             // Backlight control
#include "Display/DisplayConfiguration.h"  // Display configuration

//------------------------------------------------------------------------------
//  Menu backlight intensity
//------------------------------------------------------------------------------

void MenuBacklightIntensity( int y)
// Menu backlight intensity at row <y>
{
int Intensity;
int OldMode;

   // set backlight mode always on for testing :
   OldMode = DisplayConfiguration.Backlight.Mode;       // save old mode
   DisplayConfiguration.Backlight.Mode = BACKLIGHT_MODE_ON;  // set always on
   BacklightStart();                                    // set mode
   // test intensity :
   Intensity = DisplayConfiguration.Backlight.Intensity;
   if( !DEditSpin( DMENU_EDIT_X, y, &Intensity,
                   BACKLIGHT_INTENSITY_MIN, BACKLIGHT_INTENSITY_MAX, BacklightTest)){
      DisplayConfiguration.Backlight.Mode = OldMode;    // restore old mode
      BacklightStart ();                                // set mode
      return;
   }
   DisplayConfiguration.Backlight.Mode = OldMode;       // restore mode
   DisplayConfiguration.Backlight.Intensity = Intensity;
   BacklightStart();                                    // set mode
} // MenuBacklightIntensity
