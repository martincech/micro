//******************************************************************************
//                                                                            
//   MenuDisplay.h   Display configuration menu
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuDisplay_H__
   #define __MenuDisplay_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuDisplay( void);
// Display configuration menu

#endif
