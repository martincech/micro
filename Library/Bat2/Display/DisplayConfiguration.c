//*****************************************************************************
//
//    DisplayConfiguration.c  Display configuration
//    Version 1.0             (c) VEIT Electronics
//
//*****************************************************************************

#include "DisplayConfiguration.h"

TDisplayConfiguration DisplayConfiguration;

//------------------------------------------------------------------------------
//  Display configuration
//------------------------------------------------------------------------------

const TDisplayConfiguration DisplayConfigurationDefault = {
   /* Mode      */    DISPLAY_MODE_BASIC,
   /* Contrast  */    CONTRAST_DEFAULT,
   /* PowerSave */    YES,
   /* _Spare    */    0,
   /* Backlight */ {
      /* Mode      */ BACKLIGHT_MODE_AUTO,
      /* Intensity */ BACKLIGHT_INTENSITY_DEFAULT,      
      /* Duration  */ BACKLIGHT_DURATION_DEFAULT
   }
};
