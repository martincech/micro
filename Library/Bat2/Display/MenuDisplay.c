//******************************************************************************
//                                                                            
//   MenuDisplay.c   Display configuration menu
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "MenuDisplay.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Config/Config.h"        // Configuration
#include "Str.h"                  // Strings

#include "Display/DisplayConfiguration.h"  // Display configuration
#include "Display/MenuBacklight.h"         // Backlight menu
#include "Display/MenuContrast.h"          // Contrast control menu

static DefMenu( DisplayMenu)
   STR_DISPLAY_POWERSAVE,
   STR_DISPLAY_MODE,
   STR_BACKLIGHT,
   STR_CONTRAST,
EndMenu()

typedef enum {
   MI_DISPLAY_POWERSAVE,
   MI_DISPLAY_MODE,
   MI_BACKLIGHT,
   MI_CONTRAST
} EDisplayMenu;

// Local functions :

static void DisplayParameters( int Index, int y, void *UserData);
// Display menu parameters

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuDisplay( void)
// Display configuration menu
{
TMenuData MData;
int       i;

   DMenuClear( MData);
   forever {
      if( !DMenu( STR_DISPLAY, DisplayMenu, DisplayParameters, 0, &MData)){
         ConfigDisplayConfigurationSave();
         return;
      }
      switch( MData.Item){
         case MI_DISPLAY_POWERSAVE :
            i = DisplayConfiguration.PowerSave;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            DisplayConfiguration.PowerSave = i;
            break;

         case MI_DISPLAY_MODE :
            i = DisplayConfiguration.Mode;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_DISPLAY_MODE, _DISPLAY_MODE_COUNT)){
               break;
            }
            DisplayConfiguration.Mode = i;
            break;

         case MI_BACKLIGHT :
            MenuBacklight();
            break;

         case MI_CONTRAST :
            MenuContrast( MData.y);
            break;

      }
   }
} // MenuDisplay

//******************************************************************************

//------------------------------------------------------------------------------
//   Parameters
//------------------------------------------------------------------------------

static void DisplayParameters( int Index, int y, void *UserData)
// Display menu parameters
{
   switch( Index){
      case MI_DISPLAY_POWERSAVE :
         DLabelEnum( DisplayConfiguration.PowerSave, ENUM_DISPLAY_POWERSAVE, DMENU_PARAMETERS_X, y);
         break;

      case MI_DISPLAY_MODE :
         DLabelEnum( DisplayConfiguration.Mode, ENUM_DISPLAY_MODE, DMENU_PARAMETERS_X, y);
         break;

      case MI_CONTRAST :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", DisplayConfiguration.Contrast);
         break;
   }
} // DisplayParameters
