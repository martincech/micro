//******************************************************************************
//                                                                            
//   Contrast.h     Display contrast
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Contrast_H__
   #define __Contrast_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void ContrastSet( void);
// Set contrast

void ContrastTest( int Contrast);
// Test contrast

#endif
