//******************************************************************************
//                                                                            
//   MenuBacklight.c   Backlight menu
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#include "MenuBacklight.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Config/Config.h"        // Configuration
#include "Str.h"                  // Strings

#include "Display/Backlight.h"               // Backlight control
#include "Display/DisplayConfiguration.h"    // Display configuration
#include "Display/MenuBacklightIntensity.h"  // Backlight intensity

static DefMenu( BacklightMenu)
   STR_BACKLIGHT_MODE,
   STR_BACKLIGHT_DURATION,
   STR_BACKLIGHT_INTENSITY,
EndMenu()

typedef enum {
   MI_BACKLIGHT_MODE,
   MI_BACKLIGHT_DURATION,
   MI_BACKLIGHT_INTENSITY
} EBacklightMenu;

// Local functions :

static void BacklightParameters( int Index, int y, void *UserData);
// Backlight menu parameters

//------------------------------------------------------------------------------
//   Backlight
//------------------------------------------------------------------------------

void MenuBacklight( void)
// Backlight menu
{
TMenuData MData;
int       i;

   DMenuClear( MData);
   forever {
      MData.Mask = 0;
      if( DisplayConfiguration.Backlight.Mode != BACKLIGHT_MODE_AUTO){
         MData.Mask = (1 << MI_BACKLIGHT_DURATION);
      }
      // selection :
      if( !DMenu( STR_BACKLIGHT, BacklightMenu, BacklightParameters, 0, &MData)){
         ConfigDisplayConfigurationSave();
         BacklightStart();             // restart with the new settings
         return;
      }
      switch( MData.Item){
         case MI_BACKLIGHT_MODE :
            i = DisplayConfiguration.Backlight.Mode;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_BACKLIGHT_MODE, _BACKLIGHT_MODE_COUNT)){
               break;
            }
            DisplayConfiguration.Backlight.Mode = i;
            break;

         case MI_BACKLIGHT_DURATION :
            i = DisplayConfiguration.Backlight.Duration;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, 1, BACKLIGHT_DURATION_MAX, STR_SECONDS)){
               break;
            }
            DisplayConfiguration.Backlight.Duration = i;
            break;

         case MI_BACKLIGHT_INTENSITY :
            MenuBacklightIntensity( MData.y);
            break;
      }
   }
} // MenuBacklight

//******************************************************************************

//------------------------------------------------------------------------------
//   Parameters
//------------------------------------------------------------------------------

static void BacklightParameters( int Index, int y, void *UserData)
// Backlight menu parameters
{
   switch( Index){
      case MI_BACKLIGHT_MODE :
         DLabelEnum( DisplayConfiguration.Backlight.Mode, ENUM_BACKLIGHT_MODE, DMENU_PARAMETERS_X, y);
         break;

      case MI_BACKLIGHT_DURATION :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", DisplayConfiguration.Backlight.Duration, STR_SECONDS);
         break;

      case MI_BACKLIGHT_INTENSITY :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", DisplayConfiguration.Backlight.Intensity);
         break;
   }
} // BacklightParameters
