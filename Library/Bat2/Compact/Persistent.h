//*****************************************************************************
//
//    Persistent.h    Bat2 storage definitions
//    Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Persistent_H__
   #define __Persistent_H__

#include "Country/Country.h"

#define UCLOCK_PARENT Country.Locale

#endif
