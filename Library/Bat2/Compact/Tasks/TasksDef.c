//******************************************************************************
//
//   TasksDef.c   Bat2 tasks def
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "Multitasking/Tasks.h"
#include "Hardware.h"
#include "System/System.h"
#include "Multitasking/Multitasking.h"
#include "TasksDef.h"

#define GUI_STACK_SIZE                  6*1024
#define WEIGHING_STACK_SIZE             6*1024
#define DATA_PUBLICATION_STACK_SIZE     8*1024
#define TCP_IP_STACK_SIZE               8*1024
#define REMOTE_STACK_SIZE               8*1024
#define GSM_STACK_SIZE                  4*1024
#define SMS_STACK_SIZE                  1*1024
#define RS485_STACK_SIZE                1*1024
#define SENSORPACK_STACK_SIZE           1*1024

static dword GuiStack[GUI_STACK_SIZE / 4];
static dword WeighingStack[WEIGHING_STACK_SIZE / 4];
static dword DataPublicationStack[DATA_PUBLICATION_STACK_SIZE / 4];
static dword RemoteStack[REMOTE_STACK_SIZE / 4];
static dword GsmStack[GSM_STACK_SIZE /4];
static dword TcpIpStack[TCP_IP_STACK_SIZE /4];
static dword SMSStack[SMS_STACK_SIZE /4];
static dword RS485Stack[RS485_STACK_SIZE /4];
static dword SensorPackStack[SENSORPACK_STACK_SIZE /4];

#ifndef _WIN32
static
#endif
void GuiTaskFn( TTask *Task);
// Gui task

#ifndef _WIN32
static
#endif
void WeighingTaskFn( TTask *Task);
// Weighing task

#ifndef _WIN32
static
#endif
void DataPublicationTaskFn( TTask *Task);
// Sms task

#ifndef _WIN32
static
#endif
void RemoteTaskFn( TTask *Task);
// Remote task

#ifndef _WIN32
static
#endif
void GsmTaskFn( TTask *Task);
// Gsm task

#ifndef _WIN32
static
#endif
void TCPIPTaskFn( TTask *Task);
// TCP/IP task

#ifndef _WIN32
static
#endif
void SMSTaskFn(TTask *Task);
// SMS send/Receive task

#ifndef _WIN32
static
#endif
void RS485TaskFn(TTask *Task);
// RS485 serve task

#ifndef _WIN32
static
#endif
void SensorPackTaskFn(TTask *Task);
// Sensor pack read data task

TTask GuiTask = {
 .EntryPoint = GuiTaskFn,
 .Stack = GuiStack,
 .StackSize = sizeof(GuiStack)
};
TTask WeighingTask = {
 .EntryPoint = WeighingTaskFn,
 .Stack = WeighingStack,
 .StackSize = sizeof(WeighingStack)
};
TTask DataPublicationTask = {
 .EntryPoint = DataPublicationTaskFn,
 .Stack = DataPublicationStack,
 .StackSize = sizeof(DataPublicationStack)
};
TTask GsmTask = {
 .EntryPoint = GsmTaskFn,
 .Stack = GsmStack,
 .StackSize = sizeof(GsmStack)
};

TTask RemoteTask = {
 .EntryPoint = RemoteTaskFn,
 .Stack = RemoteStack,
 .StackSize = sizeof(RemoteStack)
};

TTask TCPIPTask = {
 .EntryPoint = TCPIPTaskFn,
 .Stack = TcpIpStack,
 .StackSize = sizeof(TcpIpStack)
};

TTask SmsTask = {
 .EntryPoint = SMSTaskFn,
 .Stack = SMSStack,
 .StackSize = sizeof(SMSStack)
};

TTask RS485Task = {
 .EntryPoint = RS485TaskFn,
 .Stack = RS485Stack,
 .StackSize = sizeof(RS485Stack)
};

TTask SensorPackTask = {
 .EntryPoint = SensorPackTaskFn,
 .Stack = SensorPackStack,
 .StackSize = sizeof(SensorPackStack)
};


TTask *Tasks[TASKS_COUNT] = {
   &WeighingTask,
   &GsmTask,
   &RemoteTask,
   &TCPIPTask,
   &DataPublicationTask,
   &SmsTask,
   &RS485Task,
   &SensorPackTask,
   &GuiTask
};


int GuiTaskScheduler() {
   return MultitaskingTaskScheduler( &GuiTask);
}



#include "Menu/Menu.h"
#include "Graphic/Graphic.h"
#include "Lcd/St7259.h"                   // LCD routines
#include "Display/Backlight.h"
#include "Display/Contrast.h"
#include "Graphic/Gpu.h"
#include "Fonts.h"
#include "Menu/MenuPowerFailure.h"
#include "Remote/MenuRemote.h"

#ifndef _WIN32
static
#endif
void GuiTaskFn( TTask *Task)
// GUI task
{
   BacklightInit();
   GInit();
   GSetFont( TAHOMA16);

   forever {
      switch(MenuExitGet()) {
         case MENU_EXIT_REBOOT:
            SysReboot();
            return;

         case MENU_EXIT_BOOTLOADER:
            SysBootloader();
            BacklightOff();
            GShutdown();
            return;

         case MENU_EXIT_POWER_OFF:
            BacklightOff();
            GShutdown();
            SysOff();
            return;

         case MENU_EXIT_REMOTE:
            MenuExitClear();
            MenuRemote();
            break;

         case MENU_EXIT_POWER_FAILURE:
            MenuExitClear();
            BacklightOff();
            MenuPowerFailure();
            break;

         default:
            MenuExitClear();
            ContrastSet();
            BacklightStart();
            MenuExecute();
            break;
      }
   }
} // WeighingTaskFn







//------------------------------------------------------------------------------
//   Weighing
//------------------------------------------------------------------------------

#ifdef OPTION_BAT2_COMPACT
   #include "Ads1232/Ads1232.h"
#endif

#include "Scheduler/WeighingScheduler.h"  // Weighing scheduler executive

#ifndef _WIN32
static
#endif
void WeighingTaskFn( TTask *Task)
// Weighing task
{
   WeighingSchedulerInit();
   WeighingSchedulerResume();
   forever {
      switch( MultitaskingTaskScheduler( Task)) {
         case K_TIMER_SLOW:
            WeighingSchedulerExecute();
            break;

         case K_SHUTDOWN:
#if defined(OPTION_BAT2_COMPACT) && !defined(OPTION_SIMULATION)
            AdcPowerDown();
#endif
            return;

         case K_IDLE:
            MultitaskingReschedule();
            break;

         default:
            break;
      }
   }
} // WeighingTaskFn


//------------------------------------------------------------------------------
//   Gsm module
//------------------------------------------------------------------------------
#include "Message/MGsm.h"

#ifndef _WIN32
static
#endif
void GsmTaskFn( TTask *Task)
// Gsm task
{
   MGsmInit();                         // initialize GSM module
   forever {
      switch( MultitaskingTaskScheduler( Task)) {
         case K_TIMER_SLOW:
            MGsmExecute();                   // core GSM messaging
            break;

         case K_SHUTDOWN:
            GsmPowerOff();
            MGsmFree();
            return;

         case K_IDLE:
            MultitaskingReschedule();
            break;

         default:
            break;
      }
   }
}

//------------------------------------------------------------------------------
//   SMS
//------------------------------------------------------------------------------

#include "Message/Message.h"
#include "Megavi/MegaviTask.h"
#include "SmsGate/SmsGate.h"

#ifndef _WIN32
static
#endif
void DataPublicationTaskFn( TTask *Task)
// Sms task
{
   MessageInit();                      // initialize SMS statistics etc. messaging
   forever {
      switch( MultitaskingTaskScheduler( Task)) {
         case K_TIMER_SLOW:
            MegaviTaskExecuteSmsRequestClear();
            MessageExecute();                // GSM send statistics etc. messaging
            break;

         case K_SHUTDOWN:
            return;

         case K_IDLE:
            MultitaskingReschedule();
            break;

         default:
            break;
      }
   }
} // DataPublicationTaskFn

//------------------------------------------------------------------------------
//   Remote
//------------------------------------------------------------------------------

#include "Remote/Remote.h"

#ifndef _WIN32
static
#endif
void RemoteTaskFn( TTask *Task)
// Remote task
{
   RemoteInit();
   forever {
      switch( MultitaskingTaskScheduler( Task)) {
         case K_SHUTDOWN:
            return;

         case K_TIMER_FAST:
            RemoteExecute();
            break;

         case K_IDLE:
            MultitaskingReschedule();
            break;

         default:
            break;
      }
   }
} // RemoteTaskFn


//------------------------------------------------------------------------------
//   TCP/IP
//------------------------------------------------------------------------------
#include "Ethernet/EthernetLibrary.h"
#include "Device/VersionDef.h"

#ifndef _WIN32
static
#endif
void TCPIPTaskFn( TTask *Task)
// TCP/IP task
{
  if(BAT2_HAS_INTERNET_CONNECTION(Bat2Version.Modification)){
     EthernetLibraryInitNetworkInterface();
     forever {
        switch( MultitaskingTaskScheduler( Task)) {
           case K_SHUTDOWN:
              EthernetLibraryFreeNetworkInterface();
              return;

           case K_TIMER_FAST:
              EthernetLibraryExecute();
              break;

           case K_IDLE:
              MultitaskingReschedule();
              break;

           default:
              break;
        }
     }
  } else {
     forever {
        switch( MultitaskingTaskScheduler( Task)) {
           case K_SHUTDOWN:
              return;
           default:
              MultitaskingReschedule();
              break;
        }
     }
  }
} // TCPIPTaskFn

//------------------------------------------------------------------------------
//   Remote GSM using sockets
//------------------------------------------------------------------------------

//#ifdef CONNECTION_INTERNET_TYPE_GSM
//   #include "Remote/SocketIfEthernet.h"
//#endif
//#include "Remote/SocketIfSms.h"
//
//static void RemoteGsmSocketsFn( TTask *Task)
//// Remote sockets using GSM module task
//{
//   forever {
//      switch( MultitaskingTaskScheduler( Task)) {
//         case K_SHUTDOWN:
//            return;
//
//         case K_TIMER_SLOW:
//#ifdef CONNECTION_INTERNET_TYPE_GSM
//            SocketIfEthernetExecute();       // ethernet socket(for cases when GSM module is used)
//#endif
//            SocketIfSmsExecute();            // Sms channel execute
//            break;
//
//         case K_IDLE:
//            MultitaskingReschedule();
//            break;
//
//         default:
//            break;
//      }
//   }
//}

#include "SmsGate/SmsGate.h"

#ifndef _WIN32
static
#endif
void SMSTaskFn(TTask *Task)
// SMS send/Receive task
{
   SmsGateInit();
   forever {
       switch( MultitaskingTaskScheduler( Task)) {
         case K_SHUTDOWN:
            return;

         case K_TIMER_FAST:
            SmsGateExecute();
            break;

         case K_IDLE:
            MultitaskingReschedule();
            break;

         default:
            break;
      }
   }
} // SMSTaskFn


//------------------------------------------------------------------------------
//   RS485
//------------------------------------------------------------------------------

#include "Rs485/Rs485.h"


#ifndef _WIN32
static
#endif
void RS485TaskFn( TTask *Task)
// Remote task
{
   Rs485Init();
   forever {
      switch( MultitaskingTaskScheduler( Task)) {
         case K_SHUTDOWN:
            Rs485Free();
            return;

         case K_TIMER_FAST:
            Rs485Execute();
            break;

         case K_TIMER_SLOW:
            break;

         case K_IDLE:
            MultitaskingReschedule();
            break;

         default:
            break;
      }
   }
} // RS485TaskFn

//------------------------------------------------------------------------------
//   SensorPack
//------------------------------------------------------------------------------

#include "Weighing/WeighingConfiguration.h"
#include "SensorPack/Modbus/ModbusReg.h"
#include "SensorPack/Sensors/Sensors.h"
#include "ModbusHoldingRegistersMaster.h"
#include "Timer/Timer.h"
#include "Ads1232/Ads1232.h"
#include "Modbus/ModbusConfig.h"

#define ADC_READ_START_OFFSET    0
#define ADC_READ_PERIOD       2500

static int NextTickAdc;

#ifndef _WIN32
static
#endif
void SensorPackTaskFn( TTask *Task)
// Remote task
{
USHORT StaticBuf[255];
int Now = SysTimer();

   NextTickAdc = ADC_READ_START_OFFSET + Now;

   forever {
      switch( MultitaskingTaskScheduler( Task)) {
         case K_SHUTDOWN:
            return;
         
         case K_TIMER_SLOW:
            break;

         case K_TIMER_FAST:
            if(AdcRunning() && Rs485ConfiguredAsSensorPack()) {
               Now = SysTimer();
               if(TimerAfter(Now, NextTickAdc)){
                  // read ADC values
                  if(MB_ENOERR == eMBMReadHoldingRegistersPolled( ModbusOptions.Address, MODBUS_REG_WEIGHT_DATA_TIMESTAMP_1, 3 * ADC_SAMPLES_TO_READ, StaticBuf)){
                     AdcSetFromModbus((TAdcModbusSample*)StaticBuf, ADC_SAMPLES_TO_READ);
                  }
                  // read sensor values
                  if(MB_ENOERR == eMBMReadHoldingRegistersPolled( ModbusOptions.Address, MODBUS_REG_SENSOR_DATA_TEMPERATURE, MODBUS_REG_SENSOR_DATA_AMONIA - MODBUS_REG_SENSOR_DATA_TEMPERATURE + 1, StaticBuf)){
                     SensorsSetFromModbus((TSensorsModbusRegisters*)StaticBuf);
                  }
                  NextTickAdc = SysTimer() + ADC_READ_PERIOD;
               } 
            }
            break;

         case K_IDLE:
            MultitaskingReschedule();
            break;

         default:
            break;
      }
   }
} // SensorPackTaskFn
