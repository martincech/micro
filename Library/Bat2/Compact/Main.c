//*****************************************************************************
//
//    Main.c       Bat2 test main module
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Hardware.h"
#include "Uart/Uart.h"                    // UART services
#include "System/System.h"
//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
   AppInit();
   forever {
      SysScheduler();
   }
} // main

//------------------------------------------------------------------------------
//   Timer executive
//------------------------------------------------------------------------------

#include "Remote/SocketIfUsb.h"

void SysTimerExecute( void)
// Timer user executive
{
   extern void vMBPortTimerExecute( void );
   vMBPortTimerExecute();
   UartTimer();
   SocketIfUsbTimer();
} // SysTimerExecute