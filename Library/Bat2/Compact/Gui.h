//******************************************************************************
//
//   Gui.h   Bat2 GUI Scheduler
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __GUI_H__
   #define __GUI_H__

#include "Unisys/Uni.h"

int GuiScheduler( void);
// Gui scheduler

int GuiEventWait( void);
// Scheduler loop, returns nonempty event

TYesNo GuiIdle( void);

#endif // __GUI_H__
