//******************************************************************************
//
//   ConfigDef.h   Bat2 configuration data
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ConfigDef_H__
   #define __ConfigDef_H__

#ifndef __StorageDef_H__
   #include "Data/uStorageDef.h"
#endif

#ifndef __Bat2Def_H__
   #include "Config/Bat2Def.h"
#endif

//------------------------------------------------------------------------------
// Storage definition
//------------------------------------------------------------------------------

typedef enum {
   CONFIG_VERSION,
   CONFIG_DEVICE,
   CONFIG_COUNTRY,
   CONFIG_WEIGHT_UNITS,
   CONFIG_DISPLAY_CONFIGURATION,
   CONFIG_WEIGHING_CONFIGURATION,
   CONFIG_RS485_OPTIONS,
   CONFIG_MODBUS_OPTIONS,
   CONFIG_MEGAVI_OPTIONS,
   CONFIG_DACS_OPTIONS,
   CONFIG_GSM_MESSAGE,
   CONFIG_DATA_PUBLICATION,
   CONFIG_CELLULAR_DATA,
   CONFIG_ETHERNET,
   CONFIG_SPARE,
   _CONFIG_LAST,
   CONFIG_COUNT = _CONFIG_LAST
} EStorageItem;

typedef enum {
   CONTEXT_MENU,
   CONTEXT_WEIGHING,
   CONTEXT_COMMUNICATION,
   CONTEXT_SMS_GATE,
   CONTEXT_CALIBRATION_COEFFICIENTS,
   CONTEXT_CALIBRATION,
   _CONTEXT_LAST,
   CONTEXT_COUNT = _CONTEXT_LAST
} EContextItem;

// storage items descriptor :
extern const UStorage Config;

#define USTORAGE_VERSION     0x010F                 // persistent storage version

//------------------------------------------------------------------------------

#endif
