//******************************************************************************
//
//   Config.h     Bat2 configuration
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Config_H__

#include "../../Config/Config.h"    // non-Bat2Compact specific config

void ConfigCalibrationCoefficientsSave( void);
// Save Calibration coefficients

#endif
