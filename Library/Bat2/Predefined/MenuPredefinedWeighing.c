//******************************************************************************
//                                                                            
//   MenuPredefinedWeighing.c Predefined weighing menu
//   Version 1.0              (c) VEIT Electronics
//
//******************************************************************************

#include "MenuPredefinedWeighing.h"
#include "Graphic/Graphic.h"                // Graphic display
#include "Console/conio.h"                  // Console
#include "Sound/Beep.h"                     // Sounds
#include "Gadget/DCursor.h"                 // List Cursor
#include "Gadget/DList.h"                   // List drawings
#include "Gadget/DLayout.h"                 // Display layout
#include "Gadget/DNamedList.h"              // Display named list operations
#include "Gadget/DDirectory.h"              // Display list directory
#include "Predefined/PredefinedList.h"      // Predefined weighings list
#include "Menu/MenuWeighingConfiguration.h" // Menu Weighing configration/Predefined weighing
#include "Config/Config.h"                  // Project configuration
#include "Str.h"                            // project directory strings

#define MenuPredefinedWeighingEdit( PredefinedList, Predefined)  MenuWeighingConfiguration( (Predefined)->Name, Predefined, YES, PredefinedList)

// Local functions :

void _WeighingConfigurationUpdate( TPredefinedList *PredefinedList, TPredefinedWeighing *Predefined, TPredefinedWeighingIndex Index);
// Check for current Weighing Configuration

//------------------------------------------------------------------------------
//  Predefined weighing
//------------------------------------------------------------------------------

void MenuPredefinedWeighing( TPredefinedList *PredefinedList)
// Display predefined weighing menu
{
TPredefinedWeighingIdentifier Index;
TPredefinedWeighing           Predefined;

   forever {
      PredefinedListDefault( &Predefined);
      switch( DNamedListOperation( STR_PREDEFINED_WEIGHINGS, PredefinedList, &Index, Predefined.Name)){
         case DLIST_OPERATION_EDIT :
         case DLIST_OPERATION_COPY :
            PredefinedListLoad( PredefinedList, &Predefined, Index);          // load predefined weighing
            MenuPredefinedWeighingEdit( PredefinedList, &Predefined);         // edit predefined weighing
            if( !PredefinedListChanged( PredefinedList, &Predefined, Index)){
               break;                                         // item data not changed
            }
            _WeighingConfigurationUpdate( PredefinedList, &Predefined, Index);// update active weighing configuration
            PredefinedListSave( PredefinedList, &Predefined, Index);          // save predefined weighing
            break;

         case DLIST_OPERATION_CREATE :
            MenuPredefinedWeighingEdit( PredefinedList, &Predefined);         // edit default predefined weighing
            PredefinedListSave( PredefinedList, &Predefined, Index);          // save predefined weighing
            break;

         case DLIST_OPERATION_RENAME :
            break;

         case DLIST_OPERATION_DELETE :
            break;

         case DLIST_OPERATION_UNDEFINED:
            break;

         default :
            return;
      }
   }
}  // MenuPredefinedWeighing

//------------------------------------------------------------------------------
//  Select predefined weighing
//------------------------------------------------------------------------------

TYesNo MenuPredefinedWeighingSelect( TPredefinedList *PredefinedList, TUniStr Title, TPredefinedWeighingIndex *Identifier, TUniStr SpecialItem)
// Select predefined weighing, add <SpecialItem> at end of list
{
TPredefinedWeighingIndex      id;
TPredefinedWeighingIdentifier Index;

   // find directory index :
   id    = *Identifier;
   Index = 0;
   if( id != ULIST_IDENTIFIER_INVALID){
      Index = uDirectoryFindIdentifier((UDirectory *) uNamedListDirectory( PredefinedList), id);
      if( Index == UDIRECTORY_INDEX_INVALID){
         Index = 0;
      }
   }
   // select directory item :
   Index = DDirectorySelect( Title, uNamedListDirectory( PredefinedList), Index, SpecialItem);
   if( Index == UDIRECTORY_INDEX_INVALID){
      return( NO);                     // no selection
   }
   // check for special item :
   if( DDirectorySpecial( Index)){
      // set special item as invalid identifier
      *Identifier = ULIST_IDENTIFIER_INVALID;
      return( YES);
   }
   *Identifier = uDirectoryItemIdentifier((UDirectory *) uNamedListDirectory( PredefinedList), Index);
   return( YES);
} // MenuPredefinedWeighingSelect

//******************************************************************************

//------------------------------------------------------------------------------
//   Weighing configuration update
//------------------------------------------------------------------------------

void _WeighingConfigurationUpdate( TPredefinedList *PredefinedList, TPredefinedWeighing *Predefined, TPredefinedWeighingIndex Index)
// Check for current Weighing Configuration
{
TYesNo Fixed;
TPredefinedWeighingIdentifier Identifier;

   Identifier = PredefinedListIdentifier( PredefinedList, Index);
   if( WeighingConfiguration.Predefined != Identifier){
      return;                          // item is not active weighing configuration
   }
   // reload current weighing configuration :
   Fixed = WeighingConfiguration.MenuMask & (WCMM_TYPE);  // save fixed kind
   WeighingConfiguration              = *Predefined;
   WeighingConfiguration.Predefined   = Identifier;
   WeighingConfiguration.MenuMask    &= ~(WCMM_TYPE);     // enable kind selection
   if( Fixed){
      WeighingConfiguration.MenuMask |=  (WCMM_TYPE);     // disable kind selection
   }
   ConfigWeighingConfigurationSave();                          // save weighing configuration
} // _WeighingConfigurationUpdate
