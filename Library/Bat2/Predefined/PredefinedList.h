//******************************************************************************
//
//   PredefinedList.h  Predefined weighings list
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#ifndef __PredefinedList_H__
   #define __PredefinedList_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __PredefinedWeighingDef_H__
   #include "Predefined/PredefinedWeighingDef.h"
#endif   

#ifdef __WIN32__
   #ifndef __uStorageDef_H__
      #include "Data/uStorageDef.h"
   #endif
   DllImportExport extern const UDirectoryDescriptor _PredefinedListDescriptor;
#endif

typedef UNamedList   TPredefinedList;

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

#ifdef __cplusplus
   extern "C" {
#endif

void PredefinedListInit( void);
// Initialize

TYesNo PredefinedListRemoteLoad( void);
// Load from remote device

TYesNo PredefinedListRemoteSave( void);
// Save to remote device

TYesNo PredefinedListOpen( TPredefinedList *PredefinedList);
// open

void PredefinedListClose( TPredefinedList *PredefinedList);
// close

TPredefinedWeighingIdentifier PredefinedListIdentifier( TPredefinedList *PredefinedList, TPredefinedWeighingIndex Index);
// Returns identifier of predefined weighing at <Index>

TPredefinedWeighingIndex PredefinedListIndex( TPredefinedList *PredefinedList, TPredefinedWeighingIdentifier Identifier);
// Returns index of predefined weighing by predefined weighing <Identifier>

TYesNo PredefinedListName( TPredefinedList *PredefinedList, TPredefinedWeighingIdentifier Identifier, char *Name);
// Returns predefined weighing <Name>

void PredefinedListLoad( TPredefinedList *PredefinedList, TPredefinedWeighing *Predefined, TPredefinedWeighingIndex Index);
// Load <Predefined> by <Index>

void PredefinedListSave( TPredefinedList *PredefinedList, TPredefinedWeighing *Predefined, TPredefinedWeighingIndex Index);
// Save <Predefined> at <Index>

TYesNo PredefinedListChanged( TPredefinedList *PredefinedList, TPredefinedWeighing *Predefined, TPredefinedWeighingIndex Index);
// Returns YES if <Predefined> don't match data at <Index>

TYesNo PredefinedListIdentifierLoad( TPredefinedList *PredefinedList, TPredefinedWeighing *Predefined, TPredefinedWeighingIdentifier Identifier);
// Load <Predefined> by <Identifier>

//TYesNo PredefinedListIdentifierSave( TPredefinedWeighing *Predefined, TPredefinedWeighingIdentifier Identifier);
// Save <Predefined> by <Identifier>

void PredefinedListDefault( TPredefinedWeighing *Predefined);
// Load <Predefined> with defaults

#ifdef __cplusplus
   }
#endif

#endif
