//******************************************************************************
//
//   InternetPublication.h       Methods for data publication to web
//   Version 1.0                 (c) VEIT Electronics
//
//******************************************************************************


#ifndef __InternetPublication_H__
    #define __InternetPublication_H__

#include "InternetPublicationDef.h"
#include "Unisys/Uni.h"
#include "String/StrDef.h"

typedef struct
{
    char *url;          // base url adddress
    char *login;        // user login
    char *password;     // user password
} InternetPublicationSettings;

TYesNo InternetPublicationCheckConnection(InternetPublicationSettings settings);

TYesNo InternetPublicationPublish(InternetPublicationSettings settings, void *data, int dataSize);

void InternetPublicationGetLastError(TUniStr *text1, TUniStr *text2);
#endif
