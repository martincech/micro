//******************************************************************************
//
//   MenuDataPublication.c  Data publication menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuDataPublication.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DTime.h"         // Display time
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Gadget/DMsg.h"          // valid parameters check
#include "Gadget/DInput.h"
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Communication.h"
#include "Message/MenuGsmContacts.h"
#include "Device/VersionDef.h"
#include "Communication/InternetPublication.h"

static DefMenu( DataPublicationMenu)
STR_INTERFACE,
STR_USERNAME,
STR_PASSWORD,
STR_URL,
STR_CONTACTS,
STR_START_FROM_DAY,
STR_PERIOD,
STR_ACCELERATE_FROM_DAY,
STR_ACCELERATED_PERIOD,
STR_SEND_AT,
EndMenu()

typedef enum {
   MI_INTERFACE,
         MI_USERNAME,
         MI_PASSWORD,
         MI_URL,
         MI_CONTACTS,
         MI_START_FROM_DAY,
         MI_PERIOD,
         MI_ACCELERATE_FROM_DAY,
         MI_ACCELERATED_PERIOD,
         MI_SEND_AT,
} EDataPublicationMenu;

// Local functions :

static void DataPublicationParameters( int Index, int y, TDataPublication *Parameters);
// Draw data publication parameters

//------------------------------------------------------------------------------
//  Menu DataPublication
//------------------------------------------------------------------------------

void MenuDataPublication( void)
// Edit data publication parameters
{
TMenuData MData;
int       i;
TDataPublication DPSetting;
char Username[ DATA_PUBLICATION_USERNAME_SIZE + 1];
char Password[ DATA_PUBLICATION_PASSWORD_SIZE + 1];
char Url[ DATA_PUBLICATION_URL_SIZE + 1];
UTime     Time;
unsigned InterfaceMask = 0; // default - all enum items are enabled

   memcpy(&DPSetting, &DataPublication,sizeof(TDataPublication));
   DMenuClear( MData);
   forever {
      MData.Mask = 0;
      if(DPSetting.Interface == DATA_PUBLICATION_INTERFACE_DISABLED){
         MData.Mask |= ~(1<< MI_INTERFACE);
      }
      if((DPSetting.Interface & DATA_PUBLICATION_INTERFACE_INTERNET) != DATA_PUBLICATION_INTERFACE_INTERNET){
         MData.Mask |= (1 << MI_USERNAME) | (1 << MI_PASSWORD) | (1 << MI_URL);
      }
      if((DPSetting.Interface & DATA_PUBLICATION_INTERFACE_SMS) != DATA_PUBLICATION_INTERFACE_SMS){
         MData.Mask |= (1<< MI_CONTACTS);
      }

      // selection :
      if( !DMenu( STR_DATA_PUBLICATION, DataPublicationMenu, (TMenuItemCb *)DataPublicationParameters, &DPSetting, &MData)){
         if((DPSetting.Interface & DATA_PUBLICATION_INTERFACE_INTERNET) == DATA_PUBLICATION_INTERFACE_INTERNET){
            DMsgWait();
            InternetPublicationSettings settings = {
               DPSetting.Url,      // url
               DPSetting.Username, //username
               DPSetting.Password  //password
            };
            if(!InternetPublicationCheckConnection(settings)){
               TUniStr str1;
               TUniStr str2;
               InternetPublicationGetLastError(&str1, &str2);
               if(DMsgOkCancel(STR_ERROR, str1, str2)){
                  continue;
               }
            }
         }
         memcpy(&DataPublication, &DPSetting, sizeof(TDataPublication));
         ConfigDataPublicationSave();
         return;
      }

      switch( MData.Item){
         case MI_INTERFACE :
            i = DPSetting.Interface;

            if(!BAT2_HAS_GSM_MODULE(Bat2Version.Modification)){
                InterfaceMask |= (1 << DATA_PUBLICATION_INTERFACE_SMS) | (1 << DATA_PUBLICATION_INTERFACE_BOTH);
            }
            if(!BAT2_HAS_INTERNET_CONNECTION(Bat2Version.Modification)){
               InterfaceMask |= (1 << DATA_PUBLICATION_INTERFACE_INTERNET) | (1 << DATA_PUBLICATION_INTERFACE_BOTH);
            }

            if( !DEditEnumMaskable( DMENU_EDIT_X, MData.y, &i, ENUM_DATA_PUBLICATION_INTERFACE, _DATA_PUBLICATION_INTERFACE_LAST, InterfaceMask)){
               break;
            }
            DPSetting.Interface = (byte)i;
            break;

         case MI_USERNAME :
            strcpy( Username, DPSetting.Username);
            if( !DInputText( STR_USERNAME, STR_USERNAME, Username, DATA_PUBLICATION_USERNAME_SIZE, NO)){
               break;
            }
            strcpy( DPSetting.Username, Username);
            break;

         case MI_PASSWORD :
            Password[0] = 0;
            if( !DInputText( STR_PASSWORD, STR_PASSWORD, Password, DATA_PUBLICATION_PASSWORD_SIZE, NO)){
               break;
            }
            strcpy( DPSetting.Password, Password);
            break;

         case MI_URL :
            strcpy( Url, DPSetting.Url);
            if( !DInputTextL(STR_URL, STR_URL, Url, DATA_PUBLICATION_URL_SIZE, NO, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~:/?#[]@!$&'()*+,;=")){
               break;
            }
            strcpy( DPSetting.Url, Url);
            break;

         case MI_CONTACTS :
            MenuGsmContactsSendStatisticsCheckList();
            break;

         case MI_START_FROM_DAY :
            i = DPSetting.StartFromDay;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, DATA_PUBLICATION_START_FROM_DAY_MIN, DATA_PUBLICATION_START_FROM_DAY_MAX, 0)){
               break;
            }
            DPSetting.StartFromDay = (TDayNumber)i;
            break;

         case MI_PERIOD :
            i = DPSetting.Period;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, DATA_PUBLICATION_PERIOD_MIN, DATA_PUBLICATION_PERIOD_MAX, 0)){
               break;
            }
            DPSetting.Period = (TDayNumber)i;
            break;

         case MI_ACCELERATE_FROM_DAY :
            i = DPSetting.AccelerateFromDay;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, DATA_PUBLICATION_ACCELERATE_FROM_DAY_MIN, DATA_PUBLICATION_ACCELERATE_FROM_DAY_MAX, 0)){
               break;
            }
            DPSetting.AccelerateFromDay = (TDayNumber)i;
            break;

         case MI_ACCELERATED_PERIOD :
            i = DPSetting.AcceleratedPeriod;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, DATA_PUBLICATION_ACCELERATED_PERIOD_MIN, DATA_PUBLICATION_ACCELERATED_PERIOD_MAX, 0)){
               break;
            }
            DPSetting.AcceleratedPeriod = (TDayNumber)i;
            break;

         case MI_SEND_AT :
            uTime( &Time, DPSetting.SendAt);
            if( !DInputTime( STR_SEND_AT, STR_ENTER_TIME, &Time)){
               break;
            }
            DPSetting.SendAt = uTimeGauge( &Time);
            break;
      }
   }
} // MenuDataPublication

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void DataPublicationParameters( int Index, int y, TDataPublication *Parameters)
// Draw data publication parameters
{
   switch( Index){
   case MI_INTERFACE :
      DLabelEnum( Parameters->Interface, ENUM_DATA_PUBLICATION_INTERFACE, DMENU_PARAMETERS_X, y);
      break;

   case MI_USERNAME :
      DLabelNarrow( Parameters->Username, DMENU_PARAMETERS_X, y);
      break;

   case MI_PASSWORD :
      DLabelNarrow( "*****", DMENU_PARAMETERS_X, y);
      break;

   case MI_URL :
      DLabelNarrow( Parameters->Url, DMENU_PARAMETERS_X, y);
      break;

   case MI_CONTACTS :
      break;

   case MI_START_FROM_DAY :
      DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->StartFromDay, 0);
      break;

   case MI_PERIOD :
      DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->Period, 0);
      break;

   case MI_ACCELERATE_FROM_DAY :
      DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->AccelerateFromDay, 0);
      break;

   case MI_ACCELERATED_PERIOD :
      DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->AcceleratedPeriod, 0);
      break;

   case MI_SEND_AT :
      DTimeShortRight( Parameters->SendAt, DMENU_PARAMETERS_X, y);
      break;

   }
} // DataPublicationParameters
