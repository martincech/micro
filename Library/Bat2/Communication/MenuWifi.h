//******************************************************************************
//
//   MenuWifi.h  Wifi menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuWifi_H__
   #define __MenuWifi_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Communication_H__
   #include "Communication.h"
#endif


void MenuWifi( void);
// Menu wifi

#endif
