//******************************************************************************
//
//   Communication.c
//   Version 1.0                 (c) VEIT Electronics
//
//******************************************************************************

#include "Communication.h"

TDataPublication DataPublication;
TCellularData CellularData;
TEthernet Ethernet;

//------------------------------------------------------------------------------
//  Data publication
//------------------------------------------------------------------------------

const TDataPublication DataPublicationDefault = {
   /* Interface */ DATA_PUBLICATION_INTERFACE_DISABLED,
   /* Username */ DATA_PUBLICATION_USERNAME_DEFAULT,
   /* Password */ DATA_PUBLICATION_PASSWORD_DEFAULT,
   /* Url */ DATA_PUBLICATION_URL_DEFAULT,
   /* StartFromDay */ DATA_PUBLICATION_START_FROM_DAY_DEFAULT,
   /* Period */ DATA_PUBLICATION_PERIOD_DEFAULT,
   /* AccelerateFromDay */ DATA_PUBLICATION_ACCELERATE_FROM_DAY_DEFAULT,
   /* AcceleratedPeriod */ DATA_PUBLICATION_ACCELERATED_PERIOD_DEFAULT,
   /* SendAt */ DATA_PUBLICATION_SEND_AT_DEFAULT
};

//------------------------------------------------------------------------------
//  Cellular data
//------------------------------------------------------------------------------

const TCellularData CellularDataDefault = {
   /* Apn */ CELLULAR_DATA_APN_DEFAULT,
   /* Username */ CELLULAR_DATA_USERNAME_DEFAULT,
   /* Password */ CELLULAR_DATA_PASSWORD_DEFAULT
};

//------------------------------------------------------------------------------
//  Ethernet
//------------------------------------------------------------------------------

const TEthernet EthernetDefault = {
   /* Dhcp */ YES,
   /* Ip */ ETHERNET_IP_DEFAULT,
   /* SubnetMask */ ETHERNET_SUBNET_MASK_DEFAULT,
   /* Gateway */ ETHERNET_GATEWAY_DEFAULT,
   /* PrimaryDns */ ETHERNET_PRIMARY_DNS_DEFAULT,
};

//------------------------------------------------------------------------------
//  GSM context
//------------------------------------------------------------------------------


#include "Archive/ArchiveDef.h"
#include "Message/GsmMessageDef.h"

TCommunicationContext CommunicationContext;

const TCommunicationContext CommunicationContextDefault = {
   /* SendTime         */ CLOCK_INVALID,
   /* ClosedDay        */ DAY_NUMBER_INVALID,
   /* SendDay          */ DAY_NUMBER_INVALID,
   /* ClosedIndex      */ ARCHIVE_INDEX_INVALID,
   /* SendIndex        */ ARCHIVE_INDEX_INVALID,
   /* StatisticContact */ CONTACT_INDEX_INVALID,

   /* CommandUnsend    */ NO,

   /* EventContact     */ CONTACT_INDEX_INVALID,
   /* LogIndex         */ LOG_INDEX_INVALID,
   /* LogTimestamp     */ 0,
   /* EventUnsend      */ NO,

   /* ContactType      */ GSM_SMS_FORMAT_MOBILE_PHONE,
   /* Message          */ "",
   /* PhoneNumber      */ "",
   /* SendSmsSlot      */ SMS_GATE_SLOT_ERROR

//   /* ISendDay         */ DAY_NUMBER_INVALID,
//   /* ISendIndex       */ ARCHIVE_INDEX_INVALID,
//   /* IPublishStartTime*/ -1
};
