//******************************************************************************
//
//   WeighingPlanList.h     Weighing plan list
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __WeighingPlanList_H__
   #define __WeighingPlanList_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeighingSchedulerDef_H__
   #include "Scheduler/WeighingSchedulerDef.h"
#endif

#ifndef __uNamedListDef_H__
   #include "Data/uNamedListDef.h"
#endif

#ifndef __uList_H__
   #include "Data/uList.h"
#endif

typedef UList TWeighingPlanList;

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

#ifdef __cplusplus
   extern "C" {
#endif
TYesNo WeighingPlanListRemoteLoad( void);
// Load from remote device

TYesNo WeighingPlanListRemoteSave( void);
// Save to remote device

void WeighingPlanListInit( void);
// Initialize

TYesNo WeighingPlanListOpen( TWeighingPlanList *WeighingPlanList);
// Open list

void WeighingPlanListClose( TWeighingPlanList *WeighingPlanList);
// Close list

TWeighingPlanIndex WeighingPlanListCount( TWeighingPlanList *WeighingPlanList);
// Returns actual list items count

TWeighingPlanIndex WeighingPlanListCapacity( void);
// Returns maximum list items count

TWeighingPlanIndex WeighingPlanListAdd( TWeighingPlanList *WeighingPlanList, TWeighingPlan *WeighingPlan);
// Add <WeighingPlan> returns index

void WeighingPlanListDelete( TWeighingPlanList *WeighingPlanList, TWeighingPlanIndex Index);
// Delete <WeighingPlan> returns index

void WeighingPlanListLoad( TWeighingPlanList *WeighingPlanList, TWeighingPlan *WeighingPlan, TWeighingPlanIndex Index);
// Load <WeighingPlan> by <Index>

void WeighingPlanListSave( TWeighingPlanList *WeighingPlanList, TWeighingPlan *WeighingPlan, TWeighingPlanIndex Index);
// Save <WeighingPlan> at <Index>


//------------------------------------------------------------------------------
//  Time list
//------------------------------------------------------------------------------

byte WeighingPlanTimeListCount( TWeighingPlan *WeighingPlan);
// Returns actual time list items count

byte WeighingPlanTimeListCapacity( void);
// Return maximum time list item count

TWeighingTimeIndex WeighingPlanTimeListAdd( TWeighingPlan *WeighingPlan, TWeighingTime *WeighingTime);
// Add <WeighingTime> to <WeighingPlan>, returns index

void WeighingPlanTimeListDelete( TWeighingPlan *WeighingPlan, TWeighingTimeIndex Index);
// Delete WeighingTime from <WeighingPlan> at index Index

void WeighingPlanTimeListLoad( TWeighingTime *WeighingTime, TWeighingPlan *WeighingPlan, TWeighingTimeIndex Index);
// Load <WeighingTime> from <WeighingPlan> at Index

void WeighingPlanTimeListSave( TWeighingTime *WeighingTime, TWeighingPlan *WeighingPlan, TWeighingTimeIndex Index);
// Save <WeighingTime> to <WeighingPlan> at Index

#ifdef __cplusplus
   }
#endif
#endif // __WeighingPlanList_H__


