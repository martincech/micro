//******************************************************************************
//
//   WeighingSchedulerCtl.c  Weighing scheduler services
//   Version 1.0             (c) VEIT Electronics
//
//******************************************************************************

#include "WeighingScheduler.h"
#include "System/System.h"
#include "Config/Config.h"
#include "Weighing/WeighingConfiguration.h"
#include "Weighing/Weighing.h"
#include "Scheduler/WeighingPlanList.h"
#include "Curve/CurveList.h"
#include "Curve/CorrectionList.h"
#include "Multitasking/Multitasking.h"
#include "Hardware.h"

#if defined(OPTION_BAT2_COMPACT) && !defined(OPTION_SIMULATION)
   #include "Weighing/pWeighing.h" // platform weighing
#endif

//#define SCHEDULER_PERIOD      30       // scheduler service period [s]
#define SCHEDULER_PERIOD      1        // scheduler service period [s]

#if SCHEDULER_PERIOD <= 1
   #define SCHEDULER_COUNTER  1        // execute immediately
#else
   #define SCHEDULER_COUNTER  TimerSlowCount( SCHEDULER_PERIOD)
#endif

#define _SchedulerTimerSet()    _SchedulerTimer = SCHEDULER_COUNTER
#define _SchedulerTimerStart()  _SchedulerTimer = 1        // execute immediately
#define _SchedulerTimerTick()  (--_SchedulerTimer == 0)

//------------------------------------------------------------------------------

// Activation times list :

static byte          _SchedulerTimer;

// Local functions :
static void _DayZeroTime( void);
// Calculate zero day time

static void _DayUpdate( TDayNumber Day);
// Update technological day and day close time by <Day>

static TYesNo _DayClosed( void);
// Check for day closed

static void _DayClose( void);
// Close active day

static void _DayResume( void);
// Resume weighing day by current data/time

static UTimeGauge _DayDuration( void);
// Returns day duration period

static TYesNo _ActiveDay( void);
// Check for active day

static TYesNo _ActiveTime( void);
// Check for active time

static void _GrowthCurvesCopy( void);
// copy growth curves to context

static void _CorrectionCurvesCopy( void);
// copy correction curves to context

static void _TimeListCopy( void);
// copy weighing plan to context

TMutex Mutex;

#define Entry()         MultitaskingMutexSet( &Mutex)

#define Return()         MultitaskingMutexRelease( &Mutex); return



//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

void WeighingSchedulerInit( void)
// Initialize
{
   WeighingInit();
   WeighingPlanListInit();
   _SchedulerTimerStart();
   MultitaskingMutexInit( &Mutex);
} // WeighingSchedulerInit

//------------------------------------------------------------------------------
//  Resume
//------------------------------------------------------------------------------

void WeighingSchedulerResume( void)
// Power on initialization
{
   Entry();
   _SchedulerTimerStart();             // start service period
   // check for day closed :
   switch( WeighingStatus()){
      case WEIGHING_STATUS_UNDEFINED :
      case WEIGHING_STATUS_STOPPED :
         WeighingPause();
         Return();

      case WEIGHING_STATUS_CALIBRATION :
         WeighingCalibration();
         Return();

      case WEIGHING_STATUS_DIAGNOSTICS :
         WeighingDiagnostics();
         Return();

      case WEIGHING_STATUS_WAIT :
         // waiting for start
         break;

      case WEIGHING_STATUS_SLEEP :
      case WEIGHING_STATUS_SUSPENDED :
      case WEIGHING_STATUS_RELEASED :
      case WEIGHING_STATUS_WEIGHING :
         // weighing is active
         if( _DayClosed()){
            _DayResume();              // close day, prepare current day
         }
         WeighingStatusSet( WEIGHING_STATUS_SLEEP);
         break;
   }
   WeighingResume();                   // resume weighing processing
   ContextWeighingContextSave();        // save context
   Return();
} // WeighingSchedulerResume

//------------------------------------------------------------------------------
//  Execute
//------------------------------------------------------------------------------

void WeighingSchedulerExecute( void)
// Executive
{
   Entry();
   WeighingExecute();
   switch( WeighingStatus()){
      case WEIGHING_STATUS_UNDEFINED :
      case WEIGHING_STATUS_STOPPED :
      case WEIGHING_STATUS_CALIBRATION :
      case WEIGHING_STATUS_DIAGNOSTICS :
         Return();

      case WEIGHING_STATUS_WAIT :
         if( WeighingStartTime() > SysClock()){
            Return();                    // start later
         }
         MultitaskingMutexRelease( &Mutex);
         WeighingSchedulerStart();     // start weighing
         break;

      case WEIGHING_STATUS_SLEEP :
      case WEIGHING_STATUS_SUSPENDED :
      case WEIGHING_STATUS_RELEASED :
      case WEIGHING_STATUS_WEIGHING :
         break;
   }
   // check for scheduler service period :
   if( !_SchedulerTimerTick()){
      Return();                          // wait for next tick
   }
   _SchedulerTimerSet();
   // check for day closed :
   if( _DayClosed()){
      _DayClose();                     // close current day, prepare next day
   }
   WeighingStatusSave();               // remember weighing status
   // check for scheduler active :
   if( !WeighingConfiguration.Planning){
      if( WeighingStatus() == WEIGHING_STATUS_SUSPENDED){
         WeighingPause();              // suspended by user
      } else {
         WeighingStatusSet( WEIGHING_STATUS_WEIGHING);
         WeighingWeighing();
      }
      // save weighing status (if needed) :
      if( WeighingStatus() != WeighingLastStatus()){
         ContextWeighingContextSave();
      }
      Return();                          // skip scheduler
   }
   // check for day and time activation :
   if( _ActiveDay() && _ActiveTime()){
      // weighing active
      if( WeighingStatus() == WEIGHING_STATUS_SUSPENDED){
         WeighingPause();              // pause weighing
         Return();                       // wait for inactive state
      }
      WeighingStatusSet( WEIGHING_STATUS_WEIGHING);
      WeighingWeighing();
   } else {
      // weighing inactive
      if( WeighingStatus() == WEIGHING_STATUS_RELEASED){
         WeighingWeighing();           // run weighing
         Return();                       // wait for active state
      }
      WeighingStatusSet( WEIGHING_STATUS_SLEEP);
      WeighingPause();
   }
   // save weighing status (if needed) :
   if( WeighingStatus() != WeighingLastStatus()){
      ContextWeighingContextSave();
   }
   Return();
} // WeighingSchedulerExecute

//------------------------------------------------------------------------------
//  Start
//------------------------------------------------------------------------------

void WeighingSchedulerStart( void)
// Start weighing
{
   Entry();
   if( (WeighingStatus() != WEIGHING_STATUS_STOPPED) &&
       (WeighingStatus() != WEIGHING_STATUS_WAIT)){
      Return();
   }

   _TimeListCopy();                              // copy data from nvm to context
   _GrowthCurvesCopy();
   _CorrectionCurvesCopy();

   WeighingStatusSet( WEIGHING_STATUS_RELEASED);    // wait for start
   WeighingStartTimeSet( SysClock());
   _DayZeroTime();                               // calculate day zero
   _DayUpdate( WeighingInitialDay());            // set actual day & day close time
   _SchedulerTimerStart();                       // start service period
   WeighingStart();                              // start processing
   WeighingDayActiveSet( NO);                    // no day started yet
   // start a new day :
   if( _ActiveDay()){
      WeighingDayNew();
   }
   ContextWeighingContextSave();                  // save context
   Return();
} // WeighingSchedulerStart

//------------------------------------------------------------------------------
//   Start at
//------------------------------------------------------------------------------

void WeighingSchedulerStartAt( UClockGauge Time)
// Start weighing
{
   Entry();
   if( WeighingStatus() != WEIGHING_STATUS_STOPPED){
      Return();
   }
   WeighingStatusSet( WEIGHING_STATUS_WAIT);
   WeighingStartTimeSet( Time);
   _SchedulerTimerStart();                       // start service period
   ContextWeighingContextSave();                  // save context
   Return();
} // WeighingSchedulerStartAt

//------------------------------------------------------------------------------
//  Stop
//------------------------------------------------------------------------------

void WeighingSchedulerStop( void)
// Stop weighing
{
   Entry();
   switch( WeighingStatus()){
      case WEIGHING_STATUS_UNDEFINED :
      case WEIGHING_STATUS_STOPPED :
         // already stopped
         Return();

      case WEIGHING_STATUS_DIAGNOSTICS :
         // stop diagnostics :
         WeighingStatusSet( WEIGHING_STATUS_STOPPED);
         WeighingStop();
         break;

      case WEIGHING_STATUS_CALIBRATION :
         // pop previous state :
         WeighingStatusRestore();
         switch(WeighingStatus()) {
            case WEIGHING_STATUS_STOPPED:
            case WEIGHING_STATUS_WAIT:
               WeighingStop();
               break;
         }
         break;

      case WEIGHING_STATUS_WAIT :
      case WEIGHING_STATUS_SLEEP :
      case WEIGHING_STATUS_WEIGHING :
      case WEIGHING_STATUS_SUSPENDED :
      case WEIGHING_STATUS_RELEASED :
         // stop weighing :
         WeighingStatusSet( WEIGHING_STATUS_STOPPED);
         WeighingStop();
         break;
   }
   ContextWeighingContextSave();
   Return();
} // WeighingSchedulerStop

//------------------------------------------------------------------------------
//  Suspend
//------------------------------------------------------------------------------

void WeighingSchedulerSuspend( void)
// Suspend weighing
{
   Entry();
   switch( WeighingStatus()){
      case WEIGHING_STATUS_UNDEFINED :
      case WEIGHING_STATUS_STOPPED :
      case WEIGHING_STATUS_WAIT :
      case WEIGHING_STATUS_SUSPENDED :
      case WEIGHING_STATUS_DIAGNOSTICS :
      case WEIGHING_STATUS_CALIBRATION :
         Return();

      default:
         WeighingStatusSet( WEIGHING_STATUS_SUSPENDED);
         break;
   }
   _SchedulerTimerStart();                       // start service period
   ContextWeighingContextSave();
   Return();
} // WeighingSchedulerSuspend

//------------------------------------------------------------------------------
//  Release
//------------------------------------------------------------------------------

void WeighingSchedulerRelease( void)
// Relase weighing
{
   Entry();
   switch( WeighingStatus()){
      case WEIGHING_STATUS_UNDEFINED :
      case WEIGHING_STATUS_STOPPED :
      case WEIGHING_STATUS_WAIT :
      case WEIGHING_STATUS_WEIGHING :
      case WEIGHING_STATUS_RELEASED :
      case WEIGHING_STATUS_DIAGNOSTICS :
      case WEIGHING_STATUS_CALIBRATION :
         Return();

      default:
         WeighingStatusSet( WEIGHING_STATUS_RELEASED);
         break;
   }
   _SchedulerTimerStart();             // start service period
   // create a new day (if needed) :
   if( !WeighingDayActive()){
      WeighingDayNew();                // create a new day
   }
   ContextWeighingContextSave();
   Return();
} // WeighingSchedulerRelease

//------------------------------------------------------------------------------
//   Calibration
//------------------------------------------------------------------------------

void WeighingSchedulerCalibration( void)
// Start weighing calibration
{
   Entry();
   if( WeighingStatus() == WEIGHING_STATUS_DIAGNOSTICS){
      Return();
   }
   if( WeighingStatus() == WEIGHING_STATUS_CALIBRATION){
      Return();
   }
   WeighingStatusSave();                         // save status
   WeighingStatusSet( WEIGHING_STATUS_CALIBRATION);
   WeighingCalibration();
   ContextWeighingContextSave();
   Return();
} // WeighingSchedulerCalibration

//------------------------------------------------------------------------------
//   Diagnostics
//------------------------------------------------------------------------------

void WeighingSchedulerDiagnostics( void)
// Start diagnostic weighing
{
   Entry();
   if( WeighingStatus() != WEIGHING_STATUS_STOPPED){
      Return();
   }
   WeighingStatusSet( WEIGHING_STATUS_DIAGNOSTICS);
   WeighingDiagnostics();
   ContextWeighingContextSave();
   Return();
} // WeighingSchedulerDiagnostics

//------------------------------------------------------------------------------
//   Next day
//------------------------------------------------------------------------------

void WeighingSchedulerDayNext( void)
// Next technological day
{
UClockGauge NewTime;

   Entry();
   UTimeGauge dayDuration = WeighingDayDuration();
   WeighingDayDurationSet(1);
   NewTime  = WeighingDayZero();
   NewTime -= _DayDuration();          // move zero one day back
   _DayClose();                        // close current day, prepare next day
   WeighingDayZeroSet( NewTime);       // set to context
   WeighingDayDurationSet(dayDuration);
   ContextWeighingContextSave();
   Return();
} // WeighingSchedulerDayNext

//------------------------------------------------------------------------------
//  Technological day start
//------------------------------------------------------------------------------

UDateTimeGauge WeighingDayStartDateTime( TDayNumber Day)
// Calculate start date time of technological <Day>
{
UDateTimeGauge DateTime;

   DateTime   = uClockDateTime( WeighingDayZero());
   DateTime  += Day * _DayDuration();
   return( DateTime);
} // WeighingDayStartDateTime

//******************************************************************************

//------------------------------------------------------------------------------
//  Day Zero time
//------------------------------------------------------------------------------

static void _DayZeroTime( void)
// Calculate zero day time
{
UDateTimeGauge DateTime;
UDateGauge     Date;
UTimeGauge     Time;

   // get wall clock time :
   DateTime = SysDateTime();
   if( _DayDuration() == TIME_DAY){
      // standard day duration
      Time     = uDateTimeTime( DateTime);
      Date     = uDateTimeDate( DateTime);
      // check for technological day start :
      if( Time < WeighingDayStart()){
         Date--;                                           // technological day started yesterday
      } // technological day started today
      Time     = WeighingDayStart();                       // set zero day time at day start
      Date    -= WeighingInitialDay();                     // move zero day date at first technological day
      DateTime = uDateTimeCompose( Date, Time);
   } else {
      // day simulation (ignore day start)
      DateTime  = (DateTime / _DayDuration()) * _DayDuration();          // start at nearest "day"
      DateTime -= WeighingInitialDay() * _DayDuration();     // move zero "day" by initial day number
   }
   WeighingDayZeroSet( uDateTimeClock( DateTime));
} // _DayZeroTime

//------------------------------------------------------------------------------
//  Technological day
//------------------------------------------------------------------------------

static TDayNumber _DayByDateTime( UDateTimeGauge DateTime)
// Calculate technological day by <DateTime>
{
UDateTimeGauge FirstDay;
TDayNumber     DayNumber;

   FirstDay = uClockDateTime( WeighingDayZero());
   if( DateTime < FirstDay){
      return( 0);                                // actual time before zero day
   }
   DateTime  -= FirstDay;                        // interval from zero day
   DayNumber  = DateTime / _DayDuration();       // number of technological days
   return( DayNumber);
} // _DayByDateTime

//------------------------------------------------------------------------------
//  Update day
//------------------------------------------------------------------------------

static void _DayUpdate( TDayNumber Day)
// Update technological day and day close time by <Day>
{
UDateTimeGauge DateTime;

   // get actual day data :
   DateTime  = WeighingDayStartDateTime( Day);   // start of actual day
   DateTime += _DayDuration();                   // end of actual day
   // save to context :
   WeighingDaySet( Day);
   WeighingDayCloseAtSet( uDateTimeClock( DateTime));
} // _DayUpdate

//------------------------------------------------------------------------------
//  Day closed
//------------------------------------------------------------------------------

static TYesNo _DayClosed( void)
// Check for day closed
{
   if( SysClock() < WeighingDayCloseAt()){
      return( NO);
   }
   return( YES);
} // _DayClosed

//------------------------------------------------------------------------------
//  Close day
//------------------------------------------------------------------------------

static void _DayClose( void)
// Close active day
{
   WeighingPause();                         // pause weighing
   if( WeighingDayActive()){
      WeighingDayClose();
   }
   // prepare next day :
   WeighingDay()++;
   _DayUpdate( WeighingDay());
   // check for suspended state :
   if( WeighingStatus() == WEIGHING_STATUS_SUSPENDED){
      ContextWeighingContextSave();          // save context for a new day
      return;                               // don't create a new day
   }
   // check for a new day activity :
   if( _ActiveDay()){
      WeighingDayNew();
   }
   ContextWeighingContextSave();             // save context for a new day
} // _DayClose

//------------------------------------------------------------------------------
//  Next day
//------------------------------------------------------------------------------

static void _DayResume( void)
// Resume weighing day by current data/time
{
TDayNumber Day;

   // check for last day activity :
   if( WeighingDayActive()){
      WeighingDayClose();                   // close day
   }
   // get a new day by current date/time :
   Day = _DayByDateTime( SysDateTime());    // actual day
   _DayUpdate( Day);                        // update day & day close time
   // don't check for suspended state (will be released)
   // check for a new day activity :
   if( _ActiveDay()){
      WeighingDayNew();
   }
   ContextWeighingContextSave();             // save context for a new day
} // _DayResume

//------------------------------------------------------------------------------
//  Day duration
//------------------------------------------------------------------------------

static UTimeGauge _DayDuration( void)
// Returns day duration period
{
   if( (WeighingDayDuration() > 0) &&
       (WeighingDayDuration() < TIME_DAY)){
      return( WeighingDayDuration());  // simulated day (less than 24h)
   }
   return( TIME_DAY);                  // standard day (24h)
} // _DayDuration

//------------------------------------------------------------------------------
//   Active day
//------------------------------------------------------------------------------

static TYesNo _ActiveDay( void)
// Check for active day
{
UDow           Dow;
UDateTimeGauge DateTime;
int            Period;
int            Actual;

   // check for scheduler active :
   if( !WeighingConfiguration.Planning){
      return( YES);                    // all days active
   }
   if( WeighingContext.WeighingPlan.Days.Mode == WEIGHING_DAYS_MODE_DAY_OF_WEEK){
      // check for active day of week
      DateTime = WeighingDayStartDateTime( WeighingDay()); // weighing day started at
      Dow = uDateTimeDow( DateTime);   // get day of week
      if( WeighingContext.WeighingPlan.Days.Days & (1 << Dow)){
         return( YES);
      }
      return( NO);                     // inactive day of week
   }
   // check for days pattern :
   Period = WeighingContext.WeighingPlan.Days.SuspendedDays;
   // check for start day :
   if( WeighingDay() < WeighingContext.WeighingPlan.Days.StartDay){
      return( NO);                     // waiting for start day
   }
   // get actual day of Period :
   Actual = (WeighingDay() - WeighingContext.WeighingPlan.Days.StartDay) % Period;
   if( Actual < WeighingContext.WeighingPlan.Days.WeighingDays){
      return( YES);                    // active day
   }
   return( NO);                        // inactive day
} // _ActiveDay

//------------------------------------------------------------------------------
//   Active time
//------------------------------------------------------------------------------

static TYesNo _ActiveTime( void)
// Check for active time
{
UTimeGauge Time;
int        i;

   if( !WeighingConfiguration.Planning){
      return( YES);                         // all times active
   }
   if( !WeighingContext.WeighingPlan.TimesCount){
      return( YES);                         // time interval list empty - always active
   }
   Time = uDateTimeTime( SysDateTime());    // current time
   for( i = 0; i < WeighingContext.WeighingPlan.TimesCount; i++){
      if( (Time >= WeighingContext.WeighingPlan.Times[ i].From) && (Time <= WeighingContext.WeighingPlan.Times[ i].To)){
         return( YES);                      // active time interval
      }
   }
   return( NO);                             // inactive time interval
} // _ActiveTime

//------------------------------------------------------------------------------
//   Time list copy
//------------------------------------------------------------------------------

static void _TimeListCopy( void)
// copy weighing plan to context
{
TWeighingPlanList WeighingPlanList;

   // load weighing plan from NVM and copy it
   while(!WeighingPlanListOpen( &WeighingPlanList));
   WeighingPlanListLoad( &WeighingPlanList, &WeighingContext.WeighingPlan, WeighingConfiguration.WeighingPlan);
   WeighingPlanListClose( &WeighingPlanList);
} // _TimeListCopy

//------------------------------------------------------------------------------
//   Growth curves copy
//------------------------------------------------------------------------------

static void _GrowthCurvesCopy( void)
// copy growth curves to context
{
TCurveList CurveList;

   while( !CurveListOpen( &CurveList));
   CurveListIdentifierLoad( &CurveList, &WeighingContext.GrowthCurveMale, WeighingConfiguration.Male.GrowthCurve);
   CurveListIdentifierLoad( &CurveList, &WeighingContext.GrowthCurveFemale, WeighingConfiguration.Female.GrowthCurve);
   CurveListClose( &CurveList);
}

//------------------------------------------------------------------------------
//   CorrectionCurvesCopy
//------------------------------------------------------------------------------

static void _CorrectionCurvesCopy( void)
// copy correction curves to context
{
TCorrectionList CorrectionList;

   while( !CorrectionListOpen( &CorrectionList));
   CorrectionListIdentifierLoad( &CorrectionList, &WeighingContext.CorrectionCurve, WeighingConfiguration.CorrectionCurve);
   CorrectionListClose( &CorrectionList);
}
