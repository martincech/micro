//******************************************************************************
//
//   WeighingPlan.c Weighing plan configuration
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "WeighingPlan.h"

TWeighingPlan WeighingPlan;

//------------------------------------------------------------------------------
//  Weighing plan
//------------------------------------------------------------------------------

const TWeighingPlan WeighingPlanDefault = {
   /* Name */             "",
   /* SyncWithDayStart */ YES,
   /* _Dummy */           0,
   /* Days */ {
      /* Mode */          WEIGHING_DAYS_MODE_DAY_OF_WEEK,
      /* Days */          WEIGHING_DAYS_DAYS_DEFAULT,
      /* WeighingDays */  WEIGHING_DAYS_WEIGHING_DAYS_DEFAULT,
      /* SuspendedDays */ WEIGHING_DAYS_SUSPENDED_DAYS_DEFAULT,
      /* StartDay */      WEIGHING_DAYS_START_DAY_DEFAULT,
   },
   /* Times */ {
      {TIME_RANGE_FROM_DEFAULT, TIME_RANGE_TO_DEFAULT}
   },
   /* TimesCount*/        1
};


