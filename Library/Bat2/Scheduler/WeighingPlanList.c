//******************************************************************************
//
//   WeighingPlanList.c     Weighing plan list
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "WeighingPlanList.h"
#include "Scheduler/WeighingPlan.h"
#include "Data/uList.h"
#include "Multitasking/Multitasking.h"
#include <string.h>

uListAlloc( _WeighingPlanListDescriptor, WEIGHING_PLAN_COUNT, sizeof( TWeighingPlan));

#define _TimeListCapacity() WEIGHING_TIME_COUNT

//------------------------------------------------------------------------------
//  Remote load
//------------------------------------------------------------------------------

TYesNo WeighingPlanListRemoteLoad( void)
// Load from remote device
{
   if (!uListCopy(&_WeighingPlanListDescriptor, FILE_WEIGHING_PLAN_LOCAL, FILE_WEIGHING_PLAN_REMOTE)){
      return NO;
   }
   return YES;
} // WeighingPlanListRemoteLoad

//------------------------------------------------------------------------------
//  Remote save
//------------------------------------------------------------------------------

TYesNo WeighingPlanListRemoteSave( void)
// Save to remote device
{
   return uListCopy( &_WeighingPlanListDescriptor, FILE_WEIGHING_PLAN_REMOTE, FILE_WEIGHING_PLAN_LOCAL);
} // WeighingPlanListRemoteSave

//------------------------------------------------------------------------------
//   Initialize
//------------------------------------------------------------------------------

void WeighingPlanListInit( void)
// Initialize
{
   uListInit( &_WeighingPlanListDescriptor, FILE_WEIGHING_PLAN_LOCAL);
}

//------------------------------------------------------------------------------
//  Open
//------------------------------------------------------------------------------

TYesNo WeighingPlanListOpen( TWeighingPlanList *WeighingPlanList)
// Open list
{
TYesNo Success;
   Success = uListOpen( WeighingPlanList, &_WeighingPlanListDescriptor, FILE_WEIGHING_PLAN_LOCAL);
   if(!Success) {
      MultitaskingReschedule();
   }
   return Success;
} // WeighingPlanListOpen

//------------------------------------------------------------------------------
//  Close
//------------------------------------------------------------------------------

void WeighingPlanListClose( TWeighingPlanList *WeighingPlanList)
// Close list
{
   uListClose( WeighingPlanList);
} // WeighingPlanListClose

//------------------------------------------------------------------------------
//   Count
//------------------------------------------------------------------------------

TWeighingPlanIndex WeighingPlanListCount( TWeighingPlanList *WeighingPlanList)
// Returns actual list items count
{
   return( uListCount( WeighingPlanList));
} // WeighingPlanListCount

//------------------------------------------------------------------------------
//   Capacity
//------------------------------------------------------------------------------

TWeighingPlanIndex WeighingPlanListCapacity( void)
// Returns maximum list items count
{
   return( uListCapacity( &_WeighingPlanListDescriptor));
} // WeighingPlanListCapacity

//------------------------------------------------------------------------------
//   Add
//------------------------------------------------------------------------------

TWeighingPlanIndex WeighingPlanListAdd( TWeighingPlanList *WeighingPlanList, TWeighingPlan *WeighingPlan)
// Add <WeighingPlan> returns index
{
   return( uListAppend( WeighingPlanList, WeighingPlan));
} // WeighingPlanListAdd

//------------------------------------------------------------------------------
//   Delete
//------------------------------------------------------------------------------

void WeighingPlanListDelete( TWeighingPlanList *WeighingPlanList, TWeighingPlanIndex Index)
// Add <WeighingPlan> returns index
{
   uListDelete(  WeighingPlanList, Index);
} // WeighingPlanListDelete

//------------------------------------------------------------------------------
//   Load
//------------------------------------------------------------------------------

void WeighingPlanListLoad( TWeighingPlanList *WeighingPlanList, TWeighingPlan *WeighingPlan, TWeighingPlanIndex Index)
// Load <WeighingPlan> by <Index>
{
   uListItemLoad( WeighingPlanList, Index, WeighingPlan);
} // WeighingPlanListLoad

//------------------------------------------------------------------------------
//   Save
//------------------------------------------------------------------------------

void WeighingPlanListSave( TWeighingPlanList *WeighingPlanList, TWeighingPlan *WeighingPlan, TWeighingPlanIndex Index)
// Save <WeighingPlan> at <Index>
{
   uListItemSave( WeighingPlanList, Index, WeighingPlan);
} // WeighingPlanListSave

//------------------------------------------------------------------------------
//   Count time list
//------------------------------------------------------------------------------

byte WeighingPlanTimeListCount( TWeighingPlan *WeighingPlan)
// Returns actual time list items count
{
   return WeighingPlan->TimesCount;
}

//------------------------------------------------------------------------------
//   Capacity time list
//------------------------------------------------------------------------------

byte WeighingPlanTimeListCapacity( void)
// Return maximum time list item count
{
   return _TimeListCapacity();
}

//------------------------------------------------------------------------------
//   Add time to time list
//------------------------------------------------------------------------------

TWeighingTimeIndex WeighingPlanTimeListAdd( TWeighingPlan *WeighingPlan, TWeighingTime *WeighingTime)
// Add <WeighingTime> to <WeighingPlan>, returns index
{
   if( WeighingPlan->TimesCount >= _TimeListCapacity()){
      return _TimeListCapacity() + 1;
   }

   WeighingPlan->Times[ WeighingPlan->TimesCount] = *WeighingTime;
   return WeighingPlan->TimesCount++;
}

//------------------------------------------------------------------------------
//   Delete time from time list
//------------------------------------------------------------------------------

void WeighingPlanTimeListDelete( TWeighingPlan *WeighingPlan, TWeighingTimeIndex Index)
// Delete WeighingTime from <WeighingPlan> at index Index
{
byte i;

   if( WeighingPlan->TimesCount <= 1 || Index >= WeighingPlan->TimesCount){
      return;
   }

   WeighingPlan->TimesCount--;
   for( i = Index; i < WeighingPlan->TimesCount; i++){
      WeighingPlan->Times[ i] = WeighingPlan->Times[ i + 1];
   }
}

//------------------------------------------------------------------------------
//  Load time from time list
//------------------------------------------------------------------------------

void WeighingPlanTimeListLoad( TWeighingTime *WeighingTime, TWeighingPlan *WeighingPlan, TWeighingTimeIndex Index)
// Load WeighingTime from <WeighingPlan> at Index
{
   if( Index >= WeighingPlan->TimesCount){
      return;
   }
   *WeighingTime = WeighingPlan->Times[ Index];
}

//------------------------------------------------------------------------------
//  Save time to time list
//------------------------------------------------------------------------------

void WeighingPlanTimeListSave( TWeighingTime *WeighingTime, TWeighingPlan *WeighingPlan, TWeighingTimeIndex Index)
// Save <WeighingTime> to <WeighingPlan> at Index
{
   if( Index >= WeighingPlan->TimesCount){
      return;
   }

   WeighingPlan->Times[ Index] = *WeighingTime;
}
