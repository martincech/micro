//******************************************************************************
//
//   WeighingPlan.h Weighing plan configuration
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __WeighingPlan_H__
   #define __WeighingPlan_H__

#ifndef __WeighingSchedulerDef_H__
   #include "Scheduler/WeighingSchedulerDef.h"
#endif

//extern       TWeighingPlan WeighingPlan;
extern const TWeighingPlan WeighingPlanDefault;

#endif
