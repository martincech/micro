//******************************************************************************
//
//   MenuWeighingPlans.c     Weighing plans menu
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWeighingPlans.h"
#include "Gadget/DMenu.h"                    // Display menu
#include "Str.h"                             // Strings
#include "Gadget/DCursor.h"                  // List Cursor
#include "Gadget/DList.h"                    // List utility
#include "Gadget/DLayout.h"
#include "Gadget/DEvent.h"
#include "Gadget/DMsg.h"
#include "Graphic/Graphic.h"
#include "Sound/Beep.h"
#include "Console/Conio.h"
#include <stdlib.h>                          // itoa function
#include <string.h>

#include "Scheduler/WeighingPlan.h"          // Default weighing time
#include "Scheduler/MenuWeighingPlan.h"
#include "Scheduler/WeighingPlanList.h"

static DefMenu( WeighingPlansMenu)
   STR_EDIT,
   STR_CREATE,
   STR_DELETE,
EndMenu()

typedef enum {
   MI_EDIT,
   MI_CREATE,
   MI_DELETE
} EWeighingPlansMenu;

static void _DisplayPage( TWeighingPlanList *WeighingPlanList, TListCursor *Cursor, TUniStr SpecialItem);
// Display plan page

//------------------------------------------------------------------------------
//  Menu Weighing plans
//------------------------------------------------------------------------------

void MenuWeighingPlans( void)
// Menu Weighing plans
{
TMenuData            MData;
TWeighingPlan        WeighingPlan;
TWeighingPlanIndex   Index;
TWeighingPlanList    WeighingPlanList;
   while(!WeighingPlanListOpen( &WeighingPlanList));
   DMenuClear( MData);
   forever {
      // check for directory capacity :
      MData.Mask = 0;
      if( WeighingPlanListCount( &WeighingPlanList) == WeighingPlanListCapacity()){
         MData.Mask |= (1 << MI_CREATE);              // disable create
      }
      // check for empty list :
      if( WeighingPlanListCount( &WeighingPlanList) <= 0){
         MData.Mask |= (1 << MI_DELETE) | (1 << MI_EDIT);              // disable delete
      }
      // selection :
      if( !DMenu( STR_WEIGHING_PLAN, WeighingPlansMenu, 0, 0, &MData)){
         WeighingPlanListClose( &WeighingPlanList);
         return;
      }
      // prepare contact for edit :
      Index = WEIGHING_PLAN_INDEX_INVALID;
      if( MData.Item != MI_CREATE){
         // select contact from list
         Index = MenuWeighingPlansSelect( &WeighingPlanList, STR_WEIGHING_PLAN, 0, 0);
         if( Index == WEIGHING_PLAN_INDEX_INVALID){
            continue;                                  // selection canceled
         }
      }
      switch( MData.Item){
         case MI_EDIT :
            WeighingPlanListLoad( &WeighingPlanList, &WeighingPlan, Index);
            MenuWeighingPlan( &WeighingPlan);
            WeighingPlanListSave( &WeighingPlanList, &WeighingPlan, Index);
            continue;

         case MI_CREATE :
            memcpy( &WeighingPlan, &WeighingPlanDefault, sizeof( TWeighingPlan)); // fill with defaults
            MenuWeighingPlan( &WeighingPlan);
            if( memequ( &WeighingPlan, &WeighingPlanDefault, sizeof( TWeighingPlan))){
               continue;                                 // not modified by user
            }
            if( strequ( WeighingPlan.Name, "")){
               // name not entered - set same as phone number
               strcpy( WeighingPlan.Name, WEIGHING_PLAN_NAME_DEFAULT);
               itoa(WeighingPlanListCount( &WeighingPlanList) + 1, &WeighingPlan.Name[ strlen(WEIGHING_PLAN_NAME_DEFAULT)], 10);
            }
            Index = WeighingPlanListAdd( &WeighingPlanList, &WeighingPlan); // append item
            continue;

         case MI_DELETE :
            WeighingPlanListLoad( &WeighingPlanList, &WeighingPlan, Index);
            if( !DMsgYesNo( STR_CONFIRMATION, STR_REALLY_DELETE, WeighingPlan.Name)){
               continue;
            }
            WeighingPlanListDelete( &WeighingPlanList, Index);
            continue;
      }
   }
} // MenuWeighingPlans


//******************************************************************************
//------------------------------------------------------------------------------
//   Select
//------------------------------------------------------------------------------

TWeighingPlanIndex MenuWeighingPlansSelect( TWeighingPlanList *WeighingPlanList, TUniStr Title, TWeighingPlanIndex CurrentPos, TUniStr SpecialItem)
// Select item from WeighingPlan list
{
TListCursor    Cursor;
int            Count;

   Count = WeighingPlanListCount( WeighingPlanList) + (SpecialItem? 1 : 0);
   DCursorInit( &Cursor, Count, DLIST_ROWS_COUNT_MAX);
   DCursorIndexSet( &Cursor, CurrentPos);                  // move cursor at initial position
   DCursorUseGridSet( &Cursor, NO);               // don't display horizontal lines
   forever {
      if( DCursorPageChanged( &Cursor)){
         // redraw page
         GClear();                                // clear display
         DLayoutTitle( Title);                    // display title
         DLayoutStatus( STR_BTN_CANCEL, STR_BTN_SELECT, 0); // display status line
      }
      if( DCursorRowChanged( &Cursor)){
         _DisplayPage( WeighingPlanList, &Cursor, SpecialItem);
         DCursorRowUpdate( &Cursor);
         GFlush();                                // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            DCursorRowUp( &Cursor);
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            DCursorRowDown( &Cursor);
            break;

         case K_RIGHT :
         case K_RIGHT | K_REPEAT :
            DCursorPageDown( &Cursor);
            break;

         case K_LEFT :
         case K_LEFT | K_REPEAT :
            DCursorPageUp( &Cursor);
            break;

         case K_ENTER :
            return( (TWeighingPlanIndex)DCursorIndex( &Cursor));

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return( WEIGHING_PLAN_INDEX_INVALID);
      }
   }
}

//------------------------------------------------------------------------------
//   Display page
//------------------------------------------------------------------------------

#define PLAN_X    5

static void _DisplayPage( TWeighingPlanList *WeighingPlanList, TListCursor *Cursor, TUniStr SpecialItem)
// Display plan page
{
int           RowY;
int           i;
TWeighingPlan WeighingPlan;
byte          Index;

   DListClear( Cursor);                // clear list area
   // draw points
   for( i = 0; i < DCursorRowCount( Cursor); i++){
      RowY = DListY( i);
      if( !DListCursor( Cursor, i)){
         continue;
      }
      GTextAt( PLAN_X, RowY);

      if( SpecialItem && (i == DCursorRowCount( Cursor) - 1)){
         //last item is special item
         cputs( SpecialItem);
         continue;
      }
      // get item data :
      Index = DCursorPage( Cursor) + i;
      WeighingPlanListLoad( WeighingPlanList, &WeighingPlan, Index);
      cputs( WeighingPlan.Name);
   }

   GFlush();                           // redraw rows
}
