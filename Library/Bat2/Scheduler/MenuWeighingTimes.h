//******************************************************************************
//
//   MenuWeighingTimes.h  Weighing times menu
//   Version 1.0          (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuWeighingTimes_H__
   #define __MenuWeighingTimes_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeighingSchedulerDef_H__
   #include "Scheduler/WeighingSchedulerDef.h"
#endif


void MenuWeighingTimes( TWeighingPlan *WeighingPlan);
// Menu weighing times

#endif
