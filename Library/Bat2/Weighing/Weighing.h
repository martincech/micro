//******************************************************************************
//
//   Weighing.h     Weighing services
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Weighing_H__
   #define __Weighing_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __uClock_H__
   #include "Time/uClock.h"
#endif

#ifndef __Bat2Def_H__
   #include "Config/Bat2Def.h"
#endif

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void WeighingInit( void);
// Initialize

void WeighingResume( void);
// Power on initialization

void WeighingExecute( void);
// Executive

//------------------------------------------------------------------------------

void WeighingStart( void);
// Start weighing

void WeighingStop( void);
// Stop weighing

void WeighingPause( void);
// Pause weighing

void WeighingWeighing( void);
// Run weighing

void WeighingCalibration( void);
// Start calibration

void WeighingDiagnostics( void);
// Start diagnostics

void WeighingDayClose( void);
// Close weighing day

void WeighingDayNew( void);
// Start a new day

#endif
