//******************************************************************************
//
//   Curve.h        Weighing curve utility
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Curve_H__
   #define __Curve_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __SexDef_H__
   #include "Weight/SexDef.h"
#endif

#ifndef __GrowthCurveDef_H__
   #include "Curve/GrowthCurveDef.h"
#endif   

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void CurveInit( void);
// Initialize

void CurveResume( void);
// Reload current curve

void CurveStart( void);
// Initialization after weighing start

TDayNumber CurvePointsCount( byte Sex);
// Growth curve points count

TYesNo CurveHasDay( byte Sex, TDayNumber Day);
// Returns YES if the <Day> is included in the curve range

TWeightGauge CurveWeight( byte Sex, TDayNumber Day);
// Returns weight for <Day>

#endif
