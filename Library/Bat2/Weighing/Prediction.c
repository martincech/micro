//******************************************************************************
//
//   Prediction.c   Weighing prediction
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Weighing/Prediction.h"
#include "Weighing/WeighingConfiguration.h"
#include "Weighing/Statistics.h"
#include "Weighing/Curve.h"
#include "Archive/Archive.h"

// Local functions :

static void _NextAutomatic( void);
// Automatic prediction

static void _NextGrowthCurve( void);
// Prediction by growth curve

//------------------------------------------------------------------------------

static void _NextAutomaticSingle( byte Sex);
// Automatic prediction

static void _NextGrowthCurveSingle( byte Sex);
// Prediction by growth curve

//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

void PredictionInit( void)
// Initialize
{
   CurveInit();
} // PredictionInit

//------------------------------------------------------------------------------
//  Resume
//------------------------------------------------------------------------------

void PredictionResume( void)
// Reload prediction data
{
   CurveResume();
} // PredictionResume

//------------------------------------------------------------------------------
//  Start
//------------------------------------------------------------------------------

void PredictionStart( void)
// Start weighing prediction
{
   WeighingContext.TargetWeight       = WeighingConfiguration.Male.InitialWeight;
   WeighingContext.TargetWeightFemale = WeighingConfiguration.Female.InitialWeight;
   CurveStart();                            // load growth curve
   switch( WeighingPredictionMode()){
      case PREDICTION_MODE_AUTOMATIC :
         break;                             // use initial weight as target

      case PREDICTION_MODE_GROWTH_CURVE :
         _NextGrowthCurve();                // prepare target by growth curve
         break;
   }
} // PredictionStart

//------------------------------------------------------------------------------
//  Next
//------------------------------------------------------------------------------

void PredictionNew( void)
// New day prediction
{
   switch( WeighingPredictionMode()){
      case PREDICTION_MODE_AUTOMATIC :
         _NextAutomatic();
         break;

      case PREDICTION_MODE_GROWTH_CURVE :
         _NextGrowthCurve();
         break;
   }
} // PredictionNew

//******************************************************************************

//------------------------------------------------------------------------------
//   Automatic
//------------------------------------------------------------------------------

static void _NextAutomatic( void)
// Automatic prediction
{
   _NextAutomaticSingle( SEX_MALE);
   if( !WeighingHasFemale()){
      return;
   }
   // female weight prediction :
   _NextAutomaticSingle( SEX_FEMALE);
} // _NextAutomatic

//------------------------------------------------------------------------------
//   Growth curve
//------------------------------------------------------------------------------

static void _NextGrowthCurve( void)
// Prediction by growth curve
{
   _NextGrowthCurveSingle( SEX_MALE);
   if( !WeighingHasFemale()){
      return;
   }
   _NextGrowthCurveSingle( SEX_FEMALE);
} // _NextGrowthCurve

//------------------------------------------------------------------------------
//   Automatic
//------------------------------------------------------------------------------

static void _NextAutomaticSingle( byte Sex)
// Automatic prediction
{
TArchiveItem Item;

   if( Sex != SEX_FEMALE){
      // (male) weight prediction :
      if( !StatisticsLast( &Item)){
         return;                       // don't modify last target
      }
      WeighingContext.TargetWeight = Item.Data.Average;
      // prediction with gain :
      if( WeighingPredictionGrowth() == PREDICTION_GROWTH_FAST){
         WeighingContext.TargetWeight += Item.Data.Gain;
      }
   } else {
      // female weight prediction :
      if( !StatisticsLastFemale( &Item)){
         return;                       // don't modify last target
      }
      WeighingContext.TargetWeightFemale = Item.Data.Average;
      // prediction with gain :
      if( WeighingPredictionGrowth() == PREDICTION_GROWTH_FAST){
         WeighingContext.TargetWeightFemale += Item.Data.Gain;
      }
   }
} // _NextAutomaticWithGainSingle

//------------------------------------------------------------------------------
//   Growth curve
//------------------------------------------------------------------------------

static void _NextGrowthCurveSingle( byte Sex)
// Prediction by growth curve
{
   if( Sex != SEX_FEMALE){
      // check for curve point :
      if( !CurveHasDay( SEX_MALE, WeighingDay())){
         _NextAutomaticSingle( SEX_MALE);        // end of growth curve
         return;
      }
      // set target by curve :
      WeighingContext.TargetWeight = CurveWeight( SEX_MALE, WeighingDay());
   } else {
      // check for curve point :
      if( !CurveHasDay( SEX_FEMALE, WeighingDay())){
         _NextAutomaticSingle( SEX_FEMALE);      // end of growth curve
         return;
      }
      // set target by curve :
      WeighingContext.TargetWeightFemale = CurveWeight( SEX_FEMALE, WeighingDay());
   }
} // _NextGrowthCurveSingle
