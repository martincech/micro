//******************************************************************************
//
//   Prediction.h   Weighing prediction
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Prediction_H__
   #define __Prediction_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void PredictionInit( void);
// Initialize

void PredictionResume( void);
// Reload prediction data

void PredictionStart( void);
// Start weighing prediction

void PredictionNew( void);
// New day prediction

#endif
