//******************************************************************************
//
//   WeighingConfigurationDef.h  Bat2 weighing configuration
//   Version 1.0                 (c) VEIT Electronics
//
//******************************************************************************

#ifndef __WeighingConfigurationDef_H__
   #ifndef _MANAGED
      #define __WeighingConfigurationDef_H__
   #endif


#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __Bat2Def_H__
   #include "Config/Bat2Def.h"
#endif

#ifndef __WeightDef_H__
   #include "Weight/WeightDef.h"
#endif

#ifndef __uClock_H__
   #include "Time/uClock.h"
#endif

#ifndef __PlatformDef_H__
   #include "Platform/PlatformDef.h"
#endif

#ifndef __GrowthCurveDef_H__
   #include "Curve/GrowthCurveDef.h"
#endif

#ifndef __CorrectionCurveDef_H__
   #include "Curve/CorrectionCurveDef.h"
#endif

#ifndef __StatisticDef_H__
   #include "Statistic/StatisticDef.h"
#endif

#ifndef __WeighingSchedulerDef_H__
   #include "Scheduler/WeighingSchedulerDef.h"
#endif

//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------
#ifndef _MANAGED
#define WEIGHING_CONFIGURATION_NAME_SIZE                UDIRECTORY_NAME_SIZE
#define WEIGHING_CONFIGURATION_NAME_DEFAULT             ""

#define WEIGHING_CONFIGURATION_FLOCK_SIZE               15
#define WEIGHING_CONFIGURATION_FLOCK_DEFAULT            ""

#define WEIGHING_CONFIGURATION_INITIAL_WEIGHT_DEFAULT        0
#define WEIGHING_CONFIGURATION_INITIAL_WEIGHT_FEMALE_DEFAULT 0

#define WEIGHING_CONFIGURATION_INITIAL_DAY_DEFAULT      1

#define WEIGHING_CONFIGURATION_CURVE_DEFAULT            ULIST_IDENTIFIER_INVALID
#define WEIGHING_CONFIGURATION_CORRECTION_CURVE_DEFAULT ULIST_IDENTIFIER_INVALID
#define WEIGHING_CONFIGURATION_PREDEFINED_DEFAULT       ULIST_IDENTIFIER_INVALID

#define WEIGHING_CONFIGURATION_DAY_START_DEFAULT        0

#define WEIGHING_CONFIGURATION_DAY_DURATION_MIN         0
#define WEIGHING_CONFIGURATION_DAY_DURATION_MAX        (24 * 3600 - 1)

//------------------------------------------------------------------------------

#define WEIGHING_CONTEXT_DAY_DEFAULT                    1
#define WEIGHING_CONTEXT_TARGET_DEFAULT                 0
#define WEIGHING_CONTEXT_CORRECTION_DEFAULT             100
#define WEIGHING_CONTEXT_START_AT_DEFAULT               0
#define WEIGHING_CONTEXT_DAY_ZERO_DEFAULT               0
#define WEIGHING_CONTEXT_DAY_CLOSE_AT_DEFAULT           0
#define WEIGHING_CONTEXT_DAY_DURATION_DEFAULT           0

//------------------------------------------------------------------------------

#define WEIGHING_ACCEPTANCE_MARGIN_ABOVE_MIN            0
#define WEIGHING_ACCEPTANCE_MARGIN_ABOVE_MAX            100
#define WEIGHING_ACCEPTANCE_MARGIN_ABOVE_DEFAULT        30

#define WEIGHING_ACCEPTANCE_MARGIN_BELOW_MIN            0
#define WEIGHING_ACCEPTANCE_MARGIN_BELOW_MAX            100
#define WEIGHING_ACCEPTANCE_MARGIN_BELOW_DEFAULT        30

//------------------------------------------------------------------------------

#define WEIGHING_DETECTION_FILTER_MIN                   1
#define WEIGHING_DETECTION_FILTER_MAX                   150
#define WEIGHING_DETECTION_FILTER_DEFAULT               10

#define WEIGHING_DETECTION_STABILIZATION_TIME_MIN       1
#define WEIGHING_DETECTION_STABILIZATION_TIME_MAX       100
#define WEIGHING_DETECTION_STABILIZATION_TIME_DEFAULT   5

#define WEIGHING_DETECTION_STABILIZATION_RANGE_MIN      0
#define WEIGHING_DETECTION_STABILIZATION_RANGE_MAX      100
#define WEIGHING_DETECTION_STABILIZATION_RANGE_DEFAULT  30
#endif
//------------------------------------------------------------------------------
//  Weighing status
//------------------------------------------------------------------------------


#ifdef _MANAGED
namespace Bat2Library{
   public enum class WeighingStatusE{
#else
typedef enum {
#endif
   WEIGHING_STATUS_UNDEFINED,          // undefined state
   WEIGHING_STATUS_STOPPED,            // weighing not active
   WEIGHING_STATUS_WAIT,               // wait for delayed weighing start
   WEIGHING_STATUS_SLEEP,              // weighing paused (inactive day/time)
   WEIGHING_STATUS_WEIGHING,           // active weighing
   WEIGHING_STATUS_SUSPENDED,          // active weighing suspended by user/scheduler
   WEIGHING_STATUS_RELEASED,           // weighing released by user

   WEIGHING_STATUS_CALIBRATION,        // platform calibration
   WEIGHING_STATUS_DIAGNOSTICS,        // platform diagnostics
#ifndef _MANAGED
    _WEIGHING_STATUS_LAST
} EWeighingStatus;
#else
   };
}
#endif

#ifndef _MANAGED
//------------------------------------------------------------------------------
//  Data types
//------------------------------------------------------------------------------

typedef UNamedListIdentifier TWeighingConfigurationIdentifier;

//------------------------------------------------------------------------------
//  Weighing acceptance
//------------------------------------------------------------------------------

typedef struct {
   byte MarginAbove;                             // Margin above target [%]
   byte MarginBelow;                             // Margin below target [%]
} TWeighingAcceptanceData;

typedef struct {
   TWeighingAcceptanceData Male;                 // Male data
   TWeighingAcceptanceData Female;               // Female data
} TWeighingAcceptance;

//------------------------------------------------------------------------------
//  Weighing detection
//------------------------------------------------------------------------------

typedef struct {
   byte Filter;                                  // Smooth filter [0.1s]
   byte StabilizationTime;                       // Stabilisation Time [0.1s]
   byte StabilizationRange;                      // Relative stabilization range [0.1%]
   byte Step;                                    // Step mode
} TWeighingDetection;

#endif

//------------------------------------------------------------------------------
//  Weighing Target Weights
//------------------------------------------------------------------------------

// target weight prediction mode :
#ifdef _MANAGED
namespace Bat2Library{
   public enum class PredictionModeE{
#else
typedef enum {
#endif
   PREDICTION_MODE_AUTOMATIC,                    // automatic prediction
   PREDICTION_MODE_GROWTH_CURVE,                 // prediction by growth curve
#ifndef _MANAGED
   _PREDICTION_MODE_LAST
} EPredictionMode;
#else
   };
}
#endif

// target weight prediction growth :
#ifdef _MANAGED
namespace Bat2Library{
   public enum class PredictionGrowthE{
#else
typedef enum {
#endif
   PREDICTION_GROWTH_SLOW,                      // prediction without daily gain
   PREDICTION_GROWTH_FAST,                      // prediction with daily gain
#ifndef _MANAGED
   _PREDICTION_GROWTH_LAST
} EPredictionGrowth;
#else
   };
}
#endif

// target weight sex differentiation :
#ifdef _MANAGED
namespace Bat2Library{
   public enum class SexDifferentiationE{
#else
typedef enum {
#endif
   SEX_DIFFERENTIATION_NO,                       // single sex
   SEX_DIFFERENTIATION_YES,                      // sex male/female
   SEX_DIFFERENTIATION_STEP_ONLY,                // disable acceptance - save all stable weights
#ifndef _MANAGED
   _SEX_DIFFERENTIATION_LAST
} ESexDifferentiation;
#else
   };
}
#endif

#ifdef _MANAGED
namespace Bat2Library{
   public enum class OnlineAdjustmentE{
#else
typedef enum {
#endif
   ADJUST_TARGET_WEIGHTS_NONE,                       // adjustment not allowed
   ADJUST_TARGET_WEIGHTS_SINGLE,                     // point by point(no curve recomputation
   ADJUST_TARGET_WEIGHTS_RECOMPUTE,                  // recompute curve after point adjustment
#ifndef _MANAGED
   _ADJUST_TARGET_WEIGHTS_AUTO_LAST = ADJUST_TARGET_WEIGHTS_RECOMPUTE,
   _ADJUST_TARGET_WEIGHTS_LAST
} EAdjustTargetWeights;
#else
   };
}
#endif

#ifndef _MANAGED
// target weights :
typedef struct {
   byte                       Mode;              // EPredictionMode - target weight prediction mode
   byte                       Growth;            // EPredictionGrowth - target weight prediction with gain
   byte                       SexDifferentiation;// ESexDifferentation - Sex differentiation
   byte                       Sex;               // Fixed sex
   byte                       AdjustTargetWeights;
} TWeighingTargetWeights;

//------------------------------------------------------------------------------
//  Weighing configuration
//------------------------------------------------------------------------------

// configuration data male/female :
typedef struct {
   TWeightGauge               InitialWeight;     // Initial weight
   TCurveIdentifier           GrowthCurve;       // Weighing curve index
} TWeighingConfigurationData;

typedef struct {
   char                             Name[ WEIGHING_CONFIGURATION_NAME_SIZE + 1];   // Weighing configuration name
   char                             Flock[ WEIGHING_CONFIGURATION_FLOCK_SIZE + 1]; // Flock name
   dword                            MenuMask;
   TDayNumber                       InitialDay;       // Initial day
   TCorrectionIdentifier            CorrectionCurve;  // Correction curve index
   TWeighingConfigurationIdentifier Predefined;       // Predefined weighing list identifier
   TWeighingConfigurationData       Male;             // Male data
   TWeighingConfigurationData       Female;           // Female data
   TWeighingTargetWeights           TargetWeights;    // Target weights data
   TWeighingDetection               Detection;        // Detection data
   TWeighingAcceptance              Acceptance;       // Acceptance data
   TStatisticParameters             Statistic;        // Statistic parameters
   UTimeGauge                       DayStart;         // Day start time (almost midnight)
   TYesNo                           Planning;         // Weighing planning enabled
   TWeighingPlanIndex               WeighingPlan;     // Weighing plan
} TWeighingConfiguration;

#endif

// Menu mask items :
// look for MenuWeighingMenuSelection.c strings !
#ifdef _MANAGED
namespace Bat2Library{
   using namespace System;
   [Flags]
   public enum class WeighingConfigurationMenuMaskE{
#else
typedef enum {
#endif
   WCMM_FLOCK              = 0x0001,
   WCMM_INITIAL_DAY        = 0x0002,
   WCMM_INITIAL_WEIGHT     = 0x0004,
   WCMM_GROWTH_CURVE       = 0x0008,
   WCMM_CORRECTION_CURVE   = 0x0010,
   WCMM_DETECTION          = 0x0020,
   WCMM_TARGET_WEIGHTS     = 0x0040,
   WCMM_ACCEPTANCE         = 0x0080,
   WCMM_STATISTICS         = 0x0100,
   WCMM_DAY_START          = 0x0200,
   WCMM_PLANNING           = 0x0400,
   WCMM_START_NOW          = 0x0800,
   WCMM_START_LATER        = 0x1000,
   WCMM_TYPE               = 0x2000,  // internal use, must be last
#ifndef _MANAGED
   _WCMM_LAST
} EWeighingConfigurationMenuMask;
#else
   };
}
#endif


//------------------------------------------------------------------------------
//  Weighing context
//------------------------------------------------------------------------------
#ifndef _MANAGED

typedef struct {
   byte                Status;                   // Actual status
   byte                LastStatus;               // Previous status
   byte                DayActive;                // Weighing day was/is active (YES/NO)
   byte                _Dummy1;
   word                Id;                       // Unique ID per weighing
   TDayNumber          Day;                      // Current day number
   TWeightGauge        TargetWeight;             // Target weight (male)
   TWeightGauge        TargetWeightFemale;       // Target weight female
   TCorrectionFactor   Correction;               // Weighing correction factor (male)
   word                _Dummy3;
   UClockGauge         StartAt;                  // Weighing started at this time
   UClockGauge         DayZero;                  // Zero day start time
   UClockGauge         DayCloseAt;               // Next day close time
   UTimeGauge          DayDuration;              // Day duration (24h or other for simulation)

   // copy of TWeighingConfiguration parameters from NVM - for delete prevention
   TGrowthCurve        GrowthCurveMale;
   TGrowthCurve        GrowthCurveFemale;
   TCorrectionCurve    CorrectionCurve;
   TWeighingPlan       WeighingPlan;
} TWeighingContext;

#endif
//------------------------------------------------------------------------------
#ifdef _MANAGED
   #undef _MANAGED
   #include "WeighingConfigurationDef.h"
   #define _MANAGED
   namespace Bat2Library{
      public ref class WeighingConfigurationC abstract sealed{
      public:
         literal int ACCEPTANCE_MARGIN_ABOVE_MIN = WEIGHING_ACCEPTANCE_MARGIN_ABOVE_MIN;
         literal int ACCEPTANCE_MARGIN_ABOVE_MAX = WEIGHING_ACCEPTANCE_MARGIN_ABOVE_MAX;
         literal int ACCEPTANCE_MARGIN_ABOVE_DEFAULT = WEIGHING_ACCEPTANCE_MARGIN_ABOVE_DEFAULT;

         literal int ACCEPTANCE_MARGIN_BELOW_MIN = WEIGHING_ACCEPTANCE_MARGIN_BELOW_MIN;
         literal int ACCEPTANCE_MARGIN_BELOW_MAX = WEIGHING_ACCEPTANCE_MARGIN_BELOW_MAX;
         literal int ACCEPTANCE_MARGIN_BELOW_DEFAULT = WEIGHING_ACCEPTANCE_MARGIN_BELOW_DEFAULT;

         literal int DETECTION_FILTER_MIN = WEIGHING_DETECTION_FILTER_MIN;
         literal int DETECTION_FILTER_MAX = WEIGHING_DETECTION_FILTER_MAX;
         literal int DETECTION_FILTER_DEFAULT = WEIGHING_DETECTION_FILTER_DEFAULT;

         literal int DETECTION_STABILIZATION_TIME_MIN = WEIGHING_DETECTION_STABILIZATION_TIME_MIN;
         literal int DETECTION_STABILIZATION_TIME_MAX = WEIGHING_DETECTION_STABILIZATION_TIME_MAX;
         literal int DETECTION_STABILIZATION_TIME_DEFAULT = WEIGHING_DETECTION_STABILIZATION_TIME_DEFAULT;

         literal int DETECTION_STABILIZATION_RANGE_MIN = WEIGHING_DETECTION_STABILIZATION_RANGE_MIN;
         literal int DETECTION_STABILIZATION_RANGE_MAX = WEIGHING_DETECTION_STABILIZATION_RANGE_MAX;
         literal int DETECTION_STABILIZATION_RANGE_DEFAULT = WEIGHING_DETECTION_STABILIZATION_RANGE_DEFAULT;

         literal int CONFIGURATION_NAME_SIZE = WEIGHING_CONFIGURATION_NAME_SIZE;
         literal int CONFIGURATION_FLOCK_SIZE = WEIGHING_CONFIGURATION_FLOCK_SIZE;
         literal int CONFIGURATION_INITIAL_DAY_MIN = DAY_NUMBER_MIN;
         literal int CONFIGURATION_INITIAL_DAY_MAX = DAY_NUMBER_MAX;

         static property double DetectionStabilizationTimeMin{double get(){ return (double)DETECTION_STABILIZATION_TIME_MIN / 10; }}
         static property double DetectionStabilizationTimeMax{double get(){ return (double)DETECTION_STABILIZATION_TIME_MAX / 10; }}
      };
   }
#endif
#endif
