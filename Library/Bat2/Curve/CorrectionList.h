//******************************************************************************
//
//   CorrectionList.h  Correction curve list
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#ifndef __CorrectionList_H__
   #define __CorrectionList_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __CorrectionCurveDef_H__
   #include "Curve/CorrectionCurveDef.h"
#endif   

#ifdef __WIN32__
   #ifndef __uStorageDef_H__
      #include "Data/uStorageDef.h"
   #endif
   DllImportExport extern const UDirectoryDescriptor CorrectionListDescriptor;
#endif

typedef UNamedList TCorrectionList;

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

#ifdef __cplusplus
   extern "C" {
#endif
TYesNo CorrectionListRemoteLoad( void);
// Load from remote device

TYesNo CorrectionListRemoteSave( void);
// Save to remote device

void CorrectionListInit( void);
// Initialize

TYesNo CorrectionListOpen( TCorrectionList *CorrectionList);
// open
void CorrectionListClose( TCorrectionList *CorrectionList);
// close

TCorrectionIdentifier CorrectionListIdentifier( TCorrectionList *CorrectionList, TCorrectionIndex Index);
// Returns identifier of curve at <Index>

TCorrectionIndex CorrectionListIndex( TCorrectionList *CorrectionList, TCorrectionIdentifier Identifier);
// Returns index of curve by curve <Identifier>

TYesNo CorrectionListName( TCorrectionList *CorrectionList, TCorrectionIdentifier Identifier, char *Name);
// Returns curve <Name>

void CorrectionListLoad( TCorrectionList *CorrectionList, TCorrectionCurve *Curve, TCorrectionIndex Index);
// Load <Curve> by <Index>

void CorrectionListSave( TCorrectionList *CorrectionList, TCorrectionCurve *Curve, TCorrectionIndex Index);
// Save <Curve> at <Index>

TYesNo CorrectionListIdentifierLoad( TCorrectionList *CorrectionList, TCorrectionCurve *Curve, TCorrectionIdentifier Identifier);
// Load <Curve> by <Identifier>

#ifdef __cplusplus
   }
#endif

#endif
