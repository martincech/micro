//******************************************************************************
//
//   CorrectionCurveDef.h Correction curve data
//   Version 1.0          (c) VEIT Electronics
//
//******************************************************************************

#ifndef __CorrectionCurveDef_H__
   #ifndef _MANAGED
   #define __CorrectionCurveDef_H__
   #endif

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __Bat2Def_H__
   #include "Config/Bat2Def.h"
#endif

#ifndef __uNamedListDef_H__
   #include "Data/uNamedListDef.h"
#endif

#ifndef _MANAGED
//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#define CORRECTION_CURVE_NAME_SIZE   UDIRECTORY_NAME_SIZE
#define CORRECTION_CURVE_POINT_COUNT 10

#define CORRECTION_FACTOR_MIN       -1000        // [0.1%]
#define CORRECTION_FACTOR_MAX        1000        // [0.1%]

//------------------------------------------------------------------------------
//  Data types
//------------------------------------------------------------------------------

typedef UDirectoryIndex      TCorrectionIndex;
typedef UDirectoryIdentifier TCorrectionIdentifier;
typedef int16                TCorrectionFactor;

//------------------------------------------------------------------------------
//  Correction point
//------------------------------------------------------------------------------

typedef struct {
   TDayNumber        Day;                                  // Technological day
   TCorrectionFactor Factor;                               // Multiplication factor [0.1%]
} TCorrectionPoint;

//------------------------------------------------------------------------------
//  Correction curve
//------------------------------------------------------------------------------

typedef struct {
   char             Name[ CORRECTION_CURVE_NAME_SIZE + 1]; // Curve name (must be first item !)
   word             Count;                                 // Curve points count
   word             _dummy;
   TCorrectionPoint Point[ CORRECTION_CURVE_POINT_COUNT];  // Correction curve points
} TCorrectionCurve;


//------------------------------------------------------------------------------
#endif // _MANAGED

#ifdef _MANAGED
#undef _MANAGED
#include "CorrectionCurveDef.h"
#define _MANAGED
namespace Bat2Library{
   public ref class CorrectionCurveC abstract sealed{
   public:
      literal TDayNumber POINT_COUNT_MAX = CORRECTION_CURVE_POINT_COUNT;
      literal byte CURVE_NAME_SIZE_MAX_LENGTH = CORRECTION_CURVE_NAME_SIZE;
      
      literal TCorrectionFactor CORRECTION_MIN = CORRECTION_FACTOR_MIN;
      literal TCorrectionFactor CORRECTION_MAX = CORRECTION_FACTOR_MAX;
   };
}
#endif
#endif
