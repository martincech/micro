//******************************************************************************
//
//   CorrectionCurve.c  Correction curve utility
//   Version 1.1        (c) VEIT Electronics
//
//******************************************************************************

/*
   June 6, 2013 - v 1.1 day duplication check added (CorrectionCurvePointDayExists)
*/

#include "CorrectionCurve.h"
#include <string.h>

// Local functions :

static void _PointDefault( TCorrectionCurve *Curve, TCorrectionPoint *Point);
// Set default <Point> data

//------------------------------------------------------------------------------
//   Default
//------------------------------------------------------------------------------

void CorrectionCurveDefault( TCorrectionCurve *Curve)
// Initialize
{
   memset( Curve, 0, sizeof( TCorrectionCurve));
   strcpy( Curve->Name, "CK000");
} // CorrectionCurveDefault

//------------------------------------------------------------------------------
//   Name
//------------------------------------------------------------------------------

char *CorrectionCurveName( TCorrectionCurve *Curve)
// Current correction curve name
{
   return( Curve->Name);
} // CorrectionCurveName

//------------------------------------------------------------------------------
//   Points count
//------------------------------------------------------------------------------

TDayNumber CorrectionCurveCount( TCorrectionCurve *Curve)
// Correction curve points count
{
   return( Curve->Count);
} // CorrectionCurveCount

//------------------------------------------------------------------------------
//   Point
//------------------------------------------------------------------------------

void CorrectionCurvePoint( TCorrectionCurve *Curve, int Index, TCorrectionPoint *Point)
// Returns curve point at <Index>
{
   _PointDefault( Curve, Point);
   if( Curve->Count == 0){
      return;                               // empty curve
   }
   if( Index >= Curve->Count){
      return;
   }
   *Point = Curve->Point[ Index];
} // CorrectionCurvePoint

//------------------------------------------------------------------------------
//   Point exists
//------------------------------------------------------------------------------

TYesNo CorrectionCurvePointDayExists( TCorrectionCurve *Curve, int Index, int Day)
// Checks if <Point>.day already exists, <Curve>.Point[Index] is not checked
{
int i;
   for(i = 0 ; i < Curve->Count ; i++) {
      if(i == Index) {
         continue;
      }

      if(Curve->Point[i].Day == Day) {
         return YES;
      }
   }

   return NO;
} // CorrectionCurvePointDayExists

//------------------------------------------------------------------------------
//   Add point
//------------------------------------------------------------------------------

int CorrectionCurveAppend( TCorrectionCurve *Curve)
// Append default point, returns index
{
int Index;

   Index = Curve->Count;
   if( Index == CORRECTION_CURVE_POINT_COUNT){
      return( CORRECTION_CURVE_POINT_COUNT - 1);      // points list full
   }
   Curve->Count++;
   _PointDefault( Curve, &Curve->Point[ Index]);
   return( Index);
} // CorrectionCurveAppend

//------------------------------------------------------------------------------
//   Delete point
//------------------------------------------------------------------------------

void CorrectionCurveDelete( TCorrectionCurve *Curve, int Index)
// Delete point at <Index>
{
int Size;

   if( Curve->Count == 0){
      return;                               // empty curve
   }
   if( Index >= Curve->Count){
      return;
   }
   Size = Curve->Count - (Index + 1);       // remaining items count
   Curve->Count--;
   if( !Size){
      return;
   }
   Size *= sizeof( TCorrectionPoint);
   memmove( &Curve->Point[ Index], &Curve->Point[ Index + 1], Size);
} // CorrectionCurveDelete

//------------------------------------------------------------------------------
//   Update
//------------------------------------------------------------------------------

TYesNo CorrectionCurveUpdate( TCorrectionCurve *Curve, int Index, TCorrectionPoint *Point)
// Update curve, with a new <Point> at <Index>
{
   if( Curve->Count == 0){
      return NO;                               // empty curve
   }
   if( Index >= Curve->Count){
      return NO;
   }
   if(CorrectionCurvePointDayExists(Curve, Index, Point->Day)) {
      return NO;
   }
   Curve->Point[ Index] = *Point;
   return YES;
} // CorrectionCurveUpdate

//------------------------------------------------------------------------------
//   Sort
//------------------------------------------------------------------------------

void CorrectionCurveSort( TCorrectionCurve *Curve, int Index)
// Sort curve, with a new data at <Index>
{
int           Size;
TCorrectionPoint   Point;
int           i;
int           NewIndex;

   if( Curve->Count == 0){
      return;                               // empty curve
   }
   if( Index >= Curve->Count){
      return;
   }
   Point = Curve->Point[ Index];            // save updated point
   CorrectionCurveDelete( Curve, Index);    // remove updated point
   NewIndex = Curve->Count;                 // append at end of list
   for( i = 0; i < Curve->Count; i++){
      if( Curve->Point[ i].Day > Point.Day){
         NewIndex = i;                      // insert before end of list
         break;
      }
   }
   Curve->Count++;                          // size with the new item
   // get space after requested position :
   Size  = Curve->Count - (NewIndex + 1);
   Size *= sizeof( TCorrectionPoint);
   if( Size){
      memmove( &Curve->Point[ NewIndex + 1], &Curve->Point[ NewIndex], Size);
   }
   // insert updated point :
   Curve->Point[ NewIndex] = Point;
} // CorrectionCurveSort

//******************************************************************************

//------------------------------------------------------------------------------
//   Default point
//------------------------------------------------------------------------------

static void _PointDefault( TCorrectionCurve *Curve, TCorrectionPoint *Point)
// Set default <Point> data
{
int i;
int DefaultDay = 0;
   Point->Day = 0;

   for(i = 0 ; i < Curve->Count ; i++) {
      if(Curve->Point[i].Day > DefaultDay) {
         DefaultDay = Curve->Point[i].Day;
      }
   }

   DefaultDay++;

   if(DefaultDay > DAY_NUMBER_MAX) {
      DefaultDay = 0;
      while(CorrectionCurvePointDayExists( Curve, Curve->Count, DefaultDay)) {
         DefaultDay++;
      }
   }

   Point->Day    = DefaultDay;
   Point->Factor = 0;
} // _PointDefault
