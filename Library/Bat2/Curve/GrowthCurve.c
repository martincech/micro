//******************************************************************************
//
//   GrowthCurve.c  Growth curve utility
//   Version 1.1    (c) VEIT Electronics
//
//******************************************************************************

/*
   June 6, 2013 - v 1.1 day duplication check added (CorrectionCurvePointDayExists)
*/

#include "GrowthCurve.h"
#include <string.h>

// Local functions :

static void _PointDefault( TGrowthCurve *Curve, TCurvePoint *Point);
// Set default <Point> data

//------------------------------------------------------------------------------
//   Default
//------------------------------------------------------------------------------

void GrowthCurveDefault( TGrowthCurve *Curve)
// Initialize
{
   memset( Curve, 0, sizeof( TGrowthCurve));
   strcpy( Curve->Name, "GC000");
} // GrowthCurveDefault

//------------------------------------------------------------------------------
//   Name
//------------------------------------------------------------------------------

char *GrowthCurveName( TGrowthCurve *Curve)
// Current growth curve name
{
   return( Curve->Name);
} // GrowthCurveName

//------------------------------------------------------------------------------
//   Points count
//------------------------------------------------------------------------------

TDayNumber GrowthCurveCount( TGrowthCurve *Curve)
// Growth curve points count
{
   return( Curve->Count);
} // GrowthCurveCount

//------------------------------------------------------------------------------
//   Point
//------------------------------------------------------------------------------

void GrowthCurvePoint( TGrowthCurve *Curve, int Index, TCurvePoint *Point)
// Returns curve point at <Index>
{
   _PointDefault( Curve, Point);
   if( Curve->Count == 0){
      return;                               // empty curve
   }
   if( Index >= Curve->Count){
      return;
   }
   *Point = Curve->Point[ Index];
} // GrowthCurvePoint

//------------------------------------------------------------------------------
//   Point exists
//------------------------------------------------------------------------------

TYesNo GrowthCurvePointDayExists( TGrowthCurve *Curve, int Index, int Day)
// Checks if <Point>.day already exists, <Curve>.Point[Index] is not checked
{
int i;
   for(i = 0 ; i < Curve->Count ; i++) {
      if(i == Index) {
         continue;
      }

      if(Curve->Point[i].Day == Day) {
         return YES;
      }
   }

   return NO;
} // GrowthCurvePointDayExists

//------------------------------------------------------------------------------
//   Add point
//------------------------------------------------------------------------------

int GrowthCurveAppend( TGrowthCurve *Curve)
// Append default point, returns index
{
int Index;

   Index = Curve->Count;
   if( Index == GROWTH_CURVE_POINT_COUNT){
      return( GROWTH_CURVE_POINT_COUNT - 1); // points list full
   }
   Curve->Count++;
   _PointDefault( Curve, &Curve->Point[ Index]);
   return( Index);
} // GrowthCurveAppend

//------------------------------------------------------------------------------
//   Delete point
//------------------------------------------------------------------------------

void GrowthCurveDelete( TGrowthCurve *Curve, int Index)
// Delete point at <Index>
{
int Size;

   if( Curve->Count == 0){
      return;                               // empty curve
   }
   if( Index >= Curve->Count){
      return;
   }
   Size = Curve->Count - (Index + 1);       // remaining items count
   Curve->Count--;
   if( !Size){
      return;
   }
   Size *= sizeof( TCurvePoint);
   memmove( &Curve->Point[ Index], &Curve->Point[ Index + 1], Size);
} // GrowthCurveDelete

//------------------------------------------------------------------------------
//   Update
//------------------------------------------------------------------------------

TYesNo GrowthCurveUpdate( TGrowthCurve *Curve, int Index, TCurvePoint *Point)
// Update curve, with a new <Point> at <Index>
{
   if( Curve->Count == 0){
      return NO;                               // empty curve
   }
   if( Index >= Curve->Count){
      return NO;
   }
   if(GrowthCurvePointDayExists(Curve, Index, Point->Day)) {
      return NO;
   }
   Curve->Point[ Index] = *Point;
   return YES;
} // GrowthCurveUpdate

//------------------------------------------------------------------------------
//   Sort
//------------------------------------------------------------------------------

void GrowthCurveSort( TGrowthCurve *Curve, int Index)
// Sort curve, with a new data at <Index>
{
int           Size;
TCurvePoint   Point;
int           i;
int           NewIndex;

   if( Curve->Count == 0){
      return;                               // empty curve
   }
   if( Index >= Curve->Count){
      return;
   }
   Point = Curve->Point[ Index];            // save updated point
   GrowthCurveDelete( Curve, Index);        // remove updated point
   NewIndex = Curve->Count;                 // append at end of list
   for( i = 0; i < Curve->Count; i++){
      if( Curve->Point[ i].Day > Point.Day){
         NewIndex = i;                      // insert before end of list
         break;
      }
   }
   Curve->Count++;                          // size with the new item
   // get space after requested position :
   Size  = Curve->Count - (NewIndex + 1);
   Size *= sizeof( TCurvePoint);
   if( Size){
      memmove( &Curve->Point[ NewIndex + 1], &Curve->Point[ NewIndex], Size);
   }
   // insert updated point :
   Curve->Point[ NewIndex] = Point;
} // GrowthCurveSort

//******************************************************************************

//------------------------------------------------------------------------------
//   Default point
//------------------------------------------------------------------------------

static void _PointDefault( TGrowthCurve *Curve, TCurvePoint *Point)
// Set default <Point> data
{
int i;
int DefaultDay = 0;
   Point->Day = 0;

   for(i = 0 ; i < Curve->Count ; i++) {
      if(Curve->Point[i].Day > DefaultDay) {
         DefaultDay = Curve->Point[i].Day;
      }
   }

   DefaultDay++;

   if(DefaultDay > DAY_NUMBER_MAX) {
      DefaultDay = 0;
      while(GrowthCurvePointDayExists( Curve, Curve->Count, DefaultDay)) {
         DefaultDay++;
      }
   }

   Point->Day    = DefaultDay;
   Point->Weight = 0;
} // _PointDefault
