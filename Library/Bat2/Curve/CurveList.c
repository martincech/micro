//******************************************************************************
//
//   CurveList.c  Growth curve list
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "CurveList.h"
#include "Data/uNamedList.h"
#include "Curve/GrowthCurve.h"
#include "Multitasking/Multitasking.h"
#include <string.h>

// data without name :
#define _GrowthCurveDataSize()       (sizeof( TGrowthCurve) - GROWTH_CURVE_NAME_SIZE - 1)
#define _GrowthCurveDataAddress( c)  ((char *)c + GROWTH_CURVE_NAME_SIZE + 1)

uNamedListAlloc( _CurveListDescriptor, GROWTH_CURVE_COUNT, _GrowthCurveDataSize());

//------------------------------------------------------------------------------
//  Remote load
//------------------------------------------------------------------------------

TYesNo CurveListRemoteLoad( void)
// Load from remote device
{
   if (!uNamedListRemCopy((UNamedListDescriptor*)&_CurveListDescriptor, FILE_GROWTH_CURVE_LOCAL, FILE_GROWTH_CURVE_REMOTE)){
      return NO;
   }
   return YES;

} // CurveListRemoteLoad

//------------------------------------------------------------------------------
//  Remote save
//------------------------------------------------------------------------------

TYesNo CurveListRemoteSave( void)
// Save to remote device
{
   return uNamedListRemCopy((UNamedListDescriptor*) &_CurveListDescriptor, FILE_GROWTH_CURVE_REMOTE, FILE_GROWTH_CURVE_LOCAL);
} // CurveListRemoteSave

//------------------------------------------------------------------------------
//   Initialize
//------------------------------------------------------------------------------

void CurveListInit( void)
// Initialize
{
   uNamedListInit( &_CurveListDescriptor, FILE_GROWTH_CURVE_LOCAL);
} // CurveListInit

TYesNo CurveListOpen( TCurveList *CurveList) {
   TYesNo Success;
   Success = uNamedListOpen( CurveList, &_CurveListDescriptor, FILE_GROWTH_CURVE_LOCAL);
   if(!Success) {
      MultitaskingReschedule();
   }
   return Success;
}

void CurveListClose( TCurveList *CurveList) {
   uNamedListClose( CurveList);
}

//------------------------------------------------------------------------------
//   Identifier
//------------------------------------------------------------------------------
/*
TCurveIdentifier CurveListIdentifier( TCurveList *CurveList, TCurveIndex Index)
// Returns identifier of curve at <Index>
{
   return( uDirectoryItemIdentifier( CurveList, Index));
} // CurveListIdentifier*/

//------------------------------------------------------------------------------
//   Index
//------------------------------------------------------------------------------

TCurveIndex CurveListIndex( TCurveList *CurveList, TCurveIdentifier Identifier)
// Returns index of curve by curve <Identifier>
{
   return( uDirectoryFindIdentifier( CurveList, Identifier));
} // CurveListIndex

//------------------------------------------------------------------------------
//   Name
//------------------------------------------------------------------------------

TYesNo CurveListName( TCurveList *CurveList, TCurveIdentifier Identifier, char *Name)
// Returns curve <Name>
{
TCurveIndex Index;

   Index = uDirectoryFindIdentifier( CurveList, Identifier);
   if( Index == UDIRECTORY_INDEX_INVALID){
      strcpy( Name, "?");
      return( NO);
   }
   uDirectoryItemName( CurveList, Index, Name);
   return( YES);
} // CurveListName

//------------------------------------------------------------------------------
//   Load
//------------------------------------------------------------------------------

void CurveListLoad( TCurveList *CurveList, TGrowthCurve *Curve, TCurveIndex Index)
// Load <Curve> by <Index>
{
TCurveIdentifier Identifier;

   Identifier = uDirectoryItemIdentifier( CurveList, Index);
   uNamedListLoad( CurveList, Identifier, _GrowthCurveDataAddress( Curve));
   uDirectoryItemName( CurveList, Index, Curve->Name);
} // CurveListLoad

//------------------------------------------------------------------------------
//   Save
//------------------------------------------------------------------------------

void CurveListSave( TCurveList *CurveList, TGrowthCurve *Curve, TCurveIndex Index)
// Save <Curve> at <Index>
{
TCurveIdentifier Identifier;

   Identifier = uDirectoryItemIdentifier( CurveList, Index);
   uDirectoryItemName( CurveList, Index, Curve->Name);
   uNamedListSave( CurveList, Identifier, _GrowthCurveDataAddress( Curve));
} // CurveListSave

//------------------------------------------------------------------------------
//   Load by identifier
//------------------------------------------------------------------------------

TYesNo CurveListIdentifierLoad( TCurveList *CurveList, TGrowthCurve *Curve, TCurveIdentifier Identifier)
// Load <Curve> by <Identifier>
{
TCurveIndex Index;

   // initialize with defaults :
   GrowthCurveDefault( Curve);
   if( Identifier == ULIST_IDENTIFIER_INVALID){
      return( NO);
   }
   // get directory index :
   Index = uDirectoryFindIdentifier( CurveList, Identifier);
   if( Index == UDIRECTORY_INDEX_INVALID){
      return( NO);
   }
   CurveListLoad( CurveList, Curve, Index);       // curve name saved separately at directory item
   return( YES);
} // CurveListIdentifierLoad
