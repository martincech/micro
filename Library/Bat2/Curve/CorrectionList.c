//******************************************************************************
//
//   CorrectionList.c  Correction curve list
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#include "CorrectionList.h"
#include "Data/uNamedList.h"
#include "Curve/CorrectionCurve.h"
#include "Multitasking/Multitasking.h"
#include <string.h>

static TCorrectionCurve _CorrectionCurve = {
   /* Name */     "BROILERS",
   /* Count */    5,
   /* _dummy */   0,
   /* Point */    {
      {1,  0},
      {2,  10},
      {3,  20},
      {4,  30},
      {5,  40},
   }
};


// data without name :
#define _CorrectionCurveDataSize()       (sizeof( TCorrectionCurve) - CORRECTION_CURVE_NAME_SIZE - 1)
#define _CorrectionCurveDataAddress( c)  ((char *)c + CORRECTION_CURVE_NAME_SIZE + 1)

uNamedListAlloc( CorrectionListDescriptor, CORRECTION_CURVE_COUNT, _CorrectionCurveDataSize());

//------------------------------------------------------------------------------
//  Remote load
//------------------------------------------------------------------------------

TYesNo CorrectionListRemoteLoad( void)
// Load from remote device
{
   if (!uNamedListRemCopy((UNamedListDescriptor*)&CorrectionListDescriptor, FILE_CORRECTION_CURVE_LOCAL, FILE_CORRECTION_CURVE_REMOTE)){
      return NO;
   }
   return YES;
} // CorrectionListRemoteLoad

//------------------------------------------------------------------------------
//  Remote save
//------------------------------------------------------------------------------

TYesNo CorrectionListRemoteSave( void)
// Save to remote device
{
   return uNamedListRemCopy((UNamedListDescriptor*)&CorrectionListDescriptor, FILE_CORRECTION_CURVE_REMOTE, FILE_CORRECTION_CURVE_LOCAL);
} // CorrectionListRemoteSave

//------------------------------------------------------------------------------
//   Initialize
//------------------------------------------------------------------------------

void CorrectionListInit( void)
// Initialize
{
   uNamedListInit( &CorrectionListDescriptor, FILE_CORRECTION_CURVE_LOCAL);
} // CorrectionListInit

TYesNo CorrectionListOpen( TCorrectionList *CorrectionList) {
   TYesNo Success;
   Success = uNamedListOpen( CorrectionList, &CorrectionListDescriptor, FILE_CORRECTION_CURVE_LOCAL);
   if(!Success) {
      MultitaskingReschedule();
   }
   return Success;
}

void CorrectionListClose( TCorrectionList *CorrectionList) {
   uNamedListClose( CorrectionList);
}

//------------------------------------------------------------------------------
//   Identifier
//------------------------------------------------------------------------------

TCorrectionIdentifier CorrectionListIdentifier( TCorrectionList *CorrectionList, TCorrectionIndex Index)
// Returns identifier of curve at <Index>
{
   return( uDirectoryItemIdentifier( CorrectionList, Index));
} // CorrectionListIdentifier

//------------------------------------------------------------------------------
//   Index
//------------------------------------------------------------------------------

TCorrectionIndex CorrectionListIndex( TCorrectionList *CorrectionList, TCorrectionIdentifier Identifier)
// Returns index of curve by curve <Identifier>
{
   return( uDirectoryFindIdentifier( CorrectionList, Identifier));
} // CorrectionListIndex

//------------------------------------------------------------------------------
//   Name
//------------------------------------------------------------------------------

TYesNo CorrectionListName( TCorrectionList *CorrectionList, TCorrectionIdentifier Identifier, char *Name)
// Returns curve <Name>
{
TCorrectionIndex Index;

   Index = uDirectoryFindIdentifier( CorrectionList, Identifier);
   if( Index == UDIRECTORY_INDEX_INVALID){
      strcpy( Name, "?");
      return( NO);
   }
   uDirectoryItemName( CorrectionList, Index, Name);
   return( YES);
} // CorrectionListName

//------------------------------------------------------------------------------
//   Load
//------------------------------------------------------------------------------

void CorrectionListLoad( TCorrectionList *CorrectionList, TCorrectionCurve *Curve, TCorrectionIndex Index)
// Load <Curve> by <Index>
{
TCorrectionIdentifier Identifier;

   Identifier = uDirectoryItemIdentifier( CorrectionList, Index);
   uNamedListLoad( CorrectionList, Identifier, _CorrectionCurveDataAddress( Curve));
   uDirectoryItemName( CorrectionList, Index, Curve->Name);
} // CorrectionListLoad

//------------------------------------------------------------------------------
//   Save
//------------------------------------------------------------------------------

void CorrectionListSave( TCorrectionList *CorrectionList, TCorrectionCurve *Curve, TCorrectionIndex Index)
// Save <Curve> at <Index>
{
TCorrectionIdentifier Identifier;

   Identifier = uDirectoryItemIdentifier( CorrectionList, Index);
   uDirectoryItemName( CorrectionList, Index, Curve->Name);
   uNamedListSave( CorrectionList, Identifier, _CorrectionCurveDataAddress( Curve));
} // CorrectionListSave

//------------------------------------------------------------------------------
//   Load by identifier
//------------------------------------------------------------------------------

TYesNo CorrectionListIdentifierLoad( TCorrectionList *CorrectionList, TCorrectionCurve *Curve, TCorrectionIdentifier Identifier)
// Load <Curve> by <Identifier>
{
TCorrectionIndex Index;

   // initialize with defaults :
   CorrectionCurveDefault( Curve);
   if( Identifier == ULIST_IDENTIFIER_INVALID){
      return( NO);
   }
   // get directory index :
   Index = uDirectoryFindIdentifier( CorrectionList, Identifier);
   if( Index == UDIRECTORY_INDEX_INVALID){
      return( NO);
   }
   CorrectionListLoad( CorrectionList, Curve, Index);       // curve name saved separately at directory item
   return( YES);
} // CorrectionListIdentifierLoad
