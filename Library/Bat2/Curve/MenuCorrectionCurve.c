//******************************************************************************
//
//   MenuCorrectionCurve.c Correction curve viewer
//   Version 1.1           (c) VEIT Electronics
//
//******************************************************************************

/*
   June 6, 2013 - v 1.1 day duplication check added
*/

#include "MenuCorrectionCurve.h"
#include "Graphic/Graphic.h"              // Graphic display
#include "Console/conio.h"                // Console
#include "Sound/Beep.h"                   // Sounds
#include "Gadget/DCursor.h"               // List Cursor
#include "Gadget/DList.h"                 // List drawings
#include "Gadget/DEvent.h"                // Event manager
#include "Gadget/DLayout.h"               // Display layout
#include "Gadget/DMsg.h"                  // Message box
#include "Gadget/DEdit.h"                 // Display edit value
#include "Gadget/DNamedList.h"            // Display named list operations
#include "Gadget/DDirectory.h"            // Display list directory
#include "Curve/CorrectionList.h"         // Correction curve list
#include "Curve/CorrectionCurve.h"        // Correction curve
#include "Str.h"                          // project directory strings
#include "Bitmap.h"                       // Bitmaps

// Constants :
#define DCURVE_DAY_X           (35)
#define DCURVE_LINE_X          (60)
#define DCURVE_FACTOR_X        (G_WIDTH / 2 + 50)

// Local functions :

static void EditItem( TListCursor *Cursor, TCorrectionCurve *Curve);
// Edit item

static void DeleteItem( TListCursor *Cursor, TCorrectionCurve *Curve);
// Delete item

static void DisplayPage( TListCursor *Cursor, TCorrectionCurve *Curve);
// Display curve page

//------------------------------------------------------------------------------
//  Correction curve
//------------------------------------------------------------------------------

void MenuCorrectionCurve( TCorrectionList *CorrectionList)
// Display correction curve
{
TCorrectionIndex  Index;
TCorrectionCurve  Curve;

   forever {
      CorrectionCurveDefault( &Curve);
      switch( DNamedListOperation( STR_CORRECTION_CURVES, CorrectionList, &Index, CorrectionCurveName( &Curve))){
         case DLIST_OPERATION_EDIT :
         case DLIST_OPERATION_COPY :
            CorrectionListLoad( CorrectionList, &Curve, Index);          // load curve
            MenuCorrectionCurveEdit( &Curve);            // edit curve
            CorrectionListSave( CorrectionList, &Curve, Index);          // save curve
            break;

         case DLIST_OPERATION_CREATE :
            MenuCorrectionCurveEdit( &Curve);            // edit default curve
            CorrectionListSave( CorrectionList, &Curve, Index);          // save curve
            break;

         case DLIST_OPERATION_RENAME :
            break;

         case DLIST_OPERATION_DELETE :
            break;

         case DLIST_OPERATION_UNDEFINED:
            break;

         default :
            return;
      }
   }
}  // MenuCorrectionCurve

//------------------------------------------------------------------------------
//  Select correction curve
//------------------------------------------------------------------------------

TYesNo MenuCorrectionCurveSelect( TCorrectionList *CorrectionList, TUniStr Title, TCorrectionIdentifier *Identifier, TUniStr SpecialItem)
// Select correction curve, add <SpecialItem> at end of list
{
TCorrectionIdentifier id;
TCorrectionIndex      Index;

   // find directory index :
   id    = *Identifier;
   Index = 0;
   if( id != ULIST_IDENTIFIER_INVALID){
      Index = uDirectoryFindIdentifier(( UDirectory *)uNamedListDirectory( CorrectionList), id);
      if( Index == UDIRECTORY_INDEX_INVALID){
         Index = 0;
      }
   }
   // select directory item :
   Index = DDirectorySelect( Title, uNamedListDirectory( CorrectionList), Index, SpecialItem);
   if( Index == UDIRECTORY_INDEX_INVALID){
      return( NO);                     // no selection
   }
   // check for special item :
   if( DDirectorySpecial( Index)){
      // set special item as invalid identifier
      *Identifier = ULIST_IDENTIFIER_INVALID;
      return( YES);
   }
   *Identifier = uDirectoryItemIdentifier(( UDirectory *) uNamedListDirectory( CorrectionList), Index);
   return( YES);
} // MenuCorrectionCurveSelect

//------------------------------------------------------------------------------
//  Edit correction curve
//------------------------------------------------------------------------------

void MenuCorrectionCurveEdit( TCorrectionCurve *Curve)
// Display/edit correction curve
{
TListCursor Cursor;

   DCursorInit( &Cursor, CorrectionCurveCount( Curve) + 1, DLIST_ROWS_COUNT_MAX);
   forever {
      if( DCursorPageChanged( &Cursor)){
         // redraw page
         GClear();                                    // clear display
         DLayoutTitle( CorrectionCurveName( Curve));  // display title
         DLayoutStatus( STR_BTN_CANCEL, STR_BTN_DELETE, &BmpButtonRight); // display status line
      }
      if( DCursorRowChanged( &Cursor)){
         DisplayPage( &Cursor, Curve);
         DCursorRowUpdate( &Cursor);
         GFlush();                               // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            DCursorRowUp( &Cursor);
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            DCursorRowDown( &Cursor);
            break;

         case K_RIGHT :
            DeleteItem( &Cursor, Curve);
            DCursorCountSet( &Cursor, CorrectionCurveCount( Curve) + 1);// update items count
            break;

         case K_ENTER :
            EditItem( &Cursor, Curve);
            DCursorCountSet( &Cursor, CorrectionCurveCount( Curve) + 1);// update items count
            break;

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return;
      }
   }
} // MenuCorrectionCurveEdit

//******************************************************************************

//------------------------------------------------------------------------------
//   Edit
//------------------------------------------------------------------------------

static void EditItem( TListCursor *Cursor, TCorrectionCurve *Curve)
// Edit item
{
TCorrectionPoint Point;
int              RowY;
int              Value;
dword            Index;

   Index = DCursorIndex( Cursor);                // get active row index
   // check for last row :
   if( Index == CorrectionCurveCount( Curve)){
      if( CorrectionCurveCount( Curve) == CORRECTION_CURVE_POINT_COUNT){
         BeepError();                            // maximum capacity reached
         return;
      }
      Index = CorrectionCurveAppend( Curve);     // append a new point
      DisplayPage( Cursor, Curve);
      GFlush();
   }
   CorrectionCurvePoint( Curve, Index, &Point);
   RowY  = DListY( DCursorRow( Cursor));
   // edit day :
   Value = Point.Day;
   if( !DEditNumber( DCURVE_DAY_X, RowY, &Value, 0, DAY_NUMBER_MIN, DAY_NUMBER_MAX, 0)){
      DisplayPage( Cursor, Curve);
      GFlush();
      return;
   }
   if(CorrectionCurvePointDayExists(Curve, Index, Value)) {
      BeepError();
      DCursorPageRedraw( Cursor);
      GFlush();
      return;
   }
   Point.Day = (TDayNumber)Value;
   CorrectionCurveUpdate( Curve, Index, &Point); // update point
   // redraw page :
   DisplayPage( Cursor, Curve);
   GFlush();
   // edit factor :
   Value = Point.Factor;
   if( DEditNumber( DCURVE_FACTOR_X, RowY, &Value, 1 | FMT_PLUS, CORRECTION_FACTOR_MIN, CORRECTION_FACTOR_MAX, "%")){
      Point.Factor = (TCorrectionFactor)Value;
   }
   // update point :
   CorrectionCurveUpdate( Curve, Index, &Point); // update point
   CorrectionCurveSort( Curve, Index);           // sort curve
   DCursorPageRedraw( Cursor);                   // redraw items
} // EditItem

//------------------------------------------------------------------------------
//  Delete item
//------------------------------------------------------------------------------

static void DeleteItem( TListCursor *Cursor, TCorrectionCurve *Curve)
// Delete item
{
char             Buffer[ 16];
TCorrectionPoint Point;
dword            Index;

   Index = DCursorIndex( Cursor);                // get active row index
   // check for last row :
   if( Index == CorrectionCurveCount( Curve)){
      BeepError();
      return;
   }
   CorrectionCurvePoint( Curve, Index, &Point);
   bprintf( Buffer, "Day %d", Point.Day);
   if( !DMsgYesNo( STR_CONFIRMATION, STR_REALLY_DELETE, Buffer)){
      DCursorPageRedraw( Cursor);                // redraw items
      return;
   }
   CorrectionCurveDelete( Curve, Index);
} // DeleteItem

//------------------------------------------------------------------------------
//  Display page
//------------------------------------------------------------------------------

static void DisplayPage( TListCursor *Cursor, TCorrectionCurve *Curve)
// Display curve page
{
int              RowY;
int              i;
TCorrectionPoint Point;
dword            Index;

   DListClear( Cursor);                // clear list area
   // draw points
   for( i = 0; i < DCursorRowCount( Cursor); i++){
      RowY = DListY( i);
      if( !DListCursor( Cursor, i)){
         continue;
      }
      // get item data :
      Index = DCursorPage( Cursor) + i;
      if( Index >= CorrectionCurveCount( Curve)){
         // empty/last row, check for cursor position :
         if( i == DCursorRow( Cursor)){
            GTextAt( DCURVE_DAY_X, RowY);
            cputs( STR_NEW_ITEM);
         }
         continue;                     // empty row
      }
      CorrectionCurvePoint( Curve, Index, &Point);
      // day number :
      GTextAt( DCURVE_DAY_X, RowY);
      DLabelFormat( DCURVE_DAY_X, RowY, "%d", Point.Day, 0);
      // factor :
      GTextAt( DCURVE_FACTOR_X, RowY);
      DLabelFormat( DCURVE_FACTOR_X, RowY, "%+5.1f %s", Point.Factor, "%");
   }
   GFlush();                           // redraw rows
   DListLine( DCURVE_LINE_X);          // vertical separator line
} // DisplayPage
