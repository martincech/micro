//******************************************************************************
//
//   DeviceList.c    Device list
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "DeviceList.h"
#include "Data/uList.h"

#define DEVICE_COUNT    255

uListAlloc( _DeviceList, DEVICE_LIST_ADDRESS, DEVICE_LIST_COUNT, sizeof( TDevice));

//------------------------------------------------------------------------------
//   Initialize
//------------------------------------------------------------------------------

void DeviceListInit( void)
// Initialize
{
   uListInit( &_DeviceList);
} // DeviceListInit

//------------------------------------------------------------------------------
//   Count
//------------------------------------------------------------------------------

byte DeviceListCount( void)
// Returns actual list items count
{
   return( uListCount( &_DeviceList));
} // DeviceListCount

//------------------------------------------------------------------------------
//   Capacity
//------------------------------------------------------------------------------

TDeviceIndex DeviceListCapacity( void)
// Returns maximum list items count
{
   return( uListCapacity( &_DeviceList));
} // DeviceListCapacity

//------------------------------------------------------------------------------
//   Add
//------------------------------------------------------------------------------

TDeviceIndex DeviceListAdd( TDevice *Device)
// Add <Device> returns index
{
   return( uListAppend( &_DeviceList, Device));
} // DeviceListAdd

//------------------------------------------------------------------------------
//   Delete
//------------------------------------------------------------------------------

void DeviceListDelete( TDeviceIndex Index)
// Delete by <Index>
{
   uListDelete(  &_DeviceList, Index);
} // DeviceListDelete

//------------------------------------------------------------------------------
//   Load
//------------------------------------------------------------------------------

void DeviceListLoad( TDevice *Device, TDeviceIndex Index)
// Load <Device> by <Index>
{
   uListItemLoad( &_DeviceList, Index, Device);
} // DeviceListLoad

//------------------------------------------------------------------------------
//   Save
//------------------------------------------------------------------------------

void DeviceListSave( TDevice *Device, TDeviceIndex Index)
// Save <Device> at <Index>
{
   uListItemSave( &_DeviceList, Index, Device);
} // DeviceListSave