//******************************************************************************
//
//   Device.h        Device
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Device_H__
   #define __Device_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __Bat2Def_H__
   #include "Config/Bat2Def.h"
#endif

#ifndef __VersionDef_H__
   #include "Device/VersionDef.h"
#endif

#ifndef __uListDef_H__
   #include "Data/uListDef.h"
#endif   


//------------------------------------------------------------------------------
//  Data types
//------------------------------------------------------------------------------

typedef UListIndex      TDeviceIndex;

//------------------------------------------------------------------------------
//  Device
//------------------------------------------------------------------------------

typedef struct {
   char           Name[ DEVICE_NAME_SIZE + 1];
   TDeviceVersion Version;
   byte           Address;
} TDevice;

#endif
