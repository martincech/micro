//******************************************************************************
//
//   Archive.h     Bat2 Archive services
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Archive_H__
   #define __Archive_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __ArchiveDef_H__
   #include "Archive/ArchiveDef.h"
#endif

#ifndef __StatistiDef_H__
   #include "Statistic/StatisticDef.h"
#endif

#ifndef __uFifo_H__
   #include "Data/uFifo.h"
#endif

typedef UFifo TArchive;

//------------------------------------------------------------------------------
//  Write
//------------------------------------------------------------------------------

#ifdef __cplusplus
   extern "C" {
#endif

TYesNo ArchiveRemoteLoad( void);
// Load from remote device

TYesNo ArchiveRemoteSave( void);
// Save to remote device

void ArchiveInit( void);
// Initialize

TYesNo ArchiveOpen( TArchive *Archive);
// Open archive

void ArchiveClose( TArchive *Archive);
// Close archive

void ArchiveClear( TArchive *Archive);
// Clear all archive

TYesNo ArchiveAppend( TArchive *Archive, TArchiveItem *Item);
// Append archive record

TYesNo ArchiveDayStart( TArchive *Archive, TArchiveItem *Marker);
// Start a new day with <Marker> data

//------------------------------------------------------------------------------
//  Read
//------------------------------------------------------------------------------

TArchiveIndex ArchiveCount( void);
// Returns archive records count


TYesNo ArchiveGet(  TArchive *Archive, TArchiveIndex Index, TArchiveItem *Item);
// Get archive record

TYesNo ArchiveIsMarker( TArchiveItem *Item);
// Returns YES if <Item> is marker

TYesNo ArchiveIsDaily( TArchiveData *Data);
// Return YES if <Data> is daily statistics

TArchiveIndex ArchiveSearch( TArchive *Archive, TDayNumber Day);
// Search for <Day> marker

//------------------------------------------------------------------------------
//  Convert
//------------------------------------------------------------------------------

void ArchiveStatistic( TStatistic *StatisticCalculate, TArchiveItem *Item);
// Convert <Item> to <Statistic>

void ArchiveHistogram( THistogram *Histogram, TArchiveItem *Item);
// Convert <Item> to <Histogram>

void ArchiveStatisticSet( TArchiveItem *Item, TStatistic *StatisticCalculate);
// Set <Item> by <Statistic>

void ArchiveHistogramSet( TArchiveItem *Item, THistogram *Histogram);
// Set <Item> by <Histogram>

//------------------------------------------------------------------------------
#ifdef __cplusplus
   }
#endif

#endif
