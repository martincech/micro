//******************************************************************************
//
//   Archive.c     Bat2 Archive services
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "Archive.h"
#include "System/System.h"
#include "Statistic/Statistic.h"
#include "Statistic/Histogram.h"
#include "Weighing/WeighingConfiguration.h"
#include "Multitasking/Multitasking.h"
#include <string.h>

#define ARCHIVE_STATISTIC_DEFAULT   0       // default statistic identifier
#define ARCHIVE_ZONE_DEFAULT        0       // default zone identifier

// archive FIFO :
uFifoAlloc( ArchiveFifoConfiguration, ARCHIVE_POINTER_ADDRESS, ARCHIVE_CAPACITY, ARCHIVE_ITEM_SIZE, NO, FILE_ARCHIVE_LOCAL, FILE_FIFO_LOCAL)

uFifoAlloc( ArchiveFifoConfigurationRemote, ARCHIVE_POINTER_ADDRESS, ARCHIVE_CAPACITY, ARCHIVE_ITEM_SIZE, NO, FILE_ARCHIVE_REMOTE, FILE_FIFO_REMOTE)


static UFifoDescriptor ArchiveFifoDescriptor;

// Local functions :
static void _HistogramCompress( TArchiveHistogram *Dst, THistogram *Src);
// Compress <Src> to <Dst> histogram

static void _HistogramExpand( THistogram *Dst, TArchiveHistogram *Src, TStatisticCount Count, THistogramValue Average);
// Expand <Src> to <Dst> histogram

//------------------------------------------------------------------------------
//  Remote load
//------------------------------------------------------------------------------

TYesNo ArchiveRemoteLoad( void)
// Load from remote device
{
   if (!uFifoCopy(&ArchiveFifoConfiguration, &ArchiveFifoConfigurationRemote)){
      return NO;
   }
   return YES;
} // ArchiveRemoteLoad

//------------------------------------------------------------------------------
//  Remote save
//------------------------------------------------------------------------------

TYesNo ArchiveRemoteSave( void)
// Save to remote device
{
   return uFifoCopy( &ArchiveFifoConfigurationRemote, &ArchiveFifoConfiguration);
} // ArchiveRemoteSave

//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

void ArchiveInit( void)
// Initialize
{
   uFifoInit( &ArchiveFifoDescriptor, &ArchiveFifoConfiguration);
} // ArchiveInit

//------------------------------------------------------------------------------
//  Open
//------------------------------------------------------------------------------

TYesNo ArchiveOpen( TArchive *Archive)
// Open archive
{
TYesNo Success;
   Success = uFifoOpen( Archive, &ArchiveFifoDescriptor);
   if(!Success) {
      MultitaskingReschedule();
   }
   return Success;
} // ArchiveOpen

//------------------------------------------------------------------------------
//  Close
//------------------------------------------------------------------------------

void ArchiveClose( TArchive *Archive)
// Close archive
{
   uFifoClose( Archive);
} // ArchiveClose

//------------------------------------------------------------------------------
//  Clear
//------------------------------------------------------------------------------

void ArchiveClear( TArchive *Archive)
// Clear all archive
{
   uFifoClear( Archive);
} // ArchiveClear

//------------------------------------------------------------------------------
//  Append
//------------------------------------------------------------------------------

TYesNo ArchiveAppend( TArchive *Archive, TArchiveItem *Item)
// Append archive record
{
   Item->Data.StatisticCalculate = ARCHIVE_STATISTIC_DEFAULT;
   Item->Data.Timestamp = SysClock();
   return( uFifoAppend( Archive, Item));
} // ArchiveAppend

//------------------------------------------------------------------------------
//  Start
//------------------------------------------------------------------------------

TYesNo ArchiveDayStart( TArchive *Archive, TArchiveItem *Marker)
// Start a new day with <Marker> data
{
   Marker->Marker.Zone      = _ArchiveMarkerSet( ARCHIVE_ZONE_DEFAULT);
   Marker->Marker.Timestamp = SysClock();
   return( uFifoAppend( Archive, Marker));
} // ArchiveDayStart

//------------------------------------------------------------------------------
//  Count
//------------------------------------------------------------------------------

TArchiveIndex ArchiveCount( void)
// Returns archive records count
{
   return( uFifoCount( &ArchiveFifoDescriptor));
} // ArchiveCount

//------------------------------------------------------------------------------
//  Get
//------------------------------------------------------------------------------

TYesNo ArchiveGet( TArchive *Archive, TArchiveIndex Index, TArchiveItem *Item)
// Get archive record
{
   return( uFifoGet( Archive, Index, Item));
} // ArchiveGet

//------------------------------------------------------------------------------
//  Is marker
//------------------------------------------------------------------------------

TYesNo ArchiveIsMarker( TArchiveItem *Item)
// Returns YES if <Item> is marker
{
   return( _ArchiveIsMarker( Item->Identifier));
} // ArchiveIsMarker

TYesNo ArchiveIsDaily( TArchiveData *Data)
// Return YES if <Data> is daily statistics
{
   return Data->HourFrom == 0 && Data->HourTo == 23;
}

//------------------------------------------------------------------------------
//  Search
//------------------------------------------------------------------------------

TArchiveIndex ArchiveSearch( TArchive *Archive, TDayNumber Day)
// Search for <Day> marker
{
TArchiveIndex i, Count;
TArchiveItem  Item;

   Count = ArchiveCount();
   for( i = 0; i < Count; i++){
      if( !uFifoGet( Archive, i, &Item)){
         break;
      }
      if( !ArchiveIsMarker( &Item)){
         continue;
      }
      if( Item.Marker.Day == Day){
         return( i);
      }
   }
   return( ARCHIVE_INDEX_INVALID);
} // ArchiveSearch

//------------------------------------------------------------------------------
//  Get statistic
//------------------------------------------------------------------------------

void ArchiveStatistic( TStatistic *Statistic, TArchiveItem *Item)
// Convert <Item> to <Statistic>
{
   Statistic->Target      = 0;         //!!!  dummy value
   Statistic->Sex         = Item->Data.Sex;
   Statistic->Count       = Item->Data.Count;
   Statistic->Average     = Item->Data.Average;
   Statistic->LastAverage = Item->Data.Average - Item->Data.Gain;
   Statistic->Gain        = Item->Data.Gain;
   Statistic->Sigma       = Item->Data.Sigma;
   Statistic->Cv          = (word)StatisticVariation( Item->Data.Average, Item->Data.Sigma);
   Statistic->Uniformity  = Item->Data.Uniformity;
} // ArchiveStatistic

//------------------------------------------------------------------------------
//  Get histogram
//------------------------------------------------------------------------------

void ArchiveHistogram( THistogram *Histogram, TArchiveItem *Item)
// Convert <Item> to <Histogram>
{
   _HistogramExpand( Histogram, &Item->Data.Histogram, Item->Data.Count, Item->Data.Average);
} // ArchiveHistogram

//------------------------------------------------------------------------------
//  Set statistic
//------------------------------------------------------------------------------

void ArchiveStatisticSet( TArchiveItem *Item, TStatistic *Statistic)
// Set <Item> by <Statistic>
{
   Item->Data.Sex         = (byte)Statistic->Sex;
   Item->Data.Count       = Statistic->Count;
   Item->Data.Average     = Statistic->Average;
   Item->Data.Gain        = Statistic->Gain;
   Item->Data.Sigma       = Statistic->Sigma;
   Item->Data.Uniformity  = Statistic->Uniformity;
} // ArchiveStatisticSet

//------------------------------------------------------------------------------
//  Set Histogram
//------------------------------------------------------------------------------

void ArchiveHistogramSet( TArchiveItem *Item, THistogram *Histogram)
// Set <Item> by <Histogram>
{
   _HistogramCompress( &Item->Data.Histogram, Histogram);
} // ArchiveHistogramSet


//******************************************************************************

//------------------------------------------------------------------------------
//  Histogram compress
//------------------------------------------------------------------------------

#define ARCHIVE_SLOT_MAX  255

static void _HistogramCompress( TArchiveHistogram *Dst, THistogram *Src)
// Compress <Src> to <Dst> histogram
{
THistogramCount MaxColumn, Slot;
dword           DstSlot;
int             i;

   memset( Dst, 0, sizeof( TArchiveHistogram));
   // check for maximum column :
   MaxColumn = 0;
   for( i = 0; i < HISTOGRAM_SLOTS; i++){
      Slot = Src->Slot[ i];
      if( Slot > MaxColumn){
         MaxColumn = Slot;
      }
   }
   if( MaxColumn == 0){
      return;                          // empty histogram
   }
   // normalize with maximum :
   for( i = 0; i < HISTOGRAM_SLOTS; i++){
      Slot     = Src->Slot[ i];
      DstSlot  = (dword)Slot * ARCHIVE_SLOT_MAX;
      DstSlot /= MaxColumn;
      Dst->Slot[ i] = (byte)DstSlot;
   }
} // _HistogramCompress

//------------------------------------------------------------------------------
//  Histogram expand
//------------------------------------------------------------------------------

static void _HistogramExpand( THistogram *Dst, TArchiveHistogram *Src, TStatisticCount Count, THistogramValue Average)
// Expand <Src> to <Dst> histogram
{
int   i;
dword Area;
dword DstSlot;

   // get histogram area :
   Area = 0;
   for( i = 0; i < HISTOGRAM_SLOTS; i++){
      Area += Src->Slot[ i];
   }
   HistogramCenterSet( Dst, Average);
   // update histogram step :
   if( WeighingConfiguration.Statistic.Histogram.Mode == HISTOGRAM_MODE_RANGE) {
      HistogramRangeSet( Dst, WeighingConfiguration.Statistic.Histogram.Range);  // by range
   } else {
      HistogramStepSet( Dst,  WeighingConfiguration.Statistic.Histogram.Step);   // by step
   }
   // check for empty histogram :
   if( Area == 0){
      for( i = 0; i < HISTOGRAM_SLOTS; i++){
         Dst->Slot[ i] = 0;
      }
      return;
   }
   // denormalize by area :
   for( i = 0; i < HISTOGRAM_SLOTS; i++){
      DstSlot  = Src->Slot[ i] * Count;
      DstSlot /= Area;
      Dst->Slot[ i] = (THistogramCount)DstSlot;
   }
} // _HistogramExpand
