//******************************************************************************
//
//   MenuArchive.c  Bat2 archive viewer
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuArchive.h"
#include "Graphic/Graphic.h"              // Graphic display
#include "Console/conio.h"                // Console
#include "Sound/Beep.h"                   // Sounds
#include "Gadget/DCursor.h"               // List Cursor
#include "Gadget/DList.h"                 // List drawings
#include "Gadget/DEvent.h"                // Event manager
#include "Gadget/DLayout.h"               // Display layout
#include "Gadget/DTime.h"                 // Display time
#include "Gadget/DLabel.h"
#include "Weight/DWeight.h"               // Display weight
#include "Statistic/DHistogram.h"         // Display histogram
#include "Statistic/DStatistic.h"         // Display statistic
#include "Weight/SexDef.h"                // Display sex
#include "Archive/Archive.h"              // Archive items
#include "Weighing/WeighingConfiguration.h" // Has female only
#include "Platform/Zone.h"
#include "Str.h"                          // project directory strings
#include "Fonts.h"                        // Project fonts
#include "Bitmap.h"                       // Bitmaps
#include <string.h>

#define ROWS_COUNT             4
// Page buffer :
typedef struct {
   TStatistic   StatisticCalculate;
   THistogram   Histogram;
} TArchiveRowGender;

typedef struct {
   word         Day;
   byte         HourFrom;
   byte         HourTo;
   TArchiveRowGender Male;
   byte         HasMales;
   TArchiveRowGender Female;
   byte         HasFemales;
   byte         HasDetails;
} TArchiveRow;

static TArchiveRow _PageArchive[ ROWS_COUNT];

static void ReadPage(TListCursor *Cursor, TDayNumber Day);
// Read page from database

static void DisplayPage(TListCursor *Cursor, TDayNumber Day);
// Display database page

static int ArchiveItemsCount(TDayNumber Day);
// return count of daily statistics for single day or count of days when day == DAY_NUMBER_INVALID

static void DisplayStatistics( TArchiveRow *Row);
// Display statistics & Histogram

//------------------------------------------------------------------------------
//  Display
//------------------------------------------------------------------------------

void MenuArchive( void)
// Display archive
{
TListCursor Cursor;
TListCursor DayBackupCursor;
TDayNumber DayDetails;

   DayDetails = DAY_NUMBER_INVALID;
   DCursorInit( &Cursor, ArchiveItemsCount(DayDetails), ROWS_COUNT);
   forever {
      if( DCursorPageChanged( &Cursor)){
         // redraw page
         ReadPage( &Cursor, DayDetails);
         GClear();                                 // clear display
         DLayoutTitle( STR_ARCHIVE);               // display title
      }
      if( DCursorRowChanged( &Cursor)){
         DisplayPage( &Cursor, DayDetails);
         DCursorRowUpdate( &Cursor);
         if(DCursorRowCount( &Cursor) == 0){
            DLayoutStatus(STR_BTN_CANCEL, 0,0);
         }else {
            if( DayDetails  == DAY_NUMBER_INVALID){
               // daily stats
               if(ArchiveItemsCount(_PageArchive[ DCursorRow( &Cursor)].Day) > 1){
                  DLayoutStatus( STR_BTN_CANCEL, STR_DETAILS, 0);     // display status line
               }else {
                  // daily without details
                  DLayoutStatus( STR_BTN_CANCEL, STR_STATISTICS, 0);     // display status line
               }
            } else {
               // non-daily
               DLayoutStatus( STR_BTN_CANCEL, STR_STATISTICS, 0);     // display status line
            }
         }
         GFlush();                               // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            DCursorCountSet( &Cursor, ArchiveItemsCount(DayDetails));    // dynamically update items count
            DCursorRowUp( &Cursor);
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            DCursorCountSet( &Cursor, ArchiveItemsCount(DayDetails));
            DCursorRowDown( &Cursor);
            break;

         case K_LEFT :
         case K_LEFT | K_REPEAT :
            DCursorCountSet( &Cursor, ArchiveItemsCount(DayDetails));
            DCursorPageUp( &Cursor);
            break;

         case K_RIGHT :
         case K_RIGHT | K_REPEAT :
            DCursorCountSet( &Cursor, ArchiveItemsCount(DayDetails));
            DCursorPageDown( &Cursor);
            break;

         case K_ENTER :
            if(DCursorRowCount( &Cursor) != 0){
               if (DayDetails  == DAY_NUMBER_INVALID && _PageArchive[ DCursorRow( &Cursor)].HasDetails){
                     // go to hourly statistics
                     DayDetails = _PageArchive[ DCursorRow( &Cursor)].Day;
                     DayBackupCursor = Cursor;
                     DCursorInit( &Cursor, ArchiveItemsCount(DayDetails), ROWS_COUNT);
               } else {
                  DisplayStatistics( &_PageArchive[ DCursorRow( &Cursor)]);
               }
               DCursorPageRedraw( &Cursor);         // force page redraw;
            }
            break;

         case K_ESC :
            if (DayDetails  != DAY_NUMBER_INVALID){
               DayDetails = DAY_NUMBER_INVALID;
               Cursor = DayBackupCursor;
               DCursorCountSet( &Cursor, ArchiveItemsCount(DayDetails));
               DCursorPageRedraw( &Cursor);         // force page redraw;
//               DCursorInit( &Cursor, ArchiveItemsCount(DayDetails), ROWS_COUNT);
               continue;
            }
            BeepKey();
         case K_TIMEOUT :
            return;
      }
   }
} // MenuArchive

//------------------------------------------------------------------------------
//  Read page
//------------------------------------------------------------------------------

static void ReadPage( TListCursor *Cursor, TDayNumber Day)
// Read page from database
{
int          i;
TArchiveIndex Index;
TArchiveIndex DailyMale;
TArchiveIndex DailyFemale;
int           PageIndex;
TArchiveItem  Item;
TArchive Archive;
TStatistic *Stats;
THistogram *Histogram;
TYesNo hasMoreItems;
TWeightGauge TargetMale;
TWeightGauge TargetFemale;

   while(!ArchiveOpen( &Archive));
   PageIndex = DCursorPage( Cursor);                           // first page item index
   i = 0;
   if(Day == DAY_NUMBER_INVALID){
      // Find marker for PageIndex day
      Index = -1;
      do{
         Index++;
         if(!ArchiveGet( &Archive, Index, &Item)){
            Index = ARCHIVE_INDEX_INVALID;
            break;
         }
         if(ArchiveIsMarker( &Item)){
            i++;
         }
      }while(i <= PageIndex);
      i = 0;
      if(Index != ARCHIVE_INDEX_INVALID || !ArchiveIsMarker( &Item)){
         hasMoreItems = YES;
         do{
            // data from marker
            _PageArchive[ i].Day             = Item.Marker.Day;
            TargetMale     = Item.Marker.TargetWeight;
            TargetFemale   = Item.Marker.TargetWeightFemale;
            _PageArchive[ i].HasDetails      = NO;
            _PageArchive[ i].HasMales = _PageArchive[ i].HasFemales = NO;
            memset(&_PageArchive[ i].Male, 0, sizeof(TArchiveRowGender));
            memset(&_PageArchive[ i].Female, 0, sizeof(TArchiveRowGender));

            do{
               // cycle until next marker and read data
               Index++;
               hasMoreItems = ArchiveGet( &Archive, Index, &Item);
               if(!hasMoreItems || ArchiveIsMarker( &Item)){
                  if(_PageArchive[ i].HasFemales && !_PageArchive[ i].HasMales &&
                        TargetFemale == 0){
                     // case when no sex diferentiation set, here target is in male part only
                     _PageArchive[ i].Female.StatisticCalculate.Target = TargetMale;
                  }
                  i++;
                  break;
               }
               if(ArchiveIsDaily(&Item.Data)){
                  if(Item.Data.Sex != SEX_FEMALE){
                     Stats = &_PageArchive[ i].Male.StatisticCalculate;
                     Histogram = &_PageArchive[ i].Male.Histogram;
                     _PageArchive[ i].HasMales = YES;
                  } else {
                     Stats = &_PageArchive[ i].Female.StatisticCalculate;
                     Histogram = &_PageArchive[ i].Female.Histogram;
                     _PageArchive[ i].HasFemales = YES;
                  }
                  // save data record :
                  ArchiveStatistic( Stats, &Item);
                  Stats->Target = Item.Data.Sex != SEX_FEMALE ? TargetMale : TargetFemale;
                  ArchiveHistogram( Histogram, &Item);
               } else {
                  _PageArchive[ i].HasDetails = YES;
               }
            }while(1);
         }while(hasMoreItems && (i < Cursor->PageRows));
      }

      if(WeighingStatus() != WEIGHING_STATUS_STOPPED && (i == 0 || _PageArchive[ i-1].Day == WeighingDay())){
         if(i != 0){i--;}
         // replace last item with current statistic
         _PageArchive[ i].Day             = WeighingDay();
         memcpy(&_PageArchive[ i].Male.StatisticCalculate, ZoneStatistic(), sizeof(TStatistic));
         _PageArchive[ i].Male.StatisticCalculate.Target = WeighingTarget();
         memcpy(&_PageArchive[ i].Male.Histogram, ZoneHistogram(), sizeof(THistogram));
         _PageArchive[ i].HasMales = YES;
         if( WeighingHasFemale()){
            memcpy(&_PageArchive[ i].Female.StatisticCalculate, ZoneStatisticFemale(), sizeof(TStatistic));
            _PageArchive[ i].Female.StatisticCalculate.Target = WeighingTargetFemale();
            memcpy(&_PageArchive[ i].Female.Histogram, ZoneHistogramFemale(), sizeof(THistogram));
            _PageArchive[ i].HasFemales = YES;
         } else {
            memset(&_PageArchive[ i].Female, 0, sizeof(TArchiveRowGender));
         }
         i++;
      }
   } else {
      Index = ArchiveSearch(&Archive, Day);
      if(Index == ARCHIVE_INDEX_INVALID){
         i=0;
      } else {
         ArchiveGet(&Archive, Index, &Item);
         // data from marker
         memset(&_PageArchive[ i].Male, 0, sizeof(TArchiveRowGender));
         memset(&_PageArchive[ i].Female, 0, sizeof(TArchiveRowGender));
         _PageArchive[ i].Day             = Item.Marker.Day;
         TargetMale     = Item.Marker.TargetWeight;
         TargetFemale   = Item.Marker.TargetWeightFemale;
         _PageArchive[ i].HasDetails      = NO;
         _PageArchive[ i].HasMales = _PageArchive[ i].HasFemales = NO;
         _PageArchive[ i].HourFrom = _PageArchive[ i].HourTo = -1;
         DailyMale = DailyFemale = ARCHIVE_INDEX_INVALID;
         Index++;
         while(ArchiveGet(&Archive, Index, &Item) && !ArchiveIsMarker(&Item)){
            Index++;
            if(ArchiveIsDaily(&Item.Data)){
               if(Item.Data.Sex != SEX_FEMALE){
                  DailyMale = Index - 1;
               }else{
                  DailyFemale = Index - 1;
               }
               continue;
            }
            if(i != 0){
               if(_PageArchive[ i - 1].HourFrom == Item.Data.HourFrom && _PageArchive[ i - 1].HourTo == Item.Data.HourTo){
                  // other sex, save it to previous row
                  i--;
               }else{
                  // different stats, copy details if place
                  if( --PageIndex >= 0){
                     i = 0;
                  }
                  if(i == Cursor->PageRows ){
                     break;
                  }
                  // new item, copy details
                  memset(&_PageArchive[ i].Male, 0, sizeof(TArchiveRowGender));
                  memset(&_PageArchive[ i].Female, 0, sizeof(TArchiveRowGender));
                  _PageArchive[ i].Day             = _PageArchive[ i - 1].Day;
                  _PageArchive[ i].HasDetails      = NO;
                  _PageArchive[ i].HourFrom = _PageArchive[ i].HourTo = -1;
                  _PageArchive[ i].HasMales = _PageArchive[ i].HasFemales = NO;
               }
            }

            if(Item.Data.Sex != SEX_FEMALE){
               Stats = &_PageArchive[ i].Male.StatisticCalculate;
               Histogram = &_PageArchive[ i].Male.Histogram;
               _PageArchive[ i].HasMales = YES;
            } else {
               Stats = &_PageArchive[ i].Female.StatisticCalculate;
               Histogram = &_PageArchive[ i].Female.Histogram;
               _PageArchive[ i].HasFemales = YES;
            }
            // save data record :
            ArchiveStatistic( Stats, &Item);
            Stats->Target = Item.Data.Sex != SEX_FEMALE ? TargetMale : TargetFemale;
            ArchiveHistogram( Histogram, &Item);
            // save hours
            _PageArchive[ i].HourFrom = Item.Data.HourFrom;
            _PageArchive[ i].HourTo = Item.Data.HourTo;
            if(_PageArchive[ i].HasFemales && !_PageArchive[ i].HasMales &&
                  TargetFemale == 0){
               // case when no sex diferentiation set, here target is in male part only
               _PageArchive[ i].Female.StatisticCalculate.Target = TargetMale;
            }
            i++;
         };

         if( --PageIndex >= 0){
            i = 0;
         }
         if(i < Cursor->PageRows){
            // add total items
            _PageArchive[ i].Day             = Day;
            _PageArchive[ i].HasDetails      = NO;
            _PageArchive[ i].HasMales = _PageArchive[ i].HasFemales = NO;

            if(WeighingStatus() != WEIGHING_STATUS_STOPPED &&
                  Day == WeighingDay()){
               // todays active weighing total
               memcpy(&_PageArchive[ i].Male.StatisticCalculate, ZoneStatistic(), sizeof(TStatistic));
               _PageArchive[ i].Male.StatisticCalculate.Target = WeighingTarget();
               memcpy(&_PageArchive[ i].Male.Histogram, ZoneHistogram(), sizeof(THistogram));
               _PageArchive[ i].HasMales = YES;
               if( WeighingHasFemale()){
                  memcpy(&_PageArchive[ i].Female.StatisticCalculate, ZoneStatisticFemale(), sizeof(TStatistic));
                  _PageArchive[ i].Female.StatisticCalculate.Target = WeighingTargetFemale();
                  memcpy(&_PageArchive[ i].Female.Histogram, ZoneHistogramFemale(), sizeof(THistogram));
                  _PageArchive[ i].HasFemales = YES;
               } else {
                  memset(&_PageArchive[ i].Female, 0, sizeof(TArchiveRowGender));
               }
               _PageArchive[ i].HourFrom        = 0;
               _PageArchive[ i].HourTo          = 23;
            }else{
               // archived totals
               if(DailyMale != ARCHIVE_INDEX_INVALID){
                  ArchiveGet(&Archive, DailyMale, &Item);
                  ArchiveStatistic( &_PageArchive[ i].Male.StatisticCalculate, &Item);
                  _PageArchive[ i].Male.StatisticCalculate.Target = TargetMale;
                  ArchiveHistogram( &_PageArchive[ i].Male.Histogram, &Item);
                  _PageArchive[ i].HasMales = YES;
               }
               if(DailyFemale != ARCHIVE_INDEX_INVALID){
                  ArchiveGet(&Archive, DailyFemale, &Item);
                  ArchiveStatistic( &_PageArchive[ i].Female.StatisticCalculate, &Item);
                  _PageArchive[ i].Female.StatisticCalculate.Target = TargetFemale;
                  ArchiveHistogram( &_PageArchive[ i].Female.Histogram, &Item);
                  _PageArchive[ i].HasFemales = YES;
               }
               _PageArchive[ i].HourFrom        = Item.Data.HourFrom;
               _PageArchive[ i].HourTo          = Item.Data.HourTo;
            }
            i++;
         }
      }
   }
   ArchiveClose( &Archive);

   DCursorRowCountSet( Cursor, i);                   // set page items count
} // ReadPage

//------------------------------------------------------------------------------
//  Display page
//------------------------------------------------------------------------------

#define SPACE_X        2
#define SPACE_Y        2
#define DAY_NUMBER_X   SPACE_X
#define LINE_DAY_X     30
#define HISTOGRAM_X    LINE_DAY_X
#define COUNT_X        70
#define HISTOGRAM_WIDTH COUNT_X - HISTOGRAM_X - SPACE_X
#define AVERAGE_X      110
#define UNIFORMITY_X   160
#define FLAG_SEX_X    (G_WIDTH - 15)
#define HEADER_Y       DLAYOUT_TITLE_H + SPACE_Y
#define CENTER_ROW_TEXT_SHIFT DListItemHeight()/4

static void DisplayPage( TListCursor *Cursor, TDayNumber Day)
// Display database page
{
int  RowY;
int  i;
UTime time;
TStatistic *Stats;
THistogram *Histogram;
const TBitmap* bmpArray[] = {
         &BmpStatisticsCount,
         &BmpStatisticsLastAverage,
         &BmpStatisticsUni};

   RowY = bmpArray[0]->Height;
   for(i = 1; i < sizeof(bmpArray)/sizeof(TBitmap*); i++){
      if(bmpArray[i]->Height > RowY){
         RowY = bmpArray[i]->Height;
      }
   }
   DListClearWithHeader(Cursor, RowY);
   // draw column icons
   GBitmap( COUNT_X,  HEADER_Y + (RowY - BmpStatisticsCount.Height)/2, &BmpStatisticsCount);
   GBitmap( AVERAGE_X,  HEADER_Y + (RowY - BmpStatisticsLastAverage.Height)/2, &BmpStatisticsLastAverage);
   GBitmap( UNIFORMITY_X, HEADER_Y + (RowY - BmpStatisticsUni.Height)/2, &BmpStatisticsUni);
   RowY++;
   // horizontal header separator
   DListHorizontalLine(RowY);
   // vertical separator lines :
   DListLine( LINE_DAY_X);

   if(DCursorRowCount( Cursor) == 0){
      DLabelCenter(STR_NO_ITEMS_FOUND, 0, RowY+(G_HEIGHT - DLAYOUT_STATUS_H - DLAYOUT_TITLE_H)/2, G_WIDTH, 0);
   } else {
      if(Day != DAY_NUMBER_INVALID){
         GSetFont(TAHOMA16);
         GTextAt( DAY_NUMBER_X, HEADER_Y);
         cprintf( "%d", Day);
      }
      // draw items
      for( i = 0; i < DCursorRowCount( Cursor); i++){
         if( !DListCursor( Cursor, i)){
            continue;                     // skip row drawing
         }
         RowY = DListY( i); // allign to center
         // day number or time:
         if( Day == DAY_NUMBER_INVALID){
            GSetFont(TAHOMA16);
            GTextAt( DAY_NUMBER_X, RowY + CENTER_ROW_TEXT_SHIFT);
            cprintf( "%d", _PageArchive[ i].Day);
         } else {
            GSetFont(ARIAL_BOLD14);
            if(_PageArchive[ i].HourFrom == 0 && _PageArchive[ i].HourTo == 23){
               DLabel(STR_TOTAL, DAY_NUMBER_X, RowY + DListItemHeight() / 2);
            }else{
               memset(&time, 0, sizeof(UTime));
               time.Hour = _PageArchive[ i].HourFrom;
               DTimeShort(uTimeGauge(&time), DAY_NUMBER_X, RowY);
               DLabel("-", DAY_NUMBER_X, RowY + 4);
               time.Hour = _PageArchive[ i].HourTo;
               time.Min = 59;
               DTimeShort(uTimeGauge(&time), DAY_NUMBER_X, RowY + DListItemHeight() / 2);
            }
         }

         if(_PageArchive[i].HasMales && _PageArchive[i].HasFemales){
            GSetFont(ARIAL_BOLD14);
         }else {
            GSetFont(TAHOMA16);
            RowY += CENTER_ROW_TEXT_SHIFT; // allign to center
         }
         Stats = !_PageArchive[i].HasMales ? &_PageArchive[ i].Female.StatisticCalculate : &_PageArchive[ i].Male.StatisticCalculate;
         Histogram = !_PageArchive[i].HasMales ? &_PageArchive[ i].Female.Histogram : &_PageArchive[ i].Male.Histogram;
         do{
            // count :
            GTextAt( COUNT_X, RowY);
            cprintf( "%d", Stats->Count);
            // average weight :
            GTextAt( AVERAGE_X, RowY);
            DWeight( Stats->Average);
            // uniformity :
            GTextAt( UNIFORMITY_X, RowY);
            cprintf( "%5.1f", Stats->Uniformity);

            // SEX icon + histogram
            if(_PageArchive[i].HasMales && _PageArchive[i].HasFemales){
               // sex :
               if( Stats->Sex == SEX_MALE){
                  GBitmap( FLAG_SEX_X, RowY, &BmpIconMale);
                  DHistogramIconSized( Histogram, HISTOGRAM_X, RowY, HISTOGRAM_WIDTH, DListItemHeight()/2 - 1);
                  Stats = &_PageArchive[ i].Female.StatisticCalculate;
                  Histogram = &_PageArchive[ i].Female.Histogram;
                  RowY += DListItemHeight() / 2;
                  continue; // print females
               }
               if( Stats->Sex == SEX_FEMALE){
                  GBitmap( FLAG_SEX_X, RowY-5, &BmpIconFemale);
                  DHistogramIconSized( Histogram, HISTOGRAM_X, RowY, HISTOGRAM_WIDTH, DListItemHeight()/2 - 1);
                  break;
               }
            } else {
               if(_PageArchive[i].HasMales || _PageArchive[i].HasFemales){
                  if( Stats->Sex == SEX_FEMALE){
                     GBitmap( FLAG_SEX_X, RowY-5, &BmpIconFemale);
                  } else if (Stats->Sex == SEX_MALE){
                     GBitmap( FLAG_SEX_X, RowY-5, &BmpIconMale);
                  }
               }

               RowY -= CENTER_ROW_TEXT_SHIFT;
               DHistogramIconSized( Histogram, HISTOGRAM_X, RowY, HISTOGRAM_WIDTH, DListItemHeight() - 1);
               break;
            }
         } while(1);
      }
   }
//   DListLine( LINE_DATE_X);
} // DisplayPage

//------------------------------------------------------------------------------
//   Display statistics
//------------------------------------------------------------------------------

static void DisplayStatistics( TArchiveRow *Row)
// Display statistics & Histogram
{
TStatistic *Stats;
THistogram *Histogram;
TYesNo      ShowStat;
TYesNo      Redraw;

   Stats = !Row->HasMales ? &Row->Female.StatisticCalculate : &Row->Male.StatisticCalculate;
   Histogram = !Row->HasMales ? &Row->Female.Histogram : &Row->Male.Histogram;
   ShowStat = YES;
   Redraw = YES;
   forever{
      if(ShowStat && Redraw){
         // statistic :
         GClear();                                           // clear display
         DLayoutTitle( STR_STATISTICS);                         // title line
         DLayoutStatus( STR_BTN_CANCEL, STR_HISTOGRAM, 0);     // status line
         DStatistic( Stats);                       // show statistic
         GFlush();                                           // update display
         Redraw = NO;
      } else if(Redraw){
         // histogram :
         GClear();
         DLayoutTitle( STR_HISTOGRAM);
         GSetColor( DCOLOR_TITLE);
         switch( Stats->Sex){
            case SEX_FEMALE :
               GBitmap( G_WIDTH - 18, 2, &BmpIconFemale);
               break;

            case SEX_MALE :
               GBitmap( G_WIDTH - 18, 2, &BmpIconMale);
               break;

            default :
               break;
         }
         GSetColor( DCOLOR_DEFAULT);
         DLayoutStatus( STR_BTN_CANCEL, STR_STATISTICS, 0);
         if( !DHistogram( Histogram, -1, -1, WeighingConfiguration.Statistic.UniformityRange)){
            return;
         }
         Redraw = ShowStat = YES;
         continue;
      }

      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
         case K_DOWN :
         case K_DOWN | K_REPEAT :
            if(!Row->HasMales || !Row->HasFemales) continue;
            if(Stats == &Row->Male.StatisticCalculate){
               Stats = &Row->Female.StatisticCalculate;
               Histogram = &Row->Female.Histogram;
            } else {
               Stats = &Row->Male.StatisticCalculate;
               Histogram = &Row->Male.Histogram;
            }
            Redraw = YES;
         case K_LEFT :
         case K_LEFT | K_REPEAT :
         case K_RIGHT :
         case K_RIGHT | K_REPEAT :
            continue;

         case K_ENTER :
            ShowStat = NO;
            Redraw = YES;
            break;
         case K_ESC :
         case K_TIMEOUT :
            return;
      }
   }
} // DisplayStatistics

static int ArchiveItemsCount(TDayNumber day)
// return count of daily statistics for single day or count of days when day == DAY_NUMBER_INVALID
{
TArchive Archive;
int count;
int archiveCount;
TArchiveItem  Item;
TArchiveIndex Index;

   count = 0;
   archiveCount = ArchiveCount();
   while(!ArchiveOpen( &Archive));
   if( day == DAY_NUMBER_INVALID){
      for(Index = 0; Index < archiveCount; Index++){
         if(!ArchiveGet( &Archive, Index, &Item)){break;}
         if(ArchiveIsMarker( &Item)){
            count++;
         }
      }
   } else {
      Index = ArchiveSearch(&Archive, day);
      if(Index != ARCHIVE_INDEX_INVALID){
         do{
            Index++;
            if(!ArchiveGet( &Archive, Index, &Item)){
               break;
            }
            if(ArchiveIsMarker(&Item)){
               break;
            }
            if(!ArchiveIsDaily(&Item.Data)){
               count++;
            }
         }while(Index < ArchiveCount());
      }
      count++; // for total statistics for details
   }
   ArchiveClose( &Archive);
   return count;
}
