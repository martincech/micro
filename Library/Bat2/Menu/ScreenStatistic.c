//******************************************************************************
//
//   ScreenStatistic.c Bat2 statistic screen
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#include "ScreenStatistic.h"
#include "Graphic/Graphic.h"           // graphic
#include "Gadget/DLayout.h"            // screen layout
#include "Gadget/DEvent.h"             // Display events
#include "Platform/Zone.h"             // Weighing zone
#include "Statistic/Calculate.h"       // Statistic calculation
#include "Statistic/DHistogram.h"      // Display histogram
#include "Statistic/DStatistic.h"      // Display statistic
#include "Weighing/WeighingConfiguration.h"
#include "Sound/Beep.h"                // Sounds
#include "Str.h"                       // Strings
#include "Bitmap.h"


// Local functions :

static TYesNo ShowHistogram( byte Sex);
// Display histogram

//------------------------------------------------------------------------------
//   Show data
//------------------------------------------------------------------------------
ESex ScreenStatistic()
{
   return ScreenStatisticForSex(SEX_MALE);
}

ESex ScreenStatisticForSex( ESex sex)
// Display statistic & histogram
{
TYesNo      ShowFemale;
TYesNo      Redraw;
TStatistic *Statistic;

   ShowFemale = sex == SEX_FEMALE ? YES:NO;
   if( !WeighingHasFemale()){
      ShowFemale = NO;
   }
   Redraw     = YES;

   forever {
      if( Redraw){
         if( ShowFemale){
            Statistic = ZoneStatisticFemale();
         } else {
            Statistic = ZoneStatistic();
         }
         GClear();                                           // clear display
         DLayoutTitle( STR_STATISTICS);                         // title line
         DLayoutStatus( STR_BTN_CANCEL, STR_BTN_HISTOGRAM, 0);     // status line
         DStatistic( Statistic);                             // show statistic
         GFlush();                                           // update display
         Redraw = NO;
      }
      switch( DEventWait()){
         case K_RIGHT :
         case K_LEFT :
            if( !WeighingHasFemale()){
               break;
            }
            ShowFemale = !ShowFemale;
            Redraw     = YES;
            break;

         case K_ENTER :
            if( !ShowHistogram( Statistic->Sex)){
               return ShowFemale? SEX_FEMALE:SEX_MALE;
            }
            Redraw = YES;
            break;

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return ShowFemale? SEX_FEMALE:SEX_MALE;
      }
   }
} // ScreenStatistic

//******************************************************************************

//------------------------------------------------------------------------------
//   Histogram
//------------------------------------------------------------------------------

static TYesNo ShowHistogram( byte Sex)
// Display histogram
{
THistogram Histogram;
   GClear();
   // title
   DLayoutTitle( STR_HISTOGRAM);
   // title icon :
   GSetColor( DCOLOR_TITLE);
   switch( Sex){
      case SEX_FEMALE :
         GBitmap( G_WIDTH - 18, 2, &BmpIconFemale);
         break;

      case SEX_MALE :
         GBitmap( G_WIDTH - 18, 2, &BmpIconMale);
         break;

      default :
         break;
   }
   GSetColor( DCOLOR_DEFAULT);
   // status line
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_STATISTICS, 0);
   // obtain local copy of histogram
   if( Sex == SEX_FEMALE){
      Histogram = *ZoneHistogramFemale();
   } else {
      Histogram = *ZoneHistogram();
   }
   return( DHistogram( &Histogram, -1, -1, WeighingConfiguration.Statistic.UniformityRange));
} // ShowHistogram
