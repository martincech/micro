//******************************************************************************
//
//   MenuDetection.h  Detection menu
//   Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuDetection_H__
   #define __MenuDetection_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeighingConfiguration_H__
   #include "Weighing/WeighingConfiguration.h"
#endif


void MenuDetection( TWeighingDetection *Detection);
// Menu detection

#endif
