//******************************************************************************
//
//   MenuGrowthCurves.c  Growth curves menu
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#include "MenuGrowthCurves.h"
#include "Gadget/DLabel.h"             // Display label
#include "Gadget/DMenu.h"              // Display menu
#include "Curve/MenuGrowthCurve.h"     // Growth curve selection
#include "Curve/CurveList.h"           // Growth curve list
#include "Str.h"                       // Strings

static TCurveList *_CurveList;

static DefMenu( GrowthCurvesMenu)
   STR_MALES,
   STR_FEMALES,
EndMenu()

typedef enum {
   MI_MALES,
   MI_FEMALES
} EGrowthCurvesMenu;

// Local functions :

static void WeighingGrowthCurvesParameters( int Index, int y, TWeighingConfiguration *Parameters);
// Draw growth curves parameters

//------------------------------------------------------------------------------
//  Menu GrowthCurves
//------------------------------------------------------------------------------

void MenuGrowthCurves( TWeighingConfiguration *Configuration, TCurveList *CurveList)
// Menu growth curves
{
TMenuData MData;
   _CurveList = CurveList;
   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_GROWTH_CURVES, GrowthCurvesMenu, (TMenuItemCb *)WeighingGrowthCurvesParameters, Configuration, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_MALES :
            MenuGrowthCurveSelect( CurveList, STR_GROWTH_CURVE, &Configuration->Male.GrowthCurve, 0);
            break;

         case MI_FEMALES :
            MenuGrowthCurveSelect( CurveList, STR_GROWTH_CURVE, &Configuration->Female.GrowthCurve, 0);
            break;

      }
   }
} // MenuGrowthCurves

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void WeighingGrowthCurvesParameters( int Index, int y, TWeighingConfiguration *Parameters)
// Draw growth curves parameters
{
char Name[ GROWTH_CURVE_NAME_SIZE + 1];

   switch( Index){
      case MI_MALES :
         CurveListName( _CurveList, Parameters->Male.GrowthCurve, Name);
         DLabelNarrow( Name, DMENU_PARAMETERS_X, y);
         break;

      case MI_FEMALES :
         CurveListName( _CurveList, Parameters->Female.GrowthCurve, Name);
         DLabelNarrow( Name, DMENU_PARAMETERS_X, y);
         break;

   }
} // WeighingGrowthCurvesParameters
