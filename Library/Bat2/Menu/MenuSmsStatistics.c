//******************************************************************************
//
//   MenuSmsStatistics.h     Show sms statistics on screen
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************
#ifdef __GSM_SMS_STATISTICS__
#include "MenuSmsStatistics.h"
#include "Gadget/DLabel.h"
#include "Gadget/DLayout.h"       // Screen layout
#include "Console/conio.h"        // Display
#include "Gadget/DTime.h"
#include "Sound/Beep.h"           // Beep
#include "Gadget/DEvent.h"        // Display events
#include "Str.h"                  // Strings
#include "Fonts.h"                // Project fonts
#include "System/System.h"        // Timer only
#include "Gsm/Gsm.h"

#define SMS_INFOPERIOD    2

#if SMS_INFOPERIOD < 1
   #define SMS_INFOCOUNTER  1
#else
   #define SMS_INFOCOUNTER  TimerSlowCount( SMS_INFOPERIOD)
#endif
#define SmsInfoTimerSet()    _SmsInfoTimer = SMS_INFOCOUNTER
#define SmsInfoTimerStart()  _SmsInfoTimer = 1   // execute immediately
#define SmsInfoTimerTick()  (--_SmsInfoTimer == 0)

static byte _SmsInfoTimer;
#define INFO_X  3
#define INFO_Y  20
#define CHAR_H  18

void MenuSmsStatistics( void)
// Menu sms statistics
{
TYesNo Redraw;
int y;
   SmsInfoTimerStart();                // start service timer
   Redraw = NO;

   forever {
      if( Redraw){
         GClear();
         y = INFO_Y;
         // frame :
         DLayoutTitle( STR_GSM_INFO);
         DLayoutStatus( STR_BTN_CANCEL, 0, 0);
         // info :
         SetFont( TAHOMA16);
         //--------------------------------------------------------------------------
         GTextAt( INFO_X, y);
         cprintf( "Data[s/r/l]: %d/%d/%d", GsmStatistics.SmsBSendCount,
                  GsmStatistics.SmsBRcvCount, GsmStatistics.SmsBLostCount);
         y += CHAR_H;
         GTextAt( INFO_X, y);
         cprintf( "Ack[s/r/l]: %d/%d/%d", GsmStatistics.SmsASendCount,
                  GsmStatistics.SmsARcvCount, GsmStatistics.SmsALostCount);
         y += CHAR_H;
         GTextAt( INFO_X, y);
         cprintf( "Deliver count[n/avgn]: %d/%d", GsmStatistics.SmsDeliveryCount, GsmStatistics.AverageDeliverCount);
         y += CHAR_H;
         GTextAt( INFO_X, y);
         cprintf( "Deliver avg time: ");
         DTimeRight( GsmStatistics.AverageDeliverTime, G_WIDTH - 5, y);
         y += CHAR_H;
         GTextAt( INFO_X, y);
         cprintf( "Text[s/r]: %d/%d", GsmStatistics.SmsTSendCount,
                  GsmStatistics.SmsTRcvCount);
         y += CHAR_H;
         //--------------------------------------------------------------------------

         GFlush();
         Redraw = NO;
      }

      switch( DEventWait()){
         case K_FLASH1 :
            // check for service period :
            if( !SmsInfoTimerTick()){
               break;
            }
            SmsInfoTimerSet();
            Redraw = YES;
            break;

         case K_ENTER :
         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return;
      }
   }
}
#endif
