//******************************************************************************
//
//   Screen.c     Basic screen drawings
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "Screen.h"
#include "System/System.h"        // Operating system
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Str.h"                  // Strings
#include "Fonts.h"                // Font list
#include "Bitmap.h"               // Project Bitmaps
#include "Weight/DWeight.h"       // Display weight
#include "Statistic/DHistogram.h" // Display histogram
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DTime.h"         // Display time
#include "Gadget/DProgress.h"     // Progress bar
#include "Platform/Zone.h"        // Weighing zone
#include "Weighing/WeighingConfiguration.h"
#include "Weight/Weight.h"        // Weight calculations
#include "Message/MGsm.h"         // GSM status
#include "Message/GsmMessageDef.h"// GSM status enum
#include "Config/Config.h"        // Configuration data
#include "Usb/Usb.h"              // Usb
#include "Power/Power.h"          // Power
#include "Accu/Accu.h"            // Power
#include "Weighing/Memory.h"        // Memory
#include "Ethernet/EthernetLibrary.h"
#include "SensorPack/Sensors/Sensors.h"

#include <string.h>

static int ScreenIndex = 0;
#define ShowFemale() (ScreenIndex == 1)

#define DLabelEnumLeft( i, e, x, y)    DLabel( (e) + (i), x, y);

//------------------------------------------------------------------------------

// vertical section :
#define WEIGHT_Y                23
#define STATISTIC_Y             70
#define STATUS_Y                (G_HEIGHT - 14)

// title :
#define TITLE_Y          0
#define TITLE_H          14
#define TITLE_LINE_Y     15
#define TITLE_COLOR      COLOR_LIGHTGRAY
#define TITLE_LINE_COLOR COLOR_DARKGRAY
#define TITLE_TEXT_Y     2                // date/time
#define TITLE_TIME_X     5
#define TITLE_DAY_X     (TITLE_ACCU_X - 25)
#define TITLE_GSM_X     (G_WIDTH - GSM_W - 5)
#define TITLE_GSM_Y     TITLE_Y
#define TITLE_USB_X     (TITLE_GSM_X - USB_W - 5)
#define TITLE_USB_Y     TITLE_Y + 1
#define TITLE_NET_X    (TITLE_USB_X - NET_W - 5)
#define TITLE_NET_Y    TITLE_Y + 2
#define TITLE_ACCU_X    (TITLE_NET_X - ACCU_W - 5)
#define TITLE_ACCU_Y    TITLE_Y + 2

// Count :
#define COUNT_ICON_X         5
#define COUNT_X              32
#define COUNT_Y              STATISTIC_Y

// Weight :
#define SEX_X                 (G_WIDTH - 17)
#define SEX_Y                 (TITLE_LINE_Y + 2)

#define AVERAGE_WEIGHT_X       0
#define AVERAGE_WEIGHT_Y       WEIGHT_Y

#define RIGHT_WEIGHT_X        (G_WIDTH  - 90)
#define RIGHT_WEIGHT_Y        (WEIGHT_Y + 10)

#define DEVIATION_X           RIGHT_WEIGHT_X + 10
#define DEVIATION_ABSOLUTE_Y  RIGHT_WEIGHT_Y - 5
#define DEVIATION_RELATIVE_Y  RIGHT_WEIGHT_Y + 9

// Statistic :
#define STATISTIC_X              116
#define STAT_LINE_Y_OFFSET       17
#define STAT_TEXT_ICON_X_OFFSET  40
#define STAT_TEXT_ICON_Y_OFFSET  3

#define HISTOGRAM_X            5
#define HISTOGRAM_Y            (COUNT_Y + BmpCount.Height + 10)
#define HISTOGRAM_W            (STATISTIC_X - 15)
#define HISTOGRAM_H            (G_HEIGHT - HISTOGRAM_Y - 10)

#define GAIN_X                (STATISTIC_X + STAT_TEXT_ICON_X_OFFSET)
#define GAIN_ICON_X           (STATISTIC_X)
#define GAIN_Y                (STATISTIC_Y - 3)
#define GAIN_ICON_Y           (GAIN_Y + STAT_TEXT_ICON_Y_OFFSET)

#define TARGET_X              (GAIN_X)
#define TARGET_ICON_X         (GAIN_ICON_X)
#define TARGET_Y              (GAIN_Y + STAT_LINE_Y_OFFSET)
#define TARGET_ICON_Y         (TARGET_Y + STAT_TEXT_ICON_Y_OFFSET)

#define UNIFORMITY_X          (TARGET_X)
#define UNIFORMITY_ICON_X     (TARGET_ICON_X)
#define UNIFORMITY_Y          (TARGET_Y + STAT_LINE_Y_OFFSET)
#define UNIFORMITY_ICON_Y     (UNIFORMITY_Y + STAT_TEXT_ICON_Y_OFFSET)

#define TEMPERATURE_ICON_X    (UNIFORMITY_ICON_X)
#define TEMPERATURE_X         (TEMPERATURE_ICON_X + BmpStatisticsTemperature.Width + 5)
#define TEMPERATURE_Y         (UNIFORMITY_Y + STAT_LINE_Y_OFFSET)
#define TEMPERATURE_ICON_Y    (TEMPERATURE_Y + STAT_TEXT_ICON_Y_OFFSET)

#define HUMIDITY_ICON_X       (TEMPERATURE_X + STAT_TEXT_ICON_X_OFFSET + 5)
#define HUMIDITY_X            (HUMIDITY_ICON_X + BmpStatisticsHumidity.Width + 5)
#define HUMIDITY_Y            (TEMPERATURE_Y)
#define HUMIDITY_ICON_Y       (TEMPERATURE_ICON_Y)

#define CO2_X                 (UNIFORMITY_X)
#define CO2_ICON_X            (UNIFORMITY_ICON_X)
#define CO2_Y                 (TEMPERATURE_Y + STAT_LINE_Y_OFFSET)
#define CO2_ICON_Y            (CO2_Y + STAT_TEXT_ICON_Y_OFFSET)



// Big accu symbol dimensions :
#define BIG_ACCU_W          30
#define BIG_ACCU_H          63
#define BIG_ACCU_X         (G_WIDTH  / 2 - BIG_ACCU_W / 2)
#define BIG_ACCU_Y          47
#define BIG_ACCU_TEXT_Y     120
#define BIG_ACCU_TEXT_H     15

// Start at :
#define START_BIG_WAIT_X      (G_WIDTH  / 2 - BmpWait.Width / 2)
#define START_BIG_WAIT_Y      (TITLE_Y + TITLE_H + 10)
#define START_AT_Y            (START_BIG_WAIT_Y + BmpWait.Height + 10)
#define START_AT_X            (0)

// Symbol dimensions :
#define ACCU_W  23
#define ACCU_H  10

#define GSM_W  14
#define GSM_H 14

#define USB_W 19
#define USB_H 13

#define NET_W 14
#define NET_H 14

#define ACCU_MAX_COUNT     6           // number of accu symbols

// Local variables :

static byte AccuCounter = 0;

//------------------------------------------------------------------------------

// Local functions :

static void _DefaultScreen();
// Default weighing screen

static void _WaitingWeighingScreen();
// Waiting to start weighing screen

static void _TopStatusLine( void);
// Display top status line

static void _GsmStatus( int x, int y);
// Display GSM status

static void _UsbStatus( int x, int y);
// Display Usb status

static void _NetStatus( int x, int y);
// Display net status

static void _AccuStatus( int x, int y);
// Display battery status

static void _DiagnosticScreen( void);
// Diagnostic screen

static void DisplayBigAccu( void);
// Display accu capacity (charger only)

//------------------------------------------------------------------------------
// Redraw
//------------------------------------------------------------------------------
void ScreenChangeAndRedrawBySex( ESex Sex, TYesNo Force)
{
   switch (Sex) {
   case SEX_FEMALE:
      ScreenIndex = 1;
      break;
   default:
      ScreenIndex = 0;
      break;
   }
   if(!WeighingHasFemale()){
      ScreenIndex = 0;
   }
   ScreenRedraw(Force);
}

void ScreenChangeAndRedraw( EKeyboardKey LastKey, TYesNo Force)
// Redraw screen, force to redraw, send last key to switch screen
{
   switch(LastKey){
      case K_RIGHT:
         if(ScreenIndex < 1){
            ScreenIndex++;
         }
         break;
      case K_LEFT:
         if(ScreenIndex > 0){
            ScreenIndex--;
         }
         break;
   }

   if(!WeighingHasFemale()){
      ScreenIndex = 0;
   }
   ScreenRedraw(Force);
}

void ScreenRedraw( TYesNo Force)
// Redraw screen, force to redraw
{
static dword SamplesCount = -1;
static dword WeighingStatusLast = -1;
static TYesNo LastGReady = NO;

   if(!GReady()) {
      LastGReady = NO;
      return;
   }
   if(!LastGReady) {
      Force = YES;
      LastGReady = YES;
   }

   switch( WeighingStatus()){
      case WEIGHING_STATUS_DIAGNOSTICS :
         Force = YES;
         break;

      default:
         if(SamplesCount != ZoneSamplesCount()) {
            Force = YES;
         }
         break;
   }

   if(WeighingStatusLast != WeighingStatus()) {
      Force = YES;
      WeighingStatusLast = WeighingStatus();
   }

   if(Force) {
      GClear();
   }

   _TopStatusLine();
   if(!Force) {
      GFlush();
   }
//-----------------------------------------------------------------------------
   switch( WeighingStatus()){
      case WEIGHING_STATUS_DIAGNOSTICS :
         _DiagnosticScreen();
         break;
      case WEIGHING_STATUS_WAIT:
         _WaitingWeighingScreen();
         break;
      default:
         if(!Force) {
            break;
         }
         _DefaultScreen();
         SamplesCount = ZoneSamplesCount();
         break;
   }
   GFlush();
} // ScreenRedraw

//------------------------------------------------------------------------------
//   Power failure
//------------------------------------------------------------------------------

void ScreenPowerFailure( void)
// Display power failure
{
   GClear();
   SetFont( ARIAL_BOLD14);
   DLabelCenter( "POWER FAIL", 0, 0, G_WIDTH, G_HEIGHT);
   GFlush();
} // ScreenPowerFailure

//------------------------------------------------------------------------------
//   Logo
//------------------------------------------------------------------------------

void ScreenLogo( void)
// Display logo
{
   GClear();
   // display "Logo" :
   SetFont( ARIAL_BOLD21);
   DLabelCenter( "BAT2", 0, 0, G_WIDTH, G_HEIGHT);
   // Display scale name :
   SetFont( TAHOMA16);
   GSetMode( GMODE_XOR);               // always visible
   GTextAt( 0, G_HEIGHT - GCharHeight() - 1);
   cputs( Bat2Device.Name);
   // version string (right aligned) :
   DLabelFormat( G_WIDTH - 5, G_HEIGHT - GCharHeight() - 1,
                 "%d.%02d-%d.%02d", Bat2Version.Software >> 8, Bat2Version.Software & 0xFF,
                                    Bat2Version.Hardware >> 8, Bat2Version.Hardware & 0xFF);
   // redraw
   GSetMode( GMODE_REPLACE);          // default mode
   GFlush();
} // ScreenLogo

//------------------------------------------------------------------------------
//   Charger
//------------------------------------------------------------------------------

void ScreenCharger( void)
// Display charger
{
   GClear();
   // date/time :
   // charger/USB symbol :
   //GBitmap( G_WIDTH - 20 - 1, 1, &BmpCharger);
   // accumulator animation :
   DisplayBigAccu();
} // ScreenCharger

//******************************************************************************

//------------------------------------------------------------------------------
// Default screen
//------------------------------------------------------------------------------

static void _DefaultScreen()
// Default weighing screen
{
TStatistic    *Statistic;
THistogram    *Histogram;
TWeightGauge   TargetWeight;
const TBitmap *SexBitmap;
   SexBitmap = 0;
   if( WeighingHasFemale() && ShowFemale()){
      Statistic    = ZoneStatisticFemale();
      Histogram    = ZoneHistogramFemale();
      TargetWeight = WeighingTargetFemale();
      SexBitmap    = &BmpIconFemale;
   } else {
      Statistic    = ZoneStatistic();
      Histogram    = ZoneHistogram();
      TargetWeight = WeighingTarget();
      if( WeighingHasFemale()){
         SexBitmap = &BmpIconMale;
      }
   }
   // sex icon :
   if( SexBitmap){
      GBitmap( SEX_X, SEX_Y, SexBitmap);
   }
   // average weight :
   SetFont( ARIAL_BOLD55);
   DWeightNarrow( AVERAGE_WEIGHT_X, AVERAGE_WEIGHT_Y, WeightGaugeDivision( Statistic->Average));
   // deviation :
   switch( WeighingPredictionMode()){
      case PREDICTION_MODE_GROWTH_CURVE :
         SetFont( ARIAL_BOLD14);
         DWeightNarrow( DEVIATION_X,  DEVIATION_ABSOLUTE_Y, Statistic->Average - TargetWeight);
         GTextAt( DEVIATION_X, DEVIATION_RELATIVE_Y);
         cprintf( "%5.1f %%", (Statistic->Average - TargetWeight) * 1000 / TargetWeight);
         break;
   }
   switch(WeighingStatus()){
      case WEIGHING_STATUS_UNDEFINED:
      case WEIGHING_STATUS_SLEEP:
      case WEIGHING_STATUS_SUSPENDED:
         SetFont( ARIAL_BOLD55);
         // suspended or sleeped weighing
         GBitmap(G_WIDTH/2-BmpPause.Width/2,  AVERAGE_WEIGHT_Y + GTextHeight("0") + 5, &BmpPause);
            break;
      case WEIGHING_STATUS_STOPPED:
         SetFont( ARIAL_BOLD55);
         GBitmap(G_WIDTH/2-BmpStop.Width/2, AVERAGE_WEIGHT_Y + GTextHeight("0") +5, &BmpStop);
            break;
      default:
         SetFont( ARIAL_BOLD21);
         // histogram :
         DHistogramIconSized(Histogram, HISTOGRAM_X, HISTOGRAM_Y, HISTOGRAM_W, HISTOGRAM_H);
         // gain :
         GBitmap( GAIN_ICON_X,  GAIN_ICON_Y, &BmpStatisticsGain);
         DWeightNarrow( GAIN_X, GAIN_Y, Statistic->Gain);
         // target :
         GBitmap( TARGET_ICON_X,  TARGET_ICON_Y, &BmpStatisticsTarget);
         DWeightNarrow( TARGET_X, TARGET_Y, TargetWeight);
         // uniformity :
         GBitmap( UNIFORMITY_ICON_X,  UNIFORMITY_ICON_Y, &BmpStatisticsUni);
         GTextAt( UNIFORMITY_X,       UNIFORMITY_Y);
         cprintf( "%5.1f%%", Statistic->Uniformity);
         // Temperature
         GBitmap( TEMPERATURE_ICON_X,  TEMPERATURE_ICON_Y, &BmpStatisticsTemperature);
         GTextAt( TEMPERATURE_X,       TEMPERATURE_Y);
         cprintf( "%d\345C", TemperatureRead());
         // Humidity
         GBitmap( HUMIDITY_ICON_X,  HUMIDITY_ICON_Y, &BmpStatisticsHumidity);
         GTextAt( HUMIDITY_X,       HUMIDITY_Y);
         cprintf( "%d%%", HumidityRead());
         // CO2
         GBitmap( CO2_ICON_X,  CO2_ICON_Y, &BmpStatisticsCO2);
         GTextAt( CO2_X,       CO2_Y);
         cprintf( "%d ppm", CO2Read());

      //-----------------------------------------------------------------------------
         // samples count :
         SetFont( ARIAL_BOLD27);
         GBitmap( COUNT_ICON_X, COUNT_Y, &BmpCount);
         GTextAt( COUNT_X, COUNT_Y);
         cprintf( "%d", ZoneSamplesCount());
   }
} // _DefaultScreen

//------------------------------------------------------------------------------
// Waiting to start
//------------------------------------------------------------------------------

static void _WaitingWeighingScreen()
// Waiting to start weighing screen
{
int height;

   SetFont( ARIAL_BOLD21);
   GBitmap( START_BIG_WAIT_X, START_BIG_WAIT_Y, &BmpWait);
   height = GTextHeight(StrGet(STR_START));
   DLabelCenter(STR_START, START_AT_X, START_AT_Y, G_WIDTH, height);
   DDateTimeCenter( uClockDateTime( WeighingStartTime()), START_AT_X, START_AT_Y + height + 5);
}

//------------------------------------------------------------------------------
// Top status Line
//------------------------------------------------------------------------------

static void _TopStatusLine( void)
// Display top status line
{
   GSetColor( TITLE_COLOR);
   GBox( 0, TITLE_Y, G_WIDTH, TITLE_H);
   GSetColor( TITLE_LINE_COLOR);
   GLine( 0, TITLE_LINE_Y, G_WIDTH - 1, TITLE_LINE_Y);
   GSetColor( DCOLOR_DEFAULT);
   // date/time :
   SetFont( ARIAL_BOLD14);
   DDateTime( SysDateTime(), TITLE_TIME_X, TITLE_TEXT_Y);
   GTextAt( TITLE_DAY_X, TITLE_TEXT_Y);
   cprintf( "%d", WeighingDay());
   _GsmStatus( TITLE_GSM_X, TITLE_GSM_Y);
   _UsbStatus( TITLE_USB_X, TITLE_USB_Y);
   //_AccuStatus( TITLE_ACCU_X, TITLE_ACCU_Y);
   _NetStatus( TITLE_NET_X, TITLE_NET_Y);
} // _TopStatusLine

//------------------------------------------------------------------------------
// GMS status
//------------------------------------------------------------------------------

static void _GsmStatus( int x, int y)
// Display GSM status
{
   switch( MGsmStatus()){
      case MGSM_STATUS_ERROR :
      case MGSM_STATUS_OFF :
         GBitmap( x, y, &BmpGsmOff);
         break;

      case MGSM_STATUS_ON :
      case MGSM_STATUS_NO_PIN :
      case MGSM_STATUS_NOT_REGISTERED :
         GBitmap( x, y, &BmpGsmNoSignal);
         break;

      case MGSM_STATUS_READY :
      case MGSM_STATUS_CHECK_REGISTERED :
         GBitmap( x, y, &BmpGsmOk);
         break;

      case MGSM_STATUS_SEND :
         GBitmap( x, y, &BmpGsmSend);
         break;

      case MGSM_STATUS_RECEIVE :
         GBitmap( x, y, &BmpGsmReceive);
         break;

      case MGSM_STATUS_WAIT_PIN :
         GBitmap( x, y, &BmpGsmPin);
         break;

      default :
         break;
   }
} // _GsmStatus

//------------------------------------------------------------------------------
// USB status
//------------------------------------------------------------------------------

static void _UsbStatus( int x, int y)
// Display Usb status
{
   GTextAt(x, y);
   switch( UsbStatusGet()){
      case USB_STATE_CHARGER :
         GBitmap( x, y, &BmpCharger);
         break;

      case USB_STATE_DEVICE_CONNECTED :
         GBitmap( x, y, &BmpPC);
         break;

      case USB_STATE_HOST_ENABLED :
      case USB_STATE_HOST_CONNECTED :
         GBitmap( x, y, &BmpUsbStick);
         break;

      default :
         break;
   }
} // _UsbStatus

//------------------------------------------------------------------------------
// USB status
//------------------------------------------------------------------------------

static void _NetStatus( int x, int y)
// Display net status
{
   GTextAt(x, y);
   if(EthernetIsLink()) {
      cputch('L');
   } else {
      cputch('\0');
   }
} // _NetStatus

//------------------------------------------------------------------------------
// USB status
//------------------------------------------------------------------------------

#include "Diagnostic/DiagnosticDef.h"
#include "Diagnostic/DiagnosticFifo.h"

static void _DiagnosticScreen( void)
// Diagnostic screen
{
   GTextAt( 50, 50);
   cprintf( "Buffer %d/%d\n", DiagnosticFifoCount(), DIAGNOSTIC_FIFO_SIZE);
   cprintf( "Saved %d, Lost %d", MemoryDiagnosticSamples(), MemoryDiagnosticLostSamples());
} // _DiagnosticScreen

//------------------------------------------------------------------------------
// Big accu
//------------------------------------------------------------------------------

static void DisplayBigAccu( void)
// Display accu capacity (charger only)
{
int Counter;
   // animation counter :
   AccuCounter++;
   if( AccuCounter >= ACCU_MAX_COUNT){
      AccuCounter = 0;
   }

   GBitmap( BIG_ACCU_X, BIG_ACCU_Y, &BmpAccu);
   SetFont( TAHOMA16);
   switch(Power.ChargerStatus) {
      case CHARGER_STATUS_CHARGING:
         DLabelCenter( STR_CHARGING, 0, BIG_ACCU_TEXT_Y, G_WIDTH, BIG_ACCU_TEXT_H);
         Counter = AccuCounter;           // charging in process
         break;

      case CHARGER_STATUS_DONE:
         DLabelCenter( STR_CHARGED, 0, BIG_ACCU_TEXT_Y, G_WIDTH, BIG_ACCU_TEXT_H);
         Counter = ACCU_MAX_COUNT - 1;    // full accu
         break;

      case CHARGER_STATUS_OFF:
         DLabelCenter( STR_CHARGING_OFF, 0, BIG_ACCU_TEXT_Y, G_WIDTH, BIG_ACCU_TEXT_H);
         Counter = (ACCU_MAX_COUNT - 1) * AccuCapacityRemaining() / 100;
         break;

      case CHARGER_STATUS_ERROR:
      default:
         DLabelCenter( STR_ERROR, 0, BIG_ACCU_TEXT_Y, G_WIDTH, BIG_ACCU_TEXT_H);
         Counter = (ACCU_MAX_COUNT - 1) * AccuCapacityRemaining() / 100;
         break;
   }
   // draw contents :
   if( Counter == 0){
      return;                          // empty accu
   }
   GSetColor( COLOR_BLACK);
   GBox( BIG_ACCU_X + 3,  BIG_ACCU_Y + BIG_ACCU_H - 13, 12, 10);
   GSetColor( COLOR_DARKGRAY);
   GBox( BIG_ACCU_X + 15, BIG_ACCU_Y + BIG_ACCU_H - 13, 12, 10);
   GSetColor( COLOR_BLACK);
   if( Counter == 1){
      return;
   }
   GBox( BIG_ACCU_X + 3,  BIG_ACCU_Y + BIG_ACCU_H - 24, 12, 10);
   GSetColor( COLOR_DARKGRAY);
   GBox( BIG_ACCU_X + 15, BIG_ACCU_Y + BIG_ACCU_H - 24, 12, 10);
   GSetColor( COLOR_BLACK);
   if( Counter == 2){
      return;
   }
   GBox( BIG_ACCU_X + 3,  BIG_ACCU_Y + BIG_ACCU_H - 35, 12, 10);
   GSetColor( COLOR_DARKGRAY);
   GBox( BIG_ACCU_X + 15, BIG_ACCU_Y + BIG_ACCU_H - 35, 12, 10);
   GSetColor( COLOR_BLACK);
   if( Counter == 3){
      return;
   }
   GBox( BIG_ACCU_X + 3,  BIG_ACCU_Y + BIG_ACCU_H - 46, 12, 10);
   GSetColor( COLOR_DARKGRAY);
   GBox( BIG_ACCU_X + 15, BIG_ACCU_Y + BIG_ACCU_H - 46, 12, 10);
   GSetColor( COLOR_BLACK);
   if( Counter == 4){
      return;
   }
   GBox( BIG_ACCU_X + 3,  BIG_ACCU_Y + BIG_ACCU_H - 56, 12, 9);
   GSetColor( COLOR_DARKGRAY);
   GBox( BIG_ACCU_X + 15, BIG_ACCU_Y + BIG_ACCU_H - 56, 12, 9);
   GSetColor( COLOR_BLACK);
} // DisplayBigAccu

//------------------------------------------------------------------------------
// Accu
//------------------------------------------------------------------------------

static void _AccuStatus( int x, int y)
// Display accu capacity
{
int Percent;
byte Capacity;
byte Status;
   if(!Power.Accu.Present) {
      return;
   }

   // animation counter :
   AccuCounter++;
   if( AccuCounter >= ACCU_MAX_COUNT){
      AccuCounter = 0;
   }

   switch(Power.ChargerStatus) {
      case CHARGER_STATUS_CHARGING:
         Percent = AccuCounter * 20;
         break;

      case CHARGER_STATUS_DONE:
      case CHARGER_STATUS_OFF:
         Capacity = Power.Accu.CapacityRemaining;
         if(Capacity != ACCU_CAPACITY_INVALID) {
            Percent = Capacity;
            break;
         }

      case CHARGER_STATUS_ERROR:
         if(AccuCounter % 2) {
            GBitmap( x, y, &BmpBattery);
         }
         return;
   }

   // accu symbol :
   GBitmap( x, y, &BmpBattery);
   GSetColor( COLOR_DARKGRAY);
   DProgressBarRight( Percent, x + 3, y + 1, ACCU_W - 4, (ACCU_H - 2) / 2);
   GSetColor( COLOR_BLACK);
   DProgressBarRight( Percent, x + 3, y + 5, ACCU_W - 4, (ACCU_H - 2) / 2);
} // DisplayAccu
