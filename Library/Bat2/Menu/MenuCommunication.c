//******************************************************************************
//
//   MenuCommunication.c  Communication menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuCommunication.h"
#include "Gadget/DMenu.h"         // Display menu
#include "Str.h"                  // Strings

#include "Menu.h"
#include "Message/MenuGsm.h"
#include "MenuRs485.h"
#include "Communication/MenuInternetConnection.h"
#include "Usb/MenuUsb.h"
#include "Device/VersionDef.h"

static DefMenu( CommunicationMenu)
   STR_CELLULAR,
   //STR_RS485,
   //STR_INTERNET_CONNECTION,
   //STR_USB,
EndMenu()

typedef enum {
   MI_CELLULAR,
   //MI_RS485,
   //MI_INTERNET_CONNECTION,
   //MI_USB
} ECommunicationMenu;

//------------------------------------------------------------------------------
//  Menu Communication
//------------------------------------------------------------------------------

void MenuCommunication( void)
// Menu communication
{
TMenuData MData;

   DMenuClear( MData);
   /*if(!BAT2_HAS_INTERNET_CONNECTION(Bat2Version.Modification)){
       MData.Mask |= (1 << MI_INTERNET_CONNECTION);
   }*/
   if(!BAT2_HAS_GSM_MODULE(Bat2Version.Modification)){
       MData.Mask |= (1 << MI_CELLULAR);
   }
   /*if(!BAT2_HAS_RS485_MODULE(Bat2Version.Modification)){
       MData.Mask |= (1 << MI_RS485);
   }*/
   forever {
      // selection :
      if( !DMenu( STR_COMMUNICATION, CommunicationMenu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_CELLULAR :
            MenuGsm();
            break;

         /*case MI_RS485 :
            MenuRs485();
            break;

         case MI_INTERNET_CONNECTION :
            MenuInternetConnection();
            break;

         case MI_USB :
            MenuUsb();
            break;*/

      }
   }
} // MenuCommunication
