//******************************************************************************
//
//   MenuWeighingTargetAdjust.h     Adjust target weights during weighing menu
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWeighingTargetAdjust.h"
#include "Weighing/WeighingConfiguration.h"
#include "Gadget/DLayout.h"
#include "Gadget/DLabel.h"
#include "Gadget/DMsg.h"
#include "Gadget/DEvent.h"
#include "Gadget/DGraph.h"
#include "Gadget/DMenu.h"
#include "Weight/DWeight.h"
#include "Weight/Weight.h"
#include "Weighing/Curve.h"
#include "Curve/CurveList.h"
#include "Platform/Acceptance.h"      // Acceptance filter
#include "Scheduler/WeighingScheduler.h"
#include "Str.h"
#include "Fonts.h"

static DefMenu( TargetAdjustMenu)
   STR_MALES,
   STR_FEMALES,
EndMenu()

typedef enum {
   MI_MALES,
   MI_FEMALES
} ETargetAdjustMenu;

static void MenuWeighingTargetAdjustOne( ESex gender);
// adjust for selected gender

static TYesNo MenuWeighingTargetAdjustAutomatic( TWeightGauge *TargetWeight);
// menu for automatic mode

static TYesNo MenuWeighingTargetAdjustCurve( TWeightGauge *TargetWeight, TGrowthCurve *Curve);
// menu for growt curves mode

//------------------------------------------------------------------------------
//  Menu WeighingTargetAdjust
//------------------------------------------------------------------------------

void MenuWeighingTargetAdjust( void)
// Menu weighing target adjustment
{
TMenuData MData;

   MData.Item = WeighingConfiguration.TargetWeights.Sex;
   if( WeighingConfiguration.TargetWeights.SexDifferentiation == SEX_DIFFERENTIATION_YES){
      DMenuClear( MData);
      // selection :
      if( !DMenu( STR_ADJUST_TARGET_WEIGHTS, TargetAdjustMenu, 0, 0, &MData)){
         return;
      }
   }
   MenuWeighingTargetAdjustOne(MData.Item);
}

static void MenuWeighingTargetAdjustOne(ESex gender)
{
TYesNo accepted;

   if( WeighingConfiguration.TargetWeights.Mode == PREDICTION_MODE_AUTOMATIC ||
      (WeighingConfiguration.TargetWeights.Mode == PREDICTION_MODE_GROWTH_CURVE &&
       !CurveHasDay(gender, WeighingDay()))){
      if( gender != SEX_FEMALE){
         accepted = MenuWeighingTargetAdjustAutomatic(&WeighingContext.TargetWeight);
      } else {
         accepted = MenuWeighingTargetAdjustAutomatic(&WeighingContext.TargetWeightFemale);
      }
   } else {
      if( gender != SEX_FEMALE){
         accepted = MenuWeighingTargetAdjustCurve(&WeighingContext.TargetWeight, &WeighingContext.GrowthCurveMale);
      } else {
         accepted = MenuWeighingTargetAdjustCurve(&WeighingContext.TargetWeight, &WeighingContext.GrowthCurveFemale);
      }

   }

   if(accepted){
      DMsgWait();
      WeighingSchedulerResume();
      while(WeighingStatus() != WEIGHING_STATUS_WEIGHING){
         DEventWait();
      }
   }
}

//------------------------------------------------------------------------------
//  Automatic mode
//------------------------------------------------------------------------------

static TYesNo MenuWeighingTargetAdjustAutomatic( TWeightGauge *TargetWeight)
// menu for automatic mode
{
   return DInputWeight(STR_ADJUST_TARGET_WEIGHTS, STR_NEW_TARGET, TargetWeight);
}


//------------------------------------------------------------------------------
//  Growth curve mode
//------------------------------------------------------------------------------
#include "Console/Conio.h"        // Console output

static TGrowthCurve *genderCurve;
static void XValueDesc( char *buf, int32 index){
   bprintf(buf, "%s: %d", StrGet(STR_DAY), genderCurve->Point[index].Day);
}
static void CurveRecompute( TGraphLine *Line, word ChangedPoint, int32 PrevValue)
{
int i;
float multiplier;

   if( PrevValue == 0){
      PrevValue = 1;
   }
   multiplier = (float)Line->Points[ChangedPoint]/(float)PrevValue;
   for( i = ChangedPoint + 1; i < Line->PointCount; i++){
      Line->Points[i] *= multiplier;
   }
}

static TYesNo MenuWeighingTargetAdjustCurve( TWeightGauge *TargetWeight, TGrowthCurve *Curve)
// menu for growt curves mode
{
TGraph         Graph;
TGraphLine     Lines[2];
int32          Points[2][GROWTH_CURVE_POINT_COUNT];
int            i;
TYesNo         Redraw = YES;

   genderCurve = Curve;
   for( i = 0; i < Curve->Count; i++){
      Points[1][i] = Points[0][i] =  Curve->Point[i].Weight;
   }
   Lines[1].PointCount = Lines[0].PointCount = Curve->Count;
   Lines[1].LegendImage = Lines[0].LegendImage = (TBitmap*) &BmpStatisticsTarget;
   Lines[0].Points = Points[0];    // original line
   Lines[1].Points = Points[1];   // edited line
   Graph.Lines = Lines;
   Graph.LineCount = 2;
   Graph.Lines[1].YValueFormater = Graph.Lines[0].YValueFormater = BWeightWithUnits;
   Graph.XValueFormater = XValueDesc;
   Graph.CursorOnPoint = WeighingDay() - 1;

   forever {
      if( Redraw){
         GClear();
         DLayoutTitle(STR_GROWTH_CURVE);
         DLayoutStatus( STR_BTN_OK, STR_BTN_SELECT, 0); // display status line
         DGraph( &Graph, DLAYOUT_TITLE_H, G_HEIGHT - DLAYOUT_TITLE_H - DLAYOUT_STATUS_H);
         GFlush();
         Redraw = NO;
      }

      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            if( Lines[1].Points[ Graph.CursorOnPoint] < WeightGauge(WeightUnits.Range)){
               i = Lines[1].Points[ Graph.CursorOnPoint];
               Lines[1].Points[ Graph.CursorOnPoint] += WeightGaugeDivision(WeightUnits.DivisionMax);
               if( WeighingConfiguration.TargetWeights.AdjustTargetWeights == ADJUST_TARGET_WEIGHTS_RECOMPUTE){
                  CurveRecompute( &Lines[1], Graph.CursorOnPoint, i);
               }
               Redraw = YES;
            }
            break;
         case K_DOWN :
         case K_DOWN | K_REPEAT :
            if( Lines[1].Points[ Graph.CursorOnPoint] > 0){
               i = Lines[1].Points[ Graph.CursorOnPoint];
               Lines[1].Points[ Graph.CursorOnPoint] -= WeightGaugeDivision(WeightUnits.DivisionMax);
               if( WeighingConfiguration.TargetWeights.AdjustTargetWeights == ADJUST_TARGET_WEIGHTS_RECOMPUTE){
                  CurveRecompute( &Lines[1], Graph.CursorOnPoint, i);
               }
               Redraw = YES;
            }
            break;
         case K_RIGHT :
         case K_RIGHT | K_REPEAT :
            if( Graph.CursorOnPoint < (Lines[0].PointCount - 1)){
               Graph.CursorOnPoint++;
               Redraw = YES;
            }
            break;
         case K_LEFT :
         case K_LEFT | K_REPEAT :
            if( Graph.CursorOnPoint > 0 && Graph.CursorOnPoint > WeighingDay() - 1){
               Graph.CursorOnPoint--;
               Redraw = YES;
            }
            break;

         case K_ENTER:
            i  = Lines[1].Points[ Graph.CursorOnPoint];
            Redraw = YES;
            if( !DInputWeight(STR_ADJUST_TARGET_WEIGHTS, STR_NEW_TARGET, &i)){
               break;
            }
            if( WeighingConfiguration.TargetWeights.AdjustTargetWeights == ADJUST_TARGET_WEIGHTS_RECOMPUTE){
               //swap new with old value
               Lines[1].Points[ Graph.CursorOnPoint] = Lines[1].Points[ Graph.CursorOnPoint] ^ i;
               i = Lines[1].Points[ Graph.CursorOnPoint] ^ i;
               Lines[1].Points[ Graph.CursorOnPoint] = Lines[1].Points[ Graph.CursorOnPoint] ^ i;

               CurveRecompute( &Lines[1], Graph.CursorOnPoint, i);
            }else {
               Lines[1].Points[ Graph.CursorOnPoint] = i;
            }
            break;

         case K_ESC :
            Redraw = NO; // Redraw is just placeholder
            for( i = 0; i < Curve->Count; i++){
               if( Curve->Point[i].Weight != Lines[1].Points[i]){
                  Redraw = YES;
                  break;
               }
            }
            if( !Redraw || !DMsgYesNo(STR_CONFIRMATION, STR_SAVE_CONFIGURATION, 0)){
               // no value changed, dont save new curve
               return NO;
            }

            if(Curve->Point[WeighingDay() - 1].Weight == Lines[1].Points[WeighingDay() - 1]){
               // today target not changed, don't change context target
               Redraw = NO;
            }
            for( i = 0; i < Curve->Count; i++){
               Curve->Point[i].Weight = Lines[1].Points[i];
            }
            if( Redraw){
               *TargetWeight = Curve->Point[WeighingDay() - 1].Weight;
            }
            return Redraw;

         case K_TIMEOUT :
            return NO;

         default:
            break;
      }

   }

   return NO;
}

