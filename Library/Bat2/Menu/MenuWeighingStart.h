//******************************************************************************
//
//   MenuWeighingStart.h Weighing start menu
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuWeighingStart_H__
   #define __MenuWeighingStart_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuWeighingStart( void);
// Menu weighing start

#endif
