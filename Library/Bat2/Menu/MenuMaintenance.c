//******************************************************************************
//
//   MenuMaintenance.c  Maintenance menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuMaintenance.h"
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DInput.h"        // Display input text
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMsg.h"
#include "Config/Config.h"        // Device configuration
#include "Str.h"                  // Strings

#include "Country/MenuCountry.h"
#include "Weight/MenuWeightUnits.h"
#include "Menu/MenuMaintenanceCalibration.h"
#include "Menu/MenuMaintenanceSimulation.h"
#include "Menu/MenuPower.h"
#include "Usb/MenuUsb.h"
#include "Menu/MenuPlatformInfo.h"
#include "Menu/Password.h"
#include "Weighing/WeighingConfiguration.h"
#include "Menu.h"
#include "Config/FactoryDefault.h"

#ifdef OPTION_BAT2_COMPACT
   #include "Gadget/Dtime.h"
   #include "Calibration/Calibration.h"  // Last calibration time display
#endif

static DefMenu( MaintenanceMenu)
   STR_SCALE_NAME,
   STR_COUNTRY,
   STR_WEIGHING,
   STR_PLATFORM,
   STR_CALIBRATION,
   STR_SIMULATION,
   STR_PASSWORD,
   STR_BOOTLOADER,
   STR_FACTORY_DEFAULTS,
EndMenu()

typedef enum {
   MI_SCALE_NAME,
   MI_COUNTRY,
   MI_WEIGHING,
   MI_PLATFORM,
   MI_CALIBRATION,
   MI_SIMULATION,
   MI_PASSWORD,
   MI_BOOTLOADER,
   MI_FACTORY_DEFAULTS
} EMaintenanceMenu;

static void MaintenanceParameters( int Index, int y, void *Parameters);
// Draw maintenance parameters

//------------------------------------------------------------------------------
//  Menu Maintenance
//------------------------------------------------------------------------------

void MenuMaintenance( void)
// Menu maintenance
{
TMenuData MData;
char      Name[ DEVICE_NAME_SIZE + 1];

   //>>> check for password :
   if( !PasswordCheck()){
      return;
   }
   //<<<
   DMenuClear( MData);
   switch(WeighingStatus()){
      case WEIGHING_STATUS_DIAGNOSTICS:
         MData.Mask |= (1 << MI_CALIBRATION);
         break;
   }
#ifndef DEBUG
      MData.Mask |= (1 << MI_PLATFORM) | (1 << MI_SIMULATION) | (1 << MI_BOOTLOADER) | (1 << MI_POWER);
#endif
   forever {
      // selection :
      if( !DMenu( STR_MAINTENANCE, MaintenanceMenu, (TMenuItemCb *)MaintenanceParameters, 0, &MData)){
         ConfigDeviceSave();
         return;
      }
      switch( MData.Item){
         case MI_SCALE_NAME :
            strcpy( Name, Bat2Device.Name);
            if( !DInputText( STR_SCALE_NAME, STR_ENTER_SCALE_NAME, Name, DEVICE_NAME_SIZE, NO)){
               break;
            }
            strcpy( Bat2Device.Name, Name);
            break;

         case MI_COUNTRY :
            MenuCountry();
            break;

         case MI_WEIGHING :
            MenuWeightUnits();
            break;

         case MI_PLATFORM :
            MenuPlatformInfo();
            break;

         case MI_CALIBRATION :
            MenuMaintenanceCalibration();
            break;

         case MI_SIMULATION :
            MenuMaintenanceSimulation();
            break;

         case MI_PASSWORD :
            PasswordEdit();
            break;

         case MI_BOOTLOADER :
            DMsgWait();
            MenuExitSet( MENU_EXIT_BOOTLOADER);
            return;

         case MI_FACTORY_DEFAULTS :
            if( !DMsgYesNo( STR_CONFIRMATION, STR_REALLY_RESET_TO_FACTORY_DEFAULTS, 0)){
               break;
            }
            DMsgWait();
            FactoryDefault();
            MenuExitSet( MENU_EXIT_REBOOT);
            return;
      }
   }
} // MenuMaintenance

//------------------------------------------------------------------------------
//  Maintenance parameters
//------------------------------------------------------------------------------

static void MaintenanceParameters( int Index, int y, void *Parameters)
// Draw maintenance parameters
{
   switch( Index){
      case MI_SCALE_NAME :
         DLabelNarrow( Bat2Device.Name, DMENU_PARAMETERS_X, y);
         break;

#if defined(OPTION_BAT2_COMPACT) && !defined(OPTION_SIMULATION)
      case MI_CALIBRATION :
         DDateTimeShortRight( CalibrationTime(), DMENU_PARAMETERS_X, y);
         break;
#endif

      case MI_PASSWORD :
         DLabelEnum( PasswordInactive() ? NO : YES, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;
   }
} // MaintenanceParameters
