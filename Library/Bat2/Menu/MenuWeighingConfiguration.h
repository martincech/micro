//******************************************************************************
//
//   MenuWeighingConfiguration.h  Weighing configuration menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuWeighingConfiguration_H__
   #define __MenuWeighingConfiguration_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __StrDef_H__
   #include "String/StrDef.h"
#endif

#ifndef __WeighingConfiguration_H__
   #include "Weighing/WeighingConfiguration.h"
#endif

#ifndef __PredefinedList_H__
   #include "Predefined/PredefinedList.h"
#endif

void MenuWeighingConfiguration( TUniStr Title, TWeighingConfiguration *Configuration, TYesNo Predefined, TPredefinedList *PredefinedList);
// Menu weighing configuration or <Predefined> weighing

#endif
