//******************************************************************************
//
//   MenuRs485.c  Rs485 menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuRs485.h"
#include "MenuRs485Def.h"
#include "MenuRs485Interface.h"
#include "Gadget/DMenu.h"         // Display menu
#include "Config/Config.h"        // Configuration
#include "Str.h"                  // Strings

// Local functions :

static DefMenu( Rs485Menu)
   STR_INTERFACE_1,
   STR_INTERFACE_2,
EndMenu()

typedef enum {
   MI_INTERFACE_1,
   MI_INTERFACE_2
} ERs485Menu;

//------------------------------------------------------------------------------
//  Menu Rs485
//------------------------------------------------------------------------------

void MenuRs485( void)
// Menu rs485
{
TMenuData MData;

   DMenuClear( MData);
   if(BAT2_HAS_RS485_I1_MODULE(Bat2Version.Modification) && BAT2_HAS_RS485_I2_MODULE(Bat2Version.Modification)){   // both modules
      if(!BAT2_HAS_RS485_I1_MODULE(Bat2Version.Modification)){
          MData.Mask |= (1 << MI_INTERFACE_1);
      }
      if(!BAT2_HAS_RS485_I2_MODULE(Bat2Version.Modification)){
          MData.Mask |= (1 << MI_INTERFACE_2);
      }
      forever {
         // selection :
         if( !DMenu( STR_RS485, Rs485Menu, 0, 0, &MData)){
            return;
         }
         MenuRs485Interface(MData.Item);
      }
   }
   else if(BAT2_HAS_RS485_I1_MODULE(Bat2Version.Modification)){  // only interface 1
      MenuRs485Interface(0);
   }
   else if (BAT2_HAS_RS485_I2_MODULE(Bat2Version.Modification)){
      MenuRs485Interface(1);  // only interface 2
   }

} // MenuRs485
