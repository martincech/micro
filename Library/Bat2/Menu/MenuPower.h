//******************************************************************************
//
//   MenuPower.h             Menu Power
//   Version 1.0             (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuPower_H__
   #define __MenuPower_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuPower( void);
// Menu Power

#endif
