//******************************************************************************
//                                                                            
//   Password.c     Password checking
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Password.h"
#include "Gadget/DInput.h"        // Display input value
#include "Gadget/DMsg.h"          // Display message
#include "Sound/Beep.h"           // Sounds
#include "Config/Config.h"        // Configuration
#include "Str.h"                  // Strings

#include <string.h>

#define PWD_FLAG_DISABLED  0x80        // trial expired
#define PWD_FLAG_ENABLED   0x40        // password already passed
#define PWD_FLAG_COUNT     0x0F        // trials counter
#define PASSWORD_TRIALS       5        // max. trials

static byte PasswordFlag;

#define PasswordDisable()    memset( Bat2Device.Password, 0, PASSWORD_SIZE)

//------------------------------------------------------------------------------
//  Check
//------------------------------------------------------------------------------

TYesNo PasswordCheck( void)
// Password checking, returns YES on passed
{
char Password[ PASSWORD_SIZE];

   if( PasswordInactive()){
      return( YES);                              // always passed
   }
   // check for password :
   if( PasswordFlag & PWD_FLAG_DISABLED){
      BeepError();                               // no trial
      return( NO);
   }
   if( PasswordFlag & PWD_FLAG_ENABLED){
      return( YES);                              // already passed
   }
   DInputPassword( STR_PASSWORD, STR_ENTER_PASSWORD, Password, PASSWORD_SIZE);
   if( memequ( Password, Bat2Device.Password, PASSWORD_SIZE)){
      PasswordFlag = PWD_FLAG_ENABLED;           // passed
      return( YES);
   }
   PasswordFlag++;
   if( (PasswordFlag & PWD_FLAG_COUNT) >= PASSWORD_TRIALS){
      PasswordFlag |= PWD_FLAG_DISABLED;         // trials expired
   }
   DMsgOk( STR_ERROR, STR_INVALID_PASSWORD, 0);
   return( NO);
} // PasswordCheck

//------------------------------------------------------------------------------
//  Edit
//------------------------------------------------------------------------------

TYesNo PasswordInactive( void)
// Password is inactive
{
   return( Bat2Device.Password[ 0] == 0);
} // PasswordInactive

//------------------------------------------------------------------------------
//  Edit
//------------------------------------------------------------------------------

void PasswordEdit( void)
// Edit a new password
{
char Password[ PASSWORD_SIZE];
char Password2[ PASSWORD_SIZE];

   // protect by password :
   if( !DMsgYesNo( STR_PASSWORD, STR_PROTECT_BY_PASSWORD, 0)){
      PasswordInactivate();
      return;
   }
   // input password :
   DInputPassword( STR_PASSWORD, STR_ENTER_NEW_PASSWORD, Password,  PASSWORD_SIZE);
   DInputPassword( STR_PASSWORD, STR_CONFIRM_PASSWORD,   Password2, PASSWORD_SIZE);
   if( !memequ( Password, Password2, PASSWORD_SIZE)){
      DMsgOk( STR_ERROR, STR_PASSWORDS_DONT_MATCH, 0);
      PasswordInactivate();
      return;
   }
   // save password
   memcpy( Bat2Device.Password, Password, PASSWORD_SIZE);
   ConfigDeviceSave();
   PasswordClear();                                        // activate now
} // PasswordEdit

//------------------------------------------------------------------------------
//  Inactivate
//------------------------------------------------------------------------------

void PasswordInactivate( void)
// Inactivate password checking
{
   PasswordDisable();
   ConfigDeviceSave();
} // PasswordInactivate

//------------------------------------------------------------------------------
//  Clear
//------------------------------------------------------------------------------

void PasswordClear( void)
// Clear password trials
{
   PasswordFlag = 0;
} // PasswordClear
