//******************************************************************************
//
//   MenuWeighingRelease.h  Weighing release menu
//   Version 1.0            (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuWeighingRelease_H__
   #define __MenuWeighingRelease_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuWeighingRelease( void);
// Menu weighing release

#endif
