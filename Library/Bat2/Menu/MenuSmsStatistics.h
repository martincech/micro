//******************************************************************************
//
//   MenuSmsStatistics.h     Show sms statistics on screen
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifdef __GSM_SMS_STATISTICS__
   #ifndef __MenuSmsStatistics_H__
   #define __MenuSmsStatistics_H__

   #ifndef __Hardware_H__
      #include "Hardware.h"
   #endif

   void MenuSmsStatistics( void);
   // Menu sms statistics

   #endif // __MenuSmsStatistics_H__
#endif


