//******************************************************************************
//
//   MenuDetection.c  Detection menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuDetection.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings

static DefMenu( DetectionMenu)
   STR_FILTER,
   STR_STABILIZATION_TIME,
   STR_STABILIZATION_RANGE,
   STR_STEP,
EndMenu()

typedef enum {
   MI_FILTER,
   MI_STABILIZATION_TIME,
   MI_STABILIZATION_RANGE,
   MI_STEP
} EDetectionMenu;

// Local functions :

static void WeighingDetectionParameters( int Index, int y, TWeighingDetection *Parameters);
// Draw detection parameters

//------------------------------------------------------------------------------
//  Menu Detection
//------------------------------------------------------------------------------

void MenuDetection( TWeighingDetection *Detection)
// Menu detection
{
TMenuData MData;
int       i;

   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_DETECTION, DetectionMenu, (TMenuItemCb *)WeighingDetectionParameters, Detection, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_FILTER :
            i = Detection->Filter;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 1, WEIGHING_DETECTION_FILTER_MIN, WEIGHING_DETECTION_FILTER_MAX, "s")){
               break;
            }
            Detection->Filter = (byte)i;
            break;

         case MI_STABILIZATION_TIME :
            i = Detection->StabilizationTime;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 1, WEIGHING_DETECTION_STABILIZATION_TIME_MIN, WEIGHING_DETECTION_STABILIZATION_TIME_MAX, "s")){
               break;
            }
            Detection->StabilizationTime = (byte)i;
            break;

         case MI_STABILIZATION_RANGE :
            i = Detection->StabilizationRange;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 1, WEIGHING_DETECTION_STABILIZATION_RANGE_MIN, WEIGHING_DETECTION_STABILIZATION_RANGE_MAX, "%")){
               break;
            }
            Detection->StabilizationRange = (byte)i;
            break;

         case MI_STEP :
            i = Detection->Step;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_STEP, _PLATFORM_STEP_LAST)){
               break;
            }
            Detection->Step = (byte)i;
            break;

      }
   }
} // MenuDetection

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void WeighingDetectionParameters( int Index, int y, TWeighingDetection *Parameters)
// Draw detection parameters
{
   switch( Index){
      case MI_FILTER :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%5.1f %s", Parameters->Filter, "s");
         break;

      case MI_STABILIZATION_TIME :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%5.1f %s", Parameters->StabilizationTime, "s");
         break;

      case MI_STABILIZATION_RANGE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%5.1f %s", Parameters->StabilizationRange, "%");
         break;

      case MI_STEP :
         DLabelEnum( Parameters->Step, ENUM_STEP, DMENU_PARAMETERS_X, y);
         break;

   }
} // WeighingDetectionParameters
