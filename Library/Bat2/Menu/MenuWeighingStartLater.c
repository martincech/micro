//******************************************************************************
//
//   MenuWeighingStartLater.c   Weighing start later menu
//   Version 1.0                (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWeighingStartLater.h"
#include "System/System.h"                // Clock only
#include "Gadget/DMsg.h"                  // Message box
#include "Gadget/DInput.h"                // Display input
#include "Scheduler/WeighingScheduler.h"  // Weighing scheduler executive
#include "Str.h"                          // Strings

#include "Weighing/Memory.h"
#include "Config/Config.h"                // Configuration

//------------------------------------------------------------------------------
//  Menu Weighing Start
//------------------------------------------------------------------------------

TYesNo MenuWeighingStartLater( void)
// Menu weighing start later
{
UDateTimeGauge Local;
UDateTime      DateTime;

   Local = SysDateTime();
   uDateTime( &DateTime, Local);
   forever {
      if( !DInputDate( STR_START_LATER, STR_ENTER_DATE, &DateTime.Date)){
         return NO;
      }
      if( !DInputTime( STR_START_LATER, STR_ENTER_TIME, &DateTime.Time)){
         return NO;
      }
      Local = uDateTimeGauge( &DateTime);
      if( Local <= SysDateTime()){
         DMsgOk( STR_START_LATER, STR_WRONG_DATE_TIME, 0);
         continue;
      }
      ConfigWeighingConfigurationSave();
      WeighingSchedulerStartAt( uDateTimeClock( Local));
      return YES;
   }
} // MenuWeighingStartLater
