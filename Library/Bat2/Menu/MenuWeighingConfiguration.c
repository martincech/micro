//******************************************************************************
//
//   MenuWeighingConfiguration.c  Weighing configuration menu
//   Version 1.0                  (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWeighingConfiguration.h"
#include "Graphic/Graphic.h"           // graphic
#include "Console/conio.h"             // Display
#include "Gadget/DLabel.h"             // Display label
#include "Gadget/DMenu.h"              // Display menu
#include "Gadget/DEdit.h"              // Display edit value
#include "Gadget/DTime.h"              // Display time
#include "Gadget/DInput.h"             // Input
#include "Weight/DWeight.h"            // Display edit weight
#include "Gadget/DMsg.h"               // Message box
#include "Curve/MenuGrowthCurve.h"     // Growth curve selection
#include "Curve/MenuCorrectionCurve.h" // Correction curve selection
#include "Str.h"                       // Strings

#include "Menu/MenuWeighingMenuSelection.h"
#include "Menu/MenuInitialWeights.h"
#include "Menu/MenuGrowthCurves.h"
#include "Menu/MenuDetection.h"
#include "Menu/MenuTargetWeights.h"
#include "Menu/MenuAcceptance.h"
#include "Statistic/MenuStatisticParameters.h"
#include "Predefined/MenuPredefinedWeighing.h"
#include "Menu/MenuWeighingStartLater.h"

#include "Weighing/WeighingConfiguration.h"
#include "Scheduler/WeighingScheduler.h"
#include "Predefined/PredefinedList.h"
#include "Curve/CurveList.h"
#include "Curve/CorrectionList.h"

#include "Scheduler/WeighingPlanList.h"   // Weighing plan list
#include "Scheduler/MenuWeighingPlans.h"  // Weighing plan selection

#include "Config/Config.h"                // Configuration
#include "Weighing/Memory.h"

static TCurveList CurveList;
static TCorrectionList CorrectionList;

static DefMenu( WeighingConfigurationMenu)
   STR_MENU_SELECTION,
   STR_TYPE,
   STR_FLOCK,
   STR_INITIAL_DAY,
   STR_INITIAL_WEIGHT,
   STR_INITIAL_WEIGHTS,
   STR_GROWTH_CURVE,
   STR_GROWTH_CURVES,
   STR_CORRECTION_CURVE,
   STR_DETECTION,
   STR_TARGET_WEIGHTS,
   STR_ACCEPTANCE,
   STR_STATISTICS,
   STR_DAY_START,
   STR_PLANNING,
   STR_START_NOW,
   STR_START_LATER,
EndMenu()

typedef enum {
   MI_MENU_SELECTION,
   MI_TYPE,
   MI_FLOCK,
   MI_INITIAL_DAY,
   MI_INITIAL_WEIGHT,
   MI_INITIAL_WEIGHTS,
   MI_GROWTH_CURVE,
   MI_GROWTH_CURVES,
   MI_CORRECTION_CURVE,
   MI_DETECTION,
   MI_TARGET_WEIGHTS,
   MI_ACCEPTANCE,
   MI_STATISTICS,
   MI_DAY_START,
   MI_PLANNING,
   MI_START_NOW,
   MI_START_LATER
} EWeighingConfigurationMenu;

// Local functions :

static void WeighingConfigurationParameters( int Index, int y, TWeighingConfiguration *Parameters);
// Draw weighing configuration parameters

static dword MenuMaskGet( TWeighingConfiguration *Configuration);
// Recalculate menu items mask by <Configuration>

static TYesNo _GrowthCurvesOk( TWeighingConfiguration *Configuration, TCurveList *CurveList);
// check growth curves when weighing by curves

//------------------------------------------------------------------------------
//  Menu WeighingConfiguration
//------------------------------------------------------------------------------

void MenuWeighingConfiguration( TUniStr Title, TWeighingConfiguration *Configuration, TYesNo Predefined, TPredefinedList *PredefinedList)
// Menu weighing configuration or <Predefined> weighing
{
TMenuData MData;
int       i;
char      Flock[ WEIGHING_CONFIGURATION_FLOCK_SIZE + 1];
UDateTime DateTime;
TPredefinedWeighingIdentifier Identifier;
TWeighingPlanIndex  WeighingPlanIndex;
TWeighingPlanList WeighingPlanList;
int Count;

   if( !CurveListOpen( &CurveList)) {
      return;
   }
   if( !CorrectionListOpen( &CorrectionList)) {
      return;
   }
   DMenuClear( MData);
   forever {
      //>>> item selection
      if( !Predefined){
         MData.Mask = MenuMaskGet( Configuration);
      } else {
         MData.Mask = (1 << MI_TYPE) | (1 << MI_START_NOW) | (1 << MI_START_LATER);
      }
      // check for sex differentiation  :
      switch( Configuration->TargetWeights.SexDifferentiation){
         case SEX_DIFFERENTIATION_YES :
            MData.Mask |= (1 << MI_INITIAL_WEIGHT)  | (1 << MI_GROWTH_CURVE);
            break;

         case SEX_DIFFERENTIATION_NO :
            MData.Mask |= (1 << MI_INITIAL_WEIGHTS) | (1 << MI_GROWTH_CURVES);
            break;

         case SEX_DIFFERENTIATION_STEP_ONLY :
            MData.Mask |= (1 << MI_INITIAL_WEIGHT)  | (1 << MI_GROWTH_CURVE)  ;
            MData.Mask |= (1 << MI_INITIAL_WEIGHTS) | (1 << MI_GROWTH_CURVES);
            MData.Mask |= (1 << MI_CORRECTION_CURVE);
            MData.Mask |= (1 << MI_ACCEPTANCE);
            break;
      }
      // check for prediction mode :
      if( Configuration->TargetWeights.Mode == PREDICTION_MODE_AUTOMATIC){
         MData.Mask |= (1 << MI_GROWTH_CURVE);   // hide growth curve selection
         MData.Mask |= (1 << MI_GROWTH_CURVES);
      }
      //<<< item selection
      // selection :
      if( !DMenu( Title, WeighingConfigurationMenu, (TMenuItemCb *)WeighingConfigurationParameters, Configuration, &MData)){
         CurveListClose( &CurveList);
         CorrectionListClose( &CorrectionList);
         return;
      }
      switch( MData.Item){
         case MI_MENU_SELECTION :
            if( !Predefined){
               break;
            }
            MenuWeighingMenuSelection( &Configuration->MenuMask);
            break;

         case MI_TYPE :
            if( Predefined){
               break;
            }
            Identifier = Configuration->Predefined;
            if( !MenuPredefinedWeighingSelect( PredefinedList, STR_TYPE, &Identifier, STR_CURVE_DISABLED)){
               break;
            }
            PredefinedListIdentifierLoad( PredefinedList, Configuration, Identifier);
            Configuration->Predefined = Identifier;
            break;

         case MI_FLOCK :
            strcpy( Flock, Configuration->Flock);
            if( !DInputText( STR_FLOCK, STR_FLOCK, Flock, WEIGHING_CONFIGURATION_FLOCK_SIZE, YES)){
               break;
            }
            strcpy( Configuration->Flock, Flock);
            break;

         case MI_INITIAL_DAY :
            i = Configuration->InitialDay;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, DAY_NUMBER_MIN, DAY_NUMBER_MAX, 0)){
               break;
            }
            Configuration->InitialDay = (TDayNumber)i;
            break;

         case MI_INITIAL_WEIGHT :
            i = Configuration->Male.InitialWeight;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            Configuration->Male.InitialWeight = (TWeightGauge)i;
            break;

         case MI_INITIAL_WEIGHTS :
            MenuInitialWeights( Configuration);
            break;

         case MI_GROWTH_CURVE :
            MenuGrowthCurveSelect( &CurveList, STR_GROWTH_CURVE, &Configuration->Male.GrowthCurve, 0);
            break;

         case MI_GROWTH_CURVES :
            MenuGrowthCurves( Configuration, &CurveList);
            break;

         case MI_CORRECTION_CURVE :
            MenuCorrectionCurveSelect( &CorrectionList, STR_CORRECTION_CURVE, &Configuration->CorrectionCurve, STR_CURVE_DISABLED);
            break;

         case MI_DETECTION :
            MenuDetection( &Configuration->Detection);
            break;

         case MI_TARGET_WEIGHTS :
            MenuTargetWeights( &Configuration->TargetWeights);
            break;

         case MI_ACCEPTANCE :
            MenuAcceptance( Configuration);
            break;

         case MI_STATISTICS :
            MenuStatisticParameters( &Configuration->Statistic);
            break;

         case MI_DAY_START :
            uTime( &DateTime.Time, Configuration->DayStart);
            if( !DInputTime( STR_DAY_START, STR_DAY_START, &DateTime.Time)){
               break;
            }
            Configuration->DayStart = uTimeGauge( &DateTime.Time);
            break;
         case MI_PLANNING :
            while( !WeighingPlanListOpen( &WeighingPlanList));
            Count = WeighingPlanListCount( &WeighingPlanList);

            if( Configuration->Planning){
               WeighingPlanIndex = Configuration->WeighingPlan;
            } else {
               WeighingPlanIndex = Count;
            }
            WeighingPlanIndex = MenuWeighingPlansSelect( &WeighingPlanList, STR_WEIGHING_PLAN, WeighingPlanIndex, STR_CURVE_DISABLED);
            WeighingPlanListClose( &WeighingPlanList);
            // Cancel
            if( WeighingPlanIndex == WEIGHING_PLAN_INDEX_INVALID){
               break;
            }
            // special item(disable) selected
            if( WeighingPlanIndex >= Count){
               Configuration->Planning = NO;
               break;
            }
            // new plan selected
            Configuration->Planning = YES;
            Configuration->WeighingPlan = WeighingPlanIndex;
            break;

         case MI_START_NOW :
            if( Predefined){
               break;
            }
            if( !_GrowthCurvesOk( Configuration, &CurveList)){
               break;
            }

            if( !DMsgYesNo( STR_CONFIRMATION, STR_WEIGHING_START_CONFIRM, 0)){
               break;
            }
            DMsgWait();
            ConfigWeighingConfigurationSave();
            CurveListClose( &CurveList);
            CorrectionListClose( &CorrectionList);
            WeighingSchedulerStart();
            return;

         case MI_START_LATER :
            if( Predefined){
               break;
            }
            if( !_GrowthCurvesOk( Configuration, &CurveList)){
               break;
            }
            CurveListClose( &CurveList);
            CorrectionListClose( &CorrectionList);
            if(MenuWeighingStartLater()) {
               return;
            }
            break;

      }
   }
} // MenuWeighingConfiguration

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void WeighingConfigurationParameters( int Index, int y, TWeighingConfiguration *Parameters)
// Draw weighing configuration parameters
{
char Name[ UDIRECTORY_NAME_SIZE + 1];
TWeighingPlan WeighingPlan;
TWeighingPlanList WeighingPlanList;

   switch( Index){
      case MI_MENU_SELECTION :
         break;

      case MI_TYPE :
         if( Parameters->Predefined == ULIST_IDENTIFIER_INVALID){
            DLabelNarrow( STR_KIND_UNKNOWN, DMENU_PARAMETERS_X, y);
         } else {
            DLabelNarrow( Parameters->Name, DMENU_PARAMETERS_X, y);
         }
         break;

      case MI_FLOCK :
         DLabelNarrow( Parameters->Flock, DMENU_PARAMETERS_X, y);
         break;

      case MI_INITIAL_DAY :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->InitialDay, 0);
         break;

      case MI_INITIAL_WEIGHT :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->Male.InitialWeight);
         break;

      case MI_INITIAL_WEIGHTS :
         break;

      case MI_GROWTH_CURVE :
         CurveListName( &CurveList, Parameters->Male.GrowthCurve, Name);
         DLabelNarrow( Name, DMENU_PARAMETERS_X, y);
         break;

      case MI_GROWTH_CURVES :
         break;

      case MI_CORRECTION_CURVE :
         if( !CorrectionListName( &CorrectionList, Parameters->CorrectionCurve, Name)){
            DLabelNarrow( STR_CURVE_DISABLED, DMENU_PARAMETERS_X, y);
            break;
         }
         DLabelNarrow( Name, DMENU_PARAMETERS_X, y);
         break;

      case MI_DETECTION :
         break;

      case MI_TARGET_WEIGHTS :
         break;

      case MI_ACCEPTANCE :
         break;

      case MI_STATISTICS :
         break;

      case MI_DAY_START :
         DTimeShortRight( Parameters->DayStart, DMENU_PARAMETERS_X, y);
         break;

      case MI_PLANNING :
         if( !Parameters->Planning){
            DLabelNarrow( STR_CURVE_DISABLED, DMENU_PARAMETERS_X, y);
            break;
         }
         while( !WeighingPlanListOpen( &WeighingPlanList));
         WeighingPlanListLoad( &WeighingPlanList, &WeighingPlan, Parameters->WeighingPlan);
         WeighingPlanListClose( &WeighingPlanList);
         DLabelNarrow( WeighingPlan.Name, DMENU_PARAMETERS_X, y);
         break;

      case MI_START_NOW :
         break;

      case MI_START_LATER :
         break;

   }
} // WeighingConfigurationParameters

//------------------------------------------------------------------------------
//   Menu mask
//------------------------------------------------------------------------------

static dword MenuMaskGet( TWeighingConfiguration *Configuration)
// Recalculate menu items mask by <Configuration>
{
dword Mask;

   Mask = 1 << MI_MENU_SELECTION;
   if( Configuration->MenuMask & WCMM_TYPE){
      Mask |= 1 << MI_TYPE;
   }
   if( Configuration->MenuMask & WCMM_FLOCK){
      Mask |= 1 << MI_FLOCK;
   }
   if( Configuration->MenuMask & WCMM_INITIAL_DAY){
      Mask |= 1 << MI_INITIAL_DAY;
   }
   if( Configuration->MenuMask & WCMM_INITIAL_WEIGHT){
      Mask |= 1 << MI_INITIAL_WEIGHT;
      Mask |= 1 << MI_INITIAL_WEIGHTS;
   }
   if( Configuration->MenuMask & WCMM_GROWTH_CURVE){
      Mask |= 1 << MI_GROWTH_CURVE;
      Mask |= 1 << MI_GROWTH_CURVES;
   }
   if( Configuration->MenuMask & WCMM_CORRECTION_CURVE){
      Mask |= 1 << MI_CORRECTION_CURVE;
   }
   if( Configuration->MenuMask & WCMM_DETECTION){
      Mask |= 1 << MI_DETECTION;
   }
   if( Configuration->MenuMask & WCMM_TARGET_WEIGHTS){
      Mask |= 1 << MI_TARGET_WEIGHTS;
   }
   if( Configuration->MenuMask & WCMM_ACCEPTANCE){
      Mask |= 1 << MI_ACCEPTANCE;
   }
   if( Configuration->MenuMask & WCMM_STATISTICS){
      Mask |= 1 << MI_STATISTICS;
   }
   if( Configuration->MenuMask & WCMM_DAY_START){
      Mask |= 1 << MI_DAY_START;
   }
   if( Configuration->MenuMask & WCMM_PLANNING){
      Mask |= 1 << MI_PLANNING;
   }
   if( Configuration->MenuMask & WCMM_START_NOW){
      Mask |= 1 << MI_START_NOW;
   }
   if( Configuration->MenuMask & WCMM_START_LATER){
      Mask |= 1 << MI_START_LATER;
   }
   return( Mask);
} // MenuMaskGet

//------------------------------------------------------------------------------
//  Parameter check
//------------------------------------------------------------------------------

static TYesNo _GrowthCurvesOk( TWeighingConfiguration *Configuration, TCurveList *CurveList)
// check growth curves when weighing by curves
{

   if( Configuration->TargetWeights.Mode == PREDICTION_MODE_GROWTH_CURVE){
      // check growth curves validity
      if( Configuration->Male.GrowthCurve == ULIST_IDENTIFIER_INVALID ||
          CurveListIndex( CurveList, WeighingConfiguration.Male.GrowthCurve) == UDIRECTORY_INDEX_INVALID ||
          (( Configuration->TargetWeights.SexDifferentiation == SEX_DIFFERENTIATION_YES) &&
           (Configuration->Female.GrowthCurve == ULIST_IDENTIFIER_INVALID ||
            CurveListIndex( CurveList, WeighingConfiguration.Female.GrowthCurve) == UDIRECTORY_INDEX_INVALID))){
         DMsgOk(STR_WARNING, STR_NO_CURVES, 0);
         return NO;
      }
   }

   return YES;
}
