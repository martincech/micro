//******************************************************************************
//
//   MenuPowerFailure.h    Menu power failure
//   Version 1.0           (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuPowerFailure_H__
   #define __MenuPowerFailure_H__

#ifndef __Menu_H__
   #include "Menu/Menu.h"
#endif

typedef enum {
   MENU_POWER_FAILURE_EXIT_POWER_OK = _MENU_EXIT_LAST
} EMenuRemoteExit;

void MenuPowerFailure( void);
// Menu power failure

#endif
