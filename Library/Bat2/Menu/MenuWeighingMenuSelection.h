//******************************************************************************
//
//   MenuWeighingMenuSelection.h  Weighing menu configuration selector
//   Version 1.0                  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuWeighingMenuSelection_H__
   #define __MenuWeighingMenuSelection_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuWeighingMenuSelection( dword *MenuMask);
// Weighing configuration menu items selector

#endif
