//******************************************************************************
//
//   Menu.c        Bat2 menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "Menu.h"
#include "System/System.h"                  // Operating system
#include "Graphic/Graphic.h"                // Graphic
#include "Gadget/DEvent.h"                  // Display events
#include "Gadget/DMsg.h"                    // Message box
#include "Sound/Beep.h"                     // Sound
#include "Menu/MenuMain.h"                  // Main menu
#include "Menu/Screen.h"                    // Basic screen
#include "Menu/ScreenStatistic.h"           // Statistic screen
#include "Scheduler/WeighingScheduler.h"    // Weighing scheduler executive
#include "Menu/Password.h"
#include "Menu/MenuDateTime.h"
#include "Memory/Nvm.h"
#include "Config/Context.h"
#include "Config/Config.h"
#include "Bitmap.h"
#include "Str.h"                       // Strings
#include "Gui.h"

static ESex MainScreenSex;
//------------------------------------------------------------------------------
//   Main loop
//------------------------------------------------------------------------------

void MenuExecute( void)
// Main menu loop
{
   MenuContext.SwitchedOff = NO;
   ContextMenuContextSave();
   PasswordClear();                    // Clear password trials
   if(SysDateTimeInvalid()) {          // set date&time if invalid
      DMsgOk( STR_DATE_AND_TIME, STR_SET_DATE_TIME, STR_CHECK_RTC_BATTERY);
      MenuDateTime();
   }
   MainScreenSex = SEX_MALE;
   ScreenChangeAndRedrawBySex(MainScreenSex,  YES);

   forever {
      DEventDiscard();                 // stop timeout chain
      switch( GuiEventWait()){
         case K_DOWN :
            break;

         case K_UP :
            MainScreenSex = ScreenStatisticForSex(MainScreenSex);
            ScreenChangeAndRedrawBySex(MainScreenSex, YES);
            break;

         case K_ENTER :
            BeepKey();
            MenuMain();                // execute main menu
            SysFlashReset();           // restart flashing
            ScreenChangeAndRedrawBySex(MainScreenSex, YES);
            break;

         case K_RIGHT :
            if(MainScreenSex != SEX_FEMALE){
               MainScreenSex = SEX_FEMALE;
               ScreenChangeAndRedrawBySex( MainScreenSex, YES);
            }
            break;

         case K_LEFT :
            if(MainScreenSex != SEX_MALE){
               MainScreenSex = SEX_MALE;
               ScreenChangeAndRedrawBySex( MainScreenSex, YES);
            }
            break;

         case K_ESC :
            break;

         case K_ESC | K_REPEAT :
            if(!DMsgOkCancel(STR_POWER_OFF, STR_REALLY_POWER_OFF, 0)) {
               ScreenChangeAndRedrawBySex(MainScreenSex,  YES);
               break;
            }
            DMsgWait();
            MenuContext.SwitchedOff = YES;
            ContextMenuContextSave();
            ConfigSave();
            MenuExitSet(MENU_EXIT_POWER_OFF);
            break;

         case K_FLASH1 :
            ScreenChangeAndRedrawBySex(MainScreenSex,  YES);
            break;

         case K_FLASH2 :
            break;

         case K_SHUTDOWN :
            return;
      }
      GFlush();
   }
} // MenuExecute
