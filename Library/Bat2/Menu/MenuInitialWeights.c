//******************************************************************************
//
//   MenuInitialWeights.c  Initial weights menu
//   Version 1.0           (c) VEIT Electronics
//
//******************************************************************************

#include "MenuInitialWeights.h"
#include "Gadget/DMenu.h"         // Display menu
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings


static DefMenu( InitialWeightsMenu)
   STR_MALES,
   STR_FEMALES,
EndMenu()

typedef enum {
   MI_MALES,
   MI_FEMALES
} EInitialWeightsMenu;

// Local functions :

static void WeighingInitialWeightsParameters( int Index, int y, TWeighingConfiguration *Parameters);
// Draw initial weights parameters

//------------------------------------------------------------------------------
//  Menu Initial Weights
//------------------------------------------------------------------------------

void MenuInitialWeights( TWeighingConfiguration *Configuration)
// Menu initial weights
{
TMenuData MData;
int       i;

   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_INITIAL_WEIGHTS, InitialWeightsMenu, (TMenuItemCb *)WeighingInitialWeightsParameters, Configuration, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_MALES :
            i = Configuration->Male.InitialWeight;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            Configuration->Male.InitialWeight = (TWeightGauge)i;
            break;

         case MI_FEMALES :
            i = Configuration->Female.InitialWeight;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            Configuration->Female.InitialWeight = (TWeightGauge)i;
            break;

      }
   }
} // MenuInitialWeights

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void WeighingInitialWeightsParameters( int Index, int y, TWeighingConfiguration *Parameters)
// Draw initial weights parameters
{
   switch( Index){
      case MI_MALES :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->Male.InitialWeight);
         break;

      case MI_FEMALES :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->Female.InitialWeight);
         break;

   }
} // WeighingInitialWeightsParameters
