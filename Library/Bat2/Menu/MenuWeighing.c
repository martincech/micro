//******************************************************************************
//
//   MenuWeighing.c  Weighing menu
//   Version 1.1     (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWeighing.h"
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DMsg.h"
#include "Str.h"                  // Strings

#include "Menu/MenuWeighingStart.h"
#include "Menu/MenuWeighingStop.h"
#include "Menu/MenuWeighingSuspend.h"
#include "Menu/MenuWeighingRelease.h"
#include "Menu/MenuWeighingTargetAdjust.h"

#include "Weighing/WeighingConfiguration.h"

#include "Weighing/Memory.h"
#include "Scheduler/WeighingScheduler.h"

static DefMenu( WeighingMenu)
   STR_START,
   STR_STOP,
   STR_SUSPEND,
   STR_RELEASE,
   STR_DIAGNOSTICS_START,
   STR_ADJUST_TARGET_WEIGHTS,
EndMenu()

typedef enum {
   MI_START,
   MI_STOP,
   MI_SUSPEND,
   MI_RELEASE,
   MI_DIAGNOSTICS_START,
   MI_TARGET_WEIGHTS_ADJUSTMENT
} EWeighingMenu;

//------------------------------------------------------------------------------
//  Menu Weighing
//------------------------------------------------------------------------------

void MenuWeighing( void)
// Menu weighing
{
TMenuData MData;

   forever {
      DMenuClear( MData);
      //>>> enable menu items :
      MData.Mask = 0;
      switch( WeighingStatus()){
         case WEIGHING_STATUS_UNDEFINED :
         case WEIGHING_STATUS_STOPPED :
            // enable start
            MData.Mask = (1 << MI_STOP) | (1 << MI_SUSPEND) | (1 << MI_RELEASE) | (1 << MI_TARGET_WEIGHTS_ADJUSTMENT);
            break;

         case WEIGHING_STATUS_WEIGHING :
         case WEIGHING_STATUS_RELEASED :
            // weighing active - enable stop & suspend
            MData.Mask = (1 << MI_START) | (1 << MI_RELEASE) | (1 << MI_DIAGNOSTICS_START);
            break;

         case WEIGHING_STATUS_SLEEP :
         case WEIGHING_STATUS_SUSPENDED :
            // weighing suspended - enable stop & release
            MData.Mask = (1 << MI_START) | (1 << MI_SUSPEND) | (1 << MI_DIAGNOSTICS_START);
            break;

         case WEIGHING_STATUS_WAIT :
            // wait for start - enable stop only
            MData.Mask = (1 << MI_START) | (1 << MI_SUSPEND) | (1 << MI_RELEASE) | (1 << MI_DIAGNOSTICS_START) | (1 << MI_TARGET_WEIGHTS_ADJUSTMENT);
            break;

         case WEIGHING_STATUS_CALIBRATION :
         case WEIGHING_STATUS_DIAGNOSTICS :
            // special mode - enable stop only
            MData.Mask = (1 << MI_START) | (1 << MI_SUSPEND) | (1 << MI_RELEASE) | (1 << MI_DIAGNOSTICS_START) | (1 << MI_TARGET_WEIGHTS_ADJUSTMENT);
            break;
      }
      MData.Mask |= (1 << MI_DIAGNOSTICS_START); // diagnostics always disabled
      if( WeighingConfiguration.TargetWeights.SexDifferentiation == SEX_DIFFERENTIATION_STEP_ONLY ||
          WeighingConfiguration.TargetWeights.AdjustTargetWeights == ADJUST_TARGET_WEIGHTS_NONE){
         MData.Mask |= (1 << MI_TARGET_WEIGHTS_ADJUSTMENT);
      }
      //<<< enable menu items :
      // selection :
      if( !DMenu( STR_WEIGHING, WeighingMenu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_START :
            MenuWeighingStart();
            break;

         case MI_STOP :
            MenuWeighingStop();
            break;

         case MI_SUSPEND :
            MenuWeighingSuspend();
            break;

         case MI_RELEASE :
            MenuWeighingRelease();
            break;

         case MI_DIAGNOSTICS_START:
            if( !DMsgYesNo( STR_CONFIRMATION, STR_WEIGHING_START_CONFIRM, 0)){
               break;
            }
            DMsgWait();
            /*if(!MemoryTest()) {
               //DMsgOk( STR_DIAGNOSTICS, STR_UNABLE_WRITE_USB, 0);
               break;
            }
            DMsgWait();*/
            WeighingSchedulerDiagnostics();
            break;

         case MI_TARGET_WEIGHTS_ADJUSTMENT :
            MenuWeighingTargetAdjust();
            break;
      }
   }
} // MenuWeighing
