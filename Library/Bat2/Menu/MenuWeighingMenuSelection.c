//******************************************************************************
//
//   MenuWeighingMenuSelection.c  Weighing menu configuration selector
//   Version 1.0                  (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWeighingMenuSelection.h"
#include "Gadget/DCheckList.h"         // Display checklist
#include "Str.h"                       // Strings


static DefCheckList( ConfigurationSelectionList)
   STR_FLOCK,
   STR_INITIAL_DAY,
   STR_INITIAL_WEIGHT,
   STR_GROWTH_CURVE,
   STR_CORRECTION_CURVE,
   STR_DETECTION,
   STR_TARGET_WEIGHTS,
   STR_ACCEPTANCE,
   STR_STATISTICS,
   STR_DAY_START,
   STR_PLANNING,
   STR_START_NOW,
   STR_START_LATER,
// STR_KIND,
EndCheckList()

//------------------------------------------------------------------------------
//  Menu weighing menu configuration selector
//------------------------------------------------------------------------------

void MenuWeighingMenuSelection( dword *MenuMask)
// Weighing configuration menu items selector
{
dword Mask;

   Mask = ~(*MenuMask);                // convert disabled items to enabled
   DCheckList( STR_MENU_SELECTION, ConfigurationSelectionList, &Mask);
   *MenuMask = ~Mask;                  // convert enabled items do disabled
} // MenuWeighingMenuSelection
