//******************************************************************************
//
//   MenuWeighingStop.c  Weighing Stop menu
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWeighingStop.h"
#include "Gadget/DMsg.h"          // Message box
#include "Str.h"                  // Strings

#include "Scheduler/WeighingScheduler.h"  // Weighing scheduler executive

//------------------------------------------------------------------------------
//  Menu Weighing Stop
//------------------------------------------------------------------------------

void MenuWeighingStop( void)
// Menu weighing stop
{
   if( !DMsgYesNo( STR_CONFIRMATION, STR_WEIGHING_STOP_CONFIRM, 0)){
      return;
   }
   DMsgWait();
   WeighingSchedulerStop();
} // MenuWeighingStop
