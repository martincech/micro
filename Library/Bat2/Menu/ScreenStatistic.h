//******************************************************************************
//
//   ScreenStatistic.h Bat2 statistic screen
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ScreenStatistic_H__
   #define __ScreenStatistic_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

ESex ScreenStatistic();
// Display statistic & histogram, return sex of the last screen

ESex ScreenStatisticForSex(ESex sex);
// Display statistic & histogram for specific sex as default screen

#endif
