//******************************************************************************
//
//   MenuDateTime.h      Date time settings
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#include "MenuDateTime.h"
#include "System/System.h"        // Operating system
#include "Gadget/DTime.h"         // Display time
#include "Gadget/DInput.h"        // Input box
#include "Str.h"                  // Strings

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuDateTime( void)
// Menu date time
{
UDateTimeGauge Local;
UDateTime      DateTime;

   Local = SysDateTime();
   uDateTime( &DateTime, Local);
   if( !DInputDate( STR_DATE_AND_TIME, STR_ENTER_DATE, &DateTime.Date)){
      return;
   }
   if( !DInputTime( STR_DATE_AND_TIME, STR_ENTER_TIME, &DateTime.Time)){
      return;
   }
   Local = uDateTimeGauge( &DateTime);
   SysDateTimeSet( Local);
} // MenuDateTime
