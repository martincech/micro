//******************************************************************************
//
//   MenuWeighingStop.h  Weighing Stop menu
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuWeighingStop_H__
   #define __MenuWeighingStop_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuWeighingStop( void);
// Menu weighing stop

#endif
