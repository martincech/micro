//******************************************************************************
//
//   MenuWeighingSuspend.h  Weighing suspend menu
//   Version 1.0            (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuWeighingSuspend_H__
   #define __MenuWeighingSuspend_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuWeighingSuspend( void);
// Menu weighing suspend

#endif
