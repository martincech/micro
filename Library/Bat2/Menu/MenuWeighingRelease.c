//******************************************************************************
//
//   MenuWeighingRelease.c  Weighing Release menu
//   Version 1.0            (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWeighingRelease.h"
#include "Gadget/DMsg.h"          // Message box
#include "Str.h"                  // Strings

#include "Scheduler/WeighingScheduler.h"  // Weighing scheduler executive

//------------------------------------------------------------------------------
//  Menu Weighing Release
//------------------------------------------------------------------------------

void MenuWeighingRelease( void)
// Menu weighing release
{
   if( !DMsgYesNo( STR_CONFIRMATION, STR_WEIGHING_RELEASE_CONFIRM, 0)){
      return;
   }
   DMsgWait();
   WeighingSchedulerRelease();
} // MenuWeighingRelease
