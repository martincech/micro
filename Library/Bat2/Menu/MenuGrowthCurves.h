//******************************************************************************
//
//   MenuGrowthCurves.h  Growth curves menu
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuGrowthCurves_H__
   #define __MenuGrowthCurves_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeighingConfiguration_H__
   #include "Weighing/WeighingConfiguration.h"
#endif

#ifndef __CurveList_H__
   #include "Curve/CurveList.h"
#endif

void MenuGrowthCurves( TWeighingConfiguration *Configuration, TCurveList *CurveList);
// Menu growth curves

#endif
