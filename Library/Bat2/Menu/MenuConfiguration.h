//******************************************************************************
//
//   MenuConfiguration.h  Configuration menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuConfiguration_H__
   #define __MenuConfiguration_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Menu_H__
   #include "Menu.h"
#endif


void MenuConfiguration( void);
// Menu configuration

#endif
