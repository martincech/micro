//******************************************************************************
//
//   MenuPlatformInfo.c  Platform info screen
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#include "MenuPlatformInfo.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLayout.h"       // screen layout
#include "Gadget/DEvent.h"        // Display events
#include "Weight/DWeight.h"       // Display weight
#include "Weight/Weight.h"       // Display weight
#include "Bitmap.h"               // Bitmaps
#include "Fonts.h"                // Project fonts
#include "Str.h"                  // Strings
#include "Gadget/DLabel.h"        // Display label

#include "Weighing/WeighingConfiguration.h"
#include "Platform/Zone.h"

#define INFO_X  3
#define INFO_Y  19
#define CHAR_H  17

//------------------------------------------------------------------------------
//  Menu Platform Info
//------------------------------------------------------------------------------

void MenuPlatformInfo( void)
// Menu platform info
{
int y;
TYesNo Redraw = YES;
   forever {
      if( Redraw){
         GClear();
         // frame :
         DLayoutTitle( "Platform");
         DLayoutStatus( STR_BTN_CANCEL, 0, 0);
         // info :
         SetFont( TAHOMA16);
         //--------------------------------------------------------------------------
         y = INFO_Y;
         GTextAt( INFO_X, y);
         cprintf( "ADC : %d,%d,%d,%d", PlatformSigmaDelta.Chop, PlatformSigmaDelta.Filter,
                                       PlatformSigmaDelta.Rate, PlatformSigmaDelta.Prefilter);
         y += CHAR_H;
         GTextAt( INFO_X, y);
         cprintf( "DetectF: %3.1f,%3.1f,", PlatformDetection.Fine.AveragingWindow, PlatformDetection.Fine.StableWindow);
                  DWeightLeft( PlatformDetection.Fine.AbsoluteRange);
                  cputch( ',');
                  DWeightLeft( PlatformDetection.Fine.SwitchoverRange);
         y += CHAR_H;
         GTextAt( INFO_X, y);
         cprintf( "DetectC: %3.1f,%3.1f,", PlatformDetection.Coarse.AveragingWindow, PlatformDetection.Coarse.StableWindow);
                  DWeightLeft( PlatformDetection.Coarse.AbsoluteRange);
                  cputch( ',');
                  DWeightLeft( PlatformDetection.Coarse.SwitchoverRange);
                  y += CHAR_H;
         GTextAt( INFO_X, y);
         cprintf( "Accept : %s/%s", ENUM_ACCEPTANCE_MODE + PlatformAcceptance.Mode,
                                    ENUM_STEP + PlatformAcceptance.Step);
         y += CHAR_H;
         if( PlatformAcceptance.Mode != PLATFORM_ACCEPTANCE_MODE_STEP){
            GTextAt( INFO_X, y);
            cputs(   "     M :");
            DWeightWithUnits( PlatformAcceptance.LowLimit);
            cputch( ',');
            DWeightWithUnits( PlatformAcceptance.HighLimit);
            y += CHAR_H;
         }
         if( PlatformAcceptance.Mode == PLATFORM_ACCEPTANCE_MODE_MIXED){
            GTextAt( INFO_X, y);
            cputs(   "     F :");
            DWeightWithUnits( PlatformAcceptance.LowLimitFemale);
            cputch( ',');
            DWeightWithUnits( PlatformAcceptance.HighLimitFemale);
            y += CHAR_H;
         }
         GTextAt( INFO_X, y);
         cprintf( "Cal    : %d,%d,", PlatformCalibration.Delay, PlatformCalibration.Duration);
         y += CHAR_H;

         //DWeightWithUnits( PlatformCalibration.FullRange);
         //--------------------------------------------------------------------------
         GFlush();
         Redraw = NO;
      }
      switch( DEventWait()){
         case K_FLASH1 :
            Redraw = YES;
            break;

         case K_ENTER :
         case K_ESC :
         case K_TIMEOUT :
            return;
      }
   }
} // MenuPlatformInfo
