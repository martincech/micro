//******************************************************************************
//
//   MenuCommunication.h  Communication menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuCommunication_H__
   #define __MenuCommunication_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Menu_H__
   #include "Menu.h"
#endif


void MenuCommunication( void);
// Menu communication

#endif
