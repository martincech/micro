//******************************************************************************
//
//   MenuUserSettings.c  User settings menu
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#include "MenuUserSettings.h"
#include "System/System.h"        // Clock only
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DTime.h"         // Display time
#include "Gadget/DInput.h"        // Input box
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration
#include "Display/DisplayConfiguration.h"

#include "Display/MenuContrast.h"              // Menu contrast intensity
#include "Display/MenuBacklightIntensity.h"    // Menu backlight intensity

#include "Menu/MenuDateTime.h"

static DefMenu( UserSettingsMenu)
   STR_DISPLAY_BACKLIGHT,
   STR_DISPLAY_CONTRAST,
   STR_DATE_AND_TIME,
EndMenu()

typedef enum {
   MI_DISPLAY_BACKLIGHT,
   MI_DISPLAY_CONTRAST,
   MI_DATE_AND_TIME
} EUserSettingsMenu;

// Local functions :

static void UserSettingsParameters( int Index, int y, void *UserData);
// Draw maintenance detection parameters

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuUserSettings( void)
// Settings menu
{
TMenuData      MData;

   DMenuClear( MData);
   forever {
      if( !DMenu( STR_USER_SETTINGS, UserSettingsMenu, UserSettingsParameters, 0, &MData)){
         ConfigDisplayConfigurationSave();
         return;
      }
      switch( MData.Item){
         case MI_DISPLAY_BACKLIGHT :
            MenuBacklightIntensity( MData.y);
            break;

         case MI_DISPLAY_CONTRAST :
            MenuContrast( MData.y);
            break;

         case MI_DATE_AND_TIME :
            MenuDateTime();
            break;
      }
   }
} // MenuUserSettings

//------------------------------------------------------------------------------
//   Parameters
//------------------------------------------------------------------------------

static void UserSettingsParameters( int Index, int y, void *UserData)
// Draw maintenance detection parameters
{
   switch( Index){
      case MI_DISPLAY_BACKLIGHT :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", DisplayConfiguration.Backlight.Intensity);
         break;

      case MI_DISPLAY_CONTRAST :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", DisplayConfiguration.Contrast);
         break;

      case MI_DATE_AND_TIME :
         break;
   }
} // UserSettingsParameters
