//******************************************************************************
//
//   MenuConfiguration.c  Configuration menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuConfiguration.h"
#include "Gadget/DMenu.h"         // Display menu
#include "Str.h"                  // Strings

#include "Menu.h"
#include "MenuConfigurationWeighing.h"
#include "Message/MenuGsm.h"
#include "Communication/MenuDataPublication.h"
#include "Display/MenuDisplay.h"
#include "Device/VersionDef.h"
#include "Usb/MenuUsb.h"

static DefMenu( ConfigurationMenu)
   STR_WEIGHING,
   STR_CELLULAR,
   STR_DATA_PUBLICATION,
   STR_DISPLAY,
   STR_USB,
EndMenu()

typedef enum {
   MI_WEIGHING,
   MI_CELLULAR,
   MI_DATA_PUBLICATION,
   MI_DISPLAY,
   MI_USB
} EConfigurationMenu;

//------------------------------------------------------------------------------
//  Menu Configuration
//------------------------------------------------------------------------------

void MenuConfiguration( void)
// Menu configuration
{
TMenuData MData;

   DMenuClear( MData);
   if(!BAT2_HAS_INTERNET_CONNECTION(Bat2Version.Modification) && !BAT2_HAS_GSM_MODULE(Bat2Version.Modification)){
      MData.Mask = (1 << MI_DATA_PUBLICATION);
   }

   forever {
      // selection :
      if( !DMenu( STR_CONFIGURATION, ConfigurationMenu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_WEIGHING :
            MenuConfigurationWeighing();
            break;

         case MI_CELLULAR :
            MenuGsm();
            break;

         case MI_DATA_PUBLICATION :
            MenuDataPublication();
            break;

         case MI_DISPLAY :
            MenuDisplay();
            break;
         case MI_USB :
            MenuUsb();
            break;
      }
   }
} // MenuConfiguration
