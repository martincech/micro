//******************************************************************************
//
//   MenuMain.c   Main menu
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "MenuMain.h"
#include "Gadget/DMenu.h"         // Display menu
#include "Str.h"                  // Strings

#include "Menu/MenuWeighing.h"
#include "Menu/MenuStatistics.h"
#include "Menu/MenuUserSettings.h"
#include "Menu/MenuConfiguration.h"
#include "Menu/MenuMaintenance.h"


static DefMenu( MainMenu)
   STR_WEIGHING,
   STR_STATISTICS,
   STR_USER_SETTINGS,
   STR_CONFIGURATION,
   STR_MAINTENANCE,
EndMenu()

typedef enum {
   MI_WEIGHING,
   MI_STATISTICS,
   MI_USER_SETTINGS,
   MI_CONFIGURATION,
   MI_MAINTENANCE
} EMainMenu;

//------------------------------------------------------------------------------
//  Menu Main
//------------------------------------------------------------------------------

void MenuMain( void)
// Menu main
{
TMenuData MData;

   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_MAIN, MainMenu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_WEIGHING :
            MenuWeighing();
            break;

         case MI_STATISTICS :
            MenuStatistics();
            break;

         case MI_USER_SETTINGS :
            MenuUserSettings();
            break;

         case MI_CONFIGURATION :
            MenuConfiguration();
            break;

         case MI_MAINTENANCE :
            MenuMaintenance();
            break;

      }
   }
} // MenuMain
