//*****************************************************************************
//
//    Dacs.cpp       Serial port Dacs communication dummz implementation
//    Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#include "Dacs/Dacs.h"
#include "Debug/uDebug.h"
//------------------------------------------------------------------------------
//  Dacs module definitions
//------------------------------------------------------------------------------
TDacsReply Buffer;
static EDacsStatus _DacsStatus;

//------------------------------------------------------------------------------
//   Initialize
//------------------------------------------------------------------------------

void DacsInit( int uartPort)
// Initialize
{
   _DacsStatus = DACS_IDLE;
   TRACE_INIT();
   TRACE( "DACS : Init");
}

void DacsDeinit( void)
// Deinitialize
{
   TRACE_INIT();
   TRACE( "DACS : Deinit");
}

TDacsStatus DacsStatus( void)
// Check for last operation status
{
   return _DacsStatus;
}

void DacsListenStart( void)
// Starts listening on port
{
   _DacsStatus = DACS_RECEIVE_LISTENING;
   TRACE( "DACS : Listening started");
}

void DacsListenStop( void)
// Stops listening on port
{
   _DacsStatus = DACS_IDLE;
   TRACE( "DACS : Listening stopped");
}

TDacsRequest *DacsReceive( void)
{
   return NULL;
}

TYesNo DacsSend( void)
// Sends reply, reply buffer should be set
{
   return NO;
}

TDacsReply *DacsGetReplyBuffer()
// Returns buffer for outgoing message, should be filled before calling DacsSend
{
   return &Buffer;
}
