//*****************************************************************************
//
//    SocketIfUsb.c     Usb socket interface
//    Version 1.0       (c) VEIT Electronics
//
//*****************************************************************************

#include "Remote/SocketIfUsb.h"
#include "Remote/Frame.h"

void SocketIfUsbInit( void) {
}

void SocketIfUsbDeinit( void) {
}

void SocketIfUsbTimer( void) {
}

void SocketIfUsbExecute( void) {
   
}

byte SocketIfUsbState( TSocket *Socket) {
   return 0;
}

int SocketIfUsbReceiveSize( TSocket *Socket) {
   return 0;
}

TYesNo SocketIfUsbReceive( TSocket *Socket, void *Buffer, int Size) {
   return YES;

}

TYesNo SocketIfUsbSend( TSocket *Socket, const void *Buffer, int Size) {
   return YES;
}
