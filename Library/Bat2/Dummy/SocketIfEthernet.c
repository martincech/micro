//*****************************************************************************
//
//    SocketIfEthernet.c    Ethernet socket interface
//    Version 1.0           (c) VEIT Electronics
//
//*****************************************************************************

#include "Remote/SocketIfEthernet.h"
#include "Remote/Frame.h"
#include "Multitasking/Multitasking.h"

void SocketIfEthernetReady( void) {
}

void SocketIfEthernetExecute( void) {
}

void SocketIfEthernetTimer( void) {
}

void SocketIfEthernetDeinit(){
}

byte SocketIfEthernetState( TSocket *Socket) {
   return 0;
}

int SocketIfEthernetReceiveSize( TSocket *Socket) {
   return 0;
}

TYesNo SocketIfEthernetSend( TSocket *Socket, const void *Buffer, int Size) {
   return YES;
}

TYesNo SocketIfEthernetReceive( TSocket *Socket, void *Buffer, int Size) {
   return YES;
}
