//******************************************************************************
//
//   Wepl.c        Weighing platform communication
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "Platform/Wepl.h"

//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

TYesNo WeplInit( void)
// Initialize
{
} // WeplInit

//------------------------------------------------------------------------------
//  Version
//------------------------------------------------------------------------------

TYesNo WeplVersionGet( TPlatformVersion *Version)
// Get device version info
{
   return NO;
} // WeplVersion

//------------------------------------------------------------------------------
//  Status
//------------------------------------------------------------------------------

TYesNo WeplStatusGet( TPlatformStatus *Status)
// Get device status
{
   return NO;
} // WeplStatusGet

//------------------------------------------------------------------------------
//  Clock
//------------------------------------------------------------------------------

TYesNo WeplClockSet( TPlatformClock Clock)
// Set device clock
{
   return NO;
} // WeplClockSet

//------------------------------------------------------------------------------
//  Picostrain
//------------------------------------------------------------------------------

TYesNo WeplPicostrainSet( TPlatformPicostrain *Picostrain)
// Set picostrain configuration
{
   return NO;
} // WeplPicostrainSet

//------------------------------------------------------------------------------
//  Sigma Delta
//------------------------------------------------------------------------------

TYesNo WeplSigmaDeltaSet( TPlatformSigmaDelta *SigmaDelta)
// Set sigma-delta configuration
{
   return NO;
} // WeplSigmaDeltaSet

//------------------------------------------------------------------------------
//  Detection
//------------------------------------------------------------------------------

TYesNo WeplDetectionSet( TPlatformDetection *Detection)
// Set detection parameters
{
   return NO;
} // WeplDetectionSet

//------------------------------------------------------------------------------
//  Acceptance
//------------------------------------------------------------------------------

TYesNo WeplAcceptanceSet( TPlatformAcceptance *Acceptance)
// Set acceptance parameters
{
   return NO;
} // WeplAcceptanceSet

//------------------------------------------------------------------------------
//  Calibration
//------------------------------------------------------------------------------

TYesNo WeplCalibrationSet( TPlatformCalibration *Calibration)
// Set calibration parameters
{
   return NO;
} // WeplCalibrationSet

//------------------------------------------------------------------------------
//  Start
//------------------------------------------------------------------------------

TYesNo WeplWeighingStart( void)
// Start weighing
{
   return NO;
} // WeplWeighingStart

//------------------------------------------------------------------------------
//  Diagnostic start
//------------------------------------------------------------------------------

TYesNo WeplDiagnosticStart( void)
// Start diagnostics
{
   return NO;
} // WeplDiagnosticStart

//------------------------------------------------------------------------------
// Diagnostics get
//------------------------------------------------------------------------------

TYesNo WeplDiagnosticWeightGet( dword IdIn, dword *IdOut, TDiagnosticFrame *Frame)
// Get diagnostic data
{
   return NO;
} // WeplDiagnosticWeightGet

//------------------------------------------------------------------------------
//  Stop
//------------------------------------------------------------------------------

TYesNo WeplStop( void)
// Stop weighing/diagnostics
{
   return NO;
} // WeplStop

//------------------------------------------------------------------------------
//  Saved weight
//------------------------------------------------------------------------------

TYesNo WeplSavedWeightGet( word *Count, TPlatformWeight *Sample)
// Returns saved <Sample> with <Count>
{
   return NO;
} // WeplSavedWeightGet

//------------------------------------------------------------------------------
//  Confirm saved weight
//------------------------------------------------------------------------------

TYesNo WeplSavedWeightConfirm( word Count)
// Confirm <Count> saved weights
{
   return NO;
} // WeplSavedWeightConfirm

//------------------------------------------------------------------------------
//   Communication set
//------------------------------------------------------------------------------

TYesNo WeplCommunicationSet( TPlatformCommunication *Communication)
// Set communication parameters (warning : parameters are set immediately)
{
   return NO;
} // WeplCommunicationSet

//------------------------------------------------------------------------------
//   Communication status get
//------------------------------------------------------------------------------

TYesNo WeplCommunicationStatusGet( TPlatformCommunicationStatus *CommunicationStatus)
// Get communication status
{
   return NO;
} // WeplCommunicationStatusGet