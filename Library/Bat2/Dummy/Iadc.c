//*****************************************************************************
//
//   Iadc.c      Internal A/D convertor functions
//   Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#include "Iadc/Iadc.h"

#define IADC_VALUE_NORMAL 0xD0

//-----------------------------------------------------------------------------
//  Initialization
//-----------------------------------------------------------------------------

void IAdcInit( void)
// Initialize convertor
{
} // IadcInit

//-----------------------------------------------------------------------------
//  Conversion
//-----------------------------------------------------------------------------

word IAdcRead( byte Channel)
// Returns measured <Channel> value
{
   return( IADC_VALUE_NORMAL);
}  // IadcRead
