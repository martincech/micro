//******************************************************************************
//
//   MenuTargetWeights.c  Target weights menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuTargetWeights.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "WeighingConfiguration.h"


static DefMenu( TargetWeightsMenu)
   STR_MODE,
   STR_GROWTH,
   STR_SEX_DIFFERENTIATION,
   STR_SEX,
   STR_ADJUST_TARGET_WEIGHTS,
EndMenu()

typedef enum {
   MI_MODE,
   MI_GROWTH,
   MI_SEX_DIFFERENTIATION,
   MI_SEX,
   MI_ADJUST_TARGET_WEIGHTS
} ETargetWeightsMenu;

// Local functions :

static void WeighingTargetWeightsParameters( int Index, int y, TWeighingTargetWeights *Parameters);
// Draw target weights parameters

//------------------------------------------------------------------------------
//  Menu TargetWeights
//------------------------------------------------------------------------------

void MenuTargetWeights( void)
// Edit target weights parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_TARGET_WEIGHTS, TargetWeightsMenu, (TMenuItemCb *)WeighingTargetWeightsParameters, &WeighingTargetWeights, &MData)){
         ConfigWeighingTargetWeightsSave();
         return;
      }
      switch( MData.Item){
         case MI_MODE :
            i = WeighingTargetWeights.Mode;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_PREDICTION_MODE, _PREDICTION_MODE_LAST)){
               break;
            }
            WeighingTargetWeights.Mode = (byte)i;
            break;

         case MI_GROWTH :
            i = WeighingTargetWeights.Growth;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_PREDICTION_GROWTH, _PREDICTION_GROWTH_LAST)){
               break;
            }
            WeighingTargetWeights.Growth = (byte)i;
            break;

         case MI_SEX_DIFFERENTIATION :
            i = WeighingTargetWeights.SexDifferentiation;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_SEX_DIFFERENTIATION, _SEX_DIFFERENTIATION_LAST)){
               break;
            }
            WeighingTargetWeights.SexDifferentiation = (byte)i;
            break;

         case MI_SEX :
            i = WeighingTargetWeights.Sex;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_SEX, _SEX_LAST)){
               break;
            }
            WeighingTargetWeights.Sex = (byte)i;
            break;

         case MI_ADJUST_TARGET_WEIGHTS :
            i = WeighingTargetWeights.AdjustTargetWeights;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_ADJUST_TARGET_WEIGHTS, _ADJUST_TARGET_WEIGHTS_LAST)){
               break;
            }
            WeighingTargetWeights.AdjustTargetWeights = (byte)i;
            break;

      }
   }
} // MenuTargetWeights

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void WeighingTargetWeightsParameters( int Index, int y, TWeighingTargetWeights *Parameters)
// Draw target weights parameters
{
   switch( Index){
      case MI_MODE :
         DLabelEnum( Parameters->Mode, ENUM_PREDICTION_MODE, DMENU_PARAMETERS_X, y);
         break;

      case MI_GROWTH :
         DLabelEnum( Parameters->Growth, ENUM_PREDICTION_GROWTH, DMENU_PARAMETERS_X, y);
         break;

      case MI_SEX_DIFFERENTIATION :
         DLabelEnum( Parameters->SexDifferentiation, ENUM_SEX_DIFFERENTIATION, DMENU_PARAMETERS_X, y);
         break;

      case MI_SEX :
         DLabelEnum( Parameters->Sex, ENUM_SEX, DMENU_PARAMETERS_X, y);
         break;

      case MI_ADJUST_TARGET_WEIGHTS :
         DLabelEnum( Parameters->AdjustTargetWeights, ENUM_ADJUST_TARGET_WEIGHTS, DMENU_PARAMETERS_X, y);
         break;

   }
} // WeighingTargetWeightsParameters
