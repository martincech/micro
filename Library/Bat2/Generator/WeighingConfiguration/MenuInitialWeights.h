//******************************************************************************
//
//   MenuInitialWeights.h  Initial weights menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuInitialWeights_H__
   #define __MenuInitialWeights_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeighingConfiguration_H__
   #include "WeighingConfiguration.h"
#endif


void MenuInitialWeights( void);
// Menu initial weights

#endif
