//******************************************************************************
//
//   MenuAcceptanceData.h  Acceptance data menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuAcceptanceData_H__
   #define __MenuAcceptanceData_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeighingConfiguration_H__
   #include "WeighingConfiguration.h"
#endif


void MenuAcceptanceData( void);
// Menu acceptance data

#endif
