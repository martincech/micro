//******************************************************************************
//
//   MenuWeighingConfiguration.c  Weighing configuration menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWeighingConfiguration.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "WeighingConfiguration.h"
#include "MenuConfigurationMenuSelection.h"
#include "MenuConfigurationInitialWeights.h"
#include "MenuConfigurationGrowthCurves.h"
#include "MenuConfigurationStandardCurves.h"
#include "MenuConfigurationDetection.h"
#include "MenuConfigurationTargetWeights.h"
#include "MenuConfigurationAcceptance.h"
#include "MenuConfigurationStatistics.h"
#include "MenuConfigurationStartNow.h"
#include "MenuConfigurationStartLater.h"


static DefMenu( WeighingConfigurationMenu)
   STR_MENU_SELECTION,
   STR_TYPE,
   STR_FLOCK,
   STR_INITIAL_DAY,
   STR_INITIAL_WEIGHT,
   STR_INITIAL_WEIGHTS,
   STR_GROWTH_CURVE,
   STR_GROWTH_CURVES,
   STR_STANDARD_CURVE,
   STR_STANDARD_CURVES,
   STR_CORRECTION_CURVE,
   STR_DETECTION,
   STR_TARGET_WEIGHTS,
   STR_ACCEPTANCE,
   STR_STATISTICS,
   STR_DAY_START,
   STR_START_NOW,
   STR_START_LATER,
EndMenu()

typedef enum {
   MI_MENU_SELECTION,
   MI_TYPE,
   MI_FLOCK,
   MI_INITIAL_DAY,
   MI_INITIAL_WEIGHT,
   MI_INITIAL_WEIGHTS,
   MI_GROWTH_CURVE,
   MI_GROWTH_CURVES,
   MI_STANDARD_CURVE,
   MI_STANDARD_CURVES,
   MI_CORRECTION_CURVE,
   MI_DETECTION,
   MI_TARGET_WEIGHTS,
   MI_ACCEPTANCE,
   MI_STATISTICS,
   MI_DAY_START,
   MI_START_NOW,
   MI_START_LATER
} EWeighingConfigurationMenu;

// Local functions :

static void WeighingConfigurationParameters( int Index, int y, TWeighingConfiguration *Parameters);
// Draw weighing configuration parameters

//------------------------------------------------------------------------------
//  Menu WeighingConfiguration
//------------------------------------------------------------------------------

void MenuWeighingConfiguration( void)
// Edit weighing configuration parameters
{
TMenuData MData;
int       i;
char Type[ WEIGHING_CONFIGURATION_TYPE_SIZE + 1];
char Flock[ WEIGHING_CONFIGURATION_FLOCK_SIZE + 1];


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_CONFIGURATION, WeighingConfigurationMenu, (TMenuItemCb *)WeighingConfigurationParameters, &WeighingConfiguration, &MData)){
         ConfigWeighingConfigurationSave();
         return;
      }
      switch( MData.Item){
         case MI_MENU_SELECTION :
            MenuConfigurationMenuSelection();
            break;

         case MI_TYPE :
            strcpy( Type, WeighingConfiguration.Type);
            if( !DInputText( STR_TYPE, STR_ENTER_TYPE, Type, WEIGHING_CONFIGURATION_TYPE_SIZE)){
               break;
            }
            strcpy( WeighingConfiguration.Type, Type);
            break;

         case MI_FLOCK :
            strcpy( Flock, WeighingConfiguration.Flock);
            if( !DInputText( STR_FLOCK, STR_ENTER_FLOCK, Flock, WEIGHING_CONFIGURATION_FLOCK_SIZE)){
               break;
            }
            strcpy( WeighingConfiguration.Flock, Flock);
            break;

         case MI_INITIAL_DAY :
            i = WeighingConfiguration.InitialDay;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_CONFIGURATION_INITIAL_DAY_MIN, WEIGHING_CONFIGURATION_INITIAL_DAY_MAX, 0)){
               break;
            }
            WeighingConfiguration.InitialDay = (TDayNumber)i;
            break;

         case MI_INITIAL_WEIGHT :
            i = WeighingConfiguration.InitialWeight;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            WeighingConfiguration.InitialWeight = (TWeightGauge)i;
            break;

         case MI_INITIAL_WEIGHTS :
            MenuConfigurationInitialWeights();
            break;

         case MI_GROWTH_CURVE :
            i = WeighingConfiguration.GrowthCurve;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_CONFIGURATION_GROWTH_CURVE_MIN, WEIGHING_CONFIGURATION_GROWTH_CURVE_MAX, 0)){
               break;
            }
            WeighingConfiguration.GrowthCurve = (TCurveIdentifier)i;
            break;

         case MI_GROWTH_CURVES :
            MenuConfigurationGrowthCurves();
            break;

         case MI_STANDARD_CURVE :
            i = WeighingConfiguration.StandardCurve;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_CONFIGURATION_STANDARD_CURVE_MIN, WEIGHING_CONFIGURATION_STANDARD_CURVE_MAX, 0)){
               break;
            }
            WeighingConfiguration.StandardCurve = (TCurveIdentifier)i;
            break;

         case MI_STANDARD_CURVES :
            MenuConfigurationStandardCurves();
            break;

         case MI_CORRECTION_CURVE :
            i = WeighingConfiguration.CorrectionCurve;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_CONFIGURATION_CORRECTION_CURVE_MIN, WEIGHING_CONFIGURATION_CORRECTION_CURVE_MAX, 0)){
               break;
            }
            WeighingConfiguration.CorrectionCurve = (TCorrectionIdentifier)i;
            break;

         case MI_DETECTION :
            MenuConfigurationDetection();
            break;

         case MI_TARGET_WEIGHTS :
            MenuConfigurationTargetWeights();
            break;

         case MI_ACCEPTANCE :
            MenuConfigurationAcceptance();
            break;

         case MI_STATISTICS :
            MenuConfigurationStatistics();
            break;

         case MI_DAY_START :
            uTime( &DateTime.Time, WeighingConfiguration.DayStart);
            if( !DInputTime( STR_, STR_ENTER_, &DateTime.Time)){
               break;
            }
            WeighingConfiguration.DayStart = uTimeGauge( &DateTime.Time);
            break;

         case MI_START_NOW :
            MenuConfigurationStartNow();
            break;

         case MI_START_LATER :
            MenuConfigurationStartLater();
            break;

      }
   }
} // MenuWeighingConfiguration

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void WeighingConfigurationParameters( int Index, int y, TWeighingConfiguration *Parameters)
// Draw weighing configuration parameters
{
   switch( Index){
      case MI_MENU_SELECTION :
         break;

      case MI_TYPE :
         DLabelNarrow( Parameters->Type, DMENU_PARAMETERS_X, y);
         break;

      case MI_FLOCK :
         DLabelNarrow( Parameters->Flock, DMENU_PARAMETERS_X, y);
         break;

      case MI_INITIAL_DAY :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->InitialDay, 0);
         break;

      case MI_INITIAL_WEIGHT :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->InitialWeight);
         break;

      case MI_INITIAL_WEIGHTS :
         break;

      case MI_GROWTH_CURVE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->GrowthCurve, 0);
         break;

      case MI_GROWTH_CURVES :
         break;

      case MI_STANDARD_CURVE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->StandardCurve, 0);
         break;

      case MI_STANDARD_CURVES :
         break;

      case MI_CORRECTION_CURVE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->CorrectionCurve, 0);
         break;

      case MI_DETECTION :
         break;

      case MI_TARGET_WEIGHTS :
         break;

      case MI_ACCEPTANCE :
         break;

      case MI_STATISTICS :
         break;

      case MI_DAY_START :
         DTimeRight( Parameters->DayStart, DMENU_PARAMETERS_X, y);
         break;

      case MI_START_NOW :
         break;

      case MI_START_LATER :
         break;

   }
} // WeighingConfigurationParameters
