//******************************************************************************
//
//   MenuStandardCurves.h  Standard curves menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuStandardCurves_H__
   #define __MenuStandardCurves_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeighingConfiguration_H__
   #include "WeighingConfiguration.h"
#endif


void MenuStandardCurves( void);
// Menu standard curves

#endif
