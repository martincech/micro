//******************************************************************************
//
//   MenuAcceptance.c  Acceptance menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuAcceptance.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "WeighingConfiguration.h"
#include "MenuAcceptanceData.h"
#include "MenuAcceptanceData.h"


static DefMenu( AcceptanceMenu)
   STR_MARGIN_ABOVE,
   STR_MARGIN_BELOW,
   STR_MALES,
   STR_FEMALES,
EndMenu()

typedef enum {
   MI_MARGIN_ABOVE,
   MI_MARGIN_BELOW,
   MI_MALES,
   MI_FEMALES
} EAcceptanceMenu;

// Local functions :

static void WeighingAcceptanceParameters( int Index, int y, TWeighingAcceptance *Parameters);
// Draw acceptance parameters

//------------------------------------------------------------------------------
//  Menu Acceptance
//------------------------------------------------------------------------------

void MenuAcceptance( void)
// Edit acceptance parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_ACCEPTANCE, AcceptanceMenu, (TMenuItemCb *)WeighingAcceptanceParameters, &WeighingAcceptance, &MData)){
         ConfigWeighingAcceptanceSave();
         return;
      }
      switch( MData.Item){
         case MI_MARGIN_ABOVE :
            i = WeighingAcceptance.MarginAbove;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_ACCEPTANCE_MARGIN_ABOVE_MIN, WEIGHING_ACCEPTANCE_MARGIN_ABOVE_MAX, "%")){
               break;
            }
            WeighingAcceptance.MarginAbove = (byte)i;
            break;

         case MI_MARGIN_BELOW :
            i = WeighingAcceptance.MarginBelow;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_ACCEPTANCE_MARGIN_BELOW_MIN, WEIGHING_ACCEPTANCE_MARGIN_BELOW_MAX, "%")){
               break;
            }
            WeighingAcceptance.MarginBelow = (byte)i;
            break;

         case MI_MALES :
            MenuAcceptanceData();
            break;

         case MI_FEMALES :
            MenuAcceptanceData();
            break;

      }
   }
} // MenuAcceptance

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void WeighingAcceptanceParameters( int Index, int y, TWeighingAcceptance *Parameters)
// Draw acceptance parameters
{
   switch( Index){
      case MI_MARGIN_ABOVE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Parameters->MarginAbove, "%");
         break;

      case MI_MARGIN_BELOW :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Parameters->MarginBelow, "%");
         break;

      case MI_MALES :
         break;

      case MI_FEMALES :
         break;

   }
} // WeighingAcceptanceParameters
