//******************************************************************************
//
//   MenuGrowthCurves.c  Growth curves menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuGrowthCurves.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "WeighingConfiguration.h"


static DefMenu( GrowthCurvesMenu)
   STR_MALES,
   STR_FEMALES,
EndMenu()

typedef enum {
   MI_MALES,
   MI_FEMALES
} EGrowthCurvesMenu;

// Local functions :

static void WeighingGrowthCurvesParameters( int Index, int y, TWeighingGrowthCurves *Parameters);
// Draw growth curves parameters

//------------------------------------------------------------------------------
//  Menu GrowthCurves
//------------------------------------------------------------------------------

void MenuGrowthCurves( void)
// Edit growth curves parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_GROWTH_CURVES, GrowthCurvesMenu, (TMenuItemCb *)WeighingGrowthCurvesParameters, &WeighingGrowthCurves, &MData)){
         ConfigWeighingGrowthCurvesSave();
         return;
      }
      switch( MData.Item){
         case MI_MALES :
            i = WeighingGrowthCurves.Males;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_GROWTH_CURVES_MALES_MIN, WEIGHING_GROWTH_CURVES_MALES_MAX, 0)){
               break;
            }
            WeighingGrowthCurves.Males = (TCurveIdentifier)i;
            break;

         case MI_FEMALES :
            i = WeighingGrowthCurves.Females;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_GROWTH_CURVES_FEMALES_MIN, WEIGHING_GROWTH_CURVES_FEMALES_MAX, 0)){
               break;
            }
            WeighingGrowthCurves.Females = (TCurveIdentifier)i;
            break;

      }
   }
} // MenuGrowthCurves

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void WeighingGrowthCurvesParameters( int Index, int y, TWeighingGrowthCurves *Parameters)
// Draw growth curves parameters
{
   switch( Index){
      case MI_MALES :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->Males, 0);
         break;

      case MI_FEMALES :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->Females, 0);
         break;

   }
} // WeighingGrowthCurvesParameters
