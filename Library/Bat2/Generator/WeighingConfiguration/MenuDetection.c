//******************************************************************************
//
//   MenuDetection.c  Detection menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuDetection.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "WeighingConfiguration.h"


static DefMenu( DetectionMenu)
   STR_FILTER,
   STR_STABILIZATION_TIME,
   STR_STABILIZATION_RANGE,
   STR_STEP,
EndMenu()

typedef enum {
   MI_FILTER,
   MI_STABILIZATION_TIME,
   MI_STABILIZATION_RANGE,
   MI_STEP
} EDetectionMenu;

// Local functions :

static void WeighingDetectionParameters( int Index, int y, TWeighingDetection *Parameters);
// Draw detection parameters

//------------------------------------------------------------------------------
//  Menu Detection
//------------------------------------------------------------------------------

void MenuDetection( void)
// Edit detection parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_DETECTION, DetectionMenu, (TMenuItemCb *)WeighingDetectionParameters, &WeighingDetection, &MData)){
         ConfigWeighingDetectionSave();
         return;
      }
      switch( MData.Item){
         case MI_FILTER :
            i = WeighingDetection.Filter;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 1, WEIGHING_DETECTION_FILTER_MIN, WEIGHING_DETECTION_FILTER_MAX, "s")){
               break;
            }
            WeighingDetection.Filter = (byte)i;
            break;

         case MI_STABILIZATION_TIME :
            i = WeighingDetection.StabilizationTime;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 1, WEIGHING_DETECTION_STABILIZATION_TIME_MIN, WEIGHING_DETECTION_STABILIZATION_TIME_MAX, "s")){
               break;
            }
            WeighingDetection.StabilizationTime = (byte)i;
            break;

         case MI_STABILIZATION_RANGE :
            i = WeighingDetection.StabilizationRange;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 1, WEIGHING_DETECTION_STABILIZATION_RANGE_MIN, WEIGHING_DETECTION_STABILIZATION_RANGE_MAX, "%")){
               break;
            }
            WeighingDetection.StabilizationRange = (byte)i;
            break;

         case MI_STEP :
            i = WeighingDetection.Step;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_PLATFORM_STEP_MODE, _PLATFORM_STEP_MODE_LAST)){
               break;
            }
            WeighingDetection.Step = (byte)i;
            break;

      }
   }
} // MenuDetection

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void WeighingDetectionParameters( int Index, int y, TWeighingDetection *Parameters)
// Draw detection parameters
{
   switch( Index){
      case MI_FILTER :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%5.1f %s", Parameters->Filter, "s");
         break;

      case MI_STABILIZATION_TIME :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%5.1f %s", Parameters->StabilizationTime, "s");
         break;

      case MI_STABILIZATION_RANGE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%7.1f %s", Parameters->StabilizationRange, "%");
         break;

      case MI_STEP :
         DLabelEnum( Parameters->Step, ENUM_PLATFORM_STEP_MODE, DMENU_PARAMETERS_X, y);
         break;

   }
} // WeighingDetectionParameters
