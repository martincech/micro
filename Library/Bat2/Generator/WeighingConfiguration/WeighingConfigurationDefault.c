TWeighingConfiguration WeighingConfiguration;
TWeighingInitialWeights WeighingInitialWeights;
TWeighingGrowthCurves WeighingGrowthCurves;
TWeighingStandardCurves WeighingStandardCurves;
TWeighingTargetWeights WeighingTargetWeights;
TWeighingAcceptance WeighingAcceptance;
TWeighingAcceptanceData WeighingAcceptanceData;
TWeighingDetection WeighingDetection;

//------------------------------------------------------------------------------
//  Weighing configuration
//------------------------------------------------------------------------------

const TWeighingConfiguration WeighingConfigurationDefault = {
   /* Type */ WEIGHING_CONFIGURATION_TYPE_DEFAULT,
   /* Flock */ WEIGHING_CONFIGURATION_FLOCK_DEFAULT,
   /* InitialDay */ WEIGHING_CONFIGURATION_INITIAL_DAY_DEFAULT,
   /* InitialWeight */ WEIGHING_CONFIGURATION_INITIAL_WEIGHT_DEFAULT,
   /* GrowthCurve */ WEIGHING_CONFIGURATION_GROWTH_CURVE_DEFAULT,
   /* StandardCurve */ WEIGHING_CONFIGURATION_STANDARD_CURVE_DEFAULT,
   /* CorrectionCurve */ WEIGHING_CONFIGURATION_CORRECTION_CURVE_DEFAULT,
   /* DayStart */ WEIGHING_CONFIGURATION_DAY_START_DEFAULT
};

//------------------------------------------------------------------------------
//  Weighing initial weights
//------------------------------------------------------------------------------

const TWeighingInitialWeights WeighingInitialWeightsDefault = {
   /* Males */ WEIGHING_INITIAL_WEIGHTS_MALES_DEFAULT,
   /* Females */ WEIGHING_INITIAL_WEIGHTS_FEMALES_DEFAULT
};

//------------------------------------------------------------------------------
//  Weighing growth curves
//------------------------------------------------------------------------------

const TWeighingGrowthCurves WeighingGrowthCurvesDefault = {
   /* Males */ WEIGHING_GROWTH_CURVES_MALES_DEFAULT,
   /* Females */ WEIGHING_GROWTH_CURVES_FEMALES_DEFAULT
};

//------------------------------------------------------------------------------
//  Weighing standard curves
//------------------------------------------------------------------------------

const TWeighingStandardCurves WeighingStandardCurvesDefault = {
   /* Males */ WEIGHING_STANDARD_CURVES_MALES_DEFAULT,
   /* Females */ WEIGHING_STANDARD_CURVES_FEMALES_DEFAULT
};

//------------------------------------------------------------------------------
//  Weighing target weights
//------------------------------------------------------------------------------

const TWeighingTargetWeights WeighingTargetWeightsDefault = {
   /* Mode */ PREDICTION_MODE_AUTOMATIC,
   /* Growth */ PREDICTION_GROWTH_SLOW,
   /* SexDifferentiation */ SEX_DIFFERENTIATION_NO,
   /* Sex */ SEX_UNDEFINED,
   /* AdjustTargetWeights */ ADJUST_TARGET_WEIGHTS_NONE
};

//------------------------------------------------------------------------------
//  Weighing acceptance
//------------------------------------------------------------------------------

const TWeighingAcceptance WeighingAcceptanceDefault = {
   /* MarginAbove */ WEIGHING_ACCEPTANCE_MARGIN_ABOVE_DEFAULT,
   /* MarginBelow */ WEIGHING_ACCEPTANCE_MARGIN_BELOW_DEFAULT
};

//------------------------------------------------------------------------------
//  Weighing acceptance data
//------------------------------------------------------------------------------

const TWeighingAcceptanceData WeighingAcceptanceDataDefault = {
   /* MarginAbove */ WEIGHING_ACCEPTANCE_DATA_MARGIN_ABOVE_DEFAULT,
   /* MarginBelow */ WEIGHING_ACCEPTANCE_DATA_MARGIN_BELOW_DEFAULT
};

//------------------------------------------------------------------------------
//  Weighing detection
//------------------------------------------------------------------------------

const TWeighingDetection WeighingDetectionDefault = {
   /* Filter */ WEIGHING_DETECTION_FILTER_DEFAULT,
   /* StabilizationTime */ WEIGHING_DETECTION_STABILIZATION_TIME_DEFAULT,
   /* StabilizationRange */ WEIGHING_DETECTION_STABILIZATION_RANGE_DEFAULT,
   /* Step */ PLATFORM_STEP_MODE_ENTER
};

