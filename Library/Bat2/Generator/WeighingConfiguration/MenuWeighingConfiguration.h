//******************************************************************************
//
//   MenuWeighingConfiguration.h  Weighing configuration menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuWeighingConfiguration_H__
   #define __MenuWeighingConfiguration_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeighingConfiguration_H__
   #include "WeighingConfiguration.h"
#endif


void MenuWeighingConfiguration( void);
// Menu weighing configuration

#endif
