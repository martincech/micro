//******************************************************************************
//
//   MenuInitialWeights.c  Initial weights menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuInitialWeights.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "WeighingConfiguration.h"


static DefMenu( InitialWeightsMenu)
   STR_MALES,
   STR_FEMALES,
EndMenu()

typedef enum {
   MI_MALES,
   MI_FEMALES
} EInitialWeightsMenu;

// Local functions :

static void WeighingInitialWeightsParameters( int Index, int y, TWeighingInitialWeights *Parameters);
// Draw initial weights parameters

//------------------------------------------------------------------------------
//  Menu InitialWeights
//------------------------------------------------------------------------------

void MenuInitialWeights( void)
// Edit initial weights parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_INITIAL_WEIGHTS, InitialWeightsMenu, (TMenuItemCb *)WeighingInitialWeightsParameters, &WeighingInitialWeights, &MData)){
         ConfigWeighingInitialWeightsSave();
         return;
      }
      switch( MData.Item){
         case MI_MALES :
            i = WeighingInitialWeights.Males;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            WeighingInitialWeights.Males = (TWeightGauge)i;
            break;

         case MI_FEMALES :
            i = WeighingInitialWeights.Females;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            WeighingInitialWeights.Females = (TWeightGauge)i;
            break;

      }
   }
} // MenuInitialWeights

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void WeighingInitialWeightsParameters( int Index, int y, TWeighingInitialWeights *Parameters)
// Draw initial weights parameters
{
   switch( Index){
      case MI_MALES :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->Males);
         break;

      case MI_FEMALES :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->Females);
         break;

   }
} // WeighingInitialWeightsParameters
