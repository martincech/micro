//******************************************************************************
//
//   MenuGsmContacts.c  Gsm contacts menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuGsmContacts.h"
#include "Gadget/DMenu.h"         // Display menu
#include "Str.h"                  // Strings

#include "MenuGsm.h"
#include "MenuContactsEdit.h"
#include "MenuContactsCreate.h"
#include "MenuContactsDelete.h"


static DefMenu( GsmContactsMenu)
   STR_EDIT,
   STR_CREATE,
   STR_DELETE,
EndMenu()

typedef enum {
   MI_EDIT,
   MI_CREATE,
   MI_DELETE
} EGsmContactsMenu;

//------------------------------------------------------------------------------
//  Menu GsmContacts
//------------------------------------------------------------------------------

void MenuGsmContacts( void)
// Menu gsm contacts
{
TMenuData MData;

   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_CONTACTS, GsmContactsMenu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_EDIT :
            MenuContactsEdit();
            break;

         case MI_CREATE :
            MenuContactsCreate();
            break;

         case MI_DELETE :
            MenuContactsDelete();
            break;

      }
   }
} // MenuGsmContacts
