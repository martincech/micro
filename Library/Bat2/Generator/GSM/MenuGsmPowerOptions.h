//******************************************************************************
//
//   MenuGsmPowerOptions.h  Gsm power options menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuGsmPowerOptions_H__
   #define __MenuGsmPowerOptions_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __MenuGsm_H__
   #include "MenuGsm.h"
#endif


void MenuGsmPowerOptions( void);
// Menu gsm power options

#endif
