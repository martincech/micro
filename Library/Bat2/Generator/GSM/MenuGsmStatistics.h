//******************************************************************************
//
//   MenuGsmStatistics.h  Gsm statistics menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuGsmStatistics_H__
   #define __MenuGsmStatistics_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __MenuGsm_H__
   #include "MenuGsm.h"
#endif


void MenuGsmStatistics( void);
// Menu gsm statistics

#endif
