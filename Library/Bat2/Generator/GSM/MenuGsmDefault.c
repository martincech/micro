TGsmContact GsmContact;
TGsmCommands GsmCommands;
TGsmEvents GsmEvents;
TGsmPowerOptions GsmPowerOptions;

//------------------------------------------------------------------------------
//  Gsm contact
//------------------------------------------------------------------------------

const TGsmContact GsmContactDefault = {
   /* Name */ GSM_CONTACT_NAME_DEFAULT,
   /* PhoneNumber */ GSM_CONTACT_PHONE_NUMBER_DEFAULT,
   /* SmsFormat */ GSM_SMS_FORMAT_MOBILE_PHONE,
   /* Statistics */ NO,
   /* SendHistogram */ NO,
   /* Commands */ NO,
   /* Events */ NO,
   /* _Dummy */ 0
};

//------------------------------------------------------------------------------
//  Gsm commands
//------------------------------------------------------------------------------

const TGsmCommands GsmCommandsDefault = {
   /* Enabled */ NO,
   /* CheckPhoneNumber */ YES,
   /* Expiration */ GSM_COMMANDS_EXPIRATION_DEFAULT
};

//------------------------------------------------------------------------------
//  Gsm events
//------------------------------------------------------------------------------

const TGsmEvents GsmEventsDefault = {
   /* WeightGreater */ GSM_EVENTS_WEIGHT_GREATER_DEFAULT,
   /* WeightCountLess */ GSM_EVENTS_WEIGHT_COUNT_LESS_DEFAULT,
   /* StatusChanged */ NO,
   /* DeviceFailure */ NO
};

//------------------------------------------------------------------------------
//  Gsm power options
//------------------------------------------------------------------------------

const TGsmPowerOptions GsmPowerOptionsDefault = {
   /* Mode */ GSM_POWER_MODE_OFF,
   /* SwitchOnPeriod */ GSM_POWER_OPTIONS_SWITCH_ON_PERIOD_DEFAULT,
   /* SwitchOnDuration */ GSM_POWER_OPTIONS_SWITCH_ON_DURATION_DEFAULT,
   /* SwitchOnTime */ GSM_POWER_OPTIONS_SWITCH_ON_TIME_DEFAULT
};

