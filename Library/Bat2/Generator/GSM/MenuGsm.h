//******************************************************************************
//
//   MenuGsm.h  Gsm menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuGsm_H__
   #define __MenuGsm_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __MenuGsm_H__
   #include "MenuGsm.h"
#endif


void MenuGsm( void);
// Menu gsm

#endif
