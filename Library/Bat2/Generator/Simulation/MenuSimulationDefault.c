TSimulation Simulation;

//------------------------------------------------------------------------------
//  Simulation
//------------------------------------------------------------------------------

TSimulation SimulationDefault = {
   /* DayDuration */ SIMULATION_DAY_DURATION_DEFAULT,
   /* TargetWeight */ SIMULATION_TARGET_WEIGHT_DEFAULT,
   /* TargetWeightFemale */ SIMULATION_TARGET_WEIGHT_FEMALE_DEFAULT
};

