TCurvePoint CurvePoint;
TGrowthCurve GrowthCurve;

//------------------------------------------------------------------------------
//  Curve point
//------------------------------------------------------------------------------

TCurvePoint CurvePointDefault = {
   /* Day */ ?,
   /* _Dummy */ 0,
   /* Weight */ CURVE_POINT_WEIGHT_DEFAULT
};

//------------------------------------------------------------------------------
//  Growth curve
//------------------------------------------------------------------------------

TGrowthCurve GrowthCurveDefault = {
   /* Name */ GROWTH_CURVE_NAME_DEFAULT,
   /* Deleted */ GROWTH_CURVE_DELETED_DEFAULT,
   /* Standard */ GROWTH_CURVE_STANDARD_DEFAULT,
   /* _Dummy */ 0,
   /* Point */ Array?
};

