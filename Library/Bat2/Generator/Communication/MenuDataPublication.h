//******************************************************************************
//
//   MenuDataPublication.h  Data publication menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuDataPublication_H__
   #define __MenuDataPublication_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Communication_H__
   #include "Communication.h"
#endif


void MenuDataPublication( void);
// Menu data publication

#endif
