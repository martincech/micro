//******************************************************************************
//
//   MenuDataPublication.c  Data publication menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuDataPublication.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Communication.h"
#include "MenuGsmContacts.h"


static DefMenu( DataPublicationMenu)
   STR_INTERFACE,
   STR_USERNAME,
   STR_PASSWORD,
   STR_URL,
   STR_CONTACTS,
   STR_START_FROM_DAY,
   STR_PERIOD,
   STR_ACCELERATE_FROM_DAY,
   STR_ACCELERATED_PERIOD,
   STR_SEND_AT,
EndMenu()

typedef enum {
   MI_INTERFACE,
   MI_USERNAME,
   MI_PASSWORD,
   MI_URL,
   MI_CONTACTS,
   MI_START_FROM_DAY,
   MI_PERIOD,
   MI_ACCELERATE_FROM_DAY,
   MI_ACCELERATED_PERIOD,
   MI_SEND_AT
} EDataPublicationMenu;

// Local functions :

static void DataPublicationParameters( int Index, int y, TDataPublication *Parameters);
// Draw data publication parameters

//------------------------------------------------------------------------------
//  Menu DataPublication
//------------------------------------------------------------------------------

void MenuDataPublication( void)
// Edit data publication parameters
{
TMenuData MData;
int       i;
char Username[ DATA_PUBLICATION_USERNAME_SIZE + 1];
char Password[ DATA_PUBLICATION_PASSWORD_SIZE + 1];
char Url[ DATA_PUBLICATION_URL_SIZE + 1];


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_DATA_PUBLICATION, DataPublicationMenu, (TMenuItemCb *)DataPublicationParameters, &DataPublication, &MData)){
         ConfigDataPublicationSave();
         return;
      }
      switch( MData.Item){
         case MI_INTERFACE :
            i = DataPublication.Interface;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_DATA_PUBLICATION_INTERFACE, _DATA_PUBLICATION_INTERFACE_LAST)){
               break;
            }
            DataPublication.Interface = (byte)i;
            break;

         case MI_USERNAME :
            strcpy( Username, DataPublication.Username);
            if( !DInputText( STR_USERNAME, STR_ENTER_USERNAME, Username, DATA_PUBLICATION_USERNAME_SIZE)){
               break;
            }
            strcpy( DataPublication.Username, Username);
            break;

         case MI_PASSWORD :
            Password[0] = 0;
            if( !DInputText( STR_PASSWORD, STR_ENTER_PASSWORD, Password, DATA_PUBLICATION_PASSWORD_SIZE)){
               break;
            }
            strcpy( DataPublication.Password, Password);
            break;

         case MI_URL :
            strcpy( Url, DataPublication.Url);
            if( !DInputText( STR_URL, STR_ENTER_URL, Url, DATA_PUBLICATION_URL_SIZE)){
               break;
            }
            strcpy( DataPublication.Url, Url);
            break;

         case MI_CONTACTS :
            MenuGsmContacts();
            break;

         case MI_SEND_AT :
            uTime( &DateTime.Time, DataPublication.SendAt);
            if( !DInputTime( STR_, STR_ENTER_, &DateTime.Time)){
               break;
            }
            DataPublication.SendAt = uTimeGauge( &DateTime.Time);
            break;

      }
   }
} // MenuDataPublication

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void DataPublicationParameters( int Index, int y, TDataPublication *Parameters)
// Draw data publication parameters
{
   switch( Index){
      case MI_INTERFACE :
         DLabelEnum( Parameters->Interface, ENUM_DATA_PUBLICATION_INTERFACE, DMENU_PARAMETERS_X, y);
         break;

      case MI_USERNAME :
         DLabelNarrow( Parameters->Username, DMENU_PARAMETERS_X, y);
         break;

      case MI_PASSWORD :
         DLabelNarrow( "*****", DMENU_PARAMETERS_X, y);
         break;

      case MI_URL :
         DLabelNarrow( Parameters->Url, DMENU_PARAMETERS_X, y);
         break;

      case MI_CONTACTS :
         break;

      case MI_SEND_AT :
         DTimeRight( Parameters->SendAt, DMENU_PARAMETERS_X, y);
         break;

   }
} // DataPublicationParameters
