//******************************************************************************
//
//   MenuMaintenance.c  Maintenance menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuMaintenance.h"
#include "Gadget/DMenu.h"         // Display menu
#include "Str.h"                  // Strings

#include "Menu.h"
#include "MenuCountry.h"
#include "MenuWeightUnits.h"
#include "MenuPlatformInfo.h"
#include "MenuMaintenanceCalibration.h"
#include "MenuMaintenanceUsb.h"
#include "MenuMaintenancePower.h"
#include "MenuMaintenanceSimulation.h"
#include "MenuMaintenanceBootloader.h"
#include "MenuMaintenanceFactoryDefaults.h"


static DefMenu( MaintenanceMenu)
   STR_SCALE_NAME,
   STR_COUNTRY,
   STR_WEIGHING,
   STR_PLATFORM,
   STR_CALIBRATION,
   STR_USB,
   STR_POWER,
   STR_SIMULATION,
   STR_BOOTLOADER,
   STR_FACTORY_DEFAULTS,
EndMenu()

typedef enum {
   MI_SCALE_NAME,
   MI_COUNTRY,
   MI_WEIGHING,
   MI_PLATFORM,
   MI_CALIBRATION,
   MI_USB,
   MI_POWER,
   MI_SIMULATION,
   MI_BOOTLOADER,
   MI_FACTORY_DEFAULTS
} EMaintenanceMenu;

//------------------------------------------------------------------------------
//  Menu Maintenance
//------------------------------------------------------------------------------

void MenuMaintenance( void)
// Menu maintenance
{
TMenuData MData;

   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_MAINTENANCE, MaintenanceMenu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_SCALE_NAME :
            strcpy( ScaleName, MainMaintenance.ScaleName);
            if( !DInputText( STR_SCALE_NAME, STR_ENTER_SCALE_NAME, ScaleName, MAIN_MAINTENANCE_SCALE_NAME_SIZE)){
               break;
            }
            strcpy( MainMaintenance.ScaleName, ScaleName);
            break;

         case MI_COUNTRY :
            MenuCountry();
            break;

         case MI_WEIGHING :
            MenuWeightUnits();
            break;

         case MI_PLATFORM :
            MenuPlatformInfo();
            break;

         case MI_CALIBRATION :
            MenuMaintenanceCalibration();
            break;

         case MI_USB :
            MenuMaintenanceUsb();
            break;

         case MI_POWER :
            MenuMaintenancePower();
            break;

         case MI_SIMULATION :
            MenuMaintenanceSimulation();
            break;

         case MI_BOOTLOADER :
            MenuMaintenanceBootloader();
            break;

         case MI_FACTORY_DEFAULTS :
            MenuMaintenanceFactoryDefaults();
            break;

      }
   }
} // MenuMaintenance
