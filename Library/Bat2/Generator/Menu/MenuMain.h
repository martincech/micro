//******************************************************************************
//
//   MenuMain.h  Main menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuMain_H__
   #define __MenuMain_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Menu_H__
   #include "Menu.h"
#endif


void MenuMain( void);
// Menu main

#endif
