//******************************************************************************
//
//   MenuCommunication.c  Communication menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuCommunication.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Menu.h"
#include "MenuGsm.h"
#include "MenuRs485.h"
#include "MenuInternetConnection.h"
#include "MenuUsb.h"


static DefMenu( CommunicationMenu)
   STR_CELLULAR,
   STR_RS485,
   STR_INTERNET_CONNECTION,
   STR_USB,
EndMenu()

typedef enum {
   MI_CELLULAR,
   MI_RS485,
   MI_INTERNET_CONNECTION,
   MI_USB
} ECommunicationMenu;

// Local functions :

static void MainCommunicationParameters( int Index, int y, TMainCommunication *Parameters);
// Draw communication parameters

//------------------------------------------------------------------------------
//  Menu Communication
//------------------------------------------------------------------------------

void MenuCommunication( void)
// Edit communication parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_COMMUNICATION, CommunicationMenu, (TMenuItemCb *)MainCommunicationParameters, &MainCommunication, &MData)){
         ConfigMainCommunicationSave();
         return;
      }
      switch( MData.Item){
         case MI_CELLULAR :
            MenuGsm();
            break;

         case MI_RS485 :
            MenuRs485();
            break;

         case MI_INTERNET_CONNECTION :
            MenuInternetConnection();
            break;

         case MI_USB :
            MenuUsb();
            break;

      }
   }
} // MenuCommunication

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void MainCommunicationParameters( int Index, int y, TMainCommunication *Parameters)
// Draw communication parameters
{
   switch( Index){
      case MI_CELLULAR :
         break;

      case MI_RS485 :
         break;

      case MI_INTERNET_CONNECTION :
         break;

      case MI_USB :
         break;

   }
} // MainCommunicationParameters
