//******************************************************************************
//
//   MenuWeighing.c  Weighing menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWeighing.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Menu.h"
#include "MenuPredefinedWeighings.h"
#include "MenuWeighingPlan.h"
#include "MenuGrowthCurve.h"
#include "MenuCorrectionCurve.h"


static DefMenu( WeighingMenu)
   STR_PREDEFINED_WEIGHINGS,
   STR_DEFAULT_WEIGHING,
   STR_WEIGHING_PLAN,
   STR_GROWTH_CURVES,
   STR_CORRECTION_CURVES,
EndMenu()

typedef enum {
   MI_PREDEFINED_WEIGHINGS,
   MI_DEFAULT_WEIGHING,
   MI_WEIGHING_PLAN,
   MI_GROWTH_CURVES,
   MI_CORRECTION_CURVES
} EWeighingMenu;

// Local functions :

static void MainWeighingParameters( int Index, int y, TMainWeighing *Parameters);
// Draw weighing parameters

//------------------------------------------------------------------------------
//  Menu Weighing
//------------------------------------------------------------------------------

void MenuWeighing( void)
// Edit weighing parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_WEIGHING, WeighingMenu, (TMenuItemCb *)MainWeighingParameters, &MainWeighing, &MData)){
         ConfigMainWeighingSave();
         return;
      }
      switch( MData.Item){
         case MI_PREDEFINED_WEIGHINGS :
            MenuPredefinedWeighings();
            break;

         case MI_DEFAULT_WEIGHING :
            i = MainWeighing.DefaultWeighing;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, MAIN_WEIGHING_DEFAULT_WEIGHING_MIN, MAIN_WEIGHING_DEFAULT_WEIGHING_MAX, 0)){
               break;
            }
            MainWeighing.DefaultWeighing = (TWeighingIndex)i;
            break;

         case MI_WEIGHING_PLAN :
            MenuWeighingPlan();
            break;

         case MI_GROWTH_CURVES :
            MenuGrowthCurve();
            break;

         case MI_CORRECTION_CURVES :
            MenuCorrectionCurve();
            break;

      }
   }
} // MenuWeighing

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void MainWeighingParameters( int Index, int y, TMainWeighing *Parameters)
// Draw weighing parameters
{
   switch( Index){
      case MI_PREDEFINED_WEIGHINGS :
         break;

      case MI_DEFAULT_WEIGHING :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->DefaultWeighing, 0);
         break;

      case MI_WEIGHING_PLAN :
         break;

      case MI_GROWTH_CURVES :
         break;

      case MI_CORRECTION_CURVES :
         break;

   }
} // MainWeighingParameters
