//******************************************************************************
//
//   MenuUserSettings.c  User settings menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuUserSettings.h"
#include "Gadget/DMenu.h"         // Display menu
#include "Str.h"                  // Strings

#include "Menu.h"
#include "MenuBacklightIntensity.h"
#include "MenuContrast.h"
#include "MenuDateTime.h"


static DefMenu( UserSettingsMenu)
   STR_DISPLAY_BACKLIGHT,
   STR_DISPLAY_CONTRAST,
   STR_DATE_AND_TIME,
EndMenu()

typedef enum {
   MI_DISPLAY_BACKLIGHT,
   MI_DISPLAY_CONTRAST,
   MI_DATE_AND_TIME
} EUserSettingsMenu;

//------------------------------------------------------------------------------
//  Menu UserSettings
//------------------------------------------------------------------------------

void MenuUserSettings( void)
// Menu user settings
{
TMenuData MData;

   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_USER_SETTINGS, UserSettingsMenu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_DISPLAY_BACKLIGHT :
            MenuBacklightIntensity();
            break;

         case MI_DISPLAY_CONTRAST :
            MenuContrast();
            break;

         case MI_DATE_AND_TIME :
            MenuDateTime();
            break;

      }
   }
} // MenuUserSettings
