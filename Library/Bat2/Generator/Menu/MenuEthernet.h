//******************************************************************************
//
//   MenuEthernet.h  Ethernet menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuEthernet_H__
   #define __MenuEthernet_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Menu_H__
   #include "Menu.h"
#endif


void MenuEthernet( void);
// Menu ethernet

#endif
