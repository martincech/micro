//******************************************************************************
//
//   MenuInternetConnection.h  Internet connection menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuInternetConnection_H__
   #define __MenuInternetConnection_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Menu_H__
   #include "Menu.h"
#endif


void MenuInternetConnection( void);
// Menu internet connection

#endif
