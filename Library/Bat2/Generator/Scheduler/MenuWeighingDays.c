//******************************************************************************
//
//   MenuWeighingDays.c  Weighing days menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWeighingDays.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Weighing Scheduler.h"


static DefMenu( WeighingDaysMenu)
   STR_MODE,
   STR_DAYS,
   STR_WEIGHING_DAYS,
   STR_SUSPENDED_DAYS,
   STR_STARTDAY,

EndMenu()

typedef enum {
   MI_MODE,
   MI_DAYS,
   MI_WEIGHING_DAYS,
   MI_SUSPENDED_DAYS,
   MI_STARTDAY,

} EWeighingDaysMenu;

// Local functions :

static void WeighingDaysParameters( int Index, int y, TWeighingDays *Parameters);
// Draw weighing days parameters

//------------------------------------------------------------------------------
//  Menu WeighingDays
//------------------------------------------------------------------------------

void MenuWeighingDays( void)
// Edit weighing days parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_WEIGHING_DAYS, WeighingDaysMenu, (TMenuItemCb *)WeighingDaysParameters, &WeighingDays, &MData)){
         ConfigWeighingDaysSave();
         return;
      }
      switch( MData.Item){
         case MI_MODE :
            i = WeighingDays.Mode;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_WEIGHING_DAYS_MODE, _WEIGHING_DAYS_MODE_LAST)){
               break;
            }
            WeighingDays.Mode = (byte)i;
            break;

         case MI_DAYS :
            i = WeighingDays.Days;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_DAYS_DAYS_MIN, WEIGHING_DAYS_DAYS_MAX, 0)){
               break;
            }
            WeighingDays.Days = (byte)i;
            break;

         case MI_WEIGHING_DAYS :
            i = WeighingDays.WeighingDays;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_DAYS_WEIGHING_DAYS_MIN, WEIGHING_DAYS_WEIGHING_DAYS_MAX, 0)){
               break;
            }
            WeighingDays.WeighingDays = (byte)i;
            break;

         case MI_SUSPENDED_DAYS :
            i = WeighingDays.SuspendedDays;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_DAYS_SUSPENDED_DAYS_MIN, WEIGHING_DAYS_SUSPENDED_DAYS_MAX, 0)){
               break;
            }
            WeighingDays.SuspendedDays = (byte)i;
            break;

         case MI_STARTDAY :
            i = WeighingDays.Startday;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_DAYS_STARTDAY_MIN, WEIGHING_DAYS_STARTDAY_MAX, 0)){
               break;
            }
            WeighingDays.Startday = (TDayNumber)i;
            break;

      }
   }
} // MenuWeighingDays

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void WeighingDaysParameters( int Index, int y, TWeighingDays *Parameters)
// Draw weighing days parameters
{
   switch( Index){
      case MI_MODE :
         DLabelEnum( Parameters->Mode, ENUM_WEIGHING_DAYS_MODE, DMENU_PARAMETERS_X, y);
         break;

      case MI_DAYS :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->Days, 0);
         break;

      case MI_WEIGHING_DAYS :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->WeighingDays, 0);
         break;

      case MI_SUSPENDED_DAYS :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->SuspendedDays, 0);
         break;

      case MI_STARTDAY :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->Startday, 0);
         break;

   }
} // WeighingDaysParameters
