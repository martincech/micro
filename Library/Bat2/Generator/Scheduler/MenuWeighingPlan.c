//******************************************************************************
//
//   MenuWeighingPlan.c  Weighing plan menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWeighingPlan.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Weighing Scheduler.h"
#include "MenuWeighingPlanWeighingDays.h"
#include "MenuWeighingPlanWeighingTime.h"


static DefMenu( WeighingPlanMenu)
   STR_PLANNING,
   STR_SYNC_WITH_DAY_START,
   STR_WEIGHING_DAYS,
   STR_WEIGHING_TIME,
EndMenu()

typedef enum {
   MI_PLANNING,
   MI_SYNC_WITH_DAY_START,
   MI_WEIGHING_DAYS,
   MI_WEIGHING_TIME
} EWeighingPlanMenu;

// Local functions :

static void WeighingPlanParameters( int Index, int y, TWeighingPlan *Parameters);
// Draw weighing plan parameters

//------------------------------------------------------------------------------
//  Menu WeighingPlan
//------------------------------------------------------------------------------

void MenuWeighingPlan( void)
// Edit weighing plan parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_WEIGHING_PLAN, WeighingPlanMenu, (TMenuItemCb *)WeighingPlanParameters, &WeighingPlan, &MData)){
         ConfigWeighingPlanSave();
         return;
      }
      switch( MData.Item){
         case MI_PLANNING :
            i = WeighingPlan.Planning;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            WeighingPlan.Planning = (byte)i;
            break;

         case MI_SYNC_WITH_DAY_START :
            i = WeighingPlan.SyncWithDayStart;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            WeighingPlan.SyncWithDayStart = (byte)i;
            break;

         case MI_WEIGHING_DAYS :
            MenuWeighingPlanWeighingDays();
            break;

         case MI_WEIGHING_TIME :
            MenuWeighingPlanWeighingTime();
            break;

      }
   }
} // MenuWeighingPlan

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void WeighingPlanParameters( int Index, int y, TWeighingPlan *Parameters)
// Draw weighing plan parameters
{
   switch( Index){
      case MI_PLANNING :
         DLabelEnum( Parameters->Planning, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

      case MI_SYNC_WITH_DAY_START :
         DLabelEnum( Parameters->SyncWithDayStart, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

      case MI_WEIGHING_DAYS :
         break;

      case MI_WEIGHING_TIME :
         break;

   }
} // WeighingPlanParameters
