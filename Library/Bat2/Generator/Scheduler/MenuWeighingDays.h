//******************************************************************************
//
//   MenuWeighingDays.h  Weighing days menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuWeighingDays_H__
   #define __MenuWeighingDays_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Weighing Scheduler_H__
   #include "Weighing Scheduler.h"
#endif


void MenuWeighingDays( void);
// Menu weighing days

#endif
