TWeighingPlan WeighingPlan;
TWeighingDays WeighingDays;
TWeighingTime WeighingTime;

//------------------------------------------------------------------------------
//  Weighing plan
//------------------------------------------------------------------------------

const TWeighingPlan WeighingPlanDefault = {
   /* Planning */ NO,
   /* SyncWithDayStart */ NO
};

//------------------------------------------------------------------------------
//  Weighing days
//------------------------------------------------------------------------------

const TWeighingDays WeighingDaysDefault = {
   /* Mode */ WEIGHING_DAYS_MODE_DAY_OF_WEEK,
   /* Days */ WEIGHING_DAYS_DAYS_DEFAULT,
   /* WeighingDays */ WEIGHING_DAYS_WEIGHING_DAYS_DEFAULT,
   /* SuspendedDays */ WEIGHING_DAYS_SUSPENDED_DAYS_DEFAULT,
   /* Startday */ WEIGHING_DAYS_STARTDAY_DEFAULT,
   /* _Dummy */ 0
};

//------------------------------------------------------------------------------
//  Weighing time
//------------------------------------------------------------------------------

const TWeighingTime WeighingTimeDefault = {
   /* WeighingFrom */ WEIGHING_TIME_WEIGHING_FROM_DEFAULT,
   /* WeighingTo */ WEIGHING_TIME_WEIGHING_TO_DEFAULT
};

