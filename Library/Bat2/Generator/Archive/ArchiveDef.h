//******************************************************************************
//
//   ArchiveDef.h  Bat2 Archive data
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ArchiveDef_H__
   #define __ArchiveDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif


//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#define ARCHIVE_HISTOGRAM_SLOT_SIZE 39

#define ARCHIVE_STATISTIC_STATISTIC_MIN 0
#define ARCHIVE_STATISTIC_STATISTIC_MAX 0
#define ARCHIVE_STATISTIC_STATISTIC_DEFAULT 

#define ARCHIVE_STATISTIC_UNIFORMITY_MIN 0
#define ARCHIVE_STATISTIC_UNIFORMITY_MAX 1000
#define ARCHIVE_STATISTIC_UNIFORMITY_DEFAULT 0

#define ARCHIVE_STATISTIC_TIMESTAMP_MIN 0
#define ARCHIVE_STATISTIC_TIMESTAMP_MAX 0
#define ARCHIVE_STATISTIC_TIMESTAMP_DEFAULT ?

#define ARCHIVE_STATISTIC_COUNT_MIN 0
#define ARCHIVE_STATISTIC_COUNT_MAX 0
#define ARCHIVE_STATISTIC_COUNT_DEFAULT 0

#define ARCHIVE_STATISTIC_AVERAGE_MIN 0
#define ARCHIVE_STATISTIC_AVERAGE_MAX 0
#define ARCHIVE_STATISTIC_AVERAGE_DEFAULT ?

#define ARCHIVE_STATISTIC_LAST_AVERAGE_MIN 0
#define ARCHIVE_STATISTIC_LAST_AVERAGE_MAX 0
#define ARCHIVE_STATISTIC_LAST_AVERAGE_DEFAULT ?

#define ARCHIVE_STATISTIC_DAILY_GAIN_MIN 0
#define ARCHIVE_STATISTIC_DAILY_GAIN_MAX 0
#define ARCHIVE_STATISTIC_DAILY_GAIN_DEFAULT ?

#define ARCHIVE_STATISTIC_SIGMA_MIN 0
#define ARCHIVE_STATISTIC_SIGMA_MAX 0
#define ARCHIVE_STATISTIC_SIGMA_DEFAULT ?

#define ARCHIVE_MARKER_ZONE_MIN 0
#define ARCHIVE_MARKER_ZONE_MAX 0
#define ARCHIVE_MARKER_ZONE_DEFAULT 

#define ARCHIVE_MARKER_TIMESTAMP_MIN 0
#define ARCHIVE_MARKER_TIMESTAMP_MAX 0
#define ARCHIVE_MARKER_TIMESTAMP_DEFAULT ?

#define ARCHIVE_MARKER_TARGET_WEIGHT_MIN 0
#define ARCHIVE_MARKER_TARGET_WEIGHT_MAX 0
#define ARCHIVE_MARKER_TARGET_WEIGHT_DEFAULT ?

#define ARCHIVE_MARKER_TARGET_WEIGHT_FEMALE_MIN 0
#define ARCHIVE_MARKER_TARGET_WEIGHT_FEMALE_MAX 0
#define ARCHIVE_MARKER_TARGET_WEIGHT_FEMALE_DEFAULT ?




//------------------------------------------------------------------------------
//  Data types
//------------------------------------------------------------------------------

typedef dword TStatisticCount;
typedef word TStatisticIdentifier;
typedef word TZoneIdentifier;

//------------------------------------------------------------------------------
//  Archive histogram
//------------------------------------------------------------------------------

typedef struct {
   byte Slot[ ARCHIVE_HISTOGRAM_SLOT_SIZE]; // Compressed histogram slots
   byte _Dummy; // Dword alignment
} TArchiveHistogram;

//------------------------------------------------------------------------------
//  Archive statistic
//------------------------------------------------------------------------------

typedef struct {
   TStatisticIdentifier Statistic; // Statistic identifier (record originator)
   word Uniformity; // Weight uniformity [0.1%]
   UClockGauge Timestamp; // Statistic timestamp
   TStatisticCount Count; // Weight samples count
   TWeightGauge Average; // Average weight
   TWeightGauge LastAverage; // Last known weight (yesterday average)
   TWeightGauge DailyGain; // Daily Gain (may be negative)
   TWeightGauge Sigma; // Weight standard deviaiton
   TArchiveHistogram Histogram; // Compressed histogram
} TArchiveStatistic;

//------------------------------------------------------------------------------
//  Archive marker
//------------------------------------------------------------------------------

typedef struct {
   TZoneIdentifier Zone; // Zone identifier (record identifier)
   Day Number Day; // Technological day
   UClockGauge Timestamp; // Marker timestamp
   TWeightGauge TargetWeight; // Target weight
   TWeightGauge TargetWeightFemale; // Target weight female
} TArchiveMarker;



//------------------------------------------------------------------------------
#endif
