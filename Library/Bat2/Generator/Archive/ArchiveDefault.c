TArchiveHistogram ArchiveHistogram;
TArchiveStatistic ArchiveStatistic;
TArchiveMarker ArchiveMarker;

//------------------------------------------------------------------------------
//  Archive histogram
//------------------------------------------------------------------------------

TArchiveHistogram ArchiveHistogramDefault = {
   /* Slot */ Array?,
   /* _Dummy */ 0
};

//------------------------------------------------------------------------------
//  Archive statistic
//------------------------------------------------------------------------------

TArchiveStatistic ArchiveStatisticDefault = {
   /* Statistic */ ARCHIVE_STATISTIC_STATISTIC_DEFAULT,
   /* Uniformity */ ARCHIVE_STATISTIC_UNIFORMITY_DEFAULT,
   /* Timestamp */ ARCHIVE_STATISTIC_TIMESTAMP_DEFAULT,
   /* Count */ ARCHIVE_STATISTIC_COUNT_DEFAULT,
   /* Average */ ARCHIVE_STATISTIC_AVERAGE_DEFAULT,
   /* LastAverage */ ARCHIVE_STATISTIC_LAST_AVERAGE_DEFAULT,
   /* DailyGain */ ARCHIVE_STATISTIC_DAILY_GAIN_DEFAULT,
   /* Sigma */ ARCHIVE_STATISTIC_SIGMA_DEFAULT,
   /* Archivehistogram */ ArchiveHistogram?
};

//------------------------------------------------------------------------------
//  Archive marker
//------------------------------------------------------------------------------

TArchiveMarker ArchiveMarkerDefault = {
   /* Zone */ ARCHIVE_MARKER_ZONE_DEFAULT,
   /* Day */ ?,
   /* Timestamp */ ARCHIVE_MARKER_TIMESTAMP_DEFAULT,
   /* TargetWeight */ ARCHIVE_MARKER_TARGET_WEIGHT_DEFAULT,
   /* TargetWeightFemale */ ARCHIVE_MARKER_TARGET_WEIGHT_FEMALE_DEFAULT
};

