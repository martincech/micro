//******************************************************************************
//
//   MessageTest.c Message Test
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "Test/TestRunner.h"

#include "Scheduler/WeighingScheduler.h"
#include "Weighing/WeighingConfiguration.h"
#include "System/System.h"
#include "Timer/Timer.h"
#include "Message/GsmMessage.h"             // GSM messaging configuration

//------------------------------------------------------------------------------
//   Main loop
//------------------------------------------------------------------------------

int MenuExecute( void)
// Main menu loop
{
   UnityMain(0, 0, TestRunner);
} // MenuExecute

//------------------------------------------------------------------------------
//   Unity
//------------------------------------------------------------------------------

TEST_GROUP( Message);

TEST_SETUP( Message)
{
}

TEST_TEAR_DOWN( Message)
{
   fflush(stdout);
}

#define RUN_TIME        3
#define SENDING_TIME    40
#define MESSAGE_DELAY   1

static void _SysDateTimeSet( UDateTimeGauge DateTimeGauge) {
UDateTime Time;
   uDateTime(&Time, DateTimeGauge);
   printf("%.2d.%.2d.%.2d %.02d:%.02d:%.02d\n", Time.Date.Day, Time.Date.Month, Time.Date.Day, Time.Time.Hour, Time.Time.Min, Time.Time.Sec);
   SysDateTimeSet(DateTimeGauge);
}

static void TimeSet(int Hour, int Minute, int Second) {
UDateTimeGauge DateTime;
UDateGauge     Date;
UTimeGauge     Time;
   DateTime = SysDateTime();
   Date     = uDateTimeDate( DateTime);
   Time     = Hour * TIME_HOUR + Minute * TIME_MIN + Second * TIME_SEC;
   DateTime = uDateTimeCompose( Date, Time);
   _SysDateTimeSet(DateTime);
}

static void TimeChange(int Hour, int Minute, int Second) {
UDateTimeGauge DateTime;
   DateTime = SysDateTime() + Hour * TIME_HOUR + Minute * TIME_MIN + Second * TIME_SEC;
   _SysDateTimeSet(DateTime);
}

static void DateChange(int Change) {
UDateTimeGauge DateTime;
UDateGauge     Date;
UTimeGauge     Time;
   DateTime = SysDateTime();
   Date     = uDateTimeDate( DateTime) + Change;
   Time     = uDateTimeTime( DateTime);
   DateTime = uDateTimeCompose( Date, Time);
   _SysDateTimeSet(DateTime);
}

TEST( Message, Send)
{
dword Timeout;
   printf("--- Send ---\n");
   TimeSet(0, 0, 0);

   GsmMessage.PowerOptions.Mode = GSM_POWER_MODE_ON;
   WeighingDayDurationSet( 24 * TIME_HOUR);
   GsmMessage.Statistics.SendAt = 13 * TIME_HOUR;

   SysSchedulerEnable(YES);
   WeighingSchedulerStop();
   WeighingSchedulerStart();

   Timeout = SysClock() + RUN_TIME;
   while(TimeBefore(SysDateTime(), Timeout)) {
      SysScheduler();
   }

   DateChange(1);
   TimeSet(0, 0, 0);

   Timeout = SysClock() + RUN_TIME;
   while(TimeBefore(SysClock(), Timeout)) {
      SysScheduler();
   }

   DateChange(0);
   TimeSet(12, 59, 50);

   Timeout = SysClock() + SENDING_TIME;
   while(TimeBefore(SysClock(), Timeout)) {
      SysScheduler();
   }
}

#define SMS_SEND_TIMEOUT         180  // s, nutno upravit podle skutecnosti
#define SMS_SEND_RETRY_TIMEOUT   1800 // s, nutno upravit podle skutecnosti

TEST( Message, SendTimeout)
{
dword Timeout;
   printf("--- Send timeout ---\n");
   printf("Timeout: %d, retry: %d\n", SMS_SEND_TIMEOUT, SMS_SEND_RETRY_TIMEOUT);

   TimeSet(0, 0, 0);

   GsmMessage.PowerOptions.Mode = GSM_POWER_MODE_ON;
   WeighingDayDurationSet( 24 * TIME_HOUR);
   GsmMessage.Statistics.SendAt = 13 * TIME_HOUR;

   SysSchedulerEnable(YES);
   WeighingSchedulerStop();
   WeighingSchedulerStart();

   Timeout = SysClock() + RUN_TIME;
   while(TimeBefore(SysDateTime(), Timeout)) {
      SysScheduler();
   }

   DateChange(1);
   TimeSet(0, 0, 0);

   Timeout = SysClock() + RUN_TIME;
   while(TimeBefore(SysClock(), Timeout)) {
      SysScheduler();
   }

   DateChange(0);
   TimeSet(14, 00, 00);
   printf("Start send\n");
   Timeout = SysClock() + RUN_TIME;
   while(TimeBefore(SysClock(), Timeout)) {
      SysScheduler();
   }

   TimeSet(14, 00, 00);
   TimeChange(0, 0, SMS_SEND_TIMEOUT - 2);
   printf("Timeout\n");

   Timeout = SysClock() + SENDING_TIME;
   while(TimeBefore(SysClock(), Timeout)) {
      SysScheduler();
   }

   TimeSet(14, 00, 00);
   TimeChange(0, 0, SMS_SEND_TIMEOUT + SMS_SEND_RETRY_TIMEOUT - 2);
   printf("Retry\n");

   Timeout = SysClock() + SENDING_TIME;
   while(TimeBefore(SysClock(), Timeout)) {
      SysScheduler();
   }
}

TEST( Message, PeriodicMode)
{
dword Timeout;
   printf("--- Periodic mode ---\n");
   TimeSet(0, 0, 0);

   GsmMessage.Commands.Enabled = YES;
   GsmMessage.PowerOptions.Mode = GSM_POWER_MODE_PERIODIC;
   GsmMessage.PowerOptions.SwitchOnPeriod = 2;
   GsmMessage.PowerOptions.SwitchOnDuration = 1;
   SysSchedulerEnable(YES);
   WeighingSchedulerStop();

   printf("Period: %d, duration: %d\n", GsmMessage.PowerOptions.SwitchOnPeriod, GsmMessage.PowerOptions.SwitchOnDuration);

   Timeout = SysClock() + 5;
   while(TimeBefore(SysDateTime(), Timeout)) {
      SysScheduler();
   }

   TimeSet(0, 0, 30);

   Timeout = SysClock() + 5;
   while(TimeBefore(SysClock(), Timeout)) {
      SysScheduler();
   }

   TimeSet(0, 1, 0);

   Timeout = SysClock() + 5;
   while(TimeBefore(SysClock(), Timeout)) {
      SysScheduler();
   }

   TimeSet(0, 1, 30);

   Timeout = SysClock() + 5;
   while(TimeBefore(SysClock(), Timeout)) {
      SysScheduler();
   }

   TimeSet(0, 2, 0);

   Timeout = SysClock() + 5;
   while(TimeBefore(SysClock(), Timeout)) {
      SysScheduler();
   }

   TimeSet(0, 2, 30);

   Timeout = SysClock() + 5;
   while(TimeBefore(SysClock(), Timeout)) {
      SysScheduler();
   }

   TimeSet(0, 3, 0);

   Timeout = SysClock() + 5;
   while(TimeBefore(SysClock(), Timeout)) {
      SysScheduler();
   }

   TimeSet(0, 3, 30);

   Timeout = SysClock() + 5;
   while(TimeBefore(SysClock(), Timeout)) {
      SysScheduler();
   }

   TimeSet(0, 4, 0);

   Timeout = SysClock() + 5;
   while(TimeBefore(SysClock(), Timeout)) {
      SysScheduler();
   }

   TimeSet(0, 4, 30);

   Timeout = SysClock() + 5;
   while(TimeBefore(SysClock(), Timeout)) {
      SysScheduler();
   }
}


TEST( Message, SendWithPeriodicModeAndCommandEnabled)
{
dword Timeout;
   printf("--- Send ---\n");
   TimeSet(0, 0, 0);

   GsmMessage.Commands.Enabled = YES;
   GsmMessage.PowerOptions.Mode = GSM_POWER_MODE_PERIODIC;
   GsmMessage.PowerOptions.SwitchOnPeriod = 2;
   GsmMessage.PowerOptions.SwitchOnDuration = 1;
   WeighingDayDurationSet( 24 * TIME_HOUR);
   GsmMessage.Statistics.SendAt = 13 * TIME_HOUR;

   SysSchedulerEnable(YES);
   WeighingSchedulerStop();
   WeighingSchedulerStart();

   Timeout = SysClock() + RUN_TIME;
   while(TimeBefore(SysDateTime(), Timeout)) {
      SysScheduler();
   }

   DateChange(1);
   TimeSet(0, 0, 0);

   Timeout = SysClock() + RUN_TIME;
   while(TimeBefore(SysClock(), Timeout)) {
      SysScheduler();
   }

   DateChange(0);
   TimeSet(12, 59, 50);

   Timeout = SysClock() + SENDING_TIME;
   while(TimeBefore(SysClock(), Timeout)) {
      SysScheduler();
   }
}

//******************************************************************************

TEST_GROUP_RUNNER( Message)
{
   //RUN_TEST_CASE( Message, Send);
   //RUN_TEST_CASE( Message, SendTimeout);
   //RUN_TEST_CASE( Message, PeriodicMode);
   RUN_TEST_CASE( Message, SendWithPeriodicModeAndCommandEnabled);
}

void TestRunner( void) {
   RUN_TEST_GROUP( Message);
}
