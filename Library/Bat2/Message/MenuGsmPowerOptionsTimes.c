//******************************************************************************
//
//   MenuGsmPowerOptionsTimes.h     Menu for GSM power options time plan settings
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "MenuGsmPowerOptionsTimes.h"
#include "Gadget/DMenu.h"                // Display menu
#include "Gadget/DTimeRange.h"           // Time range edit
#include "Str.h"                         // Strings

static DefMenu( GsmPowerTimesMenu)
   STR_EDIT,
   STR_CREATE,
   STR_DELETE,
EndMenu()

typedef enum {
   MI_EDIT,
   MI_CREATE,
   MI_DELETE
} EWeighingTimesMenu;

void MenuGsmPowerOptionsTimes( TGsmPowerOptions *PowerOpt)
// Menu weighing times
{
TMenuData   MData;
TTimeRange  TimeRange;
int         TimesCount;

   DMenuClear( MData);
   TimesCount = PowerOpt->TimesCount;
   forever {
      // check for directory capacity :
      MData.Mask = 0;
      if( TimesCount == GSM_POWER_OPTION_TIME_COUNT){
         MData.Mask |= (1 << MI_CREATE);              // disable create
         if( MData.Item == MI_CREATE){
            MData.Item = MI_EDIT;
         }
      }
      // check for empty list :
      if( TimesCount <= 1){
         MData.Mask |= (1 << MI_DELETE);              // disable delete
         if( MData.Item == MI_DELETE){
            MData.Item = MI_EDIT;
         }
      }
      // selection :
      if( !DMenu( STR_SWITCH_ON_PERIOD, GsmPowerTimesMenu, 0, 0, &MData)){
         return;
      }

      switch( MData.Item){
         case MI_EDIT :
            DTimeRangeEdit( STR_SWITCH_ON_TIME, PowerOpt->Times, TimesCount);
            break;

         case MI_CREATE :
            memcpy( &TimeRange, &TimeRangeDefault, sizeof( TTimeRange)); // fill with defaults
            DTimeRangeCreate( &TimeRange);
            if( memequ( &TimeRange, &TimeRangeDefault, sizeof( TTimeRange))){
               break;                                          // not modified by user
            }

            PowerOpt->Times[ PowerOpt->TimesCount] = TimeRange;
            PowerOpt->TimesCount++;
            TimesCount = PowerOpt->TimesCount;
            break;

         case MI_DELETE :
            DTimeRangeDelete( STR_SWITCH_ON_TIME, PowerOpt->Times, &TimesCount);
            if( PowerOpt->TimesCount != TimesCount){
               PowerOpt->TimesCount = TimesCount;
            }
            break;
      }
   }
}
