//******************************************************************************
//
//   MenuGsmPinCode.h  GSM PIN enter
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuGsmPinCode_H__
   #define __MenuGsmPinCode_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuGsmPinCode( void);
// Menu GSM PIN code enter

#endif
