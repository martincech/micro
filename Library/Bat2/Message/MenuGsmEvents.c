//******************************************************************************
//
//   MenuGsmEvents.c  Gsm events menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuGsmEvents.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Weight/DWeight.h"       // Display edit weight
#include "Gadget/DCheckList.h"         // Display checklist
#include "Str.h"                  // Strings

#include "Message/GsmMessage.h"

static DefCheckList( EventSelectionList)
   STR_STATUS_CHANGED,
   STR_DEVICE_FAILURE,
EndCheckList()

// Local functions :

//------------------------------------------------------------------------------
//  Menu GsmEvents
//------------------------------------------------------------------------------

void MenuGsmEvents( void)
// Edit gsm events parameters
{
dword Mask;

   Mask = GsmMessage.Events.EventMask;
   if( !DCheckList( STR_EVENTS, EventSelectionList, &Mask)){
      return;
   }
   GsmMessage.Events.EventMask = Mask;
} // MenuGsmEvents

//******************************************************************************

