//******************************************************************************
//
//   MenuGsmContacts.h  Gsm contacts menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuGsmContacts_H__
   #define __MenuGsmContacts_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __MenuGsm_H__
   #include "MenuGsm.h"
#endif


void MenuGsmContacts( void);
// Menu gsm contacts

void MenuGsmContactsSendStatisticsCheckList(void);
// Menu for selection GSM contact from list of all contacts
#endif
