//******************************************************************************
//
//   ContactList.c  Contacts list
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "ContactList.h"
#include "Data/uList.h"
#include "Message/GsmMessage.h"
#include "Multitasking/Multitasking.h"
#include <string.h>

uListAlloc( _ContactListDescriptor, CONTACT_COUNT, sizeof( TGsmContact));

//------------------------------------------------------------------------------
//  Remote load
//------------------------------------------------------------------------------

TYesNo ContactListRemoteLoad( void)
// Load from remote device
{

   if (!uListCopy(&_ContactListDescriptor, FILE_CONTACT_LIST_LOCAL, FILE_CONTACT_LIST_REMOTE)){
      return NO;
   }
   return YES;
} // ContactListReload

//------------------------------------------------------------------------------
//  Remote save
//------------------------------------------------------------------------------

TYesNo ContactListRemoteSave( void)
// Save to remote device
{
   return uListCopy( &_ContactListDescriptor, FILE_CONTACT_LIST_REMOTE, FILE_CONTACT_LIST_LOCAL);
} // ContactListRemoteSave

//------------------------------------------------------------------------------
//   Initialize
//------------------------------------------------------------------------------

void ContactListInit( void)
// Initialize
{
   uListInit( &_ContactListDescriptor, FILE_CONTACT_LIST_LOCAL);
} // ContactListInit

/*
//------------------------------------------------------------------------------
//   List
//------------------------------------------------------------------------------

const UList *ContactList( void)
// Returns phone number list descriptor
{
   return( &_ContactList);
} // ContactList
*/

//------------------------------------------------------------------------------
//  Open
//------------------------------------------------------------------------------

TYesNo ContactListOpen( TContactList *ContactList)
// Open contact list
{
TYesNo Success;
   Success = uListOpen( ContactList, &_ContactListDescriptor, FILE_CONTACT_LIST_LOCAL);
   if(!Success) {
      MultitaskingReschedule();
   }
   return Success;
} // ContactListOpen

//------------------------------------------------------------------------------
//  Close
//------------------------------------------------------------------------------

void ContactListClose( TContactList *ContactList)
// Close contact list
{
   uListClose( ContactList);
} // ContactListClose

//------------------------------------------------------------------------------
//   Count
//------------------------------------------------------------------------------

TContactIndex ContactListCount( TContactList *ContactList)
// Returns actual list items count
{
   return( uListCount( ContactList));
} // ContactListCount

//------------------------------------------------------------------------------
//   Capacity
//------------------------------------------------------------------------------

TContactIndex ContactListCapacity( void)
// Returns maximum list items count
{
   return( uListCapacity( &_ContactListDescriptor));
} // ContactListCapacity

//------------------------------------------------------------------------------
//   Add
//------------------------------------------------------------------------------

TContactIndex ContactListAdd( TContactList *ContactList, TGsmContact *Contact)
// Add <Contact> returns index
{
   return( uListAppend( ContactList, Contact));
} // ContactListAdd

//------------------------------------------------------------------------------
//   Delete
//------------------------------------------------------------------------------

void ContactListDelete( TContactList *ContactList, TContactIndex Index)
// Add <Contact> returns index
{
   uListDelete( ContactList, Index);
} // ContactListDelete

//------------------------------------------------------------------------------
//   Load
//------------------------------------------------------------------------------

void ContactListLoad( TContactList *ContactList, TGsmContact *Contact, TContactIndex Index)
// Load <Contact> by <Index>
{
   uListItemLoad( ContactList, Index, Contact);
} // ContactListLoad

TYesNo ContactListLoadByPhoneNumber( TContactList *ContactList, TGsmContact *c, char *PhoneNumber)
// Load <Contact> by <ContactListLoadByPhoneNumber
{
TContactIndex i, Count;
TGsmContact   Contact;

   c = NULL;
   Count = ContactListCount( ContactList);
   for( i = 0; i < Count; i++){
      ContactListLoad( ContactList, &Contact, i);
      if( strequ( Contact.PhoneNumber, PhoneNumber)){
         *c = Contact;
         return YES;
      }
   }
   return NO;
}

//------------------------------------------------------------------------------
//   Save
//------------------------------------------------------------------------------

void ContactListSave( TContactList *ContactList, TGsmContact *Contact, TContactIndex Index)
// Save <Contact> at <Index>
{
   uListItemSave( ContactList, Index, Contact);
} // ContactListSave

//------------------------------------------------------------------------------
//   Commands enabled
//------------------------------------------------------------------------------

TYesNo ContactListCommandEnabled( TContactList *ContactList, char *PhoneNumber)
// Returns YES on enabled command by <PhoneNumber>
{
TGsmContact   Contact;
   if( !ContactListLoadByPhoneNumber(ContactList, &Contact, PhoneNumber)){
      return NO;
   }
   return( Contact.Commands);
} // ContactListCommandEnabled

EGsmSmsFormat ContactListSmsFormat( TContactList *ContactList, char *PhoneNumber)
// Returns EGsmSmsFormat of contact with PhoneNumber, if no contact with this PhoneNumber then _GSM_SMS_FORMAT_LAST is returned
{
TGsmContact   Contact;

   if( !ContactListLoadByPhoneNumber(ContactList, &Contact, PhoneNumber)){
      return _GSM_SMS_FORMAT_LAST;
   }
   return Contact.SmsFormat;
}

//------------------------------------------------------------------------------
//   Statistics enabled
//------------------------------------------------------------------------------

TYesNo ContactListStatisticEnabled( TContactList *ContactList, TContactIndex Index)
// Returns YES for enabled statitistics at <Index>
{
TGsmContact Contact;

   ContactListLoad( ContactList, &Contact, Index);
   return Contact.Statistics;
} // ContactListStatisticEnabled

//------------------------------------------------------------------------------
//   Events enabled
//------------------------------------------------------------------------------

TYesNo ContactListEventEnabled( TContactList *ContactList, TContactIndex Index)
// Returns YES for enabled events at <Index>
{
TGsmContact Contact;

   ContactListLoad( ContactList, &Contact, Index);
   return Contact.Events;

} // ContactListEventEnabled

//------------------------------------------------------------------------------
//   Phone number
//------------------------------------------------------------------------------

void ContactListPhoneNumber( TContactList *ContactList, char *PhoneNumber, TContactIndex Index)
// Returns <PhoneNumber> at <Index>
{
TGsmContact Contact;

   ContactListLoad( ContactList, &Contact, Index);
   strcpy( PhoneNumber, Contact.PhoneNumber);
} // ContactListPhoneNumber
