//******************************************************************************
//
//   MenuGsmInfo.c  GSM info
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "MenuGsmInfo.h"
#include "System/System.h"        // Timer only
#include "Sound/Beep.h"           // Beep
#include "Graphic/Graphic.h"      // Graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLayout.h"       // Screen layout
#include "Gadget/DEvent.h"        // Display events
#include "Gadget/DLabel.h"
#include "Gadget/DMsg.h"
#include "Str.h"                  // Strings
#include "Fonts.h"                // Project fonts
#include "Message/Message.h"      // GSM messaging services
#include "Message/MGsm.h"         // GSM module services
#include "Multitasking/Multitasking.h"
#include <string.h>

#define GSM_INFO_PERIOD    1

#if GSM_INFO_PERIOD < 1
   #define GSM_INFO_COUNTER  1
#else
   #define GSM_INFO_COUNTER  TimerSlowCount( GSM_INFO_PERIOD)
#endif
#define GsmInfoTimerSet()    _GsmInfoTimer = GSM_INFO_COUNTER
#define GsmInfoTimerStart()  _GsmInfoTimer = 1   // execute immediately
#define GsmInfoTimerTick()  (--_GsmInfoTimer == 0)

static byte _GsmInfoTimer;

#define INFO_X  3
#define INFO_Y  20
#define CHAR_H  18

//------------------------------------------------------------------------------
//  Menu GSM info
//------------------------------------------------------------------------------

void MenuGsmInfo( void)
// Menu GSM PIN enter
{
TYesNo Redraw;
TYesNo PinOk;
TYesNo Registered;
char   Operator[ GSM_OPERATOR_SIZE + 1];
char   PhoneNumber[ GSM_PHONE_NUMBER_SIZE + 1];
int    Signal;
int    SignalNew;
EMGsmStatus Status;

   PinOk       = NO;
   Registered  = NO;
   strcpy( Operator, "?");
   strcpy( PhoneNumber, "?");
   Signal      = GSM_SIGNAL_UNDEFINED;

   MGsmPowerOn();                      // immediately power on
   GsmInfoTimerStart();                // start service timer
   DMsgWait();
   Redraw = NO;
   forever {
      if( Redraw){
         GClear();
         // frame :
         DLayoutTitle( STR_GSM_INFO);
         DLayoutStatus( STR_BTN_CANCEL, 0, 0);
         // info :
         SetFont( TAHOMA16);
         if( MGsmStatus() == MGSM_STATUS_ERROR){
            DLabelCenter(STR_GSM_MODEM_ERROR,0, (G_HEIGHT - DLAYOUT_STATUS_H - DLAYOUT_TITLE_H)/2, G_WIDTH, 0);
         } else if (MGsmStatus() == MGSM_STATUS_WAIT_PIN){
            DLabelCenter(STR_WRONG_PIN_NUMBER, 0, (G_HEIGHT - DLAYOUT_STATUS_H - DLAYOUT_TITLE_H)/2, G_WIDTH, 0);
         } else {
            //--------------------------------------------------------------------------
            GTextAt( INFO_X, INFO_Y);
            cprintf( "%s : %s", StrGet(STR_PIN), StrGet(PinOk ? STR_VALID : STR_INVALID));

            GTextAt( INFO_X, INFO_Y + 1 * CHAR_H);
            cprintf( "%s : %s", StrGet(STR_REGISTERED), StrGet(Registered ? STR_YES: STR_NO));

            GTextAt( INFO_X, INFO_Y + 2 * CHAR_H);
            cprintf( "%s : %s", StrGet(STR_OPERATOR), Operator);

            GTextAt( INFO_X, INFO_Y + 3 * CHAR_H);
            cprintf( "%s : %s", StrGet(STR_PHONE_NUMBER),PhoneNumber);

            GTextAt( INFO_X, INFO_Y + 4 * CHAR_H);
            if( Signal == GSM_SIGNAL_UNDEFINED){
               cprintf( "%s : ?", StrGet(STR_SIGNAL_STRENGTH));
            } else {
               cprintf( "%s : %d", StrGet(STR_SIGNAL_STRENGTH), Signal);
            }
            //--------------------------------------------------------------------------
         }
         GFlush();
         Redraw = NO;
      }

      switch( DEventWait()){
         case K_FLASH1 :
            // check for service period :
            if( !GsmInfoTimerTick()){
               break;
            }
            GsmInfoTimerSet();
            Status = MGsmStatus();
            if( Status == MGSM_STATUS_ERROR || Status == MGSM_STATUS_WAIT_PIN){
               Redraw = YES;
               break;
            }

            if( Status == MGSM_STATUS_NO_PIN ||
                Status == MGSM_STATUS_CHECK_REGISTERED ||
                Status == MGSM_STATUS_SEND){
               break;
            }
            // read data :
            PinOk      = MGsmPinReady();
            Registered = MGsmRegistered();
            MGsmOperator( Operator);
            MGsmPhoneNumber( PhoneNumber);
            SignalNew  = MGsmSignalStrength();
            if( SignalNew != GSM_SIGNAL_UNDEFINED){
               Signal = SignalNew;
            }
            Redraw = YES;
            break;

         case K_ENTER :
         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            MGsmPowerResume();         // resume by current power mode
            return;
      }
   }
} // MenuGsmInfo
