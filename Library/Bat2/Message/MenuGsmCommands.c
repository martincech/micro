//******************************************************************************
//
//   MenuGsmCommands.c  Gsm commands menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuGsmCommands.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Message/GsmMessage.h"


static DefMenu( GsmCommandsMenu)
   STR_ENABLED,
   STR_CHECK_PHONE_NUMBER,
   STR_EXPIRATION,
EndMenu()

typedef enum {
   MI_ENABLED,
   MI_CHECK_PHONE_NUMBER,
   MI_EXPIRATION
} EGsmCommandsMenu;

// Local functions :

static void GsmCommandsParameters( int Index, int y, TGsmCommands *Parameters);
// Draw gsm commands parameters

//------------------------------------------------------------------------------
//  Menu GsmCommands
//------------------------------------------------------------------------------

void MenuGsmCommands( void)
// Edit gsm commands parameters
{
TMenuData MData;
int       i;

   DMenuClear( MData);
   forever {
      //>>> item enable
      MData.Mask = 0;
      if( !GsmMessage.Commands.Enabled){
         MData.Mask |= (1 << MI_CHECK_PHONE_NUMBER) | (1 << MI_EXPIRATION);
      }
      //<<< item enable
      // selection :
      if( !DMenu( STR_COMMANDS, GsmCommandsMenu, (TMenuItemCb *)GsmCommandsParameters, &GsmMessage.Commands, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_ENABLED :
            i = GsmMessage.Commands.Enabled;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            GsmMessage.Commands.Enabled = (byte)i;
            break;

         case MI_CHECK_PHONE_NUMBER :
            i = GsmMessage.Commands.CheckPhoneNumber;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            GsmMessage.Commands.CheckPhoneNumber = (byte)i;
            break;

         case MI_EXPIRATION :
            i = GsmMessage.Commands.Expiration;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, GSM_COMMANDS_EXPIRATION_MIN, GSM_COMMANDS_EXPIRATION_MAX, "min")){
               break;
            }
            GsmMessage.Commands.Expiration = (word)i;
            break;

      }
   }
} // MenuGsmCommands

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void GsmCommandsParameters( int Index, int y, TGsmCommands *Parameters)
// Draw gsm commands parameters
{
   switch( Index){
      case MI_ENABLED :
         DLabelEnum( Parameters->Enabled, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

      case MI_CHECK_PHONE_NUMBER :
         DLabelEnum( Parameters->CheckPhoneNumber, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

      case MI_EXPIRATION :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Parameters->Expiration, "min");
         break;

   }
} // GsmCommandsParameters
