//******************************************************************************
//
//   MenuGsmPowerOptions.c  GSM power options menu
//   Version 1.0            (c) VEIT Electronics
//
//******************************************************************************

#include "MenuGsmPowerOptions.h"
#include "MenuGsmPowerOptionsTimes.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Str.h"                  // Strings

#include "Message/GsmMessage.h"

static DefMenu( GsmPowerOptionsMenu)
   STR_MODE,
   STR_SWITCH_ON_PERIOD,
   STR_SWITCH_ON_DURATION,
   STR_TIME_PLAN,
EndMenu()

typedef enum {
   MI_MODE,
   MI_SWITCH_ON_PERIOD,
   MI_SWITCH_ON_DURATION,
   MI_TIME_PLAN
} EGsmPowerOptionsMenu;

// Local functions :

static void GsmPowerOptionsParameters( int Index, int y, TGsmPowerOptions *Parameters);
// Draw gsm power options parameters

//------------------------------------------------------------------------------
//  Menu GsmPowerOptions
//------------------------------------------------------------------------------

void MenuGsmPowerOptions( void)
// Edit gsm power options parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      //>>> item enable
      MData.Mask = 0;
      if( GsmMessage.PowerOptions.Mode != GSM_POWER_MODE_PERIODIC){
         MData.Mask = (1 << MI_SWITCH_ON_PERIOD) | (1 << MI_SWITCH_ON_DURATION);
      }
      if( GsmMessage.PowerOptions.Mode != GSM_POWER_MODE_TIME_PLAN){
         MData.Mask |= (1 << MI_TIME_PLAN);
      }
      //<<< item enable
      // selection :
      if( !DMenu( STR_POWER_OPTIONS, GsmPowerOptionsMenu, (TMenuItemCb *)GsmPowerOptionsParameters, &GsmMessage.PowerOptions, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_MODE :
            i = GsmMessage.PowerOptions.Mode;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_GSM_POWER_MODE, _GSM_POWER_MODE_LAST)){
               break;
            }
            GsmMessage.PowerOptions.Mode = (byte)i;
            break;

         case MI_SWITCH_ON_PERIOD :
            i = GsmMessage.PowerOptions.SwitchOnPeriod;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, GSM_POWER_OPTIONS_SWITCH_ON_PERIOD_MIN, GSM_POWER_OPTIONS_SWITCH_ON_PERIOD_MAX, "min")){
               break;
            }
            GsmMessage.PowerOptions.SwitchOnPeriod = (byte)i;
            break;

         case MI_SWITCH_ON_DURATION :
            i = GsmMessage.PowerOptions.SwitchOnDuration;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, GSM_POWER_OPTIONS_SWITCH_ON_DURATION_MIN, GSM_POWER_OPTIONS_SWITCH_ON_DURATION_MAX, "min")){
               break;
            }
            GsmMessage.PowerOptions.SwitchOnDuration = (byte)i;
            break;

         case MI_TIME_PLAN:
            MenuGsmPowerOptionsTimes( &GsmMessage.PowerOptions);
            break;

      }
   }
} // MenuGsmPowerOptions

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void GsmPowerOptionsParameters( int Index, int y, TGsmPowerOptions *Parameters)
// Draw gsm power options parameters
{
   switch( Index){
      case MI_MODE :
         DLabelEnum( Parameters->Mode, ENUM_GSM_POWER_MODE, DMENU_PARAMETERS_X, y);
         break;

      case MI_SWITCH_ON_PERIOD :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Parameters->SwitchOnPeriod, "min");
         break;

      case MI_SWITCH_ON_DURATION :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Parameters->SwitchOnDuration, "min");
         break;

   }
} // GsmPowerOptionsParameters
