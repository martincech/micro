//******************************************************************************
//
//   MenuGsmContacts.c  Gsm contacts menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuGsmContacts.h"
#include "Graphic/Graphic.h"             // Graphic display
#include "Console/conio.h"               // Console
#include "Gadget/DMenu.h"                // Display menu
#include "Str.h"                         // Strings
#include "Gadget/DCursor.h"              // List Cursor
#include "Gadget/DList.h"                // List utility
#include "Gadget/DMsg.h"                 // Message box
#include "Gadget/DLayout.h"              // Display layout
#include "Gadget/DEvent.h"               // Event manager
#include "Gadget/DCheckList.h"         // Display checklist
#include "Sound/Beep.h"                  // Sounds
#include "Data/uList.h"                  // Sequential list
#include "Message/ContactList.h"         // Contact list
#include "Message/GsmMessage.h"          // Default Contact
#include "Message/MenuGsmContact.h"      // Contact edit
#include "Bitmap.h"

static DefMenu( GsmContactsMenu)
   STR_EDIT,
   STR_CREATE,
   STR_DELETE,
EndMenu()

typedef enum {
   MI_EDIT,
   MI_CREATE,
   MI_DELETE
} EGsmContactsMenu;

// Local functions :

static void _CorrectValidity( TGsmContact *Contact);
// check and correct validity of gsm contact fields
static TContactIndex _ContactSelect( TContactList *ContactList);
// Select item from Contacts list

static void DisplayPage( TListCursor *Cursor, TContactList *ContactList);
// Display contact page

//------------------------------------------------------------------------------
//  Menu GsmContacts
//------------------------------------------------------------------------------

void MenuGsmContacts( void)
// Menu gsm contacts
{
TMenuData     MData;
TGsmContact   Contact;
TContactIndex Index;
TContactList  ContactList;

   while(!ContactListOpen( &ContactList));
   DMenuClear( MData);
   forever {
      // check for directory capacity :
      MData.Mask = 0;
      if( ContactListCount( &ContactList) == ContactListCapacity()){
         MData.Mask |= (1 << MI_CREATE);              // disable create
      }
      // check for empty list :
      if( ContactListCount( &ContactList) == 0){
         MData.Mask |= (1 << MI_EDIT);                // disable edit
         MData.Mask |= (1 << MI_DELETE);              // disable delete
      }
      // selection :
      if( !DMenu( STR_CONTACTS, GsmContactsMenu, 0, 0, &MData)){
         ContactListClose( &ContactList);
         return;
      }
      // prepare contact for edit :
      Index = ULIST_INDEX_INVALID;                    // let compiler be happy
      if( MData.Item != MI_CREATE){
         // select contact from list
         Index = _ContactSelect( &ContactList);
         if( Index == ULIST_INDEX_INVALID){
            continue;                                  // selection canceled
         }
      }
      switch( MData.Item){
         case MI_EDIT :
            ContactListLoad( &ContactList, &Contact, Index);
            MenuGsmContact( &Contact);
            _CorrectValidity( &Contact);
            ContactListSave( &ContactList, &Contact, Index);
            continue;

         case MI_CREATE :
            memcpy( &Contact, &GsmContactDefault, sizeof( TGsmContact)); // fill with defaults
            MenuGsmContact( &Contact);
            if( memequ( &Contact, &GsmContactDefault, sizeof( TGsmContact))){
               continue;                                // not modified by user
            }
            if( strequ( Contact.Name, GSM_CONTACT_NAME_DEFAULT)){
               // name not entered - set same as phone number
               strncpyx( Contact.Name, Contact.PhoneNumber, GSM_CONTACT_NAME_SIZE);
            }
            _CorrectValidity( &Contact);
            Index = ContactListAdd( &ContactList, &Contact);        // append item
            continue;

         case MI_DELETE :
            ContactListLoad( &ContactList, &Contact, Index);
            if( !DMsgYesNo( STR_CONFIRMATION, STR_REALLY_DELETE, Contact.Name)){
               continue;
            }
            ContactListDelete( &ContactList, Index);
            continue;

      }
   }
} // MenuGsmContacts

void MenuGsmContactsSendStatisticsCheckList(void)
// Menu for selection GSM contact from list of all contacts
{
dword Mask;
TContactList  ContactList;
TContactIndex count;
TContactIndex i;
TGsmContact Contact;

   while(!ContactListOpen( &ContactList));
   count = ContactListCount(&ContactList);
   if(count == 0){
      ContactListClose(&ContactList);
      DMsgOk(STR_CONTACTS, STR_NO_ITEMS_FOUND, 0);
      return;
   }
   char NamesList[count+1][GSM_CONTACT_NAME_SIZE + 1];
   TUniStr SelectionList[count+1];
   Mask = 0;
   for(i = 0; i < count; i++){
      ContactListLoad(&ContactList, &Contact, i);
      strcpy(NamesList[i], Contact.Name);
      SelectionList[i] = NamesList[i];
      if(Contact.Statistics == YES){
         Mask |= (1 << i);
      }
   }

   SelectionList[count] = STR_NULL;
   DCheckList( STR_CONTACTS, SelectionList, &Mask);

   for(i = 0; i < count; i++){
      ContactListLoad(&ContactList, &Contact, i);
      if(Mask & (1 << i)){
         Contact.Statistics = YES;
      } else {
         Contact.Statistics = NO;
      }
      ContactListSave(&ContactList, &Contact, i);
   }
   ContactListClose( &ContactList);
}

//******************************************************************************

//------------------------------------------------------------------------------
//   Select
//------------------------------------------------------------------------------

static TContactIndex _ContactSelect( TContactList *ContactList)
// Select item from Contacts list
{
TListCursor Cursor;
int         Count;

   Count = ContactListCount( ContactList);
   DCursorInit( &Cursor, Count, DLIST_ROWS_COUNT_MAX);
   DCursorIndexSet( &Cursor, 0);                  // move cursor at initial position
   DCursorUseGridSet( &Cursor, NO);               // don't display horizontal lines
   forever {
      if( DCursorPageChanged( &Cursor)){
         // redraw page
         GClear();                                // clear display
         DLayoutTitle( STR_CONTACTS);             // display title
         DLayoutStatus( STR_BTN_CANCEL, STR_BTN_SELECT, 0); // display status line
      }
      if( DCursorRowChanged( &Cursor)){
         DisplayPage( &Cursor, ContactList);
         DCursorRowUpdate( &Cursor);
         GFlush();                                // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            DCursorRowUp( &Cursor);
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            DCursorRowDown( &Cursor);
            break;

         case K_RIGHT :
         case K_RIGHT | K_REPEAT :
            DCursorPageDown( &Cursor);
            break;

         case K_LEFT :
         case K_LEFT | K_REPEAT :
            DCursorPageUp( &Cursor);
            break;

         case K_ENTER :
            return( (TContactIndex)DCursorIndex( &Cursor));

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return( ULIST_INDEX_INVALID);
      }
   }
} // _ContactSelect

//------------------------------------------------------------------------------
//   Display page
//------------------------------------------------------------------------------

#define CONTACT_X           3
#define CONTACT_FLAG_X      180
#define CONTACT_FLAG_SPACE (16 + 3)

static void DisplayPage( TListCursor *Cursor, TContactList *ContactList)
// Display contact page
{
int         RowY;
int         x;
int         i;
dword       Index;
TGsmContact Contact;

   DListClear( Cursor);                // clear list area
   // draw points
   for( i = 0; i < DCursorRowCount( Cursor); i++){
      RowY = DListY( i);
      if( !DListCursor( Cursor, i)){
         continue;
      }
      // get item data :
      Index = DCursorPage( Cursor) + i;
      ContactListLoad( ContactList, &Contact, (TContactIndex)Index);
      // item name :
      GTextAt( CONTACT_X, RowY);
      cputs( Contact.Name);
      x  = CONTACT_FLAG_X;
      if( Contact.Statistics){
         GBitmap( x, RowY + 1, &BmpClock);
      }
      x += CONTACT_FLAG_SPACE;
      if( Contact.Commands){
         GBitmap( x, RowY + 1, &BmpPhone);
      }
      x += CONTACT_FLAG_SPACE;
      if( Contact.Events){
         GBitmap( x, RowY + 1, &BmpFlash);
      }
   }
   GFlush();                           // redraw rows
} // DisplayPage

//------------------------------------------------------------------------------
//   Values correctness
//------------------------------------------------------------------------------

static void _CorrectValidity( TGsmContact *Contact)
// check and correct validity of gsm contact fields
{
   if( Contact->SmsFormat == GSM_SMS_FORMAT_PC_ONLY){
      Contact->Commands = Contact->Events = NO;
   }else {
      Contact->SendHistogram = NO;
   }
}
