//******************************************************************************
//
//   MenuGsmInfo.h  GSM info
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuGsmInfo_H__
   #define __MenuGsmInfo_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuGsmInfo( void);
// Menu GSM PIN code enter

#endif
