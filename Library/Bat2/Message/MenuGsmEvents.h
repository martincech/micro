//******************************************************************************
//
//   MenuGsmEvents.h  Gsm events menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuGsmEvents_H__
   #define __MenuGsmEvents_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __MenuGsm_H__
   #include "MenuGsm.h"
#endif


void MenuGsmEvents( void);
// Menu gsm events

#endif
