//******************************************************************************
//
//   MGsm.h       Managed GSM services
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MGsm_H__
   #define __MGsm_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __uClock_H__
   #include "Time/uClock.h"
#endif
#ifndef __PT_H__
   #include "Protothreads/pt.h"            // Protothreads
#endif

//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

typedef enum {
   MGSM_STATUS_OFF,
   MGSM_STATUS_ERROR,
   MGSM_STATUS_ON,
   MGSM_STATUS_NO_PIN,
   MGSM_STATUS_WAIT_PIN,
   MGSM_STATUS_NOT_REGISTERED,
   MGSM_STATUS_CHECK_REGISTERED,
   MGSM_STATUS_READY,
   MGSM_STATUS_SEND,
   MGSM_STATUS_RECEIVE,
   _MGSM_STATUS_LAST
} EMGsmStatus;

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void MGsmInit( void);
// Initialize

PT_THREAD(MGsmExecute( void));
// Execute

void MGsmFree( void);
//------------------------------------------------------------------------------

TYesNo MGsmSend( char *PhoneNumber, char *Message);
// GSM send SMS <Message> to <PhoneNumber>

TYesNo MGsmSendBinary( char *PhoneNumber, char *Message, int32 size);
// GSM send SMS <Message> to <PhoneNumber> as binary sms

TYesNo MGsmReceive( char *PhoneNumber, char *Message, UClockGauge *ReceivedTime);
// GSM receive SMS  <Message> from <PhoneNumber>

TYesNo MGsmRegistered( void);
// Returns YES on modem registered in network

TYesNo MGsmOperator( char *Name);
// Returns operator <Name>

byte MGsmSignalStrength( void);
// Returns relative signal strength

TYesNo MGsmPhoneNumber( char *PhoneNumber);
// Returns own <PhoneNumber>

TYesNo MGsmPinReady( void);
// Returns YES on valid PIN

TYesNo MGsmPinEnter( char *Pin);
// Enters <Pin>

#ifdef _SOCKET_IF_SMS_
TYesNo MGsmSmsSocketInit();
// Sms channel service init
TYesNo MGsmSmsSocketFree();
// Sms channel service free
void MGsmSmsSocketExecute();
// Sms channel service executive
#endif

//------------------------------------------------------------------------------

void MGsmPowerOn( void);
// Instant power on

void MGsmPowerResume( void);
// Resume power mode

TYesNo MGsmIsPowerPeriod( void);
// return true if GSM is switched on by planned activity (not forced by other events)

EMGsmStatus MGsmStatus( void);
// Returns actual GSM status

//------------------------------------------------------------------------------
#ifndef __WIN32__
   // Internet service commands
   #include "fnet.h"
   #include "fnet_ip_prv.h"
   #include "fnet_tcp.h"
   #include "fnet_netif_prv.h"

   // fnet 3G module interface
   typedef struct {
      TYesNo connected;
      unsigned RxCount;
      unsigned TxCount;
   } fnet_3G_if_t;

   extern fnet_netif_t fnet_3G_if;

   TYesNo MGsmSocketOpen(SOCKET sock, const struct sockaddr_in *foreignAddr, byte protocol, fnet_tcp_sockopt_t *tcpOpt);
   // Open client socket to <foreignAddr>
   static inline TYesNo MGsmSocketOpenTcp( SOCKET sock, const struct sockaddr_in *foreign_addr, fnet_tcp_sockopt_t *tcpOpt)
   // just shortcut for TCP socket
   {
      return MGsmSocketOpen( sock, foreign_addr, FNET_IP_PROTOCOL_TCP, tcpOpt);
   }
   static inline TYesNo MGsmSocketOpenUdp( SOCKET sock, const struct sockaddr_in *foreign_addr)
   // just shortcut for UDP socket
   {
      return MGsmSocketOpen( sock, foreign_addr, FNET_IP_PROTOCOL_UDP, NULL);
   }

   TYesNo MGsmSocketClose(SOCKET sock);
   // close socket

   TYesNo MGsmSocketAddress(SOCKET sock, struct sockaddr_in *localAddr);
   // return socket address

   fnet_socket_state_t MGsmSocketState(SOCKET sock);
   // get socket state

   int MGsmSocketSend(SOCKET sock, byte *buf, int len);
   // send data to socket

   int MGsmSocketReceive(SOCKET sock, byte *buf, int maxlen);
   // read data from socket
#endif

#endif
