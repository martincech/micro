//******************************************************************************
//
//   MenuSample.c  Bat2 samples memory viewer
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuSample.h"
#include "Graphic/Graphic.h"              // Graphic display
#include "Console/conio.h"                // Console
#include "Sound/Beep.h"                   // Sounds
#include "Gadget/DCursor.h"               // List Cursor
#include "Gadget/DList.h"                 // List drawings
#include "Gadget/DEvent.h"                // Event manager
#include "Gadget/DLayout.h"               // Display layout
#include "Gadget/DTime.h"                 // Display time
#include "Weight/DWeight.h"               // Display weight
#include "Menu/Screen.h"                  // Wait
#include "Storage/Sample.h"               // Samples
#include "Str.h"                          // project directory strings
#include "Fonts.h"                        // Project fonts
#include "Bitmap.h"                       // Bitmaps

#define LIST_ROWS_COUNT 6
// Page buffer :
static TSample _PageSample[ LIST_ROWS_COUNT];

// Local functions :
static void ReadPage( TListCursor *Cursor);
// Read page from database

static void DisplayPage( TListCursor *Cursor);
// Display database page

//------------------------------------------------------------------------------
//  Display
//------------------------------------------------------------------------------

void MenuSample( void)
// Display samples memory
{
TListCursor Cursor;

   DCursorInit( &Cursor, SampleCount(), DLIST_ROWS_COUNT_MAX);
   forever {
      if( DCursorPageChanged( &Cursor)){
         GClear();                               // clear display
         DLayoutTitle( STR_TODAYS_WEIGHINGS);    // display title
         DLayoutStatus( STR_BTN_CANCEL, 0, 0);   // display status line
         ReadPage( &Cursor);
      }
      if( DCursorRowChanged( &Cursor)){
         DisplayPage( &Cursor);
         DCursorRowUpdate( &Cursor);
         GFlush();                               // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            DCursorCountSet( &Cursor, SampleCount());     // dynamically update items count
            DCursorRowUp( &Cursor);
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            DCursorCountSet( &Cursor, SampleCount());
            DCursorRowDown( &Cursor);
            break;

         case K_LEFT :
         case K_LEFT | K_REPEAT :
            DCursorCountSet( &Cursor, SampleCount());
            DCursorPageUp( &Cursor);
            break;

         case K_RIGHT :
         case K_RIGHT | K_REPEAT :
            DCursorCountSet( &Cursor, SampleCount());
            DCursorPageDown( &Cursor);
            break;

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return;
      }
   }
} // MenuSample


//------------------------------------------------------------------------------
//  Read page
//------------------------------------------------------------------------------

static void ReadPage( TListCursor *Cursor)
// Read page from database
{
byte         i;
TSampleIndex Index;
TSample      Sample;
TSamples Samples;

   if( !SampleOpen( &Samples)) {
      return;
   }
   DCursorRowCountSet( Cursor, DCursorPageRows( Cursor));  // page items count
   Index = DCursorPage( Cursor);                           // first page item index
   for( i = 0; i < DCursorPageRows( Cursor); i++){
      if( !SampleGet( &Samples, Index, &Sample)){
         DCursorRowCountSet( Cursor, i);                   // set page items count
         SampleClose( &Samples);
         return;
      }
      Index++;
      _PageSample[ i] = Sample;
   }
   SampleClose( &Samples);
} // ReadPage

//------------------------------------------------------------------------------
//  Display page
//------------------------------------------------------------------------------

#define ITEM_NUMBER_X        3
#define LINE_SEPARATOR_X     45
#define TIME_X               49
#define WEIGHT_X             150
#define FLAG_SEX_X          (G_WIDTH - 30)
#define FLAG_STATUS_X       (G_WIDTH - 15)

static void DisplayPage( TListCursor *Cursor)
// Display database page
{
int         RowY;
int         i;
TSampleFlag Flag;

   DListClear( Cursor);                // clear list area
   // draw samples
   for( i = 0; i < DCursorRowCount( Cursor); i++){
      RowY = DListY( i);
      if( !DListCursor( Cursor, i)){
         continue;                     // skip row drawing
      }
      // item number :
      GTextAt( ITEM_NUMBER_X, RowY);
      cprintf( "%d", DCursorPage( Cursor) + i + 1);
      // date/time :
      DTime( SampleTimestamp( &_PageSample[ i]), TIME_X, RowY);
      // weight :
      GTextAt( WEIGHT_X, RowY);
      DWeight( SampleWeight( &_PageSample[ i]));
      // sex :
      Flag = SampleFlag( &_PageSample[ i]);
      if( Flag & WEIGHT_FLAG_MALE){
         GBitmap( FLAG_SEX_X, RowY, &BmpIconMale);
      }
      if( Flag & WEIGHT_FLAG_FEMALE){
         GBitmap( FLAG_SEX_X, RowY, &BmpIconFemale);
      }
      // step :
      if( Flag & WEIGHT_FLAG_STEP_DOWN){
         GBitmap( FLAG_STATUS_X, RowY, &BmpIconLight);
      } else {
         GBitmap( FLAG_STATUS_X, RowY, &BmpIconHeavy);
      }
   }
   GFlush();                           // redraw rows
   DListLine( LINE_SEPARATOR_X);       // vertical separator line
} // DisplayPage
