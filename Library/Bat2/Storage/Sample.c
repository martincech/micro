//******************************************************************************
//
//   Sample.c      Bat2 samples memory
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "Sample.h"
#include "Config/Bat2Def.h"
#include "Data/uFifo.h"
#include "Multitasking/Multitasking.h"

// samples FIFO :
uFifoAlloc( SamplesFifoConfiguration, SAMPLES_POINTER_ADDRESS, SAMPLES_CAPACITY, SAMPLE_SIZE, NO, FILE_SAMPLES_LOCAL, FILE_FIFO_LOCAL)
uFifoAlloc( SamplesFifoConfigurationRemote, SAMPLES_POINTER_ADDRESS, SAMPLES_CAPACITY, SAMPLE_SIZE, NO, FILE_SAMPLES_REMOTE, FILE_FIFO_REMOTE)

uFifoAlloc( SamplesBackupFifoConfiguration, SAMPLES_BACKUP_POINTER_ADDRESS, SAMPLES_CAPACITY, SAMPLE_SIZE, NO, FILE_SAMPLES_BACKUP_LOCAL, FILE_FIFO_LOCAL)
uFifoAlloc( SamplesBackupFifoConfigurationRemote, SAMPLES_BACKUP_POINTER_ADDRESS, SAMPLES_CAPACITY, SAMPLE_SIZE, NO, FILE_SAMPLES_BACKUP_REMOTE, FILE_FIFO_REMOTE)

static UFifoDescriptor SamplesFifoDescriptor;
static UFifoDescriptor SamplesBackupFifoDescriptor;

//-----------------------------------------------------------------------------
//  Remote Load
//------------------------------------------------------------------------------

TYesNo SampleRemoteLoad( void)
// Load from remote device
{
   return uFifoCopy( &SamplesFifoConfiguration, &SamplesFifoConfigurationRemote);
} // SampleRemoteLoad

//------------------------------------------------------------------------------
//  Remote Save
//------------------------------------------------------------------------------

TYesNo SampleRemoteSave( void)
// Save to remote device
{
   return uFifoCopy( &SamplesFifoConfigurationRemote, &SamplesFifoConfiguration);
} // SampleRemoteSave

//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

void SampleInit( void)
// Initialize
{
   uFifoInit( &SamplesFifoDescriptor, &SamplesFifoConfiguration);
   uFifoInit( &SamplesBackupFifoDescriptor, &SamplesBackupFifoConfiguration);
} // SampleInit

//------------------------------------------------------------------------------
//  Open
//------------------------------------------------------------------------------

TYesNo SampleOpen( TSamples *Samples)
// Open samples
{
   TYesNo Success;
      Success = uFifoOpen( Samples, &SamplesFifoDescriptor);
      if(!Success) {
         MultitaskingReschedule();
      }
      return Success;
} // SamplesOpen

//------------------------------------------------------------------------------
//  Close
//------------------------------------------------------------------------------

void SampleClose( TSamples *Samples)
// Close samples
{
   uFifoClose( Samples);
} // SamplesClose

//------------------------------------------------------------------------------
//  Clear
//------------------------------------------------------------------------------

void SampleClear( TSamples *Samples)
// Clear samples memory
{
   uFifoClose( Samples);
   uFifoCopy( &SamplesBackupFifoConfiguration, &SamplesFifoConfiguration);
   uFifoOpen( Samples, &SamplesFifoDescriptor);
   uFifoClear( Samples);
} // SampleClear

//------------------------------------------------------------------------------
//  Append
//------------------------------------------------------------------------------

TYesNo SampleAppend( TSamples *Samples, TSample *Sample)
// Append <Sample>
{
   return( uFifoAppend( Samples, Sample));
} // SampleAppend

//------------------------------------------------------------------------------
//  Get
//------------------------------------------------------------------------------

TYesNo SampleGet( TSamples *Samples, TSampleIndex Index, TSample *Sample)
// Get <Sample> at <Index>
{
   return uFifoGet( Samples, Index, Sample);
} // SampleGet

//------------------------------------------------------------------------------
//  Count
//------------------------------------------------------------------------------

TSampleIndex SampleCount( void)
// Returns samples count
{
   return( uFifoCount( &SamplesFifoDescriptor));
} // SampleCount

//------------------------------------------------------------------------------
//  Weight
//------------------------------------------------------------------------------

TWeightGauge SampleWeight( TSample *Sample)
// Returns <Sample> weight
{
TWeightGauge Weight;

   Weight = _SampleWeightGet( Sample->Weight);
   Weight = _SampleWeightSignumExtension( Weight);
   return( Weight);
} // SampleWeight

void SampleWeightSet( TSample *Sample, TWeightGauge Weight, TSampleFlag Flag)
// Set <Sample> <Weight>
{
   Sample->Weight = _SampleWeightSet( Weight, Flag);
} // SampleWeightSet

//------------------------------------------------------------------------------
//  Flag
//------------------------------------------------------------------------------

TSampleFlag SampleFlag( TSample *Sample)
// Returns <Sample> flag
{
   return( (byte)_SampleFlagGet( Sample->Weight));
} // SampleFlag

void SampleFlagSet( TSample *Sample, TSampleFlag Flag)
// Set <Sample> <Flag>
{
   Sample->Weight = _SampleWeightSet( Sample->Weight, Flag);
} // SampleFlagSet

//------------------------------------------------------------------------------
//  Timestamp
//------------------------------------------------------------------------------

UTimeGauge SampleTimestamp( TSample *Sample)
// Returns <Sample> timestamp
{
UTimeGauge Timestamp;

   Timestamp = Sample->Timestamp * 2;     // LSB is [2s]
   return( Timestamp);
} // SampleTimestamp

//------------------------------------------------------------------------------
//  Origin
//------------------------------------------------------------------------------

TSampleOrigin SampleOrigin( TSample *Sample)
// Returns <Sample> origin
{
   return( Sample->Origin);
} // SampleOrigin

void SampleOriginSet( TSample *Sample, TSampleOrigin Origin)
// Set <Sample> <Origin>
{
   Sample->Origin = Origin;
} // SampleOriginSet
