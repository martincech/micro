//******************************************************************************
//
//   MenuEthernetInfo.h  Ethernet Info menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuEthernetInfo_H__
   #define __MenuEthernetInfo_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuEthernetInfo( void);
// Menu ethernet info

#endif
