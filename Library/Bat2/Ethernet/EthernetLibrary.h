//******************************************************************************
//
//   EthernetLibrary.h     Ethernet specific library independent interface
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __EthernetLibrary_H__
#define __EthernetLibrary_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif


//------------------------------------------------------------------------------
//  Executive
//------------------------------------------------------------------------------

void EthernetLibraryExecute();
// main library executive

//------------------------------------------------------------------------------
//  stack routines
//------------------------------------------------------------------------------

TYesNo EthernetLibraryInitNetworkInterface();
// init stack and network interface
void EthernetLibraryFreeNetworkInterface();
// free stack

//------------------------------------------------------------------------------
//  info routines
//------------------------------------------------------------------------------

void EthernetLibraryEthName( char *buffer, int bufferSize);
// return zero terminated interface name string

void EthernetLibraryIPv4( char *buffer, int bufferSize);
// return zero terminated IPv4 address

void EthernetLibraryDnsServer( char *buffer, int bufferSize);
// return zero terminated IPv4 dns server address

void EthernetLibraryLinkStatus( char *buffer, int bufferSize);
// return zero terminated link status string

dword EthernetLibraryUpTime();
// return time in seconds for which is library used

dword EthernetLibraryRxPacketCount();
// return received packet count

dword EthernetLibraryTxPacketCount();
// return transmit packet count

TYesNo EthernetLibraryIpAssigned();
// return whether IP succesfully established, ie. address assigned etc. (DHCP or manualy)

void EthernetSleepEnable( TYesNo Enable);

TYesNo EthernetIsLink( void);


#ifdef __cplusplus
   }
#endif

#endif // __EthernetLibrary_H__


