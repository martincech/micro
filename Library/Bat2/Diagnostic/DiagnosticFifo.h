//******************************************************************************
//
//   DiagnosticFifo.h      Diagnostic FIFO
//   Version 1.0           (c) VEIT Electronics
//
//******************************************************************************

#ifndef __DiagnosticFifo_H__
   #define __DiagnosticFifo_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __Diagnostic_H__
   #include "Diagnostic/Diagnostic.h"
#endif

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void DiagnosticFifoInit( void);
// Initialize

void DiagnosticFifoPut( TDiagnosticSample Weight);
// Save <Weight>

dword DiagnosticFifoCount( void);
// Returns samples count

dword DiagnosticFifoOverrunReset( void);
// Reset and return lost samples

TYesNo DiagnosticFifoGet( TDiagnosticSample *Weight);
// Returns <Weight> at <Index>, removes item from Fifo

//------------------------------------------------------------------------------

#endif
