//******************************************************************************
//
//   ActionDef.h    Bat2 actions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ActionDef_H__
   #ifndef _MANAGED
      #define __ActionDef_H__
   #endif

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __UDateTime_H__
   #include "Time/UDateTime.h"
#endif

#ifdef _MANAGED
namespace Bat2Library{
   public enum class ActionCmdE{
#else
typedef enum {
#endif
   ACTION_WEIGHING_SCHEDULER_START,
   ACTION_WEIGHING_SCHEDULER_START_AT,
   ACTION_WEIGHING_SCHEDULER_STOP,
   ACTION_WEIGHING_SCHEDULER_SUSPEND,
   ACTION_WEIGHING_SCHEDULER_RELEASE,
   ACTION_TIME_SET,
   ACTION_TIME_GET,
   ACTION_REBOOT
#ifndef _MANAGED
} EActionCmd;
#else
   };
}
#endif


#ifndef _MANAGED
#define ActionCmdSimpleSize()      (1)
#define ActionCmdSize( Item)  (ActionCmdSimpleSize() + sizeof( TActionCmd##Item))

typedef struct {
   UDateTimeGauge DateTime;
} TActionCmdWeighingSchedulerStartAt;

typedef struct {
   UDateTimeGauge DateTime;
} TActionCmdTimeSet;

typedef union {
   TActionCmdWeighingSchedulerStartAt WeighingSchedulerStartAt;
   TActionCmdTimeSet TimeSet;
} TActionCmdUnion;

#ifdef __WIN32__
   #pragma pack( push, 1)              // byte alignment
#endif

typedef struct {
   byte            Cmd;
   TActionCmdUnion Data;
} __packed TActionCmd;

#ifdef __WIN32__
   #pragma pack( pop)                  // original alignment
#endif



#define ActionReplySimpleSize()      (1)
#define ActionReplySize( Item)  (ActionReplySimpleSize() + sizeof( TActionReply##Item))

typedef struct {
   UDateTimeGauge DateTime;
} TActionReplyTimeGet;

typedef union {
   TActionReplyTimeGet TimeGet;
} TActionReplyUnion;

#ifdef __WIN32__
   #pragma pack( push, 1)              // byte alignment
#endif

typedef struct {
   byte            Reply;
   TActionReplyUnion Data;
} __packed TActionReply;

#ifdef __WIN32__
   #pragma pack( pop)                  // original alignment
#endif


#define ActionReply(Cmd, Ok)   (Cmd | (Ok << 7))

#endif

#endif