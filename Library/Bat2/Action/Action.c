
#include "Action.h"
#include "Scheduler/WeighingScheduler.h"
#include "Remote/SocketDef.h"
#include "Config/Config.h"
#include "System/System.h"

TYesNo ActionSimpleExecute( byte ActionCode, TActionReply *Reply, int *ReplySize)
// Simple action
{
   TActionCmd Action;
   Action.Cmd = ActionCode;

   return ActionExecute( &Action, ActionCmdSimpleSize(), Reply, ReplySize);
} // ActionSimple




TYesNo ActionExecute( TActionCmd *Action, int ActionSize, TActionReply *Reply, int *ReplySize) {
TYesNo Success = YES;

   *ReplySize = 0;
   *ReplySize = ActionReplySimpleSize();
   
   switch(Action->Cmd) {
      case ACTION_WEIGHING_SCHEDULER_START:
         if(ActionSize != ActionCmdSimpleSize()) {
            return NO;
         }
         ConfigLoad();
         WeighingSchedulerStart();
         break;

      case ACTION_WEIGHING_SCHEDULER_START_AT:
         if(ActionSize != ActionCmdSize( WeighingSchedulerStartAt)) {
            return NO;
         }
         ConfigLoad();
         WeighingSchedulerStartAt( uDateTimeClock( Action->Data.WeighingSchedulerStartAt.DateTime));
         break;

      case ACTION_WEIGHING_SCHEDULER_STOP:
         if(ActionSize != ActionCmdSimpleSize()) {
            return NO;
         }
         WeighingSchedulerStop();
         break;

      case ACTION_WEIGHING_SCHEDULER_SUSPEND:
         if(ActionSize != ActionCmdSimpleSize()) {
            return NO;
         }
         WeighingSchedulerSuspend();
         break;
      case ACTION_WEIGHING_SCHEDULER_RELEASE:
         if(ActionSize != ActionCmdSimpleSize()) {
            return NO;
         }
         WeighingSchedulerRelease();
         break;

      case ACTION_TIME_SET:
         if(ActionSize != ActionCmdSize( TimeSet)) {
            return NO;
         }
         SysDateTimeSet( Action->Data.TimeSet.DateTime);
         break;

      case ACTION_TIME_GET:
         if(ActionSize != ActionCmdSimpleSize()) {
            return NO;
         }
         Reply->Data.TimeGet.DateTime = SysDateTime();
         *ReplySize = ActionReplySize( TimeGet);
         break;

      default:
         return NO;
   }

   Reply->Reply = ActionReply( Action->Cmd, Success);
   return YES;
}