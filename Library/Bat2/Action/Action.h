//******************************************************************************
//
//   Action.h      Bat2 actions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Action_H__
   #define __Action_H__

#ifndef __ActionDef_H__
   #include "ActionDef.h"
#endif

TYesNo ActionExecute( TActionCmd *Action, int ActionSize, TActionReply *Reply, int *ReplySize);

#endif
