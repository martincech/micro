//******************************************************************************
//
//   ActionRemote.h     Bat2 actions
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ActionRemote_H__
   #define __ActionRemote_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __UDateTime_H__
   #include "Time/UDateTime.h"
#endif

#ifndef __UClock_H__
   #include "Time/UClock.h"
#endif

#include "Remote/Socket.h"

#ifdef __cplusplus
   extern "C" {
#endif

TYesNo ActionRemoteSetup(TSocket *Socket);
// Setup action

TYesNo ActionRemoteWeighingSchedulerStart( void);
// Weighing scheduler start

TYesNo ActionRemoteWeighingSchedulerStartAt( UClockGauge DateTime);
// Weighing scheduler start at

TYesNo ActionRemoteWeighingSchedulerStop( void);
// Weighing scheduler stop

TYesNo ActionRemoteWeighingSchedulerSuspend( void);
// Weighing scheduler suspend

TYesNo ActionRemoteWeighingSchedulerRelease( void);
// Weighing scheduler release

TYesNo ActionRemoteTimeSet( UDateTimeGauge DateTime);
// Time set

TYesNo ActionRemoteTimeGet( UDateTimeGauge *DateTime);
// Time get

TYesNo ActionRemoteReboot();
// Reboot

#ifdef __cplusplus
   }
#endif

#endif
