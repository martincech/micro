//******************************************************************************
//
//   ActionRemote.cpp    Bat2 remote actions
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "ActionRemote.h"
#include "ActionDef.h"
#include "ActionDef.h"
#include "Remote/Cmd.h"

static TSocket *Socket;

TYesNo ActionExecute( TActionCmd *Action, int ActionSize, TActionReply *Reply, int *ReplySize, int DesiredReplySize);
// Execute action

//------------------------------------------------------------------------------
//  Setup
//------------------------------------------------------------------------------

TYesNo ActionRemoteSetup(TSocket *_Socket)
// Setup action
{
   Socket = _Socket;
   return YES;
} // ActionSetup

//------------------------------------------------------------------------------
//  Weighing scheduler start
//------------------------------------------------------------------------------

TYesNo ActionRemoteWeighingSchedulerStart( void)
// Weighing scheduler start
{
   TActionCmd Cmd;
   TActionReply Reply;
   int ReplySize;

   Cmd.Cmd = ACTION_WEIGHING_SCHEDULER_START;
   return ActionExecute(&Cmd, ActionCmdSimpleSize(), &Reply, &ReplySize, ActionReplySimpleSize());
} // ActionRemoteWeighingSchedulerStart

//------------------------------------------------------------------------------
//  Weighing scheduler start at
//------------------------------------------------------------------------------

TYesNo ActionRemoteWeighingSchedulerStartAt( UClockGauge DateTime)
// Weighing scheduler start at
{
   TActionCmd Cmd;
   TActionReply Reply;
   int ReplySize;

   Cmd.Cmd = ACTION_WEIGHING_SCHEDULER_START_AT;
   Cmd.Data.WeighingSchedulerStartAt.DateTime = DateTime;
   return ActionExecute(&Cmd, ActionCmdSize( WeighingSchedulerStartAt), &Reply, &ReplySize, ActionReplySimpleSize());
} // ActionRemoteWeighingSchedulerStartAt

//------------------------------------------------------------------------------
//  Weighing scheduler stop
//------------------------------------------------------------------------------

TYesNo ActionRemoteWeighingSchedulerStop( void)
// Weighing scheduler stop
{
   TActionCmd Cmd;
   TActionReply Reply;
   int ReplySize;

   Cmd.Cmd = ACTION_WEIGHING_SCHEDULER_STOP;
   return ActionExecute(&Cmd, ActionCmdSimpleSize(), &Reply, &ReplySize, ActionReplySimpleSize());
} // ActionRemoteWeighingSchedulerStop

//------------------------------------------------------------------------------
//  Weighing scheduler suspend
//------------------------------------------------------------------------------

TYesNo ActionRemoteWeighingSchedulerSuspend( void)
// Weighing scheduler suspend
{
   TActionCmd Cmd;
   TActionReply Reply;
   int ReplySize;

   Cmd.Cmd = ACTION_WEIGHING_SCHEDULER_SUSPEND;
   return ActionExecute(&Cmd, ActionCmdSimpleSize(), &Reply, &ReplySize, ActionReplySimpleSize());
} // ActionRemoteWeighingSchedulerSuspend

//------------------------------------------------------------------------------
//  Weighing scheduler release
//------------------------------------------------------------------------------

TYesNo ActionRemoteWeighingSchedulerRelease( void)
// Weighing scheduler release
{
   TActionCmd Cmd;
   TActionReply Reply;
   int ReplySize;

   Cmd.Cmd = ACTION_WEIGHING_SCHEDULER_RELEASE;
   return ActionExecute(&Cmd, ActionCmdSimpleSize(), &Reply, &ReplySize, ActionReplySimpleSize());
} // ActionRemoteWeighingSchedulerRelease

//------------------------------------------------------------------------------
//  Time set
//------------------------------------------------------------------------------

TYesNo ActionRemoteTimeSet( UDateTimeGauge DateTime)
// Time set
{
   TActionCmd Cmd;
   TActionReply Reply;
   int ReplySize;

   Cmd.Cmd = ACTION_TIME_SET;
   Cmd.Data.TimeSet.DateTime = DateTime;
   return ActionExecute(&Cmd, ActionCmdSize( TimeSet), &Reply, &ReplySize, ActionReplySimpleSize());
} // ActionRemoteTimeSet

//------------------------------------------------------------------------------
//  Time get
//------------------------------------------------------------------------------

TYesNo ActionRemoteTimeGet( UDateTimeGauge *DateTime)
// Time get
{
   TActionCmd Cmd;
   TActionReply Reply;
   int ReplySize;

   Cmd.Cmd = ACTION_TIME_GET;
   if(!ActionExecute(&Cmd, ActionCmdSimpleSize(), &Reply, &ReplySize, ActionReplySize( TimeGet))) {
      return NO;
   }
   *DateTime = Reply.Data.TimeGet.DateTime;
   return YES;
} // ActionRemoteTimeGet

//------------------------------------------------------------------------------
//  Reboot
//------------------------------------------------------------------------------

TYesNo ActionRemoteReboot()
// Reboot
{
   TActionCmd Cmd;
   TActionReply Reply;
   int ReplySize;

   Cmd.Cmd = ACTION_REBOOT;
   if(!ActionExecute(&Cmd, ActionCmdSimpleSize(), &Reply, &ReplySize, ActionReplySimpleSize())) {
      return NO;
   }
   return YES;
}

//******************************************************************************

//------------------------------------------------------------------------------
//  Execute
//------------------------------------------------------------------------------

TYesNo ActionExecute( TActionCmd *Action, int ActionSize, TActionReply *Reply, int *ReplySize, int DesiredReplySize)
// Execute action
{
   *ReplySize = DesiredReplySize;
   if(!CmdAndReplyAction(Socket, Action, ActionSize, Reply, ReplySize)) {
      return NO;
   }

   if(Reply->Reply != ActionReply(Action->Cmd, YES)) {
      return NO;
   }

   return YES;
}
