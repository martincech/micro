//*****************************************************************************
//
//    Main.c       Bat2 test main module
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Hardware.h"
#include "System/System.h"
#include "System/SysTimer.h"
#include "Cpu/Cpu.h"
#include "Uart/Uart.h"            // UART services
#include "Kbd/Kbd.h"              // Keyboard services
#include "Graphic/Graphic.h"      // Graphic display
#include "Console/conio.h"        // Console output
#include "Console/uconio.h"       // UART console output
#include "Display/BacklightPwm.h" // Display backlight
#include "Display/DisplayConfigurationDef.h" // BACKLIGHT constants only
#include "Test/TestDef.h"         // Test commands
#include "Rtc/Rx8025.h"           // RTC controller
#include "Rtc/RtcTest.h"          // RTC test
#include "Fram/Fm25h20.h"         // FRAM memory
#include "Fram/FramTest.h"        // FRAM memory test
#include "Flash/At25dfxx.h"       // FLASH memory
#include "Flash/NorFlashTest.h"   // FLASH memory test
#include "Iadc/Iadc.h"            // Internal ADC
#include "Fonts.h"                // Installed fonts

#define BUFFER_SIZE 256

static byte _buffer[ BUFFER_SIZE];

static word _Tick;
static word _FastTick;

#define TickSet()              _Tick = TIMER_SLOW_PERIOD
#define TickActive()           (--_Tick == 0)

#define FastTickSet()          _FastTick = TIMER_FAST_PERIOD
#define FastTickActive()       (--_FastTick == 0)

// Local functions :
static TYesNo _FramTest( void);
// Run FRAM memory test

static TYesNo _FlashTest( void);
// Run FLASH memory test

static void _UartInit( TUartAddress Uart);
// Initialize RS485

static void _UartTest( TUartAddress Uart);
// Run RS485 test

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
byte ch;
byte r;
word Value;

   CpuInit();
   StatusLedInit();
   StatusLed0Off();
   StatusLed1Off();
   
   KbdInit();   
   BacklightPwmInit();
   GInit();
   
   BacklightPwmOn( BACKLIGHT_INTENSITY_MAX);
   GSetFont( FONT_8x8);
   cputs( "Start...\n");
   cputs( "Display : ON\n");

   // flash at start :
   StatusLed0On();
   SysDelay( 500);
   StatusLed0Off();
   
   // initialize devices :
   _UartInit( UART_UART0);
   cputs( "UART0 : ON\n");
   
   UartInit( TEST_MAIN_UART);
   UartSetup( TEST_MAIN_UART, UART1_BAUD, UART1_FORMAT);
   UartTimeoutSet( TEST_MAIN_UART, UART_TIMEOUT_OFF, UART1_TIMEOUT);
   cputs( "UART1 : ON\n");

   _UartInit( UART_UART2);
   cputs( "UART2 : ON\n");
   
   _UartInit( UART_UART3);
   cputs( "UART3 : ON\n");

   // FRAM :
   FramInit();
   cputs( "FRAM : ON\n");

   // FLASH :
   FlashInit();
   cputs( "FLASH : ON\n");
   
   // RTC :
   RtcInit();
   cputs( "RTC : ON\n");
   
   // IADC :
   IadcInit();
   cputs( "IADC : ON\n");
   
   // system run :
   TickSet();
   FastTickSet();
   SysTimerStart();
   InterruptEnable();
   WatchDogInit();                     // start watchdog
   cputs( "Wait for commands...\n");
   uputs( "ST\n");                     // start indication

   // main loop :
   forever {
      if( kbhit()){
         switch( getch()){
            case K_ESC :
               StatusLed0Toggle();
               break;
            case K_ENTER :
               StatusLed0Toggle();
               break;
            case K_UP :
               StatusLed0Toggle();
               break;
            case K_DOWN :
               break;
            case K_RIGHT :
               StatusLed0Toggle();
               break;
            case K_LEFT :
               StatusLed0Toggle();
               break;  
            default :
               break;
         }
      }
      // check for command :
      if( !UartRxWait( TEST_MAIN_UART, 0)){
         continue;
      }	   
      if( !UartRxChar( TEST_MAIN_UART, &ch)){
         continue;
      }
      r = TEST_OK;
      switch( ch){
         case TEST_DISPLAY :
            GSetColor( COLOR_BLACK);
            GBox( 0, 0, G_WIDTH, G_HEIGHT);
            GFlush();
            SysDelay( 500);
            cclear();
            cputs( "Display : OK\n");
            break;
         
         case TEST_BACKLIGHT_ON :
            cputs( "Backlight : ON\n");
            BacklightPwmOn( BACKLIGHT_INTENSITY_MAX);
            break;
         
         case TEST_BACKLIGHT_OFF :
            cputs( "Backlight : OFF\n");
            BacklightPwmOff();
            break;
         
         case TEST_RTC :
            cputs( "RTC : ");
            if( !RtcTest()){
               cputs( "ERR\n");
               r = TEST_ERR;
               break;
            }
            cputs( "OK\n");
            break;
         
         case TEST_FRAM :
            cputs( "FRAM : ");
            if( !_FramTest()){
               cputs( "\nERR\n");
               r = TEST_ERR;
               break;
            }
            cputs( "OK\n");
            break;
            
         case TEST_FLASH :
            cputs( "FLASH : ");
            if( !_FlashTest()){
               cputs( "\nERR\n");
               r = TEST_ERR;
               break;
            }
            cputs( "OK\n");
            break;
         
         case TEST_UART0 :
            cputs( "UART0 : ");
            _UartTest( UART_UART0);
            cputs( "OK\n");
            break;
           
         case TEST_UART2 :
            cputs( "UART2 : ");
            _UartTest( UART_UART2);
            cputs( "OK\n");
            break;
         
         case TEST_UART3 :
            cputs( "UART3 : ");
            _UartTest( UART_UART3);
            cputs( "OK\n");
            break;

         case TEST_IADC :
            cputs( "IADC : ");
            Value = IadcRead( IADC_POWER_VOLTAGE);
            cputs( "OK\n");
            uprintf( "%08x", Value);
            break;
       
         case TEST_KEYBOARD :
         case TEST_USB_DEVICE :
         case TEST_USB_HOST :
         default :
            r = ch;                        // echo only
            break;
      }
      UartTxChar( TEST_MAIN_UART, r);
   }
} // main

//------------------------------------------------------------------------------
//   Timer executive
//------------------------------------------------------------------------------

void SysTimerExecute( void)
// Timer user executive
{
   UartTimer();
   if( FastTickActive()){
      FastTickSet();
   }   
   if( TickActive()){
      TickSet();
      StatusLed1Toggle();
   }   
} // SysTimerExecute

//------------------------------------------------------------------------------
//   FRAM Memory test
//------------------------------------------------------------------------------

static TYesNo _FramTest( void)
// Run FRAM memory test
{
   cputs( "00,");
   if( !FramPatternTest( 0x00)){
      return( NO);
   }
   cputs( "55,");
   if( !FramPatternTest( 0x55)){
      return( NO);
   }
   cputs( "AA,");
   if( !FramPatternTest( 0xAA)){
      return( NO);
   }
   cputs( "pp,");
   if( !FramPageTest()){
      return( NO);
   }
   cputs( "FF\n");
   if( !FramPatternTest( 0xFF)){
      return( NO);
   }
   return( YES);
} // _FramTest

//------------------------------------------------------------------------------
//   FLASH Memory test
//------------------------------------------------------------------------------

static TYesNo _FlashTest( void)
// Run FLASH memory test
{
   cputs( "SG,");
   if( !NorFlashSignature()){
      return( NO);
   }
   cputs( "00,");
   if( !NorFlashPatternTest( 0x00)){
      return( NO);
   }
   /*
   cputs( "55,");
   if( !NorFlashPatternTest( 0x55)){
      return( NO);
   }
   cputs( "AA,");
   if( !NorFlashPatternTest( 0xAA)){
      return( NO);
   }
*/   
   cputs( "pp,");
   if( !NorFlashPageTest()){
      return( NO);
   }
   cputs( "FF\n");
   if( !NorFlashBlankCheck()){
      return( NO);
   }
   return( YES);
} // _FlashTest

//------------------------------------------------------------------------------
//   UART initialize
//------------------------------------------------------------------------------

static void _UartInit( TUartAddress Uart)
// Initialize RS485
{
   
   UartInit( Uart);
   UartSetup( Uart, UART1_BAUD, UART1_FORMAT);
   UartTimeoutSet( Uart, UART_TIMEOUT_OFF, UART1_TIMEOUT);
   UartSendDelaySet( Uart, 100);
   UartBufferSet( Uart, _buffer, BUFFER_SIZE);
   UartModeSet( Uart, UART_MODE_BINARY_SLAVE);
} // _UartInit	


//------------------------------------------------------------------------------
//   UART test
//------------------------------------------------------------------------------

static void _UartTest( TUartAddress Uart)
// Run RS485 test
{
byte r;
int Size;
   
   UartReceive( Uart);
   forever {
      // terminate with a key :
      if( kbhit()){
         r = getch();
         return;
      }
      // check for command :
      switch( UartReceiveStatus( Uart)){
         case UART_RECEIVE_ACTIVE :
            // receiving active
            break;          
         
         case UART_RECEIVE_FRAMING_ERROR :
            // receiving terminated with framing/parity error
            cputs( "Framing error\n");
            UartReceive( Uart);
            break;
         case UART_RECEIVE_OVERRUN_ERROR :
            // receive interrupt overrun
            cputs( "Overrun error\n");
            UartReceive( Uart);
            break;
         case UART_RECEIVE_SIZE_ERROR :
            // received characters count greater than buffer size
            cputs( "Size error\n");
            UartReceive( Uart);
            break;
         case UART_RECEIVE_REPLY_TIMEOUT :
            // no characters received up to reply timeout
            cputs( "Reply T.O.\n");
            UartTimeoutSet( Uart, UART_TIMEOUT_OFF, UART1_TIMEOUT);
            UartReceive( Uart);
            break;
         
         case UART_RECEIVE_TIMEOUT :
           // intercharacter timeout   
           Size = UartReceiveSize( Uart);
           if( Size == 3 && _buffer[ 0] == 0xFF && _buffer[ 1] == 0xFF && _buffer[ 2] == 0xFF){
              return;				  
           }			  	  
           UartTimeoutSet( Uart, UART1_REPLY_TIMEOUT, UART1_TIMEOUT);
           UartSend( Uart, _buffer, Size);
           do {
              r = UartSendStatus( Uart);	   
           } while( r != UART_SEND_DONE);
           // Uart receive started automatically
           break;
          
         case UART_RECEIVE_HEADER_CRC_ERROR : 
            // wrong received header CRC
         case UART_RECEIVE_CRC_ERROR :
            // wrong received frame CRC
         case UART_RECEIVE_FRAME :
            // complete frame received
         default :
            cputs( "Unknown error\n");
            UartTimeoutSet( Uart, UART_TIMEOUT_OFF, UART1_TIMEOUT);
            UartReceive( Uart);
            break;
      }      
   }
} // UartTest
