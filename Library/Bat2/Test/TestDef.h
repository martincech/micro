//******************************************************************************
//
//   TestDef.h    Bat2 test definitions
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __TestDef_H__
#define __TestDef_H__

// Test commands :
typedef enum {
   TEST_DISPLAY        = 'D',                 // run display test
   TEST_BACKLIGHT_ON   = 'B',                 // switch backlight off
   TEST_BACKLIGHT_OFF  = 'G',                 // switch backlight on
   TEST_FLASH          = 'M',                 // run flash memory test
   TEST_FRAM           = 'A',                 // run FRAM memory test
   TEST_RTC            = 'R',                 // run RTC test
   TEST_KEYBOARD       = 'K',                 // run keyboard test
   TEST_USB_DEVICE     = 'H',                 // USB device test
   TEST_USB_HOST       = 'F',                 // USB host test
   TEST_UART0          = '0',                 // test UART0
   TEST_UART2          = '2',                 // test UART2
   TEST_UART3          = '3',                 // test UART3
   TEST_IADC           = 'U',                 // test internal ADC
   _TEST_LAST          = 0xFF
} TTestCommand;

// Test reply :
#define TEST_OK  'Y'
#define TEST_ERR 'N'

#endif
