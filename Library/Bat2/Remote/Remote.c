//*****************************************************************************
//
//    Remote.c     Remote
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "Remote.h"
#include "Remote/Rc.h"
#include "System/System.h"

#include "Console/Conio.h"          // print
#include "Graphic/Graphic.h"        // graphics
#include "Gadget/DMsg.h"            // Message box
#include "Sound/Beep.h"
#include "Menu/Screen.h"            // main screen 
#include "Menu/Menu.h"              // main menu
#include "MenuRemote.h"             // remote menu
#include "Gui.h"
#include "Str.h"                    // strings
#include "Fonts.h"                // Font list

#include "Multitasking/Multitasking.h" // Rescheduling
#include "Remote/SocketIfUsb.h"
#include "Remote/Socket.h"

static TYesNo Remote = NO;
static TYesNo Error;
#define BUFFER_SIZE (sizeof(TRcReply) > sizeof(TRcCmd)? sizeof(TRcReply) : sizeof(TRcCmd))

typedef struct {
   TSocket Socket;
   byte Buffer[ BUFFER_SIZE];
   byte SessionId;
} TSession;

#include "Remote/Socket.h"

static TSession Sessions[SOCKET_COUNT];

static void TerminateSessionsAndCloseSockets( void);
// Terminates all sessions and closes all sockets

//------------------------------------------------------------------------------
//  Menu remote
//------------------------------------------------------------------------------

void MenuRemote( void)
// Remote menu
{
   Remote = YES;
   Error = NO;


   forever {
      if(!Remote && !Error) {
         return;
      }
      switch( GuiEventWait()){
         case K_FLASH1 :
            GClear();
            SetFont( ARIAL_BOLD21);
            DLabelCenter( STR_REMOTE_CLIENT_CONNECTED, 0, 0, G_WIDTH, G_HEIGHT);
            if(Error) {
               GTextAt(0, 20);
               cprintf( StrGet(STR_ERROR));
            }
            break;

         case K_SHUTDOWN :
            Remote = NO;
            return;
         case K_UP :
         case K_DOWN :
         case K_LEFT :
         case K_RIGHT :
         case K_ESC :
         case K_ENTER :
            BeepKey();
            SetFont( TAHOMA16);
            if( DMsgYesNo( STR_REMOTE_CLIENT_CONNECTED, STR_REMOTE_TERMINATE_SESSION, 0)){
               DMsgWait();
               RcSessionTerminateForce();
               TerminateSessionsAndCloseSockets();
               Remote = NO;
               Error = NO;
            }
            break;
      }
      GFlush();
   }
} // MenuRemote

//------------------------------------------------------------------------------
//  Initialisation
//------------------------------------------------------------------------------

void RemoteInit( void)
// Init
{
int i;
   RcInit();

   for(i = 0 ; i < SOCKET_COUNT ; i++) {
      Sessions[i].SessionId = RC_SESSION_ID_INVALID;
   }
} // RcInit

//------------------------------------------------------------------------------
//  Execute
//------------------------------------------------------------------------------

void RemoteExecute( void)
// Execute remote tasks - must be in separate task
{
int i;
TYesNo Processed;
TRcCmd *Command;
word ReplyLength;
byte State;
TYesNo ActiveSocket = NO;
TRcId RcId;
TYesNo SocketActive;
TSocket Socket;
TYesNo Found;

   RcExecute();

   SocketActive = 0;

   // Accept new connections
   if(SocketListen( &Socket)) {
      Found = NO;
      for(i = 0 ; i < SOCKET_COUNT ; i++) {
         if(Sessions[i].SessionId == RC_SESSION_ID_INVALID) {
            Sessions[i].Socket = Socket;
            Found = YES;
            break;
         }
      }
      if(Found) {
         if(!Remote) {
            MenuExitSet(MENU_EXIT_REMOTE);
            while( !Remote){
               MultitaskingReschedule();
            }
         }

         Sessions[i].SessionId = RcSessionStart();
         if(Sessions[i].SessionId == RC_SESSION_ID_INVALID) {
            SocketClose( &Sessions[i].Socket);
         }
      }
   }
         
   // Execute current connections
   for(i = 0 ; i < SOCKET_COUNT ; i++) {
      if(Sessions[i].SessionId == RC_SESSION_ID_INVALID) {
         continue;
      }
      
      if( !Remote || !RcSessionIsActive( Sessions[i].SessionId)){
         Sessions[i].SessionId = RC_SESSION_ID_INVALID;
         SocketClose(&Sessions[i].Socket);
         continue;
      }

      SocketActive = YES;
      State = SocketState(&Sessions[i].Socket);

      switch(State) {
         case SOCKET_STATE_DISCONNECTED:
            if(Sessions[i].SessionId == RC_SESSION_ID_INVALID) {
               break;
            }
            RcSessionTerminate( Sessions[i].SessionId);
            Sessions[i].SessionId = RC_SESSION_ID_INVALID;
            break;

         case SOCKET_STATE_RECEIVE_ACTIVE:
            break;

         case SOCKET_STATE_SEND_ACTIVE:
            break;

         case SOCKET_STATE_RECEIVE_DONE:
            Command = (TRcCmd *)Sessions[i].Buffer;
            if( Command->Cmd < _RC_CMD_NO_SESSION){
               RcId.SessionId = Sessions[i].SessionId;
               RcId.InterfaceId = SocketInterfaceId( &Sessions[i].Socket);   
               Processed = RcCommand( &RcId, Command, SocketReceiveSize(&Sessions[i].Socket),(TRcReply *) Sessions[i].Buffer, &ReplyLength);
            } else {
               Processed = RcNoSessionCommand( Command, SocketReceiveSize(&Sessions[i].Socket),(TRcReply *) Sessions[i].Buffer, &ReplyLength);
            }

            if( !Processed){
               // command processing error, continue listening
               SocketReceive(&Sessions[i].Socket, Sessions[i].Buffer, BUFFER_SIZE);
               break;
            }
            SocketSend( &Sessions[i].Socket, Sessions[i].Buffer, ReplyLength);
            break;

         case SOCKET_STATE_SEND_DONE:
         case SOCKET_STATE_CONNECTED:
            SocketReceive(&Sessions[i].Socket, Sessions[i].Buffer, BUFFER_SIZE);
            break;

         default:
            Error = YES;
            SocketReceive(&Sessions[i].Socket, Sessions[i].Buffer, BUFFER_SIZE);
            break;
      }
   }

   if(!SocketActive) {
      if(Remote) {
         MenuExitSet( MENU_EXIT_REMOTE_EXIT);
         while( Remote){
            MultitaskingReschedule();
         }
      }
   }
} // RemoteExecute

//******************************************************************************

//------------------------------------------------------------------------------
//  Terminate
//------------------------------------------------------------------------------

static void TerminateSessionsAndCloseSockets( void)
// Terminates all sessions and closes all sockets
{
int i;
   for(i = 0 ; i < SOCKET_COUNT ; i++) {
      if(Sessions[i].SessionId == RC_SESSION_ID_INVALID) {
         continue;
      }
      
      RcSessionTerminate( Sessions[i].SessionId);
      SocketClose(&Sessions[i].Socket);
   }
} // TerminateSessionsAndCloseSockets