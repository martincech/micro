//******************************************************************************
//
//   MenuRemote.h     Main screen for remote connection
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuRemote_H__
#define __MenuRemote_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif


void MenuRemote( void);
// Show menu remote

#endif // __MenuRemote_H__


