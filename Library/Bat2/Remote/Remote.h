//*****************************************************************************
//
//    Remote.h     Remote
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Remote_H__
   #define __Remote_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

void RemoteInit( void);
// Init

void RemoteExecute( void);
// Execute remote tasks - must be in separate task

#endif