//******************************************************************************
//
//   RcDef.h        Remote command definitions
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __RcDef_H__
   #define __RcDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __uDateTime_H__
   #include "Time/uDateTime.h"
#endif

#ifndef __NvmDef_H__
   #include "Memory/NvmDef.h"
#endif

#ifndef __File_H__
   #include "Memory/File.h"
#endif

#ifndef __VersionDef_H__
   #include "Device/VersionDef.h"
#endif

#ifndef __ActionDef_H__
   #include "Action/ActionDef.h"
#endif

#define RC_FILE_DATA_MAX  244// (256 - 6 - 6) // to maximise HID report utilization: 6 B HidNative frame, 6 B Remote procedure call

typedef enum {
   RC_CMD_FILE_OPEN,
   RC_CMD_FILE_CLOSE,
   RC_CMD_FILE_LOAD,
   RC_CMD_FILE_SAVE,
   RC_CMD_FILE_FILL,
   RC_CMD_FILE_MATCH,
   RC_CMD_FILE_SUM,
   RC_CMD_FILE_MOVE,
   RC_CMD_FILE_COMMIT,
   RC_CMD_ACTION,
   // non session commands
   _RC_CMD_NO_SESSION = 0x30,
   RC_CMD_GSM = _RC_CMD_NO_SESSION,
   RC_CMD_GSM_SEND = RC_CMD_GSM,
   RC_CMD_GSM_SEND_TO,
   RC_CMD_GSM_STATUS,
   _RC_CMD_COUNT
} ERcCmd;

#ifdef __WIN32__
   #pragma pack( push, 1)              // byte alignment
#endif

#define RcCmdSimpleSize()      (1)
#define RcCmdSize( Item)  (RcCmdSimpleSize() + sizeof( TRcCmd##Item))

typedef struct {
   TFileName FileName;
   byte Mode;
} __packed TRcCmdFileOpen;

typedef struct {
   TFileName FileName;
} __packed TRcCmdFileClose;

typedef struct {
   TFileName FileName;
   TFileAddress Address;
   word Size;
} __packed TRcCmdFileLoad;

typedef struct {
   TFileName FileName;
   TFileAddress Address;
   word Size;
   byte Data[RC_FILE_DATA_MAX];
} __packed TRcCmdFileSave;

#define RcCmdFileSaveSize( Count)         (RcCmdSimpleSize() + sizeof(TRcCmdFileSave) - RC_FILE_DATA_MAX + Count)

typedef struct {
   TFileName FileName;
   TFileAddress Address;
   word Size;
   byte Data[RC_FILE_DATA_MAX];
} __packed TRcCmdFileMatch;

#define RcCmdFileMatchSize( Count)         (RcCmdSimpleSize() + sizeof(TRcCmdFileMatch) - RC_FILE_DATA_MAX + Count)

typedef struct {
   TFileName FileName;
   TFileAddress Address;
   word Size;
   byte Pattern;
} __packed TRcCmdFileFill;

typedef struct {
   TFileName FileName;
   TFileAddress Address;
   word Size;
} __packed TRcCmdFileSum;

typedef struct {
   TFileName FileName;
   TFileAddress ToAddress;
   TFileAddress FromAddress;
   word Size;
} __packed TRcCmdFileMove;

typedef struct {
   TFileName FileName;
} __packed TRcCmdFileCommit;

typedef struct {
   TActionCmd Action;
} __packed TRcCmdAction;

#define RcRcToActionSize( RcSize)         (RcSize - RcCmdSimpleSize())

#include "SmsGate/SmsGateDef.h"
#include "Gsm/GsmDef.h"

typedef struct {
   char SmsText[ GSM_SMS_SIZE];
} __packed TRcCmdGsmSend;

typedef struct {
   char PhoneNumber[ GSM_PHONE_NUMBER_SIZE];
   char SmsText[ GSM_SMS_SIZE];
} __packed TRcCmdGsmSendTo;

typedef struct {
   TSmsGateSlotNumber SlotNumber;
} __packed TRcCmdGsmStatus;

typedef union {
   TFileName          FileName;
   TRcCmdFileOpen     FileOpen;
   TRcCmdFileClose    FileClose;
   TRcCmdFileLoad     FileLoad;
   TRcCmdFileSave     FileSave;
   TRcCmdFileMatch    FileMatch;
   TRcCmdFileFill     FileFill;
   TRcCmdFileSum      FileSum;
   TRcCmdFileMove     FileMove;
   TRcCmdFileCommit   FileCommit;
   TRcCmdAction       Action;
   TRcCmdGsmSend      GsmSend;
   TRcCmdGsmSendTo    GsmSendTo;
   TRcCmdGsmStatus    GsmStatus;
} TRcCmdUnion;

typedef struct {
   byte         Cmd;
   TRcCmdUnion Data;
} __packed TRcCmd;



#define RcReplySimpleSize()      (1)
#define RcReplySize( Item)  (RcReplySimpleSize() + sizeof( TRcReply##Item))

typedef struct {
   byte Data[RC_FILE_DATA_MAX];
} TRcReplyFileLoad;

#define RcReplyFileLoadSize( Count)         (RcReplySimpleSize() + Count)

typedef struct {
   TYesNo Match;
} __packed TRcReplyFileMatch;

typedef struct {
   TNvmSum Sum;
} __packed TRcReplyFileSum;

#define RcActionToRcSize( ActionSize)         (RcReplySimpleSize() + ActionSize)

typedef struct {
   TSmsGateSlotNumber SlotNumber;
} __packed TRcReplyGsmSend;

typedef TRcReplyGsmSend TRcReplyGsmSendTo;

typedef struct {
   TSmsGateSmsStatus SmsStatus;
} __packed TRcReplyGsmStatus;

typedef struct {
   TActionReply Action;
} __packed TRcReplyAction;


typedef union {
   TRcReplyFileLoad  FileLoad;
   TRcReplyFileMatch FileMatch;
   TRcReplyFileSum   FileSum;
   TRcReplyAction    Action;
   TRcReplyGsmSend   GsmSend;
   TRcReplyGsmSendTo GsmSendTo;
   TRcReplyGsmStatus GsmStatus;
} TRcReplyUnion;

typedef struct {
   byte          Reply;
   TRcReplyUnion Data;
} __packed TRcReply;

#ifdef __WIN32__
   #pragma pack( pop)                  // original alignment
#endif

#define RcReply(Cmd, Ok)   (Cmd | (Ok ? 0x00 : 0x80))

#endif
