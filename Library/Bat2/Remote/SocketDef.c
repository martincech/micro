//*****************************************************************************
//
//   SocketDef.c     Bat2 socket definitions
//   Version 1.0     (c) VEIT Electronics
//
//*****************************************************************************

#include "SocketDef.h"
#include "Remote/SocketInternal.h"

#include "Remote/SocketIfUsb.h"
#include "Remote/SocketIfMsd.h"

const TSocketInterface Interface[SOCKET_INTERFACE_COUNT] = {
      {
      SocketIfUsbPermission,
      SocketIfUsbState,
      SocketIfUsbReceive,
      SocketIfUsbReceiveSize,
      SocketIfUsbSend,
      SocketIfUsbClose,
      SocketIfUsbListen,
   },
      {
      SocketIfMsdPermission,
      SocketIfMsdState,
      SocketIfMsdReceive,
      SocketIfMsdReceiveSize,
      SocketIfMsdSend,
      SocketIfMsdClose,
      SocketIfMsdListen,
   },
};

const TSocketDescriptor Sockets[SOCKET_COUNT] = {
   {SOCKET_INTERFACE_USB},
   {SOCKET_INTERFACE_MSD},
};
