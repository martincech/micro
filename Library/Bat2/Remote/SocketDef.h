//*****************************************************************************
//
//   SocketDef.h     Bat2 socket definitions
//   Version 1.0     (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __SocketDef_H__
   #define __SocketDef_H__

typedef enum {
   SOCKET_INTERFACE_USB,
   SOCKET_INTERFACE_MSD,
   SOCKET_INTERFACE_COUNT
} ESocketInterface;

#define SOCKET_USB_COUNT         1
#define SOCKET_MSD_COUNT         2

#define SOCKET_COUNT    (SOCKET_USB_COUNT + SOCKET_MSD_COUNT)

typedef enum {
   SOCKET_USB_0,
   SOCKET_MSD_0,
} ESocketId;

typedef struct {
   int Interface;
} TSocketDescriptor;

typedef struct {
   byte Id;
   byte Interface;
} TSocket;

#endif
