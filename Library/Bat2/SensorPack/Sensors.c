//*****************************************************************************
//
//    Sensors.c     SensorPack sensors
//    Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#include "SensorPack/Sensors/Sensors.h"

int8 _Temperature;
uint8 _Humidity;
uint16 _CO2;

int64 TemperatureSum;
int TemperatureCount;
int64 HumiditySum;
int HumidityCount;
int64 CO2Sum;
int CO2Count;

void SensorsInit( void) {
}

int8 TemperatureRead( void) {
   return _Temperature;
}

uint8 HumidityRead( void) {
   return _Humidity;
}

uint16 CO2Read( void) {
   return _CO2;
}

int8 TemperatureAverageRead( void) {
   if(TemperatureCount == 0) {
      return 0;
   }
   int Temperature =  TemperatureSum / TemperatureCount;
   TemperatureSum = TemperatureCount = 0;
   return Temperature;
}

uint8 HumidityAverageRead( void) {
   if(HumidityCount == 0) {
      return 0;
   }
   int Humidity =  HumiditySum / HumidityCount;
   HumiditySum = HumidityCount = 0;
   return Humidity;
}

uint16 CO2AverageRead( void) {
   if(CO2Count == 0) {
      return 0;
   }
   int CO2 =  CO2Sum / CO2Count;
   CO2Sum = CO2Count = 0;
   return CO2;
}

void TemperatureSet( int8 Temperature) {
   _Temperature = Temperature;
}

void HumiditySet( uint8 Humidity) {
   _Humidity = Humidity;
}

void CO2Set( uint16 CO2) {
   _CO2 = CO2;
}

void SensorsSetFromModbus( TSensorsModbusRegisters *Sensors) {
   _Temperature = Sensors->Temperature;
   TemperatureSum += Sensors->Temperature;
   TemperatureCount++;
   _Humidity = Sensors->Humidity;
   HumiditySum += Sensors->Humidity;
   HumidityCount++;
   _CO2 = Sensors->CO2;
   CO2Sum += Sensors->CO2;
   CO2Count++;
}
