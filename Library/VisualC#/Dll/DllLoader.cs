﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;

namespace System {
   public static class DllLoader {
      public static void Load(string Dll) {
         // Load dll from resources
         System.Reflection.Assembly asm = System.Reflection.Assembly.GetEntryAssembly();
         System.IO.Stream stream = asm.GetManifestResourceStream(GetType().Name + "." + Dll); //this return null of course
         byte[] DllData = new byte[stream.Length];
         stream.Read(DllData, 0, (int)stream.Length);

         // Write to temporary files
         string DllPath = System.IO.Path.GetTempPath() + Dll;
         File.Delete(DllPath);
         FileStream fs = new FileStream(DllPath, FileMode.CreateNew);
         BinaryWriter w = new BinaryWriter(fs);
         w.Write(DllData);
         w.Close();
         fs.Close();

         // Set dll path
         SetDllDirectory(System.IO.Path.GetTempPath());


         List<String> Devices = EzPort.ListUp();

         if(Devices.Count > 0) {
            DeviceComboBox.DataSource = Devices;
            DeviceComboBox.SelectedIndex = 0;
         }
      }

      [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
      private static extern void SetDllDirectory(string lpPathName);
   }
}
