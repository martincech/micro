//******************************************************************************
//
//   UsbBase.cpp  USB basic operations
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __UsbBase_H__
   #define __UsbBase_H__

#include <windows.h>
#include <setupapi.h>

//******************************************************************************
// TUsbBase
//******************************************************************************

class TUsbBase {
public :
   virtual ~TUsbBase();
   // Destructor

   virtual BOOL Open();
   // Open located device

   virtual void Close();
   // Close opened device
   
   BOOL IsOpen();
   // Check status of opened device

   char *GetPath();
   // Returns device path

   bool operator==(const TUsbBase& other);
   bool operator!=(const TUsbBase& other){ return !operator==(other); }
protected:
   TUsbBase();
   // Constructor

   BOOL Locate(GUID *Guid, int Index = 0);
   // Locate class by <Guid> with device <Index>


   HANDLE GetHandle();
   // Get opened handle

//------------------------------------------------------------------------------
protected :
   char   *Path;
   HANDLE Handle;
}; // TUsbBase

#endif
