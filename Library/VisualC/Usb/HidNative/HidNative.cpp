//******************************************************************************
//
//   HidNative.cpp    Native HID
//   Version 1.0     (c) Veit Electronics
//
//******************************************************************************

#include <windows.h>
#pragma hdrstop

#include "HidNative.h"
#include "Usb/Hid/Hid.h"
#include <stdio.h>

#ifdef DEBUG
static FILE *logFile;
#endif
#define LoggerFrame( frame, size)    for(int i = 0; i< size;i++){LoggerReport("%02X ", (frame)[i]);}LoggerReport("\n")
#define LoggerTx( frame, size)       LoggerReport( "TX: ");LoggerFrame((frame), size)
#define LoggerRx( frame, size)       LoggerReport( "RX: ");LoggerFrame((frame), size)
#define LoggerGarbage( frame, size)  LoggerReport( "GARBAGE: ");LoggerFrame((frame), size)
#ifdef DEBUG
#define LoggerReport( txt, ...)      fprintf(logFile, txt, ##__VA_ARGS__);fprintf(stderr, txt, ##__VA_ARGS__)
#else
#define LoggerReport( txt, ...)
#endif

//******************************************************************************
// Constructor
//******************************************************************************

THidNative::THidNative(THid *hid)
// Constructor
{
#ifdef DEBUG
   logFile = fopen("D:\\LogFile.txt", "w");
#endif
   State = new TFrameState();
   Hid = hid;
   FrameInit( State);
   FrameTimeoutSet( State, 0, 0);
   //_CrtDump = NULL;
   IsOpen = NO;
} // THidNative

THidNative::THidNative()
{
   THidNative(THid::Open(MY_DEVICE_NAME));
}
//******************************************************************************
// Destructor
//******************************************************************************

THidNative::~THidNative()
// Destructor
{
#ifdef DEBUG
   fclose(logFile);
#endif
   delete State;
   delete Hid;
} // ~THidNative

//******************************************************************************
// Crt
//******************************************************************************

//******************************************************************************
// Send
//******************************************************************************

#define CHUNK_SIZE      64

TYesNo THidNative::Send(const void *Buffer, int Size)
// Send
{
   byte Chunk[CHUNK_SIZE];
   int ChunkSize;
   if(!CheckOpen()) {
      LoggerReport( "Tx error: No device\n");
      return NO;
   }

   if(!FrameSendInit(State, Buffer, Size)) {
      LoggerReport( "Tx error\n");
      return NO;
   }

   forever {
      if(!FrameSendProccess(State, Chunk, CHUNK_SIZE, &ChunkSize)) {
         return NO;
      }
      if(!Hid->WriteReport(Chunk)) {
         return NO;
      }
      LoggerTx(Chunk, CHUNK_SIZE);
      switch(State->Status) {
         case FRAME_STATE_SEND_ACTIVE:
            break;

         case FRAME_STATE_SEND_DONE:
            /*LoggerTx((byte*)Buffer, Size);*/
            return YES;

         default:
            return NO;
      }
   }
} // HidNativeSend

//******************************************************************************
// Receive
//******************************************************************************

TYesNo THidNative::Receive(void *Buffer, int Size, int *ReceivedSize)
// Receive
{
   byte Chunk[CHUNK_SIZE];
   if(!CheckOpen()) {
      LoggerReport( "Rx error: No device\n");
      return NO;
   }

   if(!FrameReceiveStart(State, Buffer, Size)) {
      LoggerReport( "Rx error\n");
      return NO;
   }

   forever {
      if(!Hid->ReadReport(Chunk)) {
         return NO;
      }
      LoggerRx(Chunk, CHUNK_SIZE);
      FrameReceiveProccess(State, Chunk, CHUNK_SIZE);
      switch(State->Status) {
         case FRAME_STATE_RECEIVE_ACTIVE:
            break;

         case FRAME_STATE_RECEIVE_FRAME:
            *ReceivedSize = State->SizeProccessed;
            //LoggerRx((byte*)Buffer, *ReceivedSize);
            return YES;

         default:
            return NO;
      }
   }
} // HidNativeReceive

//******************************************************************************
// Flush
//******************************************************************************

void THidNative::Flush( void)
// Flush
{
   if (Hid){
      Hid->Flush();
   }
} // Flush

//******************************************************************************
// Receive size
//******************************************************************************

word THidNative::ReceiveSize( void)
// Receive size
{
   return State->SizeProccessed;
} //HidNativeReceiveSize

void THidNative::Close(void)
{
byte Buffer[4];
   if (Hid){
      Hid->SetFeature(&Buffer);
      Hid->Close();
   }
}

byte THidNative::Status()
{
   return State->Status;
}
//*****************************************************************************



//******************************************************************************
// Check open
//******************************************************************************

TYesNo THidNative::CheckOpen( void)
// Check open
{
byte Buffer[CHUNK_SIZE];
   
   if (!Hid) return NO;
   if(IsOpen) {
      if(!Hid->SetInputBuffers( 512)) { // check if HID is open
         IsOpen = NO;
      } else {
         return YES;
      }
   }
   if(!((TUsbBase*)Hid)->Open()) {
      return NO;
   }
   if(!Hid->SetInputBuffers( 512)) {
      return NO;
   }
   if(Hid->GetInputBuffers() != 512) {
      return NO;
   }
   IsOpen = YES;

   memset(Buffer, 0x33, CHUNK_SIZE);
   Send(Buffer, 1);
   return YES;
}
