//******************************************************************************
//
//   HidNative.h      Native HID
//   Version 1.0     (c) Veit Electronics
//
//******************************************************************************

#ifndef __HidNative_H__
	#define __HidNative_H__

#include "Usb/Hid/Hid.h"
#include "Remote/Frame.h"


//******************************************************************************
// THid
//******************************************************************************

class THidNative {
public :
   THidNative(THid *hid);
   // Constructor
   THidNative();
   virtual ~THidNative();
   // Destructor

   TYesNo Send(const void *Buffer, int Size);
   // Send

   TYesNo Receive(void *Buffer, int Size, int *ReceivedSize);
   // Receive

   void Flush( void);
   // Flush

   word ReceiveSize( void);
   // Receive size

   void Close( void);

   byte Status();

//---------------------------------------------------------------------------

   THid *Hid;
protected:
   TFrameState *State;
   TYesNo IsOpen;

   TYesNo CheckOpen( void);
   // Check open

}; // THidNative

#endif

