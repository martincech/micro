//******************************************************************************
//
//   Hid.cpp      HID library class
//   Version 1.0  (c) VymOs
//
//******************************************************************************


#include <windows.h>
#include <setupapi.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <exception>
#pragma hdrstop
#include <dbt.h> 
#include "Hid.h"

#include "Unisys/Uni.h"
#pragma comment (lib, "hid.lib")


#define HID_MAX_DEVICES   100    // maximum number of checked devices
#define HID_WRITE_TIMEOUT 500    // write timeout
#define HID_READ_TIMEOUT  500    // read timeout

#define MAX_STRING        128

#define wcsequ( s1, s2)   (!wcscmp( s1, s2))
#define if_errh( function) if( (function) == INVALID_HANDLE_VALUE)
#define if_okh(  function) if( (function) != INVALID_HANDLE_VALUE)


HDEVNOTIFY THid::callBackHandle = NULL;
//******************************************************************************
// Constructor
//******************************************************************************
THid::THid(char *path) : TUsbBase()
{
   Path = new char[strlen(path) + 1];
   strcpy(Path, path);
   if (!TUsbBase::Open()){
      std::string msg = "Bad path: ";
      msg += path;
      delete(Path);
      Path = NULL;
      throw std::exception(msg.c_str());
   }
   init();
   this->GetCapabilities();
   TUsbBase::Close();
}

THid::THid() : TUsbBase()
// Constructor
{
   init();
} // THid

void THid::init()
{
   Report = (byte *)malloc(HID_REPORT_SIZE + 1);
   // create event :
   this->ReportEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
   HidOverlapped.hEvent = this->ReportEvent;
   HidOverlapped.Offset = 0;
   HidOverlapped.OffsetHigh = 0;

   memset(&Capabilities, 0, sizeof(Capabilities));
   Capabilities.OutputReportByteLength = 1;
   Capabilities.InputReportByteLength = 1;
}
//******************************************************************************
// Destructor
//******************************************************************************

THid::~THid()
// Destructor
{
   if_okh(this->ReportEvent){
      CloseHandle(this->ReportEvent);
   }
   if( Report){
      free( Report);
   }
} // ~THid

std::vector<THid *> THid::ConnectedDevices()
{
   std::vector<THid *> devices;
   // convert name to wide character :
   /*wchar_t WName[MAX_STRING + 1];
   ::MultiByteToWideChar(CP_ACP, 0, MY_DEVICE_NAME, -1, WName, MAX_STRING);*/
   // get HID GUID :
   GUID Guid;
   HidD_GetHidGuid(&Guid);
   // locate by name :
   wchar_t Name[MAX_STRING + 1];;
   THid *hid = new THid();
   for (int i = 0; i < HID_MAX_DEVICES; i++){
      // locate GUID :
      if (!hid->Locate(&Guid, i)){
         break;
      }
      // get device path :
      if (!hid->Path){
         break;
      }
      // open device :
      if (!((TUsbBase*)hid)->Open()){
         continue;
      }
      // get product name :
      if (!HidD_GetProductString(hid->Handle, Name, MAX_STRING + 1)){
         hid->Close();
         continue;
      }
      if (!hid->GetCapabilities()){
         hid->Close();
         continue;
      }
      hid->Close();
      devices.push_back(hid);
      hid = new THid();
   }
   delete(hid);
   return(devices);
}

bool THid::RegisterDeviceChangeCallback(HANDLE hWnd)
{
   // get HID GUID :
   if (callBackHandle){
      return false;
   }
   GUID Guid;
   HidD_GetHidGuid( &Guid);
   // notification filter for adding/removing device
   DEV_BROADCAST_DEVICEINTERFACE NotificationFilter;
   ZeroMemory(&NotificationFilter, sizeof(NotificationFilter));
   NotificationFilter.dbcc_size = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
   NotificationFilter.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
   NotificationFilter.dbcc_classguid = Guid;
   
   callBackHandle = RegisterDeviceNotification(
      hWnd,                       // events recipient
      &NotificationFilter,        // type of device
      DEVICE_NOTIFY_WINDOW_HANDLE |
      DEVICE_NOTIFY_ALL_INTERFACE_CLASSES // type of recipient handle
      );

   if (callBackHandle == NULL){
      return false;
   }
   return true;
}

void THid::UnregisterCallback()
{
   if (!callBackHandle){
      return;
   }
   UnregisterDeviceNotification(callBackHandle);
   callBackHandle = NULL;
}
//******************************************************************************
// Open
//******************************************************************************

THid *THid::Open( char *DeviceName)
// Open device by <Name>
{
   // convert name to wide character :
   wchar_t WName[ MAX_STRING + 1];
   if( !::MultiByteToWideChar( CP_ACP, 0, DeviceName, -1, WName, MAX_STRING)){
      return( FALSE);
   }
   // get HID GUID :
   GUID Guid;
   HidD_GetHidGuid( &Guid);
   // locate by name :
   wchar_t Name[ MAX_STRING + 1];
   BOOL    Found = FALSE;
   THid *hid = new THid();
   for( int i = 0; i < HID_MAX_DEVICES; i++){
      // locate GUID :
      if( !hid->Locate( &Guid, i)){
         delete(hid);
         return(NULL);
      }
      // get device path :
      if (!hid->Path){
         delete(hid);
         return(NULL);
      }
      // open device :
      if (!((TUsbBase*)hid)->Open()){
         continue;
      }
      // get product name :
      if (!HidD_GetProductString(hid->Handle, Name, MAX_STRING + 1)){
         hid->Close();
         continue;
      }
      if( wcsequ( WName, Name)){
         Found = TRUE;
         break;
      }
      hid->Close();
   }
   if( !Found){
      delete(hid);
      return( NULL);
   }
   // get device capabilities :
   if (!hid->GetCapabilities()){
      hid->Close();
      delete(hid);
      return(NULL);
   }
   return( hid);
} // Open

////******************************************************************************
// Open by VID & PID
//******************************************************************************

THid *THid::Open(int Vid, int Pid)
// Open device by <Vid> & <Pid>
{
   // get HID GUID :
   GUID Guid;
   HidD_GetHidGuid( &Guid);
   // locate by VID & PID :
   HIDD_ATTRIBUTES Attributes;
   BOOL            Found = FALSE;
   
   THid *hid = new THid();
   for (int i = 0; i < HID_MAX_DEVICES; i++){
      // locate GUID :
      if (!hid->Locate(&Guid, i)){
         delete(hid);
         return(NULL);
      }
      // get device path :
      if (!hid->Path){
         delete(hid);
         return(NULL);
      }
      // open device :
      if (!((TUsbBase *)hid)->Open()){
         continue;
      }
      // get HID attributes :
      Attributes.Size = sizeof( HIDD_ATTRIBUTES);
      if( !HidD_GetAttributes( hid->GetHandle(), &Attributes)){
         hid->Close();
         continue;
      }
      if( Attributes.VendorID  == Vid &&
          Attributes.ProductID == Pid){
         Found = TRUE;
         break;
      }
      hid->Close();
   }
   if( !Found){
      return( FALSE);
   }
   // get device capabilities :
   if( !hid->GetCapabilities()){
      hid->Close();
      delete(hid);
      return(NULL);
   }
   return( hid);
} // OpenBy

//******************************************************************************
// Write report
//******************************************************************************

BOOL THid::WriteReport( void *Buffer)
// Write single report <Buffer>
{
   Report[ 0] = 0;                     // Report ID
   memcpy( &Report[ 1], Buffer, Capabilities.OutputReportByteLength - 1);
   DWORD WrittenBytes;
   if( !WriteFile( Handle, Report, Capabilities.OutputReportByteLength, &WrittenBytes, &HidOverlapped)){
      if( GetLastError() != ERROR_IO_PENDING){
         return( FALSE);
      }
      DWORD Result = WaitForSingleObject( this->ReportEvent, HID_WRITE_TIMEOUT);
      if( Result == WAIT_TIMEOUT || Result == WAIT_ABANDONED){
         CancelIo( Handle);
         return( FALSE);
      }
      if( Result != WAIT_OBJECT_0){
         return( FALSE);
      }
      if( !GetOverlappedResult( Handle, &HidOverlapped, &WrittenBytes, FALSE)){
         return( FALSE);
      }
   } // else done immediately
   if( WrittenBytes != (DWORD)Capabilities.OutputReportByteLength){
      return( FALSE);
   }
   return( TRUE);
} // WriteReport

//******************************************************************************
// Set feature
//******************************************************************************
#include "Timer/Timer.h"
BOOL THid::SetFeature( void *Buffer)
   // Set feature
{
   Report[ 0] = 0;                     // Report ID
   memcpy( &Report[ 1], Buffer, Capabilities.FeatureReportByteLength - 1);
   if(HidD_SetFeature(Handle, Report, Capabilities.FeatureReportByteLength)) {
      return true;
   }
   WORD error = GetLastError();
   return false;
} // SetFeature

//******************************************************************************
// Read report
//******************************************************************************

BOOL THid::ReadReport( void *Buffer)
// Read single report <Buffer>
{
   DWORD ReadBytes;
   DWORD Size;
   Size = GetFileSize( Handle, NULL);
   if( !ReadFile( Handle, Report, Capabilities.InputReportByteLength, &ReadBytes,  &HidOverlapped)){
      if( GetLastError() != ERROR_IO_PENDING){
         return( FALSE);
      }
      DWORD Result = WaitForSingleObject( this->ReportEvent, HID_READ_TIMEOUT);
      if( Result == WAIT_TIMEOUT || Result == WAIT_ABANDONED){
         CancelIo(Handle);
         return( FALSE);
      }
      if( Result != WAIT_OBJECT_0){
         return( FALSE);
      }
      if( !GetOverlappedResult( Handle, &HidOverlapped, &ReadBytes, FALSE)){
         return( FALSE);
      }
   } // else done immediately
   if( ReadBytes != (DWORD)Capabilities.InputReportByteLength){
      return( FALSE);
   }
   // copy report, skip report ID :
   memcpy( Buffer, &Report[ 1], Capabilities.InputReportByteLength - 1);
   return( TRUE);
} // ReadReport

//******************************************************************************
// Get feature
//******************************************************************************

BOOL THid::GetFeature( void *Buffer)
   // Set feature
{
   bool Status = false;
   DWORD Start = GetTickCount();
   Report[ 0] = 0;                     // Report ID
   while(TimeBefore(GetTickCount(), Start + HID_WRITE_TIMEOUT)) {
      if(HidD_GetFeature(Handle, Report, Capabilities.FeatureReportByteLength)) {
         Status = true;
         break;
      }
   }

   if(!Status) {
      return false;
   }

   memcpy( Buffer, &Report[ 1], Capabilities.FeatureReportByteLength - 1);
   return Status;
} // SetFeature

//******************************************************************************
// Flush
//******************************************************************************

BOOL THid::Flush()
// Flush input buffer
{
   return( HidD_FlushQueue( Handle));
} // Flush

//******************************************************************************
// Set input buffers
//******************************************************************************

BOOL THid::SetInputBuffers( int Count)
// Set input buffer reports <Count>
{
   return( HidD_SetNumInputBuffers( Handle, (ULONG)Count));
} // SetInputBuffers

//******************************************************************************
// Get input buffers
//******************************************************************************

int THid::GetInputBuffers()
// Returns input buffer reports count
{
   ULONG Count;
   if( !HidD_GetNumInputBuffers( Handle, &Count)){
      return( 0);
   }
   return( (int)Count);
} // GetInputBuffers

//******************************************************************************
// Report size
//******************************************************************************

int THid::GetOutputReportSize()
// Returns output report size
{
   return( Capabilities.OutputReportByteLength - 1);  // without ID byte
} // GetOutputReportSize

int THid::GetInputReportSize()
// Returns input report size
{
   return( Capabilities.InputReportByteLength - 1);   // without ID byte
} // GetInputReportSize

int THid::GetFeatureReportSize()
// Returns feature report size
{
   return( Capabilities.FeatureReportByteLength - 1);  // without ID byte
} // GetFeatureReportSize

//******************************************************************************
// Device name
//******************************************************************************

char *THid::GetDeviceName()
// Returns USB device name
{
   return( DeviceName);
} // GetDeviceName

//******************************************************************************
// Manufacturer
//******************************************************************************

char *THid::GetManufacturer()
// Returns USB device manufacturer
{
   return( Manufacturer);
} // GetManufacturer

//******************************************************************************
// Serial number
//******************************************************************************

char *THid::GetSerialNumber()
// Returns USB device serial number
{
   return( SerialNumber);
} // GetSerialNumber

//******************************************************************************
// Get VID
//******************************************************************************

int THid::GetVid()
// Returns USB device VID
{
   return( DeviceVid);
} // GetVid

//******************************************************************************
// Get PID
//******************************************************************************

int THid::GetPid()
// Returns USB device PID
{
   return( DevicePid);
} // GetPid

//------------------------------------------------------------------------------

//******************************************************************************
// Get capabilities
//******************************************************************************
BOOL THid::GetCapabilities()
// Fill capabilities and strings
{
wchar_t String[ MAX_STRING + 1];
char    Ansi[ MAX_STRING + 1];

   // get device attributes :
   HIDD_ATTRIBUTES Attributes;
   Attributes.Size = sizeof( HIDD_ATTRIBUTES);
   if( !HidD_GetAttributes( TUsbBase::GetHandle(), &Attributes)){
      return( FALSE);
   }
   DeviceVid = Attributes.VendorID;
   DevicePid = Attributes.ProductID;
   // get device capability :
	PHIDP_PREPARSED_DATA HidParsedData;
	if( !HidD_GetPreparsedData( Handle, &HidParsedData)){
      return( FALSE);
   }
	if( !HidP_GetCaps( HidParsedData, &Capabilities)){
      HidD_FreePreparsedData( HidParsedData);
      return( FALSE);
   }
   HidD_FreePreparsedData( HidParsedData);
   // get product string :
   if( !HidD_GetProductString( Handle, String, MAX_STRING + 1)){
      return( FALSE);
   }
   if( !::WideCharToMultiByte( CP_ACP, 0, String,  -1, Ansi, MAX_STRING, NULL, NULL)){
      return( FALSE);
   }
   strncpy( DeviceName, Ansi, HID_DEVICE_NAME_SIZE);
   DeviceName[ HID_DEVICE_NAME_SIZE] = '\0';
   // get manufacturer string :
   if( !HidD_GetManufacturerString( Handle, String, MAX_STRING + 1)){
      return( FALSE);
   }
   if( !::WideCharToMultiByte( CP_ACP, 0, String,  -1, Ansi, MAX_STRING, NULL, NULL)){
      return( FALSE);
   }
   strncpy( Manufacturer, Ansi, HID_MANUFACTURER_SIZE);
   DeviceName[ HID_MANUFACTURER_SIZE] = '\0';
   // get serial number string :
   if( !HidD_GetSerialNumberString( Handle, String, MAX_STRING + 1)){
      return( FALSE);
   }
   if( !::WideCharToMultiByte( CP_ACP, 0, String,  -1, Ansi, MAX_STRING, NULL, NULL)){
      return( FALSE);
   }
   strncpy( SerialNumber, Ansi, HID_SERIAL_NUMBER_SIZE);
   DeviceName[ HID_SERIAL_NUMBER_SIZE] = '\0';
   return( TRUE);
} // GetCapabilities
