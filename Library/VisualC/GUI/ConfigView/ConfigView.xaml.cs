﻿using System;
using System.Windows.Controls;
using System.Windows;
using Bat2Library;
using Localization;

namespace BatApp
{
   /// <summary>
   /// Interaction logic for ConfigView.xaml
   /// </summary>
   public partial class ConfigView
   {
      
      public ConfigView()
      {
         InitializeComponent();
      }
      #region Command execution handlers
      void ServerCommand_ExecutionProblem(object sender, Exception e)
      {
         MessageBox.Show(e.Message, "Communication Error", MessageBoxButton.OK, MessageBoxImage.Error);
      }

      void DiscardConfigChangesCommand_AboutToExecute(object sender, CancelCommandEventArgs e)
      {
         e.Cancel = false;
         if( MessageBox.Show("Really discard changes?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question)
            == MessageBoxResult.No)
         {
            e.Cancel = true;
         }
      }
      #endregion

      #region Events
      private void window_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
      {
         DeviceManagerViewModel vm = e.OldValue as DeviceManagerViewModel;

         if (vm != null)
         {
            vm.LoadDevicesCommand.ExecutionProblem -= ServerCommand_ExecutionProblem;
            vm.SaveConfigCommand.ExecutionProblem -= ServerCommand_ExecutionProblem;
            vm.DiscardConfigChangesCommand.AboutToExecute -= DiscardConfigChangesCommand_AboutToExecute;
         }

         vm = e.NewValue as DeviceManagerViewModel;
         if (vm != null)
         {
            vm.LoadDevicesCommand.ExecutionProblem += ServerCommand_ExecutionProblem;
            vm.SaveConfigCommand.ExecutionProblem += ServerCommand_ExecutionProblem;
            vm.DiscardConfigChangesCommand.AboutToExecute += DiscardConfigChangesCommand_AboutToExecute;
         }
      }
      
      private void DateFormatCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
      {
         RedrawDateExample();
      }
      private void FirstSeparatorTB_TextChanged(object sender, TextChangedEventArgs e)
      {
         RedrawDateExample();
      }
      private void SecondSeparatorTB_TextChanged(object sender, TextChangedEventArgs e)
      {
         RedrawDateExample();
      }
      private void TimeFormatCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
      {
         RedrawTimeExample();
      }

      private void TimeSeparatorTB_TextChanged(object sender, TextChangedEventArgs e)
      {
         RedrawTimeExample();
      }
      #endregion

      #region Private Helpers
      //------------------------------------------------------------------------------
      //   Redraw date example
      //------------------------------------------------------------------------------

      private void RedrawDateExample()
      {
         if (DateFormatCB.SelectedItem == null)
         {
            return;
         }
         string str = "";
         string fSep = FirstSeparatorTB.Text;
         string sSep = SecondSeparatorTB.Text;
         switch ((DateFormatE)new DateFormatEConverter().ConvertFrom(null, null, DateFormatCB.SelectedItem))
         {
            case DateFormatE.DATE_FORMAT_DDMMYYYY:
               str = DateTime.Now.ToString("dd" + fSep + "MM" + sSep + "yy");
               break;
            case DateFormatE.DATE_FORMAT_MMDDYYYY:
               str = DateTime.Now.ToString("MM" + fSep + "dd" + sSep + "yy");
               break;
            case DateFormatE.DATE_FORMAT_YYYYMMDD:
               str = DateTime.Now.ToString("yy" + fSep + "MM" + sSep + "dd");
               break;
            case DateFormatE.DATE_FORMAT_YYYYDDMM:
               str = DateTime.Now.ToString("yy" + fSep + "dd" + sSep + "MM");
               break;
            case DateFormatE.DATE_FORMAT_DDMMMYYYY:
               str = DateTime.Now.ToString("dd" + fSep + "MMM" + sSep + "yy");
               break;
            case DateFormatE.DATE_FORMAT_MMMDDYYYY:
               str = DateTime.Now.ToString("MMM" + fSep + "dd" + sSep + "yy");
               break;
            case DateFormatE.DATE_FORMAT_YYYYMMMDD:
               str = DateTime.Now.ToString("yy" + fSep + "MMM" + sSep + "dd");
               break;
            case DateFormatE.DATE_FORMAT_YYYYDDMMM:
               str = DateTime.Now.ToString("yy" + fSep + "dd" + sSep + "MMM");
               break;
         }
         DateExampleTB.Text = str;
      }

      //------------------------------------------------------------------------------
      //   Redraw time example
      //------------------------------------------------------------------------------

      private void RedrawTimeExample()
      {
         if (TimeFormatCB.SelectedItem == null)
         {
            return;
         }
         string str = "";
         string sep = TimeSeparatorTB.Text;
         switch ((TimeFormatE)new TimeFormatEConverter().ConvertFrom(null, null, TimeFormatCB.SelectedItem))
         {
            case TimeFormatE.TIME_FORMAT_12:
               str = DateTime.Now.ToString("hh" + sep + "mmtt");
               break;
            case TimeFormatE.TIME_FORMAT_24:
               str = DateTime.Now.ToString("HH" + sep + "mm");
               break;
         }
         TimeExampleTB.Text = str;
      }
      #endregion

   }
}
