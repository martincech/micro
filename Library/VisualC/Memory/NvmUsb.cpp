//*****************************************************************************
//
//   Nvm.cpp       Nonvolatile memory simulator for Qt
//   Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#include "Memory/Nvm.h"
#include <string.h>
#include "Usb/Bat2Usb.h"
#include "HidNative.h"
#include "System/System.h"

TBat2Usb *Usb;
THidNative *HidNative;

#define REMOTE_ENTER_TIMEOUT        5   // s

//------------------------------------------------------------------------------
//  Setup
//------------------------------------------------------------------------------

TYesNo NvmSetup( const char *FileName)
// Setup NVM from <FileName>
{
dword Start;
   HidNative = new THidNative();
   Usb = new TBat2Usb( HidNative, NULL);

   Start = SysClock();
   while(Start + REMOTE_ENTER_TIMEOUT > SysClock()) {
      if(Usb->RemoteEnter()) {
         return YES;
      }
   }
   return NO;
} // NvmSetup

//------------------------------------------------------------------------------
//  Shutdown
//------------------------------------------------------------------------------

TYesNo NvmShutdown( const char *FileName)
// Save NVM to <FileName>
{
   NvmCommit();
   delete Usb;
   delete HidNative;
   return YES;
} // NvmShutdown

//------------------------------------------------------------------------------
//  Initialization
//------------------------------------------------------------------------------

void NvmInit( void)
// Initialize
{
} // NvmInit

//------------------------------------------------------------------------------
//  Write byte
//------------------------------------------------------------------------------

void NvmByteWrite( TNvmAddress Address, byte Value)
// Write <Value> at <Address>
{
   NvmSave( Address, &Value, sizeof(Value));
} // NvmByteWrite

//------------------------------------------------------------------------------
//  Write word
//------------------------------------------------------------------------------

void NvmWordWrite( TNvmAddress Address, word Value)
// Write <Value> at <Address>
{
   NvmSave( Address, &Value, sizeof(Value));
} // NvmWordWrite

//------------------------------------------------------------------------------
//  Write dword
//------------------------------------------------------------------------------

void NvmDwordWrite( TNvmAddress Address, dword Value)
// Write <Value> at <Address>
{
   NvmSave( Address, &Value, sizeof(Value));
} // NvmDwordWrite

//------------------------------------------------------------------------------
//  Read byte
//------------------------------------------------------------------------------

byte NvmByteRead( TNvmAddress Address)
// Returns value from <Address>
{
byte Value = 0;
   NvmLoad( Address, &Value, sizeof(Value));
   return Value;
} // NvmByteRead

//------------------------------------------------------------------------------
//  Read word
//------------------------------------------------------------------------------

word NvmWordRead( TNvmAddress Address)
// Returns value from <Address>
{
word Value = 0;
   NvmLoad( Address, &Value, sizeof(Value));
   return Value;
} // NvmWordRead

//------------------------------------------------------------------------------
//  Read dword
//------------------------------------------------------------------------------

dword NvmDwordRead( TNvmAddress Address)
// Returns value from <Address>
{
dword Value = 0;
   NvmLoad( Address, &Value, sizeof(Value));
   return Value;
} // NvmDwordRead

//------------------------------------------------------------------------------
//  Save
//------------------------------------------------------------------------------

TYesNo NvmSave( TNvmAddress Address, const void *Data, int Size)
// Save <Data> with <Size> at <Address>
{
   return Usb->NvmSave(Address, Data, Size);
} // NvmSave

//------------------------------------------------------------------------------
//  Load
//------------------------------------------------------------------------------

void NvmLoad( TNvmAddress Address, void *Data, int Size)
// Load <Data> with <Size> from <Address>
{
   Usb->NvmLoad(Address, Data, Size);
} // NvmLoad

//------------------------------------------------------------------------------
//  Fill
//------------------------------------------------------------------------------

TYesNo NvmFill( TNvmAddress Address, byte Pattern, int Size)
// Write <Pattern> with <Size> at <Address>
{
   return Usb->NvmFill(Address, Pattern, Size);
} // NvmFill

//------------------------------------------------------------------------------
//  Match
//------------------------------------------------------------------------------

TYesNo NvmMatch( TNvmAddress Address, const void *Data, int Size)
// Compare <Data> with <Size> at <Address>
{
TYesNo Match;
   if(!Usb->NvmMatch(Address, Data, Size, &Match)) {
      return NO;
   }
   return Match;
} // NvmMatch

//------------------------------------------------------------------------------
//  Sum
//------------------------------------------------------------------------------

TNvmSum NvmSum( TNvmAddress Address, int Size)
// Sum NVM contents at <Address> with <Size>
{
TNvmSum Sum;
   if(!Usb->NvmSum(Address, Size, &Sum)) {
      return -1;
   }
   return Sum;
} // NvmSum

//------------------------------------------------------------------------------
//  Move
//------------------------------------------------------------------------------

void NvmMove( TNvmAddress ToAddress, TNvmAddress FromAddress, int Size)
// Move data <FromAddress> to <ToAddress> with <Size>. Check for overlapping
{
   Usb->NvmMove(ToAddress, FromAddress, Size);
} // NvmMove

//------------------------------------------------------------------------------
//  Commit
//------------------------------------------------------------------------------

void NvmCommit( void)
// Permanently save data
{
   Usb->NvmCommit();
} // NvmCommit

//------------------------------------------------------------------------------
//  Rollback
//------------------------------------------------------------------------------

void NvmRollback( TNvmAddress Address)
// Rollback whole cache for <Address>
{
} // NvmRollback