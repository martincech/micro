//-----------------------------------------------------------------------------
//
//    Uni.h  - Universal definitions MS Visual C++
//    Version 1.0  (c) VymOs
//
//-----------------------------------------------------------------------------

#ifndef __Uni_H__
   #define __Uni_H__

#include <stddef.h>           // macro offsetof
#include <string.h>           // memcmp && strcmp etc



#ifdef _MANAGED
// exact size Intel-like data types :
typedef System::Byte      byte;
typedef System::UInt16     word;
typedef System::UInt32     dword;
typedef System::UInt64     qword;

// exact size signed types :
typedef System::SByte      int8;       // signed byte
typedef System::Int16      int16;      // signed word
typedef System::Int32      int32;      // signed dword
typedef System::Int64      int64;      // signed qword

// exact size unsigned types :
typedef System::Byte      uint8;      // byte
typedef System::UInt16     uint16;     // word
typedef System::UInt32     uint32;     // dword
typedef System::UInt64     uint64;     // qword
#else
// exact size Intel-like data types :
typedef unsigned char      byte;
typedef unsigned short     word;
typedef unsigned int       dword;
typedef unsigned __int64   qword;
typedef signed char        sbyte;
typedef signed short       sword;
typedef signed int         sdword;

// exact size signed types :
typedef signed char        int8;       // signed byte
typedef signed short       int16;      // signed word
typedef signed int         int32;      // signed dword
typedef signed __int64     int64;      // signed qword

// exact size unsigned types :
typedef unsigned char      uint8;      // byte
typedef unsigned short     uint16;     // word
typedef unsigned int       uint32;     // dword
typedef unsigned __int64   uint64;     // qword
#endif

// System's independent definition
#define if__err( function) if( (function) != 0)
#define if__ok( function)  if( (function) == 0)

#ifdef __WIN32__
#define if_errh( function) if( (function) == INVALID_HANDLE_VALUE)
#define if_okh(  function) if( (function) != INVALID_HANDLE_VALUE)
// Error messages 
// DOS and Win32 console
#define IERROR   printf( "Error on file %s line %d\n", __FILE__, __LINE__);
#define IDEFAULT default : printf( "Error switch file %s line %d\n", __FILE__, __LINE__);
#endif

#define if_null( function) if( (function) == NULL)
#define if_nonull( function) if( (function) != NULL)

#define while_nonull( function) while( (function) != NULL)

#define set_nonull( var, value) if( var != NULL){ *var = value;}

// Unused parameters
#define arg_used( a)   a = a

typedef union {
   qword  qw;         // 64 bitu long
   dword  dw;
   word   w;
   byte   b;
   float  f;
   double d;
   byte   array[8];
} TDataConvertor;

// nibble opration
#define lnibble( x)     ((x) & 0x0F)
#define hnibble( x)     (((x) >> 4) & 0x0F)

typedef enum {
   NO,
   YES
} TYesNoEnum;

// logical data types :
typedef dword         TYesNo;          // portable boolean

// infinite loop :
#ifndef forever
   #define forever     for(;;)
#endif

// mnemonic function aliases :
#define strequ( s1, s2)      !strcmp( s1, s2)
#define striequ( s1, s2)     !stricmp( s1, s2)
#define strnequ( s1, s2, n)  !strncmp( s1, s2, n)
#define strnequi( s1, s2, n) !strncmpi( s1, s2, n)
#define memequ( m1, m2, l)   !memcmp( m1, m2, l)

// string cut copy :
#define strncpyx( s1, s2, n) strncpy( s1, s2, n); s1[ n] = '\0';

// sizeof by type definition :
#define TSizeOf( type, item)   sizeof(((type *)0)->item)

// TYesNo coding  ( TYesNo is integer, any nonzero value == YES) :
#define YesNoSet( Item, Value)   Item = NO; if( Value) Item = YES
#define YesNoGet( Value)       ((Value) ? YES : NO)

// Fixed decimal point division :
#define DivCover( n, d)      (((n) + (d) - 1) / (d))    // division n / d, result * d >= n
#define DivInterior( n, d)   ((n) / (d))                // division n / d, result * d <= n
#define IntegerRound( n, m)  (((n) / (m)) * (m))        // integer rounding n to modulus m

// Array operations :
#define ArrayItemSize( Array)   (sizeof( Array[ 0]))                     // array item size
#define ArrayItemCount( Array)  (sizeof( Array) / ArrayItemSize( Array)) // array items count

// structure/union alignment :
#define __packed
/*
   Please use :   
   #pragma pack( push, 1)                    // byte alignment
      // packet data definitions
   #pragma pack( pop)                        // original alignment
*/

#ifdef _DLL
   #ifdef __cplusplus
      #define DllImportExport     extern "C" __declspec(dllexport)
   #else 
      #define DllImportExport     __declspec(dllexport)
   #endif
#else
   #ifdef __cplusplus
      #define DllImportExport     extern "C" __declspec(dllimport)
   #else 
      #define DllImportExport     __declspec(dllimport)
   #endif
#endif


#define ENDIAN_CONVERT_WORD( Data)       ((Data & 0xFF00) >> 8) | ((Data & 0x00FF) << 8)
#define ENDIAN_CONVERT_DWORD( Data)      ((Data & 0xFF000000) >> 24) | ((Data & 0x00FF0000) >> 8) | ((Data & 0x0000FF00) << 8) | ((Data & 0x000000FF) << 24)

#define ENDIAN_FROM_BIG_WORD( Data)      Data
#define ENDIAN_FROM_BIG_DWORD( Data)     Data

#define ENDIAN_TO_BIG_WORD( Data)        Data = Data
#define ENDIAN_TO_BIG_DWORD( Data)       Data = Data

#define ENDIAN_FROM_LITTLE_WORD( Data)   ENDIAN_CONVERT_WORD( Data)
#define ENDIAN_FROM_LITTLE_DWORD( Data)  ENDIAN_CONVERT_DWORD( Data)

#define ENDIAN_TO_LITTLE_WORD( Data)     Data = ENDIAN_CONVERT_WORD( Data)
#define ENDIAN_TO_LITTLE_DWORD( Data)    Data = ENDIAN_CONVERT_DWORD( Data)
#endif
