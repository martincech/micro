//******************************************************************************
//
//   HidStdio.h      Hid stdio
//   Version 1.0     (c) Veit Electronics
//
//******************************************************************************

#include <windows.h>
#pragma hdrstop

#include "HidStdio.h"
#include "Usb/Hid/Hid.h"

#define MY_VID 0x15A2
#define MY_PID 0x0300

//******************************************************************************
// Constructor
//******************************************************************************

THidStdio::THidStdio()
// Constructor
{
   HidNative = new THidNative();
} // TBootloader

//******************************************************************************
// Destructor
//******************************************************************************

THidStdio::~THidStdio()
// Destructor
{
   delete HidNative;
} // ~TBootloader

int THidStdio::putchar(int __c) {
   BufferTx[BufferTxPtr++] = __c;
   
   if(BufferTxPtr == BUFFER_SIZE) {
      HidNative->Send(BufferTx, BufferTxPtr);
   }
   
   return __c;
}

int THidStdio::kbhit( void) {
   return BufferRxPtr < BufferRxSize;
}

int THidStdio::getchar(const byte *Firmware, int Size) {
int Ch;

   while(BufferRxPtr == 0) {
      if(!HidNative->Receive(BufferHid, BUFFER_SIZE)) {
         continue;
      }
      
      BufferRxSize = HidNative->ReceiveSize();

      if(BufferRxSize != 0) {
         memcpy(BufferRx, BufferHid, BufferRxSize);
         break;
      }
   }

   Ch = BufferRx[BufferRxPtr++];
   
   if(BufferRxPtr == BufferRxSize) {
      BufferRxPtr = 0;
   }
   
   return Ch;
}