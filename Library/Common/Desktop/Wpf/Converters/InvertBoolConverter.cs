using System;
using System.Globalization;
using System.Windows.Data;

namespace Desktop.Wpf.Converters
{
   /// <summary>
   /// Bool inversion converter
   /// </summary>
   public class InvertBoolConverter : IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         object o = !System.Convert.ToBoolean(value);
         return o;
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return Convert(value, targetType, parameter, culture);
      }

      #endregion
   }
}