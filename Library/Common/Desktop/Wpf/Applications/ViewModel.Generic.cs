﻿#if NET45
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Desktop.Wpf.Presentation;

namespace Desktop.Wpf.Applications
{
   /// <summary>
   /// ViewModel class, represent ViewModel with single or multiple Views of type <see cref="IView"/>.
   /// This ViewModel corresponds to specific tModel, as specified by TModel.
   /// </summary>
   public abstract class ViewModel<TModel> : ViewModel
      where TModel : class, INotifyPropertyChanged, INotifyDataErrorInfo
   {
      private TModel model;

      #region Constructors

      /// <summary>
      /// Initializes a new instance of the <see cref="ViewModel"/> class for single view and attaches itself as <c>DataContext</c> to the view.
      /// </summary>
      /// <param name="models">existing models to be used by default</param>
      /// <param name="view">The view which will have this ViewModel attached as <c>DataContext</c>.</param>
      protected ViewModel(IView view = null, IEnumerable<TModel> models = null)
         : base(view)
      {
         Models = models == null
            ? new ObservableCollection<TModel>()
            : new ObservableCollection<TModel>(models);
         SelectedModels = new ObservableCollection<TModel>();
      }

      #endregion

      /// <summary>
      /// Currently selected tModel, changing of this member will change all tModel properties of this ViewModel.
      /// Have to be allways one of <see cref="Models"/> item, otherwise exception is thrown when trying to set this property.
      /// </summary>
      public TModel Model
      {
         get { return model; }
         set { SetPropertyAndValidate(ref model, value); }
      }

      /// <summary>
      /// Collection of all known <see cref="TModel"/> objects.
      /// </summary>
      public ObservableCollection<TModel> Models { get; private set; }

      /// <summary>
      /// Collection of selected models, subset of <see cref="Models"/>
      /// All items have to be allways from <see cref="Models"/>, otherwise exception is thrown.
      /// </summary>
      public ObservableCollection<TModel> SelectedModels { get; private set; }


      /// <summary>
      /// Delete model from <see cref="Models"/> collection and optionaly
      /// select next item in this collection, aka set <see cref="Model"/> to next model
      /// and add to <see cref="SelectedModels"/> as well, when empty. 
      /// Additional functionality (besides moving to next) is removing deleting model 
      /// from <see cref="SelectedModels"/> and from <see cref="Model"/>
      /// 
      /// </summary>
      /// <param name="c">model to be deleted</param>
      /// <param name="moveToNext">true/false whether to move/not move to next item in <see cref="Models"/></param>
      protected void DeleteModelWithOptionalMove(TModel c, bool moveToNext = false)
      {
         var index = Models.IndexOf(c);
         var newCount = Models.Count - 1;
         var selContains = SelectedModels.Contains(c);
         Models.Remove(c);
         SelectedModels.Remove(c);
         if (moveToNext && selContains && SelectedModels.Count == 0 && newCount != 0)
         {
            SelectedModels.Add(newCount > index
               ? Models[index]
               : Models.LastOrDefault());
         }
         if (Model == c)
         {
            Model = moveToNext ?
               (newCount == 0 ? null :
                  newCount > index
                     ? Models[index]
                     : Models.LastOrDefault()) : null;
         }
      }

      /// <summary>
      /// Add new model to <see cref="Models"/> collection and optionally select it,
      /// aka set it to <see cref="Model"/> property and add it to <see cref="SelectedModels"/>
      /// </summary>
      /// <param name="c">New model to be added</param>
      /// <param name="select">true/false whether select/not select this newly added model</param>
      protected void AddModelWithOptionalSelect(TModel c, bool select = false)
      {
         Models.Add(c);
         if (!select) return;

         Model = c;
         SelectedModels.Add(c);
      }

   }
}
#endif