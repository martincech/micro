using System;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Windows;

namespace Desktop.Wpf.Applications
{
   /// <summary>
   /// Synchronization object for collection synchronization. Synchronizes 2 collections
   /// which implements <see cref="INotifyCollectionChanged"/> where at begining
   /// those collection are initialized to values from <see cref="List"/>.
   /// </summary>
   public class CollectionsSynchronizer : IWeakEventListener
   {
      private INotifyCollectionChanged dependentList;
      private INotifyCollectionChanged list;

      /// <summary>
      /// Constructor to easily set properties.
      /// </summary>
      /// <param name="list">Main list (master), both will have this list values at begining</param>
      /// <param name="dependentList">Dependent list (slave)</param>
      public CollectionsSynchronizer(
         INotifyCollectionChanged list = null,
         INotifyCollectionChanged dependentList = null)
      {
         this.dependentList = dependentList;
         this.list = list;
         StartSynchronize();
      }

      ~CollectionsSynchronizer()
      {
         StopSynchronize();
      }

      /// <summary>
      /// Dependent list, will be set to values in <see cref="List"/> when set.
      /// </summary>
      public INotifyCollectionChanged DependentList
      {
         get { return dependentList; }
         set
         {
            if (dependentList == value){ return; }
            StopSynchronize();
            dependentList = value;
            StartSynchronize();
         }
      }
      
      /// <summary>
      /// Master list, change will set <see cref="DependentList"/> to this collection objects.
      /// </summary>
      public INotifyCollectionChanged List
      {
         get { return list; }
         set
         {
            if (list == value) { return; }
            StopSynchronize();
            list = value;
            StartSynchronize();
         }
      }


      /// <summary>
      /// Start synchronizing those two lists
      /// </summary>
      protected void StartSynchronize()
      {
         if (dependentList == null || list == null
            || dependentList == list) { return; }
         Debug.Assert(dependentList is IList);
         Debug.Assert(list is IList);

         var eList = list as IList;
         var eDepList = dependentList as IList;
         var copList = eList.Cast<object>().ToList();
         object value;
         foreach (var v in eDepList)
         {
            value = ConvertValue(eList, v);
            if (!eList.Contains(value))
            {
               eList.Add(value);
            }
         }
         foreach (var v in copList)
         {
            value = ConvertValue(eDepList, v);
            if (!eDepList.Contains(value))
            {
               eDepList.Add(value);
            }
         }

         ListenForChange(dependentList);
         ListenForChange(list);
      }

      /// <summary>
      /// Stop synchronizing those two lists
      /// </summary>
      protected void StopSynchronize()
      {
         ListenForChange(dependentList, false);
         ListenForChange(list, false);
      }

      /// <summary>
      /// Starts/Stops listening for listToListen change action.
      /// </summary>
      /// <param name="listToListen">listToListen to be listen on</param>
      /// <param name="listen">true/false whether start or stop</param>
      private void ListenForChange(INotifyCollectionChanged listToListen, bool listen = true)
      {
         if (listToListen == null)
         {
            return;
         }
         if (listen)
         {
            CollectionChangedEventManager.AddListener(listToListen, this);
         }
         else
         {
            CollectionChangedEventManager.RemoveListener(listToListen, this);
         }
      }

      /// <summary>
      /// Performs specific action on specific listToListen
      /// </summary>
      /// <param name="action">action to be performed</param>
      /// <param name="listForAction">listToListen to be performed on (action parameter)</param>
      /// <param name="e">change specification (action parameter)</param>
      private void PerformListAction(Action<IList, NotifyCollectionChangedEventArgs> action, IList listForAction, NotifyCollectionChangedEventArgs e)
      {
         ListenForChange(listForAction as INotifyCollectionChanged, false);
         action(listForAction, e);
         ListenForChange(listForAction as INotifyCollectionChanged);
      }

      /// <summary>
      /// Use converter to convert value, if converter exists
      /// </summary>
      /// <param name="l">listToListen to which value will be converted in</param>
      /// <param name="value">value to be converted</param>
      /// <returns>converted value or original value when conversion not posible</returns>
      protected virtual object ConvertValue(IEnumerable l, object value)
      {
         return value;
      }
      
      /// <summary>
      /// Add new items action
      /// </summary>
      /// <param name="l"></param>
      /// <param name="ce"></param>
      private void AddItems(IList l, NotifyCollectionChangedEventArgs ce)
      {
         var count = ce.NewItems.Count;
         for (var i = 0; i < count; i++)
         {
            var insertionPoint = ce.NewStartingIndex + i;

            if (insertionPoint > l.Count)
            {
               l.Add(ConvertValue(l, ce.NewItems[i]));
            }
            else
            {
               l.Insert(insertionPoint, ConvertValue(l, ce.NewItems[i]));
            }
         }
      }

      /// <summary>
      /// Remove items action
      /// </summary>
      /// <param name="l"></param>
      /// <param name="ce"></param>
      private static void RemoveItems(IList l, NotifyCollectionChangedEventArgs ce)
      {
         var count = ce.OldItems.Count;
         for (var i = 0; i < count; i++)
         {
            l.RemoveAt(ce.OldStartingIndex);
         }
      }



      /// <summary>
      /// Receives events from the centralized event manager.
      /// </summary>
      /// <returns>
      /// true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager"/> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
      /// </returns>
      /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager"/> calling this method.</param><param name="sender">Object that originated the event.</param><param name="e">Event data.</param>
      public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         if (!(e is NotifyCollectionChangedEventArgs)) return false;
         ReceiveWeakEvent(sender, e as NotifyCollectionChangedEventArgs);
         return true;
      }


      /// <summary>
      /// Some of lists changed, serve it function.
      /// </summary>
      /// <param name="sender">listToListen where change occured</param>
      /// <param name="e">change description</param>
      /// <returns>always true</returns>
      public void ReceiveWeakEvent(object sender, NotifyCollectionChangedEventArgs e)
      {
         var sourceList = sender as IList;
         var destList = (sourceList == DependentList ? List : DependentList) as IList;

         switch (e.Action)
         {
            case NotifyCollectionChangedAction.Add:
               PerformListAction(AddItems, destList, e);
               break;
            case NotifyCollectionChangedAction.Remove:
               PerformListAction(RemoveItems, destList, e);
               break;
            case NotifyCollectionChangedAction.Move:
            case NotifyCollectionChangedAction.Replace:
               PerformListAction((l, ce) =>
               {
                  RemoveItems(l, ce);
                  AddItems(l, ce);
               }, destList, e);
               break;
            case NotifyCollectionChangedAction.Reset:
               PerformListAction((l, ce) =>
               {
                  l.Clear();
                  Debug.Assert(sourceList != null);
                  foreach (var o in sourceList)
                  {
                     l.Add(ConvertValue(l, o));
                  }
               }, destList, e);
               break;
         }
      }
   }
}