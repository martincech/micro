﻿using System;
using System.IO;

namespace Desktop.Wpf
{
   public static class Logger
   {
      /// <summary>
      /// File path
      /// </summary>
      public static string Path 
      {
         get { return _path; }
         set
         {
            _path = value;
            _fullPath = System.IO.Path.Combine(_path, Name);
         } 
      }

      private static string _path;
      private static string _fullPath;
      private const string Name = "log.txt";

      static Logger()
      {
         Path = @"c:\Logs";
         if (!Directory.Exists(Path))
         {
            Directory.CreateDirectory(Path);
         }
         Write("=== " + DateTime.Now + " ==========================", false);
      }

      /// <summary>
      /// Write string to log file.
      /// </summary>
      /// <param name="message">string to write</param>
      /// <param name="timeFlag">if true - add time to message</param>
      public static void Write(string message, bool timeFlag = true)
      {
         var file = new StreamWriter(_fullPath, true);
         var time = "";
         if (timeFlag)
         {
            time = DateTime.Now.ToString("hh:mm:ss:fff tt") + " - ";
         }
         file.WriteLine(time + message);
         file.Close();
      }
   }
}
