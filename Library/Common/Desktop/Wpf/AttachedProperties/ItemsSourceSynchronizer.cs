using System;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Desktop.Wpf.AttachedProperties
{
   /// <summary>
   /// Attached property for synchronizing ItemsSource property of <see cref="DependencyObject"/> with specific list.,
   /// </summary>
   public static class ItemsSourceSynchronizer
   {
      #region ItemsSourceProperty
      /// <summary>
      /// ItemsSourceProperty holds list to be synchronized with <see cref="ItemsControl.ItemsSource"/> property.
      /// </summary>
      public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.RegisterAttached(
         "ItemsSource", typeof(IList), typeof(ItemsSourceSynchronizer), new PropertyMetadata(null, OnItemsSourcePropertyChanged));

      /// <summary>
      /// Getter for <see cref="ItemsSourceProperty"/>
      /// </summary>
      /// <param name="dependencyObject"><see cref="DependencyObject"/> to which this property is attached to</param>
      /// <returns>Attached list.</returns>
      public static IList GetItemsSource(DependencyObject dependencyObject)
      {
         return (IList)dependencyObject.GetValue(ItemsSourceProperty);
      }

      /// <summary>
      /// Setter for <see cref="ItemsSourceProperty"/>
      /// </summary>
      /// <param name="dependencyObject"><see cref="DependencyObject"/> to be property attached to</param>
      /// <param name="value">Object to be attached</param>
      public static void SetItemsSource(DependencyObject dependencyObject, IList value)
      {
         var prop = dependencyObject.GetType().GetProperty("ItemsSource");
         if (prop == null)
         {
            throw new InvalidOperationException("Target object has no ItemsSource property to bind!");
         }
         if (!(value is INotifyCollectionChanged))
         {
            throw new ArgumentException("Source collection must implement INotifyCollectionChanged interface!");
         }
         dependencyObject.SetValue(ItemsSourceProperty, value);
      }

      private static void OnItemsSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
      {
         var list = e.NewValue as INotifyCollectionChanged;
         if (list == null)
         {
            return;
         }
         var s = CollectionsSynchronizerAttachedProperty.GetSynchronizer(d);
         if (s != null)
         {
            // release old synchronizer
            s.List = list;
         }
         else
         {
            // create new synchronizer object
            CollectionsSynchronizerAttachedProperty.SetSynchronizer(d, new CollectionsSynchronizer(GetDependencyObjectList(d), GetItemsSource(d) as INotifyCollectionChanged, GetItemsSourceConverter(d)));
         }
      }
      #endregion

      #region ItemsSourceConverterProperty
      /// <summary>
      /// ItemsSourceConverterProperty holds converter to be used for conversion.
      /// </summary>
      public static readonly DependencyProperty ItemsSourceConverterProperty = DependencyProperty.RegisterAttached(
         "ItemsSourceConverter", typeof(IValueConverter), typeof(ItemsSourceSynchronizer), new PropertyMetadata(null, OnItemsSourceConverterPropertyChanged));


      public static IValueConverter GetItemsSourceConverter(DependencyObject dependencyObject)
      {
         return (IValueConverter)dependencyObject.GetValue(ItemsSourceConverterProperty);
      }

      public static void SetItemsSourceConverter(DependencyObject dependencyObject, IValueConverter value)
      {
         var prop = dependencyObject.GetType().GetProperty("ItemsSource");
         if (prop == null)
         {
            throw new InvalidOperationException("Target object has no ItemsSource property to bind.");
         }
         dependencyObject.SetValue(ItemsSourceConverterProperty, value);
      }

      private static void OnItemsSourceConverterPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
      {
         var converter = e.NewValue as IValueConverter;
         if (converter == null)
         {
            return;
         }
         var s = CollectionsSynchronizerAttachedProperty.GetSynchronizer(d);
         if (s != null)
         {
            s.Converter = converter;
         }
      }
      #endregion

      private static INotifyCollectionChanged GetDependencyObjectList(DependencyObject synchroObject)
      {
         INotifyCollectionChanged dependencyList;
         if (synchroObject is ItemsControl)
         {
            Debug.Assert((synchroObject as ItemsControl).ItemsSource != null, string.Format("ItemsSource is not assigned on object of type {0}!", synchroObject.GetType().Name));
            dependencyList = (synchroObject as ItemsControl).ItemsSource as INotifyCollectionChanged;
         }
         else
         {
            var prop = synchroObject.GetType().GetProperty("ItemsSource");
            Debug.Assert(prop != null, "ItemsSource not found on attached object");
            dependencyList = prop.GetValue(synchroObject,null) as INotifyCollectionChanged;
         }

         Debug.Assert(dependencyList != null, "ItemsSource does not implemenet INotifyCollectionChanged interface!");
         return dependencyList;
      }
   }
}