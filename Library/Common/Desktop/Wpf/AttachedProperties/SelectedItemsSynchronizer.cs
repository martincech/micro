﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;

namespace Desktop.Wpf.AttachedProperties
{
   /// <summary>
   /// Attached property for for synchronizing SelectedItems property of <see cref="DependencyObject"/>s with specific list.
   /// </summary>
   public static class SelectedItemsSynchronizer
   {
      #region SelectedItemsProperty
      /// <summary>
      /// SelectedItemsProperty holds list to be synchronized with <see cref="MultiSelector"/> or <see cref="ListBox"/> SelectedItems property.
      /// </summary>
      public static readonly DependencyProperty SelectedItemsProperty = DependencyProperty.RegisterAttached(
      "SelectedItems", typeof(IList), typeof(SelectedItemsSynchronizer), new PropertyMetadata(null, OnSelectedItemsPropertyChanged));

      /// <summary>
      /// Getter for <see cref="SelectedItemsProperty"/>
      /// </summary>
      /// <param name="dependencyObject"><see cref="DependencyObject"/> to which this property is attached to</param>
      /// <returns>Attached list.</returns>
      public static IList GetSelectedItems(DependencyObject dependencyObject)
      {
         return (IList)dependencyObject.GetValue(SelectedItemsProperty);
      }

      /// <summary>
      /// Setter for <see cref="SelectedItemsProperty"/>
      /// </summary>
      /// <param name="dependencyObject"><see cref="DependencyObject"/> to be property attached to</param>
      /// <param name="value">Object to be attached</param>
      public static void SetSelectedItems(DependencyObject dependencyObject, IList value)
      {
         var prop = dependencyObject.GetType().GetProperty("SelectedItems");
         if (prop == null || prop.PropertyType != typeof(IList))
         {
            throw new InvalidOperationException("Target object has no SelectedItems property to bind!");
         }
         if (!(value is INotifyCollectionChanged))
         {
            throw new ArgumentException("Source collection must implement INotifyCollectionChanged interface!");
         }
         dependencyObject.SetValue(SelectedItemsProperty, value);
      }

      private static void OnSelectedItemsPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
      {
         var list = e.NewValue as INotifyCollectionChanged;
         if (list == null)
         {
            return;
         }
         var s = CollectionsSynchronizerAttachedProperty.GetSynchronizer(d);
         if (s != null)
         {
            // release old synchronizer
            s.List = list;
         }
         else {
            // create new synchronizer object
            CollectionsSynchronizerAttachedProperty.SetSynchronizer(d, new CollectionsSynchronizer(GetDependencyObjectList(d), GetSelectedItems(d) as INotifyCollectionChanged, GetSelectedItemsConverter(d)));
         }
      }
      #endregion

      #region SelectedItemsConverterProperty
      /// <summary>
      /// SelectedItemsConverterProperty holds converter to be used for conversion.
      /// </summary>
      public static readonly DependencyProperty SelectedItemsConverterProperty = DependencyProperty.RegisterAttached(
      "SelectedItemsConverter", typeof(IValueConverter), typeof(SelectedItemsSynchronizer), new PropertyMetadata(null, OnSelectedItemsConverterPropertyChanged));


      public static IValueConverter GetSelectedItemsConverter(DependencyObject dependencyObject)
      {
         return (IValueConverter)dependencyObject.GetValue(SelectedItemsConverterProperty);
      }

      public static void SetSelectedItemsConverter(DependencyObject dependencyObject, IValueConverter value)
      {
         var prop = dependencyObject.GetType().GetProperty("SelectedItems");
         if (prop == null || prop.PropertyType != typeof(IList))
         {
            throw new InvalidOperationException("Target object has no SelectedItems property to bind.");
         }
         dependencyObject.SetValue(SelectedItemsConverterProperty, value);
      }

      private static void OnSelectedItemsConverterPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
      {
         var converter = e.NewValue as IValueConverter;
         if (converter == null)
         {
            return;
         }
         var s = CollectionsSynchronizerAttachedProperty.GetSynchronizer(d);
         if( s != null){
            s.Converter = converter;
         }
      }
      #endregion

      private static INotifyCollectionChanged GetDependencyObjectList(DependencyObject synchroObject)
      {
         INotifyCollectionChanged dependencyList;
         if (synchroObject is MultiSelector)
         {
            Debug.Assert((synchroObject as MultiSelector).SelectedItems != null, string.Format("SelectedItems is not assigned on object of type {0}!", synchroObject.GetType().Name));
            dependencyList = (synchroObject as MultiSelector).SelectedItems as INotifyCollectionChanged;
         }
         else if (synchroObject is ListBox)
         {
            Debug.Assert((synchroObject as ListBox).SelectedItems != null, string.Format("SelectedItems is not assigned on object of type {0}!", synchroObject.GetType().Name));
            dependencyList = (synchroObject as ListBox).SelectedItems as INotifyCollectionChanged;
         }
         else
         {
            var prop = synchroObject.GetType().GetProperty("SelectedItems");
            Debug.Assert(prop != null, "SelectedItems not found on attached object");
            dependencyList = prop.GetValue(synchroObject,null) as INotifyCollectionChanged;
         }

         Debug.Assert(dependencyList != null, "SelectedItems not found on attached object or it doesn't implemenet INotifyCollectionChanged interface!");
         return dependencyList;
      }
   }

   #region Synchronizer object

   #endregion
}
