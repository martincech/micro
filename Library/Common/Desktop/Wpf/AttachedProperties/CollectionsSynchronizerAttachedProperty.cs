using System.Windows;

namespace Desktop.Wpf.AttachedProperties
{
   /// <summary>
   /// Internal class for attaching <see cref="CollectionsSynchronizer"/> to <see cref="DependencyObject"/>.
   /// </summary>
   internal static class CollectionsSynchronizerAttachedProperty
   {
      #region Synchronizer private property
      /// <summary>
      /// Synchronizer property - this propety hold synchronization object
      /// </summary>
      public static readonly DependencyProperty SynchronizerProperty = DependencyProperty.RegisterAttached(
         "Synchronizer", typeof(CollectionsSynchronizer), typeof(SelectedItemsSynchronizer), new PropertyMetadata(null));

      public static CollectionsSynchronizer GetSynchronizer(DependencyObject dependencyObject)
      {
         return (CollectionsSynchronizer)dependencyObject.GetValue(SynchronizerProperty);
      }

      public static void SetSynchronizer(DependencyObject dependencyObject, CollectionsSynchronizer value)
      {
         dependencyObject.SetValue(SynchronizerProperty, value);
      }
      #endregion
   }
}