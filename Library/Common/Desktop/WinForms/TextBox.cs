﻿using System;
using System.Threading;
using GUI;

namespace Desktop.WinForms
{
   public class TextBox : System.Windows.Forms.TextBox, ITextBox
   {
      #region Implementation of ITextBox

      public override string Text
      {
         get
         {
            if (!InvokeRequired) return base.Text;

            var s = "";
            var waitEvent = new ManualResetEvent(false);
            Invoke(new Action(() =>
            {
               s = base.Text;
               waitEvent.Set();
            }));
            waitEvent.WaitOne();
            return s;
         }
         set
         {
            if (InvokeRequired)
            {
               Invoke(new Action<string>(s => base.Text = s), new object[] {value});
            }
            else
            {
               base.Text = value;
            }
         }
      }

      public new void AppendText(string text)
      {
         if (InvokeRequired)
         {
            Invoke(new Action<string>(AppendText), new object[] {text});
         }
         else
         {
            base.AppendText(text);
         }
      }

      #endregion
   }
}
