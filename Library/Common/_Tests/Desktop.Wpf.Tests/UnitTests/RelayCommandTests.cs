﻿using System;
using Desktop.Wpf.Applications;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Desktop.Wpf.Tests.UnitTests
{
   [TestClass]
   public class RelayCommandTests
   {
      private RelayCommand<object> commandClassType;
      private RelayCommand<DateTime> commandValueType;
      private RelayCommand commandNonParam;

      private object classValue;
      private DateTime valueValue;
      private bool nonParamValue;

      [TestInitialize]
      public void Init()
      {
         commandClassType = new RelayCommand<object>(
            d => classValue = d,
            d => d != null);
         commandValueType = new RelayCommand<DateTime>(
            d => valueValue = d,
            d=> d != default(DateTime));
         commandNonParam = new RelayCommand(
            () => { },
            () => nonParamValue);

      }

      [TestMethod]
      public void CanExecuteIsFalse()
      {
         Assert.IsFalse(commandClassType.CanExecute(null));
         Assert.IsFalse(commandValueType.CanExecute(null));
         Assert.IsFalse(commandNonParam.CanExecute(null));
      }

      [TestMethod]
      public void CanExecuteIsTrue()
      {
         Assert.IsTrue(commandClassType.CanExecute(new object()));
         Assert.IsTrue(commandValueType.CanExecute(DateTime.Now));
         nonParamValue = true;
         Assert.IsTrue(commandNonParam.CanExecute(null));
      }

      [TestMethod]
      [ExpectedException(typeof (InvalidOperationException))]
      public void Execute_Class_ThrowsException_WhenCanExecuteFalse()
      {
         commandClassType.Execute(null);
      }
      [TestMethod]
      [ExpectedException(typeof(InvalidOperationException))]
      public void ExecuteValue_ThrowsException_WhenCanExecuteFalse()
      {
         commandValueType.Execute(null);
      }

      [TestMethod]
      [ExpectedException(typeof(InvalidOperationException))]
      public void ExecuteNonParams_ThrowsException_WhenCanExecuteFalse()
      {
         commandNonParam.Execute(null);
      }

      [TestMethod]
      public void ExecuteExecuted()
      {
         var o = new object();
         commandClassType.Execute(o);
         Assert.AreSame(o, classValue);

         var d = DateTime.Now;
         commandValueType.Execute(d);
         Assert.AreEqual(d, valueValue);

         nonParamValue = true;
         commandNonParam.Execute(null);
         Assert.AreEqual(true, nonParamValue);
      }

      [TestMethod]
      public void CanExecuteChangedRaised()
      {
         var raised1 = false;
         commandClassType.CanExecuteChanged += (o, e) =>
         {
            raised1 = true;
         };
         var raised2 = false;
         commandValueType.CanExecuteChanged += (o, e) =>
         {
            raised2 = true;
         };
         var raised3 = false;
         commandNonParam.CanExecuteChanged += (o, e) =>
         {
            raised3 = true;
         };

         commandClassType.RaiseCanExecuteChanged();
         commandValueType.RaiseCanExecuteChanged();
         commandNonParam.RaiseCanExecuteChanged();
         Assert.IsTrue(raised1, "Can execute changed never raised");
         Assert.IsTrue(raised2, "Can execute changed never raised");
         Assert.IsTrue(raised3, "Can execute changed never raised");
      }
   }
}
