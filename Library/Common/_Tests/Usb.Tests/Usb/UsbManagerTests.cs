﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Usb.Tests.Usb
{
   [TestClass]
   public class UsbManagerTests
   {
      

      [TestMethod]
      public void DeviceConnectedTest()
      {
         var connectedEvent = new ManualResetEvent(false);
         var manager = UsbDeviceManager.Instance;
         manager.DeviceConnected += (sender, device) => connectedEvent.Set();
         Console.WriteLine("Waiting 20 s for device connection!");
         Assert.IsTrue(connectedEvent.WaitOne(20 * 1000), "Connection not occured!");
         connectedEvent.Reset();
         Console.WriteLine("Waiting another 20 s for device connection!");
         Assert.IsTrue(connectedEvent.WaitOne(20 * 1000), "Connection not occured!");
      }
   }
}
