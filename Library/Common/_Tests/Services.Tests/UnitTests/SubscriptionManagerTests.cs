﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.PublishSubscribe;

namespace Services.Tests.UnitTests
{
   [TestClass]
   public class SubscriptionManagerTests
   {
      private SubscribtionClient subscribtionClient;
      private SubscribtionPersistentClient persistentClient;
      private const string DUMMY_ADDRESS = "net.pipe://callback";

      [TestInitialize]
      public void Init()
      {
         Subscribtion.StartService();
         subscribtionClient = new SubscribtionClient(
            new InstanceContext(new EventsCallback()),
            Subscribtion.UnsecureBinding, new EndpointAddress(Subscribtion.ENDPOINT));
         CreatePersistentClient();
      }

      private void CreatePersistentClient()
      {
         persistentClient = new SubscribtionPersistentClient(
            Subscribtion.UnsecureBinding, new EndpointAddress(Subscribtion.ENDPOINT_PERSISTENT));
      }

      [TestCleanup]
      public void Clean()
      {
         subscribtionClient.Close();
         persistentClient.Close();
         Subscribtion.StopService();
      }

      [TestMethod]
      public void SubscriptionManager_Created()
      {
         Assert.IsNotNull(Subscribtion.Host);
      }


      [TestMethod]
      [ExpectedException(typeof(FaultException))]
      public void Subscribe_ThrowException_WhenInvalidAddressSpecified()
      {
         Assert.IsNotNull(typeof(ITestServiceEvents).GetMethods().First(), "No methods in interface ITestServiceEvents!");
         persistentClient.Subscribe("address", typeof (ITestServiceEvents).GetMethods().First().Name);
      }

      [TestMethod]
      [ExpectedException(typeof(FaultException))]
      public void Subscribe_ThrowException_WhenUnsuportedAddressSpecified()
      {
         Assert.IsNotNull(typeof(ITestServiceEvents).GetMethods().First(), "No methods in interface ITestServiceEvents!");
         persistentClient.Subscribe("ftp://address", typeof(ITestServiceEvents).GetMethods().First().Name);
      }

      [TestMethod]
      [ExpectedException(typeof(FaultException))]
      public void Subscribe_ThrowException_WhenInvalidOperationSpecified()
      {
         const string name = "StupidName";
         var methodNames = (typeof (ITestServiceEvents).GetMethods().Select(info => info.Name)).ToList();
         CollectionAssert.DoesNotContain(methodNames, name);

         persistentClient.Subscribe(DUMMY_ADDRESS, name);
      }

      [TestMethod]
      public void Subscribe_And_UnSubscribted_Persistently()
      {
         Assert.IsNotNull(typeof(ITestServiceEvents).GetMethods().First(), "No methods in interface ITestServiceEvents!");
         var methodName = typeof (ITestServiceEvents).GetMethods().First().Name;
         persistentClient.Subscribe(DUMMY_ADDRESS, methodName);
         using (var dataContext = new PublishSubscribeDataContext())
         {
            var query = from subscriber in dataContext.Subscribers
            where subscriber.Address == DUMMY_ADDRESS &&
                  subscriber.Operation == methodName
            select subscriber;
            Assert.IsTrue(query.Any(), "Method not registered!");
            persistentClient.Close();
            Subscribtion.StopService();
            Subscribtion.StartService();
            CreatePersistentClient();
            Assert.IsTrue(query.Any(), "Method is not saved permanently!");
            persistentClient.Unsubscribe(DUMMY_ADDRESS, methodName);
            Assert.IsFalse(query.Any(), "Method still saved even though unsubscribe called!");
         }
      }

      [TestMethod]
      public void Subscribe_And_UnSubscribted_Transiently()
      {
         var methodName = typeof(ITestServiceEvents).GetMethods().First().Name;
         var transientStore = Subscribtion.MTransientStore;
         List<ITestServiceEvents> clientList;
         Assert.IsNotNull(transientStore);
         Assert.IsTrue(transientStore.ContainsKey(methodName));
         transientStore.TryGetValue(methodName, out clientList);
         Assert.IsNotNull(clientList);
         Assert.IsFalse(clientList.Any());
         
         subscribtionClient.Subscribe(methodName);
         transientStore.TryGetValue(methodName, out clientList);
         Assert.IsNotNull(clientList);
         Assert.IsTrue(clientList.Any(), "Client not subscribted!");

         subscribtionClient.Unsubscribe(methodName);
         transientStore.TryGetValue(methodName, out clientList);
         Assert.IsNotNull(clientList);
         Assert.IsFalse(clientList.Any(), "Client not unsubscribted!");
      }

      [TestMethod]
      public void InterfaceType_MethodsInherited()
      {
         var inheritedSubscribtion = new InheritedSubscribtion();
         var transientStore = InheritedSubscribtion.MTransientStore;
         foreach (var methodInfo in typeof(ITestServiceEvents).GetMethods())
         {
            Assert.IsTrue(transientStore.ContainsKey(methodInfo.Name));
         }
      }

      [TestMethod]
      public void GetSubscribersFromAddress_Returs_Subscribtion()
      {
         var methodName = typeof(ITestServiceEvents).GetMethods().First().Name;
         var methodName2 = typeof(ITestServiceEvents).GetMethods().Last().Name;
         persistentClient.Subscribe(DUMMY_ADDRESS, methodName);
         persistentClient.Subscribe(DUMMY_ADDRESS, methodName2);

         Assert.IsTrue(Subscribtion.GetAllSubscribersFromAddress(DUMMY_ADDRESS).Any(subscription => subscription.Address == DUMMY_ADDRESS && subscription.Operation == methodName));
         Assert.IsTrue(Subscribtion.GetAllSubscribersFromAddress(DUMMY_ADDRESS).Any(subscription => subscription.Address == DUMMY_ADDRESS && subscription.Operation == methodName2));
         Assert.IsTrue(Subscribtion.GetAllSubscribersFromAddress(DUMMY_ADDRESS).Count() == 2);
         persistentClient.Unsubscribe(DUMMY_ADDRESS, methodName);
         persistentClient.Unsubscribe(DUMMY_ADDRESS, methodName2);
      }


      [TestMethod]
      public void GetAllSubscribers_Returns_Address()
      {
         var methodName = typeof(ITestServiceEvents).GetMethods().First().Name;
         persistentClient.Subscribe(DUMMY_ADDRESS, methodName);
         Assert.IsTrue(Subscribtion.GetSubscribers().Any(subscription => subscription.Address == DUMMY_ADDRESS && subscription.Operation == methodName));
         Assert.IsTrue(Subscribtion.GetSubscribers().Count() == 1);
         persistentClient.Unsubscribe(DUMMY_ADDRESS, methodName);
      }

      [TestMethod]
      public void GetSubscribersToOperation_Returns_Address()
      {
         var methodName = typeof(ITestServiceEvents).GetMethods().First().Name;
         persistentClient.Subscribe(DUMMY_ADDRESS, methodName);

         Assert.IsTrue(Subscribtion.GetSubscribersToOperation(methodName).Any(address => address == DUMMY_ADDRESS));
         Assert.IsTrue(Subscribtion.GetSubscribersToOperation(methodName).Count() == 1);
         persistentClient.Unsubscribe(DUMMY_ADDRESS, methodName);
      }
   }

   public class InheritedSubscribtion : SubscriptionManager<ITestServiceEventsInherited>,
      ITestInheritedContract
   {

   }
}
