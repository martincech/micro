﻿using System;
using System.ServiceModel;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.PublishSubscribe;
using Services.QueuedProcessing;

namespace Services.Tests.FunctionalTests
{
   [TestClass]
   public class PublishSubscribeTests
   {
      private PublishingClient publishingClient;
      private SubscribtionClient subscribtionClient;
      private SubscribtionPersistentClient persistentClient;
      private EventsCallbackService callbackService;
      private EventsCallback callback;
      private static bool _raised1;
      private static bool _raised2;
      private static bool _raised3;
      private static EventHandler _event1Delegate;
      private static EventHandler _event2Delegate;
      private static EventHandler _event3Delegate;
      private static ManualResetEvent _event1Raised;
      private static ManualResetEvent _event2Raised;
      private static ManualResetEvent _event3Raised;
      private static object _lockObj;

      [ClassCleanup]
      public static void ClassClean()
      {
         QueuedServiceHelper.DeleteQueue(EventsCallbackService.ENDPOINT);
      }

      [TestInitialize]
      public void Init()
      {
         using (var db = new PublishSubscribeDataContext())
         {
            db.Database.CreateIfNotExists();
         }
         _lockObj = new object();
         PublishingService.StartService();
         Subscribtion.StartService();
         callbackService = EventsCallbackService.StartService();
         publishingClient = new PublishingClient(PublishingService.NetPipeBinding, new EndpointAddress(PublishingService.ENDPOINT_NET_PIPE));

         callback = new EventsCallback();
         subscribtionClient = new SubscribtionClient(
            new InstanceContext(callback),
            Subscribtion.UnsecureBinding, new EndpointAddress(Subscribtion.ENDPOINT));
         CreatePersistentClient();
         _event1Raised = new ManualResetEvent(false);
         _event2Raised = new ManualResetEvent(false);
         _event3Raised = new ManualResetEvent(false);

         _event1Delegate = delegate
         {
            lock (_lockObj)
            {
               _event1Raised.Set();
               _raised1 = true;
            }
         };
         _event2Delegate = delegate
         {
            lock (_lockObj)
            {
               _event2Raised.Set();
               _raised2 = true;
            }
         };
         _event3Delegate = delegate
         {
            lock (_lockObj)
            {
               _event3Raised.Set();
               _raised3 = true;
            }
         };
         callback.Event1Invoked += _event1Delegate;
         callback.Event2Invoked += _event2Delegate;
         callback.Event3Invoked += _event3Delegate;
         callbackService.Event1Invoked += _event1Delegate;
         callbackService.Event2Invoked += _event2Delegate;
         callbackService.Event3Invoked += _event3Delegate;
         ClearRaisedFlags();
      }

      [TestCleanup]
      public void Clean()
      {
         publishingClient.Close();
         subscribtionClient.Close();
         PublishingService.StopService();
         Subscribtion.StopService();
         EventsCallbackService.StopService();
         callback.Event1Invoked -= _event1Delegate;
         callback.Event2Invoked -= _event2Delegate;
         callback.Event3Invoked -= _event3Delegate;
         callbackService.Event1Invoked -= _event1Delegate;
         callbackService.Event2Invoked -= _event2Delegate;
         callbackService.Event3Invoked -= _event3Delegate;
         using (var db = new PublishSubscribeDataContext())
         {
            db.Database.Delete();
         }
      }


      [TestMethod]
      public void Transient_EventInvoked()
      {
         subscribtionClient.Subscribe("OnEvent1");
         publishingClient.OnEvent1();   
         WaitAndAssertEvents(true);
         subscribtionClient.Unsubscribe("OnEvent1");
      }

      [TestMethod]
      public void Transient_EventInvoked_OnlySubscribtedArrived()
      {
         subscribtionClient.Subscribe("OnEvent1");
         subscribtionClient.Subscribe("OnEvent2");
         publishingClient.OnEvent1();
         publishingClient.OnEvent2(1);
         WaitAndAssertEvents(true, true);
         
         ClearRaisedFlags();
         subscribtionClient.Unsubscribe("OnEvent1");
         subscribtionClient.Unsubscribe("OnEvent2");
         publishingClient.OnEvent1();
         publishingClient.OnEvent2(1);
         WaitAndAssertEvents();
      }

      [TestMethod]
      public void Persistent_EventInvoked()
      {
         const string address = EventsCallbackService.ENDPOINT;
         persistentClient.Subscribe(address, "OnEvent1");
         publishingClient.OnEvent1();
         WaitAndAssertEvents(true);
         persistentClient.Unsubscribe(address, "OnEvent1");
      }

      [TestMethod]
      public void Persistent_Subscribtion_Survived_Client_Close()
      {
         const string address = EventsCallbackService.ENDPOINT;
         persistentClient.Subscribe(address, "OnEvent1");
         EventsCallbackService.StopService();
         
         // it doesn't arive because it is closed
         publishingClient.OnEvent1();
         WaitAndAssertEvents();
         
         ClearRaisedFlags();
         EventsCallbackService.StartService();
         // now it should arive because client exist again and subscribtion is stil valid
         publishingClient.OnEvent1();
         WaitAndAssertEvents(true);
         persistentClient.Unsubscribe(address, "OnEvent1");
      }

      [TestMethod]
      public void Persistent_Subscribtion_Arrived_On_Client_Restart()
      {
         const string address = EventsCallbackService.ENDPOINT;
         persistentClient.Subscribe(address, "OnEvent1");
         EventsCallbackService.StopService();
         // it doesn't arive because it is closed
         publishingClient.OnEvent1();
         WaitAndAssertEvents();

         ClearRaisedFlags();
         EventsCallbackService.StartService();
         // now it should arive because client exist again and subscribtion is stil valid
         WaitAndAssertEvents(true);
         persistentClient.Unsubscribe(address, "OnEvent1");
      }



      #region Helper methods

      private static void WaitAndAssertEvents(bool ev1Raised = false, bool ev2Raised = false, bool ev3Raised = false)
      {
         _event1Raised.WaitOne(TimeSpan.FromSeconds(1));
         _event2Raised.WaitOne(TimeSpan.FromSeconds(1));
         _event3Raised.WaitOne(TimeSpan.FromSeconds(1));
         Assert.AreEqual(ev1Raised, _raised1, "Event 1 problem");
         Assert.AreEqual(ev2Raised, _raised2, "Event 2 problem");
         Assert.AreEqual(ev3Raised, _raised3, "Event 3 problem");
      }

      private static void ClearRaisedFlags()
      {
         lock (_lockObj)
         {
            _event1Raised.Reset();
            _raised1 = false;
            _event2Raised.Reset();
            _raised2 = false;
            _event3Raised.Reset();
            _raised3 = false;
         }
      }

      private void CreatePersistentClient()
      {
         persistentClient = new SubscribtionPersistentClient(
            Subscribtion.UnsecureBinding, new EndpointAddress(Subscribtion.ENDPOINT_PERSISTENT));
      }

      #endregion

   }
}
