﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.PublishSubscribe;
using Services.PublishSubscribe.Contracts;

namespace Services.Tests
{

   [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
   internal class Subscribtion
      : SubscriptionManager<ITestServiceEvents>, 
      IPersistentSubscriptionContract,
      ITestContractContract
   {
      #region Private field and constants

      private const int OPEN_CLOSE_TIMEOUT = 2;
      private const int RECEIVE_SEND_TIMEOUT = 15;

// ReSharper disable InconsistentNaming
      private static readonly Binding _unsecureBinding = new NetNamedPipeBinding
// ReSharper restore InconsistentNaming
      {
         Security = {Mode = NetNamedPipeSecurityMode.None},
         CloseTimeout = TimeSpan.FromSeconds(OPEN_CLOSE_TIMEOUT),
         OpenTimeout = TimeSpan.FromSeconds(OPEN_CLOSE_TIMEOUT),
         ReceiveTimeout = TimeSpan.FromSeconds(RECEIVE_SEND_TIMEOUT),
         SendTimeout = TimeSpan.FromSeconds(RECEIVE_SEND_TIMEOUT),
         TransactionFlow = true
      };

      #endregion

      #region Public stuff

      public const string ENDPOINT = "net.pipe://localhost/ITestContractContract";
      public const string ENDPOINT_PERSISTENT = "net.pipe://localhost/IPersistentSubscriptionContract";
      public static Binding UnsecureBinding
      {
         get { return _unsecureBinding; }
      }

      public static ServiceHost Host = null;

      #endregion


      private Subscribtion()
      {
      }

      public static void StartService()
      {
         if (Host == null)
         {
            try
            {
               Host = new ServiceHost(typeof (Subscribtion));
               Host.AddServiceEndpoint(typeof(ITestContractContract), UnsecureBinding, ENDPOINT);
               Host.AddServiceEndpoint(typeof(IPersistentSubscriptionContract), UnsecureBinding, ENDPOINT_PERSISTENT);
               Host.Open();
            }
            catch (Exception ex)
            {
               Assert.IsTrue(false, string.Format("Exception when creating server: {0}", ex.Message));
            }
         }
         Assert.IsTrue(Host.State == CommunicationState.Opened);
      }

      public static void StopService()
      {
         if (Host == null)
         {
            return;
         }
         var ev = new ManualResetEvent(false);
         var handler = new EventHandler(delegate { ev.Set(); });
         Host.Closed += handler;
         Host.Close();
         ev.WaitOne();
         Host.Closed -= handler;
         Assert.IsTrue(Host.State == CommunicationState.Closed);
         Host = null;
      }
   }
}