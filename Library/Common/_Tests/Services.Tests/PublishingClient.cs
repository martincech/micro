﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Services.Tests
{
   internal class PublishingClient : ClientBase<ITestServiceEvents>, ITestServiceEvents
   {

      public PublishingClient()
      {
      }

      public PublishingClient( string endpointConfigurationName) :
         base( endpointConfigurationName)
      {
      }

      public PublishingClient( string endpointConfigurationName, string remoteAddress) :
         base( endpointConfigurationName, remoteAddress)
      {
      }

      public PublishingClient( string endpointConfigurationName, EndpointAddress remoteAddress) :
         base( endpointConfigurationName, remoteAddress)
      {
      }

      public PublishingClient( Binding binding, EndpointAddress remoteAddress) :
         base( binding, remoteAddress)
      {
      }

      #region Implementation of ITestServiceEvents

      public void OnEvent1()
      {
         Channel.OnEvent1();
      }

      public void OnEvent2(int number)
      {
         Channel.OnEvent2(number);
      }

      public void OnEvent3(int number, string text)
      {
         Channel.OnEvent3(number,text);
      }

      #endregion

      public new void Close()
      {
         if (State == CommunicationState.Opened)
         {
            base.Close();
         }
         else
         {
            Abort();
         }
         Assert.IsTrue(State == CommunicationState.Closed);
      }
   }
}
