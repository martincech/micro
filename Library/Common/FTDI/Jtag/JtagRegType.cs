namespace Ftdi.Jtag
{
   /// <summary>
   /// The register type of JTAG device
   ///  
   ///  This represent the register type of JTAG inteface. There is two type of 
   ///  register. One is Data the other is Instruction. 
   /// </summary>
   public enum JtagRegType
   {
      /// <summary>
      /// state transition is going through Data path
      /// </summary>
      Data = 0,
      
      /// <summary>
      /// state transition is going through Instruciton path
      /// </summary>
      Instruction = 1
   }
}