﻿namespace Ftdi
{
   /// <summary>
   /// the Logic Level of physical pin
   /// </summary>
   public enum LogicLevel
   {
      /// <summary>
      /// Logic Level Low
      /// </summary>
      L = 0,
      /// <summary>
      /// Logic Level High
      /// </summary>
      H = 1,
      /// <summary>
      /// Logic Level Undef
      /// </summary>
      Undef = -1 
   };
}