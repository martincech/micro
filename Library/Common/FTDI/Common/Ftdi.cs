﻿//******************************************************************************
//
//   Ftdi.cs     FTD2XX_NET wrapper - some usefull functions added
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;
using FTD2XX_NET;

namespace Ftdi
{
   public class Ftdi : FTDI
   {
      #region Private fields

      private static int _maxid;

      #endregion

      #region Public interface

      //************************************************************************
      // Open - arbitrary device type
      //************************************************************************

      public FT_STATUS Open(int identifier)
         // Open device by <Identifier>
      {
         return Open(identifier, FT_DEVICE.FT_DEVICE_UNKNOWN);
      }

      //************************************************************************
      // Open - fixed device type
      //************************************************************************

      public FT_STATUS Open(int identifier, FT_DEVICE deviceType)
         // Open device by <Identifier>
      {
         //lock(Locker) {
         if (IsOpen)
         {
            Close(); // close previous
         }

         // Check text data first (inaccessible after open) :
         uint ftdiDeviceCount = 0;

         var status = GetNumberOfDevices(ref ftdiDeviceCount);

         // Check status
         if (status != FT_STATUS.FT_OK)
         {
            return (status);
         }

         if (ftdiDeviceCount == 0)
         {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND);
         }

         status = OpenByIndex((uint) identifier);

         if (status != FT_STATUS.FT_OK)
         {
            return (status);
         }

         // Get device type
         var openedDeviceType = FT_DEVICE.FT_DEVICE_UNKNOWN;

         GetDeviceType(ref openedDeviceType);

         // If neccessary, check device type
         if (deviceType != FT_DEVICE.FT_DEVICE_UNKNOWN)
         {
            if (openedDeviceType != deviceType)
            {
               Close();
               return (FT_STATUS.FT_DEVICE_NOT_FOUND);
            }
         }

         return FT_STATUS.FT_OK;
         //}
      } // Open

      //************************************************************************
      // Locate - arbitrary device type
      //************************************************************************

      public FT_STATUS Locate(string name, ref int identifier)
         // Find device by <Name>, returns <Identifier>
      {
         return Locate(name, ref identifier, FT_DEVICE.FT_DEVICE_UNKNOWN);
      }

      //************************************************************************
      // Locate - arbitrary device type
      //************************************************************************

      public FT_STATUS Locate(string name, string serialNumber, ref int identifier)
         // Find device by <Name>, returns <Identifier>
      {
         return Locate(name, serialNumber, ref identifier, FT_DEVICE.FT_DEVICE_UNKNOWN);
      }

      //************************************************************************
      // Locate - fixed device type
      //************************************************************************

      public FT_STATUS Locate(string name, ref int identifier, FT_DEVICE deviceType)
         // Find device by <Name>, returns <Identifier>
      {
         //lock(Locker) {

         // Count of FTDI devices :
         uint numDevices = 0;

         var status = GetNumberOfDevices(ref numDevices);

         if (status != FT_STATUS.FT_OK)
         {
            return (status);
         }

         if (numDevices == 0)
         {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND); // no devices
         }

         // Allocate storage for device info list
         var ftdiDeviceList = new FT_DEVICE_INFO_NODE[numDevices + 5];

         status = GetDeviceList(ftdiDeviceList);

         // Populate our device list
         if (status != FT_STATUS.FT_OK)
         {
            return (status);
         }

         // Search by device name
         var count = 0;
         var usbIndex = 0;
         var index = -1;

         foreach (var device in ftdiDeviceList)
         {
            index++;

            if (device == null)
            {
               continue;
            }

            Debug.WriteLine(device.LocId.ToString(CultureInfo.InvariantCulture));
            Debug.WriteLine("\r\n");

            if (device.Description.ToString(CultureInfo.InvariantCulture) != name)
            {
               continue;
            }

            // If neccessary, check device type
            if (deviceType != FT_DEVICE.FT_DEVICE_UNKNOWN)
            {
               if (device.Type != deviceType)
               {
                  continue;
               }
            }

            usbIndex = index;
            count++;
         }

         if (count == 0)
         {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND); // no device
         }

         if (count > 1)
         {
            return (FT_STATUS.FT_OTHER_ERROR); // more devices
         }

         identifier = usbIndex; // one device only

         return (FT_STATUS.FT_OK);
         //}
      } // Locate

      //************************************************************************
      // Locate - fixed device type
      //************************************************************************

      public FT_STATUS Locate(string name, string serialNumber, ref int identifier, FT_DEVICE deviceType)
         // Find device by <Name>, returns <Identifier>
      {
         //lock(Locker) {

         // Count of FTDI devices :
         uint numDevices = 0;

         var status = GetNumberOfDevices(ref numDevices);

         if (status != FT_STATUS.FT_OK)
         {
            return (status);
         }

         if (numDevices == 0)
         {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND); // no devices
         }

         // Allocate storage for device info list
         var ftdiDeviceList = new FT_DEVICE_INFO_NODE[numDevices + 5];

         status = GetDeviceList(ftdiDeviceList);

         // Populate our device list
         if (status != FT_STATUS.FT_OK)
         {
            return (status);
         }

         // Search by device name
         var count = 0;
         var usbIndex = 0;
         var index = -1;

         foreach (var device in ftdiDeviceList)
         {
            index++;

            if (device == null)
            {
               continue;
            }

            if (device.Description != name && device.SerialNumber != serialNumber)
            {
               continue;
            }

            // If neccessary, check device type
            if (deviceType != FT_DEVICE.FT_DEVICE_UNKNOWN)
            {
               if (device.Type != deviceType)
               {
                  continue;
               }
            }

            usbIndex = index;
            count++;
         }

         if (count == 0)
         {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND); // no device
         }

         if (count > 1)
         {
            return (FT_STATUS.FT_OTHER_ERROR); // more devices
         }

         identifier = usbIndex; // one device only

         return (FT_STATUS.FT_OK);
         //}
      } // Locate

      //************************************************************************
      // Locate - fixed device type
      //************************************************************************

      public FT_STATUS Locate(uint locId, ref int identifier, FT_DEVICE deviceType)
         // Find device by <Name>, returns <Identifier>
      {
         var id = _maxid++;
         //lock(Locker) {
         Debug.WriteLine("Start " + id.ToString(CultureInfo.InvariantCulture));
         // Count of FTDI devices :
         uint numDevices = 0;

         var status = GetNumberOfDevices(ref numDevices);
         Debug.WriteLine("Number " + id.ToString(CultureInfo.InvariantCulture));
         if (status != FT_STATUS.FT_OK)
         {
            return (status);
         }

         if (numDevices == 0)
         {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND); // no devices
         }

         // Allocate storage for device info list
         var ftdiDeviceList = new FT_DEVICE_INFO_NODE[numDevices + 5];
         Debug.WriteLine("List " + id.ToString(CultureInfo.InvariantCulture));
         status = GetDeviceList(ftdiDeviceList);

         // Populate our device list
         if (status != FT_STATUS.FT_OK)
         {
            return (status);
         }

         // Search by device name
         var count = 0;
         var usbIndex = 0;
         var index = -1;

         foreach (var device in ftdiDeviceList)
         {
            index++;

            if (device == null)
            {
               continue;
            }

            if (device.LocId != locId)
            {
               continue;
            }

            usbIndex = index;
            count++;
         }

         if (count == 0)
         {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND); // no device
         }

         if (count > 1)
         {
            return (FT_STATUS.FT_OTHER_ERROR); // more devices
         }

         identifier = usbIndex; // one device only

         return (FT_STATUS.FT_OK);
         //}
      } // Locate

      //************************************************************************
      // Access - arbitrary device type
      //************************************************************************

      public FT_STATUS Access(int identifier)
         // Check device access by <Identifier>
      {
         return Access(identifier, FT_DEVICE.FT_DEVICE_UNKNOWN);
      }

      //************************************************************************
      // Access - fixed device type
      //************************************************************************

      public FT_STATUS Access(int identifier, FT_DEVICE deviceType)
         // Check device access by <Identifier>
      {
         //lock(Locker) {
         var status = OpenByIndex((uint) identifier);

         if (status != FT_STATUS.FT_OK)
         {
            return (status);
         }

         // Get device type
         var openedDeviceType = FT_DEVICE.FT_DEVICE_UNKNOWN;

         GetDeviceType(ref openedDeviceType);

         // If neccessary, check device type
         if (deviceType != FT_DEVICE.FT_DEVICE_UNKNOWN)
         {
            if (openedDeviceType != deviceType)
            {
               Close();
               return (FT_STATUS.FT_DEVICE_NOT_FOUND);
            }
         }

         return Close();
         //}
      } // Access


      /// <summary>
      /// Write data stream int FT2232, stati method using handle
      /// 
      /// A write command. This is very generic command. After reset, this command 
      /// work as write command from UART. To make it in the MPSSE mode, you should call
      /// FtcSpi.Initialize() function.
      /// </summary>
      /// <param name="ftHandle">  handle given by Open or OpenEx</param>
      /// <param name="pWriteDataBuffer">  buffer which contains data to be written</param>
      /// <param name="dwNumBytesToWrite"> the number of the bytes which should be writen.</param>
      /// <param name="lpdwNumBytesWritten">  the data length which are writen [BYTE]</param>
      [DllImport("FTD2XX.dll", EntryPoint = "FT_Write")]
      public static extern FT_STATUS
         Write(
         int ftHandle,
         byte[] pWriteDataBuffer,
         int dwNumBytesToWrite,
         out int lpdwNumBytesWritten
         );


      /// <summary>
      /// Read data stream from FT2232, static method using handle
      /// 
      /// A read command. This is very generic command. After reset, this command 
      /// work as read command from UART. To make it in the MPSSE mode, you should call
      /// FtcSpi.Initialize() function.
      /// </summary>
      /// <param name="ftHandle">  handle given by Open or OpenEx</param>
      /// <param name="pReadDataBuffer">  buffer which contains data to be read</param>
      /// <param name="dwNumBytesToRead"> the number of the bytes which should be read.</param>
      /// <param name="lpdwNumBytesReturned">  the data length which are read [BYTE]</param>
      [DllImport("FTD2XX.dll", EntryPoint = "FT_Read")]
      public static extern FT_STATUS
         Read(
         int ftHandle,
         byte[] pReadDataBuffer,
         int dwNumBytesToRead,
         out int lpdwNumBytesReturned
         );

      #endregion
   }
}