﻿namespace Ftdi
{
   /// <summary>
   /// the direction setting of physical pin
   /// </summary>
   public enum PinDirection
   {
      /// <summary>
      /// GPIO pin is configured as Input
      /// </summary>
      Input = 0, 
      /// <summary>
      /// GPIO pin is configured as Output
      /// </summary>
      Output = 1 
   };
}