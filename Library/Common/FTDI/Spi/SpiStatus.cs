﻿namespace Ftdi.Spi
{
   /// <summary>
   /// Error code of FTDI function.
   /// 
   /// The returned value of FTDI DLL function is coded here. The name is changed 
   /// from FULL C Macro style to nearly C# style, but not perfect.
   /// </summary>
   public enum SpiStatus
   {
      Success = 0,
      InvalidHandle = 1,
      DeviceNotFound = 2,
      DeviceNotOpened = 3,
      IoError = 4,
      InsufficientResources = 5,

      FailedtoCompleteCommand = 20, // cannot change, error code mapped from FT2232c classes
      FailedtoSynchronizeDeviceMpsse = 21, // cannot change, error code mapped from FT2232c classes
      InvalidDeviceNameIndex = 22, // cannot change, error code mapped from FT2232c classes
      NullDeviceNameBufferPointer = 23, // cannot change, error code mapped from FT2232c classes 
      DeviceNameBufferTooSmall = 24, // cannot change, error code mapped from FT2232c classes
      InvalidDeviceName = 25, // cannot change, error code mapped from FT2232c classes
      InvalidLocationId = 26, // cannot change, error code mapped from FT2232c classes
      DeviceInUse = 27, // cannot change, error code mapped from FT2232c classes
      TooManyDevices = 28, // cannot change, error code mapped from FT2232c classes
      InvalidClockDivisor = 29,
      NullInputBufferPointer = 30,
      NullChipSelectBufferPointer = 31,
      NullInputOutputBufferPointer = 32,
      NullOutputPinsBufferPointer = 33,
      NullInitialConditionBufferPointer = 34,
      NullWriteControlBufferPointer = 35,
      NullWriteDataBufferPointer = 36,
      NullWaitDataWriteBufferPointer = 37,
      NullReadDataBufferPointer = 38,
      NullReadCmdsDataBufferPointer = 39,
      InvalidNumberControlBits = 40,
      InvalidNumberControlBytes = 41,
      NumberControlBytesTooSmall = 42,
      InvalidNumberWriteDataBits = 43,
      InvalidNumberWriteDataBytes = 44,
      NumberWriteDataBytesTooSmall = 45,
      InvalidNumberReadDataBits = 46,
      InvalidInitClockPinState = 47,
      InvalidFt2232CChipSelectPin = 48,
      InvalidFt2232CDataWriteCompletePin = 49,
      DataWriteCompleteTimeout = 50,
      InvalidConfigurationHighgerGpioPin = 51,
      CommandSequenceBufferFull = 52,
      NoCommandSequence = 53,
      NullDllVersionBufferPointer = 54,
      DllVersionBufferTooSmall = 55,
      NullLanguageCodeBufferPointer = 56,
      NullErrorMessageBufferPointer = 57,
      ErrorMessageBufferTooSmall = 58,
      InvalidLanguageCode = 59,
      InvalidStatusCode = 60
   };
}