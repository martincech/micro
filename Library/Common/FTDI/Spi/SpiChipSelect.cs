﻿namespace Ftdi.Spi
{
   /// <summary>
   /// pins for Chipselect
   /// 
   /// These identifier represent the pins which can be used as chip select signal
   /// of SPI. You can select one chip select when you access SPI device from FT2232.
   /// chip select signal can be changed by SPI.SetGPIO function. 
   /// </summary>
   public enum SpiChipSelect
   {
      // ReSharper disable InconsistentNaming
      ADBUS3ChipSelect = 0,
      ADBUS4GPIOL0,
      ADBUS5GPIOL1,
      ADBUS6GPIOL2,
      ADBUS7GPIOL3
   };
}