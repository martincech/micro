﻿#region License

/* Copyright 2012-2013 James F. Bellinger <http://www.zer7.com/software/hidsharp>

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. */

#endregion

using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Usb
{
   internal abstract class UsbManager<T> : IPlatformSupport where T : UsbDevice
   {
      private readonly Dictionary<object, T> deviceList;
      private readonly object syncRoot;


      protected UsbManager()
      {
         deviceList = new Dictionary<object, T>();
         syncRoot = new object();
      }

      protected object SyncRoot
      {
         get { return syncRoot; }
      }

      public virtual void Init()
      {
      }

      internal void RunImpl(object readyEvent)
      {
         ((ManualResetEvent) readyEvent).Set();
      }

      public IEnumerable<T> GetDevices()
      {
         lock (SyncRoot)
         {
            var devices = Refresh();
            var additions = devices.Except(deviceList.Keys).ToArray();
            var removals = deviceList.Keys.Except(devices).ToArray();

            if (additions.Length > 0)
            {
               var completedAdditions = 0;

               foreach (var addition in additions)
               {
                  ThreadPool.QueueUserWorkItem(addition_ =>
                  {
                     T device;
                     object creationState;
                     var created = TryCreateDevice(addition_, out device, out creationState);

                     if (created)
                     {
                        // By not adding on failure, we'll end up retrying every time.
                        lock (deviceList)
                        {
                           deviceList.Add(addition_, device);
                        }
                     }

                     lock (deviceList)
                     {
                        completedAdditions++;
                        Monitor.Pulse(deviceList);
                     }

                     if (created)
                     {
                        CompleteDevice(addition_, device, creationState);
                     }
                  }, addition);
               }

               lock (deviceList)
               {
                  while (completedAdditions != additions.Length)
                  {
                     Monitor.Wait(deviceList);
                  }
               }
            }

            foreach (var removal in removals)
            {
               deviceList.Remove(removal);
            }

            return deviceList.Values.ToArray();
         }
      }

      protected abstract object[] Refresh();

      protected abstract bool TryCreateDevice(object key, out T device, out object creationState);

      protected abstract void CompleteDevice(object key, T device, object creationState);

      public abstract bool IsSupported { get; }
   }
}