﻿namespace Usb
{
   internal interface IPlatformSupport
   {
      bool IsSupported { get; }
   }
}