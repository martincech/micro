﻿using System.Globalization;

namespace Usb
{
   public abstract class UsbDevice
   {
      /// <summary>
      /// The manufacturer name.
      /// </summary>
      public abstract string Manufacturer
      {
         get;
      }

      /// <summary>
      /// The USB product ID. These are listed at: http://usb-ids.gowdy.us
      /// </summary>
      public abstract int ProductID
      {
         get;
      }

      /// <summary>
      /// The product name.
      /// </summary>
      public abstract string ProductName
      {
         get;
      }

      /// <summary>
      /// The product version.
      /// This is a 16-bit number encoding the major and minor versions in the upper and lower 8 bits, respectively.
      /// </summary>
      public abstract int ProductVersion
      {
         get;
      }

      /// <summary>
      /// The device serial number.
      /// </summary>
      public abstract string SerialNumber
      {
         get;
      }

      /// <summary>
      /// The USB vendor ID. These are listed at: http://usb-ids.gowdy.us
      /// </summary>
      public abstract int VendorID
      {
         get;
      }

      /// <inheritdoc />
      public override string ToString()
      {
         return string.Format(CultureInfo.InvariantCulture, "{0} ({1}VID {2}, PID {3}, version {4})",
             Manufacturer.Length > 0 || ProductName.Length > 0 ? Manufacturer.Trim() + " " + ProductName.Trim() : "(unnamed)",
             SerialNumber.Length > 0 ? "serial " + SerialNumber.Trim() + ", " : "", VendorID, ProductID, ProductVersion);
      }
   }
}