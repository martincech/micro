﻿namespace Usb.Platform.Windows
{
   internal abstract class WinManager<T> : UsbManager<T> where T : UsbDevice 
   {
      public override bool IsSupported
      {
         get {
            return WinSystem.IsSupported();
         }
      }
   }
}
