﻿using System;
using System.Runtime.InteropServices;

namespace Usb.Platform.Windows
{
   internal static class WinSystem
   {
      internal static bool IsSupported()
      {
         if (Environment.OSVersion.Platform != PlatformID.Win32NT) return false;

         var version = new NativeMethods.OSVERSIONINFO
         {
            OSVersionInfoSize = Marshal.SizeOf(typeof(NativeMethods.OSVERSIONINFO))
         };

         try
         {
            if (NativeMethods.GetVersionEx(ref version) && version.PlatformID == 2)
            {
               return true;
            }
         }
         catch
         {
            // Apparently we have no P/Invoke access.
         }

         return false;
      }
   }
}