﻿namespace Usb.Enums
{
   public enum Parity
   {
      None,
      Odd,
      Even,
      Mark,
      Space
   }
}
