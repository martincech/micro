﻿namespace Usb.HID.ReportDescriptors.Parser
{
   public delegate void ReportScanCallback
      (byte[] buffer, int bitOffset, ReportSegment segment);
}