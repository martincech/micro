﻿namespace Usb.MODEM
{
   public abstract class ModemDevice : UsbDevice
   {
      public abstract string PortName { get; set; }
      public abstract int BaudRate { get; set; }
      public abstract int RxTimeout { get; set; }
    
      public abstract ModemStream Open();
   }
}
