﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Threading.Tasks;
using Usb.Helpers;
using Usb.Platform.Windows;

namespace Usb.MODEM.Platform.Windows
{
   internal class WinModemManager : WinManager<ModemDevice>
   {
      private static Dictionary<string, DataContent> _modemObjects;

      private struct DataContent
      {
         public int Vid;
         public int Pid;
         public string Description;
         public string Manufacturer;
      }

      protected override object[] Refresh()
      {
         try
         {
            var coms = ModemDevices();
            return coms.Cast<object>().ToArray();
         }
         catch (Exception)
         {
            return new object[]{};
         }
      }

      protected override bool TryCreateDevice(object key, out ModemDevice device, out object creationState)
      {
         try
         {
            Debug.Assert(key is KeyValuePair<string, DataContent>, "Invalid use of WinModemManager");
            var comNumber = ((KeyValuePair<string, DataContent>)key).Key;
            var content = ((KeyValuePair<string, DataContent>)key).Value;
            
            var modemDevice = new WinModemDevice(comNumber, content.Description, content.Pid, 
                  content.Vid, content.Manufacturer);
            device = modemDevice;
            creationState = null;
            return true;
         }
         catch (Exception)
         {
            device = null;
            creationState = null;
            return false;
         }
      }

      protected override void CompleteDevice(object key, ModemDevice device, object creationState)
      {
      }

      private static IEnumerable<KeyValuePair<string, DataContent>> ModemDevices()
      {
         _modemObjects = new Dictionary<string, DataContent>();
         foreach (var managementObject in new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_POTSModem").Get())
         {
            var status = managementObject["Status"].ToString();
            if (!status.Equals("OK")) continue;

            var deviceId = managementObject["DeviceID"].ToString();
            var port = managementObject["AttachedTo"].ToString();

            var data = new DataContent
            {
               Description = managementObject["Description"].ToString(),
               Pid = VidPidParser.GetPid(deviceId),
               Vid = VidPidParser.GetVid(deviceId),
               Manufacturer = managementObject["ProviderName"].ToString()
            };
            _modemObjects.Add(port, data);
         }
         
         // device is not pure modem, check if is plug via com port
         var searcher = new ManagementObjectSearcher("SELECT * FROM Win32_SerialPort").Get();
         var tasks = new Task[searcher.Count];
         var index = 0;
         foreach (var managementObject in searcher)
         {
            var obj = managementObject;
            tasks[index] = Task.Factory.StartNew(() =>
            {
               var status = obj["Status"].ToString();
               if (!status.Equals("OK")) return;
               var port = obj["DeviceID"].ToString();
               if (!port.Contains("COM") || _modemObjects.ContainsKey(port)) return;

               var test = new TestModem();
               string model;
               if (!test.IsComModem(port, out model)) return;
               
               var data = new DataContent
               {
                  Description = model,
                  Manufacturer = ""
               };
               _modemObjects.Add(port, data);
            });
         
            index++;
         }
         Task.WaitAll(tasks);
         return _modemObjects;
      }
   }
}
