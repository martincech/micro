﻿using System.Collections.Generic;
using System.Text;

namespace Usb.UMS
{
   public abstract class UmsDevice : UsbDevice
   {
      /// <summary>
      ///    The operating system's assigned paths to this mass storage device
      ///    Eg. on windows it could be something like e:/, on unix /dev/media0 etc...
      /// </summary>
      public abstract IEnumerable<string> BasePaths { get; }

      /// <inheritdoc />
      public override string ToString()
      {
         var sb = new StringBuilder();
         foreach (string basePath in BasePaths)
         {
            sb.Append(sb.Length == 0 ? basePath : ", " + basePath);
         }
         return sb + " " + base.ToString();
      }
   }
}