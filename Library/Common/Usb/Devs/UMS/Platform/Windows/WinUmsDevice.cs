﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Management;

namespace Usb.UMS.Platform.Windows
{
   internal sealed class WinUmsDevice : UmsDevice
   {
      private readonly List<string> basePaths;
      private string manufacturer;
      private int pid;
      private string productName;
      private int productVersion;
      private string serialNumber;
      private int vid;

      internal WinUmsDevice(char volumeLabel)
         : this(new List<char> {volumeLabel})
      {
      }

      internal WinUmsDevice(IEnumerable<char> volumeLabels)
      {
         basePaths = new List<string>();
         foreach (var volumeLabel in volumeLabels)
         {
            basePaths.Add(volumeLabel + ":" + Path.DirectorySeparatorChar);
         }
         serialNumber = "";
         manufacturer = "";
         productName = "";
         pid = -1;
         vid = -1;
         productVersion = -1;
      }

      #region Overrides of UmsDevice

      /// <inheritdoc />
      public override IEnumerable<string> BasePaths
      {
         get { return basePaths; }
      }

      #endregion

      internal void GetInfo(ManagementBaseObject diskDrive, int vendorId, int productId)
      {
         try
         {
            vid = vendorId;
            pid = productId;
            serialNumber = (string) diskDrive["SerialNumber"] ?? "";
            manufacturer = (string) diskDrive["Manufacturer"] ?? "";
            productName = (string) diskDrive["Model"] ?? "";
            try
            {
               productVersion = int.Parse(diskDrive["FirmwareRevision"].ToString());
            }
            catch
            {
               var s = diskDrive["FirmwareRevision"];

               var version = double.Parse(s.ToString(), CultureInfo.InvariantCulture);
               byte decimalPart = (byte)Math.Floor(version);
               var fractionalPart = version - decimalPart;
               while (fractionalPart < 0)
               {
                  fractionalPart *= 10;
               }
               byte[] ver = new[] { decimalPart, (byte)fractionalPart };
               productVersion = BitConverter.ToInt16(ver, 0);
            }
         }
         catch
         {
            productVersion = 0;
         }
      }

      #region Overrides of UsbDevice

      /// <summary>
      ///    The manufacturer name.
      /// </summary>
      public override string Manufacturer
      {
         get { return manufacturer; }
      }

      /// <summary>
      ///    The USB product ID. These are listed at: http://usb-ids.gowdy.us
      /// </summary>
      public override int ProductID
      {
         get { return pid; }
      }

      /// <summary>
      ///    The product name.
      /// </summary>
      public override string ProductName
      {
         get { return productName; }
      }

      /// <summary>
      ///    The product version.
      ///    This is a 16-bit number encoding the major and minor versions in the upper and lower 8 bits, respectively.
      /// </summary>
      public override int ProductVersion
      {
         get { return productVersion; }
      }

      /// <summary>
      ///    The device serial number.
      /// </summary>
      public override string SerialNumber
      {
         get { return serialNumber; }
      }

      /// <summary>
      ///    The USB vendor ID. These are listed at: http://usb-ids.gowdy.us
      /// </summary>
      public override int VendorID
      {
         get { return vid; }
      }

      #endregion
   }
}