﻿using System;
using System.IO;
using System.Threading;
using Usb.Helpers;

namespace Usb.ELO.Platform.Windows
{
   internal class WinEloStream : EloStream
   {
      #region Private fields

      private readonly Uart port;

      #endregion

      public WinEloStream()
      {
         port = new Uart();
      }

      #region Overrides of EloStream

      internal protected override void ChangeParameters()
      {
         if (Device == null) return;
         port.Close();
         CheckConnect();
      }

      #endregion

      #region Overrides of Stream

      /// <summary>
      ///    Closes the current stream and releases any resources (such as sockets and file handles) associated with the current
      ///    stream. Instead of calling this method, ensure that the stream is properly disposed.
      /// </summary>
      public override void Close()
      {
         if (port == null) return;
         if (port.IsOpen)
         {
            port.Close();
         }
      }

      public override void Flush()
      {
         port.Flush();
      }

      public override int Read(byte[] buffer, int offset, int count)
      {

         var i = port.Read(buffer, count);
         if (i == 0)
         {
            throw new Exception();
         }

         return i;
      }

      public override void Write(byte[] buffer, int offset, int count)
      {
         port.Flush();
         port.Write(buffer, count);
      }

      #endregion

      #region Private helpers

      private bool CheckConnect()
      {
         if (port.IsOpen)
         {
            return true;
         }
         if (Device == null) return false;

         try
         {
            if (!port.Open(Device.PortName)) return false;

            port.SetParameters(Device.BaudRate,
                               Device.Parity,
                               Device.DataBits,
                               Uart.StopBit.One,
                               Device.Handshake);

            port.SetRxWait(ReadTimeout);
            port.Flush();
            return true;
         }
         catch (IOException ioEx)
         {
            if (ioEx.Message.Contains("COM")) return false;
            Thread.Sleep(200);
            return CheckConnect();
         }
         catch (Exception)
         {
            return false;
         }
      }

      #endregion
   }
}
