﻿using System;
using Usb.Enums;

namespace Usb.ELO.Platform.Windows
{
    internal sealed class WinEloDevice : EloDevice
    {
       #region Private Fields

       private readonly string manufacturer;
       private readonly int productId;
       private readonly string productName;
       private readonly int productVersion;
       private readonly string serialNumber;
       private readonly int vendorId;

       private readonly string portName;
       private int baudRate;
       private int rxTimeout;
       private Parity parity;
       private Handshake handshake;
       private int dataBits;
       private EloStream stream;
       

       private const int DEFAULT_BAUD_RATE = 19200;
       private const int DEFAULT_READ_TIMEOUT = 1000; // ms
       private const Handshake DEFAULT_HANDSHAKE = Handshake.None;
       private const Parity DEFAULT_PARITY = Parity.Even;
       private const int DEFAULT_DATA_BITS = 8;
       #endregion

       #region Constructor

       public WinEloDevice(string port, string productName, int pid, int vid, string manufacturer, string serialNumber)
       {
          dataBits = DEFAULT_DATA_BITS;
          baudRate = DEFAULT_BAUD_RATE;
          rxTimeout = DEFAULT_READ_TIMEOUT;
          parity = DEFAULT_PARITY;
          handshake = DEFAULT_HANDSHAKE;
          portName = port;
          this.productName = productName;

          this.manufacturer = manufacturer;
          this.serialNumber = serialNumber;
          productId = pid;
          productVersion = -1;
          vendorId = vid;
       }

       #endregion

       #region Overrides of EloDevice

       public override EloStream Open()
       {
          if (stream != null)
          {
             stream.Close();
          }
          stream = new WinEloStream();
          try
          {
             stream.Init(this);
             return stream;
          }
          catch (Exception)
          {
             stream.Close();
             throw;
          }
       }

       public override string PortName { get { return portName; } }

       public override int BaudRate
       {
          get { return baudRate; }
          set
          {
             if (SetProperty(ref baudRate, value) && stream != null)
             {
                stream.ChangeParameters();
             }
          }
       }

       public override int RxTimeout
       {
          get { return rxTimeout; }
          set
          {
             if (SetProperty(ref rxTimeout, value) && stream != null)
             {
                stream.ChangeParameters();
             }
          }
       }

       public override Parity Parity
       {
          get { return parity; }
          set
          {
             if (SetProperty(ref parity, value) && stream != null)
             {
                stream.ChangeParameters();
             }
          }
       }

       public override Handshake Handshake
       {
          get { return handshake; }
          set
          {
             if (SetProperty(ref handshake, value) && stream != null)
             {
                stream.ChangeParameters();
             }
          }
       }

       public override int DataBits
       {
          get { return dataBits; }
          set
          {
             if (SetProperty(ref dataBits, value) && stream != null)
             {
                stream.ChangeParameters();
             }
          }
       }

       #endregion

       #region Overrides of UsbDevice

       public override string Manufacturer
       {
          get { return manufacturer; }
       }

       public override int ProductID
       {
          get { return productId; }
       }

       public override string ProductName
       {
          get { return productName; }
       }

       public override int ProductVersion
       {
          get { return productVersion; }
       }

       public override string SerialNumber
       {
          get { return serialNumber; }
       }

       public override int VendorID
       {
          get { return vendorId; }
       }

       #endregion

       #region Private helpers

       private bool SetProperty<T>(ref T variable, T value)
       {
          if (variable.Equals(value)) return false;
          variable = value;
          return true;
       }

       #endregion
    }
}
