﻿using System;
using System.IO;

namespace Usb.ELO
{
   public abstract class EloStream : Stream
   {
      internal void Init(EloDevice eloDevice)
      {
         Device = eloDevice;
         ChangeParameters();
      }

      protected EloDevice Device { get; private set; }

      internal protected abstract void ChangeParameters();

      #region Overrides of Stream

      /// <exclude />
      public override bool CanRead
      {
         get { return true; }
      }

      /// <exclude />
      public override bool CanSeek
      {
         get { return false; }
      }

      /// <exclude />
      public override bool CanWrite
      {
         get { return true; }
      }

      /// <exclude />
      public override bool CanTimeout
      {
         get { return true; }
      }

      /// <exclude />
      public override int ReadTimeout
      {
         get
         {
            return Device != null ? Device.RxTimeout : 0;
         }
         set
         {
            if (Device == null)
            {
               return;
            }
            Device.RxTimeout = value;
            ChangeParameters();
         }
      }

      /// <exclude />
      public override int WriteTimeout { get; set; }

      /// <exclude />
      public override void SetLength(long value)
      {
         throw new NotSupportedException();
      }

      /// <exclude />
      public override long Length
      {
         get { throw new NotSupportedException(); }
      }

      /// <exclude />
      public override long Position
      {
         get { throw new NotSupportedException(); }
         set { throw new NotSupportedException(); }
      }

      /// <exclude />
      public override long Seek(long offset, SeekOrigin origin)
      {
         throw new NotSupportedException();
      }

      #endregion
   }
}
