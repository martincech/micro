﻿using Usb.Enums;

namespace Usb.ELO
{
   public abstract class EloDevice : UsbDevice
   {
      public abstract string PortName { get;}
      public abstract int BaudRate { get; set; }
      public abstract Parity Parity { get; set; }
      public abstract int RxTimeout { get; set; }
      public abstract Handshake Handshake { get; set; }
      public abstract int DataBits { get; set; }
      public abstract EloStream Open();
   }
}