﻿using System;
using System.Diagnostics;
using System.Linq;
using Usb.Platform.Windows;

namespace Usb.FTDI.Platform.Windows
{
// ReSharper disable once InconsistentNaming
   internal class WinFTDIManager : WinManager<FTDIDevice>
   {
      #region Overrides of UsbManager<FTDIDevice>

      protected override object[] Refresh()
      {
         var devs = new FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE[] {};

         try
         {
            do
            {
               var ftdi = new Ftdi.Ftdi();
               uint devcount = 0;

               ftdi.GetNumberOfDevices(ref devcount);
               if (devcount == 0)
               {
                  break;
               }

               devs = new FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE[devcount];

               if (ftdi.GetDeviceList(devs) != FTD2XX_NET.FTDI.FT_STATUS.FT_OK)
               {
                  break;
               }
            } while (false);
         }
         catch
         {
         }

         return devs.Where(d => d != null).Distinct().Cast<object>().ToArray();
      }

      protected override bool TryCreateDevice(object key, out FTDIDevice device, out object creationState)
      {
         try
         {
            Debug.Assert(key is FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE, "Invalid use of WinFTDIManager");
            device = new WinFTDIDevice(key as FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE);
            creationState = null;
            return true;
         }
         catch (Exception)
         {
            device = null;
            creationState = null;
            return false;
         }
      }

      protected override void CompleteDevice(object key, FTDIDevice device, object creationState)
      {
         var ee2232 = new FTD2XX_NET.FTDI.FT2232_EEPROM_STRUCTURE();
         var ee232R = new FTD2XX_NET.FTDI.FT232R_EEPROM_STRUCTURE();
         var ee2232H = new FTD2XX_NET.FTDI.FT2232H_EEPROM_STRUCTURE();
         var ee4232H = new FTD2XX_NET.FTDI.FT4232H_EEPROM_STRUCTURE();
         var winDevice = (WinFTDIDevice) device;
         var ftdi = new Ftdi.Ftdi();
         switch (winDevice.Type)
         {
            case FTD2XX_NET.FTDI.FT_DEVICE.FT_DEVICE_2232:
               ftdi.ReadFT2232EEPROM(ee2232);
               winDevice.EepromData = ee2232;
               break;
            case FTD2XX_NET.FTDI.FT_DEVICE.FT_DEVICE_232R:
               ftdi.ReadFT232REEPROM(ee232R);
               winDevice.EepromData = ee232R;
               break;
            case FTD2XX_NET.FTDI.FT_DEVICE.FT_DEVICE_2232H:
               ftdi.ReadFT2232HEEPROM(ee2232H);
               winDevice.EepromData = ee2232H;
               break;
            case FTD2XX_NET.FTDI.FT_DEVICE.FT_DEVICE_4232H:
               ftdi.ReadFT4232HEEPROM(ee4232H);
               winDevice.EepromData = ee4232H;
               break;
         }
      }

      #endregion
   }
}