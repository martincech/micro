﻿/**
 * Copyright 2008-2009 The Asagao Project. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 *
 *  1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE ASAGAO PROJECT ``AS IS'' AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE ASAGAO PROJECT OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE 
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation 
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of the Asagao Project.
 * 
 * http://sourceforge.net/projects/asagao/
* \file bitop.cs
* \brief bit controling for bytes array.
*/

namespace Utilities
{
   /// <summary>
   /// \brief Bit controling class
   /// 
   /// This class provide a set of bit manupiration functions. which set/clear/toggle a specified bit by bit position. 
   /// </summary>
   public static class BitOp
   {
      /// <summary>
      /// \brief calc a position of bit in a byte and byte index in an array.
      /// \param bitPos Input parameter which contains bit position in a long string. After call, 
      /// it contains a bit position in a byte.
      /// \param bytePos Out put parameter which represent a byte index in a byte array.
      /// 
      /// Calc a bit/byte index in a byte from a bit position. Bit position is given as integer. 
      /// And it is assumed contains 8 bits in a byte. The position in an array is from LSB to MSB.
      /// </summary>
      private static void calcBitPos(ref int bitPos, out int bytePos)
      {
         bytePos = bitPos >> 3; // Take except LSB 3bits
         bitPos &= 0x07; // Take LSB 3bits
      }

      /// <summary>
      /// \brief Set a specified bit
      /// \param byteArray an array of byte which will be manupirated
      /// \param bitPos specify a bit by position. Zero origin. 
      /// 
      /// Set a specified bit to 1 in a byteArray parameter. Note that bit position is
      /// given as zero origin number. 
      /// </summary>
      public static void Set(byte[] byteArray, int bitPos)
      {
         int bytePos;

         calcBitPos(ref bitPos, out bytePos); // get byte index and bit in a byte
         byteArray[bytePos] |= (byte)(1 << bitPos); // set a bit
      }

      /// <summary>
      /// \brief Clear a specified bit
      /// \param byteArray an array of byte which will be manupirated
      /// \param bitPos specify a bit by position. Zero origin. 
      /// 
      /// Clear a specified bit to 0 in a byteArray parameter. Note that bit position is
      /// given as zero origin number. 
      /// </summary>
      public static void Clr(byte[] byteArray, int bitPos)
      {
         int bytePos;

         calcBitPos(ref bitPos, out bytePos); // get byte index and bit in a byte
         byteArray[bytePos] &= (byte)~(1 << bitPos); // clear a byte
      }

      /// <summary>
      /// \brief Toggle a specified bit
      /// \param byteArray an array of byte which will be manupirated
      /// \param bitPos specify a bit by position. Zero origin. 
      /// 
      /// Toggle a specified bit to 0 in a byteArray parameter. Note that bit position is
      /// given as zero origin number. 
      /// </summary>
      public static void Toggle(byte[] byteArray, int bitPos)
      {
         int bytePos;

         calcBitPos(ref bitPos, out bytePos);
         byteArray[bytePos] ^= (byte)(1 << bitPos);
      }

      /// <summary>
      /// \brief Extract a specified bit with byte offset
      /// \param byteArray an array of byte which will be manupirated
      /// \param byteOffset
      /// \param bitPos specify a bit by position. Zero origin. 
      /// 
      /// Toggle a specified bit to 0 in a byteArray parameter. Note that bit position is
      /// given as zero origin number. byte offset shift the origin of bitPos.
      /// </summary>
      public static int Extract(byte[] byteArray, int byteOffset, int bitPos)
      {
         int bytePos;
         calcBitPos(ref bitPos, out bytePos);
         if ((byteArray[bytePos + byteOffset] & (byte)(1 << bitPos)) != 0)
            return 1;
         else
            return 0;
      }

      /// <summary>
      /// \brief Extract a bit from byte array
      /// \param byteArray an array of byte which will be manupirated
      /// \param bitPos specify a bit by position. Zero origin.
      /// \return value of specified bit
      /// 
      /// Extract a specified bit in a byteArray parameter. Note that bit position is
      /// given as zero origin number. 
      /// </summary>
      public static int Extract(byte[] byteArray, int bitPos)
      {
         return Extract(byteArray, 0, bitPos);
      }
   }
} // JtagKey
