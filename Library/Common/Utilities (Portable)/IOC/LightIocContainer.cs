﻿using System;
using System.Collections.Generic;

namespace Utilities.IOC
{
   public class LightIocContainer : IIocContainer
   {
      private readonly Dictionary<Type, Lazy<object>> services = new
         Dictionary<Type, Lazy<object>>();

      #region Implementation of IIocContainer

      /// <summary>
      /// Register function for creation object of type T
      /// </summary>
      /// <typeparam name="T">type of object to be registered</typeparam>
      /// <param name="function">function which creates an instance of <see cref="T"/></param>
      public void Bind<T>(Func<T> function)
      {
         services[typeof(T)] = new Lazy<object>(() => function());
      }

      /// <summary>
      /// Get object from this container specified by its type.
      /// </summary>
      /// <typeparam name="T">Generic type to get</typeparam>
      /// <returns>object of type T if this type is registered in IOC</returns>
      /// <exception cref="NotSupportedException">when T is not registered in IOC or is unknown</exception>
      public T Get<T>()
      {
         return (T)Get(typeof(T));
      }

      /// <summary>
      /// Get object from this container as dynamically specified by its type.
      /// </summary>
      /// <param name="t"><see cref="Type"/> of object to get</param>
      /// <returns>object of <see cref="Type"/> <see cref="t"/> if this type is registered in IOC</returns>
      /// /// <exception cref="NotSupportedException">when <see cref="t"/> is not registered in IOC or is unknown</exception>
      public object Get(Type t)
      {
         Lazy<object> service;
         if (services.TryGetValue(t, out service))
         {
            return service.Value;
         }
         throw new Exception("Service not found!");
      }

      #endregion
   }
}
