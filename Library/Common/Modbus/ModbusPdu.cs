﻿using System.Runtime.InteropServices;

namespace Modbus {
    public class ModbusPdu {
        public const ushort MIN_ITEM_ADDRESS = 0x0000;        // minimalni adresa veliciny
        public const ushort MAX_ITEM_ADDRESS = 0xFFFF;        // maximalni adresa veliciny

        // Kody vyjimek
        public enum ExceptionType : byte {
            NONE                     = 0x00,
            ILLEGAL_FUNCTION         = 0x01,   // function code not served
            ILLEGAL_DATA_ADDRESS     = 0x02,   // data address not allowable
            ILLEGAL_DATA_VALUE       = 0x03,   // data in the query is not allowable
            SLAVE_DEVICE_FAILURE     = 0x04,   // unrecoverable error
            ACKNOWLEDGE              = 0x05,   // programming is running, wait for response
            SLAVE_DEVICE_BUSY        = 0x06,   // long duration program is running, wait for response
            MEMORY_PARITY_ERROR      = 0x08,   // memory consistency error
            GATEWAY_PATH_UNAVAILABLE = 0x0A,   // gateway processing - no path to target
            GATEWAY_TARGET_FAILED    = 0x0B,   // gateway processing - no response by target
            _LAST
        }

        // Funkcni kody
        public enum Function : byte {
            READ_COILS               = 0x01,
            READ_DISCRETE            = 0x02,
            READ_HOLDING_REGISTERS   = 0x03,
            READ_INPUT_REGISTERS     = 0x04,
            WRITE_SINGLE_COIL        = 0x05,
            WRITE_SINGLE_REGISTER    = 0x06,
            READ_EXCEPTION_STATUS    = 0x07,
            DIAGNOSTIC               = 0x08,
            GET_COM_EVENT_COUNTER    = 0x0B,
            GET_COM_EVENT_LOG        = 0x0C,
            WRITE_MULTIPLE_COILS     = 0x0F,
            WRITE_MULTIPLE_REGISTERS = 0x10,
            REPORT_SLAVE_ID          = 0x11,
            READ_FILE_RECORD         = 0x14,
            WRITE_FILE_RECORD        = 0x15,
            MASK_WRITE_REGISTER      = 0x16,
            RW_MULTIPLE_REGISTERS    = 0x17,
            READ_FIFO_QUEUE          = 0x18,
            READ_DEVICE_ID           = 0x2B,
            _LAST
        }

        // Exception
        [StructLayout(LayoutKind.Sequential, Pack=1)]                                   // Zarovnani na 1 bajt
        public struct ExceptionReply {
            public byte Function;
            public byte Code;
        }

        //-----------------------------------------------------------------------------
        // Read (holding) registers
        //-----------------------------------------------------------------------------

        public const int MAX_READ_REGISTERS_COUNT = 125;

        [StructLayout(LayoutKind.Sequential, Pack=1)]                                   // Zarovnani na 1 bajt
        public struct ReadRegisters {
            public byte   Function;
            public ushort Address;
            public ushort Count;
        }

        // reply :
        [StructLayout(LayoutKind.Sequential, Pack=1)]                                   // Zarovnani na 1 bajt
        public struct ReadRegistersReply {
            public byte Function;
            public byte ByteCount;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_READ_REGISTERS_COUNT)]
            public ushort[] Data;
        }

        public const int SizeofReadRegistersReply = 2;       // Bez pole dat

        //-----------------------------------------------------------------------------
        // Write Single (holding) Register
        //-----------------------------------------------------------------------------

        [StructLayout(LayoutKind.Sequential, Pack=1)]                                   // Zarovnani na 1 bajt
        public struct WriteSingleRegister {
            public byte   Function;
            public ushort Address;
            public ushort Value;
        }

        public const int SizeofWriteSingleRegister = 5;

        //-----------------------------------------------------------------------------
        // Write (multiple) registers
        //-----------------------------------------------------------------------------

        public const int MAX_WRITE_REGISTERS_COUNT = 120;

        [StructLayout(LayoutKind.Sequential, Pack=1)]                                   // Zarovnani na 1 bajt
        public struct WriteRegisters {
            public byte   Function;
            public ushort Address;
            public ushort Count;
            public byte   ByteCount;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_WRITE_REGISTERS_COUNT)]
            public ushort[] Data;
        }

        public const int SizeofWriteRegisters = 6;       // Bez pole dat

        // reply :
        [StructLayout(LayoutKind.Sequential, Pack=1)]                                   // Zarovnani na 1 bajt
        public struct WriteRegistersReply {
            public byte   Function;
            public ushort Address;
            public ushort Count;
        }

        static public int SizeofWriteRegistersReply() {
            return 5;
        }

        //-----------------------------------------------------------------------------
        // Exception status
        //-----------------------------------------------------------------------------

        [StructLayout(LayoutKind.Sequential, Pack=1)]                                   // Zarovnani na 1 bajt
        public struct ExceptionStatus {
            public byte Function;
        }

        // reply :
        [StructLayout(LayoutKind.Sequential, Pack=1)]                                   // Zarovnani na 1 bajt
        public struct ExceptionStatusReply {
            public byte Function;
            public byte Status;
        }

        //-----------------------------------------------------------------------------
        // Union vyslanych PDU
        //-----------------------------------------------------------------------------

        // Union lze vytvorit jen ze struktur, ktere neobsahuji pole. Struktury s polem musim
        // marshalovat zvlast, mimo union.
        [StructLayout(LayoutKind.Explicit, Pack=1)]
        public struct PduReply {
            [FieldOffset(0)]
            public byte                      Function;                  // samotny funkcni kod
            [FieldOffset(0)]
            public ExceptionReply           Exception;
            [FieldOffset(0)]
            public WriteSingleRegister      WriteSingleRegister;
            [FieldOffset(0)]
            public WriteRegistersReply      WriteRegisters;
            [FieldOffset(0)]
            public ExceptionStatusReply     ExceptionStatus;
        }

    }
}