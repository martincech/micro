﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using ParadoxReader.Enums;

namespace ParadoxReader.Db
{
   public class File : IDisposable
   {
      public string TableName;

      public ushort RecordSize { get; private set; }
      ushort headerSize;
      public FileType FileType { get; private set; }
      byte maxTableSize;
      public int RecordCount { get; private set; }
      ushort nextBlock;
      ushort fileBlocks;
      ushort firstBlock;
      ushort lastBlock;
      ushort unknown12x13;
      byte modifiedFlags1;
      byte indexFieldNumber;
      int primaryIndexWorkspace;
      int unknownPtr1A;
      protected ushort pxRootBlockId;
      protected byte pxLevelCount;
      public short FieldCount { get; private set; }
      short primaryKeyFields;
      int encryption1;
      byte sortOrder;
      byte modifiedFlags2;
      private byte[] unknown2Bx2C;  //  array[$002B..$002C] of byte;
      byte changeCount1;
      byte changeCount2;
      byte unknown2F;
      private int tableNamePtrPtr; // ^pchar;
      private int fldInfoPtr;  //  PFldInfoRec;
      byte writeProtected;
      byte fileVersionID;
      ushort maxBlocks;
      byte unknown3C;
      byte auxPasswords;
      private byte[] unknown3Ex3F; //  array[$003E..$003F] of byte;
      private int cryptInfoStartPtr; //  pointer;
      int cryptInfoEndPtr;
      byte unknown48;
      private int autoIncVal; //  longint;
      private byte[] unknown4Dx4E;  //array[$004D..$004E] of byte;
      byte indexUpdateRequired;
      byte[] unknown50x54;  //array[$0050..$0054] of byte;
      private byte refIntegrity;
      byte[] unknown56x57;  //array[$0056..$0057] of byte;
      private V4Hdr V4Header;
      internal FieldInfo[] FieldTypes { get; set; } // array[1..255] of TFldInfoRec);
      private int tableNamePtr;
      private int[] fieldNamePtrArray;
      public string[] FieldNames { get; private set; }

      private readonly Stream stream;
      private readonly BinaryReader reader;

      public File(string fileName)
         : this(new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
      {
      }

      public File(Stream stream)
      {
         this.stream = stream;
         reader = new BinaryReader(stream);
         stream.Position = 0;
         ReadHeader();
      }

      public virtual void Dispose()
      {
         stream.Dispose();
      }

      internal virtual byte[] ReadBlob(byte[] blobInfo)
      {
         return null;
      }

      public IEnumerable<Record> Enumerate(Predicate<Record> where = null)
      {
         for (int blockId = 0; blockId < fileBlocks; blockId++)
         {
            var block = GetBlock(blockId);
            for (var recId = 0; recId < block.RecordCount; recId++)
            {
               var rec = block[recId];
               if (where == null || where(rec))
               {
                  yield return rec;
               }
            }
         }
      }

      private void ReadHeader()
      {
         var r = reader;
         RecordSize = r.ReadUInt16();
         headerSize = r.ReadUInt16();
         FileType = (FileType)r.ReadByte();
         maxTableSize = r.ReadByte();
         RecordCount = r.ReadInt32();
         nextBlock = r.ReadUInt16();
         fileBlocks = r.ReadUInt16();
         firstBlock = r.ReadUInt16();
         lastBlock = r.ReadUInt16();
         unknown12x13 = r.ReadUInt16();
         modifiedFlags1 = r.ReadByte();
         indexFieldNumber = r.ReadByte();
         primaryIndexWorkspace = r.ReadInt32();
         unknownPtr1A = r.ReadInt32();
         pxRootBlockId = r.ReadUInt16();
         pxLevelCount = r.ReadByte();
         FieldCount = r.ReadInt16();
         primaryKeyFields = r.ReadInt16();
         encryption1 = r.ReadInt32();
         sortOrder = r.ReadByte();
         modifiedFlags2 = r.ReadByte();
         unknown2Bx2C = r.ReadBytes(0x002C - 0x002B + 1);
         changeCount1 = r.ReadByte();
         changeCount2 = r.ReadByte();
         unknown2F = r.ReadByte();
         tableNamePtrPtr = r.ReadInt32(); // ^pchar;
         fldInfoPtr = r.ReadInt32(); //  PFldInfoRec;
         writeProtected = r.ReadByte();
         fileVersionID = r.ReadByte();
         maxBlocks = r.ReadUInt16();
         unknown3C = r.ReadByte();
         auxPasswords = r.ReadByte();
         unknown3Ex3F = r.ReadBytes(0x003F - 0x003E + 1);
         cryptInfoStartPtr = r.ReadInt32(); //  pointer;
         cryptInfoEndPtr = r.ReadInt32();
         unknown48 = r.ReadByte();
         autoIncVal = r.ReadInt32(); //  longint;
         unknown4Dx4E = r.ReadBytes(0x004E - 0x004D + 1);
         indexUpdateRequired = r.ReadByte();
         unknown50x54 = r.ReadBytes(0x0054 - 0x0050 + 1);
         refIntegrity = r.ReadByte();
         unknown56x57 = r.ReadBytes(0x0057 - 0x0056 + 1);

         if ((FileType == FileType.DbFileIndexed ||
              FileType == FileType.DbFileNotIndexed ||
              FileType == FileType.XnnFileInc ||
              FileType == FileType.XnnFileNonInc) &&
             fileVersionID >= 5)
         {
            V4Header = new V4Hdr(r);
         }
         var buff = new List<FieldInfo>();
         for (int i = 0; i < FieldCount; i++)
         {
            buff.Add(new FieldInfo(r));
         }
         if (FileType == FileType.PxFile)
         {
            FieldCount += 3;
            buff.Add(new FieldInfo(Enums.FieldTypes.Short, sizeof(short)));
            buff.Add(new FieldInfo(Enums.FieldTypes.Short, sizeof(short)));
            buff.Add(new FieldInfo(Enums.FieldTypes.Short, sizeof(short)));
         }
         FieldTypes = buff.ToArray();
         tableNamePtr = r.ReadInt32();
         if (FileType == FileType.DbFileIndexed ||
             FileType == FileType.DbFileNotIndexed)
         {
            fieldNamePtrArray = new int[FieldCount];
            for (int i = 0; i < FieldCount; i++)
            {
               fieldNamePtrArray[i] = r.ReadInt32();
            }
         }
         var tableNameBuff = r.ReadBytes(fileVersionID >= 0x0C ? 261 : 79);
         TableName = Encoding.ASCII.GetString(tableNameBuff, 0, Array.FindIndex(tableNameBuff, b => b == 0));
         if (FileType == FileType.DbFileIndexed ||
             FileType == FileType.DbFileNotIndexed)
         {
            FieldNames = new string[FieldCount];
            for (int i = 0; i < FieldCount; i++)
            {
               var fldNameBuff = new StringBuilder();
               char ch;
               while ((ch = r.ReadChar()) != '\x00') fldNameBuff.Append(ch);
               FieldNames[i] = fldNameBuff.ToString();
            }
         }
      }

      internal DataBlock GetBlock(int blockId)
      {
         stream.Position = blockId * maxTableSize * 0x0400 + headerSize;
         return new DataBlock(this, reader);
      }

      public string GetString(byte[] data, int from, int maxLength)
      {
         int stringLength = Array.FindIndex(data, from, b => b == 0) - from;
         if (stringLength > maxLength)
            stringLength = maxLength;
         return Encoding.Default.GetString(data, from, stringLength);
      }

      public string GetStringFromMemo(byte[] data, int from, int size)
      {
         var memoBufferSize = size - 10;
         var memoDataBuffer = new byte[memoBufferSize];
         var memoMetaData = new byte[10];
         Array.Copy(data, from, memoDataBuffer, 0, memoBufferSize);
         Array.Copy(data, from + memoBufferSize, memoMetaData, 0, 10);

         //var offsetIntoMemoFile = (long)BitConverter.ToInt32(memoMetaData, 0); 
         //offsetIntoMemoFile &= 0xffffff00;
         //var memoModNumber = BitConverter.ToInt16(memoMetaData,8); 
         //var index = memoMetaData[0]; 

         var memoSize = BitConverter.ToInt32(memoMetaData, 4);
         return GetString(memoDataBuffer, 0, memoSize);
      }

      public class V4Hdr
      {
         short fileVerID2;
         short fileVerID3;
         int encryption2;
         int fileUpdateTime;  // 4.0 only
         ushort hiFieldID;
         ushort hiFieldIDinfo;
         short sometimesNumFields;
         ushort dosCodePage;
         private byte[] unknown6Cx6F;  //array[$006C..$006F] of byte;
         private short changeCount4;
         private byte[] unknown72x77; //    :  array[$0072..$0077] of byte;

         public V4Hdr(BinaryReader r)
         {
            fileVerID2 = r.ReadInt16();
            fileVerID3 = r.ReadInt16();
            encryption2 = r.ReadInt32();
            fileUpdateTime = r.ReadInt32(); // 4.0 only
            hiFieldID = r.ReadUInt16();
            hiFieldIDinfo = r.ReadUInt16();
            sometimesNumFields = r.ReadInt16();
            dosCodePage = r.ReadUInt16();
            unknown6Cx6F = r.ReadBytes(0x006F - 0x006C + 1); //array[$006C..$006F] of byte;
            changeCount4 = r.ReadInt16();
            unknown72x77 = r.ReadBytes(0x0077 - 0x0072 + 1); //    :  array[$0072..$0077] of byte;
         }

      }

      internal class DataBlock
      {
         public File file;
         ushort nextBlock;
         ushort blockNumber;
         short addDataSize;
         public byte[] data;
         private Record[] recCache;

         public int RecordCount { get; private set; }

         public DataBlock(File file, BinaryReader r)
         {
            this.file = file;
            nextBlock = r.ReadUInt16();
            blockNumber = r.ReadUInt16();
            addDataSize = r.ReadInt16();

            RecordCount = (addDataSize / file.RecordSize) + 1;
            data = r.ReadBytes(RecordCount * file.RecordSize);
            recCache = new Record[data.Length];
         }

         public Record this[int recIndex]
         {
            get
            {
               if (recCache[recIndex] == null)
               {
                  recCache[recIndex] = new Record(this, recIndex);
               }
               return recCache[recIndex];
            }
         }
      }

      internal class FieldInfo
      {
         public FieldTypes fType;
         public byte fSize;

         public FieldInfo(FieldTypes fType, byte fSize)
         {
            this.fType = fType;
            this.fSize = fSize;
         }

         public FieldInfo(BinaryReader r)
         {
            fType = (FieldTypes)r.ReadByte();
            fSize = r.ReadByte();
         }
      }
   }
}
