﻿using System;
using System.IO;
using System.Text;
using ParadoxReader.Enums;

namespace ParadoxReader.Db
{
   public class Record
   {
      internal readonly File.DataBlock block;
      private readonly int recIndex;

      internal Record(File.DataBlock block, int recIndex)
      {
         this.block = block;
         this.recIndex = recIndex;
      }

      private object[] data;

      public object[] DataValues
      {
         get
         {
            if (data == null)
            {
               var buff = new MemoryStream(block.data);
               buff.Position = block.file.RecordSize * recIndex;
               using (var r = new BinaryReader(buff, Encoding.Default))
               {
                  data = new object[block.file.FieldCount];
                  for (int colIndex = 0; colIndex < data.Length; colIndex++)
                  {
                     var fInfo = block.file.FieldTypes[colIndex];
                     var dataSize = fInfo.fType == FieldTypes.BCD ? 17 : fInfo.fSize;
                     var empty = true;
                     for (var i = 0; i < dataSize; i++)
                     {
                        if (block.data[buff.Position + i] != 0)
                        {
                           empty = false;
                           break;
                        }
                     }
                     if (empty)
                     {
                        data[colIndex] = DBNull.Value;
                        buff.Position += dataSize;
                        continue;
                     }
                     object val;
                     switch (fInfo.fType)
                     {
                        case FieldTypes.Alpha:
                           val = block.file.GetString(block.data, (int)buff.Position, dataSize);
                           buff.Position += dataSize;
                           break;
                        case FieldTypes.MemoBLOb:
                           val = block.file.GetStringFromMemo(block.data, (int)buff.Position, dataSize);
                           buff.Position += dataSize;
                           break;
                        case FieldTypes.Short:
                           ConvertBytes((int)buff.Position, dataSize);
                           val = r.ReadInt16();
                           break;
                        case FieldTypes.Long:
                        case FieldTypes.AutoInc:
                           ConvertBytes((int)buff.Position, dataSize);
                           val = r.ReadInt32();
                           break;
                        case FieldTypes.Currency:
                           ConvertBytes((int)buff.Position, dataSize);
                           val = r.ReadDouble();
                           break;
                        case FieldTypes.Number:
                           ConvertBytesNum((int)buff.Position, dataSize);
                           var dbl = r.ReadDouble();
                           val = (double.IsNaN(dbl)) ? (object)DBNull.Value : dbl;
                           break;
                        case FieldTypes.Date:
                           ConvertBytes((int)buff.Position, dataSize);
                           var days = r.ReadInt32();
                           val = new DateTime(1, 1, 1).AddDays(days - 1);
                           break;
                        case FieldTypes.Timestamp:
                           ConvertBytes((int)buff.Position, dataSize);
                           var ms = r.ReadDouble();
                           val = new DateTime(1, 1, 1).AddMilliseconds(ms).AddDays(-1);
                           break;
                        case FieldTypes.Time:
                           ConvertBytes((int)buff.Position, dataSize);
                           val = TimeSpan.FromMilliseconds(r.ReadInt32());
                           break;
                        case FieldTypes.Logical:
                           // False is stored as 128, and True looks like 129.
                           val = (block.data[(int)buff.Position] - 128) > 0;
                           buff.Position += dataSize;
                           break;
                        case FieldTypes.BLOb:
                           var blobInfo = new byte[dataSize];
                           r.Read(blobInfo, 0, dataSize);
                           val = block.file.ReadBlob(blobInfo);
                           break;
                        default:
                           val = null; // not supported
                           buff.Position += dataSize;
                           break;
                     }
                     data[colIndex] = val;
                  }
               }
            }
            return data;
         }
      }

      private void ConvertBytes(int start, int length)
      {
         block.data[start] = (byte)(block.data[start] ^ 0x80);
         Array.Reverse(block.data, start, length);
      }

      private void ConvertBytesNum(int start, int length) /* amk */
      {
         if ((block.data[start] & 0x80) != 0)
            block.data[start] = (byte)(block.data[start] & 0x7F);
         else if (block.data[start + 0] == 0 &&
                     block.data[start + 1] == 0 &&
                     block.data[start + 2] == 0 &&
                     block.data[start + 3] == 0 &&
                     block.data[start + 4] == 0 &&
                     block.data[start + 5] == 0 &&
                     block.data[start + 6] == 0 &&
                     block.data[start + 7] == 0) /* sorry, did not check lenght */
            ;
         else for (int i = 0; i < 8; i++)
               block.data[start + i] = (byte)(~(block.data[start + i]));

         Array.Reverse(block.data, start, length);
      }

   }
}
