﻿using System;
using System.Collections.Generic;
using System.Data;
using ParadoxReader.Db;
using ParadoxReader.Enums;

namespace ParadoxReader
{
   public class DataReader : IDataReader
   {
      public File File { get; private set; }

      private IEnumerator<Record> enumerator;

      public Record CurrentRecord
      {
         get { return enumerator.Current; }
      }

      public DataReader(File file, IEnumerable<Record> query)
      {
         File = file;
         enumerator = query.GetEnumerator();
      }

      public void Dispose()
      {
      }

      public string GetName(int i)
      {
         return File.FieldNames[i];
      }

      public string GetDataTypeName(int colIndex)
      {
         return "pxf" + File.FieldTypes[colIndex].fType;
      }

      public Type GetFieldType(int colIndex)
      {
         var fInfo = File.FieldTypes[colIndex];
         switch (fInfo.fType)
         {
            case FieldTypes.Alpha:
            case FieldTypes.MemoBLOb:
               return typeof(string);
            case FieldTypes.Short:
               return typeof(short);
            case FieldTypes.Long:
               return typeof(uint);
            case FieldTypes.Currency:
               return typeof(double);
            case FieldTypes.Number:
               return typeof(double);
            case FieldTypes.Date:
               return typeof(DateTime);
            case FieldTypes.Timestamp:
               return typeof(DateTime);
            default:
               throw new NotSupportedException();
         }
      }

      public object GetValue(int i)
      {
         return CurrentRecord.DataValues[i];
      }

      public int GetValues(object[] values)
      {
         return 0;
      }

      public int GetOrdinal(string name)
      {
         return Array.FindIndex(File.FieldNames,
                                f => string.Equals(f, name, StringComparison.InvariantCultureIgnoreCase));
      }

      public bool GetBoolean(int i)
      {
         return (bool)GetValue(i);
      }

      public byte GetByte(int i)
      {
         return (byte)GetValue(i);
      }

      public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
      {
         throw new NotImplementedException();
      }

      public char GetChar(int i)
      {
         throw new NotImplementedException();
      }

      public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
      {
         throw new NotImplementedException();
      }

      public Guid GetGuid(int i)
      {
         throw new NotImplementedException();
      }

      public short GetInt16(int i)
      {
         return (short)GetValue(i);
      }

      public int GetInt32(int i)
      {
         return (int)GetValue(i);
      }

      public long GetInt64(int i)
      {
         return (long)GetValue(i);
      }

      public float GetFloat(int i)
      {
         return (float)GetValue(i);
      }

      public double GetDouble(int i)
      {
         return (double)GetValue(i);
      }

      public string GetString(int i)
      {
         return (string)GetValue(i);
      }

      public decimal GetDecimal(int i)
      {
         return (decimal)GetValue(i);
      }

      public DateTime GetDateTime(int i)
      {
         return (DateTime)GetValue(i);
      }

      public IDataReader GetData(int i)
      {
         throw new NotImplementedException();
      }

      public bool IsDBNull(int i)
      {
         return GetValue(i) == DBNull.Value;
      }

      public int FieldCount
      {
         get { return File.FieldCount; }
      }

      public object this[int i]
      {
         get { return GetValue(i); }
      }

      public object this[string name]
      {
         get { return GetValue(GetOrdinal(name)); }
      }

      public void Close()
      {
         throw new NotImplementedException();
      }

      public DataTable GetSchemaTable()
      {
         throw new NotImplementedException();
      }

      public bool NextResult()
      {
         throw new NotImplementedException();
      }

      public bool Read()
      {
         return enumerator.MoveNext();
      }

      public int Depth
      {
         get { return 0; }
      }

      public bool IsClosed
      {
         get { return false; }
      }

      public int RecordsAffected
      {
         get { return 0; }
      }
   }
}
