﻿using System.Collections.Generic;
using ParadoxReader.Db;

namespace ParadoxReader
{
   public class PrimaryKey : File
   {
      private readonly Table table;

      public PrimaryKey(Table table, string filePath)
         : base(filePath)
      {
         this.table = table;
      }

      public IEnumerable<Record> Enumerate(Condition condition)
      {
         return Enumerate(condition, (ushort)(pxRootBlockId - 1), pxLevelCount);
      }

      private IEnumerable<Record> Enumerate(Condition condition, ushort blockId, int indexLevel)
      {
         if (indexLevel == 0)
         {
            var block = table.GetBlock(blockId);
            for (int i = 0; i < block.RecordCount; i++)
            {
               var rec = block[i];
               if (condition.IsDataOk(rec))
               {
                  yield return rec;
               }
            }
         }
         else
         {
            var block = GetBlock(blockId);
            var blockIdFldIndex = FieldCount - 3;
            for (int i = 0; i < block.RecordCount; i++)
            {
               var rec = block[i];
               if (condition.IsIndexPossible(rec, i < block.RecordCount - 1 ? block[i + 1] : null))
               {
                  var qry = Enumerate(condition, (ushort)((short)rec.DataValues[blockIdFldIndex] - 1), indexLevel - 1);
                  foreach (var dataRec in qry)
                  {
                     yield return dataRec;
                  }
               }
            }
         }
      }
   }
}
