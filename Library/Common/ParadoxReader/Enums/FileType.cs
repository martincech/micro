﻿namespace ParadoxReader.Enums
{
   public enum FileType : byte
   {
      DbFileIndexed = 0,
      PxFile = 1,
      DbFileNotIndexed = 2,
      XnnFileNonInc = 3,
      YnnFile = 4,
      XnnFileInc = 5,
      XgnFileNonInc = 6,
      YgnFile = 7,
      XgnFileInc = 8
   }
}
