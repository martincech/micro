﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Services.WebApi
{
   public class WebApiClient
   {
      #region Private fields

      private const string Bat2ApiPath = "api/v1/Bat2s";
      private const string Bat2OldCurvesApiPath = "api/v1/Bat2OldCurves";
      private const string Bat2OldFlocksApiPath = "api/v1/Bat2OldFlocks";
      private const string Bat2OldStatisticsApiPath = "api/v1/Bat2OldStatistics";
      private const string AccountRolePath = "api/v1/AccountRoles";

      private const string AuthBasic = "Basic";
      private const string MediaJson = "application/json";

      private readonly string _server;
      private readonly string _userName;
      private readonly string _password;

      private const string DataTypeData = "Data";
      private const string DataTypeScales = "Scales";
      private const string DataTypeCurves = "Curves";
      private const string DataTypeFlocks = "Flocks";

      private string _lastErrorMsg;

      #endregion

      #region Public interfaces

      #region Constructor

      public WebApiClient(string server, string userName, string password)
      {
         _server = server;
         _userName = userName;
         _password = password;
      }


      public delegate void ItemSyncedHandler(object sender, int percent);
      public event ItemSyncedHandler ItemSynced;
      public int CreatedItemsCount { get; private set; }

      #endregion

      /// <summary>
      /// Get last error message.
      /// </summary>
      public string LastErrorMsg { get { return _lastErrorMsg; } }

      /// <summary>
      /// Call web api methods corresponding with parameter type
      /// </summary>
      /// <param name="data">list of records from database</param>
      /// <param name="type">database file name</param>
      /// <returns>number of correctly synchronized items</returns>
      public int Synchronize(IEnumerable<object> data, string type)
      {
         var list = data.ToList();
         var apiPath = "";
         if (type.Equals(DataTypeData))
         {
            apiPath = Bat2OldStatisticsApiPath;
         }
         else if (type.Equals(DataTypeScales))
         {
            apiPath = Bat2ApiPath;

            // transform data from collection of id to anonymous type           
            var transformData = new List<object>();
            foreach (var item in list)
            {
               transformData.Add(new { SerialNumber = 0, Name = item });
            }
            list = transformData;
         }
         else if (type.Equals(DataTypeCurves))
         {
            apiPath = Bat2OldCurvesApiPath;
         }
         else if (type.Equals(DataTypeFlocks))
         {
            apiPath = Bat2OldFlocksApiPath;
         }

         var count = 0;
         foreach (var item in list)
         {
            var response = Post(item, apiPath);
            response.Wait();
            if (response.Result)
            {
               count += CreatedItemsCount;
            }
            if (ItemSynced != null)
            {
               double itemNumber = list.IndexOf(item) + 1;
               double countNumber = list.Count;
               double result = (itemNumber / countNumber) * 100;
               ItemSynced(data, (int)result);
            }

         }
         return count;
      }

      /// <summary>
      /// Test connect to address. Check user credentials.
      /// Call simple get method.
      /// </summary>
      /// <returns>true - user can connect to address</returns>
      public bool TestConnect()
      {
         var response = Get(Bat2ApiPath);
         response.Wait();
         return response.Result != null;
      }


      /// <summary>
      /// Get current user's role.
      /// </summary>
      /// <returns>Role</returns>
      public string UserRole()
      {  
         var response = Get(AccountRolePath);
         response.Wait();

         if (response.Result == null) return string.Empty;
         return response.Result.ToString().Replace("\"", "");
      }

      #endregion

      #region Private helpers

      /// <summary>
      /// Check if message is html.
      /// It checks only begining of the message.
      /// </summary>
      /// <param name="message">message</param>
      /// <returns>true - message has html doctype, 
      /// but it can't be valid html document</returns>
      private bool IsHtml(string message)
      {
         const int minimalHtmlRecognizeLength = 14;
         const string htmlHeaderStart = "<!doctype html";

         if (message.Length > minimalHtmlRecognizeLength)
         {
            var str = message.Substring(0, minimalHtmlRecognizeLength).ToLower();
            if (str.Equals(htmlHeaderStart))
            {
               return true;
            }
         }
         return false;
      }

      /// <summary>
      /// Call web api get method. 
      /// </summary>
      /// <param name="apiPath">route (e.g. api/Bat2s)</param>
      /// <returns>returned object</returns>
      private async Task<object> Get(string apiPath)
      {
         _lastErrorMsg = "";
         var uri = CheckServerAddress(_server);
         if (uri == null)
         {
            _lastErrorMsg = Properties.Resources.ErrorBadUri;
            return null;
         }
         using (var client = new HttpClient())
         {
            client.BaseAddress = uri;
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MediaJson));
            client.DefaultRequestHeaders.Authorization = CreateHeaderAuthentication(AuthBasic);
            try
            {
               var res = client.GetAsync(apiPath).Result;
               var receiveData = await res.Content.ReadAsStringAsync();

               if (!res.IsSuccessStatusCode)
               {
                  _lastErrorMsg = Properties.Resources.ErrorBadRequest;
                  return null;
               }
               if (IsHtml(receiveData))
               {
                  _lastErrorMsg = Properties.Resources.ErrorBadLogin;
                  return null;
               }
               return receiveData;
            }
            catch (Exception)
            {
               _lastErrorMsg = Properties.Resources.ErrorConnectionFailed;
               return null;
            }
         }
      }

      /// <summary>
      /// Call web api post method.
      /// </summary>
      /// <param name="body">sending data</param>
      /// <param name="apiPath">route (e.g. api/Bat2s)</param>
      /// <returns>true - data was saved on address</returns>
      private async Task<bool> Post(object body, string apiPath)
      {
         _lastErrorMsg = "";
         var uri = CheckServerAddress(_server);
         if (uri == null)
         {
            _lastErrorMsg = Properties.Resources.ErrorBadUri;
            return false;
         }
         using (var client = new HttpClient())
         {
            client.BaseAddress = uri;
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MediaJson));
            client.DefaultRequestHeaders.Authorization = CreateHeaderAuthentication(AuthBasic);

            CreatedItemsCount = -1;
            var response = await client.PostAsJsonAsync(apiPath, body);
            if (response.IsSuccessStatusCode)
            {
               var text = response.Content.ReadAsStringAsync().Result;
               if (text.Equals("null") || IsHtml(text)) return false;

               CreatedItemsCount = NumberOfRecords(text);
               return true;
            }
            return false;
         }
      }

      /// <summary>
      /// Parse response text, check if is collection and return 
      /// number of items.
      /// </summary>
      /// <param name="text">message</param>
      /// <returns>number of items at message</returns>
      private int NumberOfRecords(string text)
      {
         if (text[0] == '[' && text[text.Length - 1] == ']')
         {
            var deserColl = JsonConvert.DeserializeObject<IEnumerable<object>>(text);
            return deserColl.Count();
         }
         return 1;   // text is not collection, contains only 1 record
      }

      /// <summary>
      /// Create header authentication.
      /// </summary>
      /// <param name="type">authentication type</param>
      /// <returns>authentication header</returns>
      private AuthenticationHeaderValue CreateHeaderAuthentication(string type)
      {
         if (type.Equals(AuthBasic))
         {
            return new AuthenticationHeaderValue(AuthBasic,
               Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(
                  string.Format("{0}:{1}", _userName, _password)))
               );
         }
         return null;
      }

      private static Uri CheckServerAddress(string address)
      {
         address = address.EndsWith("/") ? address : address + "/";
         if (!Uri.IsWellFormedUriString(address, UriKind.RelativeOrAbsolute))
         {
            return null;
         }
         Uri uri;
         try
         {
            uri = new Uri(address);
         }
         catch (Exception)
         {
            uri = null;
         }

         if (uri != null && Uri.CheckSchemeName(uri.Scheme) && uri.Scheme == Uri.UriSchemeHttp) return uri;

         // try to add http in front of address
         address = "http://" + address;
         try
         {
            uri = new Uri(address);
         }
         catch (Exception)
         {
            if (uri == null || !Uri.CheckSchemeName(uri.Scheme) || uri.Scheme != Uri.UriSchemeHttp)
            {
               return null;
            }
         }

         return uri;
      }

      #endregion
   }
}
