﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using Microsoft.CSharp.RuntimeBinder;

namespace Services.Bindings
{
   /// <summary>
   /// Attribute for specifying <see cref="Binding"/>s reguirements
   /// </summary>
   [AttributeUsage(AttributeTargets.Class)]
   public class BindingRequirementAttribute : Attribute, IServiceBehavior,IEndpointBehavior
   {
      #region Attribute parameters

      /// <summary>
      /// Reliability is required
      /// </summary>
      public bool ReliabilityRequired { get; set; }

      /// <summary>
      /// Transaction flow is enabled
      /// </summary>
      public bool TransactionFlowEnabled { get; set; }

      #endregion
      
      #region IServiceBehavior implementation

      public void Validate(ServiceDescription description, ServiceHostBase host)
      {
         IEndpointBehavior endpointBehavior = this;
         foreach (var endpoint in description.Endpoints)
         {
            endpointBehavior.Validate(endpoint);
         }
      }

      public void AddBindingParameters(ServiceDescription description, ServiceHostBase host,
         Collection<ServiceEndpoint> endpoints, BindingParameterCollection parameters)
      {
      }

      public void ApplyDispatchBehavior(ServiceDescription description, ServiceHostBase host)
      {
      }

      #endregion

      #region IEndpointBehavior implementation

      public void AddBindingParameters(ServiceEndpoint serviceEndpoint, BindingParameterCollection bindingParameters)
      {
      }

      public void ApplyClientBehavior(ServiceEndpoint serviceEndpoint, ClientRuntime behavior)
      {
      }

      public void ApplyDispatchBehavior(ServiceEndpoint serviceEndpoint, EndpointDispatcher endpointDispatcher)
      {
      }

      public void Validate(ServiceEndpoint endpoint)
      {
         if (TransactionFlowEnabled)
         {
            ValidateTransactionFlow(endpoint);
         }
         if (ReliabilityRequired)
         {
            ValidateReliability(endpoint);
         }
      }

      #endregion

      #region Private helpers

      private static void ValidateReliability(ServiceEndpoint endpoint)
      {
         if (endpoint.Binding is NetNamedPipeBinding) //Inherently reliable
         {
            return;
         }
         if (endpoint.Binding is WSDualHttpBinding) //Always reliable
         {
            return;
         }
         if (endpoint.Binding is NetTcpBinding)
         {
            var tcpBinding = endpoint.Binding as NetTcpBinding;
            if (tcpBinding.ReliableSession.Enabled)
            {
               return;
            }
         }
         if (endpoint.Binding is WSHttpBindingBase)
         {
            var wsBinding = endpoint.Binding as WSHttpBindingBase;
            if (wsBinding.ReliableSession.Enabled)
            {
               return;
            }
         }
         throw new InvalidOperationException(
            "BindingRequirementAttribute requires reliability enabled, but binding for the endpoint with address "
               + endpoint.Address
               + " for contract " + endpoint.Contract.ContractType + " has does not support reliability or has it disabled");
      }

      private static void ValidateTransactionFlow(ServiceEndpoint endpoint)
      {
         Exception exception =
            new InvalidOperationException(
               "BindingRequirementAttribute requires transaction flow enabled, but binding for the endpoint with address "
               + endpoint.Address
               + " for contract " + endpoint.Contract.ContractType + " has it disabled");

         foreach (var operation in endpoint.Contract.Operations)
         {
            var attribute = operation.Behaviors.Find<TransactionFlowAttribute>();
            if (attribute != null)
            {
               if (attribute.Transactions == TransactionFlowOption.Allowed)
               {
                  try
                  {
                     dynamic binding = endpoint.Binding;
                     if (binding.TransactionFlow == false)
                     {
                        throw exception;
                     }
                  }
                  catch (RuntimeBinderException)
                  {
                     throw new InvalidOperationException(
                        "BindingRequirementAttribute requires transaction flow enabled, but binding for the endpoint with contract " +
                        endpoint.Contract.ContractType + " does not support transaction flow");
                  }
               }
            }
         }
      }

      #endregion

   }
} 





