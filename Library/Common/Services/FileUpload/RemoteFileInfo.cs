using System;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Services.FileUpload
{
   [MessageContract]
   [KnownType(typeof(ProtectedRemoteFileInfo))]
   public class RemoteFileInfo : IDisposable
   {
      [MessageBodyMember(Order = 1)]
      public Stream InputStream;

      public void Dispose()
      {
         if (InputStream == null) return;

         InputStream.Close();
         InputStream = null;
      }
   }
}