using System.ServiceModel;

namespace Services.FileUpload
{
   [MessageContract]
   public class StatusInfo
   {
      [MessageBodyMember] public string Status;
   }
}