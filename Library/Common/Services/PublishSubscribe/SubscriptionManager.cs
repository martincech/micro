﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Services.Bindings;
using Services.PublishSubscribe.Contracts;
using Utilities.Extensions;

namespace Services.PublishSubscribe
{
   /// <summary>
   /// SubscriptionManager for set of subscribtors in Subscribe/Publish patern.
   /// </summary>
   /// <typeparam name="T">Type of service to use subscription manager for</typeparam>
   [BindingRequirement(TransactionFlowEnabled = true)]
   public abstract class SubscriptionManager<T> where T : class
   {
      #region Private Fields

      /// <summary>
      /// Transient storage for transient subscribers. 
      /// Key is name of method from <see cref="T"/>, Value are subscribted client of type <see cref="T"/>
      /// </summary>
      internal static readonly Dictionary<string, List<T>> MTransientStore;

      internal static string ContractName
      {
         get { return typeof (T).Name; }
      }

      private static bool ServesPersistent { get; set; }

      #endregion

      #region Constructors

      static SubscriptionManager()
      {
         MTransientStore = new Dictionary<string, List<T>>();
         var methods = GetOperations();
         Action<string> insert = methodName => MTransientStore.Add(methodName, new List<T>());

         methods.ForEach(insert);

         try
         {
            using (var db = new PublishSubscribeDataContext())
            {
               db.Database.CreateIfNotExists();
            }
            ServesPersistent = true;
         }
         catch (Exception)
         {
            ServesPersistent = false;
         }
      }

      /// <summary>
      /// Create subscribtion manager service
      /// </summary>
      protected SubscriptionManager()
      {
// ReSharper disable SuspiciousTypeConversion.Global
         Debug.Assert((this is ISubscriptionContract) || (this is IPersistentSubscriptionContract),
// ReSharper restore SuspiciousTypeConversion.Global
         string.Format("SubscriptionManager<{0}>: Failed to inherit from {1} or {2}",
            typeof (T).Name,
            typeof (ISubscriptionContract).Name, typeof (IPersistentSubscriptionContract).Name));
      }

      #endregion

      #region Transient subscriptions management

      #region Private

      /// <summary>
      /// Add new subscriber to list of all subscribers for specified operation
      /// </summary>
      /// <param name="subscriber">Subscriber to be added to list</param>
      /// <param name="eventOperation">operation to be subscribted</param>
      [MethodImpl(MethodImplOptions.Synchronized)]
      private static void AddTransient(T subscriber, string eventOperation)
      {
         var list = MTransientStore[eventOperation];
         if (list.Contains(subscriber))
         {
            return;
         }
         list.Add(subscriber);
      }

      /// <summary>
      /// Remove subscriber from list of existing subsribers for specified operation
      /// </summary>
      /// <param name="subscriber">Subscriber to be removed from list</param>
      /// <param name="eventOperation">operation to be unsubscribted </param>
      [MethodImpl(MethodImplOptions.Synchronized)]
      internal static void RemoveTransient(T subscriber, string eventOperation)
      {
         var list = MTransientStore[eventOperation];
         list.Remove(subscriber);
      }

      [MethodImpl(MethodImplOptions.Synchronized)]
      internal static IEnumerable<T> GetTransientList(string eventOperation)
      {
         if (!MTransientStore.ContainsKey(eventOperation)) return new T[] {};
         var list = MTransientStore[eventOperation];
         return list.ToArray();
      }

      #endregion

      /// <summary>
      /// Subscribe for all events
      /// </summary>
      public void SubscribeAll()
      {
         var methods = GetOperations();
         foreach (var method in methods)
         {
            Subscribe(method);
         }
      }

      /// <summary>
      /// Subscribe client for operation callback.
      /// </summary>
      /// <param name="eventOperation">event to be subscribted or empty for all events </param>
      [MethodImpl(MethodImplOptions.Synchronized)]
      public void Subscribe(string eventOperation)
      {
         VerifyOperation(eventOperation);
         var subscriber = OperationContext.Current.GetCallbackChannel<T>();
         if (String.IsNullOrEmpty(eventOperation) == false)
         {
            AddTransient(subscriber, eventOperation);
         }
         else
         {
            var methods = GetOperations();
            Action<string> addTransient = methodName => AddTransient(subscriber, methodName);
            methods.ForEach(addTransient);
         }
      }

      /// <summary>
      /// Unsubscribe client for operation callback.
      /// </summary>
      /// <param name="eventOperation">event to be unsubscribted or empty for all events </param>
      [MethodImpl(MethodImplOptions.Synchronized)]
      public void Unsubscribe(string eventOperation)
      {
         VerifyOperation(eventOperation);
         var subscriber = OperationContext.Current.GetCallbackChannel<T>();
         if (String.IsNullOrEmpty(eventOperation) == false)
         {
            RemoveTransient(subscriber, eventOperation);
         }
         else
         {
            var methods = GetOperations();
            Action<string> removeTransient = methodName => RemoveTransient(subscriber, methodName);
            methods.ForEach(removeTransient);
         }
      }

      /// <summary>
      /// Unsubscribe for all events
      /// </summary>
      [MethodImpl(MethodImplOptions.Synchronized)]
      public void UnsubscribeAll()
      {
         var methods = GetOperations();
         foreach (var method in methods)
         {
            Unsubscribe(method);
         }
      }

      #endregion

      #region Persistend subscriptions management

      #region Private

      /// <summary>
      /// Check database whether it contains some records.
      /// </summary>
      /// <param name="address">address to be checked</param>
      /// <param name="eventsContract">contract name to be checked</param>
      /// <param name="eventOperation">operation name to be checked</param>
      /// <returns>true/false</returns>
      private static bool ContainsPersistent(string address, string eventsContract, string eventOperation)
      {
         using (var dataContext = new PublishSubscribeDataContext())
         {
            try
            {
               return
                  dataContext.Subscribers.Any(
                     item => item.Address == address && item.Contract == eventsContract && item.Operation == eventOperation);
            }
            catch (Exception e)
            {
               Console.WriteLine(e);
               throw;
            }
         }
      }

      /// <summary>
      /// Adds record to database.
      /// </summary>
      /// <param name="address">address of client</param>
      /// <param name="eventsContract">contract to be added</param>
      /// <param name="eventOperation">operation to be added</param>
      private static void AddPersistent(string address, string eventsContract, string eventOperation)
      {
         if (ContainsPersistent(address, eventsContract, eventOperation))
         {
            return;
         }
         using (var dataContext = new PublishSubscribeDataContext())
         {
            dataContext.Subscribers.Add(new Subscriber
            {
               Address = address,
               Operation = eventOperation,
               Contract = eventsContract
            });
            dataContext.SaveChanges();
         }
      }

      /// <summary>
      /// Remove records from database
      /// </summary>
      /// <param name="address">address of client</param>
      /// <param name="eventsContract">contract to be removed</param>
      /// <param name="eventOperation">operation to be removed</param>
      private static void RemovePersistent(string address, string eventsContract, string eventOperation)
      {
         using (var dataContext = new PublishSubscribeDataContext())
         {
            var removedSubscribers =
               from subscriber in dataContext.Subscribers
               where
                  subscriber.Address == address && subscriber.Contract == eventsContract &&
                  subscriber.Operation == eventOperation
               select subscriber;
            foreach (var removedSubscriber in removedSubscribers)
            {
               dataContext.Subscribers.Remove(removedSubscriber);
            }
            dataContext.SaveChanges();
         }
      }

      /// <summary>
      /// Convert Database representation of subscribers to WCF transferable representation
      /// </summary>
      /// <param name="subscribers">list of db objects</param>
      /// <returns>list of wcf objects</returns>
      private static IEnumerable<PersistentSubscription> Convert(IEnumerable<Subscriber> subscribers)
      {
         return subscribers.Select(subscriber => new PersistentSubscription {Address = subscriber.Address, Operation = subscriber.Operation}).ToList();
      }

      /// <summary>
      /// Get addresses subscribers subscribted to specific contract and operations
      /// </summary>
      /// <param name="eventOperation">operation name</param>
      /// <returns>addresses of subscribers</returns>
      private static IEnumerable<string> SubscribersToOperation(string eventOperation)
      {
         IEnumerable<string> subscribers;

         using (var dataContext = new PublishSubscribeDataContext())
         {
            subscribers = (from subscriber in dataContext.Subscribers
               where subscriber.Contract == ContractName && subscriber.Operation == eventOperation
               select subscriber.Address).ToArray();
         }

         return subscribers.ToArray();
      }

      internal static IEnumerable<T> GetPersistentList(string eventOperation)
      {
         if (!ServesPersistent) return new List<T>();
         var addresses = SubscribersToOperation(eventOperation).ToList();

         var subscribers = new List<T>(addresses.Count);

         foreach (var address in addresses)
         {
            var binding = GetBindingFromAddress(address);
            var factory = new ChannelFactory<T>(binding, new EndpointAddress(address));
            var proxy = factory.CreateChannel();
            subscribers.Add(proxy);
         }
         return subscribers;
      }

      #endregion

      /// <summary>
      /// Get list of all subscribers
      /// </summary>
      /// <returns>subscribers</returns>
      internal static IEnumerable<PersistentSubscription> GetSubscribers()
      {
         if (!ServesPersistent) return new List<PersistentSubscription>();
         IEnumerable<Subscriber> subscribers;

         using (var dataContext = new PublishSubscribeDataContext())
         {
            subscribers = (
               from subscriber in dataContext.Subscribers
               where subscriber.Contract == ContractName
               select subscriber).ToArray();
         }
         return Convert(subscribers);
      }

      /// <summary>
      /// Get addresses subscribers subscribted to specific contract and operations
      /// </summary>
      /// <param name="eventOperation">operation name</param>
      /// <returns>addresses of subscribers</returns>
      internal static IEnumerable<string> GetSubscribersToOperation(string eventOperation)
      {
         return !ServesPersistent ? new List<string>() : SubscribersToOperation(eventOperation);
      }

      /// <summary>
      /// Get all subscribtions from specific address
      /// </summary>
      /// <param name="address">address string</param>
      /// <returns>all existing subscribtions</returns>
      internal static IEnumerable<PersistentSubscription> GetAllSubscribersFromAddress(string address)
      {
         if (!ServesPersistent) return new List<PersistentSubscription>();
         VerifyAddress(address);

         IEnumerable<Subscriber> subscribers;
         using (var dataContext = new PublishSubscribeDataContext())
         {
            subscribers = (from subscriber in dataContext.Subscribers
               where subscriber.Address == address &&
                     subscriber.Contract == ContractName
               select subscriber).ToArray();
         }
         return Convert(subscribers);
      }

      /// <summary>
      /// Subscribe for all events
      /// </summary>
      /// <param name="address">Address where should events be sended</param>
      [OperationBehavior(TransactionScopeRequired = true)]
      public void SubscribeAll(string address)
      {
         var methods = GetOperations();
         foreach (var method in methods)
         {
            Subscribe(address, method);
         }
      }

      /// <summary>
      /// Permanently subscribe client from address to specific contract and operation.
      /// </summary>
      /// <param name="address">address of client</param>
      /// <param name="eventOperation">operation from contract to be subscribted</param>
      [OperationBehavior(TransactionScopeRequired = true)]
      public void Subscribe(string address, string eventOperation)
      {
         VerifyAddress(address);
         VerifyOperation(eventOperation);
         VerifyPersistency();
         if (String.IsNullOrEmpty(eventOperation) == false)
         {
            AddPersistent(address, ContractName, eventOperation);
         }
         else
         {
            var methods = GetOperations();
            Action<string> addPersistent = methodName => AddPersistent(address, ContractName, methodName);
            methods.ForEach(addPersistent);
         }
      }


      /// <summary>
      /// Permanently unsubscribe client from address from specific contract and operation.
      /// </summary>
      /// <param name="address">address of client</param>
      /// <param name="eventOperation">operation from contract to be unsubscribted</param>
      [OperationBehavior(TransactionScopeRequired = true)]
      public void Unsubscribe(string address, string eventOperation)
      {
         VerifyAddress(address);
         VerifyOperation(eventOperation);
         VerifyPersistency();

         if (String.IsNullOrEmpty(eventOperation) == false)
         {
            RemovePersistent(address, ContractName, eventOperation);
         }
         else
         {
            var methods = GetOperations();
            Action<string> removePersistent = methodName => RemovePersistent(address, ContractName, methodName);
            methods.ForEach(removePersistent);
         }
      }


      /// <summary>
      /// Unsubscribe for all events
      /// </summary>
      /// <param name="address">Address where should events be sended</param>
      [OperationBehavior(TransactionScopeRequired = true)]
      public void UnsubscribeAll(string address)
      {
         var methods = GetOperations();
         foreach (var method in methods)
         {
            Unsubscribe(address, method);
         }
      }
      #endregion

      #region Helper methods

      /// <summary>
      /// Verify addres for valid format and for valid type of binding
      /// /// Exception is thrown when verification failed.
      /// </summary>
      /// <param name="address">address to be verified</param>
      /// /// <exception cref="T:System.ArgumentException">Invalid format of address.</exception>
      /// /// /// <exception cref="T:System.ArgumentOutOfRangeException">Unsuporting address type.</exception>
      private static void VerifyAddress(string address)
      {
         if (!Uri.IsWellFormedUriString(address, UriKind.Absolute))
         {
            throw new ArgumentException("Invalid address format specification");
         }
         if (address.StartsWith("http:") || address.StartsWith("https:"))
         {
            return;
         }
         if (address.StartsWith("net.tcp:"))
         {
            return;
         }
         if (address.StartsWith("net.pipe:"))
         {
            return;
         }
         if (address.StartsWith("net.msmq:"))
         {
            return;
         }
         throw new ArgumentOutOfRangeException("address", "Unsupported protocol specified");
      }

      /// <summary>
      /// Verify event operation name, whether contract contains this name.
      /// Exception is thrown when verification failed.
      /// </summary>
      /// <param name="eventOperation">operation to be verified</param>
      /// <exception cref="T:System.InvalidOperationException">When peration is not valid.</exception>
      private static void VerifyOperation(string eventOperation)
      {
         if (GetOperations().All(op => op != eventOperation))
         {
            throw new InvalidOperationException(string.Format("{0} is not valid operation of {1} contract",
               eventOperation,
               ContractName));
         }
      }

      private static void VerifyPersistency()
      {
         if (!ServesPersistent)
         {
            throw new InvalidOperationException(
               string.Format("Operation cannot be performed, permanent storage for service is unavailable!"));
         }
      }

      /// <summary>
      /// Create binding from address
      /// </summary>
      /// <param name="address"></param>
      /// <returns></returns>
      internal static Binding GetBindingFromAddress(string address)
      {
         if (address.StartsWith("http:") || address.StartsWith("https:"))
         {
            var binding = new WSHttpBinding {ReliableSession = {Enabled = true}, TransactionFlow = true};
            return binding;
         }
         if (address.StartsWith("net.tcp:"))
         {
            var binding = new NetTcpBinding {ReliableSession = {Enabled = true}, TransactionFlow = true};
            return binding;
         }
         if (address.StartsWith("net.pipe:"))
         {
            var binding = new NetNamedPipeBinding {TransactionFlow = true};
            return binding;
         }
         if (address.StartsWith("net.msmq:"))
         {
            var binding = new NetMsmqBinding {Security = {Mode = NetMsmqSecurityMode.None}};
            return binding;
         }
         Debug.Assert(false, "Unsupported binding specified");
         return null;
      }
      
      /// <summary>
      /// Returns list of names of available operations for <see cref="T"/>. 
      /// </summary>
      /// <returns>list of names</returns>
      private static IEnumerable<string> GetOperations()
      {         
         return typeof(T).GetMethodsR().Select(method => method.Name);
      }

      
      #endregion
   }

   /// <summary>
   /// Data contract for persistent subscribers
   /// </summary>
   public struct PersistentSubscription
   {
      /// <summary>
      /// Address of subscriber
      /// </summary>
      public string Address { get; set; }

      /// <summary>
      /// Subscribted operation
      /// </summary>
      public string Operation { get; set; }
   }
}