#pragma once
#include <windows.h>
#include <winerror.h>
#include <setupapi.h>
#include <difxapi.h>

namespace Native{
   public enum class WinErrors
   {
      ErrorSuccess = ERROR_SUCCESS,
      ErrorInvalidFunction = ERROR_INVALID_FUNCTION,
      ErrorFileNotFound = ERROR_FILE_NOT_FOUND,
      ErrorPathNotFound = ERROR_PATH_NOT_FOUND,
      ErrorTooManyOpenFiles = ERROR_TOO_MANY_OPEN_FILES,
      ErrorAccessDenied = ERROR_ACCESS_DENIED,
      ErrorBadEnvironment = ERROR_BAD_ENVIRONMENT,
      ErrorCantAccessFile = ERROR_CANT_ACCESS_FILE,
      ErrorFilenameExcedRange = ERROR_FILENAME_EXCED_RANGE,
      ErrorInstallFailure = ERROR_INSTALL_FAILURE,
      ErrorInvalidCatalogData = ERROR_INVALID_CATALOG_DATA,
      ErrorInvalidName = ERROR_INVALID_NAME,
      ErrorInvalidParameter = ERROR_INVALID_PARAMETER,
      ErrorNoDeviceId = ERROR_NO_DEVICE_ID,
      ErrorNoMoreItems = ERROR_NO_MORE_ITEMS,
      ErrorNoSuchDevinst = ERROR_NO_SUCH_DEVINST,
      ErrorOutOfMemory = ERROR_OUTOFMEMORY,
      ErrorSharingViolation = ERROR_SHARING_VIOLATION,
      ErrorSignatureOsattributeMismatch = ERROR_SIGNATURE_OSATTRIBUTE_MISMATCH,
      ErrorUnsupportedType = ERROR_UNSUPPORTED_TYPE,
      ErrorCertEExpired = CERT_E_EXPIRED,
      ErrorCertEUntrusted = CERT_E_UNTRUSTEDROOT,
      ErrorCertEWrongUsage = CERT_E_WRONG_USAGE,
      ErrorCryptEFileError = CRYPT_E_FILE_ERROR,
      ErrorTrustENosignature = TRUST_E_NOSIGNATURE,
      ErrorInWow64 = ERROR_IN_WOW64
   };

   public class WinConstants{
      static const Int32 DriverPackageRepair = DRIVER_PACKAGE_REPAIR;
      static const Int32 DriverPackageSilent = DRIVER_PACKAGE_SILENT;
      static const Int32 DriverPackageForce = DRIVER_PACKAGE_FORCE;
      static const Int32 DriverPackageOnlyIfDevicePresent = DRIVER_PACKAGE_ONLY_IF_DEVICE_PRESENT;
      static const Int32 DriverPackageLegacyMode = DRIVER_PACKAGE_LEGACY_MODE;
      static const Int32 DriverPackageDeleteFiles = DRIVER_PACKAGE_DELETE_FILES;
   };
}

