#include "Lock.h"
#include "WinErrors.h"
using namespace System;
using namespace System::Threading;
using namespace Native;

Lock::Lock(Object ^pObject) : lockObject(pObject){
   Monitor::Enter(lockObject);
}
