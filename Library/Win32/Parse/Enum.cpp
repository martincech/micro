//******************************************************************************
//
//   Enum.cpp     Enumeration services
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include "Enum.h"
#include <string.h>
#include <stdio.h>

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------

Enum::Enum()
{
   _textList   = 0;
   _base       = 0;
   _count      = 0;
   _ignoreCase = NO;
} // Enum

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------

Enum::Enum( const char *textList, int base)
// initialize enumeration with <textList>, first string code is <base>
{
   _textList   = 0;
   _base       = 0;
   _count      = 0;
   _ignoreCase = NO;
   setList( textList, base);
} // Enum

//------------------------------------------------------------------------------
//  Destructor
//------------------------------------------------------------------------------

Enum::~Enum()
// destructor
{
   if( _textList){
      delete [] _textList;
   }
} // ~Enum

//------------------------------------------------------------------------------
//  Set list
//------------------------------------------------------------------------------

void Enum::setList( const char *textList, int base)
// set enumeration with <textList>, , first string code is <base>
{
   // remove old definition :
   if( _textList){
      delete [] _textList;
   }
   // initialize with new list :
   _base       = base;
   _ignoreCase = NO;
   // check for item count
   int         itemCount;
   const char *p;
   p         = textList;
   itemCount = 0;
   while( *p != '\0'){
      p = strchr( p, '\0');            // search for string terminator
      p++;                             // next character after terminator
      itemCount++;                     // strings count
   }
   _count = itemCount;                 // remember items count
   // duplicate items string :
   size_t size = p - textList + 1;
   char *list = new char[ size];
   memcpy( list, textList, size);
   _textList = list;
} // setList

//------------------------------------------------------------------------------
//  To string
//------------------------------------------------------------------------------

const char *Enum::toString( int code)
// returns string for <code>
{
   if( !hasCode( code)){
      return( "");
   }
   code -= _base;                      // remove base
   const char *p;
   p = _textList;
   while( code--){
      p = strchr( p, '\0');            // search for string terminator
      p++;                             // next character after terminator
   }
   return( p);
} // toString

//------------------------------------------------------------------------------
//  To code
//------------------------------------------------------------------------------

int Enum::toCode( const char *text)
// returns code for <text>
{
   int code;
   code = findString( text);
   if( code < 0){
      return( _base);                  // return secure value
   }
   return( code + _base);              // add base
} // toCode

//------------------------------------------------------------------------------
//  Has string
//------------------------------------------------------------------------------

TYesNo Enum::hasString( const char *text)
// check for <text> available
{
   return( findString( text) >= 0);
} // hasString

//------------------------------------------------------------------------------
//  Has code
//------------------------------------------------------------------------------

TYesNo Enum::hasCode( int code)
// check for <code> validity
{
   code -= _base;                     // remove base
   if( code < 0){
      return( NO);                    // code less than base
   }
   if( code > _count - 1){
      return( NO);                    // code over items count
   }
   return( YES);
} // hasCode

//------------------------------------------------------------------------------
//  Items count
//------------------------------------------------------------------------------

int Enum::itemsCount()
// returns items count
{
   return( _count);
} // itemsCount

//------------------------------------------------------------------------------
//  Ignore case
//------------------------------------------------------------------------------

void Enum::ignoreCase( TYesNo ignore)
// ignore case sesitivity for text item search
{
   _ignoreCase = ignore;
} // ignoreCase

//******************************************************************************

//------------------------------------------------------------------------------
//  Find string
//------------------------------------------------------------------------------

int Enum::findString( const char *text)
// returns <text> code or -1 if not found
{
   const char *p;
   int         index;
   p     = _textList;
   index = 0;
   while( *p != '\0'){
      if( _ignoreCase){
         if( striequ( text, p)){
            return( index);
         }
      } else {
         if( strequ( text, p)){
            return( index);
         }
      }
      p = strchr( p, '\0');            // search for string terminator
      p++;                             // next character after terminator
      index++;                         // next item
   }
   return( -1);
} // findString
