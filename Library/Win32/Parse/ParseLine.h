//******************************************************************************
//
//   ParseLine.h  Simple line parsing utility
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef PARSELINE_H
#define PARSELINE_H

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

class ParseLine
{
public:

   static void hexadecimal( byte *data, const char *line, int *size, int maximumSize);
   // Parse hexadecimal <line>, returns <data> with <size>

   static void escapeString( byte *data, const char *line, int *size);
   // Parse <line> with C-like escape characters, returns <data> with <size>

}; // ParseLine;

#endif // PARSELINE_H
