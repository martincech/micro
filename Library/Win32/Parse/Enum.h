//******************************************************************************
//
//   Enum.h       Enumeration services
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef ENUM_H
#define ENUM_H

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

/* text list is single string :

const char textList[] =
   "Case One\0"
   "Case Two\0"
   "Case Last\0"
;

*/

//------------------------------------------------------------------------------

class Enum
{
public :
   Enum();
   Enum( const char *textList, int base = 0);
   // initialize enumeration with <textList>, first string code is <base>

   ~Enum();
   // destructor

   void setList( const char *textList, int base = 0);
   // set enumeration with <textList>, , first string code is <base>

   const char *toString( int code);
   // returns string for <code>

   int toCode( const char *text);
   // returns code for <text>

   TYesNo hasString( const char *text);
   // check for <text> available

   TYesNo hasCode( int code);
   // check for <code> validity

   int itemsCount();
   // returns items count

   void ignoreCase( TYesNo ignore);
   // ignore case sesitivity for text item search

private :
   const char *_textList;
   int         _base;
   int         _count;
   TYesNo      _ignoreCase;

   int findString( const char *text);
   // returns <text> code or -1 if not found
}; // Enum

//------------------------------------------------------------------------------

#endif // ENUM_H
