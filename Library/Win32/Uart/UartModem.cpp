//******************************************************************************
//
//   UartModem.cpp  Serial port modem support
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Uart/UartModem.h"
#include <string.h>

#define BUFFER_MAX   1022

static WinUart *_Uart = NULL;
static CrtDump *_Dump;
static char    *_RxBuffer;
static int      _RxBufferSize;
static byte     _ModemStatus;        // fake last operation status

//------------------------------------------------------------------------------
// Setup
//------------------------------------------------------------------------------

void UartModemSetup( WinUart *Uart, CrtDump *Dump)
// Initialize with <Uart> and optional <Dump>
{
   _Uart = Uart;
   _Dump = Dump;
} // UartModemSetup

//------------------------------------------------------------------------------
//   Initialize
//------------------------------------------------------------------------------

void UartModemInit( void)
// Initialize
{
   _ModemStatus = UART_MODEM_IDLE;
} // UartModemInit

void UartModemDeinit( void)
// Deinitialiize
{
   _ModemStatus = UART_MODEM_IDLE;
}

//------------------------------------------------------------------------------
//   Baud rate
//------------------------------------------------------------------------------

void UartModemBaudRateSet( int BaudRate)
// Set modem <BaudRate>
{
   if(_Uart == NULL){return;}
   _Uart->setBaudRate( BaudRate);
} // UartModemBaudRateSet

//------------------------------------------------------------------------------
//   Rx timeout
//------------------------------------------------------------------------------

void UartModemTimeoutSet( int Timeout)
// Set receive <Timeout>
{
   if(_Uart == NULL){return;}
   _Uart->setReceiveTimeout( Timeout, 100);
} // UartModemTimeoutSet

//------------------------------------------------------------------------------
//   Rx Buffer
//------------------------------------------------------------------------------

void UartModemBufferSet( void *Buffer, int Size)
// Set Rx <Buffer> with <Size>
{
   _RxBuffer     = (char *)Buffer;
   _RxBufferSize = Size;
} // UartModemBufferSet

//------------------------------------------------------------------------------
//   Send
//------------------------------------------------------------------------------

void UartModemSend( char *String, int Size)
// Send <String> with optional <Size>
{
   if(_Uart == NULL){ return; }
   if( Size == 0){
      Size = strlen( String);
   }
   _Uart->flush();
   if( ! _Uart->send( String, Size)){
      if( _Dump){
         _Dump->puts( "Unable send\n");
      }
      return;
   }
   if( _Dump){
      _Dump->show( CrtDump::TX, String, Size);
   }
   _ModemStatus = UART_MODEM_SEND_DONE;
} // UartModemSend

//------------------------------------------------------------------------------
//   Receive
//------------------------------------------------------------------------------
#include <QtConcurrentRun>
#include "Multitasking/Multitasking.h"

static int ReceiveThread(int Size){
   return _Uart->receive( _RxBuffer, Size);
}

TYesNo UartModemReceive( int Size)
// Receive buffer with <Size> (may be zero - receive up to timeout)
{
int RxSize;

   if(_Uart == NULL){return NO;}
   if( Size == 0){
      Size = _RxBufferSize - 1;
   }
   _ModemStatus = UART_MODEM_RECEIVE_ACTIVE;
   QFuture<int> recThread = QtConcurrent::run(ReceiveThread, Size);
   while(recThread.isRunning()){
      MultitaskingReschedule();
   }
   RxSize = recThread.result();

   if( RxSize == 0){
      _ModemStatus = UART_MODEM_RECEIVE_ERROR;
      if( _Dump){
         _Dump->puts( "Receive timeout\n");
      }
      return( NO);
   }
   _RxBuffer[ RxSize] = '\0';
   _ModemStatus = UART_MODEM_IDLE;
   if( _Dump){
      _Dump->show( CrtDump::RX, _RxBuffer, strlen( _RxBuffer));
   }
   return( YES);
} // UartModemReceive

//------------------------------------------------------------------------------
//   Modem status
//------------------------------------------------------------------------------

TUartModemStatus UartModemStatus( void)
// Check for last operation status
{
TUartModemStatus OldStatus;

   // check for idle :
   if( _ModemStatus == UART_MODEM_IDLE){
      return( _ModemStatus);
   }
   // fake status processing :
   OldStatus = _ModemStatus;
   if( _ModemStatus == UART_MODEM_SEND_DONE){
      _ModemStatus = UART_MODEM_RECEIVE_DONE;
   } else {
      _ModemStatus = UART_MODEM_IDLE;
   }
   return( OldStatus);
} // UartModemStatus
