//******************************************************************************
//
//   WinNative.cpp Uart native communication
//   Version 1.0   (c) VEIT electronics
//
//******************************************************************************

#include "WinNative.h"

#define LoggerTx( frame, size)       if( _logger) _logger->show( CrtDump::TX, frame, size)
#define LoggerRx( frame, size)       if( _logger) _logger->show( CrtDump::RX, frame, size)
#define LoggerGarbage( frame, size)  if( _logger) _logger->show( CrtDump::GARBAGE, frame, size)
#define LoggerReport( txt)           if( _logger) _logger->puts( txt)

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------

WinNative::WinNative() : WinUart()
// Constructor
{
   _logger        = 0;
   _rxTimeoutShow = YES;
} // WinNative

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------

WinNative::~WinNative()
// Destructor
{
   close();
} //~WinNative

//------------------------------------------------------------------------------
//  Set logger
//------------------------------------------------------------------------------

void WinNative::setLogger( CrtDump *logger)
// Set data visualisation <logger>
{
   _logger = logger;
} // setLogger

//------------------------------------------------------------------------------
//  Rx timeout log
//------------------------------------------------------------------------------

void WinNative::setRxTimeoutShow( TYesNo enable)
// Enable Rx timeout logging
{
   _rxTimeoutShow = enable;
} // setRxTimeoutShow

//------------------------------------------------------------------------------
// Send
//------------------------------------------------------------------------------

TYesNo WinNative::send( TNativeAddress address, const void *data, int size)
// Send <data> with <size> to <address>
{
   if( size > NATIVE_DATA_SIZE){
      LoggerReport( "Too big Tx size\n");
      return( NO);
   }
   TNativeFrame frame;
   frame.Leader    = NATIVE_LEADER;
   frame.Address   = address;
   frame.Size      = size;
   frame.HeaderCrc = headerCrc( &frame);
   memcpy( &frame.Data, data, size);
   // data Crc :
   word crc = dataCrc( &frame);
   frame.Data[ size]     = (byte)(crc & 0xFF);
   frame.Data[ size + 1] = (byte)(crc >> 8);
   int frameSize = NativeFrameSize( size);
   flush();
   if( !WinUart::send( (const void *)&frame, frameSize)){
      LoggerReport( "Tx error (unaccesible COM?)\n");
      return( NO);
   }
   LoggerTx( &frame, frameSize);
   return( YES);
} // send

//------------------------------------------------------------------------------
// Receive
//------------------------------------------------------------------------------

TYesNo WinNative::receive( TNativeAddress *address, void *data, int *size)
// Receive <data> with <size> from <address>
{
   TNativeFrame frame;
   int headerSize = NATIVE_DATA_OFFSET;
   int rxSize;
   // receive header :
   rxSize = WinUart::receive( &frame, headerSize);
   if( rxSize != headerSize){
      if( !rxSize){
         if( _rxTimeoutShow){
            LoggerReport( "Rx timeout\n");
         } // else hide message
         return( NO);                  // Rx timeout
      }
      LoggerGarbage( &frame, rxSize);
      return( NO);                     // short header
   }
   // check for header data :
   if( frame.Leader != NATIVE_LEADER){
      LoggerGarbage( &frame, rxSize);
      LoggerReport( "Wrong leader\n");
      return( NO);                     // wrong leader
   }
   TNativeHeaderCrc hCrc = headerCrc( &frame);
   if( frame.HeaderCrc != hCrc){
      LoggerGarbage( &frame, rxSize);
      LoggerReport( "Wrong header CRC\n");
      return( NO);                     // corrupted header
   }
   if( frame.Size > NATIVE_DATA_SIZE){
      LoggerGarbage( &frame, rxSize);
      LoggerReport( "Wrong data size\n");
      return( NO);                     // wrong size
   }
   // read payload :
   int payloadSize = frame.Size + sizeof( TNativeCrc);
   rxSize = WinUart::receive( frame.Data, payloadSize);
   if( rxSize != payloadSize){
      LoggerGarbage( &frame, headerSize + payloadSize);
      LoggerReport( "Short payload data (Rx timeout ?)\n");
      return( NO);                     // data timeout
   }
   TNativeCrc dCrc = dataCrc( &frame);
   TNativeCrc fCrc = NativeCrc( &frame);
   if( fCrc != dCrc){
      LoggerGarbage( &frame, headerSize + payloadSize);
      LoggerReport( "Wrong data CRC\n");
      return( NO);                     // wrong CRC
   }
   memcpy( data, frame.Data, frame.Size);
   *size    = frame.Size;
   *address = frame.Address;
   LoggerRx( &frame, headerSize + payloadSize);
   if( frame.Size == 0){
      LoggerReport( "Zero data size\n");
   }
   return( YES);
} // receive

//******************************************************************************

//------------------------------------------------------------------------------
// Header Crc
//------------------------------------------------------------------------------

TNativeHeaderCrc WinNative::headerCrc( TNativeFrame *frame)
{
   return( NativeHeaderCrc( frame));
} // headerCrc

//------------------------------------------------------------------------------
// Header Crc
//------------------------------------------------------------------------------

TNativeCrc WinNative::dataCrc( TNativeFrame *frame)
{
   byte *p    = frame->Data;
   int   size = frame->Size;
   TNativeCrc crc = 0;
   while( size--){
      crc += *p;
      p++;
   }
   return( (TNativeCrc)-crc);
} // dataCrc
