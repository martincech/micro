//*****************************************************************************
//
//   FlashFake.h   Fake Flash memory
//   Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __FlashFake_H__
   #define __FlashFake_H__

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

#include "Flash/NorFlash.h"

//-----------------------------------------------------------------------------
// Device data
//-----------------------------------------------------------------------------

#define FLASH_WRITE_SIZE   256                // maximum block write size
#define FLASH_ERASE_SIZE   (4  * 1024)        // sector erase size
#define FLASH_SIZE         (8  * 1024 * 1024) // 64Mbits

#ifdef __cplusplus
   extern "C" {
#endif
//-----------------------------------------------------------------------------
// Fake SPI
//-----------------------------------------------------------------------------

byte _FlashByteRead( void);
// Fake SPI Read byte

void _FlashByteWrite( byte Value);
// Fake SPI write byte <Value>

byte *_FlashArray( void);
// Returns array address

//-----------------------------------------------------------------------------
#ifdef __cplusplus
   }
#endif

#endif
