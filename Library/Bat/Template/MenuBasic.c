//******************************************************************************
//
//   Menu$NAME$.c  $DESCRIPTION$
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "Menu$NAME$.h"
#include "Gadget/DMenu.h"         // Display menu
#include "Str.h"                  // Strings

$INCLUDE$

static DefMenu( $NAME$Menu)
$STRINGS$
EndMenu()

typedef enum {
$ENUM$
} E$NAME$Menu;

//------------------------------------------------------------------------------
//  Menu $NAME$
//------------------------------------------------------------------------------

void Menu$NAME$( void)
// Menu $LNAME$
{
TMenuData MData;

   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_$UNAME$, $NAME$Menu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
$CASE$
      }
   }
} // Menu$NAME$
