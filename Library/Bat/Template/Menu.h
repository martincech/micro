//******************************************************************************
//
//   Menu$NAME$.h  $DESCRIPTION$
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Menu$NAME$_H__
   #define __Menu$NAME$_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

$INCLUDE$

void Menu$NAME$( void);
// Menu $LNAME$

#endif
