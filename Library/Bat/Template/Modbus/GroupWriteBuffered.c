   // buffered registers
   if( R >=  $FIRST$ && R <= $LAST$){
     i = _GetRegisterMapping( R);
     if( i < 0){ return NO; }
     WriteValuesBuffer.$GROUP_CCNAME$Change[ i >> 3] |= 1 << (7 - (i & 7));
     WriteValuesBuffer.$GROUP_CCNAME$[ i] = D;
     return YES;
   }
