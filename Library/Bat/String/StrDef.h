//******************************************************************************
//
//  StrDef.h       Text string definitions
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __StrDef_H__
   #define __StrDef_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

typedef const char *TUniStr;           // language independent string handle

#if STR_LANGUAGE_COUNT > 1
   typedef char const *const TAllStrings[][ STR_LANGUAGE_COUNT];    // dictionary
#else
   typedef char const *const TAllStrings[];                         // single list
#endif

extern  TAllStrings AllStrings;        // global dictionary

const char *StrGet( TUniStr Str);
// Get translated string

void StrTrimRight( char *String);
// Remove right spaces

void StrSetWidth( char *String, int Width);
// Extend text with spaces, up to <Width>

#endif
