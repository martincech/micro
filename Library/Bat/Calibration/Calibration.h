//******************************************************************************
//
//   Calibration.h  Calibration utility
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Calibration_H__
   #define __Calibration_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeightDef_H__
   #include "Weight/WeightDef.h"
#endif


TYesNo CalibrationLoad( void);
// Load calibration from EEPROM

void CalibrationSave( void);
// Save calibration to EEPROM

void CalibrationUpdate( TRawWeight RawZero, TRawWeight RawRange, TWeightGauge Range);
// Update calibration : ADC value <RawZero> of zero, <RawRange> of physical <Range>

TWeightGauge CalibrationRangeGet( void);
// Returns physical range

TYesNo CalibrationInversion( void);
// Returns bridge polarity inversion

TWeightGauge CalibrationWeight( TRawWeight RawValue);
// Calculate physical weight of <RawValue>

TRawWeight CalibrationRawWeight( TWeightGauge Value);
// Calculate raw weight of physical <Value>

#endif
