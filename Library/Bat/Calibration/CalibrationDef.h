//******************************************************************************
//
//   CalibrationDef.h  Calibration definition
//   Version 0.0       (c) VEIT Electronics
//
//******************************************************************************

#ifndef __CalibrationDef_H__
   #define __CalibrationDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __WeightDef_H__
   #include "Weight/WeightDef.h"
#endif

typedef word TCalibrationCrc;          // calibration checksum

typedef struct {
   TRawWeight      Offset;             // zero calibration
   TRawWeight      Factor;             // range calibration
   TWeightGauge    Range;              // physical range
   byte            Inversion;          // reversed bridge polarity
   byte            _Spare;
   TCalibrationCrc Crc;                // record checksum
} TCalibration;

#endif
