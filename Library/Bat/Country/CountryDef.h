//******************************************************************************
//
//   CountryDef.h   Country specific locale definitions
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#ifndef __CountryDef_H__
   #ifndef _MANAGED
   #define __CountryDef_H__
   #endif

#include "Country/CountrySet.h"  // country, language, code page enums

// Date formats :
#ifdef _MANAGED
namespace Bat2Library{
   public enum class DateFormatE{
#else
typedef enum {
#endif
   DATE_FORMAT_DDMMYYYY,
   DATE_FORMAT_MMDDYYYY,
   DATE_FORMAT_YYYYMMDD,
   DATE_FORMAT_YYYYDDMM,

   DATE_FORMAT_DDMMMYYYY,
   DATE_FORMAT_MMMDDYYYY,
   DATE_FORMAT_YYYYMMMDD,
   DATE_FORMAT_YYYYDDMMM,
#ifndef _MANAGED
   _DATE_FORMAT_COUNT
} EDateFormat;
#else
   };
}
#endif



// Time formats :
#ifdef _MANAGED
namespace Bat2Library{
   public enum class TimeFormatE{
#else
typedef enum {
#endif
   TIME_FORMAT_24,
   TIME_FORMAT_12,
#ifndef _MANAGED
   _TIME_FORMAT_COUNT
} ETimeFormat;
#else
   };
}
#endif

// Time suffix :
#ifdef _MANAGED
namespace Bat2Library{
   public enum class TimeSuffixE{
#else
typedef enum {
#endif
   TIME_AM,
   TIME_PM
#ifndef _MANAGED
} ETimeSuffix;
#else
   };
}
#endif

#ifndef _MANAGED
// Locales descriptor :
typedef struct {
   byte CodePage;           // ECodePage
   byte DateFormat;         // date format
   char DateSeparator1;     // date separator character 1
   char DateSeparator2;     // date separator character 2
   byte TimeFormat;         // time format
   char TimeSeparator;      // time separator character
   byte DaylightSavingType; // daylight saving type
   byte _Spare;
} TLocaleData;

// Country descriptor :
typedef struct {
   byte        Country;      // country code ECountry
   byte        Language;     // language code ELanguage
   word        _Spare;
   TLocaleData Locale;       // locale settings
} TCountry;
#endif

#ifdef _MANAGED
   #undef _MANAGED
   #include "CountryDef.h"
   #define _MANAGED
#endif

#endif//__CountryDef_H__
