//******************************************************************************
//                                                                            
//   MenuCountry.c   Country configuration menu
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "MenuCountry.h"
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DInput.h"        // Display input value
#include "Gadget/DEdit.h"         // Display edit value
#include "Config/Config.h"        // Configuration
#include "Str.h"                  // Strings

#define TIME_SEPARATOR  ":-"
#define DATE_SEPARATOR  ".-/"

static DefMenu( CountryMenu)
   STR_COUNTRY,
   STR_LANGUAGE,
   STR_DATE_FORMAT,
   STR_TIME_FORMAT,
   STR_DAYLIGHT_SAVING_TIME,
EndMenu()

typedef enum {
   MI_COUNTRY,
   MI_LANGUAGE,
   MI_DATE_FORMAT,
   MI_TIME_FORMAT,
   MI_DAYLIGHT_SAVING_TIME
} ECountryMenu;

// Local functions :

static void CountryParameters(  int Index, int y, void *UserData);
// Menu country parameters

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuCountry( void)
// Country configuration menu
{
TMenuData MData;
int       i;
char      Buff[ 2];

   DMenuClear( MData);
   forever {
      if( !DMenu( STR_COUNTRY, CountryMenu, CountryParameters, 0, &MData)){
         ConfigCountrySave();
         return;
      }
      switch( MData.Item){
         case MI_COUNTRY :
            i = Country.Country;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_COUNTRY, _COUNTRY_COUNT)){
               break;
            }
            CountrySet( i);
            break;

         case MI_LANGUAGE :
            i = CountryLanguage();
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_LANGUAGE, _LNG_COUNT)){
               break;
            }
            CountryLanguageSet( i);
            break;

         case MI_DATE_FORMAT :
            i = Country.Locale.DateFormat;
            if( !DInputEnum( STR_DATE_FORMAT, STR_ENTER_DATE_FORMAT, &i, ENUM_DATE_FORMAT, _DATE_FORMAT_COUNT)){
               break;
            }
            Country.Locale.DateFormat = i;
            Buff[ 0] = Country.Locale.DateSeparator1;
            Buff[ 1] = '\0';
            if( !DInputTextL( STR_DATE_FORMAT, STR_ENTER_DATE_SEPARATOR1, Buff, 1, YES, DATE_SEPARATOR)){
               break;
            }
            Country.Locale.DateSeparator1 = Buff[ 0];
            // empty string as space :
            if( Country.Locale.DateSeparator1 == '\0'){
               Country.Locale.DateSeparator1 = ' ';
            }
            Buff[ 0] = Country.Locale.DateSeparator2;
            Buff[ 1] = '\0';
            if( !DInputTextL( STR_DATE_FORMAT, STR_ENTER_DATE_SEPARATOR2, Buff, 1, YES, DATE_SEPARATOR)){
               break;
            }
            Country.Locale.DateSeparator2 = Buff[ 0];
            // empty string as space :
            if( Country.Locale.DateSeparator2 == '\0'){
               Country.Locale.DateSeparator2 = ' ';
            }
            break;

         case MI_TIME_FORMAT :
            i = Country.Locale.TimeFormat;
            if( !DInputEnum( STR_TIME_FORMAT, STR_ENTER_TIME_FORMAT, &i, ENUM_TIME_FORMAT, _TIME_FORMAT_COUNT)){
               break;
            }
            Country.Locale.TimeFormat = i;
            Buff[ 0] = Country.Locale.TimeSeparator;
            Buff[ 1] = '\0';
            if( !DInputTextL( STR_TIME_FORMAT, STR_ENTER_TIME_SEPARATOR, Buff, 1, YES, TIME_SEPARATOR)){
               break;
            }
            Country.Locale.TimeSeparator = Buff[ 0];
            // empty string as space :
            if( Country.Locale.TimeSeparator == '\0'){
               Country.Locale.TimeSeparator = ' ';
            }
            break;

         case MI_DAYLIGHT_SAVING_TIME :
            i = Country.Locale.DaylightSavingType;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_DAYLIGHT_SAVING_TYPE, _DAYLIGHT_SAVING_COUNT)){
               break;
            }
            Country.Locale.DaylightSavingType = i;
            break;
      }
   }
} // MenuCountry

//******************************************************************************

//------------------------------------------------------------------------------
//   Parameters
//------------------------------------------------------------------------------

static void CountryParameters(  int Index, int y, void *UserData)
// Menu country parameters
{
   switch( Index){
      case MI_COUNTRY :
         DLabelEnum( Country.Country, ENUM_COUNTRY, DMENU_PARAMETERS_X, y);
         break;

      case MI_LANGUAGE :
         DLabelEnum( Country.Language, ENUM_LANGUAGE, DMENU_PARAMETERS_X, y);
         break;

      case MI_DATE_FORMAT :
         DLabelEnum( Country.Locale.DateFormat, ENUM_DATE_FORMAT, DMENU_PARAMETERS_X, y);
         break;

      case MI_TIME_FORMAT :
         DLabelEnum( Country.Locale.TimeFormat, ENUM_TIME_FORMAT, DMENU_PARAMETERS_X, y);
         break;

      case MI_DAYLIGHT_SAVING_TIME :
         DLabelEnum( Country.Locale.DaylightSavingType, ENUM_DAYLIGHT_SAVING_TYPE, DMENU_PARAMETERS_X, y);
         break;
   }
} // CountryParameters
