//******************************************************************************
//
//  xTime.c        putchar based display date & time
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "xTime.h"
#include "Country/Country.h"
#include "Console/xprint.h"

#ifndef TIME_SUFFIX_AM
   #define TIME_SUFFIX_AM "am"
#endif
#ifndef TIME_SUFFIX_PM
   #define TIME_SUFFIX_PM "pm"
#endif

//------------------------------------------------------------------------------
//   Time
//------------------------------------------------------------------------------

void xTime( TPutchar *xputchar, UTimeGauge Now)
// Display time only
{
UTime Time;
char  Separator;
const char *PmStr;

   uTime( &Time, Now);
   Separator = CountryTimeSeparator();
   switch( CountryTimeFormat()){
      case TIME_FORMAT_24 :
         xprintf( xputchar, "%02d%c%02d%c%02d", Time.Hour, Separator, Time.Min, Separator, Time.Sec);
         break;

      case TIME_FORMAT_12 :
         if( Time.Hour >= 12){
            PmStr = xTimeSuffix( TIME_PM);
         } else {
            PmStr = xTimeSuffix( TIME_AM);
         }
         if( Time.Hour == 0){
            // 00:00 .. 00:59
            Time.Hour = 12;           // 12:xx am
         } else if( Time.Hour > 12){
            // 13:00 .. 23:59
            Time.Hour -= 12;
         } // else 01:00 .. 12.59
         xprintf( xputchar, "%02d%c%02d%c%02d%s", Time.Hour, Separator,
                                                  Time.Min,  Separator,
                                                  Time.Sec,  PmStr);
         break;
   }
} // xTime

//------------------------------------------------------------------------------
//   Time width
//------------------------------------------------------------------------------

int xTimeWidth( void)
// Returns character width of time
{
   switch( CountryTimeFormat()){
      case TIME_FORMAT_24 :
         return( 8);
      case TIME_FORMAT_12 :
         return( 10);
   }
   return( 0);
} // xTimeWidth

//------------------------------------------------------------------------------
//   Short Time
//------------------------------------------------------------------------------

void xTimeShort( TPutchar *xputchar, UTimeGauge Now)
// Display time without seconds
{
UTime Time;
char  Separator;
const char *PmStr;

   uTime( &Time, Now);
   Separator = CountryTimeSeparator();
   switch( CountryTimeFormat()){
      case TIME_FORMAT_24 :
         xprintf( xputchar, "%02d%c%02d", Time.Hour, Separator, Time.Min);
         break;

      case TIME_FORMAT_12 :
         if( Time.Hour > 12){
            Time.Hour -= 12;
            PmStr = xTimeSuffix( YES);
         } else {
            PmStr = xTimeSuffix( NO);
         }
         xprintf( xputchar, "%02d%c%02d%s", Time.Hour, Separator, Time.Min, PmStr);
         break;
   }
} // xTimeShort

//------------------------------------------------------------------------------
//   Time width
//------------------------------------------------------------------------------

int xTimeShortWidth( void)
// Returns character width of time
{
   switch( CountryTimeFormat()){
      case TIME_FORMAT_24 :
         return( 5);
      case TIME_FORMAT_12 :
         return( 7);
   }
   return( 0);
} // xTimeShortWidth

//------------------------------------------------------------------------------
//   Time suffix
//------------------------------------------------------------------------------

const char *xTimeSuffix( ETimeSuffix Suffix)
// Returns suffix by <Suffix> enum
{
   if( Suffix == TIME_AM){
      return( TIME_SUFFIX_AM);
   } else {
      return( TIME_SUFFIX_PM);
   }
} // xTimeSuffix

#ifdef DATE_YEAR_YYYY
//------------------------------------------------------------------------------
//   Date
//------------------------------------------------------------------------------

void xDate(TPutchar *xputchar, UDateGauge Now)
{
   xDateFormated(xputchar, Now, CountryDateFormat(), CountryDateSeparator1(), CountryDateSeparator2());
}

void xDateFormated( TPutchar *xputchar, UDateGauge Now, EDateFormat dateFormat, char Separator1, char Separator2)
// Display date only
{
UDate Date;
const char *StrMonth;
int   Year;

   uDate( &Date, Now);
   StrMonth   = xDateMonth( Date.Month);
   Year       = Date.Year + 2000;
   switch( dateFormat){
      case DATE_FORMAT_DDMMYYYY :
         xprintf( xputchar, "%02d%c%02d%c%04d", Date.Day, Separator1, Date.Month, Separator2, Year);
         break;
      case DATE_FORMAT_MMDDYYYY :
         xprintf( xputchar, "%02d%c%02d%c%04d", Date.Month, Separator1, Date.Day, Separator2, Year);
         break;
      case DATE_FORMAT_YYYYMMDD :
         xprintf( xputchar, "%04d%c%02d%c%02d", Year, Separator1, Date.Month, Separator2, Date.Day);
         break;
      case DATE_FORMAT_YYYYDDMM :
         xprintf( xputchar, "%04d%c%02d%c%02d", Year, Separator1, Date.Day, Separator2, Date.Month);
         break;

      case DATE_FORMAT_DDMMMYYYY :
         xprintf( xputchar, "%02d%c%s%c%04d", Date.Day, Separator1, StrMonth, Separator2, Year);
         break;
      case DATE_FORMAT_MMMDDYYYY :
         xprintf( xputchar, "%s%c%02d%c%04d", StrMonth, Separator1, Date.Day, Separator2, Year);
         break;
      case DATE_FORMAT_YYYYMMMDD :
         xprintf( xputchar, "%04d%c%s%c%02d", Year, Separator1, StrMonth, Separator2, Date.Day);
         break;
      case DATE_FORMAT_YYYYDDMMM :
         xprintf( xputchar, "%04d%c%02d%c%s", Year, Separator1, Date.Day, Separator2, StrMonth);
         break;
   }
} // xDate

//------------------------------------------------------------------------------
//   Date width
//------------------------------------------------------------------------------

int xDateWidth( void)
// Returns character width of date
{
   if( CountryDateFormat() >= DATE_FORMAT_DDMMMYYYY){
      return( 11);
   } else {
      return( 10);
   }
} // xDateWidth
#else // DATE_YEAR_YY
//------------------------------------------------------------------------------
//   Date
//------------------------------------------------------------------------------

void xDate(TPutchar *xputchar, UDateGauge Now)
{
   xDateFormated(xputchar, Now, CountryDateFormat(), CountryDateSeparator1(), CountryDateSeparator2());
}

void xDateFormated( TPutchar *xputchar, UDateGauge Now, EDateFormat dateFormat, char Separator1, char Separator2)
// Display date only
{
UDate Date;
const char *StrMonth;

   uDate( &Date, Now);

   StrMonth   = xDateMonth( Date.Month);
   switch( dateFormat){
      case DATE_FORMAT_DDMMYYYY :
         xprintf( xputchar, "%02d%c%02d%c%02d", Date.Day, Separator1, Date.Month, Separator2, Date.Year);
         break;
      case DATE_FORMAT_MMDDYYYY :
         xprintf( xputchar, "%02d%c%02d%c%02d", Date.Month, Separator1, Date.Day, Separator2, Date.Year);
         break;
      case DATE_FORMAT_YYYYMMDD :
         xprintf( xputchar, "%02d%c%02d%c%02d", Date.Year, Separator1, Date.Month, Separator2, Date.Day);
         break;
      case DATE_FORMAT_YYYYDDMM :
         xprintf( xputchar, "%02d%c%02d%c%02d", Date.Year, Separator1, Date.Day, Separator2, Date.Month);
         break;

      case DATE_FORMAT_DDMMMYYYY :
         xprintf( xputchar, "%02d%c%s%c%02d", Date.Day, Separator1, StrMonth, Separator2, Date.Year);
         break;
      case DATE_FORMAT_MMMDDYYYY :
         xprintf( xputchar, "%s%c%02d%c%02d", StrMonth, Separator1, Date.Day, Separator2, Date.Year);
         break;
      case DATE_FORMAT_YYYYMMMDD :
         xprintf( xputchar, "%02d%c%s%c%02d", Date.Year, Separator1, StrMonth, Separator2, Date.Day);
         break;
      case DATE_FORMAT_YYYYDDMMM :
         xprintf( xputchar, "%02d%c%02d%c%s", Date.Year, Separator1, Date.Day, Separator2, StrMonth);
         break;
   }
} // xDate

//------------------------------------------------------------------------------
//   Date width
//------------------------------------------------------------------------------

int xDateWidth( void)
// Returns character width of date
{
   if( CountryDateFormat() >= DATE_FORMAT_DDMMMYYYY){
      return( 9);
   } else {
      return( 8);
   }
} // xDateWidth
#endif // DATE_YEAR_YY

//------------------------------------------------------------------------------
//   Date time
//------------------------------------------------------------------------------

void xDateTime( TPutchar *xputchar, UDateTimeGauge Now)
// Display date and time
{
UDateGauge DateGauge;
UTimeGauge TimeGauge;

   // split date & time :
   DateGauge = uDateTimeDate( Now);
   TimeGauge = uDateTimeTime( Now);
   xDate( xputchar, DateGauge);
   (*xputchar)(' ');
   xTime( xputchar, TimeGauge);
} // xDateTime

//------------------------------------------------------------------------------
//   Date time width
//------------------------------------------------------------------------------

int xDateTimeWidth( void)
// Returns character width of date time
{
   return( xDateWidth() + xTimeWidth() + 1);
} // xDateTimeWidth

//------------------------------------------------------------------------------
//   Short Date time
//------------------------------------------------------------------------------

void xDateTimeShort( TPutchar *xputchar, UDateTimeGauge Now)
// Display date and time without seconds
{
UDateGauge DateGauge;
UTimeGauge TimeGauge;

   // split date & time :
   DateGauge = uDateTimeDate( Now);
   TimeGauge = uDateTimeTime( Now);
   xDate( xputchar, DateGauge);
   (*xputchar)(' ');
   xTimeShort( xputchar, TimeGauge);
} // xDateTimeShort

//------------------------------------------------------------------------------
//   Date time width
//------------------------------------------------------------------------------

int xDateTimeShortWidth( void)
// Returns character width of date time
{
   return( xDateWidth() + xTimeShortWidth() + 1);
} // xDateTimeShortWidth

//------------------------------------------------------------------------------
//   Date month
//------------------------------------------------------------------------------

const char _MonthShortcut[_MONTH_COUNT][XDATE_MONTH_SIZE + 1] = {
   "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

const char *xDateMonth( UMonth Month)
// Returns date shortcut
{
   return( _MonthShortcut[ Month - 1]); // January == 1
} // xDateMonth
