//******************************************************************************
//
//   Country.h     Country & locales utility
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#ifndef __Country_H__
   #define __Country_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __CountryDef_H__
   #include "Country/CountryDef.h"
#endif

//------------------------------------------------------------------------------

extern TCountry Country;
extern const TCountry CountryDefault;

//------------------------------------------------------------------------------

void CountrySet( int CountryCode);
// Fill country data by <CountryCode>

int CountryLanguage( void);
// Returns language code

void CountryLanguageSet( int Language);
// Set <Language> and code page

int CountryCodePage( void);
// Returns code page

int CountryDateFormat( void);
// Returns date format enum

char CountryDateSeparator1( void);
// Returns first date separator character

char CountryDateSeparator2( void);
// Returns second date separator character

int CountryTimeFormat( void);
// Returns time format enum

char CountryTimeSeparator( void);
// Returns time separator character

int CountryDaylightSavingType( void);
// Returns daylight saving type

#endif
