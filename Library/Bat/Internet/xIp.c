//******************************************************************************
//
//  xIp.c          putchar based display IP
//  Version 1.0    (c) Veit Electronics
//
//******************************************************************************

#include "xIp.h"
#include "Console/xprint.h"
#include "fnet_inet.h"
//------------------------------------------------------------------------------
//   IP
//------------------------------------------------------------------------------

void xIp( TPutchar *xputchar, fnet_ip4_addr_t Ip)
// Display Ip
{
byte *IpByte = (byte *) &Ip;
   xprintf( xputchar, "%3d.%3d.%3d.%3d", IpByte[0], IpByte[1], IpByte[2], IpByte[3]);
} // xIp

//------------------------------------------------------------------------------
//   IP short
//------------------------------------------------------------------------------

void xIpShort( TPutchar *xputchar, fnet_ip4_addr_t Ip)
// Display Ip short
{
char ip_str[FNET_IP4_ADDR_STR_SIZE];

   fnet_inet_ntoa(*((struct in_addr *)&Ip), ip_str);

   xprintf( xputchar, "%s", ip_str);
} // xIpShort
