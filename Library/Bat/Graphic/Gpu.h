//*****************************************************************************
//
//    Gpu.h         Graphic controller basic services
//    Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Gpu_H__
   #define __Gpu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef G_PLANES
   #error "Undefined G_PLANES count"
#endif

//---- graphical buffer descriptor :

#define GB_HEIGHT   ((G_HEIGHT + 7) / 8)       // graphical buffer byte height
#define GB_WIDTH    (((G_WIDTH + 7) / 8) * 8)  // graphical buffer pixel width

#if G_PLANES == 1

typedef struct {
   word MinX;                   // X writing boundary
   word MaxX;
   word MinY;                   // Y writing boundary
   word MaxY;
   byte Buffer[ GB_HEIGHT][ GB_WIDTH]; // size as 8 modulo
} TGraphicBuffer;

#else

typedef struct {
   word MinX;                   // X writing boundary
   word MaxX;
   word MinY;                   // Y writing boundary
   word MaxY;
   byte Buffer[ G_PLANES][ GB_HEIGHT][ GB_WIDTH]; // size as 8 modulo
} TGraphicBuffer;

#endif

#ifdef __cplusplus
   extern "C" {
#endif

extern TGraphicBuffer GBuffer;

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void GpuInit( void);
// Initialisation

void GpuFlush( void);
// Redraw buffer

void GpuShutdown( void);
// Shutdown

#ifdef __cplusplus
   }
#endif

#endif
