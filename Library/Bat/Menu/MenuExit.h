//******************************************************************************
//
//   MenuExit.h       Menu exit
//   Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuExit_H__
   #define __MenuExit_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif
   
typedef enum {
   MENU_EXIT_POWER_OFF = 1,
   MENU_EXIT_POWER_FAILURE,
   MENU_EXIT_BOOTLOADER,
   MENU_EXIT_REBOOT,
   MENU_EXIT_REMOTE,
   MENU_EXIT_REMOTE_EXIT,
   MENU_EXIT_TIMEOUT,
   _MENU_EXIT_LAST,
} EMenuExit;

#ifdef __cplusplus
   extern "C" {
#endif

extern byte _Code;

#ifdef __cplusplus
   }
#endif

#define MenuExitSet( Code)        _Code = Code
// Menu exit set

#define MenuExitGet()             _Code
// Menu exit get

#define MenuExitClear()           MenuExitSet(0)
// Menu exit clear



#endif