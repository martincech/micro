//******************************************************************************
//
//    DHistogram.h   Display histogram
//    Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __DHistogram_H__
   #define __DHistogram_H__

#ifndef __Histogram_H__
   #include "Statistic/Histogram.h"
#endif

TYesNo DHistogram( THistogram *Histogram,  int LowLimit, int HighLimit,
                   TStatisticUniformityRange UniformityRange);
// Displays histogram with <LowLimit>, <HighLimit> and <UniformityRange>
// markers. Returns YES on Enter

void DHistogramIcon( THistogram *Histogram, int x, int y);
// Displays histogram icon at <x,y>

void DHistogramIconSized( THistogram *Histogram, int x, int y, int width, int height);
// Displays histogram icon at <x,y> with maximum <width,height>

#endif
