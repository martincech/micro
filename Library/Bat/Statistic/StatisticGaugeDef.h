//*****************************************************************************
//
//    StatisticGaugeDef.h  Statistic internal data definition
//    Version 1.0          (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __StatisticGaugeDef_H__
  #define __StatisticGaugeDef_H__

//------------------------------------------------------------------------------
// Statistic internal representation
//------------------------------------------------------------------------------

// Internal descriptor :
typedef struct {
   TStatisticNumber XSuma;
   TStatisticNumber X2Suma;
   TStatisticNumber UniMin;
   TStatisticNumber UniMax;
   TStatisticCount  Count;
   TStatisticCount  CountIn;
   TStatisticCount  CountOut;
} TStatisticGauge;

#endif
