//*****************************************************************************
//
//    PutcharDef.h  putchar function prototype
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __PutcharDef_H__
   #define __PutcharDef_H__

typedef int TPutchar( int c);
// putchar function

#endif

