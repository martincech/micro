//*****************************************************************************
//
//    Fmt.h        Format specifications
//    Version 1.2  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Fmt_H__
   #define __Fmt_H__

// Flags for <Width> parameter :
#define FMT_NEGATIVE       0x8000           // negative signum for hexadecimal print
#define FMT_UNSIGNED       0x4000           // decimal number without signum
#define FMT_LEADING_0      0x2000           // fill with leading zeroes
#define FMT_PLUS           0x1000           // mandatory plus signum
#define FMT_WIDTH          0x000F           // mask - total number of digits
#define FMT_DECIMALS       0x00F0           // mask - number of fraction digits
#define FMT_DECIMALS_SHIFT 4                // fraction digits shift


// standard format for printf <Total> is total number of digits + 2 (signum and decimal dot),
// <Decimal> is fraction digits count

#define FmtPrecision( Total, Decimal) (((Total) & FMT_WIDTH)  | \
                                      (((Decimal) << FMT_DECIMALS_SHIFT) & FMT_DECIMALS))

#define FmtSetWidth(    Width)    ((Width) & FMT_WIDTH)
#define FmtSetDecimals( Decimal)  (((Decimal) << FMT_DECIMALS_SHIFT) & FMT_DECIMALS)

#define FmtGetWidth( w)           ((w) & FMT_WIDTH)
#define FmtGetDecimals( w)        (((w) & FMT_DECIMALS) >> FMT_DECIMALS_SHIFT)

#endif
