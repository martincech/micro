//******************************************************************************
//
//    WeightFlagDef.h  Weight flags definitions
//    Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#ifndef __WeightFlagDef_H__
   #define __WeightFlagDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#define WEIGHT_FLAG_NORMAL    0x00      // no additional flags
#define WEIGHT_FLAG_MALE      0x01      // male flag
#define WEIGHT_FLAG_FEMALE    0x02      // female flag
#define WEIGHT_FLAG_OVERFLOW  0x20      // weight too high
#define WEIGHT_FLAG_UNDERFLOW 0x40      // weight too low
#define WEIGHT_FLAG_STEP_DOWN 0x80      // step down

#define WEIGHT_FLAG_SEX       0x03      // male/female mask

#endif
