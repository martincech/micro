//******************************************************************************
//
//   DWeight.c    Display weight
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "DWeight.h"
#include "Weight/Weight.h"        // Weight internal representation
#include "Console/conio.h"        // putchar
#include "Graphic/Graphic.h"      // graphics
#include "Console/sputchar.h"     // string putchar
#include "Gadget/DLabel.h"        // Display label
#include "Weight/xWeight.h"       // weight formatting

#include "Str.h"                  // Strings
#include "Gadget/DInput.h"        // Display input value
#include "Gadget/DEdit.h"         // Display edit value

//------------------------------------------------------------------------------
// Weight
//------------------------------------------------------------------------------

void DWeight( TWeightGauge Weight)
// Display weight
{
   xWeight( cputchar, Weight);
} // DWeight

//------------------------------------------------------------------------------
// Weight left
//------------------------------------------------------------------------------

void DWeightLeft( TWeightGauge Weight)
// Display weight left aligned
{
   xWeightLeft( cputchar, Weight);
} // DWeightLeft

//------------------------------------------------------------------------------
// Units
//------------------------------------------------------------------------------

void DWeighingUnits( void)
// Display current units
{
   xWeighingUnits( cputchar);
} // DWeighingUnits

//------------------------------------------------------------------------------
// Weight & Units
//------------------------------------------------------------------------------

void DWeightWithUnits( TWeightGauge Weight)
// Display weight with units
{
   xWeightWithUnits( cputchar, Weight);
} // DWeightWithUnits

//------------------------------------------------------------------------------
// Weight & Units left
//------------------------------------------------------------------------------

void DWeightWithUnitsNarrow( int x, int y, TWeightGauge Weight)
// Display weight with units left aligned
{
char Buffer[ DWEIGHT_MAX_LENGTH];

   BWeightWithUnits( Buffer, Weight);
#ifndef DALIGN_RIGHT
   DLabel( Buffer, x, y);
#else
   DLabelRight( Buffer, x, y);
#endif
} // DWeightWithUnitsNarrow

void DWeightWithUnitsNarrowLeft( int x, int y, TWeightGauge Weight)
// Display weight with units left aligned
{
char Buffer[ DWEIGHT_MAX_LENGTH];

   BWeightWithUnits( Buffer, Weight);
   DLabel( Buffer, x, y);
} // DWeightWithUnitsNarrow

void DWeightWithUnitsNarrowRight( int x, int y, TWeightGauge Weight)
// Display weight with units right aligned
{
char Buffer[ DWEIGHT_MAX_LENGTH];

   BWeightWithUnits( Buffer, Weight);
   DLabelRight(Buffer, x, y);
}

void DWeightWithUnitsNarrowCentered( int x, int xWidth, int y, int yWidth, TWeightGauge Weight)
// Display weight with units left aligned and centered
{
char Buffer[ DWEIGHT_MAX_LENGTH];

   BWeightWithUnits( Buffer, Weight);
   DLabelCenter(Buffer, x, y, xWidth, yWidth);
}

//------------------------------------------------------------------------------
// Weight narrow
//------------------------------------------------------------------------------

void DWeightNarrow( int x, int y, TWeightGauge Weight)
// Display weight with narrow decimal dot
{
   GTextAt( x, y);
   GSetNumericPitch();
   xWeight( cputchar, Weight);
   GSetNormalPitch();
} // DWeightNarrow

//------------------------------------------------------------------------------
// Weight & Units to buffer
//------------------------------------------------------------------------------

void BWeightWithUnits( char *Buffer, TWeightGauge Weight)
// Format <Weight> with units to <Buffer>
{
   sputcharbuffer( Buffer);
   xWeightWithUnitsLeft( sputchar, Weight);
   sputchar( '\0');                    // string terminator
} // BWeightWithUnits

//------------------------------------------------------------------------------
// Input
//------------------------------------------------------------------------------

TYesNo DInputWeight( TUniStr Caption, TUniStr Text, TWeightGauge *Weight)
// Input weight
{
   return( DInputWeightRange( Caption, Text, Weight, 0, WeightUnits.Range));
} // DInputWeight

//------------------------------------------------------------------------------
// Edit
//------------------------------------------------------------------------------

TYesNo DEditWeight( int x, int y, TWeightGauge *Weight)
// Edit weight
{
   return( DEditWeightRange( x, y, Weight, 0, WeightUnits.Range));
} // DEditWeight

//------------------------------------------------------------------------------
// Input range
//------------------------------------------------------------------------------

TYesNo DInputWeightRange( TUniStr Caption, TUniStr Text, TWeightGauge *Weight, NWeight LoLimit, NWeight HiLimit)
// Input weight
{
NWeight Number;

   Number = WeightNumber( *Weight);
   if( !DInputNumber( Caption, Text, &Number, WeightUnits.Decimals, LoLimit, HiLimit,
                      ENUM_WEIGHT_UNITS + WeightUnits.Units)){
      return( NO);
   }
   *Weight = WeightGauge( Number);
   return( YES);
} // DInputWeightRange

//------------------------------------------------------------------------------
// Edit range
//------------------------------------------------------------------------------

TYesNo DEditWeightRange( int x, int y, TWeightGauge *Weight, NWeight LoLimit, NWeight HiLimit)
// Edit weight
{
NWeight Number;

   Number = WeightNumber( *Weight);
   if( !DEditNumber( x, y, &Number, WeightUnits.Decimals, LoLimit, HiLimit,
                        ENUM_WEIGHT_UNITS + WeightUnits.Units)){
      return( NO);
   }
   *Weight = WeightGauge( Number);
   return( YES);
} // DEditWeightRange

//------------------------------------------------------------------------------
// Weight width
//------------------------------------------------------------------------------

int DWeightWidth( void)
// Returns weight width (pixels)
{
   return( xWeightWidth() * GCharWidth());
} // DWeightWidth

//------------------------------------------------------------------------------
// Narrow Weight width
//------------------------------------------------------------------------------

int DWeightWidthNarrow( void)
// Returns weight width with narrow decimal dot (pixels)
{
   return( (xWeightWidth() - 1) * GCharWidth() + GTextWidth( "."));
} // DWeightWidthNarrow
