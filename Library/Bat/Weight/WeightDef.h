//*****************************************************************************
//
//    WeightDef.h  Weight definitions
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __WeightDef_H__
   #ifndef _MANAGED
   #define __WeightDef_H__
   #endif

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

//------------------------------------------------------------------------------
// Weighing units
//------------------------------------------------------------------------------

#ifdef _MANAGED
namespace Bat2Library{
   public enum class WeightUnitsE {
#else
typedef enum {
#endif
   WEIGHT_UNITS_KG,
   WEIGHT_UNITS_G,
   WEIGHT_UNITS_LB,
#ifndef _MANAGED
   _WEIGHT_UNITS_COUNT
} EWeightUnits;
#else
   };
}
#endif

// weighing capacity
#ifdef _MANAGED
namespace Bat2Library{
   public enum class WeightCapacityE{
#else
typedef enum {
#endif
   WEIGHT_CAPACITY_NORMAL,
   WEIGHT_CAPACITY_EXTENDED,
#ifndef _MANAGED
   _WEIGHT_CAPACITY_COUNT
} EWeightCapacity;
#else
   };
}
#endif

#ifndef _MANAGED
//------------------------------------------------------------------------------
// Data types
//------------------------------------------------------------------------------

// weight gauge constants :
#define WEIGHT_INVALID        0xFF800000
#define WEIGHT_SHORT_INVALID  0x8000

// internal representation :
typedef int32 TWeightGauge;       // Internal representation of the weight [0.1g]
typedef int16 TWeightShortGauge;  // Internal representation with reduced range [0.1g]

// ADC representation :
typedef int32 TRawWeight;         // ADC representation of the weight [LSB]
typedef int32 TSumWeight;         // ADC weight averaging sum [LSB]
typedef int64 TMulWeight;         // ADC weight multiplication [LSB]

// display representation :
typedef int32 NWeight;            // Current units weight [kg,g,lb]
typedef int16 NWeightShort;       // Current units short weight [kg,g,lb]

//------------------------------------------------------------------------------
// Limits
//------------------------------------------------------------------------------

// display representation :
#define NWEIGHT_MAX        99999
#define NWEIGHT_MIN        -9999
#define NWEIGHT_SHORT_MAX    999
#define NWEIGHT_SHORT_MIN   -999



//------------------------------------------------------------------------------
// Units descriptor
//------------------------------------------------------------------------------

typedef struct {
   NWeight      Range;                 // weighing capacity
   byte         Units;                 // weighing units EWeightUnits
   byte         Decimals;              // decimals count
   NWeightShort Division;              // scale division
   NWeightShort DivisionMax;           // division limit
   word         _Dummy;
} TWeightUnits;

//------------------------------------------------------------------------------
// Units constants
//------------------------------------------------------------------------------

#define WEIGHT_UNITS_DIVISION_MIN    1           // minimum division

#define WEIGHT_UNITS_KG_RANGE        30000       // basic range
#define WEIGHT_UNITS_KG_EXT_RANGE    50000       // extended range
#define WEIGHT_UNITS_KG_DECIMALS     3
#define WEIGHT_UNITS_KG_MAX_DIVISION 100
#define WEIGHT_UNITS_KG_DIVISION     WEIGHT_UNITS_DIVISION_MIN

#define WEIGHT_UNITS_G_RANGE         30000
#define WEIGHT_UNITS_G_EXT_RANGE     50000
#define WEIGHT_UNITS_G_DECIMALS      0
#define WEIGHT_UNITS_G_MAX_DIVISION  100
#define WEIGHT_UNITS_G_DIVISION      WEIGHT_UNITS_DIVISION_MIN

#define WEIGHT_UNITS_LB_RANGE        60000
#define WEIGHT_UNITS_LB_EXT_RANGE    99999
#define WEIGHT_UNITS_LB_DECIMALS     3
#define WEIGHT_UNITS_LB_MAX_DIVISION 100
#define WEIGHT_UNITS_LB_DIVISION     WEIGHT_UNITS_DIVISION_MIN

#define WEIGHT_UNITS_FACTOR          10          // internal representation coefficient

#endif // #ifndef _MANAGED

#ifdef _MANAGED
   #undef _MANAGED
   #include "WeightDef.h"
   #define _MANAGED
   namespace Bat2Library{
      public ref class WeightUnitsC abstract sealed{
      public:
         literal int32 DIVISION_MIN = WEIGHT_UNITS_DIVISION_MIN;

         literal NWeight KG_RANGE = WEIGHT_UNITS_KG_RANGE;
         literal NWeight KG_EXT_RANGE = WEIGHT_UNITS_KG_EXT_RANGE;
         literal byte KG_DECIMALS = WEIGHT_UNITS_KG_DECIMALS;
         literal NWeightShort KG_MAX_DIVISION = WEIGHT_UNITS_KG_MAX_DIVISION;
         literal NWeightShort KG_DIVISION = WEIGHT_UNITS_DIVISION_MIN;

         literal NWeight G_RANGE = WEIGHT_UNITS_G_RANGE;
         literal NWeight G_EXT_RANGE = WEIGHT_UNITS_G_EXT_RANGE;
         literal byte G_DECIMALS = WEIGHT_UNITS_G_DECIMALS;
         literal NWeightShort G_MAX_DIVISION = WEIGHT_UNITS_G_MAX_DIVISION;
         literal NWeightShort G_DIVISION = WEIGHT_UNITS_DIVISION_MIN;

         literal NWeight LB_RANGE = WEIGHT_UNITS_LB_RANGE;
         literal NWeight LB_EXT_RANGE = WEIGHT_UNITS_LB_EXT_RANGE;
         literal byte LB_DECIMALS = WEIGHT_UNITS_LB_DECIMALS;
         literal NWeightShort LB_MAX_DIVISION = WEIGHT_UNITS_LB_MAX_DIVISION;
         literal NWeightShort LB_DIVISION = WEIGHT_UNITS_DIVISION_MIN;

         literal int32 UNITS_FACTOR = WEIGHT_UNITS_FACTOR;

         static property int32   DivisionMin{int32 get(){return DIVISION_MIN;}}

         static property NWeight KgRange{NWeight get(){return KG_RANGE;}}
         static property NWeight KgExtRange{NWeight get(){return KG_EXT_RANGE;}}
         static property byte    KgDecimals{byte get(){return KG_DECIMALS;}}
         static property NWeightShort KgMaxDivision{NWeightShort get(){return KG_MAX_DIVISION;}}
         static property NWeightShort KgDivision{NWeightShort get(){return KG_DIVISION;}}

         static property NWeight GRange{NWeight get(){return G_RANGE;}}
         static property NWeight GExtRange{NWeight get(){return G_EXT_RANGE;}}
         static property byte    GDecimals{byte get(){return G_DECIMALS;}}
         static property NWeightShort GMaxDivision{NWeightShort get(){return G_MAX_DIVISION;}}
         static property NWeightShort GDivision{NWeightShort get(){return G_DIVISION;}}

         static property NWeight LbRange{NWeight get(){return LB_RANGE;}}
         static property NWeight LbExtRange{NWeight get(){return LB_EXT_RANGE;}}
         static property byte    LbDecimals{byte get(){return LB_DECIMALS;}}
         static property NWeightShort LbMaxDivision{NWeightShort get(){return LB_MAX_DIVISION;}}
         static property NWeightShort LbDivision{NWeightShort get(){return LB_DIVISION;}}

      };
   }
#endif


#endif
