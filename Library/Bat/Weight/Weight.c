//******************************************************************************
//                                                                            
//   Weight.c       Weight utility
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Weight.h"

// Basic units [g]
const TWeightUnits WeightUnitsG = {
   /* Range       */   WEIGHT_UNITS_G_RANGE,
   /* Units       */   WEIGHT_UNITS_G,
   /* Decimals    */   WEIGHT_UNITS_G_DECIMALS,
   /* Division    */   WEIGHT_UNITS_G_DIVISION,
   /* DivisionMax */   WEIGHT_UNITS_G_MAX_DIVISION,
   /* _Dummy      */   0
};

// Basic units [kg]
const TWeightUnits WeightUnitsKg = {
   /* Range       */   WEIGHT_UNITS_KG_RANGE,
   /* Units       */   WEIGHT_UNITS_KG,
   /* Decimals    */   WEIGHT_UNITS_KG_DECIMALS,
   /* Division    */   WEIGHT_UNITS_KG_DIVISION,
   /* DivisionMax */   WEIGHT_UNITS_KG_MAX_DIVISION,
   /* _Dummy      */   0
};

// Basic units [lb]
const TWeightUnits WeightUnitsLb = {
   /* Range       */   WEIGHT_UNITS_LB_RANGE,
   /* Units       */   WEIGHT_UNITS_LB,
   /* Decimals    */   WEIGHT_UNITS_LB_DECIMALS,
   /* Division    */   WEIGHT_UNITS_LB_DIVISION,
   /* DivisionMax */   WEIGHT_UNITS_LB_MAX_DIVISION,
   /* _Dummy      */   0
};

TWeightUnits WeightUnits;

//!!!
// Factor approximation for lb/kg recalculation (1lb = 0.453592kg)
// Factor is CAL_LB_MUL / (1 << CAL_LB_SHIFT) :
#define WEIGHT_LB_MUL      475626   // 0.453592 * (1024 * 1024) rounded
#define WEIGHT_LB_SHIFT    20       // 1024 * 1024
// Value = (TWeight)(((TMulWeight)Value << CAL_LB_SHIFT) / CAL_LB_MUL);

//------------------------------------------------------------------------------
// Initialize
//------------------------------------------------------------------------------

void WeightInit( void)
// Initialize
{
   WeightUnitsSet( WEIGHT_UNITS_KG);
} // WeightInit

//------------------------------------------------------------------------------
// Set units
//------------------------------------------------------------------------------

void WeightUnitsSet( byte Units)
// Set current <Units>
{
   switch( Units){
      case WEIGHT_UNITS_KG :
         WeightUnits = WeightUnitsKg;
         break;

      case WEIGHT_UNITS_G :
         WeightUnits = WeightUnitsG;
         break;

      case WEIGHT_UNITS_LB :
         WeightUnits = WeightUnitsLb;
         break;
   }
} // WeightUnitsSet

//------------------------------------------------------------------------------
// Weight number
//------------------------------------------------------------------------------

NWeight WeightNumber( TWeightGauge Weight)
// Returns current units number of <Weight>
{
NWeight Number;

   Number = Weight;
   switch( WeightUnits.Units){
      case WEIGHT_UNITS_KG :
      case WEIGHT_UNITS_G :
         break;

      case WEIGHT_UNITS_LB :
         Number = (NWeight)(((TMulWeight)Weight << WEIGHT_LB_SHIFT) / WEIGHT_LB_MUL);
         break;
   }
   // round  :
   if( Number >= 0){
      Number += WEIGHT_UNITS_FACTOR / 2;
   } else {
      Number -= WEIGHT_UNITS_FACTOR / 2;
   }
   Number /= WEIGHT_UNITS_FACTOR;
   return( Number);
} // WeightNumber

//------------------------------------------------------------------------------
// Weight division
//------------------------------------------------------------------------------

NWeight WeightNumberDivision( TWeightGauge Weight)
// Returns current units number of <Weight> rounded to division
{
NWeight Number;

   Number = WeightNumber( Weight);
   // round to nearest division :
   if( Number >= 0){
      Number += WeightUnits.Division / 2;
   } else {
      Number -= WeightUnits.Division / 2;
   }
   Number /= WeightUnits.Division;
   Number *= WeightUnits.Division;
   if( Number >= 0){
      if( Number > NWEIGHT_MAX){
         return( NWEIGHT_MAX);
      }
   } else {
      if( Number < NWEIGHT_MIN){
         return( NWEIGHT_MIN);
      }
   }
   return( Number);
} // WeightNumberDivision

//------------------------------------------------------------------------------
// Weight gauge
//------------------------------------------------------------------------------

TWeightGauge WeightGauge( NWeight Number)
// Returns internal representation of <Number> units
{
   switch( WeightUnits.Units){
      case WEIGHT_UNITS_KG :
      case WEIGHT_UNITS_G :
         return( Number * WEIGHT_UNITS_FACTOR);

      case WEIGHT_UNITS_LB :
         return(((TMulWeight)Number * WEIGHT_LB_MUL * WEIGHT_UNITS_FACTOR) / (1 << WEIGHT_LB_SHIFT));
   }
   return( 0);
} // WeightGauge

//------------------------------------------------------------------------------
//  Weight gauge division
//------------------------------------------------------------------------------

TWeightGauge WeightGaugeDivision( TWeightGauge Weight)
// Returns internal representation rounded to division
{
NWeight Number;

   Number = WeightNumberDivision( Weight);
   return( WeightGauge( Number));
} // WeightGaugeDivision

//------------------------------------------------------------------------------
//   Nice
//------------------------------------------------------------------------------

TWeightGauge WeightNice( TWeightGauge Weight)
// Returns nice number of <Weight> by current units
{
   return( Weight);
} // WeightNice
