//******************************************************************************
//                                                                            
//   MenuWeightUnits.h   Weighing units menu
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuWeightUnits_H__
   #define __MenuWeightUnits_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuWeightUnits( void);
// Menu weighing units

#endif
