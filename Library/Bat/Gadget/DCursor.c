//******************************************************************************
//                                                                            
//   DCursor.c     Display cursor utility
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "DCursor.h"
#include "DEvent.h"
#include "DLayout.h"                 // Display layout
#include "Graphic/Graphic.h"
#include "Str.h"                     // strings from project directory
#include "Fonts.h"                   // project fonts
#include "Bitmap.h"
#include "Sound/Beep.h"

// Local functions :

static void _FirstPage( TListCursor *Cursor);
// Move to first page

static void _LastPage( TListCursor *Cursor);
// Move to last page

//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

void DCursorInit( TListCursor *Cursor, dword Count, byte PageRows)
// Initialize <Cursor>
{
   Cursor->Count    = Count;
   Cursor->PageRows = PageRows;
   Cursor->UseGrid  = YES;
   _FirstPage( Cursor);
} // DCursorInit

//------------------------------------------------------------------------------
//   Row up
//------------------------------------------------------------------------------

TYesNo DCursorRowUp( TListCursor *Cursor)
// Move cursor row up
{
   if( Cursor->Row > 0){
      BeepKey();
      Cursor->Row--;
      return( YES);
   }
   return( DCursorPageUp( Cursor));
} // DCursorRowUp

//------------------------------------------------------------------------------
//   Row down
//------------------------------------------------------------------------------

TYesNo DCursorRowDown( TListCursor *Cursor)
// Move cursor row down
{
   if( Cursor->Row < Cursor->RowCount - 1){
      BeepKey();
      Cursor->Row++;
      return( YES);
   }
   return( DCursorPageDown( Cursor));
} // DCursorRowDown

//------------------------------------------------------------------------------
//   Page up
//------------------------------------------------------------------------------

TYesNo DCursorPageUp( TListCursor *Cursor)
// Move cursor page up
{
   // check for short page :
   if( Cursor->Count <= Cursor->PageRows){
      BeepError();
      Cursor->Page     = 0;
      Cursor->RowCount = Cursor->Count;
      return( NO);                          // one page only
   }
   // check for rollover :
   if( Cursor->Page == 0){
      // at first page, roll to last page
      BeepRollover();
      _LastPage( Cursor);
      return( YES);
   }
   // page up :
   BeepKey();
   Cursor->Page -= Cursor->PageRows;        // previous page
   if( Cursor->Count < Cursor->PageRows){
      Cursor->RowCount = Cursor->Count;
   } else {
      Cursor->RowCount = Cursor->PageRows;
   }
   Cursor->Row = Cursor->RowCount - 1;      // move cursor to last item
   Cursor->PageChanged = YES;
   return( YES);
} // DCursorPageUp

//------------------------------------------------------------------------------
//   Page down
//------------------------------------------------------------------------------

TYesNo DCursorPageDown( TListCursor *Cursor)
// Move cursor page down
{
int NextCount;

   // check for short page :
   if( Cursor->Count <= Cursor->PageRows){
      BeepError();
      Cursor->Page     = 0;
      Cursor->RowCount = Cursor->Count;
      return( NO);                          // one page only
   }
   // check for rollover :
   if( Cursor->Page + Cursor->PageRows >= Cursor->Count){
      // last visible page, roll to first page
      BeepRollover();
      _FirstPage( Cursor);
      return( YES);
   }
   // page down :
   BeepKey();
   Cursor->Page += Cursor->PageRows;        // move to next page
   NextCount     = Cursor->Count - Cursor->Page;
   if( NextCount >= Cursor->PageRows){
      Cursor->RowCount = Cursor->PageRows;  // whole page
   } else {
      Cursor->RowCount = NextCount;         // short page
   }
   Cursor->Row = 0;                         // move cursor to first item
   Cursor->PageChanged = YES;
   return( YES);
} // DCursorPageDown

//------------------------------------------------------------------------------
//   Page changed
//------------------------------------------------------------------------------

TYesNo DCursorPageChanged( TListCursor *Cursor)
// Test for current page changed
{
   if( !Cursor->PageChanged){
      return( NO);
   }
   Cursor->PageChanged = NO;
   Cursor->RowPrevious = DCURSOR_ROW_INVALID;    // force cursor row redraw
   return( YES);
} // DCursorPageChanged

//------------------------------------------------------------------------------
//   Row changed
//------------------------------------------------------------------------------

TYesNo DCursorRowChanged( TListCursor *Cursor)
// Test for current row changed
{
   if( Cursor->Row == Cursor->RowPrevious){
      return( NO);
   }
   return( YES);
} // DCursorRowChanged

//------------------------------------------------------------------------------
//   Row update
//------------------------------------------------------------------------------

void DCursorRowUpdate( TListCursor *Cursor)
// Set current row as updated
{
   Cursor->RowPrevious = Cursor->Row;
} // DCursorRowUpdate

//------------------------------------------------------------------------------
//   Page redraw
//------------------------------------------------------------------------------

void DCursorPageRedraw( TListCursor *Cursor)
// Force page redraw
{
   Cursor->PageChanged = YES;
} // DCursorPageRedraw

//------------------------------------------------------------------------------
//   Set count
//------------------------------------------------------------------------------

void DCursorCountSet( TListCursor *Cursor, dword Count)
// Update items count
{
int Rows;

   if( Cursor->Count == Count){
      return;                                    // same size
   }
   Cursor->Count = Count;                        // new size
   if( Cursor->Page + Cursor->Row < Cursor->Count){
      // cursor in range
      Rows = Cursor->Count - Cursor->Page;
      if( Rows > Cursor->PageRows){
         Rows = Cursor->PageRows;
      }
      Cursor->RowCount    = Rows;                // update page rows count
      Cursor->RowPrevious = DCURSOR_ROW_INVALID; // forget previous row
      Cursor->PageChanged = YES;                 // force redraw
      return;
   }
   _LastPage( Cursor);
} // DCursorCountSet

//------------------------------------------------------------------------------
//   Index
//------------------------------------------------------------------------------

dword DCursorIndex( TListCursor *Cursor)
// Returns index of cursor item
{
   return( Cursor->Page + Cursor->Row);
} // DCursorIndex

//------------------------------------------------------------------------------
//   Set index
//------------------------------------------------------------------------------

void DCursorIndexSet( TListCursor *Cursor, dword Index)
// Set <Index> as cursor item
{
int Page;
int Row;
int RowCount;

   if( Cursor->Count == 0){
      _FirstPage( Cursor);
      return;
   }
   Page     = DivCover( Index + 1, Cursor->PageRows); // pages count
   Page--;                                            // page index
   Page    *= Cursor->PageRows;                       // page start index
   Row      = Index % Cursor->PageRows;               // page row
   RowCount = Cursor->Count - Page;                   // get remaing items
   if( RowCount > Cursor->PageRows){
      RowCount = Cursor->PageRows;
   }
   Cursor->Page        = Page;
   Cursor->Row         = (byte)Row;
   Cursor->RowPrevious = DCURSOR_ROW_INVALID;
   Cursor->RowCount    = (byte)RowCount;
   Cursor->PageChanged = YES;                    // force redraw
} // DCursorIndexSet

//******************************************************************************

//------------------------------------------------------------------------------
//   First page
//------------------------------------------------------------------------------

static void _FirstPage( TListCursor *Cursor)
// Move to first page
{
   Cursor->Page        = 0;
   Cursor->Row         = 0;
   Cursor->RowPrevious = DCURSOR_ROW_INVALID;
   Cursor->RowCount    = Cursor->PageRows;
   if( Cursor->Count < Cursor->PageRows){
      Cursor->RowCount = (byte)Cursor->Count;    // clip rows to count
   }
   Cursor->PageChanged = YES;                    // force redraw
} // _FirstPage

//------------------------------------------------------------------------------
//   Last page
//------------------------------------------------------------------------------

static void _LastPage( TListCursor *Cursor)
// Move to last page
{
   if( Cursor->Count == 0){
      _FirstPage( Cursor);
      return;
   }
   DCursorIndexSet( Cursor, Cursor->Count - 1);
} // _LastPage
