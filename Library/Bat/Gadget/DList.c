//******************************************************************************
//
//   DList.c       Display list utility
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "DList.h"
#include "DEvent.h"
#include "DLayout.h"                 // Display layout
#include "Graphic/Graphic.h"
#include "Fonts.h"                   // project fonts

// Constants :
#define DLIST_TEXT_Y        23          // top of the text area
#define DLIST_MAX_ITEM_HEIGHT 19
#define DLIST_ROWS_COUNT 6      // max list items per page

static int Rows = DLIST_ROWS_COUNT;
static int HeaderW = 0;
#define DLIST_HEIGHT    (G_HEIGHT - DLIST_TEXT_Y - DLAYOUT_STATUS_H - HeaderW)  // items area height
static int DListRows( void)
// Returns rows per page count
{
   return Rows;
} // DListRows


static void DListSetRows(int rows)
// set rows count, single item height will be computed accordingly
{
   Rows = rows;
}
//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

void DListClear( TListCursor *Cursor)
// Clear list area
{
   DListClearWithHeader(Cursor, 0);
} // DListClear

void DListClearWithHeader(TListCursor *Cursor, int HeaderWidth)
// Clear list area
{
int color = GGetColor();

   HeaderW = HeaderWidth;
   SetFont( DLIST_FONT);
   // check for page redraw :
   if( !DCursorPreviousRowValid( Cursor)){
      GSetColor( DCOLOR_BACKGROUND);
      GBox( 0, DLAYOUT_TITLE_H, G_WIDTH, G_HEIGHT - DLAYOUT_TITLE_H - DLAYOUT_STATUS_H);
   }
   DListSetRows(Cursor->PageRows);
   GSetColor(color);
}

//------------------------------------------------------------------------------
//   Rows count
//------------------------------------------------------------------------------


int DListItemHeight()
// get single row height
{
   return DLIST_HEIGHT / DListRows();
}

//------------------------------------------------------------------------------
//   Get Y
//------------------------------------------------------------------------------

int DListY( int Row)
// Returns Y coordinate of <Row>
{
   return((G_HEIGHT - DLAYOUT_STATUS_H) - (DListRows() - Row) * DListItemHeight());
} // DListY

//------------------------------------------------------------------------------
//   Cursor
//------------------------------------------------------------------------------

TYesNo DListCursor( TListCursor *Cursor, int Row)
// Draw <Cursor> of <Row>
{
int RowY;

   RowY = DListY( Row);
   if( DCursorPreviousRowValid( Cursor)){
      // draw partialy (cursor move only)
      if( Row != DCursorRowPrevious( Cursor) && Row != DCursorRow( Cursor)){
         return( NO);                  // skip other rows
      }
      // erase row
      GSetColor( DCOLOR_BACKGROUND);
      GBox( 0, RowY, G_WIDTH, DListItemHeight());
      GSetColor( DCOLOR_DEFAULT);
   }
   // even line shadow :
   if( DCursorUseGrid( Cursor) && !(Row & 0x01)){
      GSetColor( DCOLOR_SHADOW);
      GBox( 0, RowY, G_WIDTH, DListItemHeight());
   }
   // draw cursor box :
   if( Row == DCursorRow( Cursor)){
      GSetColor( DCOLOR_CURSOR);
      GBoxRound( 0, RowY, G_WIDTH, DListItemHeight(), DCURSOR_R, DCURSOR_R);
      GSetColor( DCOLOR_BACKGROUND);
   } else {
      GSetColor( DCOLOR_DEFAULT);
   }
   return( YES);
} // DListCursor

//------------------------------------------------------------------------------
//   Line separator
//------------------------------------------------------------------------------

void DListLine( int x)
// Draw vertical line separator at <x>
{
int color;

   color = GGetColor();
   GSetColor( COLOR_LIGHTGRAY);
   GLine( x, DLAYOUT_TITLE_H + 2, x, G_HEIGHT - DLAYOUT_STATUS_H - 2);
   GSetColor( color);
} // DListLine

void DListHorizontalLine( int y)
{
int color;

   y += DLAYOUT_TITLE_H + 2;
   if(y > G_HEIGHT - DLAYOUT_STATUS_H - 2){
      // not allowed here
      return;
   }
   color = GGetColor();
   GSetColor( COLOR_LIGHTGRAY);
   GLine( 0, y, G_WIDTH, y);
   GSetColor( color);
}
