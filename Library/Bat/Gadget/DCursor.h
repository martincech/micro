//******************************************************************************
//                                                                            
//   DCursor.h     Display cursor utility
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __DCursor_H__
   #define __DCursor_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#define DCURSOR_ROW_INVALID   0xFF

// cursor internals :
typedef struct {
   dword Count;              // total items count
   byte  PageRows;           // maximum items per page
   byte  Row;                // cursor row
   byte  RowPrevious;        // previous cursor row
   byte  RowCount;           // rows per page
   byte  PageChanged;        // page number changed
   byte  UseGrid;            // use horizontal lines
   dword Page;               // page number
} TListCursor;

//------------------------------------------------------------------------------
//   Functions
//------------------------------------------------------------------------------

void DCursorInit( TListCursor *Cursor, dword Count, byte PageRows);
// Initialize <Cursor>

TYesNo DCursorRowUp( TListCursor *Cursor);
// Move cursor row up

TYesNo DCursorRowDown( TListCursor *Cursor);
// Move cursor row down

TYesNo DCursorPageUp( TListCursor *Cursor);
// Move cursor page up

TYesNo DCursorPageDown( TListCursor *Cursor);
// Move cursor page down

TYesNo DCursorPageChanged( TListCursor *Cursor);
// Test for current page changed

TYesNo DCursorRowChanged( TListCursor *Cursor);
// Test for current row changed

void DCursorRowUpdate( TListCursor *Cursor);
// Set current row as updated

void DCursorPageRedraw( TListCursor *Cursor);
// Force page redraw

void DCursorCountSet( TListCursor *Cursor, dword Count);
// Update items count

dword DCursorIndex( TListCursor *Cursor);
// Returns index of cursor item

void DCursorIndexSet( TListCursor *Cursor, dword Index);
// Set <Index> as cursor item

//------------------------------------------------------------------------------

// Macros for standard <Cursor> variable

#define DCursorCount( Cursor)                    (Cursor)->Count
// Returns items count

#define DCursorPageRows( Cursor)                 (Cursor)->PageRows
// Returns maximum page rows

#define DCursorPage( Cursor)                     (Cursor)->Page
// Returns start of page index

#define DCursorRow( Cursor)                      (Cursor)->Row
// Returns actual cursor row

#define DCursorRowCount( Cursor)                 (Cursor)->RowCount
// Returns actual page rows count

#define DCursorRowCountSet( Cursor, NewRows)     (Cursor)->RowCount = NewRows
// Update actual page rows count

#define DCursorRowPrevious( Cursor)              (Cursor)->RowPrevious
// Returns last cursor row

#define DCursorPreviousRowValid( Cursor)        ((Cursor)->RowPrevious != DCURSOR_ROW_INVALID)
// check for last row validity

#define DCursorUseGrid( Cursor)                 ((Cursor)->UseGrid)
// Set <YesNo> to use horizontal grid

#define DCursorUseGridSet( Cursor, YesNo)        (Cursor)->UseGrid = YesNo
// Set <YesNo> to use horizontal grid

#endif
