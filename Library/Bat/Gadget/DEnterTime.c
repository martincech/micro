//******************************************************************************
//
//   DEnterTime.c   Display enter date & time
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Gadget/DEnter.h"
#include "Graphic/Graphic.h"
#include "Console/conio.h"
#include "Convert/uBcd.h"
#include "Sound/Beep.h"
#include "Gadget/DEvent.h"
#include "Gadget/DLabel.h"
#include "Country/Country.h"
#include "Country/xTime.h"
#include "System/System.h"
// Local functions :

static unsigned TimeToBcd( UTime *Time);
// Returns BCD time

static void BcdToTime( UTime *Time, unsigned Bcd);
// Converts <Bcd> to <Time>

static int TimeMaxField( void);
// Returns highest possible field index

static int TimeGetOffset( int Field);
// Returns character offset of the field

static int TimeGetNumber( int Field);
// Returns limit on <Field>

static void TimePrint( int Value);
// Print BCD <Value> time

//------------------------------------------------------------------------------
//  Time
//------------------------------------------------------------------------------

// time fields 24hours (12 hours) :
typedef enum {
   FLD_1S,         // PM
   FLD_10S,        // 1s
   FLD_1MIN,       // 10s
   FLD_10MIN,      // 1min
   FLD_1HOUR,      // 10min
   FLD_10HOUR,     // 1h
   FLD_PM,         // 10h
   _FLD_LAST
} TTimeField;

TYesNo DEnterTime( UTime *Time, int x, int y)
// Enter time
{
int    Field;         // active field
int    FieldMax;      // max field
int    Value;
int    OldValue;
TYesNo ShowCursor;    // show/hide cursor
TYesNo FlashCursor;   // draw/skip cursor
int    TmpValue;
int    Event;
TYesNo AutoMove;
int    AutoMoveTimeout;

   if( uTimeValid( Time) != TIME_OK){
      // repair garbage
      Time->Hour = 0;
      Time->Min  = 0;
      Time->Sec  = 0;
   }
   Field       = TimeMaxField();       // edit MSB
   FieldMax    = Field;                // max field
   Value       = TimeToBcd( Time);
   OldValue    = -1;
   ShowCursor  = YES;                  // show cursor
   FlashCursor = NO;                   // no cursor
   SysKeyAscii( SYS_KEY_NUMBERS);
   AutoMove = YES;
   AutoMoveTimeout = TIMER_KEY_POS_SHIFT;
   forever {
      if( Value != OldValue){          // value changed - redraw
         // clear area :
         GSetColor( DCOLOR_ENTER_BG);
         GBox( x, y,  DEnterTimeWidth(), DENTER_H);
         GSetColor( DCOLOR_ENTER);
         // draw text :
         GTextAt( x, y);
         GSetFixedPitch();             // set nonproportional font
         TimePrint( Value);
         GSetNormalPitch();            // restore font setting
         // draw cursor :
         if( !ShowCursor || FlashCursor){
            GSetMode( GMODE_XOR);
            GSetColor( DCOLOR_ENTER_CURSOR);
            GBox( x + TimeGetOffset( Field) * GCharWidth(), y, GCharWidth() - 1, DENTER_H);
            GSetMode( GMODE_REPLACE);
         }
         GSetColor( DCOLOR_DEFAULT);
         OldValue = Value;             // remember value
         GFlush();                     // redraw
      }
      Event = DEventWait();
      switch( Event){
         case K_UP | K_REPEAT :
         case K_UP :
            AutoMove = YES;
            ShowCursor = NO;                               // disable cursor
            TmpValue = uBcdDigitGet( Value, Field);        // select digit
            if( TmpValue >= TimeGetNumber( Field)){
               TmpValue = 0;
            } else {
               TmpValue++;
            }
            BeepKey();
            Value = uBcdDigitSet( Value, Field, TmpValue); // compose digit
            break;

         case K_DOWN | K_REPEAT :
         case K_DOWN :
            AutoMove = YES;
            ShowCursor = NO;                               // disable cursor
            TmpValue = uBcdDigitGet( Value, Field);        // select digit
            if( TmpValue == 0){
               TmpValue = TimeGetNumber( Field);
            } else {
               TmpValue--;
            }
            BeepKey();
            Value = uBcdDigitSet( Value, Field, TmpValue); // compose digit
            break;
         case K_FLASH1 :
            if( !AutoMove){
               AutoMoveTimeout--;
            }

            FlashCursor = YES;
            OldValue    = -1;       // force redraw
            if( AutoMoveTimeout == 0){
               AutoMove = YES;
               SysKeyAscii( SYS_KEY_NUMBERS);
            } else {
               break;
            }
         while(AutoMove){
            default:
               if( Event & K_ASCII){
                  if(!(Event & K_REPEAT) && !AutoMove){
                     AutoMove = YES;
                  }else {
                     AutoMove = NO;
                     AutoMoveTimeout = TIMER_KEY_POS_SHIFT;
                     if( (Event & ~K_ASCII & ~K_REPEAT) == ' '){
                        break;
                     }
                     Value = uBcdDigitSet( Value, Field, (Event & ~K_ASCII & ~K_REPEAT) - '0');
                     ShowCursor = YES;
                     break; // cycle break
                  }
               }else {
                  if (Event & K_RELEASED){
                     ShowCursor = YES;       // key released, redraw cursor
                     OldValue   = -1;        // force redraw
                     break;
                  }else if( Event != K_FLASH1){
                     break; // cycle break
                  }else{
                     AutoMove = NO;
                     AutoMoveTimeout = TIMER_KEY_POS_SHIFT;
                  }
               }
            case K_RIGHT | K_REPEAT :
            case K_RIGHT :
               ShowCursor = NO;                               // disable cursor
               if( Field == 0){
                  if( AutoMove) continue; // cycle continue/break
                  else {
                     BeepError();
                     break;
                  }
               }
               BeepKey();
               Field--;
               OldValue   = -1;        // force redraw
               if( AutoMove) continue; // cycle continue/break
               else break;
         }
            if( (Event & ~K_REPEAT) == K_RIGHT || (Event == K_FLASH1)){
               AutoMove = YES;
            }
            if( Event == K_FLASH1){
               ShowCursor = YES;
            }
            break; // case break

         case K_LEFT | K_REPEAT :
         case K_LEFT :
            AutoMove = YES;
            ShowCursor = NO;                               // disable cursor
            if( Field == FieldMax){
               BeepError();
               break;
            }
            BeepKey();
            Field++;
            OldValue   = -1;        // force redraw
            break;

         case K_RELEASED :
            ShowCursor = YES;       // key released, redraw cursor
            OldValue   = -1;        // force redraw
            break;

         case K_FLASH2 :
            FlashCursor = NO;
            OldValue    = -1;       // force redraw
            break;

         case K_ENTER :
            BcdToTime( Time, Value);
            if( CountryTimeFormat() == TIME_FORMAT_12 &&
                 (Value & 0xF000000) &&
                ((Value & 0x0F00000) > 0x0200000)){
               // > 12:xx:xx
               Field   = FLD_10HOUR + 1;
               BeepError();
               OldValue   = -1;           // force redraw
               break;
            }
            switch( uTimeValid( Time)){
               case TIME_OK :
                  BeepKey();
                  SysKeyAscii( SYS_KEY_DEFAULT);
                  return( YES);
               case TIME_WRONG_SEC :
                  Field   = FLD_10S;
                  break;
               case TIME_WRONG_MIN :
                  Field   = FLD_10MIN;
                  break;
               case TIME_WRONG_HOUR :
                  Field   = FLD_10HOUR;
                  break;
               default :
                  Field = FLD_10HOUR;
                  break;
            }
            if( CountryTimeFormat() == TIME_FORMAT_12){
               Field++;                // shift because of AM/PM field
            }
            BeepError();
            OldValue   = -1;           // force redraw
            break;

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            SysKeyAscii( SYS_KEY_DEFAULT);
            return( NO);
      }
   }
} // DEnterTime

//------------------------------------------------------------------------------
//  Time width
//------------------------------------------------------------------------------

int DEnterTimeWidth( void)
// Returns width of time field
{
   return( xTimeWidth() * GCharWidth());
} // DEnterTimeWidth

//******************************************************************************

//------------------------------------------------------------------------------
//  BCD time
//------------------------------------------------------------------------------

static unsigned TimeToBcd( UTime *Time)
// Returns BCD time
{
unsigned Bcd;
int      Pm;

   if( CountryTimeFormat() == TIME_FORMAT_24){
      return( uBinaryToBcd( Time->Hour * 10000 + Time->Min * 100 + Time->Sec));
   }
   // TIME_FORMAT_12
   Pm = 0;
   if( Time->Hour >= 12){
      Pm = 1;
   }
   if( Time->Hour == 0){
      // 00:00 .. 00:59
      Time->Hour = 12;                // 12:xx am
   } else if( Time->Hour > 12){
      // 13:00 .. 23:59
      Time->Hour -= 12;
   } // else 01:00 .. 12:59
   Bcd = uBinaryToBcd( Time->Hour * 100000 + Time->Min * 1000 + Time->Sec * 10);
   if( Pm){
      Bcd++;                           // add PM flag to LSB
   }
   return( Bcd);
} // TimeToBcd

static void BcdToTime( UTime *Time, unsigned Bcd)
// Converts <Bcd> to <Time>
{
int Pm;

   if( CountryTimeFormat() == TIME_FORMAT_12){
      Pm  = Bcd & 0x0F;
      Bcd = uBcdToBinary( Bcd >> 4);
   } else {
      Pm  = 0;
      Bcd = uBcdToBinary( Bcd);
   }
   Time->Sec  = Bcd % 100;
   Bcd /= 100;
   Time->Min  = Bcd % 100;
   Bcd /= 100;
   Time->Hour = Bcd;
   if( CountryTimeFormat() == TIME_FORMAT_24){
      return;
   }
   if( Pm){
      // PM
      if( Time->Hour < 12){
         Time->Hour += 12;       // 01:00 pm .. 11:59 pm
      } // else 12:xx pm or error
   } else {
      // AM
      if( Time->Hour == 12){
         Time->Hour = 0;         // 12:00 am .. 12:59 am
      } // else 01:00 am .. 11:59 am or error
   }
} // BcdToTime

//------------------------------------------------------------------------------
//  Time field
//------------------------------------------------------------------------------

static int TimeMaxField( void)
// Returns highest possible field index
{
   if( CountryTimeFormat() == TIME_FORMAT_24){
      return( FLD_10HOUR);
   }
   // TIME_FORMAT_12
   return( FLD_PM);
} // TimeMaxField

//------------------------------------------------------------------------------
//  Time offset
//------------------------------------------------------------------------------

// time offsets :
static const byte Offset[ _TIME_FORMAT_COUNT][ _FLD_LAST] = {
/* 1s */   {7, 6, 4, 3, 1, 0, 0}, /* 10 hour */
/* PM */   {8, 7, 6, 4, 3, 1, 0}  /* 10 hour */
};

static int TimeGetOffset( int Field)
// Returns character offset of the field
{
   return( Offset[ CountryTimeFormat()][ Field]);
} // TimeGetOffset

//------------------------------------------------------------------------------
//  Time number
//------------------------------------------------------------------------------

// time limits :
static const byte Limit[ _TIME_FORMAT_COUNT][ _FLD_LAST] = {
/* 1s */   {9, 5, 9, 5, 9, 2, 0}, /* 10 hour */
/* PM */   {1, 9, 5, 9, 5, 9, 1}  /* 10 hour */
};

static int TimeGetNumber( int Field)
// Returns limit on <Field>
{
   return( Limit[ CountryTimeFormat()][ Field]);
} // TimeCheckNumber

//------------------------------------------------------------------------------
//  Time print
//------------------------------------------------------------------------------

static void TimePrint( int Value)
// Print BCD <Value> time
{
char Separator;
int  Hour, Min, Sec;
const char *PmStr;

   Separator = CountryTimeSeparator();
   if( CountryTimeFormat() == TIME_FORMAT_12){
      PmStr   = xTimeSuffix( Value & 0x0F);
      Value >>= 4;
   } else {
      PmStr   = "";
   }
   Hour = (Value >> 16) & 0xFF;
   Min  = (Value >>  8) & 0xFF;
   Sec  =  Value & 0xFF;
   cprintf( "%02x%c%02x%c%02x%s", Hour, Separator, Min, Separator, Sec, PmStr);
} // TimePrint
