//******************************************************************************
//
//    DGraph.h       Display graph
//    Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __DGraph_H__
   #define __DGraph_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#include "Bitmap.h"               // Bitmaps
#include "Str.h"

#define DGRAPH_LEGEND_H   20
#define DGRAPH_INVALID_POINT ((uint32) -1)
#define DGRAPH_MAX_LINE_COUNT 2

typedef void TGraphValueFormater(char *to, int32 value);
// print value for point on cursor to legend

typedef struct {
   int32                *Points;          // point for line(y)
   word                 PointCount;       // number of points
   TBitmap              *LegendImage;     // image for this line into legend
   TGraphValueFormater  *YValueFormater;   // if 0 just print default format
} TGraphLine;

typedef struct {
   TGraphLine           *Lines;           // lines array
   byte                 LineCount;        // length of lines array
   TYesNo               LinesSameYAxis;   // units on x axis are the same - dont scale agains each other
   word                 CursorOnPoint;    // position of cursor on first line,if >= PointCount => no cursor
   TGraphValueFormater  *XValueFormater;   // if 0 print nothing
} TGraph;

void DGraph(TGraph *graph, int y, int height);
// Draw graph for n lines with legend


//void DGraph( TGraph *Graph, TYesNo Zoomed);
//// Display all archive statistics as graph, when Zoomed = Yes then show only those around cursor

//void DGraphCursorCur( TGraph *Graph);
//// Redraw cursor on current possition, update status bar

//void DGraphCursorPlusPlus( TGraph *Graph);
//// Moves cursor to right, updates status bar

//void DGraphCursorMinusMinus( TGraph *Graph);
//// Moves cursor to left, updates status bar

#endif // __DGraph_H__
