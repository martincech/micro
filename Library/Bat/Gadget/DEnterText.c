//******************************************************************************
//
//   DEnterText.c   Display enter text box
//   Version 1.1    (c) VymOs
//
//******************************************************************************

#include "DInput.h"
#include "DLabel.h"
#include "DGrid.h"
#include "DEnter.h"
#include "DEvent.h"
#include "DMsg.h"
#include "System/System.h"
#include "Sound/Beep.h"
#include "Graphic/Graphic.h"
#include "Console/conio.h"
#include "Str.h"                        // strings from project directory
#include <string.h>

// Letter table :
#define SPECIAL_LETTERS
static const char Letters[] = "0123456789 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_+-*/=!?.,:;%#&";
//#define LETTERS_COUNT   ((int)(sizeof( Letters) - 1))

// Local functions :

static int FindLetter( char Letter, const char *AllowedLetters);
// Find letter

static TYesNo _DEnterText( char *Text, int Width, int x, int y, const char *AllowedLetters);
// Display and edit <Text> with maximal <Width>, letters limited to <AllowedLetters>

//------------------------------------------------------------------------------
//  Text
//------------------------------------------------------------------------------

TYesNo DEnterText( char *Text, int Width, int x, int y)
// Display and edit <Text> with maximal <Width>
{
   return _DEnterText(Text, Width, x, y, Letters);
} // DEnterText

//------------------------------------------------------------------------------
//  Text with limited letters
//------------------------------------------------------------------------------

TYesNo DEnterTextL( char *Text, int Width, int x, int y, const char *AllowedLetters)
// Display and edit <Text> with maximal <Width>, letters limited to <AllowedLetters>
{
   return _DEnterText(Text, Width, x, y, AllowedLetters);
} // DEnterTextL

//------------------------------------------------------------------------------
//  Text Width
//------------------------------------------------------------------------------

int DEnterTextWidth( int Width)
// Returns pixel width of text field
{
   return( Width * GCharWidth() + 1);
} // DEnterTextWidth

//******************************************************************************

//------------------------------------------------------------------------------
//  Text
//------------------------------------------------------------------------------

static TYesNo _DEnterText( char *Text, int Width, int ix, int iy, const char *AllowedLetters)
// Display and edit <Text> with maximal <Width>
{
int    Position;      // cursor position
int    LetterIndex;   // index in the Letters array
TYesNo Redraw;        // redraw text
TYesNo ShowCursor;    // show/hide cursor
TYesNo DrawCursor;    // draw/skip cursor
int    LineW;
int    x, y;
int    MaxLineChars;
char   *TextPtr;
int    Event;
TYesNo AutoMove;
int    AutoMoveTimeout;
int    AsciiMask;

   x = ix;
   y = iy;
   Position    = strlen(Text);
   memset(&Text[Position], 0, Width-Position);
   if( Position > 0){ Position--;}       // at last character
   Redraw      = YES;                  // first draw
   ShowCursor  = YES;                  // show cursor
   DrawCursor  = NO;                   // wait for flash
//   StrSetWidth( Text, Width);          // fill with spaces
   LetterIndex = FindLetter( Text[ Position], AllowedLetters);
   LineW = DEnterTextWidth( Width);
   if(G_WIDTH - (x << 1) < LineW){
      LineW = G_WIDTH - (x << 1);//2x
   }
   MaxLineChars = LineW/GCharWidth();
   if( MaxLineChars*GCharWidth() > LineW){
      MaxLineChars--;
   }
   TextPtr = Text;
   if( Position >= MaxLineChars){
      TextPtr += Position - MaxLineChars + 1;
      Position = MaxLineChars - 1;
   }
   AsciiMask = SYS_KEY_DEFAULT;
   if( strchr(AllowedLetters, '0')){
      AsciiMask |= SYS_KEY_NUMBERS;
   }
   if( strchr(AllowedLetters, 'A')){
      AsciiMask |= SYS_KEY_LETTER_UC;
   }
   if( strchr(AllowedLetters, 'a')){
      AsciiMask |= SYS_KEY_LETTER_LC;
   }
   iy = strlen(AllowedLetters);
   for( ix = 0; ix < iy; ix++){
      if((AllowedLetters[ix] >= '1' && AllowedLetters[ix] <= '9') ||
         (AllowedLetters[ix] >= 'A' && AllowedLetters[ix] <= 'Z') ||
         (AllowedLetters[ix] >= 'a' && AllowedLetters[ix] <= 'z') ||
         (AllowedLetters[ix] == ' ')){
         continue;
      }
      // special letter found
      AsciiMask |= SYS_KEY_SPECIAL;
      break;
   }

   SysKeyAscii( AsciiMask);
   AutoMove = YES;
   AutoMoveTimeout = TIMER_KEY_POS_SHIFT;
   forever {
      if( Redraw){
         // clear area :
         GSetColor( DCOLOR_ENTER_BG);
         GBox( x, y,  LineW, DENTER_H);
         GSetColor( DCOLOR_ENTER);
         // draw text :
         GTextAt( x + 1, y);
         GSetFixedPitch();             // set nonproportional font
         cputs( TextPtr);
         GSetNormalPitch();            // restore font setting
         // draw cursor :
         if( !ShowCursor || DrawCursor){
            GSetMode( GMODE_XOR);
            GSetColor( DCOLOR_ENTER_CURSOR);
            GBox( x + 1 + Position * GCharWidth(), y, GCharWidth() - 1, DENTER_H);
            GSetMode( GMODE_REPLACE);
         }
         GSetColor( DCOLOR_DEFAULT);
         Redraw = NO;
         GFlush();                     // redraw
      }
      Event = DEventWait();
      switch( Event){
         case K_DELETE:
            AutoMove = YES;
            if( TextPtr[ Position + 1] == '\0'){
               if( TextPtr[ Position] == ' '){
                  BeepError();
               }else{
                  TextPtr[ Position] = ' ';
                  Redraw = YES;
               }
               break;
            }
            memmove(&TextPtr[ Position], &TextPtr[ Position + 1], strlen(&TextPtr[ Position]));
            Redraw = YES;
            break;
         case K_BACKSPACE:
            AutoMove = YES;
            if( TextPtr == Text && Position == 0){
               BeepError();
               break;
            }
            Event = strlen(&TextPtr[ Position]); // just place holder
            if( Position == 0){
               memmove(&TextPtr[ Position], &TextPtr[ Position + 1], Event);
            }else {
               memmove(&TextPtr[ Position-1], &TextPtr[ Position], Event);
               Position--;
            }

            TextPtr[Position + Event] = '\0';
            Redraw = YES;
            break;
         case K_UP :
         case K_UP | K_REPEAT :
            AutoMove = YES;
            BeepKey();
            ShowCursor = NO;           // disable cursor
            if( AllowedLetters[LetterIndex + 1] == '\0'){
               LetterIndex = 0;
            } else {
               LetterIndex++;
            }
            // update character :
            TextPtr[ Position] = AllowedLetters[ LetterIndex];
            Redraw = YES;              // redraw edit box
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            AutoMove = YES;
            BeepKey();
            ShowCursor = NO;           // disable cursor
            if( LetterIndex == 0){
               LetterIndex = strlen(AllowedLetters) - 1;
            } else {
               LetterIndex--;
            }
            // update character :
            TextPtr[ Position] = AllowedLetters[ LetterIndex];
            Redraw = YES;              // redraw edit box
            break;

         case K_FLASH1 :
            if( !AutoMove){
               AutoMoveTimeout--;
            }
            DrawCursor = YES;
            Redraw     = YES;
            if( AutoMoveTimeout == 0){
               AutoMove = YES;
               SysKeyAscii( AsciiMask);
            } else {
               break;
            }
         while(AutoMove){
            default:
               if( Event & K_ASCII){
                  if(!(Event & K_REPEAT) && !AutoMove){
                     AutoMove = YES;
                  }else {
                     AutoMove = NO;
                     AutoMoveTimeout = TIMER_KEY_POS_SHIFT;
                     TextPtr[ Position] = (Event & ~K_ASCII & ~K_REPEAT);
                     LetterIndex = AllowedLetters[LetterIndex + 1];
                     Redraw = YES;
                     ShowCursor = YES;
                     break; // cycle break
                  }
               }else {
                  if (Event & K_RELEASED){
                     ShowCursor = YES;       // key released, redraw cursor
                     Redraw     = YES;
                     break;
                  }else if( Event != K_FLASH1){
                     break; // cycle break
                  }else{
                     AutoMove = NO;
                     AutoMoveTimeout = TIMER_KEY_POS_SHIFT;
                  }
               }
            case K_RIGHT | K_REPEAT :
            case K_RIGHT :
               if( Width < 2){
                  if( AutoMove) continue; // cycle continue/break
                  else break;
               }
               ShowCursor = NO;           // disable cursor
               if( Position == Width - 1 || ((TextPtr == (Text + Width - MaxLineChars)) && TextPtr != Text)
                   || (TextPtr[Position + 1] == '\0' &&
                       FindLetter( ' ', AllowedLetters) == 0 &&
                       AllowedLetters[0] != ' ' && TextPtr[Position] == ' ')){
                  if( AutoMove) continue; // cycle continue/break
                  else {
                     BeepError();
                     break;
                  }
               }
               BeepKey();
               if( Position + 1  == MaxLineChars){
                  TextPtr++;
               }else {
                  Position++;
               }
               if(TextPtr[Position] == '\0'){
                  TextPtr[Position+1] = '\0';
                  TextPtr[Position] = ' ';
                  LetterIndex = FindLetter( ' ', AllowedLetters);
               } else {
                  LetterIndex = FindLetter( TextPtr[ Position], AllowedLetters);
               }
               if(TextPtr[Position-1] == '\0'){
                  TextPtr[Position-1] = ' ';
               }
               Redraw = YES;
               if( AutoMove) continue; // cycle continue/break
               else break;
         }
            if( (Event & ~K_REPEAT) == K_RIGHT || (Event == K_FLASH1)){
               AutoMove = YES;
            }
            if( Event == K_FLASH1){
               ShowCursor = YES;
            }
            break; // case break
         case K_LEFT | K_REPEAT :
         case K_LEFT :
            AutoMove = YES;
            if( Width < 2){
               break;
            }
            ShowCursor = NO;           // disable cursor
            if( Position == 0){
               BeepError();
               break;
            }
            BeepKey();
            if( TextPtr != Text){
               TextPtr--;
            } else {
               Position--;
            }
            LetterIndex = FindLetter( TextPtr[ Position], AllowedLetters);
            Redraw = YES;
            break;

         case K_RELEASED :
            ShowCursor = YES;       // key released, redraw cursor
            Redraw     = YES;
            break;

         case K_FLASH2 :
            DrawCursor = NO;
            Redraw     = YES;
            break;


         case K_ENTER :
            BeepKey();
            // remove trailing spaces
            StrTrimRight( Text);
            SysKeyAscii( SYS_KEY_DEFAULT);
            return( YES);

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            // remove trailing spaces
            StrTrimRight( Text);
            SysKeyAscii( SYS_KEY_DEFAULT);
            return( NO);
      }
   }
} // _DEnterText

//------------------------------------------------------------------------------
//  Find
//------------------------------------------------------------------------------

static int FindLetter( char Letter, const char *AllowedLetters)
// Find letter
{
int  i = 0;

   while(AllowedLetters[i]){
      if( Letter == AllowedLetters[ i]){
         return( i);
      }
      i++;
   }
   return 0;
} // FindLetter

