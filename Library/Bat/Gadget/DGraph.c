//******************************************************************************
//
//    DGraph.h       Display graph
//    Version 1.0    (c) VEIT Electronics
//
//******************************************************************************
#include "DGraph.h"
#include "DLayout.h"
#include "Gadget/DLabel.h"        // Display labels
#include "Weight/DWeight.h"       // Display weight
#include "Archive/Archive.h"      // Archive items
#include "Console/Conio.h"        // Console output
#include "Math/uGeometry.h"       // Interpolation
#include "Bitmap.h"               // Bitmaps
#include "Fonts.h"                // Project fonts
#include "Str.h"                  // project directory strings
#include <assert.h>
#include <string.h>

#define GRAPH_LEGEND_OFFSET 2
#define GRAPH_LEGEND_FONT TAHOMA16

typedef double TMatrix[3][3];
typedef double THomoPoint[3];

static void mult( const TMatrix *m1, const TMatrix *m2, TMatrix *res)
{
int i,j, inner;
   memset(res,0,sizeof(TMatrix));
   for(i = 0; i < 3; i++){
      for(j = 0; j < 3; j++){
         for (inner = 0; inner < 3; inner++) {
            (*res)[i][j] += (*m1)[i][inner] * (*m2)[inner][j];
         }
      }
   }
}

static void transform(double *x, double *y, const TMatrix *m)
{
   TMatrix res;
   TMatrix left;
   memset(&left,0,sizeof(TMatrix));
   left[0][0] = *x;
   left[0][1] = *y;
   left[0][2] = 1;
   mult(&left, m, &res);
   *x = res[0][0];
   *y = res[0][1];
}

static void TransMatrix(TMatrix *m, double dx, double dy){
   memset(m,0,sizeof(TMatrix));
   (*m)[0][0] = 1;
   (*m)[1][1] = 1;
   (*m)[2][2] = 1;
   (*m)[2][0] = dx;
   (*m)[2][1] = dy;
}

static void ScaleMatrix(TMatrix *m, double sx, double sy){
   memset(m,0,sizeof(TMatrix));
   (*m)[0][0] = sx;
   (*m)[1][1] = sy;
   (*m)[2][2] = 1;
}

// draw data lines
static void DrawLegend(TGraph *Graph, int y);
// draw legend line and x description
static void DrawCursor( TGraph *Graph, int x, int xwidth, int cursorY, int legendY);
// Draw cursor and legend identification
//------------------------------------------------------------------------------
//   Graph view
//------------------------------------------------------------------------------
#include <limits.h>
void DGraph(TGraph *graph, int iy, int height)
// Draw graph for n lines with legend
{
int i, j;
TColor Color;
TMatrix transformationMatrix[DGRAPH_MAX_LINE_COUNT];
TMatrix scaleMatrix;
TMatrix transMatrix;
int32 min[DGRAPH_MAX_LINE_COUNT], max[DGRAPH_MAX_LINE_COUNT];
int32 absMin, absMax;
double xwidth, ywidth;
int32 ybase;
int32 cursorx = -1;
double x, prevx, y, prevy;
int xtrans = 0;

   ybase = ywidth = height - DGRAPH_LEGEND_H;
   ybase += iy;
   xwidth = G_WIDTH;

   if( graph->LineCount > DGRAPH_MAX_LINE_COUNT){
      return;
   }
   for( i = 0; i < graph->LineCount-1; i++){
      if( graph->Lines[i].PointCount != graph->Lines[i + 1].PointCount){
         return;
      }
   }

   if( graph->CursorOnPoint < graph->Lines[0].PointCount){
      xwidth -= BmpGraphCursor.Width;
      ywidth -= BmpGraphCursor.Height;
      xtrans = BmpGraphCursor.Width/2;
   }

   absMax = INT_MIN;
   absMin = INT_MAX;
   // find minimum and maximum for each line
   for(j = 0; j < graph->LineCount; j++){
      min[j] = INT_MAX;
      max[j] = INT_MIN;
      for( i = 0; i < graph->Lines[0].PointCount; i++){
         if( graph->Lines[j].Points[i] == DGRAPH_INVALID_POINT){
            continue;
         }
         if( graph->Lines[j].Points[i] < min[j]){
            min[j] = graph->Lines[j].Points[i];
         }
         if( graph->Lines[j].Points[i] > max[j]){
            max[j] = graph->Lines[j].Points[i];
         }
      }
      if( min[j] < absMin){
         absMin = min[j];
      }
      if( max[j] > absMax){
         absMax = max[j];
      }
   }

   // create matrices for each line
   for( i = 0; i < graph->LineCount; i++){
      if( graph->LinesSameYAxis == YES){
         j = absMax - absMin;
         TransMatrix(&transMatrix, 0, -absMin);
      }else {
         j = max[i] - min[i];
         TransMatrix(&transMatrix, 0, -min[i]);
      }
      if( j == 0){j = 1;}
      x = graph->Lines[0].PointCount - 1;
      if( x == 0) {
         x = 1;
      }
      ScaleMatrix(&scaleMatrix, xwidth/(int)x, ywidth/j);
      mult(&transMatrix, &scaleMatrix, transformationMatrix[i]);
   }

   // apply transformation and draw points
   Color = COLOR_WHITE;
   for(j = 0; j < graph->LineCount ; j++){
      if( Color == COLOR_WHITE){
         Color = COLOR_BLACK;
      } else {
         Color--;
      }

      prevx = 0;
      prevy = DGRAPH_INVALID_POINT;
      GSetColor(Color);
      if( graph->Lines[j].PointCount == 1){
         y = graph->Lines[j].Points[0];
         x=1;
         transform(&x, &y, &transformationMatrix[j]);
         GLine( xtrans, ybase - ((int)y), x + xtrans, ybase - ((int)y));
      }else {
         for( i = 0; i < graph->Lines[0].PointCount; i++){
            y = graph->Lines[j].Points[i];
            x = i;

            if((int32)y != DGRAPH_INVALID_POINT){
               transform(&x, &y, &transformationMatrix[j]);
               if( prevy != DGRAPH_INVALID_POINT){
                  GLine( ((int)prevx) + xtrans , ybase - ((int)prevy), ((int)x) + xtrans, ybase - ((int)y));
               } else if(i == graph->Lines[0].PointCount){
                  // special case when all invalid only last one is valid
                  prevy = y;
                  GLine( ((int)prevx) + xtrans , ybase - ((int)prevy), ((int)x) + xtrans, ybase - ((int)y));
               }
               prevy = y;
               prevx = x;
            }
         }
      }
   }

   DrawLegend(graph, ybase + 1);
   if( graph->CursorOnPoint < graph->Lines[0].PointCount){
      x = graph->CursorOnPoint;
      y = 1;
      transform(&x, &y, &transformationMatrix[0]);
      cursorx = x + xtrans;
      DrawCursor(graph, cursorx, xwidth, iy, ybase + 1);
   }
}

//------------------------------------------------------------------------------
//   Legend
//------------------------------------------------------------------------------

static void DrawLegend( TGraph *Graph, int y)
// Draw bottom legend images
{
char Buf[20];
   // horizontal line
   GSetColor( COLOR_BLACK);
   GLine( 0, y,   G_WIDTH, y);
   GLine( 0, y+1, G_WIDTH, y+1);
   GSetMode( GMODE_REPLACE);

   if( Graph->XValueFormater && Graph->Lines[0].PointCount > 0){
      SetFont( GRAPH_LEGEND_FONT);
      // x text
      GSetColor(COLOR_BLACK);
      GTextAt(GRAPH_LEGEND_OFFSET, y);
      Graph->XValueFormater( Buf, Graph->CursorOnPoint);
      cputs(Buf);
   }

}

//------------------------------------------------------------------------------
//   Cursor
//------------------------------------------------------------------------------

static void DrawCursor( TGraph *Graph, int x, int xwidth, int cursorY, int legendY)
// Draw cursor and legend identification
{
int width;
TColor Color;
int    i;
char buffer[20];

   // CURSOR
   GSetColor(COLOR_LIGHTGRAY);
   while( x - BmpGraphCursor.Width/2 < 0){
      x++;
   }
   while( x + BmpGraphCursor.Width/2 > G_WIDTH){
      x--;
   }
   GBitmap( x - BmpGraphCursor.Width/2, cursorY, &BmpGraphCursor);
   GLine(x, cursorY + BmpGraphCursor.Height, x, legendY);

   // LEGEND DATA
   Color = COLOR_LIGHTGRAY;
   SetFont( GRAPH_LEGEND_FONT);
   width = 0;
   for( i = Graph->LineCount-1; i >= 0 ; i--){
      if( Color > COLOR_BLACK){
         Color = COLOR_LIGHTGRAY;
      } else {
         Color++;
      }

      GSetColor(Color);
      if( Graph->Lines[0].PointCount != 0 &&
          Graph->Lines[i].Points[Graph->CursorOnPoint] != DGRAPH_INVALID_POINT){
         if( Graph->Lines[i].YValueFormater){
            Graph->Lines[i].YValueFormater( buffer, Graph->Lines[i].Points[Graph->CursorOnPoint]);
         }else {
            bprintf(buffer, "%d", Graph->Lines[i].Points[Graph->CursorOnPoint]);
         }
      } else {
         bprintf(buffer, "---");
      }
      width += GTextWidth(buffer) + GRAPH_LEGEND_OFFSET;
      GTextAt(G_WIDTH - width, legendY + GRAPH_LEGEND_OFFSET);
      cputs(buffer);
      if( Graph->Lines[i].LegendImage){
         width += Graph->Lines[i].LegendImage->Width + GRAPH_LEGEND_OFFSET;
         GBitmap(G_WIDTH - width, legendY + GRAPH_LEGEND_OFFSET, Graph->Lines[i].LegendImage);
      }
   }
}
