//******************************************************************************
//                                                                            
//   DInput.c       Display input box
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "DInput.h"
#include "Graphic/Graphic.h"
#include "Convert/uBcd.h"
#include "Console/conio.h"
#include "DLabel.h"               // Display label
#include "DEnter.h"               // Enter data
#include "DMsg.h"                 // Message box
#include "DLayout.h"              // Display layout
#include "DFormat.h"              // Display format
#include "Str.h"                  // project strings
#include "Bitmap.h"               // project bitmaps
#include "Fonts.h"                // Project fonts
#include <string.h>

//------------------------------------------------------------------------------
//  Integer
//------------------------------------------------------------------------------

TYesNo DInputNumber( TUniStr Caption, TUniStr Text, int32 *Value,
                     int Decimals, int LoLimit, int HiLimit, TUniStr Units)
// Input number
{
int    Width;
int    EditWidth;
int    NewValue;
int    x;                 // edit field position
TYesNo HasSignum;
#ifdef DINPUT_RANGE_ENABLE
   char RangeTxt[ 32];
#endif


   HasSignum = NO;
   if( Decimals &  FMT_PLUS){
      Decimals &= ~FMT_PLUS;
      HasSignum = YES;
   }
   SetFont( DINPUT_FONT);
   EditWidth = uBinaryWidth( HiLimit);
   Width     = EditWidth;
   if( Width <= Decimals){
      Width = Decimals + 1;            // fraction only, add 0.000
   }
   if( HasSignum){
      Width |= FMT_PLUS;
   }
   NewValue  = *Value;
   x         = G_WIDTH / 2 - DEnterNumberWidth( Width, Decimals) / 2;
   forever {
      // draw widgets :
      GClear();
      DLayoutTitle( Caption);
#ifdef DINPUT_RANGE_ENABLE
      DLabelCenter( Text, 0, DINPUT_CAPTION_Y - 10, G_WIDTH, 0);
      DFormatRange( RangeTxt, Decimals, LoLimit, HiLimit, Units);
      DLabelCenter( RangeTxt, 0, DINPUT_CAPTION_Y + 10, G_WIDTH, 0);
#else
      DLabelCenter( Text, DINPUT_CAPTION_Y, 40, G_WIDTH, 0);
#endif
      DLabel( Units, G_WIDTH / 2 + (DEnterNumberWidth( Width, Decimals) + GCharWidth()) / 2, DINPUT_EDIT_Y);
      DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
      GFlush();
      // edit number :
      if( !DEnterNumber( &NewValue, Width, Decimals, EditWidth, x, DINPUT_EDIT_Y)){
         return( NO);                  // escape
      }
      if( NewValue >= LoLimit && NewValue <= HiLimit){
         *Value = NewValue;
         return( YES);         
      }
#ifdef DINPUT_RANGE_ENABLE
      DMsgOk( STR_ERROR, STR_OUT_OF_LIMITS, RangeTxt);
#else
      DMsgOk( STR_ERROR, STR_OUT_OF_LIMITS, 0);
#endif
   }
} // DInputNumber

//------------------------------------------------------------------------------
//  Enum
//------------------------------------------------------------------------------

#ifdef DENTER_CALLBACK
TYesNo DInputEnum(   TUniStr Caption, TUniStr Text, int *Value, 
                     TUniStr Base, int EnumCount)
{
   return( DInputEnumCallback( Caption, Text, Value, Base, EnumCount, 0));
} // DInputEnum

TYesNo DInputEnumCallback( TUniStr Caption, TUniStr Text, int *Value, 
                           TUniStr Base, int EnumCount, TAction *OnChange)
// Input enum with <OnChange> callback

#else
TYesNo DInputEnum(   TUniStr Caption, TUniStr Text, int *Value, 
                     TUniStr Base, int EnumCount)
// Input enum
#endif
{
int x;                    // edit field position
int Width;

   SetFont( DINPUT_FONT);
   Width = DEnterEnumWidth( Base, EnumCount);
   x = G_WIDTH / 2 - Width / 2;
   // draw widgets :
   GClear();
   DLayoutTitle( Caption);
   DLabelCenter( Text, 0, DINPUT_CAPTION_Y, G_WIDTH, 0);
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
   // arrows :
   GBitmap( x + Width / 2 - 4, DINPUT_EDIT_Y - 5,                 &BmpEditUp);
   GBitmap( x + Width / 2 - 4, DINPUT_EDIT_Y + DINPUT_EDIT_H + 1, &BmpEditDown);
   GFlush();
   // edit enum :
#ifdef DENTER_CALLBACK
   return( DEnterEnum( Value, Base, EnumCount, OnChange, x, DINPUT_EDIT_Y, CENTER_MIDDLE, 0));
#else
   return( DEnterEnum( Value, Base, EnumCount, x, DINPUT_EDIT_Y, CENTER_MIDDLE, 0));
#endif
} // DInputEnum

#ifdef DINPUT_PROGRESS
//------------------------------------------------------------------------------
//  Progress
//------------------------------------------------------------------------------

TYesNo DInputProgress( TUniStr Caption, TUniStr Text, int *Value, 
                       int MaxValue, TAction *OnChange)
// Input value by progress bar
{
int x;                    // edit field position

   SetFont( DINPUT_FONT);
   x = G_WIDTH / 2 - DEnterProgressWidth() / 2;
   // draw widgets :
   GClear();
   DLayoutTitle( Caption);
   DLabelCenter( Text, 0, DINPUT_CAPTION_Y, G_WIDTH, 0);
      DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
   GFlush();
   // edit enum :
   return( DEnterProgress( Value, MaxValue, OnChange, x, DINPUT_EDIT_Y));
} // DInputProgress

#endif // DINPUT_PROGRESS

//------------------------------------------------------------------------------
//  Password
//------------------------------------------------------------------------------

void DInputPassword(  TUniStr Caption, TUniStr Text, char *Password, int Width)
// Input password
{
int x;                    // edit field position

   SetFont( DINPUT_FONT);
   x = G_WIDTH / 2 - DEnterPasswordWidth( Width) / 2;
   // draw widgets :
   GClear();
   DLayoutTitle( Caption);
   DLabelCenter( Text, 0, DINPUT_CAPTION_Y, G_WIDTH, 0);
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
   GFlush();
   // edit enum :
   DEnterPassword( Password, Width, x, DINPUT_EDIT_Y);
} // DInputPassword

#ifndef DINPUT_TIME_DISABLE
//------------------------------------------------------------------------------
//  Time
//------------------------------------------------------------------------------

TYesNo DInputTime( TUniStr Caption, TUniStr Text, UTime *Time)
// Enter time
{
int x;                    // edit field position

   SetFont( DINPUT_FONT);
   x = G_WIDTH / 2 - DEnterTimeWidth() / 2;
   // draw widgets :
   GClear();
   DLayoutTitle( Caption);
   DLabelCenter( Text, 0, DINPUT_CAPTION_Y, G_WIDTH, 0);
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
   GFlush();
   // edit enum :
   return( DEnterTime( Time, x, DINPUT_EDIT_Y));
} // DInputTime
#endif // DINPUT_TIME_DISABLE

#ifndef DINPUT_DATE_DISABLE
//------------------------------------------------------------------------------
//  Date
//------------------------------------------------------------------------------

TYesNo DInputDate( TUniStr Caption, TUniStr Text, UDate *Date)
// Enter date
{
int x;                    // edit field position

   SetFont( DINPUT_FONT);
   x = G_WIDTH / 2 - DEnterDateWidth() / 2;
   // draw widgets :
   GClear();
   DLayoutTitle( Caption);
   DLabelCenter( Text, 0, DINPUT_CAPTION_Y, G_WIDTH, 0);
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
   GFlush();
   // edit enum :
   return( DEnterDate( Date, x, DINPUT_EDIT_Y));
} // DInputDate
#endif // DINPUT_DATE_DISABLE

#ifndef DINPUT_IP_DISABLE
//------------------------------------------------------------------------------
//  IP
//------------------------------------------------------------------------------

TYesNo DInputIp( TUniStr Caption, TUniStr Text, fnet_ip4_addr_t *Ip)
// Enter IP address
{
int x;                    // edit field position

   SetFont( DINPUT_FONT);
   x = G_WIDTH / 2 - DEnterIpWidth() / 2;
   // draw widgets :
   GClear();
   DLayoutTitle( Caption);
   DLabelCenter( Text, 0, DINPUT_CAPTION_Y, G_WIDTH, 0);
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
   GFlush();
   // edit IP :
   return( DEnterIp( Ip, x, DINPUT_EDIT_Y));
} // DInputIp
#endif // DINPUT_IP_DISABLE
