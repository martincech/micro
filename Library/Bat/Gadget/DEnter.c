//******************************************************************************
//
//   DEnter.c       Display enter value
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "DEnter.h"
#include "Graphic/Graphic.h"
#include "Convert/uBcd.h"
#include "Console/conio.h"
#include "Sound/Beep.h"
#include "System/System.h"
#include "DEvent.h"
#include "DLabel.h"
#include "DProgress.h"
#include "assert.h"

//------------------------------------------------------------------------------
//  Number
//------------------------------------------------------------------------------

TYesNo DEnterNumber( int *InitialValue, int Width, int Decimals, int EditWidth, int x, int y)
// Display and edit <InitialValue>.
// <Width> - total digits, <Decimals> digits after decimal dot, <EditWidth> editable width
{
int    Order;         // active decimal order
int    Value;
int    OldValue;
int    Event;
TYesNo ShowCursor;    // show/hide cursor
TYesNo FlashCursor;   // draw/skip cursor
dword  Format;
byte   TmpValue;
TYesNo HasSignum;
TYesNo IsNegative;
TYesNo EnterInserted;
TYesNo AutoMove;
int    AutoMoveTimeout;

   HasSignum    = NO;
   if( Width & FMT_PLUS){
      Width &= ~FMT_PLUS;
      Width++;
      EditWidth++;
      HasSignum = YES;
   }
   if( Decimals){
      // with decimal point
      Format = FmtPrecision( Width + 1, Decimals) | FMT_LEADING_0; // width plus dot
   } else {
      // integer number
      Format = FmtSetWidth( Width) | FMT_LEADING_0;                // width only
   }

   if( HasSignum){
      Format |= FMT_PLUS;
   }
   IsNegative  = NO;
   Value       = *InitialValue;
   if( Value < 0){
      Value      = -Value;
      IsNegative = YES;
   }
   Value         = uBinaryToBcd( Value);
   OldValue      = -1;
   ShowCursor    = YES;                  // show cursor
   FlashCursor   = NO;                   // no cursor
   EnterInserted = NO;                   // wait for enter
   Order         = EditWidth - 1;        // edit MSB

   SysKeyAscii( SYS_KEY_NUMBERS);
   AutoMove = YES;
   AutoMoveTimeout = TIMER_KEY_POS_SHIFT;
   forever {
      if( EnterInserted){
         // clear area :
         GSetColor( COLOR_LIGHTGRAY);
         GBox( x, y, DEnterNumberWidth( Width, Decimals), DENTER_H);
         GSetColor( DCOLOR_DEFAULT);
         GFlush();
         return( YES);
      }
      if( Value != OldValue){          // value changed - redraw
         // clear area :
         GSetColor( DCOLOR_ENTER_BG);
         GBox( x, y, DEnterNumberWidth( Width, Decimals), DENTER_H);
         // draw text :
         GSetColor( DCOLOR_ENTER);
         GTextAt( x + 1, y);
         GSetNumericPitch();           // set nonproportional/numeric font
         if( IsNegative){
            cprinthex( Value, Format | FMT_NEGATIVE);
         } else {
            cprinthex( Value, Format);
         }
         GSetNormalPitch();            // restore font setting
         // draw cursor :
         if( !ShowCursor || FlashCursor){
            GSetMode( GMODE_XOR);
            GSetColor( DCOLOR_ENTER_CURSOR);
            if( Order < Decimals){
               GBox( x + 1 + (Width - Order - 1) * GNumberWidth() + GLetterWidth( '.'), y, GNumberWidth() - 1, DENTER_H); // add dot
            } else {
               GBox( x + 1 + (Width - Order - 1) * GNumberWidth(), y, GNumberWidth() - 1, DENTER_H);
            }
            GSetMode( GMODE_REPLACE);
         }
         GSetColor( DCOLOR_DEFAULT);
         OldValue = Value;             // remember value
         GFlush();                     // redraw
      }
      Event = DEventWait();
      switch( Event){
         case K_UP | K_REPEAT :
         case K_UP :
            AutoMove = YES;
            ShowCursor = NO;                               // disable cursor
            // check for signum position :
            if( HasSignum && (Order == EditWidth - 1)){
               IsNegative = !IsNegative;
               OldValue    = -1;                           // force redraw
               BeepKey();
               break;
            }
            // numeric position :
            TmpValue = uBcdDigitGet( Value, Order);        // select digit
            if( TmpValue == 9){
               TmpValue = 0;
            } else {
               TmpValue++;
            }
            BeepKey();
            Value = uBcdDigitSet( Value, Order, TmpValue); // compose digit
            break;

         case K_DOWN | K_REPEAT :
         case K_DOWN :
            AutoMove = YES;
            ShowCursor = NO;                               // disable cursor
            // check for signum position :
            if( HasSignum && (Order == EditWidth - 1)){
               IsNegative = !IsNegative;
               OldValue    = -1;                           // force redraw
               BeepKey();
               break;
            }
            // numeric position :
            TmpValue = uBcdDigitGet( Value, Order);        // select digit
            if( TmpValue == 0){
               TmpValue = 9;
            } else {
               TmpValue--;
            }
            BeepKey();
            Value = uBcdDigitSet( Value, Order, TmpValue); // compose digit
            break;

         case K_FLASH1 :
            if( !AutoMove){
               AutoMoveTimeout--;
            }

            FlashCursor = YES;
            OldValue    = -1;       // force redraw
            if( AutoMoveTimeout == 0){
               AutoMove = YES;
               SysKeyAscii( SYS_KEY_NUMBERS);
            } else {
               break;
            }
         while(AutoMove){
            default:
               if( Event & K_ASCII){
                  if(!(Event & K_REPEAT) && !AutoMove){
                     AutoMove = YES;
                  }else {
                     AutoMove = NO;
                     AutoMoveTimeout = TIMER_KEY_POS_SHIFT;
                     Value = uBcdDigitSet( Value, Order, (Event & ~K_ASCII & ~K_REPEAT) - '0');
                     ShowCursor = YES;
                     break; // cycle break
                  }
               }else {
                  if (Event & K_RELEASED){
                     ShowCursor = YES;       // key released, redraw cursor
                     break;
                  }else if( Event != K_FLASH1){
                     break; // cycle break
                  }else{
                     AutoMove = NO;
                     AutoMoveTimeout = TIMER_KEY_POS_SHIFT;
                  }
               }
            case K_RIGHT | K_REPEAT :
            case K_RIGHT :
               if( EditWidth < 2){
                  if( AutoMove) continue; // cycle continue/break
                  else break;
               }
               ShowCursor = NO;                               // disable cursor
               if( Order == 0){
                  if( AutoMove) continue; // cycle continue/break
                  else {
                     BeepError();
                     break;
                  }
               }
               BeepKey();
               Order--;
               OldValue   = -1;        // force redraw
               if( AutoMove) continue; // cycle continue/break
               else break;
         }
            if( (Event & ~K_REPEAT) == K_RIGHT || (Event == K_FLASH1)){
               AutoMove = YES;
            }
            if( Event == K_FLASH1){
               ShowCursor = YES;
            }
            break; // case break

         case K_LEFT | K_REPEAT :
         case K_LEFT :
            AutoMove = YES;
            if( EditWidth < 2){
               break;
            }
            ShowCursor = NO;                               // disable cursor
            if( Order == EditWidth - 1){
               BeepError();
               break;
            }
            BeepKey();
            Order++;
            OldValue   = -1;        // force redraw
            break;

         case K_RELEASED :
            ShowCursor = YES;       // key released, redraw cursor
            OldValue   = -1;        // force redraw
            break;

         case K_FLASH2 :
            FlashCursor = NO;
            OldValue    = -1;       // force redraw
            break;

         case K_ENTER :
            BeepKey();
            Value = uBcdToBinary( Value);
            if( IsNegative){
               Value = -Value;
            }
            *InitialValue =  Value;
            EnterInserted = YES;
            SysKeyAscii(SYS_KEY_DEFAULT);
            break;

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            SysKeyAscii( SYS_KEY_DEFAULT);
            return( NO);

      }
   }
} // DEnterNumber

//------------------------------------------------------------------------------
//  Number width
//------------------------------------------------------------------------------

int DEnterNumberWidth( int Width, int Decimals)
// Returns pixel width of number field
{
int SignumWidth;

   SignumWidth = 0;
   if( Width & FMT_PLUS){
      Width &= ~FMT_PLUS;
      SignumWidth = GLetterWidth( '+');
   }
   return( SignumWidth + Width * GNumberWidth() + (Decimals ? GLetterWidth( '.') : 0) + 1);
} // DEnterNumberWidth

//------------------------------------------------------------------------------
//  Enum
//------------------------------------------------------------------------------

#ifndef DENTER_CALLBACK
   TYesNo DEnterEnum( int *InitialValue, TUniStr Base, int EnumCount, int x, int y, TCenterType Center, unsigned Mask)
   // Display and edit <InitialValue>
#else
   TYesNo DEnterEnum( int *InitialValue, TUniStr Base, int EnumCount, TAction *OnChange, int x, int y, TCenterType Center, unsigned Mask)
   // Display and edit <InitialValue>
#endif
{
int Width;
int Value;
int OldValue;
int i;

   Width = DEnterEnumWidth( Base, EnumCount);
   Value    = *InitialValue;

   unsigned flag = ((1 << Value) & Mask);
   assert(("Default enum value is masked", flag == 0));

   OldValue = -1;
   forever {
      if( Value != OldValue){
         // clear area
         GSetColor( DCOLOR_ENTER_BG);
         GBox( x, y,  Width, DENTER_H);
         GSetColor( DCOLOR_ENTER);
         DLabelEnumCenter( Value, Base, x, y, Width, Center);
         GSetColor( DCOLOR_DEFAULT);   // default
         OldValue = Value;             // remember value
         GFlush();                     // redraw
#ifdef DENTER_CALLBACK
         if( OnChange){
            (*OnChange)( Value);       // callback
         }
#endif
      }
      switch( DEventWait()){
         case K_UP | K_REPEAT :
         case K_UP :
            if( Value >= EnumCount - 1){
               BeepError();
               break;
            }
            BeepKey();

            i = 1;
            while (i < EnumCount && (Value + i) < EnumCount)
            {
                if (((1 << (Value + i)) & Mask) == 0)
                {
                    Value += i;
                    break;
                }
                i++;
            }
            break;

         case K_DOWN | K_REPEAT :
         case K_DOWN :
            if( Value == 0){
               BeepError();
               break;
            }
            BeepKey();

            i = 1;
            while (i < EnumCount && (Value - i) >= 0)
            {
                if (((1 << (Value - i)) & Mask) == 0)
                {
                    Value -= i;
                    break;
                }
                i++;
            }
            break;

         case K_ENTER :
            BeepKey();
            *InitialValue = Value;
            return( YES);

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return( NO);
      }
   }
} // DEnterEnum

//------------------------------------------------------------------------------
//  Enum width
//------------------------------------------------------------------------------

int DEnterEnumWidth( TUniStr Base, int EnumCount)
// Returns pixel width of enum field
{
   return( DLabelEnumWidth( Base, EnumCount) + 2);
} // DEnterEnumWidth

#ifdef DENTER_PROGRESS
//------------------------------------------------------------------------------
//  Progress
//------------------------------------------------------------------------------

TYesNo DEnterProgress( int *InitialValue, int MaxValue, TAction *OnChange, int x, int y)
// Enter value by progress bar
{
int Value;
int OldValue;

   Value    = *InitialValue;
   OldValue = -1;
   forever {
      if( Value != OldValue){
         // clear area
         GSetColor( DCOLOR_BACKGROUND);
         GBox( x, y,  DENTER_PROGRESS_WIDTH, DENTER_PROGRESS_HEIGHT);
         GSetColor( DCOLOR_DEFAULT);
         DProgress((Value * 100) / MaxValue, x, y, DENTER_PROGRESS_WIDTH, DENTER_PROGRESS_HEIGHT);
         OldValue = Value;             // remember value
         GFlush();                     // redraw
         if( OnChange){
            (*OnChange)( Value);       // callback
         }
      }
      switch( DEventWait()){
         case K_RIGHT | K_REPEAT :
         case K_RIGHT :
            if( Value >= MaxValue){
               BeepError();
               break;
            }
            BeepKey();
            Value++;
            break;

         case K_LEFT | K_REPEAT :
         case K_LEFT :
            if( Value == 0){
               BeepError();
               break;
            }
            BeepKey();
            Value--;
            break;

         case K_ENTER :
            BeepKey();
            *InitialValue = Value;
            return( YES);

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return( NO);
      }
   }
} // DEnterProgress

//------------------------------------------------------------------------------
//  Progress width
//------------------------------------------------------------------------------

int DEnterProgressWidth( void)
// Returns pixel width of progress field
{
   return( DENTER_PROGRESS_WIDTH);
} // DEnterProgressWidth

#endif // DENTER_PROGRESS
//------------------------------------------------------------------------------
//  Password
//------------------------------------------------------------------------------

void DEnterPassword( char *Password, int Width, int x, int y)
// Enter password into <Password> array
{
int Count;
int OldCount;
int i;
int Key;

   // initial password :
   for( i = 0; i < Width; i++){
      Password[ i] = K_NULL;
   }
   Count    = 0;
   OldCount = -1;
   forever {
      if( Count != OldCount){
         GSetFixedPitch();
         GTextAt( x, y);
         cputch( '[');
         for( i = 0; i < Count; i++){
            cputch( '*');
         }
         for( i = Count; i < Width; i++){
            cputch( ' ');
         }
         cputch( ']');
         GSetNormalPitch();
         OldCount = Count;             // remember count
         GFlush();                     // redraw
      }
      switch( Key = DEventWait()){
         case K_0:
         case K_1:
         case K_2:
         case K_3:
         case K_4:
         case K_5:
         case K_6:
         case K_7:
         case K_8:
         case K_9:
         case K_ENTER :
         case K_ESC :
            BeepKey();
            Password[ Count] = Key;
            Count++;
            if( Count == Width){
               return;
            }
            break;

         case K_TIMEOUT :
            return;
      }
   }
} // DEnterPassword

//------------------------------------------------------------------------------
//  Password width
//------------------------------------------------------------------------------

int DEnterPasswordWidth( int Width)
// Returns width of password field
{
   return( (Width + 2) * GCharWidth());
} // DEnterPasswordWidth
