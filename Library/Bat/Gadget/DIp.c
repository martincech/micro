//******************************************************************************
//
//  DIp.c          Display IP
//  Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "DIp.h"
#include "Graphic/Graphic.h"
#include "Console/sputchar.h"
#include "Gadget/DLabel.h"         // text label
#include "Console/conio.h"         // putchar
#include "Internet/xIp.h"          // IP

//------------------------------------------------------------------------------
//  IP
//------------------------------------------------------------------------------

void DIp( fnet_ip4_addr_t Ip, int x, int y)
// Display IP
{
   GTextAt( x, y);
   xIp( cputchar, Ip);
} // DIp

//******************************************************************************

//------------------------------------------------------------------------------
//  IP right
//------------------------------------------------------------------------------

void DIpShortRight( fnet_ip4_addr_t Ip, int x, int y)
// Display short Ip
{
char Buffer[ DLABEL_LENGTH_MAX + 1];

   sputcharbuffer( Buffer);
   xIpShort( sputchar, Ip);
   sputchar( '\0');                    // string terminator
   DLabelRight( Buffer, x, y);
} // DTimeRight