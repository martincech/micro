//******************************************************************************
//
//   DList.h       Display list utility
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __DList_H__
   #define __DList_H__

#ifndef __Hardware_H__
   #include "Gadget/DCursor.h"
#endif

#define DLIST_ROWS_COUNT_MAX 6      // max list items per page
//------------------------------------------------------------------------------
//   Functions
//------------------------------------------------------------------------------

void DListClear( TListCursor *Cursor);
// Clear list area

void DListClearWithHeader( TListCursor *Cursor, int HeaderWidth);
// Clear list area and let dont put anything inside header, just clear it

int DListItemHeight();
// get single row height

int DListY( int Row);
// Returns Y coordinate of <Row>

TYesNo DListCursor( TListCursor *Cursor, int Row);
// Draw <Cursor> of <Row>

void DListLine( int x);
// Draw vertical line separator at <x>

void DListHorizontalLine( int y);
// Draw horizontal line separator at <y>

#endif
