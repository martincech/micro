//******************************************************************************
//
//   DInputText.c   Display input text box
//   Version 1.1    (c) VymOs
//
//******************************************************************************

#include "DInput.h"
#include "DLabel.h"
#include "DEnter.h"
#include "DMsg.h"
#include "DLayout.h"
#include "Graphic/Graphic.h"
#include "Str.h"                    // strings from project directory
#include "Fonts.h"                  // project fonts
#include <string.h>

//------------------------------------------------------------------------------
//  Text
//------------------------------------------------------------------------------

TYesNo DInputText( TUniStr Caption, TUniStr Text, char *String, int Width, TYesNo EnableEmpty)
// Input text up to <Width> letters. <EnableEmpty> string
{
   return DInputTextL(Caption, Text, String, Width, EnableEmpty, NULL);
} // DInputText

//------------------------------------------------------------------------------
//  Text with limited letters
//------------------------------------------------------------------------------

TYesNo DInputTextL( TUniStr Caption, TUniStr Text, char *String, int Width, TYesNo EnableEmpty, const char *AllowedLetters)
// Input text up to <Width> letters. <EnableEmpty> string, letters limited to <AllowedLetters>
{
int x;                    // edit field position
int xx;
   SetFont( DINPUT_FONT);
   xx = DEnterTextWidth( Width);
   if( xx > G_WIDTH){
      x = 5; // just some border
   }else {
      x = G_WIDTH / 2 - xx / 2;
   }
   forever {
      // draw widgets :
      GClear();
      DLayoutTitle( Caption);
      DLabelCenter( Text, 0, DINPUT_CAPTION_Y, G_WIDTH, 0);
      DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
      GFlush();
      // edit text :
      if(AllowedLetters == NULL){
         if( !DEnterText( String, Width, x, DINPUT_EDIT_Y)){
            return( NO);                  // escape
         }
      }
      else{
         if( !DEnterTextL( String, Width, x, DINPUT_EDIT_Y, AllowedLetters)){
            return( NO);                  // escape
         }
      }
      StrTrimRight( String);
      if( strlen( String) > 0){
         return( YES);
      }
      // check for empty string enabled :
      if( EnableEmpty){
         return( YES);
      }
      // error message for empty string :
      DMsgOk( STR_ERROR, STR_STRING_EMPTY, 0);
   }
} // DInputTextL
