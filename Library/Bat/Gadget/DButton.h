//******************************************************************************
//                                                                            
//  DButton.h      Display button
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DButton_H__
   #define __DButton_H__

#ifndef __StrDef_H__
   #include "String/StrDef.h"
#endif

void DButton( TUniStr Text, int x, int y, int width, int height);
// Display button with <Text> at position <x,y> with <width,height>

#endif
