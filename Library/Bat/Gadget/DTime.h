//******************************************************************************
//
//  DTime.h        Display time
//  Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __DTime_H__
   #define __DTime_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __uDateTime_H__
   #include "Time/uDateTime.h"
#endif

void DTime( UTimeGauge Now, int x, int y);
// Display time only

void DTimeShort( UTimeGauge Now, int x, int y);
// Display time without seconds

void DDate( UDateGauge Now, int x, int y);
// Display date only

void DDateTime( UDateTimeGauge Now, int x, int y);
// Display date and time

void DDateTimeShort( UDateTimeGauge Now, int x, int y);
// Display date and time without seconds

//------------------------------------------------------------------------------
//   Right aligned
//------------------------------------------------------------------------------

void DTimeRight( UTimeGauge Now, int x, int y);
// Display time only

void DTimeShortRight( UTimeGauge Now, int x, int y);
// Display time without seconds

void DDateRight( UDateGauge Now, int x, int y);
// Display date only

void DDateTimeRight( UDateTimeGauge Now, int x, int y);
// Display date and time

void DDateTimeCenter( UDateTimeGauge Now, int x, int y);
// Display date and time centered


void DDateTimeShortRight( UDateTimeGauge Now, int x, int y);
// Display date and time without seconds

#endif
