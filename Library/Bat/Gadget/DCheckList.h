//******************************************************************************
//                                                                            
//  DCheckList.h   Display check list
//  Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __DCheckList_H__
   #define __DCheckList_H__

#ifndef __StrDef_H__
   #include "String/StrDef.h"
#endif

// Menu definition :
#define DefCheckList( list) const TUniStr list[] = {
#define EndCheckList()      STR_NULL};

// Use :
// DefCheckList( MenuFile)
//    STR_OPTION1,
//    STR_OPTION2,
//    ...
// EndCheckList()

TYesNo DCheckList( TUniStr Caption, const TUniStr *CheckList, dword *Options);
// Displays <CheckList>, returns <Options>

#endif
