//******************************************************************************
//
//    SystemTimer.c  AVR8 System timer
//    Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#if F_CPU != 8000000
   #error "Missing F_CPU clock support"
#endif
   
#define TIMER_COUNT  (125 - 1)     // 1000Hz/1ms tick (8MHz / 64 / 125)

// timer initialization :
#define timerInit()          _timerPeriodSet();_timerStart();timerEnable()

// timer interrupt :
#define timerEnable()        TIMSK0 |=  (1 << OCIE0A)    // enable timer interrupt
#define timerDisable()       TIMSK0 &= ~(1 << OCIE0A)    // disable timer interrupt

// timer handler header :
#define timerHandler()       ISR( TIMER0_COMPA_vect)

// timer interrupt confirm :
#define timerInterruptConfirm()

// timer executive :
#define timerExecute()       _timerUart0() _timerUart1()

//------------------------------------------------------------------------------

// timer period :
#define _timerPeriodSet()     OCR0A = TIMER_COUNT

// WGM 2 (reset mode), COM 0 (no output), prescaler clk/64 :
#define _timerStart()         TCCR0A  = (2 << WGM00); TCCR0B  = (3 << CS00);

// Uart0 timeout :
#ifdef UART0_TIMER
   void Uart0Timer( void);
   #define _timerUart0() Uart0Timer();
#else
   #define _timerUart0()
#endif

// Uart1 timeout :
#ifdef UART1_TIMER
   void Uart1Timer( void);
   #define _timerUart1() Uart1Timer();
#else   
   #define _timerUart1()
#endif
