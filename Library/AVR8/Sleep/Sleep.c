//******************************************************************************
//
//   Sleep.c         Low power controller
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "Unisys/Uni.h"
#include "Hardware.h"
# include <avr/sleep.h>

//-----------------------------------------------------------------------------
// Init
//-----------------------------------------------------------------------------

void SleepInit( void)
// Init
{
} // SleepInit

//-----------------------------------------------------------------------------
// Scheduler
//-----------------------------------------------------------------------------

int SleepScheduler( void)
// Sleep scheduler
{
   //sleep_cpu();
   return 0;
} // SleepScheduler

//-----------------------------------------------------------------------------
// Scheduler
//-----------------------------------------------------------------------------

void SleepEnable( TYesNo Enable)
// <Enable> low power mode
{
   if(Enable) {
      SLEEP.CTRL = SLEEP_SMODE_PSAVE_gc | SLEEP_SEN_bm;
   } else {
      SLEEP.CTRL = SLEEP_SMODE_PSAVE_gc;
   }
} // SleepEnable