//*****************************************************************************
//
//    Eep.h        AVR Eeprom
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Eep_H__
   #define __Eep_H__
   
#include <avr/eeprom.h>

#define EepByteRead( Address)               eeprom_read_byte( (void *)(Address))
// Read byte from <Address>

#define EepWordRead( Address)               eeprom_read_word( (void *)(Address))
// Read word from <Address>

#define EepDwordRead( Address)              eeprom_read_dword( (void *)(Address))
// Read word from <Address>

#define EepByteWrite( Address, Value)       eeprom_write_byte( (void *)(Address), Value)
// Write byte <Value> to <Address>

#define EepWordWrite( Address, Value)       eeprom_write_word( (void *)(Address), Value)
// Write word <Value> to <Address>

#define EepDwordWrite( Address, Value)      eeprom_write_dword( (void *)(Address), Value)
// Write word <Value> to <Address>

#define EepDataLoad( Address, Data, Size)   eeprom_read_block( Data, (void *)(Address), Size)
// Load <Data> with <Size> from <Address>

#define EepDataSave( Address, Data, Size)   eeprom_write_block( Data, (void *)(Address), Size)
// Save <Data> with <Size> at <Address>

#endif
