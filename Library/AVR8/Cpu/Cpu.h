//*****************************************************************************
//
//    Cpu.h       AVR basic macros
//    Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Cpu_H__
   #define __Cpu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#include <avr/interrupt.h>
#include <avr/wdt.h>

void CpuInit( void);
// Init

//-----------------------------------------------------------------------------
// Watchdog
//-----------------------------------------------------------------------------

#ifdef WATCHDOG_ENABLE
   #define WatchDogInit()       wdt_enable(WDTO_2S)
   #define WatchDog()           wdt_reset()
#else
   #define WatchDogInit()
   #define WatchDog()
#endif

//-----------------------------------------------------------------------------
// Interrupt
//-----------------------------------------------------------------------------

#define InterruptDisable()   cli()      // Disable interrupts
#define InterruptEnable()    sei()      // Enable interrupts

//-----------------------------------------------------------------------------
// Intrinsics
//-----------------------------------------------------------------------------

#define Nop()  __asm__ __volatile__ ("nop")

//-----------------------------------------------------------------------------
// Clock
//-----------------------------------------------------------------------------

#define CpuClockDivide( n)   XDIV = (129 - (n)) | (1 << XDIVEN)     // divide clock by n
#define CpuClockNormal()     XDIV = 0                               // disable clock division

#endif
