//*****************************************************************************
//
//    Eint.h       External interrupt services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Eint_H__
   #define __Eint_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// External interrupt sense :
#define EINT_SENSE_LOW_LEVEL    0
#define EINT_SENSE_ANY_EDGE     1
#define EINT_SENSE_FALLING_EDGE 2
#define EINT_SENSE_RISING_EDGE  3

// External interrupt commands :
#define EintDisableAll()        EIMSK  = 0
#define EintDisable( n)         EIMSK &= ~(1 << (n))
#define EintEnable( n)          EIMSK |=  (1 << (n))
#define EintSense( n, Sense)    EICRA &= ~(3 << (2 * (n)));EICRA |= ((Sense) << (2 * (n)))
#define EintClearFlag( n)       EIFR  |=  (1 << (n))
#define EintGetFlag( n)        (EIFR  &  (1 << (n)))


#endif
