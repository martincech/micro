//*****************************************************************************
//
//    Sensors.x     SensorPack sensors
//    Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#include "Sensors.h"
#include "Hardware.h"
#include "Cpu/Cpu.h"
#include "Uart/Uart.h"
#include "IAdc/Iadc.h"

// Resistor divider
#define RESISTOR_DIVIDER_HI      6800
#define RESISTOR_DIVIDER_LO      3300

// Sensor output
#define VOLTAGE_MIN  0
#define VOLTAGE_MAX  10000

// Divided voltage bounds
#define VOLTAGE_DIV_MIN   (VOLTAGE_MIN * RESISTOR_DIVIDER_LO / (RESISTOR_DIVIDER_HI + RESISTOR_DIVIDER_LO))
#define VOLTAGE_DIV_MAX   (VOLTAGE_MAX * RESISTOR_DIVIDER_LO / (RESISTOR_DIVIDER_HI + RESISTOR_DIVIDER_LO))

// ADC reference
#define VOLTAGE_REFERENCE  3300

// ADC raw bounds
#define VOLTAGE_RAW_MIN    VOLTAGE_DIV_MIN * 0xFFFF / VOLTAGE_REFERENCE
#define VOLTAGE_RAW_MAX    VOLTAGE_DIV_MAX * 0xFFFF / VOLTAGE_REFERENCE

void SensorsInit( void) {
   IAdcChannelsInit();
   IAdcInit();
}


#define TEMPERATURE_MIN    0
#define TEMPERATURE_MAX    50

int8 TemperatureRead( void) {
   word TemperatureRaw = IAdcRead( TEMPERATURE_IADC_CHANNEL);
   return TEMPERATURE_MIN + ((TEMPERATURE_MAX - TEMPERATURE_MIN) * (TemperatureRaw - VOLTAGE_RAW_MIN)) / (VOLTAGE_RAW_MAX - VOLTAGE_RAW_MIN);
}


#define HUMIDITY_MIN    0
#define HUMIDITY_MAX    100

uint8 HumidityRead( void) {
   word HumidityRaw = IAdcRead( HUMIDITY_IADC_CHANNEL);
   return HUMIDITY_MIN + ((HUMIDITY_MAX - HUMIDITY_MIN) * (HumidityRaw - VOLTAGE_RAW_MIN)) / (VOLTAGE_RAW_MAX - VOLTAGE_RAW_MIN);
}


#define CO2_MIN    0
#define CO2_MAX    10000ull

uint16 CO2Read( void) {
   word CO2Raw = IAdcRead( CO2_IADC_CHANNEL);
   uint16 val = CO2_MIN + ((CO2_MAX - CO2_MIN) * (CO2Raw - VOLTAGE_RAW_MIN)) / (VOLTAGE_RAW_MAX - VOLTAGE_RAW_MIN);
   return val;
}
