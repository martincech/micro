//*****************************************************************************
//
//    Sensors.h     SensorPack sensors
//    Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#include "Unisys/Uni.h"

typedef struct {
   int16 Temperature;
   int16 Humidity;
   int16 CO2;
   int16 Amonia;
} TSensorsModbusRegisters;


void SensorsInit( void);

int8 TemperatureRead( void);

uint8 HumidityRead( void);

uint16 CO2Read( void);

int8 TemperatureAverageRead( void);
// Read average, starts new averaging period

uint8 HumidityAverageRead( void);
// Read average, starts new averaging period

uint16 CO2AverageRead( void);
// Read average, starts new averaging period

void SensorsSetFromModbus( TSensorsModbusRegisters *Sensors);