//******************************************************************************
//
//   ModbusConfigDataGroup.c      Modbus Sensor data register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "ModbusConfigDataGroup.h"
#include "Hardware.h"

// Locals :


//------------------------------------------------------------------------------
//  Read Sensor data register
//------------------------------------------------------------------------------
word ModbusRegReadConfigData( EModbusRegNum R)
// Read Config Data register group
{

   switch ( R){
      case MODBUS_REG_CONFIG_VERSION :
      {
        return VERSION;
      }
      case MODBUS_REG_CONFIG_BUILD :
      {
        return BUILD;
      }
      case MODBUS_REG_CONFIG_HWVERSION :
      {
        return HW_VERSION;
      }
      default :
         return 0;
   }

   return 0;
}



