//******************************************************************************
//
//   ModbusWeightDataGroup.h  Modbus Weight data register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ModbusWeightDataGroup_H__
   #define __ModbusWeightDataGroup_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif
#ifndef __ModbusReg_H__
   #include "ModbusReg.h"
#endif










//------------------------------------------------------------------------------
word ModbusRegReadWeightData( EModbusRegNum R);
// Read Weight data register group



#endif
