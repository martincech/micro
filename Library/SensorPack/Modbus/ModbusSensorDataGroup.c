//******************************************************************************
//
//   ModbusSensorDataGroup.c      Modbus Sensor data register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "ModbusSensorDataGroup.h"
#include "Sensors/Sensors.h"
#include "ModbusRegRangeCheck.h"




// Locals :


//------------------------------------------------------------------------------
//  Read Sensor data register
//------------------------------------------------------------------------------
word ModbusRegReadSensorData( EModbusRegNum R)
// Read Sensor data register group
{
int8 temperature;
uint16 value;

   switch ( R){
      case MODBUS_REG_SENSOR_DATA_TEMPERATURE :
      {
         temperature = TemperatureRead();
         return temperature;
      }
      case MODBUS_REG_SENSOR_DATA_HUMIDITY :
      {
         value = HumidityRead();
         return value;
      }
      case MODBUS_REG_SENSOR_DATA_CARBON_DIOXIDE :
      {
         value = CO2Read();
         return value;
      }
      case MODBUS_REG_SENSOR_DATA_AMONIA :
      {
         return /* TODO */0;
      }

      default :
         return 0;
   }

   return 0;
}

//------------------------------------------------------------------------------
//  Write Sensor data register
//------------------------------------------------------------------------------
TYesNo ModbusRegWriteSensorData( EModbusRegNum R, word D)
// Write Sensor data register group
{
   return NO;
}




