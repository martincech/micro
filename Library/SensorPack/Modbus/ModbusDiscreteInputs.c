//******************************************************************************
//
//   ModbusDiscreteInputs.c     Modbus process reply - Discrete Input commands
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "mb.h"

eMBErrorCode eMBRegDiscreteCB( UCHAR *pucRegBuffer, USHORT usAddress, USHORT usNCoils)
{
   return eMBRegCoilsCB(pucRegBuffer, usAddress, usNCoils, MB_REG_READ);
}
