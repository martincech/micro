//******************************************************************************
//
//   ModbusConfigDataGroup.h  Modbus Sensor data register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ModbusConfigDataGroup_H__
   #define __ModbusConfigDataGroup_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif
#ifndef __ModbusReg_H__
   #include "ModbusReg.h"
#endif


//------------------------------------------------------------------------------
word ModbusRegReadConfigData( EModbusRegNum R);
// Read Config Data register group



#endif
