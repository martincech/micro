//******************************************************************************
//
//   MenuPlatformDetection.c  Platform detection menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuPlatformDetection.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Platform.h"
#include "MenuDetectionData.h"
#include "MenuDetectionData.h"


static DefMenu( PlatformDetectionMenu)
   STR_FINE,
   STR_COARSE,
EndMenu()

typedef enum {
   MI_FINE,
   MI_COARSE
} EPlatformDetectionMenu;

// Local functions :

static void PlatformDetectionParameters( int Index, int y, TPlatformDetection *Parameters);
// Draw platform detection parameters

//------------------------------------------------------------------------------
//  Menu PlatformDetection
//------------------------------------------------------------------------------

void MenuPlatformDetection( void)
// Edit platform detection parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_DETECTION, PlatformDetectionMenu, (TMenuItemCb *)PlatformDetectionParameters, &PlatformDetection, &MData)){
         ConfigPlatformDetectionSave();
         return;
      }
      switch( MData.Item){
         case MI_FINE :
            MenuDetectionData();
            break;

         case MI_COARSE :
            MenuDetectionData();
            break;

      }
   }
} // MenuPlatformDetection

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void PlatformDetectionParameters( int Index, int y, TPlatformDetection *Parameters)
// Draw platform detection parameters
{
   switch( Index){
      case MI_FINE :
         break;

      case MI_COARSE :
         break;

   }
} // PlatformDetectionParameters
