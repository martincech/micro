//******************************************************************************
//
//   MenuPlatformDiagnostics.c  Platform diagnostics menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuPlatformDiagnostics.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Platform.h"


static DefMenu( PlatformDiagnosticsMenu)
   STR_SAMPLE_SIZE,
   STR_BURST_COUNT,
   STR_AVERAGING,
   STR_FRAME_COUNT,
EndMenu()

typedef enum {
   MI_SAMPLE_SIZE,
   MI_BURST_COUNT,
   MI_AVERAGING,
   MI_FRAME_COUNT
} EPlatformDiagnosticsMenu;

// Local functions :

static void PlatformDiagnosticsParameters( int Index, int y, TPlatformDiagnostics *Parameters);
// Draw platform diagnostics parameters

//------------------------------------------------------------------------------
//  Menu PlatformDiagnostics
//------------------------------------------------------------------------------

void MenuPlatformDiagnostics( void)
// Edit platform diagnostics parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_DIAGNOSTICS, PlatformDiagnosticsMenu, (TMenuItemCb *)PlatformDiagnosticsParameters, &PlatformDiagnostics, &MData)){
         ConfigPlatformDiagnosticsSave();
         return;
      }
      switch( MData.Item){
         case MI_SAMPLE_SIZE :
            i = PlatformDiagnostics.SampleSize;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_DIAGNOSTICS_SAMPLE_SIZE_MIN, PLATFORM_DIAGNOSTICS_SAMPLE_SIZE_MAX, 0)){
               break;
            }
            PlatformDiagnostics.SampleSize = (byte)i;
            break;

         case MI_BURST_COUNT :
            i = PlatformDiagnostics.BurstCount;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_DIAGNOSTICS_BURST_COUNT_MIN, PLATFORM_DIAGNOSTICS_BURST_COUNT_MAX, 0)){
               break;
            }
            PlatformDiagnostics.BurstCount = (byte)i;
            break;

         case MI_AVERAGING :
            i = PlatformDiagnostics.Averaging;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_DIAGNOSTICS_AVERAGING_MIN, PLATFORM_DIAGNOSTICS_AVERAGING_MAX, 0)){
               break;
            }
            PlatformDiagnostics.Averaging = (byte)i;
            break;

         case MI_FRAME_COUNT :
            i = PlatformDiagnostics.FrameCount;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_DIAGNOSTICS_FRAME_COUNT_MIN, PLATFORM_DIAGNOSTICS_FRAME_COUNT_MAX, 0)){
               break;
            }
            PlatformDiagnostics.FrameCount = (byte)i;
            break;

      }
   }
} // MenuPlatformDiagnostics

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void PlatformDiagnosticsParameters( int Index, int y, TPlatformDiagnostics *Parameters)
// Draw platform diagnostics parameters
{
   switch( Index){
      case MI_SAMPLE_SIZE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->SampleSize, 0);
         break;

      case MI_BURST_COUNT :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->BurstCount, 0);
         break;

      case MI_AVERAGING :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->Averaging, 0);
         break;

      case MI_FRAME_COUNT :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->FrameCount, 0);
         break;

   }
} // PlatformDiagnosticsParameters
