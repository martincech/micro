//******************************************************************************
//
//   MenuPlatformAcceptance.h  Platform acceptance menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuPlatformAcceptance_H__
   #define __MenuPlatformAcceptance_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Platform_H__
   #include "Platform.h"
#endif


void MenuPlatformAcceptance( void);
// Menu platform acceptance

#endif
