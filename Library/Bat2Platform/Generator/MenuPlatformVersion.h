//******************************************************************************
//
//   MenuPlatformVersion.h  Platform version menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuPlatformVersion_H__
   #define __MenuPlatformVersion_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Platform_H__
   #include "Platform.h"
#endif


void MenuPlatformVersion( void);
// Menu platform version

#endif
