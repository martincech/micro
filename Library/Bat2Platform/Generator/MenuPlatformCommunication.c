//******************************************************************************
//
//   MenuPlatformCommunication.c  Platform communication menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuPlatformCommunication.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Platform.h"


static DefMenu( PlatformCommunicationMenu)
   STR_BAUD_RATE,
   STR_BUS_ADDRESS,
   STR_FORMAT,
   STR_INTERCHARACTER_TIMEOUT,
   STR_TRANSMITTER_DELAY,

EndMenu()

typedef enum {
   MI_BAUD_RATE,
   MI_BUS_ADDRESS,
   MI_FORMAT,
   MI_INTERCHARACTER_TIMEOUT,
   MI_TRANSMITTER_DELAY,

} EPlatformCommunicationMenu;

// Local functions :

static void PlatformCommunicationParameters( int Index, int y, TPlatformCommunication *Parameters);
// Draw platform communication parameters

//------------------------------------------------------------------------------
//  Menu PlatformCommunication
//------------------------------------------------------------------------------

void MenuPlatformCommunication( void)
// Edit platform communication parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_COMMUNICATION, PlatformCommunicationMenu, (TMenuItemCb *)PlatformCommunicationParameters, &PlatformCommunication, &MData)){
         ConfigPlatformCommunicationSave();
         return;
      }
      switch( MData.Item){
         case MI_BAUD_RATE :
            i = PlatformCommunication.BaudRate;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_COMMUNICATION_BAUD_RATE_MIN, PLATFORM_COMMUNICATION_BAUD_RATE_MAX, "Bd")){
               break;
            }
            PlatformCommunication.BaudRate = (dword)i;
            break;

         case MI_BUS_ADDRESS :
            i = PlatformCommunication.BusAddress;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_COMMUNICATION_BUS_ADDRESS_MIN, PLATFORM_COMMUNICATION_BUS_ADDRESS_MAX, 0)){
               break;
            }
            PlatformCommunication.BusAddress = (byte)i;
            break;

         case MI_FORMAT :
            i = PlatformCommunication.Format;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_SERIAL_FORMAT, _SERIAL_FORMAT_LAST)){
               break;
            }
            PlatformCommunication.Format = (byte)i;
            break;

         case MI_INTERCHARACTER_TIMEOUT :
            i = PlatformCommunication.IntercharacterTimeout;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_COMMUNICATION_INTERCHARACTER_TIMEOUT_MIN, PLATFORM_COMMUNICATION_INTERCHARACTER_TIMEOUT_MAX, "ms")){
               break;
            }
            PlatformCommunication.IntercharacterTimeout = (word)i;
            break;

         case MI_TRANSMITTER_DELAY :
            i = PlatformCommunication.TransmitterDelay;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_COMMUNICATION_TRANSMITTER_DELAY_MIN, PLATFORM_COMMUNICATION_TRANSMITTER_DELAY_MAX, "ms")){
               break;
            }
            PlatformCommunication.TransmitterDelay = (word)i;
            break;

      }
   }
} // MenuPlatformCommunication

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void PlatformCommunicationParameters( int Index, int y, TPlatformCommunication *Parameters)
// Draw platform communication parameters
{
   switch( Index){
      case MI_BAUD_RATE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Parameters->BaudRate, "Bd");
         break;

      case MI_BUS_ADDRESS :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->BusAddress, 0);
         break;

      case MI_FORMAT :
         DLabelEnum( Parameters->Format, ENUM_SERIAL_FORMAT, DMENU_PARAMETERS_X, y);
         break;

      case MI_INTERCHARACTER_TIMEOUT :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Parameters->IntercharacterTimeout, "ms");
         break;

      case MI_TRANSMITTER_DELAY :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Parameters->TransmitterDelay, "ms");
         break;

   }
} // PlatformCommunicationParameters
