//******************************************************************************
//
//   MenuPlatformSigmaDelta.c  Platform sigma delta menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuPlatformSigmaDelta.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Platform.h"


static DefMenu( PlatformSigmaDeltaMenu)
   STR_RATE,
   STR_PREFILTER,
   STR_FILTER,
   STR_CHOP,

EndMenu()

typedef enum {
   MI_RATE,
   MI_PREFILTER,
   MI_FILTER,
   MI_CHOP,

} EPlatformSigmaDeltaMenu;

// Local functions :

static void PlatformSigmaDeltaParameters( int Index, int y, TPlatformSigmaDelta *Parameters);
// Draw platform sigma delta parameters

//------------------------------------------------------------------------------
//  Menu PlatformSigmaDelta
//------------------------------------------------------------------------------

void MenuPlatformSigmaDelta( void)
// Edit platform sigma delta parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_SIGMA_DELTA, PlatformSigmaDeltaMenu, (TMenuItemCb *)PlatformSigmaDeltaParameters, &PlatformSigmaDelta, &MData)){
         ConfigPlatformSigmaDeltaSave();
         return;
      }
      switch( MData.Item){
         case MI_RATE :
            i = PlatformSigmaDelta.Rate;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_SIGMA_DELTA_RATE_MIN, PLATFORM_SIGMA_DELTA_RATE_MAX, "Hz")){
               break;
            }
            PlatformSigmaDelta.Rate = (word)i;
            break;

         case MI_PREFILTER :
            i = PlatformSigmaDelta.Prefilter;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_SIGMA_DELTA_PREFILTER_MIN, PLATFORM_SIGMA_DELTA_PREFILTER_MAX, 0)){
               break;
            }
            PlatformSigmaDelta.Prefilter = (word)i;
            break;

         case MI_FILTER :
            i = PlatformSigmaDelta.Filter;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_SIGMA_DELTA_FILTER, _SIGMA_DELTA_FILTER_LAST)){
               break;
            }
            PlatformSigmaDelta.Filter = (byte)i;
            break;

         case MI_CHOP :
            i = PlatformSigmaDelta.Chop;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            PlatformSigmaDelta.Chop = (byte)i;
            break;

      }
   }
} // MenuPlatformSigmaDelta

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void PlatformSigmaDeltaParameters( int Index, int y, TPlatformSigmaDelta *Parameters)
// Draw platform sigma delta parameters
{
   switch( Index){
      case MI_RATE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Parameters->Rate, "Hz");
         break;

      case MI_PREFILTER :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->Prefilter, 0);
         break;

      case MI_FILTER :
         DLabelEnum( Parameters->Filter, ENUM_SIGMA_DELTA_FILTER, DMENU_PARAMETERS_X, y);
         break;

      case MI_CHOP :
         DLabelEnum( Parameters->Chop, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

   }
} // PlatformSigmaDeltaParameters
