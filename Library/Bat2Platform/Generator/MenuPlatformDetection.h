//******************************************************************************
//
//   MenuPlatformDetection.h  Platform detection menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuPlatformDetection_H__
   #define __MenuPlatformDetection_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Platform_H__
   #include "Platform.h"
#endif


void MenuPlatformDetection( void);
// Menu platform detection

#endif
