//******************************************************************************
//
//   MenuPlatformDiagnostics.h  Platform diagnostics menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuPlatformDiagnostics_H__
   #define __MenuPlatformDiagnostics_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Platform_H__
   #include "Platform.h"
#endif


void MenuPlatformDiagnostics( void);
// Menu platform diagnostics

#endif
