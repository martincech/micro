//******************************************************************************
//
//   MenuPlatformDetectionCoarse.h  Platform detection coarse menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuPlatformDetectionCoarse_H__
   #define __MenuPlatformDetectionCoarse_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Platform_H__
   #include "Platform.h"
#endif


void MenuPlatformDetectionCoarse( void);
// Menu platform detection coarse

#endif
