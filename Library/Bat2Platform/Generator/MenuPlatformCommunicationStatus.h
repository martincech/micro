//******************************************************************************
//
//   MenuPlatformCommunicationStatus.h  Platform communication status menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuPlatformCommunicationStatus_H__
   #define __MenuPlatformCommunicationStatus_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Platform_H__
   #include "Platform.h"
#endif


void MenuPlatformCommunicationStatus( void);
// Menu platform communication status

#endif
