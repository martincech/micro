//******************************************************************************
//
//   MenuPlatformVersion.c  Platform version menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuPlatformVersion.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Platform.h"
#include "MenuDeviceVersion.h"


static DefMenu( PlatformVersionMenu)
   STR_DEVICE,
   STR_WEIGHT_MAX,
   STR_CALIBRATION_TIME,
   STR_SAMPLES_COUNT,
   STR_SENSOR_INVERSION,

EndMenu()

typedef enum {
   MI_DEVICE,
   MI_WEIGHT_MAX,
   MI_CALIBRATION_TIME,
   MI_SAMPLES_COUNT,
   MI_SENSOR_INVERSION,

} EPlatformVersionMenu;

// Local functions :

static void PlatformVersionParameters( int Index, int y, TPlatformVersion *Parameters);
// Draw platform version parameters

//------------------------------------------------------------------------------
//  Menu PlatformVersion
//------------------------------------------------------------------------------

void MenuPlatformVersion( void)
// Edit platform version parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_VERSION, PlatformVersionMenu, (TMenuItemCb *)PlatformVersionParameters, &PlatformVersion, &MData)){
         ConfigPlatformVersionSave();
         return;
      }
      switch( MData.Item){
         case MI_DEVICE :
            MenuDeviceVersion();
            break;

         case MI_WEIGHT_MAX :
            i = PlatformVersion.WeightMax;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            PlatformVersion.WeightMax = (TWeightGauge)i;
            break;

         case MI_CALIBRATION_TIME :
            uDateTime( &DateTime, PlatformVersion.CalibrationTime);
            if( !DInputDateTime( STR_$UITEM$, STR_ENTER_$UITEM$, &DateTime)){
               break;
            }
            PlatformVersion.CalibrationTime = uDateTimeGauge( &DateTime);
            break;

         case MI_SAMPLES_COUNT :
            i = PlatformVersion.SamplesCount;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_VERSION_SAMPLES_COUNT_MIN, PLATFORM_VERSION_SAMPLES_COUNT_MAX, 0)){
               break;
            }
            PlatformVersion.SamplesCount = (word)i;
            break;

         case MI_SENSOR_INVERSION :
            i = PlatformVersion.SensorInversion;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            PlatformVersion.SensorInversion = (byte)i;
            break;

      }
   }
} // MenuPlatformVersion

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void PlatformVersionParameters( int Index, int y, TPlatformVersion *Parameters)
// Draw platform version parameters
{
   switch( Index){
      case MI_DEVICE :
         break;

      case MI_WEIGHT_MAX :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->WeightMax);
         break;

      case MI_CALIBRATION_TIME :
         DDateTimeRight( Parameters->CalibrationTime, DMENU_PARAMETERS_X, y);
         break;

      case MI_SAMPLES_COUNT :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->SamplesCount, 0);
         break;

      case MI_SENSOR_INVERSION :
         DLabelEnum( Parameters->SensorInversion, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

   }
} // PlatformVersionParameters
