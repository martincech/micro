//******************************************************************************
//
//   MenuPlatformStatus.h  Platform status menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuPlatformStatus_H__
   #define __MenuPlatformStatus_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Platform_H__
   #include "Platform.h"
#endif


void MenuPlatformStatus( void);
// Menu platform status

#endif
