//******************************************************************************
//
//   Status.h     Device status handling
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Status_H__
   #define __Status_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __PlatformDef_H__
   #include "Platform/PlatformDef.h"
#endif


typedef enum {
   STATUS_DISPLAY_OFF,                 // display off
   STATUS_DISPLAY_START,               // flash at start
   STATUS_DISPLAY_ZERO,                // wait for zero load
   STATUS_DISPLAY_ZERO_WEIGHING,       // wait for zero weighing,
   STATUS_DISPLAY_LOAD,                // wait for full range load
   STATUS_DISPLAY_LOAD_WEIGHING,       // wait for full range weighing
   STATUS_DISPLAY_CALIBRATION_OK,      // calibration succesfully saved
   STATUS_DISPLAY_CALIBRATION_ERROR,   // wrong calibration
   STATUS_DISPLAY_CALIBRATION_STOP,    // stop calibration
   _STATUS_DISPLAY_LAST
} EDeviceStatus;

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void StatusInit( void);
// Initialize

void StatusExecute( void);
// Main executive

void StatusErrorSet( byte Error);
// Set <Error> state

void StatusOperationSet( byte Operation);
// Set current <Operation>

void StatusDisplay( byte Status);
// Display temporary status

#endif
