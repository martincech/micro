//*****************************************************************************
//
//    Scheduler.c  Bat2 wired platform scheduler
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Hardware.h"
#include "System/System.h"
#include "Remote/Rcs.h"              // Remote control server
#include "Weighing/pWeighing.h"       // Weighing operations
#include "Diagnostic/pDiagnostic.h"   // Diagnostic
#include "Device/Status.h"           // Status display

//------------------------------------------------------------------------------
// Scheduler
//------------------------------------------------------------------------------

int SysScheduler( void)
// Scheduler cycle, returns any event
{
int Key;

   WatchDog();
   RcsExecute();                       // remote control server
   // standard scheduler :
   Key = SysSchedulerDefault();
   switch( Key){
      case K_TIMER_FAST :
         return( K_IDLE);              // disable fast timer event

      case K_TIMER_SLOW :
         pWeighingExecute();            // weighing executive
         StatusExecute();              // status executive
         break;

      default :
         break;
   }
   return( Key);
} // SysScheduler
