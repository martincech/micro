//*****************************************************************************
//
//    main.c       Bat2 wired platform main
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Hardware.h"
#include "System/System.h"
#include "Kbd/Kbd.h"
#include "Platform/Platform.h"           // Platform
#include "Remote/Rcs.h"                  // Remote control server
#include "Config/Configuration.h"        // Platform configuration
#include "Calibration/Calibration.h"     // Calibration record
#include "Weighing/Weighing.h"           // Weighing operations
#include "Menu/Menu.h"                   // Menu operations
#include "Device/Status.h"               // Status display
static void Bootloader( void);
//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
   CpuInit();
   WatchDogInit();
   EepromInit();
   Bootloader();
   RcsInit();                          // remote control server
   // initialize devices :
   StatusLedInit();
   BarInit();
   KbdInit();
   StatusInit();                          // status display init
   StatusDisplay( STATUS_DISPLAY_START);  // flash on start
   // load configuration:
   if( !ConfigLoad()){
      StatusErrorSet( PLATFORM_ERROR_CONFIGURATION);
   }
   // check for power up key :
   if( KbdPowerUpKey() == K_MATCHING){
      KbdPowerUpRelease();
      StatusOperationSet( PLATFORM_OPERATION_MATCHING);
   } else {
      StatusOperationSet( PLATFORM_OPERATION_STOP);
   }
   // subsystems :
  
   RcsAddressAssign(0);
   pWeighingInit();                     // weighing subsystem
   WatchDog();                         // update watchdog
   // start system :
   SysInit();
   // main loop :
   MenuMain();
   return( 0);
} // main

//------------------------------------------------------------------------------
//   Timer executive
//------------------------------------------------------------------------------

void SysTimerExecute( void)
// Timer user executive
{
   UartTimer();
} // SysTimerExecute

#include "Cpu/Cpu.h"

#include "Flash/IFlash.h"

static void Bootloader( void) {
   TPlatformVersion PlatformVersion;
    #warning Predpoklada se zarovnani clenu SerialNumber na hranici dword
   if(((int)&pPlatformVersion.Device.SerialNumber) % sizeof(dword)) {
      return;
   }
   if(pPlatformVersion.Device.SerialNumber != SERIAL_NUMBER_INVALID) {
      return;
   }
   memset(&PlatformVersion, 0xFF, sizeof(PlatformVersion));
   IFlashReadOnce(60, &PlatformVersion.Device.SerialNumber, sizeof(PlatformVersion.Device.SerialNumber));

   IFlashProgram(&pPlatformVersion.Device.SerialNumber, &PlatformVersion.Device.SerialNumber, sizeof(PlatformVersion.Device.SerialNumber));
}