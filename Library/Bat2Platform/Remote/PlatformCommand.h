//*****************************************************************************
//
//    PlatformCommand.h  Remote control command processing
//    Version 1.0        (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __PlatformCommand_H__
   #define __PlatformCommand_H__

#ifndef __PlatformRpc_H__
   #include "Platform/PlatformRpc.h"
#endif   

#ifdef __cplusplus
   extern "C" {
#endif

//------------------------------------------------------------------------------
//  Command
//------------------------------------------------------------------------------
   
void PlatformCommandInit( void);
// Initialize
   
TYesNo PlatformCommand( TPlatformCommand *Command, word CommandSize, TPlatformReply *Reply, word *ReplySize);
// Execute received command

//------------------------------------------------------------------------------

#ifdef __cplusplus
   }
#endif
  
#endif
