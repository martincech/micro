//*****************************************************************************
//
//    CommonCommand.c  Remote control command processing
//    Version 1.0     (c) VEIT Electronics
//
//*****************************************************************************

#include "CommonCommand.h"
#include "Device/CommonCommandDef.h"

//-----------------------------------------------------------------------------
// Initialize
//-----------------------------------------------------------------------------

void CommonCommandInit( void)
// Initialize
{
} // CommonCommandInit

//-----------------------------------------------------------------------------
// Command
//-----------------------------------------------------------------------------

TYesNo CommonCommand( TCommonCommand *Command, word CommandSize, TCommonReply *Reply, word *ReplySize)
// Execute received command
{
   switch( Command->Command){
      case COMMON_CMD_DISCOVER :
         if( CommandSize != CommonSimpleCommandSize()){
            return NO;
         }
         //Reply.Data.Discover.Version = XYZ;
         *ReplySize = CommonReplySize( DiscoverReply);
         break;

      case COMMON_CMD_ADDRESS_ASSIGN:
         if( CommandSize != CommonCommandSize( AddressAssign)){
            return NO;
         }
         /*if(Command.Data.AddressAssign.Version != XYZ) {
            return NO;
         }
         AddressAssign(Command.Data.AddressAssign.Address);*/
         *ReplySize = CommonSimpleReplySize();
         break;
         
      default:
         return NO;
   }

   return YES;
} // CommonCommand
