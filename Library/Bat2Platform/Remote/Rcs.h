//*****************************************************************************
//
//    Rcs.h        Remote control server
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Rcs_H__
   #define __Rcs_H__

#ifndef __Hardware_H__
   #include "Hardware.h"               // keycodes
#endif

void RcsInit( void);
// Initialize

void RcsExecute( void);
// Server executive
   
#endif
