//******************************************************************************
//
//   CalibrationDef.h  Calibration definition
//   Version 0.0       (c) VEIT Electronics
//
//******************************************************************************

#ifndef __CalibrationDef_H__
   #define __CalibrationDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __WeightDef_H__
   #include "Weight/WeightDef.h"
#endif

#ifdef OPTION_SIGMA_DELTA
   #include "Calibration/Sigma/CalibrationDef.h"
#else
   #include "Calibration/Pico/CalibrationDef.h"
#endif

#endif
