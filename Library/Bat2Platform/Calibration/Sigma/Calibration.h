//******************************************************************************
//
//   Calibration.h  SigmaDelta calibration utility
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

TYesNo CalibrationUpdate( byte Points, TRawWeight *Raw, TWeightGauge *Weight);
// Update calibration

TWeightGauge CalibrationRangeGet( void);
// Returns physical range

UDateTimeGauge CalibrationTime( void);
// Returns last calibration time

TYesNo CalibrationInversion( void);
// Returns bridge polarity inversion

TWeightGauge CalibrationWeight( TRawWeight RawValue);
// Calculate physical weight, <RawValue> inversed already by user according to CalibrationInversion()

TRawWeight CalibrationRawWeight( TWeightGauge Value);
// Calculate raw weight of physical <Value>
