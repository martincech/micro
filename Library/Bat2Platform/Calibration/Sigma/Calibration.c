//******************************************************************************
//
//   Calibration.c  SigmaDelta calibration utility
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Calibration/Calibration.h"
#include "Config/Config.h"
#include "System/System.h"
#include <string.h>

TCalibration Calibration;

const TCalibration CalibrationDefault = {
   /* Time */       0,
   /* Points */     2,
   /* Raw */        {0, 1},
   /* Weight */     {0, 1},
   /* Inversion */  0,
   /* _Spare */     0,
   /* Crc */        0
};

//------------------------------------------------------------------------------
// Update
//------------------------------------------------------------------------------

TYesNo CalibrationUpdate( byte Points, TRawWeight *Raw, TWeightGauge *Weight)
// Update calibration
{
int i;
   if(Points < 2) {
      return NO;
   }
   // inverse, if neccessary
   Calibration.Inversion = NO;
   if(Raw[0] > Raw[1]) {
      Calibration.Inversion = YES;
      for(i = 0 ; i < Points ; i++) {
         Raw[i] = -Raw[i];
      }
   }
   // sequence must be rising
   for(i = 0 ; i < Points - 1 ; i++) {
      if(Raw[i] >= Raw[i + 1]) {
         return NO;
      }
      if(Weight[i] >= Weight[i + 1]) {
         return NO;
      }
   }
   Calibration.Points = Points;
   memcpy(Calibration.Raw, Raw, sizeof(*Raw) * Points);
   memcpy(Calibration.Weight, Weight, sizeof(*Weight) * Points);
   Calibration.Time = SysClock();
   return YES;
} // CalibrationUpdate

//------------------------------------------------------------------------------
// Inversion
//------------------------------------------------------------------------------

UDateTimeGauge CalibrationTime( void)
// Returns last calibration time
{
   return( Calibration.Time);
} // CalibrationInversion

//------------------------------------------------------------------------------
// Inversion
//------------------------------------------------------------------------------

TYesNo CalibrationInversion( void)
// Returns bridge polarity inversion
{
   return( Calibration.Inversion);
} // CalibrationInversion

//------------------------------------------------------------------------------
// Weight
//------------------------------------------------------------------------------

TWeightGauge CalibrationWeight( TRawWeight RawValue)
// Calculate physical weight of <RawValue>
{
int i;
TMulWeight tmp;
TWeightGauge Weight;
   if(RawValue <= Calibration.Raw[0]) { // outside - lower?
      i = 0;
   } else if(RawValue >= Calibration.Raw[Calibration.Points - 1]) { // outside - higher?
      i = Calibration.Points - 2;
   } else { // inside
      for(i = 0 ; i < Calibration.Points - 1 ; i++) {
         if(RawValue >= Calibration.Raw[i]) {
            break;
         }
      }
   }
   // interpolate
   tmp = (TMulWeight)(RawValue - Calibration.Raw[i]) * (Calibration.Weight[i + 1] - Calibration.Weight[i]);
   Weight = (TWeightGauge)(tmp / (Calibration.Raw[i + 1] - Calibration.Raw[i])) + Calibration.Weight[i];
   return Weight;
} // CalibrationWeight

//------------------------------------------------------------------------------
// Raw Weight
//------------------------------------------------------------------------------

TRawWeight CalibrationRawWeight( TWeightGauge Weight)
// Calculate raw weight of physical <Value>
{
int i;
TMulWeight tmp;
TRawWeight Raw;
   if(Weight <= Calibration.Weight[0]) { // outside - lower?
      i = 0;
   } else if(Weight >= Calibration.Weight[Calibration.Points - 1]) { // outside - higher?
      i = Calibration.Points - 2;
   } else { // inside
      for(i = 0 ; i < Calibration.Points - 1 ; i++) {
         if(Weight >= Calibration.Weight[i]) {
            break;
         }
      }
   }
   // interpolate
   tmp = (TMulWeight)(Weight - Calibration.Weight[i]) * (Calibration.Raw[i + 1] - Calibration.Raw[i]);
   Raw = (TRawWeight)(tmp / (Calibration.Weight[i + 1] - Calibration.Weight[i])) + Calibration.Raw[i];
   return Raw;
} // CalibrationRawWeight
