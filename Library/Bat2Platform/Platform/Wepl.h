//******************************************************************************
//
//   Wepl.h         Weighing platform communication
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Wepl_H__
   #define __Wepl_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __PlatformRcp_H__
   #include "Platform/PlatformRpc.h"
#endif

#ifdef __cplusplus
   #include "Uart/WinNative.h"
   #include "Uart/UartParameters.h"
   #include "Crt/CrtDump.h"

   TYesNo WeplSetup( CrtDump *dump, UartParameters *parameters);
   // Setup communication parameters
#endif

#ifdef __cplusplus
   extern "C" {
#endif

//------------------------------------------------------------------------------
//  Basic
//------------------------------------------------------------------------------

TYesNo WeplInit( void);
// Initialize

void WeplAddressSet( TPlatformAddress Address);
// Set current platform <Address>

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

TYesNo WeplVersionGet( TPlatformVersion *Version);
// Get device version info

TYesNo WeplStatusGet( TPlatformStatus *Status);
// Get device status

TYesNo WeplClockSet( TPlatformClock Clock);
// Set device clock


#ifdef OPTION_SIGMA_DELTA
   TYesNo WeplSigmaDeltaSet( TPlatformSigmaDelta *SigmaDelta);
   // Set sigma-delta configuration
#endif   

#ifdef OPTION_PICOSTRAIN
   TYesNo WeplPicostrainSet( TPlatformPicostrain *Picostrain);
   // Set picostrain configuration
#endif

TYesNo WeplDetectionSet( TPlatformDetection *Detection);
// Set detection parameters

TYesNo WeplAcceptanceSet( TPlatformAcceptance *Acceptance);
// Set acceptance parameters

TYesNo WeplCalibrationSet( TPlatformCalibration *Calibration);
// Set calibration parameters

//------------------------------------------------------------------------------
//  Actions
//------------------------------------------------------------------------------

TYesNo WeplWeighingStart( void);
// Start weighing

TYesNo WeplDiagnosticStart( void);
// Start diagnostics

TYesNo WeplStop( void);
// Stop weighing/diagnostics

//------------------------------------------------------------------------------
//  Data
//------------------------------------------------------------------------------

TYesNo WeplSavedWeightGet( word *Count, TPlatformWeight *Sample);
// Returns saved <Sample>s with <Count>

TYesNo WeplSavedWeightConfirm( word Count);
// Confirm <Count> saved weights

TYesNo WeplDiagnosticWeightGet( dword IdIn, dword *IdOut, TDiagnosticFrame *Frame);
// Get diagnostic data

//------------------------------------------------------------------------------
//  Communication
//------------------------------------------------------------------------------

TYesNo WeplCommunicationSet( TPlatformCommunication *Communication);
// Set communication parameters (warning : parameters are set immediately)

TYesNo WeplCommunicationStatusGet( TPlatformCommunicationStatus *CommunicationStatus);
// Get communication status

//------------------------------------------------------------------------------

#ifdef __cplusplus
   }
#endif
   
#endif
