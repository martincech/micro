//******************************************************************************
//
//   Platform.h     Platform services
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Platform_H__
   #define __Platform_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __PlatformDef_H__
   #include "Platform/PlatformDef.h"
#endif

#ifndef __uConst_H__
   #include "Data/uConst.h"
#endif

//------------------------------------------------------------------------------
//  Internal data
//------------------------------------------------------------------------------

#define PlatformOperation()          PlatformStatus.Operation
#define PlatformOperationSet( o)     PlatformStatus.Operation = o
#define PlatformError()              PlatformStatus.Error
#define PlatformErrorSet( e)         PlatformStatus.Error = e
#define PlatformPower()              PlatformStatus.Power
#define PlatformPowerSet( p)         PlatformStatus.Power = p
#define PlatformActualWeightSet( w)  PlatformStatus.Weight = w
#define PlatformSamplesCountSet( s)  PlatformStatus.SamplesCount = s

//------------------------------------------------------------------------------
//  Data
//------------------------------------------------------------------------------

// device status :
extern TPlatformStatus                 PlatformStatus;
extern TPlatformDiagnostics            PlatformDiagnostics;

// weighing parameters :
#ifdef OPTION_SIGMA_DELTA
   extern                TPlatformSigmaDelta  PlatformSigmaDelta;
   extern uConstDeclare( TPlatformSigmaDelta) PlatformSigmaDeltaDefault;
#endif   
#ifdef OPTION_PICOSTRAIN
   extern                TPlatformPicostrain  PlatformPicostrain;
   extern uConstDeclare( TPlatformPicostrain) PlatformPicostrainDefault;
#endif
extern TPlatformDetection              PlatformDetection;
extern TPlatformAcceptance             PlatformAcceptance;
extern TPlatformCalibration            PlatformCalibration;

// communication :
extern TPlatformCommunication          PlatformCommunication;
extern TPlatformCommunicationStatus    PlatformCommunicationStatus;

// defaults :
extern uConstDeclare( TPlatformDetection)     PlatformDetectionDefault;
extern uConstDeclare( TPlatformAcceptance)    PlatformAcceptanceDefault;
extern uConstDeclare( TPlatformCalibration)   PlatformCalibrationDefault;
extern uConstDeclare( TPlatformCommunication) PlatformCommunicationDefault;
extern uConstDeclare( TPlatformDiagnostics)   PlatformDiagnosticsDefault;

// fixed version record :
extern uConstDeclare( TPlatformVersion) pPlatformVersion;

// default values :
extern uConstDeclare( TPlatformCommunication) PlatformCommunicationDefault;

#ifdef __cplusplus
   extern "C" {
#endif

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void PlatformInit( void);
// Initialize

//------------------------------------------------------------------------------
#ifdef __cplusplus
   }
#endif

#endif
