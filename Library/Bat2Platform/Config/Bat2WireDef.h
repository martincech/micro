//******************************************************************************
//
//   Bat2WireDef.h  Bat2 RS485 platform
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Bat2WireDef_H__
   #define __Bat2WireDef_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

//------------------------------------------------------------------------------
//  Version info
//------------------------------------------------------------------------------

#define BAT2_SW_VERSION   0x0100
//#define BAT2_HW_VERSION   0x0100 -> Bat2Hardware.h

//------------------------------------------------------------------------------
//  Device data
//------------------------------------------------------------------------------

#define BAT2_WEIGHT_MAX  500000L       // Maximum load [0.1g]
#define BAT2_SAMPLES_MAX 100           // Maximum samples FIFO size

#endif
