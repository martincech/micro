//-----------------------------------------------------------------------------
//
//    ConfigurationDef.h  Configuration data definitions
//    Version 1.0         (c) VEIT Electronics
//
//-----------------------------------------------------------------------------

#ifndef __ConfigurationDef_H__
   #define __ConfigurationDef_H__

#ifndef __PlatformDef_H__
   #include "Platform/PlatformDef.h"
#endif
   
//-----------------------------------------------------------------------------
// Constants
//-----------------------------------------------------------------------------

#define CONFIGURATION_VERSION_MAJOR        1            // Major version number
#define CONFIGURATION_VERSION_MINOR        0            // Minor version number

#define CONFIGURATION_VERSION      ((CONFIGURATION_VERSION_MAJOR << 8) | CONFIGURATION_VERSION_MINOR)

//-----------------------------------------------------------------------------
// Types
//-----------------------------------------------------------------------------

typedef word TConfigurationCrc;

//-----------------------------------------------------------------------------
// Configuration record
//-----------------------------------------------------------------------------

typedef struct {
   word                    Version;         // Version
   TPlatformCommunication  Communication;   // Communication parameters
   TConfigurationCrc       Crc;             // Device configuration checksum
} TConfiguration;

#endif
