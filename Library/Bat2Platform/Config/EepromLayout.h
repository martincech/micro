//-----------------------------------------------------------------------------
//
//    EepromLayout.h  EEPROM layout definitions
//    Version 1.0     (c) VEIT Electronics
//
//-----------------------------------------------------------------------------

#ifndef __EepromLayout_H__
   #define __EepromLayout_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __CalibrationDef_H__
   #include "Calibration/CalibrationDef.h"
#endif

#ifndef __ConfigurationDef_H__
   #include "Config/ConfigurationDef.h"
#endif

//-----------------------------------------------------------------------------
// EEPROM constants
//-----------------------------------------------------------------------------

#define EEPROM_CALIBRATION_SIZE        (32)         // calibration reserved space
#define EEPROM_CALIBRATION_SPARE       (EEPROM_CALIBRATION_SIZE - sizeof( TCalibration))
#define EEPROM_CONFIGURATION_SIZE      (31)         // configuration reserved space
#define EEPROM_CONFIGURATION_SPARE     (EEPROM_CONFIGURATION_SIZE - sizeof( TConfiguration))

#if EEPROM_CALIBRATION_SIZE + EEPROM_CONFIGURATION_SIZE > EEPROM_SIZE
   #error Eeprom overflow
#endif

//-----------------------------------------------------------------------------
// EEPROM layout
//-----------------------------------------------------------------------------

typedef struct {
   TCalibration   Calibration;
   byte           _Spare0[ EEPROM_CALIBRATION_SPARE];      // calibration spare space
   TConfiguration Configuration;                           // device configuration data
   byte           _Spare1[ EEPROM_CONFIGURATION_SPARE];    // configuration spare space
} TEepromLayout;


#endif
