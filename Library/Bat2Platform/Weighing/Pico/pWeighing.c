//******************************************************************************
//
//   Weighing.c   Weighing utilities
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "Weighing/Weighing.h"
#include "System/System.h"                 // Operating system
#include "AdcPs081/AdcPs081.h"             // A/D converter
#include "Platform/Platform.h"             // Platform data
#include "Fifo/Fifo.h"                     // Samples FIFO
#include "Diagnostic/Diagnostic.h"         // Diagnostic utility

// Local data :
static TWeightGauge _ActualWeight;         // currently measured weight

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------

void pWeighingInit( void)
// Initialize weighing
{
   AdcInit();
   FifoInit();
   _ActualWeight = 0;
} // WeighingInit

//------------------------------------------------------------------------------
// Start
//------------------------------------------------------------------------------

void pWeighingStart( void)
// Power up start weighing
{
   _ActualWeight = 0;
   FifoInit();                         // clear samples FIFO
   AdcOutputModeSet(ADC_PS081_OUTPUT_MODE_STABLE);
   AdcModeSet(PlatformPicostrain.Mode);
   AdcDoSwitchEnable(PlatformPicostrain.AccuracySwitch);
   AdcRateSet(PlatformPicostrain.Rate);
   AdcFilterSet(PlatformPicostrain.Filter);
   AdcAccuracySet(PlatformPicostrain.AccuracyFine, PlatformPicostrain.AccuracyCoarse);
   AdcPrefilterSet(PlatformPicostrain.Prefilter);
   AdcDetectionSet(PlatformDetection.Fine.AveragingWindow,
                   PlatformDetection.Coarse.AveragingWindow,
                   PlatformDetection.Fine.StableWindow,
                   PlatformDetection.Coarse.StableWindow,
                   PlatformDetection.Fine.AbsoluteRange,
                   PlatformDetection.Coarse.AbsoluteRange,
                   PlatformDetection.Fine.SwitchoverRange,
                   PlatformDetection.Coarse.SwitchoverRange); 
   AdcStart();
} // WeighingStart

//------------------------------------------------------------------------------
// Stop
//------------------------------------------------------------------------------

void pWeighingStop( void)
// Power down stop weighing
{
   AdcStop();
   DiagnosticStop();                   // stop diagnostic mode
   _ActualWeight = 0;
} // WeighingStop

//------------------------------------------------------------------------------
// Diagnostic
//------------------------------------------------------------------------------

void WeighingDiagnosticStart( void)
// Start diagnostics
{
   DiagnosticStart();                  // start diagnostic mode
} // WeighingDiagnosticStart

//------------------------------------------------------------------------------
// Execute
//------------------------------------------------------------------------------

void pWeighingExecute( void)
// Execute automatic weighing
{
   if( PlatformOperation() != PLATFORM_OPERATION_WEIGHING){
      return;
   }
   // set actual value :
   _ActualWeight = AdcLowPassRead();
} // WeighingExecute

//------------------------------------------------------------------------------
// Weight
//------------------------------------------------------------------------------

TWeightGauge WeighingWeight( void)
// Returns actual weight
{
   return( _ActualWeight);
} // WeighingWeight
