//******************************************************************************
//
//   pDiagnosticFifo.c     Diagnostic Samples FIFO
//   Version 1.0           (c) VEIT Electronics
//
//******************************************************************************

#include "pDiagnosticFifo.h"
#include "System/System.h"        // Operating system
#include "Time/uTime.h"
#include "Cpu/Cpu.h"

#ifdef __WIN32__
   #pragma pack( push, 1)                   // byte alignment
#endif

typedef struct {
   byte ShortTimestamp;
   TDiagnosticWeight Weight;
} __packed TDiagnosticFifoSampleInternal;

#ifdef __WIN32__
   #pragma pack( pop)                       // original alignment
#endif

#define PDIAGNOSTIC_FIFO_SIZE    128

static TDiagnosticFifoCount _MarkCount;
static TDiagnosticFifoCount _ReadIndex;
static TDiagnosticFifoCount _WriteIndex;
static TDiagnosticFifoCount _Overrun;
static TDiagnosticFifoSample _Fifo[ PDIAGNOSTIC_FIFO_SIZE];

#define FifoFull()    ((_ReadIndex + PDIAGNOSTIC_FIFO_SIZE) == _WriteIndex)
#define FifoDelete()   (_ReadIndex++)
#define FifoWrite( w)   _Fifo[ _WriteIndex++ & (PDIAGNOSTIC_FIFO_SIZE - 1)] = w
#define FifoRead( i)    _Fifo[ (_ReadIndex + i) & (PDIAGNOSTIC_FIFO_SIZE - 1)]
#define FifoFlush( c)   _ReadIndex += c

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------

void pDiagnosticFifoInit( void)
// Initialize
{
   InterruptDisable();
   _MarkCount  = 0;
   _ReadIndex  = 0;
   _WriteIndex = 0;
   _Overrun = 0;
   InterruptEnable();
} // FifoInit

//------------------------------------------------------------------------------
// Put
//------------------------------------------------------------------------------

void pDiagnosticFifoPut( TDiagnosticFifoSample Weight)
// Save <Weight>
{
   InterruptDisable();
   if( FifoFull()){
      _Overrun++;
      FifoDelete();                    // remove oldest item
   }
   FifoWrite( Weight);
   InterruptEnable();
} // FifoPut

//------------------------------------------------------------------------------
// Count
//------------------------------------------------------------------------------

TDiagnosticFifoCount pDiagnosticFifoCount( void)
// Returns samples count
{
TDiagnosticFifoCount Count;
   InterruptDisable();
   Count =  _WriteIndex - _ReadIndex;
   InterruptEnable();
   return Count;
} // FifoCount

//------------------------------------------------------------------------------
// Overrun
//------------------------------------------------------------------------------

TDiagnosticFifoCount pDiagnosticFifoOverrun( void)
// Returns lost samples
{
TDiagnosticFifoCount RetOverrun;
   InterruptDisable();
   RetOverrun = _Overrun;
   InterruptEnable();
   return( RetOverrun);
} // pDiagnosticFifoOverrun

//------------------------------------------------------------------------------
// Mark
//------------------------------------------------------------------------------

void pDiagnosticFifoMark( TDiagnosticFifoCount Count)
// Mark <Count> items for read
{
   InterruptDisable();
   _MarkCount = Count;
   InterruptEnable();
} // FifoMark

//------------------------------------------------------------------------------
// Get
//------------------------------------------------------------------------------

TYesNo pDiagnosticFifoGet( TDiagnosticFifoCount Index, TDiagnosticFifoSample *Weight)
// Returns <Weight> at <Index>
{
   InterruptDisable();
   _Overrun = 0;
   if( Index >= pDiagnosticFifoCount()){
      InterruptEnable();
      return( NO);
   }
   *Weight = FifoRead( Index);
   InterruptEnable();
   return( YES);
} // FifoGet

//------------------------------------------------------------------------------
// Remove
//------------------------------------------------------------------------------

TYesNo pDiagnosticFifoRemove( TDiagnosticFifoCount Count)
// Remove <Count> items
{
TDiagnosticFifoCount TotalCount;
   InterruptDisable();
   if( _MarkCount != Count){
      InterruptEnable();
      return( NO);
   }
   _MarkCount = 0;
   TotalCount = pDiagnosticFifoCount();
   if( TotalCount < Count){
      Count = TotalCount;
   }
   if(_Overrun > Count) { // remove from overrun
      _Overrun -= Count;
   } else if(_Overrun > 0) {
      FifoFlush(Count - _Overrun);
      _Overrun = 0;
   } else { // remove from fifo
      FifoFlush( Count);
   }
   InterruptEnable();
   return( YES);
} // FifoRemove
