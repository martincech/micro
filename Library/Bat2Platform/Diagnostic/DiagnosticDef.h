//******************************************************************************
//
//   DiagnosticDef.h  Platform diagnostic data
//   Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#ifndef __DiagnosticDef_H__
   #define __DiagnosticDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __WeightDef_H__
   #include "Weight/WeightDef.h"
#endif

//------------------------------------------------------------------------------
//  Diagnostic
//------------------------------------------------------------------------------

#define DIAGNOSTIC_FIFO_SIZE  128 // terminal diagnostic fifo size
#define DIAGNOSTIC_BURST_MAX  20

typedef byte TDiagnosticFrameId;

#ifdef __WIN32__
   #pragma pack( push, 1)                   // byte alignment
#endif

typedef struct {
   word HiWord;
   byte LoByte;
} __packed TDiagnosticWeight;

typedef struct {
   dword Timestamp;
   dword LostSamples;
   byte Count;
   TDiagnosticWeight Weight[DIAGNOSTIC_BURST_MAX];
} __packed TDiagnosticFrame;

#ifdef __WIN32__
   #pragma pack( pop)                       // original alignment
#endif

#define DiagnosticFrameSize(Frame)        (sizeof(*(Frame)) - (DIAGNOSTIC_BURST_MAX - (Frame)->Count) * sizeof((Frame)->Weight[0]))
#define DiagnosticWeightCompose(DiagWeightPtr, Weight)   (DiagWeightPtr)->HiWord = Weight >> 8; (DiagWeightPtr)->LoByte = Weight
#define DiagnosticWeightDecompose(DiagnosticWeight)  ((DiagnosticWeight.HiWord & 0x8000 ? 0xFF000000: 0) | (DiagnosticWeight.HiWord << 8) | DiagnosticWeight.LoByte) 

#endif
