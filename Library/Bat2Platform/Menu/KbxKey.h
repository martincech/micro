//*****************************************************************************
//
//    KbxKey.h     Bat2 Platform keyboard codes
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __KbxKey_H__
   #define __KbxKey_H__

#ifndef __Hardware_H__
   #include "Hardware.h"               // keycodes
#endif

// keyboard events :   
typedef enum {
   K_KEY_BLACK  = _K_KEYBOARD_BASE,   // black button
   K_KEY_RED,                         // red button
   _K_KEY_LAST,

   K_REPEAT   = 0x2000,               // autorepeat flag
   K_RELEASED = 0x4000                // key released
} EKeyboardKey;
   
#endif
