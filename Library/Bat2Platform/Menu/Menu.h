//******************************************************************************
//
//   Menu.h       Bat2 wired platform menu
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Menu_H__
   #define __Menu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuMain( void);
// Platform main menu

#endif
