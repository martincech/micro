//*****************************************************************************
//
//   Menu.c       Bat2 wired platform menu
//   Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Menu/Menu.h"
#include "System/System.h"
#include "Platform/Platform.h"         // Platform data
#include "Menu/MenuCalibration.h"      // Calibration menu
#include "Cpu/Cpu.h"

//-----------------------------------------------------------------------------
// Main menu
//-----------------------------------------------------------------------------

void MenuMain( void)
// Platform main menu
{
int a = 0;
   forever {
      if(a) {
         EepromWrite(0, "DATA", 4);
         EepromExecute();
         SysDelay(100);
      }
      switch( SysEventWait()){
         case K_MODE :
            break;

         case K_OK :
            break;

         case K_ILLUMINATION :
            break;

         case K_TIMER_SLOW :
            // check for external command :
            if( PlatformOperation() == PLATFORM_OPERATION_CALIBRATION){
               MenuCalibration();
            }
            break;

         default :
            break;
      }
   } // forever
} // MenuMain
