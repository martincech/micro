//******************************************************************************
//
//   MenuCalibration.h   Calibration menu
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuCalibration_H__
   #define __MenuCalibration_H__

void MenuCalibration( void);
// Platform calibration main menu

#endif
