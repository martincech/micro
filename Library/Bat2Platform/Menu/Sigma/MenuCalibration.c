//*****************************************************************************
//
//   MenuCalibration.c  SigmaDelta calibration menu
//   Version 1.0        (c) VEIT Electronics
//
//*****************************************************************************

#include "Menu/MenuCalibration.h"
#include "System/System.h"
#include "Platform/Platform.h"         // Platform data
#include "Weighing/Weighing.h"         // Weighing operations
#include "Calibration/Calibration.h"   // Calibration data
#include "Device/Status.h"             // Status display

//-----------------------------------------------------------------------------
// Calibration
//-----------------------------------------------------------------------------

void MenuCalibration( void)
// Calibration menu
{
TRawWeight RawZero, RawRange;
TYesNo     GetZero;
TYesNo     Restart;

   StatusDisplay( STATUS_DISPLAY_ZERO);          // wait for release
   RawZero  = 0;
   RawRange = 0;
   GetZero  = YES;
   Restart  = NO;
   forever {
      switch( SysEventWait()){
         case K_MODE :
            StatusDisplay( STATUS_DISPLAY_CALIBRATION_STOP);
            StatusOperationSet( PLATFORM_OPERATION_STOP);
            return;

         case K_OK :
            // check for calibration restart :
            if( Restart){
               Restart = NO;
               GetZero = YES;                                // get zero again
               StatusDisplay( STATUS_DISPLAY_ZERO);          // wait for release
               break;
            }
            if( GetZero){
               // zero weighing
               StatusDisplay( STATUS_DISPLAY_ZERO_WEIGHING);
               RawZero = WeighingRaw();
               StatusDisplay( STATUS_DISPLAY_LOAD);
               GetZero = NO;
            } else {
               // load weighing
               StatusDisplay( STATUS_DISPLAY_LOAD_WEIGHING);
               RawRange = WeighingRaw();
               CalibrationUpdate( RawZero, RawRange, PlatformCalibration.FullRange);
               ConfigCalibrationSave();
               StatusDisplay( STATUS_DISPLAY_CALIBRATION_OK);
               PlatformErrorSet( PLATFORM_ERROR_OK);     // clear calibration error
               Restart = YES;
            }
            break;

         case K_ILLUMINATION :
            break;

         case K_TIMER_SLOW :
            // check for external command :
            if( PlatformOperation() != PLATFORM_OPERATION_CALIBRATION){
               StatusDisplay( STATUS_DISPLAY_CALIBRATION_STOP);
                  return;                                   // stopped by external command
            }
            break;

         default :
            break;
      }
   } // forever
} // MenuCalibration
