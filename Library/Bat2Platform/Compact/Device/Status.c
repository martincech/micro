//******************************************************************************
//
//   Status.c     Device status handling
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "Device/Status.h"
#include "Platform/Platform.h"    // Platform data

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------

void StatusInit( void)
// Initialize
{
} // StatusInit

//------------------------------------------------------------------------------
// Executive
//------------------------------------------------------------------------------

void StatusExecute( void)
// Main executive
{
} // StatusExecute

//------------------------------------------------------------------------------
// Set error
//------------------------------------------------------------------------------

void StatusErrorSet( byte Error)
// Set <Error> state
{
   PlatformErrorSet( Error);
} // StatusErrorSet

//------------------------------------------------------------------------------
// Set operation
//------------------------------------------------------------------------------

void StatusOperationSet( byte Operation)
// Set current <Operation>
{
   PlatformOperationSet( Operation);
   if( PlatformError() != PLATFORM_ERROR_OK){
      if( Operation != PLATFORM_OPERATION_MATCHING){
         return;                                 // error state signalized
      }
      PlatformErrorSet( PLATFORM_ERROR_OK);      // clear error
   }      
} // StatusOperationSet

//------------------------------------------------------------------------------
// Display status
//------------------------------------------------------------------------------

void StatusDisplay( byte Status)
// Display temporary status
{
} // StatusDisplay
