//******************************************************************************
//
//   Fifo.h       Samples FIFO
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Fifo_H__
   #define __Fifo_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __PlatformDef_H__
   #include "Platform/PlatformDef.h"
#endif

typedef word TFifoCount;

#ifdef __cplusplus
   extern "C" {
#endif

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void FifoInit( void);
// Initialize

void FifoPut( TWeightGauge Weight, byte Flags);
// Save <Weight> with <Flags>

TFifoCount FifoCount( void);
// Returns samples count

void FifoMark( TFifoCount Count);
// Mark <Count> items for read

TYesNo FifoGet( TFifoCount Index, TPlatformWeight *Weight);
// Returns <Weight> at <Index>

TYesNo FifoRemove( TFifoCount Count);
// Remove <Count> items

//------------------------------------------------------------------------------

#ifdef __cplusplus
   }
#endif

#endif
