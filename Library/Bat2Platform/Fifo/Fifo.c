//******************************************************************************
//
//   Fifo.c       Samples FIFO
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "Fifo.h"
#include "System/System.h"        // Operating system
#include "Time/uTime.h"

static word _MarkCount;
static word _ReadIndex;
static word _WriteIndex;

static TPlatformWeight _Fifo[ FIFO_SIZE];

#define FifoFull()    ((_ReadIndex + FIFO_SIZE) == _WriteIndex)
#define FifoDelete()   (_ReadIndex++)
#define FifoWrite( w)   _Fifo[ _WriteIndex++ & (FIFO_SIZE - 1)] = w
#define FifoRead( i)    _Fifo[ (_ReadIndex + i) & (FIFO_SIZE - 1)]
#define FifoFlush( c)   _ReadIndex += c

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------

void FifoInit( void)
// Initialize
{
   _MarkCount  = 0;
   _ReadIndex  = 0;
   _WriteIndex = 0;
} // FifoInit

//------------------------------------------------------------------------------
// Put
//------------------------------------------------------------------------------

void FifoPut( TWeightGauge Weight, byte Flags)
// Save <Weight> with <Flags>
{
TPlatformWeight w;

   w.Weight    = PlatformWeightSet( Weight, Flags);
   // get seconds from start of day LSB [2s]
   w.Timestamp = (word)((SysClock() % TIME_DAY) / 2);
   // check for FIFO full :
   if( FifoFull()){
      FifoDelete();                    // remove oldest item
   }
   FifoWrite( w);
} // FifoPut

//------------------------------------------------------------------------------
// Count
//------------------------------------------------------------------------------

TFifoCount FifoCount( void)
// Returns samples count
{
   return( _WriteIndex - _ReadIndex);
} // FifoCount

//------------------------------------------------------------------------------
// Mark
//------------------------------------------------------------------------------

void FifoMark( TFifoCount Count)
// Mark <Count> items for read
{
   _MarkCount = Count;
} // FifoMark

//------------------------------------------------------------------------------
// Get
//------------------------------------------------------------------------------

TYesNo FifoGet( TFifoCount Index, TPlatformWeight *Weight)
// Returns <Weight> at <Index>
{
   if( Index >= FifoCount()){
      return( NO);
   }
   *Weight = FifoRead( Index);
   return( YES);
} // FifoGet

//------------------------------------------------------------------------------
// Remove
//------------------------------------------------------------------------------

TYesNo FifoRemove( TFifoCount Count)
// Remove <Count> items
{
TFifoCount TotalCount;

   if( _MarkCount != Count){
      return( NO);
   }
   _MarkCount = 0;
   TotalCount = FifoCount();
   if( TotalCount < Count){
      Count = TotalCount;
   }
   FifoFlush( Count);
   return( YES);
} // FifoRemove
