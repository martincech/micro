﻿namespace BatLibrary
{
   public enum Temperature
   {
      Celsius,
      Fahrenheit
   }
}
