﻿using System.Collections.Generic;
using System.Linq;

namespace BatLibrary
{
   public class Curve
   {
      #region Public interfaces

      #region Constructor

      public Curve()
      {
         Points = new List<CurvePoint>();
      }

      #endregion

      public ICollection<CurvePoint> Points { get; private set; }

      public Weight GetCurveValue(int x)
      {
         if (Points.Count == 0)
         {
            return null;
         }

         var list = Points.OrderBy(i => i.X).ToList();
         var nearestPointIndex = -1;      //index of the nearest points (smaller than x)
         for (var i = 0; i < list.Count(); i++)
         {
            var point = list.ElementAt(i);
            if (point.X < x)
            {
               nearestPointIndex = i;
            }
            else if (point.X == x)
            {
               return point.Y;
            }
            else
            {
               break;
            }
         }

         if (nearestPointIndex < 0 || nearestPointIndex + 1 >= list.Count)
         {  // condition for interpolation: x0 < x < x1
            return null;
         }

         //point doesn't exist in list, we must interpolate it
         //TODO polynom interpolation instead of linear
         var interpolatedValue = Interpolate(list[nearestPointIndex].X, list[nearestPointIndex].Y.AsG,
            list[nearestPointIndex + 1].X, list[nearestPointIndex + 1].Y.AsG, x);
         return new Weight(interpolatedValue);
      }

      #endregion


      #region Private helpers

      /// <summary>
      /// Linear interpolation
      /// </summary>
      private double Interpolate(double x0, double y0, double x1, double y1, double x)
      {
         var y = y0 + ((y1 - y0)/(x1 - x0))*(x - x0);
         return y;
      }

      #endregion

   }
}
