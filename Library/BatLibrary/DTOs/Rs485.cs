﻿namespace DTOs
{
   public class Rs485
   {
      public int Speed { get; set; }
      public short Parity { get; set; }
      public short Protocol { get; set; }
      public short ReplyDelay { get; set; }
      public short SilentInterval { get; set; }
   }
}
