﻿namespace DTOs
{
   public class Gsm
   {
      public bool Use { get; set; }
      public bool SendMidnight { get; set; }
      public bool SendRequest { get; set; }
      public bool CheckNumbers { get; set; }
      public short Period1 { get; set; }
      public short Period2 { get; set; }
      public short Day1 { get; set; }
      public short SendHour { get; set; }
      public short SendMin { get; set; }
      public string Number0 { get; set; }
      public string Number1 { get; set; }
      public string Number2 { get; set; }
      public string Number3 { get; set; }
      public string Number4 { get; set; }
   }
}
