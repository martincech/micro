﻿namespace DTOs
{
   public class CurveValue
   {
      public int Id { get; set; }
      public int CurveId { get; set; }
      public int Day { get; set; }
      public double Weight { get; set; }
   }
}
