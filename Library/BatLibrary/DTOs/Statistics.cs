﻿namespace DTOs
{
   public class Statistics
   {
      public short HistogramRange { get; set; }
      public short UniformityRange { get; set; }
   }
}
