﻿using System.Collections.Generic;
using Bat2Library;

namespace DTOs
{
   public class ConfigurationV2
   {
      #region Constructors

      public ConfigurationV2()
      {
         Statistics = new Statistics();
         Gsm = new Gsm();
         Rs485 = new Rs485();
         ScaleConfig = new ScaleConfig();
         Correction = new Correction();
      } 

      #endregion

      public int Id { get; set; }
      public string Name { get; set; }
      public BacklightModeE Backlight { get; set; }
      public int? ScaleId { get; set; }
      public bool Active { get; set; }

      public Statistics Statistics { get; set; }
      public Gsm Gsm { get; set; }
      public Rs485 Rs485 { get; set; }
      public ScaleConfig ScaleConfig { get; set; }
      public Correction Correction { get; set; }

      public ICollection<FlockV2> FlockV2 { get; set; }
   }
}
