﻿namespace BatLibrary
{
   public enum Sex
   {
      Male,
      Female,
      Undefined
   }
}