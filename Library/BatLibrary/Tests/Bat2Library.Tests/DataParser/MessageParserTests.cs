﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bat2Library.Tests.DataParser
{
    [TestClass]
    public class MessageParserTests
    {
        private const int PACKAGE_SIZE_M = MessageParser.ExpectedMaleMessageSize;
        private const int PACKAGE_SIZE_MF = MessageParser.ExpectedCompleteMessageSize;

        [TestMethod]
        public void Bat2_Message_Male_ParseOk()
        {
            PublishedData responseM;
            var sampleM = new PublishedData
            {
                DateTime = new DateTime(2014, 12, 31, 11, 59, 58),
                Day = 2,
                Male = new GenderStats
                {
                    Average = 3,
                    Count = 4,
                    Cv = 5,
                    Gain = 6,
                    Sigma = 7,
                    Target = 8,
                    Uniformity = 9
                },
                SerialNumber = 10
            };
            var dataM = MessageParser.GenerateData(sampleM);
            Assert.AreEqual(PACKAGE_SIZE_M, dataM.Length);

            var resultM = MessageParser.Parse(dataM, out responseM);
            Assert.IsTrue(resultM);
            Assert.AreEqual(sampleM.DateTime, responseM.DateTime);
            Assert.AreEqual(sampleM.Day, responseM.Day);
            GS_AreEqual(sampleM.Male, responseM.Male);
            Assert.AreEqual(sampleM.SerialNumber, responseM.SerialNumber);
        }


        [TestMethod]
        public void Bat2_Message_MaleFemale_ParseOk()
        {
            PublishedData responseMf;

            var sampleMf = new PublishedData
            {
                DateTime = new DateTime(2014, 12, 31, 23, 59, 58),
                Day = 2,
                Male = new GenderStats
                {
                    Average = 3,
                    Count = 4,
                    Cv = 5,
                    Gain = 6,
                    Sigma = 7,
                    Target = 8,
                    Uniformity = 9
                },
                Female = new GenderStats
                {
                    Average = 10,
                    Count = 11,
                    Cv = 12,
                    Gain = 13,
                    Sigma = 14,
                    Target = 15,
                    Uniformity = 16
                },
                SerialNumber = 17
            };

            var dataMf = MessageParser.GenerateData(sampleMf);
            Assert.AreEqual(PACKAGE_SIZE_MF, dataMf.Length);

            var resultMf = MessageParser.Parse(dataMf, out responseMf);
            Assert.IsTrue(resultMf);
            Assert.AreEqual(sampleMf.DateTime, responseMf.DateTime);
            Assert.AreEqual(sampleMf.Day, responseMf.Day);
            GS_AreEqual(sampleMf.Male, responseMf.Male);
            GS_AreEqual(sampleMf.Female, responseMf.Female);
            Assert.AreEqual(sampleMf.SerialNumber, responseMf.SerialNumber);
        }


        [TestMethod]
        public void Bat2_Message_ParseNOk()
        {
            PublishedData response;

            var result = MessageParser.Parse(null, out response);
            Assert.IsFalse(result);

            result = MessageParser.Parse(new byte[PACKAGE_SIZE_M], out response);
            Assert.IsFalse(result);

            result = MessageParser.Parse(new byte[PACKAGE_SIZE_MF], out response);
            Assert.IsFalse(result);
        }

        private static void GS_AreEqual(GenderStats expected, GenderStats actual)
        {
            Assert.AreEqual(expected.Average, actual.Average);
            Assert.AreEqual(expected.Count, actual.Count);
            Assert.AreEqual(expected.Cv, actual.Cv);
            Assert.AreEqual(expected.Gain, actual.Gain);
            Assert.AreEqual(expected.Sigma, actual.Sigma);
            Assert.AreEqual(expected.Target, actual.Target);
            Assert.AreEqual(expected.Uniformity, actual.Uniformity);
        }
    }
}
