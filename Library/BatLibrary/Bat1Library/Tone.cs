﻿namespace Bat1Library
{
   public enum Tone
   {
      TONE1,
      TONE2,
      TONE3,
      TONE4,
      TONE5,
      TONE6,
      TONE7,
      TONE8,
      MELODY1,
      MELODY2,
      MELODY3,
      MELODY4,
   };
}