﻿namespace Bat1Library
{
   /// <summary>
   /// Saving parameters
   /// </summary>
   public struct SavingConfig {
      public SavingMode Mode;                       // TSavingMode Saving mode
      public bool       EnableMoreBirds;            // Enable weighing more birds
      public int        NumberOfBirds;              // Number of birds
      public double     Filter;                     // Filter averaging time [sec]
      public double     StabilisationTime;          // Stable duration [sec]
      public double     MinimumWeight;              // Charge/discharge treshold [kg]
      public double     StabilisationRange;         // Stable range [%]
   }
}