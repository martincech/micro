﻿namespace Bat1Library
{
   /// <summary>
   /// Display configuration
   /// </summary>
   public struct DisplayConfig {
      public DisplayMode     Mode;              // TDisplayMode Display mode
      public int             Contrast;          // Display contrast
      public BacklightConfig Backlight;         // Backlight setup
   }
}