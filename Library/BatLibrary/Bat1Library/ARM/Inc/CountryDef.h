//******************************************************************************
//
//   CountryDef.h   Country specific locale definitions
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#ifndef __CountryDef_H__
   #define __CountryDef_H__

#ifndef __DtDef_H__
   #include "ARM/Inc/DtDef.h"
#endif

#include "ARM/Bat1/CountrySet.h"  // project specific country, language, code page enums

// Date formats :
typedef enum {
   DATE_FORMAT_DDMMYYYY,
   DATE_FORMAT_MMDDYYYY,
   DATE_FORMAT_YYYYMMDD,
   DATE_FORMAT_YYYYDDMM,

   DATE_FORMAT_DDMMMYYYY,
   DATE_FORMAT_MMMDDYYYY,
   DATE_FORMAT_YYYYMMMDD,
   DATE_FORMAT_YYYYDDMMM,
   _DATE_FORMAT_COUNT
} TDateFormatEnum;


// Time formats :
typedef enum {
   TIME_FORMAT_24,
   TIME_FORMAT_12,
   _TIME_FORMAT_COUNT
} TTimeFormatEnum;

// Time suffix :
typedef enum {
   TIME_AM,
   TIME_PM,
} TTimeSuffix;

// Locales descriptor :
typedef struct {
   byte CodePage;           // TCodePageEnum
   byte DateFormat;         // date format
   char DateSeparator1;     // date separator character 1
   char DateSeparator2;     // date separator character 2
   byte TimeFormat;         // time format
   char TimeSeparator;      // time separator character
   byte DstType;            // daylight saving type
   byte _Spare;
} TLocaleData;

// Country descriptor :
typedef struct {
   byte        Country;      // country code
   byte        Language;     // language code
   word        _Spare;
   TLocaleData Locale;       // locale settings
} TCountry;

#endif
