//******************************************************************************
//
//   CalDef.h     Calibration definition
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef __CalDef_H__
   #define __CalDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __WeightDef_H__
   #include "ARM/Bat1/WeightDef.h"
#endif

typedef word TCalCrc;                  // calibration checksum

typedef struct {
   TRawWeight Offset;                  // zero calibration
   TRawWeight Factor;                  // range calibration
   TWeight    Range;                   // physical range
   TCalCrc    CheckSum;                // record checksum
   word       _Spare;
} TCalibration;

#endif
