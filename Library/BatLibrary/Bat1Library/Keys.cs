﻿namespace Bat1Library
{
   public enum Keys
   {
      K_NULL, // Unused
      K_ENTER, // Enter
      K_LEFT, // Left arrow
      K_ESC, // Esc
      K_UP, // Up arrow
      K_RIGHT, // Right arrow
      K_DOWN, // Down arrow
   };
}