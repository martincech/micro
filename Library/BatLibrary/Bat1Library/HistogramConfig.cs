﻿namespace Bat1Library
{
   /// <summary>
   /// Histogram parameters
   /// </summary>
   public struct HistogramConfig {
      public HistogramMode Mode;             // Mode of histogram
      public int           Range;		       // Histogram step in +/- perecents of average weight (or 0 if not used)
      public double        Step;		       // Histogram step in kg (or 0 if not used)
   }
}