using System.Runtime.InteropServices;
using System.Text;

// ReSharper disable InconsistentNaming
//=============================================================================
// DLL interface
//=============================================================================

namespace Bat1Library
{
   public class Dllxx
   {
//-----------------------------------------------------------------------------
//   Helper functions
//-----------------------------------------------------------------------------

      [DllImport("Bat1Native.dll")]
      public static extern uint GetDllVersion();

      // Returns DLL version

      [DllImport("Bat1Native.dll")]
      public static extern bool IsUsbLess();

      // Returns TRUE if library is without USB support

      [DllImport("Bat1Native.dll")]

      public static extern void EnableLogger(bool Enable);
      // Enable communication logger (at console)

      [DllImport("Bat1Native.dll")]
      public static extern void DecodeTime(int Timestamp, out int Day, out int Month, out int Year, out int Hour,
         out int Min, out int Sec);

      // Decode <Timestamp> to items

      [DllImport("Bat1Native.dll")]
      public static extern int EncodeTime(int Day, int Month, int Year, int Hour, int Min, int Sec);

      // Returns timestamp by items

      [DllImport("Bat1Native.dll")]
      public static extern double DecodeWeight(int Weight);

      // Decode <Weight> to number

      [DllImport("Bat1Native.dll")]
      public static extern int EncodeWeight(double Weight);

      // Encode <Weight> to internal representation

//-----------------------------------------------------------------------------
//   Device functions
//-----------------------------------------------------------------------------

      [DllImport("Bat1Native.dll")]
      public static extern bool CheckDevice();

      // Returns TRUE if device present

      [DllImport("Bat1Native.dll")]
      public static extern bool DeviceIsOn(out bool PowerOn);

      // Sets <PowerOn> TRUE on powered device

      [DllImport("Bat1Native.dll")]
      public static extern bool DevicePowerOff();

      // Switch power off

      [DllImport("Bat1Native.dll")]
      public static extern bool GetTime(out int Clock);

      // Get device clock

      [DllImport("Bat1Native.dll")]
      public static extern bool SetTime(int Clock);

      // Set device clock

      [DllImport("Bat1Native.dll")]
      public static extern void NewDevice();

      // Create new empty device

      [DllImport("Bat1Native.dll")]
      public static extern bool LoadConfiguration();

      // Load configuration data only from device

      [DllImport("Bat1Native.dll")]
      public static extern bool LoadDevice();

      // Load data from device

      [DllImport("Bat1Native.dll")]
      public static extern bool SaveDevice();

      // Save data to device

      [DllImport("Bat1Native.dll")]
      public static extern bool ReloadConfiguration();

      // Reload configuration from EEPROM to device

      [DllImport("Bat1Native.dll")]
      public static extern bool LoadEstimation(out int Promile);

      // Load size estimation [%%]

      [DllImport("Bat1Native.dll")]
      public static extern void SaveEstimation(out int Promile);

      // Save size estimation [%%]

      [DllImport("Bat1Native.dll")]
      public static extern bool LoadCrashInfo();

      // Load device crash info

//-----------------------------------------------------------------------------
//   EEPROM access
//-----------------------------------------------------------------------------

      [DllImport("Bat1Native.dll")]
      public static extern bool ReadEeprom(int Address, byte[] Buffer, int Size);

      // Read data from device EEPROM

      [DllImport("Bat1Native.dll")]
      public static extern bool WriteEeprom(int Address, byte[] Buffer, int Size);

      // Write data to device EEPROM

      [DllImport("Bat1Native.dll")]
      public static extern int GetEepromSize();

      // Returns device EEPROM size

      [DllImport("Bat1Native.dll")]
      public static extern bool DeviceByEeprom(byte[] Buffer);

      // Setup device by EEPROM contents

      [DllImport("Bat1Native.dll")]
      public static extern int GetLogoSize();

      // Returns logo size

      [DllImport("Bat1Native.dll")]
      public static extern int GetLogoAddress();

      // Returns logo start address

//-----------------------------------------------------------------------------
//   Configuration data
//-----------------------------------------------------------------------------

//---- get/set version

      [DllImport("Bat1Native.dll")]
      public static extern uint GetDeviceVersion();

      // Get device version

      [DllImport("Bat1Native.dll")]
      public static extern void SetDeviceVersion(uint Version);

      // Set device version

      [DllImport("Bat1Native.dll")]
      public static extern uint GetBuild();

      // Get device build

      [DllImport("Bat1Native.dll")]
      public static extern void SetBuild(uint Build);

      // Set device build

      [DllImport("Bat1Native.dll")]
      public static extern uint GetHwVersion();

      // Get hardware version

      [DllImport("Bat1Native.dll")]
      public static extern void SetHwVersion(uint Version);

      // Set hardware version

//---- get/set scale parameters

      [DllImport("Bat1Native.dll")]
      public static extern void GetScaleName(StringBuilder Name);

      // Get scale name

      [DllImport("Bat1Native.dll")]
      public static extern void SetScaleName(string Name);

      // Set scale name

      [DllImport("Bat1Native.dll")]
      public static extern void ClearPassword();

      // Disable password

      [DllImport("Bat1Native.dll")]
      public static extern bool ValidPassword();

      // Returns TRUE if password is in use

      [DllImport("Bat1Native.dll")]
      public static extern void GetPassword(byte[] Password);

      // Get password

      [DllImport("Bat1Native.dll")]
      public static extern void SetPassword(byte[] Password);

      // Set password

//---- get/set country data

      [DllImport("Bat1Native.dll")]
      public static extern int GetCountry();

      // Get country

      [DllImport("Bat1Native.dll")]
      public static extern void SetCountry(int Country);

      // Set country

      [DllImport("Bat1Native.dll")]
      public static extern int GetLanguage();

      // Get language

      [DllImport("Bat1Native.dll")]
      public static extern void SetLanguage(int Language);

      // Set language

      [DllImport("Bat1Native.dll")]
      public static extern int GetCodePage();

      // Get code page

      [DllImport("Bat1Native.dll")]
      public static extern void SetCodePage(int CodePage);

      // Set code page

      [DllImport("Bat1Native.dll")]
      public static extern int GetDeviceDateFormat();

      // Get date format

      [DllImport("Bat1Native.dll")]
      public static extern void SetDeviceDateFormat(int Format);

      // Set date format

      [DllImport("Bat1Native.dll")]
      public static extern char GetDateSeparator1();

      // Get first date separator

      [DllImport("Bat1Native.dll")]
      public static extern void SetDateSeparator1(char Separator);

      // Set first date separator

      [DllImport("Bat1Native.dll")]
      public static extern char GetDateSeparator2();

      // Get second date separator

      [DllImport("Bat1Native.dll")]
      public static extern void SetDateSeparator2(char Separator);

      // Set second date separator

      [DllImport("Bat1Native.dll")]
      public static extern int GetDeviceTimeFormat();

      // Get time format

      [DllImport("Bat1Native.dll")]
      public static extern void SetDeviceTimeFormat(int Format);

      // Set time format

      [DllImport("Bat1Native.dll")]
      public static extern char GetTimeSeparator();

      // Get time separator

      [DllImport("Bat1Native.dll")]
      public static extern void SetTimeSeparator(char Separator);

      // Set time separator

      [DllImport("Bat1Native.dll")]
      public static extern int GetDaylightSavingType();

      // Get daylight saving time type

      [DllImport("Bat1Native.dll")]
      public static extern void SetDaylightSavingType(int DstType);

      // Set daylight saving time type

//---- get/set weighing units

      [DllImport("Bat1Native.dll")]
      public static extern int GetWeighingUnits();

      // Get weighing units

      [DllImport("Bat1Native.dll")]
      public static extern void SetWeighingUnits(int Units);

      // Set weighing units

      [DllImport("Bat1Native.dll")]
      public static extern int GetWeighingCapacity();

      // Get weighing capacity

      [DllImport("Bat1Native.dll")]
      public static extern void SetWeighingCapacity(int Capacity);

      // Set weighing capacity

      [DllImport("Bat1Native.dll")]
      public static extern int GetWeighingRange();

      // Get weighing range

      [DllImport("Bat1Native.dll")]
      public static extern int GetWeighingDecimals();

      // Get weighing decimals

      [DllImport("Bat1Native.dll")]
      public static extern int GetWeighingMaxDivision();

      // Get weighing max. division

      [DllImport("Bat1Native.dll")]
      public static extern int GetWeighingDivision();

      // Get weighing division

      [DllImport("Bat1Native.dll")]
      public static extern void SetWeighingDivision(int Division);

      // Set weighing division

//---- get/set sound settings

      [DllImport("Bat1Native.dll")]
      public static extern int GetToneDefault();

      // Get default beep

      [DllImport("Bat1Native.dll")]
      public static extern void SetToneDefault(int Tone);

      // Set default beep

      [DllImport("Bat1Native.dll")]
      public static extern int GetToneLight();

      // Get below beep

      [DllImport("Bat1Native.dll")]
      public static extern void SetToneLight(int Tone);

      // Set below beep

      [DllImport("Bat1Native.dll")]
      public static extern int GetToneOk();

      // Get within beep

      [DllImport("Bat1Native.dll")]
      public static extern void SetToneOk(int Tone);

      // Set within beep

      [DllImport("Bat1Native.dll")]
      public static extern int GetToneHeavy();

      // Get above beep

      [DllImport("Bat1Native.dll")]
      public static extern void SetToneHeavy(int Tone);

      // Set above beep

      [DllImport("Bat1Native.dll")]
      public static extern int GetToneKeyboard();

      // Get keyboard beep

      [DllImport("Bat1Native.dll")]
      public static extern void SetToneKeyboard(int Tone);

      // Set keyboard beep

      [DllImport("Bat1Native.dll")]
      public static extern bool GetEnableSpecialSounds();

      // Get enable special sounds

      [DllImport("Bat1Native.dll")]
      public static extern void SetEnableSpecialSounds(bool Enable);

      // Set enable special sounds

      [DllImport("Bat1Native.dll")]
      public static extern int GetVolumeSaving();

      // Get saving volume

      [DllImport("Bat1Native.dll")]
      public static extern void SetVolumeSaving(int Volume);

      // Set saving volume

      [DllImport("Bat1Native.dll")]
      public static extern int GetVolumeKeyboard();

      // Get keyboard volume

      [DllImport("Bat1Native.dll")]
      public static extern void SetVolumeKeyboard(int Volume);

      // Set keyboard volume

//---- get/set display settings

      [DllImport("Bat1Native.dll")]
      public static extern int GetDisplayMode();

      // Get display mode

      [DllImport("Bat1Native.dll")]
      public static extern void SetDisplayMode(int Mode);

      // Set display mode

      [DllImport("Bat1Native.dll")]
      public static extern int GetDisplayContrast();

      // Get display contrast

      [DllImport("Bat1Native.dll")]
      public static extern void SetDisplayContrast(int Contrast);

      // Set display contrast

      [DllImport("Bat1Native.dll")]
      public static extern int GetBacklightMode();

      // Get backlight mode

      [DllImport("Bat1Native.dll")]
      public static extern void SetBacklightMode(int Mode);

      // Set backlight mode

      [DllImport("Bat1Native.dll")]
      public static extern int GetBacklightIntensity();

      // Get backlight intensity

      [DllImport("Bat1Native.dll")]
      public static extern void SetBacklightIntensity(int Intensity);

      // Set backlight intensity

      [DllImport("Bat1Native.dll")]
      public static extern int GetBacklightDuration();

      // Get backlight duration

      [DllImport("Bat1Native.dll")]
      public static extern void SetBacklightDuration(int Duration);

      // Set backlight duration

//---- get/set printer settings

      [DllImport("Bat1Native.dll")]
      public static extern int GetPrinterPaperWidth();

      // Get printer paper width

      [DllImport("Bat1Native.dll")]
      public static extern void SetPrinterPaperWidth(int Width);

      // Set printer paper width

      [DllImport("Bat1Native.dll")]
      public static extern int GetPrinterCommunicationFormat();

      // Get printer communication format

      [DllImport("Bat1Native.dll")]
      public static extern void SetPrinterCommunicationFormat(int Format);

      // Set printer communication format

      [DllImport("Bat1Native.dll")]
      public static extern int GetPrinterCommunicationSpeed();

      // Get printer communication format

      [DllImport("Bat1Native.dll")]
      public static extern void SetPrinterCommunicationSpeed(int Speed);

      // Set printer communication format

//---- get/set global parameters

      [DllImport("Bat1Native.dll")]
      public static extern int GetKeyboardTimeout();

      // Get keyboard timeout

      [DllImport("Bat1Native.dll")]
      public static extern void SetKeyboardTimeout(int Timeout);

      // Set keyboard timeout

      [DllImport("Bat1Native.dll")]
      public static extern int GetPowerOffTimeout();

      // Get power off timeout

      [DllImport("Bat1Native.dll")]
      public static extern void SetPowerOffTimeout(int Timeout);

      // Set power off timeout

      [DllImport("Bat1Native.dll")]
      public static extern bool GetEnableFileParameters();

      // Get enable file parameters

      [DllImport("Bat1Native.dll")]
      public static extern void SetEnableFileParameters(bool Enable);

      // Set enable file parameters

//---- get/set weighing parameters

      [DllImport("Bat1Native.dll")]
      public static extern bool GetEnableMoreBirds();

      // Get enable more birds

      [DllImport("Bat1Native.dll")]
      public static extern void SetEnableMoreBirds(bool Enable);

      // Set enable more birds

      [DllImport("Bat1Native.dll")]
      public static extern int GetWeightSortingMode();

      // Get weight sorting mode

      [DllImport("Bat1Native.dll")]
      public static extern void SetWeightSortingMode(int Mode);

      // Set weight sorting mode

      [DllImport("Bat1Native.dll")]
      public static extern int GetSavingMode();

      // Get saving mode

      [DllImport("Bat1Native.dll")]
      public static extern void SetSavingMode(int Mode);

      // Set saving mode

      [DllImport("Bat1Native.dll")]
      public static extern int GetFilter();

      // Get filter

      [DllImport("Bat1Native.dll")]
      public static extern void SetFilter(int Filter);

      // Set filter

      [DllImport("Bat1Native.dll")]
      public static extern int GetStabilisationTime();

      // Get stabilisation time

      [DllImport("Bat1Native.dll")]
      public static extern void SetStabilisationTime(int StabilisationTime);

      // Set stabilisation time

      [DllImport("Bat1Native.dll")]
      public static extern int GetMinimumWeight();

      // Get minimum weight

      [DllImport("Bat1Native.dll")]
      public static extern void SetMinimumWeight(int Weight);

      // Set minimum weight

      [DllImport("Bat1Native.dll")]
      public static extern int GetStabilisationRange();

      // Get stabilisation range

      [DllImport("Bat1Native.dll")]
      public static extern void SetStabilisationRange(int Range);

      // Set stabilisation range

//---- get/set statistics

      [DllImport("Bat1Native.dll")]
      public static extern int GetUniformityRange();

      // Get uniformity range

      [DllImport("Bat1Native.dll")]
      public static extern void SetUniformityRange(int Range);

      // Set uniformity range

      [DllImport("Bat1Native.dll")]
      public static extern int GetHistogramMode();

      // Get histogram mode

      [DllImport("Bat1Native.dll")]
      public static extern int GetHistogramRange();

      // Get histogram range

      [DllImport("Bat1Native.dll")]
      public static extern void SetHistogramRange(int Range);

      // Set histogram range

      [DllImport("Bat1Native.dll")]
      public static extern int GetHistogramStep();

      // Get histogram step

      [DllImport("Bat1Native.dll")]
      public static extern void SetHistogramStep(int Step);

      // Set histogram step

//-----------------------------------------------------------------------------
//  Crash data
//-----------------------------------------------------------------------------

      [DllImport("Bat1Native.dll")]
      public static extern int GetExceptionTimestamp();

      // Get exception date & time

      [DllImport("Bat1Native.dll")]
      public static extern int GetExceptionAddress();

      // Get exception address

      [DllImport("Bat1Native.dll")]
      public static extern int GetExceptionType();

      // Get exception type

      [DllImport("Bat1Native.dll")]
      public static extern int GetExceptionStatus();

      // Get exception status

      [DllImport("Bat1Native.dll")]
      public static extern int GetWatchDogTimestamp();

      // Get watchdog date & time

      [DllImport("Bat1Native.dll")]
      public static extern int GetWatchDogStatus();

      // Get watchdog status

//-----------------------------------------------------------------------------
//  Data files
//-----------------------------------------------------------------------------

//---- directory maitenance

      [DllImport("Bat1Native.dll")]
      public static extern int GetFilesCount();

      // Get number of files

      [DllImport("Bat1Native.dll")]
      public static extern void FilesDeleteAll();

      // Delete all files

      [DllImport("Bat1Native.dll")]
      public static extern int FileCreate();

      // Create new file, returns index

//---- get/set directory data

      [DllImport("Bat1Native.dll")]
      public static extern void GetFileName(int Index, StringBuilder Name);

      // Get file name

      [DllImport("Bat1Native.dll")]
      public static extern void SetFileName(int Index, string Name);

      // Set file name

      [DllImport("Bat1Native.dll")]
      public static extern void GetFileNote(int Index, StringBuilder Note);

      // Get file note

      [DllImport("Bat1Native.dll")]
      public static extern void SetFileNote(int Index, string Note);

      // Set file note

      [DllImport("Bat1Native.dll")]
      public static extern uint GetFileCreation(int Index);

      // Get file creation date

      [DllImport("Bat1Native.dll")]
      public static extern int GetFileRawSize(int Index);

      // Get file size [bytes]

      [DllImport("Bat1Native.dll")]
      public static extern bool IsCurrentFile(int Index);

      // Returns TRUE on current working file

      [DllImport("Bat1Native.dll")]
      public static extern void SetCurrentFile(int Index);

      // Set file on <Index> as current working file

//---- get/set file configuration

      [DllImport("Bat1Native.dll")]
      public static extern bool GetFileEnableMoreBirds(int Index);

      // Get enable more birds

      [DllImport("Bat1Native.dll")]
      public static extern void SetFileEnableMoreBirds(int Index, bool Enable);

      // Set enable more birds

      [DllImport("Bat1Native.dll")]
      public static extern int GetFileNumberOfBirds(int Index);

      // Get number of birds

      [DllImport("Bat1Native.dll")]
      public static extern void SetFileNumberOfBirds(int Index, int NumberOfBirds);

      // Set number of birds

      [DllImport("Bat1Native.dll")]
      public static extern int GetFileWeightSortingMode(int Index);

      // Get weight sorting mode

      [DllImport("Bat1Native.dll")]
      public static extern void SetFileWeightSortingMode(int Index, int Mode);

      // Set weight sorting mode

      [DllImport("Bat1Native.dll")]
      public static extern int GetFileLowLimit(int Index);

      // Get weight sorting low limit

      [DllImport("Bat1Native.dll")]
      public static extern void SetFileLowLimit(int Index, int LowLimit);

      // Set weight sorting low limit

      [DllImport("Bat1Native.dll")]
      public static extern int GetFileHighLimit(int Index);

      // Get weight sorting high limit

      [DllImport("Bat1Native.dll")]
      public static extern void SetFileHighLimit(int Index, int HighLimit);

      // Set weight sorting high limit

      [DllImport("Bat1Native.dll")]
      public static extern int GetFileSavingMode(int Index);

      // Get saving mode

      [DllImport("Bat1Native.dll")]
      public static extern void SetFileSavingMode(int Index, int Mode);

      // Set saving mode

      [DllImport("Bat1Native.dll")]
      public static extern int GetFileFilter(int Index);

      // Get filter

      [DllImport("Bat1Native.dll")]
      public static extern void SetFileFilter(int Index, int Filter);

      // Set filter

      [DllImport("Bat1Native.dll")]
      public static extern int GetFileStabilisationTime(int Index);

      // Get stabilisation time

      [DllImport("Bat1Native.dll")]
      public static extern void SetFileStabilisationTime(int Index, int StabilisationTime);

      // Set stabilisation time

      [DllImport("Bat1Native.dll")]
      public static extern int GetFileMinimumWeight(int Index);

      // Get minimum weight

      [DllImport("Bat1Native.dll")]
      public static extern void SetFileMinimumWeight(int Index, int Weight);

      // Set minimum weight

      [DllImport("Bat1Native.dll")]
      public static extern int GetFileStabilisationRange(int Index);

      // Get stabilisation range

      [DllImport("Bat1Native.dll")]
      public static extern void SetFileStabilisationRange(int Index, int Range);

      // Set stabilisation range

//---- get/set samples

      [DllImport("Bat1Native.dll")]
      public static extern int GetFileSamplesCount(int Index);

      // Get file samples count

      [DllImport("Bat1Native.dll")]
      public static extern void FileClearSamples(int Index);

      // Clear file samples

      [DllImport("Bat1Native.dll")]
      public static extern void FileAllocSamples(int Index, int SamplesCount);

      // Allocate file samples

      [DllImport("Bat1Native.dll")]
      public static extern int GetSampleTimestamp(int Index, int SampleIndex);

      // Get file sample timestamp

      [DllImport("Bat1Native.dll")]
      public static extern void SetSampleTimestamp(int Index, int SampleIndex, int Timestamp);

      // Set file sample timestamp

      [DllImport("Bat1Native.dll")]
      public static extern int GetSampleWeight(int Index, int SampleIndex);

      // Get file sample weight

      [DllImport("Bat1Native.dll")]
      public static extern void SetSampleWeight(int Index, int SampleIndex, int Weight);

      // Set file sample weight

      [DllImport("Bat1Native.dll")]
      public static extern int GetSampleFlag(int Index, int SampleIndex);

      // Get file sample flag

      [DllImport("Bat1Native.dll")]
      public static extern void SetSampleFlag(int Index, int SampleIndex, int Flag);

      // Set file sample flag

//-----------------------------------------------------------------------------
//  File Groups
//-----------------------------------------------------------------------------

//---- directory maitenance

      [DllImport("Bat1Native.dll")]
      public static extern int GetGroupsCount();

      // Get number of groups

      [DllImport("Bat1Native.dll")]
      public static extern void GroupsDeleteAll();

      // Delete all groups

      [DllImport("Bat1Native.dll")]
      public static extern int GroupCreate();

      // Create new group, returns index


//---- get/set directory data

      [DllImport("Bat1Native.dll")]
      public static extern void GetGroupName(int Index, StringBuilder Name);

      // Get group name

      [DllImport("Bat1Native.dll")]
      public static extern void SetGroupName(int Index, string Name);

      // Set group name

      [DllImport("Bat1Native.dll")]
      public static extern void GetGroupNote(int Index, StringBuilder Note);

      // Get group note

      [DllImport("Bat1Native.dll")]
      public static extern void SetGroupNote(int Index, string Note);

      // Set group note

      [DllImport("Bat1Native.dll")]
      public static extern uint GetGroupCreation(int Index);

      // Get group creation date

//--- file list

      [DllImport("Bat1Native.dll")]
      public static extern int GetGroupFilesCount(int Index);

      // Returns number of files in the group

      [DllImport("Bat1Native.dll")]
      public static extern void GroupClearFiles(int Index);

      // Clear group files

      [DllImport("Bat1Native.dll")]
      public static extern int GetGroupFile(int Index, int FileIndex);

      // Returns index into file list from the group at position <FileIndex>

      [DllImport("Bat1Native.dll")]
      public static extern void AddGroupFile(int Index, int FileIndex);

      // Add <FileIndex> into group
   }
}