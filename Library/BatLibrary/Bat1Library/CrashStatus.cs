namespace Bat1Library
{
   public class CrashStatus
   {
      public const int PPR = 1; // PPR signal status
      public const int CHG = 2; // CHG signal status
      public const int USBON = 4; // USBON signal status
   };
}