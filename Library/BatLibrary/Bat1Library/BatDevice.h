//******************************************************************************
//
//   BatDevice.h      Bat device basics
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#ifndef BatDeviceH
   #define BatDeviceH

#ifndef PktAdapterH
   #include "PktAdapter.h"
#endif

#ifndef UsbDefH
   #include "ARM/Bat1/UsbDef.h"
#endif

//******************************************************************************
// BatDevice
//******************************************************************************

class TBatDevice {
public :
   TBatDevice();
   // Constructor

   ~TBatDevice();
   // Destructor

   TYesNo Check();
   // Check for device connected

   TYesNo GetVersion( int &Version);
   // Get bat version

   TYesNo GetStatus( TYesNo &PowerOn);
   // Get power status

   TYesNo PowerOff();
   // Switch power off

   TYesNo ReloadConfig();
   // Reload configuration by EEPROM

   TYesNo GetTime( TTimestamp &Timestamp);
   // Get device time

   TYesNo SetTime( TTimestamp Timestamp);
   // Set device time

   TYesNo ReadMemory( int Address, void *Buffer, int Size);
   // Read EEPROM

   TYesNo WriteMemory( int Address, void *Buffer, int Size);
   // Write EEPROM data

   TYesNo DirectoryBegin( int &Count);
   // Read directory begin. Returns <Count> of directory items

   TYesNo DirectoryNext( TUsbDirectoryItem *Item);
   // Read directory entry. Fills <Item>
   
   TYesNo FileOpen( int Handle);
   // Open file by <Handle>

   TYesNo FileClose();
   // Close Current file

   TYesNo FileCreate( char *Name, char *Note, int Class, int &Handle);
   // Create file by <Name>, <Note>, <Class>. Returns <Handle>

   TYesNo FileRead( void *Buffer, int Size);
   // Read file

   TYesNo FileWrite( void *Buffer, int Size);
   // Write file

   TYesNo FormatFilesystem();
   // Format filesystem

   TPktAdapter *Adapter;
   TYesNo         WriteVerification;
//------------------------------------------------------------------------------
protected :
   TYesNo SendCommand( int &Command, int &Data);
   // Send command with error recovery

   TYesNo WriteFragment( int WriteCommand, void *Buffer, int Size);
   // Write fragment of data with recovery
}; // BatDevice

#endif
