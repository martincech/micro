#include "Locales.h"
//#include "Bat1Dll.h"

using namespace Bat1Library::Connection::Native;
using namespace Bat1Library;
using namespace System::Runtime::InteropServices;

String^ Locales::GetCountryName(int CountryCode)
{   
   switch ((Country)CountryCode)
   {
      case Country::INTERNATIONAL:
         return gcnew String("International");
      case Country::ALBANIA:
         return gcnew String("Albania");
      case Country::ALGERIA:
         return gcnew String("Algeria");
      case Country::ARGENTINA:
         return gcnew String("Argentina");
      case Country::AUSTRALIA:
         return gcnew String("Australia");
      case Country::AUSTRIA:
         return gcnew String("Austria");
      case Country::BANGLADESH:
         return gcnew String("Bangladesh");
      case Country::BELARUS:
         return gcnew String("Belarus");
      case Country::BELGIUM:
         return gcnew String("Belgium");
      case Country::BOLIVIA:
         return gcnew String("Bolivia");
      case Country::BRAZIL:
         return gcnew String("Brazil");
      case Country::BULGARIA:
         return gcnew String("Bulgaria");
      case Country::CANADA:
         return gcnew String("Canada");
      case Country::CHILE:
         return gcnew String("Chile");
      case Country::CHINA:
         return gcnew String("China");
      case Country::COLOMBIA:
         return gcnew String("Colombia");
      case Country::CYPRUS:
         return gcnew String("Cyprus");
      case Country::CZECH:
         return gcnew String("Czech Republic");
      case Country::DENMARK:
         return gcnew String("Denmark");
      case Country::ECUADOR:
         return gcnew String("Ecuador");
      case Country::EGYPT:
         return gcnew String("Egypt");
      case Country::ESTONIA:
         return gcnew String("Estonia");
      case Country::FINLAND:
         return gcnew String("Finland");
      case Country::FRANCE:
         return gcnew String("France");
      case Country::GERMANY:
         return gcnew String("Germany");
      case Country::GREECE:
         return gcnew String("Greece");
      case Country::HUNGARY:
         return gcnew String("Hungary");
      case Country::INDIA:
         return gcnew String("India");
      case Country::INDONESIA:
         return gcnew String("Indonesia");
      case Country::IRAN:
         return gcnew String("Iran");
      case Country::IRELAND:
         return gcnew String("Ireland");
      case Country::ISRAEL:
         return gcnew String("Israel");
      case Country::ITALY:
         return gcnew String("Italy");
      case Country::JAPAN:
         return gcnew String("Japan");
      case Country::JORDAN:
         return gcnew String("Jordan");
      case Country::LATVIA:
         return gcnew String("Latvia");
      case Country::LEBANON:
         return gcnew String("Lebanon");
      case Country::LITHUANIA:
         return gcnew String("Lithuania");
      case Country::LUXEMBOURG:
         return gcnew String("Luxembourg");
      case Country::MALAYSIA:
         return gcnew String("Malaysia");
      case Country::MALTA:
         return gcnew String("Malta");
      case Country::MEXICO:
         return gcnew String("Mexico");
      case Country::MONGOLIA:
         return gcnew String("Mongolia");
      case Country::MOROCCO:
         return gcnew String("Morocco");
      case Country::NEPAL:
         return gcnew String("Nepal");
      case Country::NETHERLANDS:
         return gcnew String("Netherlands");
      case Country::NEW_ZEALAND:
         return gcnew String("New Zealand");
      case Country::NIGERIA:
         return gcnew String("Nigeria");
      case Country::NORWAY:
         return gcnew String("Norway");
      case Country::PAKISTAN:
         return gcnew String("Pakistan");
      case Country::PARAGUAY:
         return gcnew String("Paraguay");
      case Country::PERU:
         return gcnew String("Peru");
      case Country::PHILIPPINES:
         return gcnew String("Philippines");
      case Country::POLAND:
         return gcnew String("Poland");
      case Country::PORTUGAL:
         return gcnew String("Portugal");
      case Country::ROMANIA:
         return gcnew String("Romania");
      case Country::RUSSIA:
         return gcnew String("Russia");
      case Country::SLOVAKIA:
         return gcnew String("Slovakia");
      case Country::SLOVENIA:
         return gcnew String("Slovenia");
      case Country::SOUTH_AFRICA:
         return gcnew String("South Africa");
      case Country::SOUTH_KOREA:
         return gcnew String("South Korea");
      case Country::SPAIN:
         return gcnew String("Spain");
      case Country::SWEDEN:
         return gcnew String("Sweden");
      case Country::SWITZERLAND:
         return gcnew String("Switzerland");
      case Country::SYRIA:
         return gcnew String("Syria");
      case Country::THAILAND:
         return gcnew String("Thailand");
      case Country::TUNISIA:
         return gcnew String("Tunisia");
      case Country::TURKEY:
         return gcnew String("Turkey");
      case Country::UKRAINE:
         return gcnew String("Ukraine");
      case Country::UK:
         return gcnew String("United Kingdom");
      case Country::USA:
         return gcnew String("United States");
      case Country::URUGUAY:
         return gcnew String("Uruguay");
      case Country::VENEZUELA:
         return gcnew String("Venezuela");
      case Country::VIETNAM:
         return gcnew String("Vietnam");
      default:
         return gcnew String("International");
   }
}

int Locales::GetLanguage(int Country)
{
   return GetLocaleLanguage(Country);
}

int Locales::GetCodePage(int Country)
{
   return GetLocaleCodePage(Country);
}

int Locales::GetDateFormat(int Country)
{
   return GetLocaleDateFormat(Country);
}

wchar_t Locales::GetDateSeparator1(int Country)
{
   return GetLocaleDateSeparator1(Country);
}

wchar_t Locales::GetDateSeparator2(int Country)
{
   return GetLocaleDateSeparator2(Country);
}

int Locales::GetTimeFormat(int Country)
{
   return GetLocaleTimeFormat(Country);
}

wchar_t Locales::GetTimeSeparator(int Country)
{
   return GetLocaleTimeSeparator(Country);
}

int Locales::GetDaylightSavingType(int Country)
{
   return GetLocaleDaylightSavingType(Country);
}
