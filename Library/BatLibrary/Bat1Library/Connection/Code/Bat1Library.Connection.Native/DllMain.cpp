#include "Bat1Dll.h"

#ifdef _DEBUG
#include <stdio.h>
#define TRACE( msg)       printf( msg "\n")
#define TRACE1( msg, p1)  printf( msg "\n", p1)
#else
#define TRACE( msg)
#define TRACE1( msg, p1)
#endif
#include <Windows.h>
#pragma unmanaged
extern "C"
BOOL APIENTRY DllMain(HANDLE hModule, DWORD dwReason, LPVOID lpReserved)
{
   // process initialisation :
   if (dwReason == DLL_PROCESS_ATTACH){
      TRACE("DllMain : initialisation");
      Initialize();
      return(1);
   }
   // process shutdown :
   if (dwReason == DLL_PROCESS_DETACH){
      TRACE("DllMain : terminate");
      DeInitialize();
      return(1);
   }
   // thread maipulations :
   if (dwReason == DLL_THREAD_ATTACH){
      TRACE("DllMain : thread attach");
      return(1);
   }
   if (dwReason == DLL_THREAD_DETACH){
      TRACE("DllMain : thread detach");
      return(1);
   }
   // unknown reason :
   TRACE1("DllMain : unknown reason %d", dwReason);
   return(1);
} // DllMain