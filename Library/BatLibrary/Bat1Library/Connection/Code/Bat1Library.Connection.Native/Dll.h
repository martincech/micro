#pragma once
#include "Bat1Dll.h"

#define BUFF_SIZE 255

namespace Bat1Library
{
   namespace Connection
   {
      namespace Native
      {
         using namespace System;
         using namespace System::Text;

         static public ref class Dll
         {
         public:
            static unsigned int GetDllVersion();
            static bool IsUsbLess();
            static void EnableLogger(bool Enable);     
            static void DecodeTime(int Timestamp, 
               [System::Runtime::InteropServices::OutAttribute] int %Day, 
               [System::Runtime::InteropServices::OutAttribute] int %Month, 
               [System::Runtime::InteropServices::OutAttribute] int %Year, 
               [System::Runtime::InteropServices::OutAttribute] int %Hour,
               [System::Runtime::InteropServices::OutAttribute] int %Min, 
               [System::Runtime::InteropServices::OutAttribute] int %Sec);
            static int EncodeTime(int Day, int Month, int Year, int Hour, int Min, int Sec);      
            static double DecodeWeight(int Weight);       
            static int EncodeWeight(double Weight);

            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------           
            static bool CheckDevice();      
            static bool DeviceIsOn([System::Runtime::InteropServices::OutAttribute] bool %PowerOn);
            static bool DevicePowerOff();         
            static bool GetTime([System::Runtime::InteropServices::OutAttribute] int %Clock);
            static bool SetTime(int Clock);          
            static void NewDevice();           
            static bool LoadConfiguration();          
            static bool LoadDevice();        
            static bool SaveDevice();          
            static bool ReloadConfiguration();          
            static bool LoadEstimation([System::Runtime::InteropServices::OutAttribute] int %Promile);
            static void SaveEstimation([System::Runtime::InteropServices::OutAttribute] int %Promile);
            static bool LoadCrashInfo();

            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------           
            static bool ReadEeprom(int Address, array<byte>^ Buffer, int Size);          
            static bool WriteEeprom(int Address, array<byte>^ Buffer, int Size);
            static int GetEepromSize();           
            static bool DeviceByEeprom(array<byte>^ Buffer);
            static int GetLogoSize();          
            static int GetLogoAddress();

            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------        
            static unsigned int GetDeviceVersion();         
            static void SetDeviceVersion(unsigned int Version);         
            static unsigned int GetBuild();         
            static void SetBuild(unsigned int Build);         
            static unsigned int GetHwVersion();
            static void SetHwVersion(unsigned int Version);       
            static void GetScaleName(StringBuilder^ Name);
            static void SetScaleName(String^ Name);       
            static void ClearPassword();        
            static bool ValidPassword();        
            static void GetPassword(array<byte>^ Password);
            static void SetPassword(array<byte>^ Password);
            static int GetCountry();       
            static void SetCountry(int Country);         
            static int GetLanguage();         
            static void SetLanguage(int Language);         
            static int GetCodePage();         
            static void SetCodePage(int CodePage);
            static int GetDeviceDateFormat();        
            static void SetDeviceDateFormat(int Format);        
            static wchar_t GetDateSeparator1();         
            static void SetDateSeparator1(wchar_t Separator);
            static wchar_t GetDateSeparator2();
            static void SetDateSeparator2(wchar_t Separator);
            static int GetDeviceTimeFormat();           
            static void SetDeviceTimeFormat(int Format);           
            static wchar_t GetTimeSeparator();
            static void SetTimeSeparator(wchar_t Separator);
            static int GetDaylightSavingType();          
            static void SetDaylightSavingType(int DstType);          
            static int GetWeighingUnits();           
            static void SetWeighingUnits(int Units);           
            static int GetWeighingCapacity();           
            static void SetWeighingCapacity(int Capacity);
            static int GetWeighingRange();          
            static int GetWeighingDecimals();         
            static int GetWeighingMaxDivision();
            // Get weighing max. division           
            static int GetWeighingDivision();          
            static void SetWeighingDivision(int Division);
            //---- get/set sound settings           
            static int GetToneDefault();          
            static void SetToneDefault(int Tone);          
            static int GetToneLight();          
            static void SetToneLight(int Tone);          
            static int GetToneOk();          
            static void SetToneOk(int Tone);          
            static int GetToneHeavy();          
            static void SetToneHeavy(int Tone);          
            static int GetToneKeyboard();          
            static void SetToneKeyboard(int Tone);          
            static bool GetEnableSpecialSounds();          
            static void SetEnableSpecialSounds(bool Enable);          
            static int GetVolumeSaving();          
            static void SetVolumeSaving(int Volume);          
            static int GetVolumeKeyboard();          
            static void SetVolumeKeyboard(int Volume);
            //---- get/set display settings           
            static int GetDisplayMode();           
            static void SetDisplayMode(int Mode);           
            static int GetDisplayContrast();           
            static void SetDisplayContrast(int Contrast);           
            static int GetBacklightMode();           
            static void SetBacklightMode(int Mode);          
            static int GetBacklightIntensity();          
            static void SetBacklightIntensity(int Intensity);         
            static int GetBacklightDuration();         
            static void SetBacklightDuration(int Duration);
            //---- get/set printer settings          
            static int GetPrinterPaperWidth();          
            static void SetPrinterPaperWidth(int Width);          
            static int GetPrinterCommunicationFormat();          
            static void SetPrinterCommunicationFormat(int Format);          
            static int GetPrinterCommunicationSpeed();          
            static void SetPrinterCommunicationSpeed(int Speed);
            //---- get/set global parameters          
            static int GetKeyboardTimeout();          
            static void SetKeyboardTimeout(int Timeout);          
            static int GetPowerOffTimeout();          
            static void SetPowerOffTimeout(int Timeout);         
            static bool GetEnableFileParameters();          
            static void SetEnableFileParameters(bool Enable);
            //---- get/set weighing parameters          
            static bool GetEnableMoreBirds();          
            static void SetEnableMoreBirds(bool Enable);         
            static int GetWeightSortingMode();         
            static void SetWeightSortingMode(int Mode);         
            static int GetSavingMode();         
            static void SetSavingMode(int Mode);
            static int GetFilter();          
            static void SetFilter(int Filter);         
            static int GetStabilisationTime();         
            static void SetStabilisationTime(int StabilisationTime);         
            static int GetMinimumWeight();         
            static void SetMinimumWeight(int Weight);         
            static int GetStabilisationRange();         
            static void SetStabilisationRange(int Range);
            //---- get/set statistics          
            static int GetUniformityRange();          
            static void SetUniformityRange(int Range);         
            static int GetHistogramMode();         
            static int GetHistogramRange();         
            static void SetHistogramRange(int Range);         
            static int GetHistogramStep();         
            static void SetHistogramStep(int Step);
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------           
            static int GetExceptionTimestamp();
            // Get exception date & time          
            static int GetExceptionAddress();          
            static int GetExceptionType();          
            static int GetExceptionStatus();        
            static int GetWatchDogTimestamp();
            // Get watchdog date & time          
            static int GetWatchDogStatus();
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //---- directory maitenance           
            static int GetFilesCount();           
            static void FilesDeleteAll();           
            static int FileCreate();
            // Create new file, returns index
            //---- get/set directory data         
            static void GetFileName(int Index, StringBuilder^ Name);           
            static void SetFileName(int Index, String^ Name);           
            static void GetFileNote(int Index, StringBuilder^ Note);
            static void SetFileNote(int Index, String^ Note);
            static unsigned int GetFileCreation(int Index);            
            static int GetFileRawSize(int Index);          
            static bool IsCurrentFile(int Index);           
            static void SetCurrentFile(int Index);          
            static bool GetFileEnableMoreBirds(int Index);          
            static void SetFileEnableMoreBirds(int Index, bool Enable);          
            static int GetFileNumberOfBirds(int Index);          
            static void SetFileNumberOfBirds(int Index, int NumberOfBirds);           
            static int GetFileWeightSortingMode(int Index);           
            static void SetFileWeightSortingMode(int Index, int Mode);          
            static int GetFileLowLimit(int Index);           
            static void SetFileLowLimit(int Index, int LowLimit);          
            static int GetFileHighLimit(int Index);           
            static void SetFileHighLimit(int Index, int HighLimit);          
            static int GetFileSavingMode(int Index);           
            static void SetFileSavingMode(int Index, int Mode);           
            static int GetFileFilter(int Index);           
            static void SetFileFilter(int Index, int Filter);          
            static int GetFileStabilisationTime(int Index);         
            static void SetFileStabilisationTime(int Index, int StabilisationTime);         
            static int GetFileMinimumWeight(int Index);          
            static void SetFileMinimumWeight(int Index, int Weight);          
            static int GetFileStabilisationRange(int Index);          
            static void SetFileStabilisationRange(int Index, int Range);         
            static int GetFileSamplesCount(int Index);           
            static void FileClearSamples(int Index);           
            static void FileAllocSamples(int Index, int SamplesCount);          
            static int GetSampleTimestamp(int Index, int SampleIndex);          
            static void SetSampleTimestamp(int Index, int SampleIndex, int Timestamp);          
            static int GetSampleWeight(int Index, int SampleIndex);          
            static void SetSampleWeight(int Index, int SampleIndex, int Weight);           
            static int GetSampleFlag(int Index, int SampleIndex);       
            static void SetSampleFlag(int Index, int SampleIndex, int Flag);      
            static int GetGroupsCount();           
            static void GroupsDeleteAll();          
            static int GroupCreate();     
            static void GetGroupName(int Index, StringBuilder^ Name);
            static void SetGroupName(int Index, String^ Name);          
            static void GetGroupNote(int Index, StringBuilder^ Note);
            static void SetGroupNote(int Index, String^ Note);           
            static unsigned int GetGroupCreation(int Index);              
            static int GetGroupFilesCount(int Index);          
            static void GroupClearFiles(int Index);          
            static int GetGroupFile(int Index, int FileIndex);          
            static void AddGroupFile(int Index, int FileIndex);
         };
      }
   }
}