#include "Dll.h"
//#include "Bat1Dll.h"
#include "ARM/Bat1/ConfigDef.h"
#include "ARM/Inc/File/FdirDef.h"

using namespace System::Runtime::InteropServices;
using namespace System::Text;
using namespace Bat1Library::Connection::Native;
using namespace Bat1Library;

unsigned int Dll::GetDllVersion()
{
   return Bat1DllH::GetDllVersion();
}

bool Dll::IsUsbLess()
{
   return Bat1DllH::IsUsbLess();
}

void Dll::EnableLogger(bool Enable)
{
   return Bat1DllH::EnableLogger(Enable);
}

void Dll::DecodeTime(int Timestamp, int %Day, int %Month, int %Year, int %Hour, int %Min, int %Sec)
{
   pin_ptr<int> ppDay = &Day;
   pin_ptr<int> ppMonth = &Month;
   pin_ptr<int> ppYear = &Year;
   pin_ptr<int> ppHour = &Hour;
   pin_ptr<int> ppMin = &Min;
   pin_ptr<int> ppSec = &Sec;
   return Bat1DllH::DecodeTime(Timestamp, ppDay, ppMonth, ppYear, ppHour, ppMin, ppSec);
}

int Dll::EncodeTime(int Day, int Month, int Year, int Hour, int Min, int Sec)
{
   return Bat1DllH::EncodeTime(Day, Month, Year, Hour, Min, Sec);
}

double Dll::DecodeWeight(int Weight)
{
   return Bat1DllH::DecodeWeight(Weight);
}

int Dll::EncodeWeight(double Weight)
{
   return Bat1DllH::EncodeWeight(Weight);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------           
bool Dll::CheckDevice()
{
   return Bat1DllH::CheckDevice();
}

bool Dll::DeviceIsOn(bool %PowerOn)
{
   TYesNo yn = PowerOn ? YES : NO;
   if (!Bat1DllH::DeviceIsOn(&yn)){
      PowerOn = false;
      return false;
   }
   PowerOn = yn == YES;
   return true;
}

bool Dll::DevicePowerOff()
{
   return Bat1DllH::DevicePowerOff();
}

bool Dll::GetTime(int %Clock)
{   
   pin_ptr<int> pp = &Clock;
   return Bat1DllH::GetTime(pp);
}

bool Dll::SetTime(int Clock)
{
   return Bat1DllH::SetTime(Clock);
}

void Dll::NewDevice()
{
   return Bat1DllH::NewDevice();
}

bool Dll::LoadConfiguration()
{
   return Bat1DllH::LoadConfiguration();
}

bool Dll::LoadDevice()
{
   return Bat1DllH::LoadDevice();
}

bool Dll::SaveDevice()
{
   return Bat1DllH::SaveDevice();
}

bool Dll::ReloadConfiguration()
{
   return Bat1DllH::ReloadConfiguration();
}

bool Dll::LoadEstimation(int %Promile)
{
   pin_ptr<int> pp = &Promile;
   return Bat1DllH::LoadEstimation(pp);
}

void Dll::SaveEstimation(int %Promile)
{
   pin_ptr<int> pp = &Promile;
   return Bat1DllH::SaveEstimation(pp);
}

bool Dll::LoadCrashInfo()
{
   return Bat1DllH::LoadCrashInfo();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------           
bool Dll::ReadEeprom(int Address, array<byte>^ Buffer, int Size)
{
   pin_ptr<array<byte> ^> buf = &Buffer;
   return Bat1DllH::ReadEeprom(Address, (unsigned char*)buf, Size);
}

bool Dll::WriteEeprom(int Address, array<byte>^ Buffer, int Size)
{
   pin_ptr<array<byte> ^> buf = &Buffer;
   return Bat1DllH::WriteEeprom(Address, (unsigned char*)buf, Size);
}

int Dll::GetEepromSize()
{
   return Bat1DllH::GetEepromSize();
}

bool Dll::DeviceByEeprom(array<byte>^ Buffer)
{
   pin_ptr<array<byte> ^> buf = &Buffer;
   return Bat1DllH::DeviceByEeprom((unsigned char*)buf);
}

int Dll::GetLogoSize()
{
   return Bat1DllH::GetLogoSize();
}

int Dll::GetLogoAddress()
{
   return Bat1DllH::GetLogoAddress();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------        
unsigned int Dll::GetDeviceVersion()
{
   return Bat1DllH::GetDeviceVersion();
}

void Dll::SetDeviceVersion(unsigned int Version)
{
   return Bat1DllH::SetDeviceVersion(Version);
}

unsigned int Dll::GetBuild()
{
   return Bat1DllH::GetBuild();
}

void Dll::SetBuild(unsigned int Build)
{
   return Bat1DllH::SetBuild(Build);
}

unsigned int Dll::GetHwVersion()
{
   return Bat1DllH::GetHwVersion();
}

void Dll::SetHwVersion(unsigned int Version)
{
   return Bat1DllH::SetHwVersion(Version);
}

void Dll::GetScaleName(StringBuilder^ Name)
{
   char buf[SCALE_NAME_LENGTH + 1];
   Bat1DllH::GetScaleName(buf);
     
   int len = strlen(buf);
   array<Byte>^ byteArray = gcnew array<Byte>(len);
   Marshal::Copy((IntPtr)buf, byteArray, 0, len);    
   for (int i = byteArray->GetLowerBound(0); i <= byteArray->GetUpperBound(0); i++) 
   {
      char dc = *(Byte^)byteArray->GetValue(i);
      Name->Append((Char)dc);    
   } 
}

void Dll::SetScaleName(String^ Name)
{
   IntPtr ptr = Marshal::StringToHGlobalAnsi(Name);
   Bat1DllH::SetScaleName((char*)ptr.ToPointer());
   Marshal::FreeHGlobal(ptr);
}

void Dll::ClearPassword()
{
   return Bat1DllH::ClearPassword();
}

bool Dll::ValidPassword()
{
   return Bat1DllH::ValidPassword();
}

void Dll::GetPassword(array<byte>^ Password)
{
   pin_ptr<array<byte> ^> buf = &Password;
   return Bat1DllH::GetPassword((unsigned char*)buf);
}

void Dll::SetPassword(array<byte>^ Password)
{
   pin_ptr<array<byte> ^> buf = &Password;
   return Bat1DllH::SetPassword((unsigned char*)buf);
}

int Dll::GetCountry()
{
   return Bat1DllH::GetCountry();
}

void Dll::SetCountry(int Country)
{
   return Bat1DllH::SetCountry(Country);
}

int Dll::GetLanguage()
{
   return Bat1DllH::GetLanguage();
}

void Dll::SetLanguage(int Language)
{
   return Bat1DllH::SetLanguage(Language);
}

int Dll::GetCodePage()
{
   return Bat1DllH::GetCodePage();
}

void Dll::SetCodePage(int CodePage)
{
   return Bat1DllH::SetCodePage(CodePage);
}

int Dll::GetDeviceDateFormat()
{
   return Bat1DllH::GetDeviceDateFormat();
}

void Dll::SetDeviceDateFormat(int Format)
{
   return Bat1DllH::SetDeviceDateFormat(Format);
}

wchar_t Dll::GetDateSeparator1()
{
   return Bat1DllH::GetDateSeparator1();
}

void Dll::SetDateSeparator1(wchar_t Separator)
{
   return Bat1DllH::SetDateSeparator1(Separator);
}

wchar_t Dll::GetDateSeparator2()
{
   return Bat1DllH::GetDateSeparator2();
}

void Dll::SetDateSeparator2(wchar_t Separator)
{
   return Bat1DllH::SetDateSeparator2(Separator);
}

int Dll::GetDeviceTimeFormat()
{
   return Bat1DllH::GetDeviceTimeFormat();
}

void Dll::SetDeviceTimeFormat(int Format)
{
   return Bat1DllH::SetDeviceTimeFormat(Format);
}

wchar_t Dll::GetTimeSeparator()
{
   return Bat1DllH::GetTimeSeparator();
}

void Dll::SetTimeSeparator(wchar_t Separator)
{
   return Bat1DllH::SetTimeSeparator(Separator);
}

int Dll::GetDaylightSavingType()
{
   return Bat1DllH::GetDaylightSavingType();
}

void Dll::SetDaylightSavingType(int DstType)
{
   return Bat1DllH::SetDaylightSavingType(DstType);
}

int Dll::GetWeighingUnits()
{
   return Bat1DllH::GetWeighingUnits();
}

void Dll::SetWeighingUnits(int Units)
{
   return Bat1DllH::SetWeighingUnits(Units);
}

int Dll::GetWeighingCapacity()
{
   return Bat1DllH::GetWeighingCapacity();
}

void Dll::SetWeighingCapacity(int Capacity)
{
   return Bat1DllH::SetWeighingCapacity(Capacity);
}

int Dll::GetWeighingRange()
{
   return Bat1DllH::GetWeighingRange();
}

int Dll::GetWeighingDecimals()
{
   return Bat1DllH::GetWeighingDecimals();
}

int Dll::GetWeighingMaxDivision()
{
   return Bat1DllH::GetWeighingMaxDivision();
}

// Get weighing max. division           
int Dll::GetWeighingDivision()
{
   return Bat1DllH::GetWeighingDivision();
}

void Dll::SetWeighingDivision(int Division)
{
   return Bat1DllH::SetWeighingDivision(Division);
}

//---- get/set sound settings           
int Dll::GetToneDefault()
{
   return Bat1DllH::GetToneDefault();
}

void Dll::SetToneDefault(int Tone)
{
   return Bat1DllH::SetToneDefault(Tone);
}

int Dll::GetToneLight()
{
   return Bat1DllH::GetToneLight();
}

void Dll::SetToneLight(int Tone)
{
   return Bat1DllH::SetToneLight(Tone);
}

int Dll::GetToneOk()
{
   return Bat1DllH::GetToneOk();
}

void Dll::SetToneOk(int Tone)
{
   return Bat1DllH::SetToneOk(Tone);
}

int Dll::GetToneHeavy()
{
   return Bat1DllH::GetToneHeavy();
}

void Dll::SetToneHeavy(int Tone)
{
   return Bat1DllH::SetToneHeavy(Tone);
}

int Dll::GetToneKeyboard()
{
   return Bat1DllH::GetToneKeyboard();
}

void Dll::SetToneKeyboard(int Tone)
{
   return Bat1DllH::SetToneKeyboard(Tone);
}

bool Dll::GetEnableSpecialSounds()
{
   return Bat1DllH::GetEnableSpecialSounds();
}

void Dll::SetEnableSpecialSounds(bool Enable)
{
   return Bat1DllH::SetEnableSpecialSounds(Enable);
}

int Dll::GetVolumeSaving()
{
   return Bat1DllH::GetVolumeSaving();
}

void Dll::SetVolumeSaving(int Volume)
{
   return Bat1DllH::SetVolumeSaving(Volume);
}

int Dll::GetVolumeKeyboard()
{
   return Bat1DllH::GetVolumeKeyboard();
}

void Dll::SetVolumeKeyboard(int Volume)
{
   return Bat1DllH::SetVolumeKeyboard(Volume);
}

//---- get/set display settings           
int Dll::GetDisplayMode()
{
   return Bat1DllH::GetDisplayMode();
}

void Dll::SetDisplayMode(int Mode)
{
   return Bat1DllH::SetDisplayMode(Mode);
}

int Dll::GetDisplayContrast()
{
   return Bat1DllH::GetDisplayContrast();
}

void Dll::SetDisplayContrast(int Contrast)
{
   return Bat1DllH::SetDisplayContrast(Contrast);
}

int Dll::GetBacklightMode()
{
   return Bat1DllH::GetBacklightMode();
}

void Dll::SetBacklightMode(int Mode)
{
   return Bat1DllH::SetBacklightMode(Mode);
}

int Dll::GetBacklightIntensity()
{
   return Bat1DllH::GetBacklightIntensity();
}

void Dll::SetBacklightIntensity(int Intensity)
{
   return Bat1DllH::SetBacklightIntensity(Intensity);
}

int Dll::GetBacklightDuration()
{
   return Bat1DllH::GetBacklightDuration();
}

void Dll::SetBacklightDuration(int Duration)
{
   return Bat1DllH::SetBacklightDuration(Duration);
}

//---- get/set printer settings          
int Dll::GetPrinterPaperWidth()
{
   return Bat1DllH::GetPrinterPaperWidth();
}

void Dll::SetPrinterPaperWidth(int Width)
{
   return Bat1DllH::SetPrinterPaperWidth(Width);
}

int Dll::GetPrinterCommunicationFormat()
{
   return Bat1DllH::GetPrinterCommunicationFormat();
}

void Dll::SetPrinterCommunicationFormat(int Format)
{
   return Bat1DllH::SetPrinterCommunicationFormat(Format);
}

int Dll::GetPrinterCommunicationSpeed()
{
   return Bat1DllH::GetPrinterCommunicationSpeed();
}

void Dll::SetPrinterCommunicationSpeed(int Speed)
{
   return Bat1DllH::SetPrinterCommunicationSpeed(Speed);
}

//---- get/set global parameters          
int Dll::GetKeyboardTimeout()
{
   return Bat1DllH::GetKeyboardTimeout();
}

void Dll::SetKeyboardTimeout(int Timeout)
{
   return Bat1DllH::SetKeyboardTimeout(Timeout);
}

int Dll::GetPowerOffTimeout()
{
   return Bat1DllH::GetPowerOffTimeout();
}

void Dll::SetPowerOffTimeout(int Timeout)
{
   return Bat1DllH::SetPowerOffTimeout(Timeout);
}

bool Dll::GetEnableFileParameters()
{
   return Bat1DllH::GetEnableFileParameters();
}

void Dll::SetEnableFileParameters(bool Enable)
{
   return Bat1DllH::SetEnableFileParameters(Enable);
}

//---- get/set weighing parameters          
bool Dll::GetEnableMoreBirds()
{
   return Bat1DllH::GetEnableMoreBirds();
}

void Dll::SetEnableMoreBirds(bool Enable)
{
   return Bat1DllH::SetEnableMoreBirds(Enable);
}

int Dll::GetWeightSortingMode()
{
   return Bat1DllH::GetWeightSortingMode();
}

void Dll::SetWeightSortingMode(int Mode)
{
   return Bat1DllH::SetWeightSortingMode(Mode);
}

int Dll::GetSavingMode()
{
   return Bat1DllH::GetSavingMode();
}

void Dll::SetSavingMode(int Mode)
{
   return Bat1DllH::SetSavingMode(Mode);
}

int Dll::GetFilter()
{
   return Bat1DllH::GetFilter();
}

void Dll::SetFilter(int Filter)
{
   return Bat1DllH::SetFilter(Filter);
}

int Dll::GetStabilisationTime()
{
   return Bat1DllH::GetStabilisationTime();
}

void Dll::SetStabilisationTime(int StabilisationTime)
{
   return Bat1DllH::SetStabilisationTime(StabilisationTime);
}

int Dll::GetMinimumWeight()
{
   return Bat1DllH::GetMinimumWeight();
}

void Dll::SetMinimumWeight(int Weight)
{
   return Bat1DllH::SetMinimumWeight(Weight);
}

int Dll::GetStabilisationRange()
{
   return Bat1DllH::GetStabilisationRange();
}

void Dll::SetStabilisationRange(int Range)
{
   return Bat1DllH::SetStabilisationRange(Range);
}

//---- get/set statistics          
int Dll::GetUniformityRange()
{
   return Bat1DllH::GetUniformityRange();
}

void Dll::SetUniformityRange(int Range)
{
   return Bat1DllH::SetUniformityRange(Range);
}

int Dll::GetHistogramMode()
{
   return Bat1DllH::GetHistogramMode();
}

int Dll::GetHistogramRange()
{
   return Bat1DllH::GetHistogramRange();
}

void Dll::SetHistogramRange(int Range)
{
   return Bat1DllH::SetHistogramRange(Range);
}

int Dll::GetHistogramStep()
{
   return Bat1DllH::GetHistogramStep();
}

void Dll::SetHistogramStep(int Step)
{
   return Bat1DllH::SetHistogramStep(Step);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------           
int Dll::GetExceptionTimestamp()
{
   return Bat1DllH::GetExceptionTimestamp();
}

// Get exception date & time          
int Dll::GetExceptionAddress()
{
   return Bat1DllH::GetExceptionAddress();
}

int Dll::GetExceptionType()
{
   return Bat1DllH::GetExceptionType();
}

int Dll::GetExceptionStatus()
{
   return Bat1DllH::GetExceptionStatus();
}

int Dll::GetWatchDogTimestamp()
{
   return Bat1DllH::GetWatchDogTimestamp();
}

// Get watchdog date & time          
int Dll::GetWatchDogStatus()
{
   return Bat1DllH::GetWatchDogStatus();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//---- directory maitenance           
int Dll::GetFilesCount()
{
   return Bat1DllH::GetFilesCount();
}

void Dll::FilesDeleteAll()
{
   return Bat1DllH::FilesDeleteAll();
}

int Dll::FileCreate()
{
   return Bat1DllH::FileCreate();
}

// Create new file, returns index
//---- get/set directory data         
void Dll::GetFileName(int Index, StringBuilder^ Name)
{
   char buf[FDIR_NAME_LENGTH + 1];
   Bat1DllH::GetFileName(Index, buf);
   
   int len = strlen(buf);
   array<Byte>^ byteArray = gcnew array<Byte>(len);
   Marshal::Copy((IntPtr)buf, byteArray, 0, len);
   for (int i = byteArray->GetLowerBound(0); i <= byteArray->GetUpperBound(0); i++)
   {
      char dc = *(Byte^)byteArray->GetValue(i);
      Name->Append((Char)dc);
   }
}

void Dll::SetFileName(int Index, String^ Name)
{
   IntPtr ptr = Marshal::StringToHGlobalAnsi(Name);
   Bat1DllH::SetFileName(Index, (char*)ptr.ToPointer());
   Marshal::FreeHGlobal(ptr);
}

void Dll::GetFileNote(int Index, StringBuilder^ Note)
{
   Note->Clear();

   char buf[FDIR_NOTE_LENGTH + 1];
   Bat1DllH::GetFileNote(Index, buf);
   
   int len = strlen(buf);
   array<Byte>^ byteArray = gcnew array<Byte>(len);
   Marshal::Copy((IntPtr)buf, byteArray, 0, len);
   for (int i = byteArray->GetLowerBound(0); i <= byteArray->GetUpperBound(0); i++)
   {
      char dc = *(Byte^)byteArray->GetValue(i);
      Note->Append((Char)dc);
   }
}

void Dll::SetFileNote(int Index, String^ Note)
{
   IntPtr ptr = Marshal::StringToHGlobalAnsi(Note);
   Bat1DllH::SetFileNote(Index, (char*)ptr.ToPointer());
   Marshal::FreeHGlobal(ptr);
}

unsigned int Dll::GetFileCreation(int Index)
{
   return Bat1DllH::GetFileCreation(Index);
}

int Dll::GetFileRawSize(int Index)
{
   return Bat1DllH::GetFileRawSize(Index);
}

bool Dll::IsCurrentFile(int Index)
{
   return Bat1DllH::IsCurrentFile(Index);
}

void Dll::SetCurrentFile(int Index)
{
   return Bat1DllH::SetCurrentFile(Index);
}

bool Dll::GetFileEnableMoreBirds(int Index)
{
   return Bat1DllH::GetFileEnableMoreBirds(Index);
}

void Dll::SetFileEnableMoreBirds(int Index, bool Enable)
{
   return Bat1DllH::SetFileEnableMoreBirds(Index, Enable);
}

int Dll::GetFileNumberOfBirds(int Index)
{
   return Bat1DllH::GetFileNumberOfBirds(Index);
}

void Dll::SetFileNumberOfBirds(int Index, int NumberOfBirds)
{
   return Bat1DllH::SetFileNumberOfBirds(Index, NumberOfBirds);
}

int Dll::GetFileWeightSortingMode(int Index)
{
   return Bat1DllH::GetFileWeightSortingMode(Index);
}

void Dll::SetFileWeightSortingMode(int Index, int Mode)
{
   return Bat1DllH::SetFileWeightSortingMode(Index, Mode);
}

int Dll::GetFileLowLimit(int Index)
{
   return Bat1DllH::GetFileLowLimit(Index);
}

void Dll::SetFileLowLimit(int Index, int LowLimit)
{
   return Bat1DllH::SetFileLowLimit(Index, LowLimit);
}

int Dll::GetFileHighLimit(int Index)
{
   return Bat1DllH::GetFileHighLimit(Index);
}

void Dll::SetFileHighLimit(int Index, int HighLimit)
{
   return Bat1DllH::SetFileHighLimit(Index, HighLimit);
}

int Dll::GetFileSavingMode(int Index)
{
   return Bat1DllH::GetFileSavingMode(Index);
}

void Dll::SetFileSavingMode(int Index, int Mode)
{
   return Bat1DllH::SetFileSavingMode(Index, Mode);
}

int Dll::GetFileFilter(int Index)
{
   return Bat1DllH::GetFileFilter(Index);
}

void Dll::SetFileFilter(int Index, int Filter)
{
   return Bat1DllH::SetFileFilter(Index, Filter);
}

int Dll::GetFileStabilisationTime(int Index)
{
   return Bat1DllH::GetFileStabilisationTime(Index);
}

void Dll::SetFileStabilisationTime(int Index, int StabilisationTime)
{
   return Bat1DllH::SetFileStabilisationTime(Index, StabilisationTime);
}

int Dll::GetFileMinimumWeight(int Index)
{
   return Bat1DllH::GetFileMinimumWeight(Index);
}

void Dll::SetFileMinimumWeight(int Index, int Weight)
{
   return Bat1DllH::SetFileMinimumWeight(Index, Weight);
}

int Dll::GetFileStabilisationRange(int Index)
{
   return Bat1DllH::GetFileStabilisationRange(Index);
}

void Dll::SetFileStabilisationRange(int Index, int Range)
{
   return Bat1DllH::SetFileStabilisationRange(Index, Range);
}

int Dll::GetFileSamplesCount(int Index)
{
   return Bat1DllH::GetFileSamplesCount(Index);
}

void Dll::FileClearSamples(int Index)
{
   return Bat1DllH::FileClearSamples(Index);
}

void Dll::FileAllocSamples(int Index, int SamplesCount)
{
   return Bat1DllH::FileAllocSamples(Index, SamplesCount);
}

int Dll::GetSampleTimestamp(int Index, int SampleIndex)
{
   return Bat1DllH::GetSampleTimestamp(Index, SampleIndex);
}

void Dll::SetSampleTimestamp(int Index, int SampleIndex, int Timestamp)
{
   return Bat1DllH::SetSampleTimestamp(Index, SampleIndex, Timestamp);
}

int Dll::GetSampleWeight(int Index, int SampleIndex)
{
   return Bat1DllH::GetSampleWeight(Index, SampleIndex);
}

void Dll::SetSampleWeight(int Index, int SampleIndex, int Weight)
{
   return Bat1DllH::SetSampleWeight(Index, SampleIndex, Weight);
}

int Dll::GetSampleFlag(int Index, int SampleIndex)
{
   return Bat1DllH::GetSampleFlag(Index, SampleIndex);
}

void Dll::SetSampleFlag(int Index, int SampleIndex, int Flag)
{
   return Bat1DllH::SetSampleFlag(Index, SampleIndex, Flag);
}

int Dll::GetGroupsCount()
{
   return Bat1DllH::GetGroupsCount();
}

void Dll::GroupsDeleteAll()
{
   return Bat1DllH::GroupsDeleteAll();
}

int Dll::GroupCreate()
{
   return Bat1DllH::GroupCreate();
}

void Dll::GetGroupName(int Index, StringBuilder^ Name)
{
   char buf[FDIR_NAME_LENGTH + 1];
   Bat1DllH::GetGroupName(Index, buf);
   
   int len = strlen(buf);
   array<Byte>^ byteArray = gcnew array<Byte>(len);
   Marshal::Copy((IntPtr)buf, byteArray, 0, len);
   for (int i = byteArray->GetLowerBound(0); i <= byteArray->GetUpperBound(0); i++)
   {
      char dc = *(Byte^)byteArray->GetValue(i);
      Name->Append((Char)dc);
   }
}

void Dll::SetGroupName(int Index, String^ Name)
{
   IntPtr ptr = Marshal::StringToHGlobalAnsi(Name);
   Bat1DllH::SetGroupName(Index, (char*)ptr.ToPointer());
   Marshal::FreeHGlobal(ptr);
}

void Dll::GetGroupNote(int Index, StringBuilder^ Note)
{
   char buf[FDIR_NOTE_LENGTH + 1];
   Bat1DllH::GetGroupNote(Index, buf);
   
   int len = strlen(buf);
   array<Byte>^ byteArray = gcnew array<Byte>(len);
   Marshal::Copy((IntPtr)buf, byteArray, 0, len);
   for (int i = byteArray->GetLowerBound(0); i <= byteArray->GetUpperBound(0); i++)
   {
      char dc = *(Byte^)byteArray->GetValue(i);
      Note->Append((Char)dc);
   }
}

void Dll::SetGroupNote(int Index, String^ Note)
{
   IntPtr ptr = Marshal::StringToHGlobalAnsi(Note);
   Bat1DllH::SetGroupNote(Index, (char*)ptr.ToPointer());
   Marshal::FreeHGlobal(ptr);
}

unsigned int Dll::GetGroupCreation(int Index)
{
   return Bat1DllH::GetGroupCreation(Index);
}

int Dll::GetGroupFilesCount(int Index)
{
   return Bat1DllH::GetGroupFilesCount(Index);
}

void Dll::GroupClearFiles(int Index)
{
   return Bat1DllH::GroupClearFiles(Index);
}

int Dll::GetGroupFile(int Index, int FileIndex)
{
   return Bat1DllH::GetGroupFile(Index, FileIndex);
}

void Dll::AddGroupFile(int Index, int FileIndex)
{
   return Bat1DllH::AddGroupFile(Index, FileIndex);
}

