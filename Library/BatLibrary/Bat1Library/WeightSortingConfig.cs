﻿namespace Bat1Library
{
   /// <summary>
   /// Weight sorting
   /// </summary>
   public struct WeightSortingConfig {
      public WeightSorting Mode;	               // TWeightSortingMode - Mode of limits used
      public double        LowLimit;		       // Limit / Low limit
      public double        HighLimit;		       // High limit
   }
}