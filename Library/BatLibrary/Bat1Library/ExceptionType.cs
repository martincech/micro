namespace Bat1Library
{
   public enum ExceptionType
   {
      UNDEFINED, // unknowm instruction
      SWI, // sw interrupt
      PREFETCH_ABORT, // code address out of range
      DATA_ABORT, // data address out of range
   };
}