﻿namespace Bat1Library
{
   /// <summary>
   /// Histogram mode
   /// </summary>
   public enum HistogramMode {
      RANGE,
      STEP,
   }
}