namespace Bat1Library
{
   /// <summary>
   /// Flag stored at each sample
   /// </summary>
   public enum Flag {
      NONE = 0,       // Flags not used during weighing
      LIGHT,          // Weight lower than low limit
      OK,             // Weight within low and high limits
      HEAVY,          // Weight higher than high limit
      MALE,           // Male
      FEMALE,         // Female
      _COUNT,
      ALL             // Use all flags (for searching only)
   };
}