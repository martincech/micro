﻿namespace Bat1Library
{
   /// <summary>
   /// Display mode
   /// </summary>
   public enum DisplayMode {
      DISPLAY_MODE_BASIC,                 // Basic display mode
      DISPLAY_MODE_ADVANCED,              // Advanced display mode
      DISPLAY_MODE_STRONG,                // Strong display mode
      _DISPLAY_MODE_COUNT
   }
}