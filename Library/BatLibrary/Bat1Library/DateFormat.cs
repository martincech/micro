namespace Bat1Library
{
   public enum DateFormat
   {
      DDMMYYYY,
      MMDDYYYY,
      YYYYMMDD,
      YYYYDDMM,

      DDMMMYYYY,
      MMMDDYYYY,
      YYYYMMMDD,
      YYYYDDMMM,
   };
}