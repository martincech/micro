namespace Bat2Library.Sms
{
    public enum SmsOperationMode
    {
        STANDBY,            // Modem prihlaseny, opakovane cteni sily signalu
        READ_ALL,           // Nacteni vsech SMS v pameti
        SEND,               // Poslani 1 SMS
        REQUEST             // Request statistiky z urciteho cisla
    }
}