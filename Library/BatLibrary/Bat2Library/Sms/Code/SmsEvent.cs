﻿namespace Bat2Library.Sms
{
    public enum SmsEvent
    {
        INIT,               // Inicializace
        DETECT,             // Detekce modemu na zadanem COM portu
        RESET,              // Reset
        CHECK,              // Kontrola pritomnosti modemu
        SIGNAL,             // Cekani na signal
        OPERATOR,           // Nacteni operatora
        STRENGTH,           // Nacteni sily signalu
        READY,              // Cekani, zaroven update sily signalu
        SMS_SENDING,        // Posilani SMS (progress = index tel. cisla v seznamu)
        SMS_SEND_OK,        // Odeslana SMS (progress = index tel. cisla v seznamu)
        SMS_SEND_ERROR,     // Chyba pri odesilani SMS (progress = index tel. cisla v seznamu)
        SMS_READING,        // Cteni SMS na dane pozici
        SMS_READ_OK,        // Nactena SMS na dane pozici
        SMS_READ_ERROR,     // Chyba cteni SMS na dane pozici
        SMS_DELETING,       // Mazani SMS na dane pozici
        SMS_DELETE_OK,      // Smazana SMS na dane pozici
        SMS_DELETE_ERROR,   // Chyba pri mazani SMS na dane pozici
        CANCEL              // Rucni preruseni
    }
}