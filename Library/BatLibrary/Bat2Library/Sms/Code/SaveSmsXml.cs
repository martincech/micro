﻿using System;
using System.IO;
using System.Linq.Expressions;
using System.Xml;

namespace Bat2Library.Sms
{
   [System.Obsolete("Use XmlSerializer class")]
    public class SaveSmsXml
    {
        public static bool Save(string path, SmsStatus sms)
        {
            XmlDocument xmlDoc = new XmlDocument();

            XmlNode root = null;
            if (File.Exists(path))
            {
                xmlDoc.Load(path);
                root = xmlDoc.DocumentElement;
            }
            else
            {
                XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);
                root = xmlDoc.CreateElement("Messages");
                xmlDoc.InsertBefore(xmlDeclaration, xmlDoc.DocumentElement);
                xmlDoc.AppendChild(root);
            }


            XmlElement body = xmlDoc.CreateElement("Message");
            root.AppendChild(body);

            XmlElement elem = xmlDoc.CreateElement("Date");
            elem.InnerText = DateTime.Now.ToString();
            body.AppendChild(elem);

            elem = xmlDoc.CreateElement(Get<SmsStatus, string>(x => x.ModemName));
            elem.InnerText = sms.ModemName;
            body.AppendChild(elem);

            elem = xmlDoc.CreateElement(Get<SmsStatus, string>(x => x.OperatorName));
            elem.InnerText = sms.OperatorName;
            body.AppendChild(elem);

            elem = xmlDoc.CreateElement(Get<SmsStatus, string>(x => x.SmsNumber));
            elem.InnerText = sms.SmsNumber;
            body.AppendChild(elem);

            elem = xmlDoc.CreateElement(Get<SmsStatus, string>(x => x.SmsText));
            elem.InnerText = sms.SmsText;
            body.AppendChild(elem);

            elem = xmlDoc.CreateElement(Get<SmsStatus, string>(x => x.PortName));
            elem.InnerText = sms.PortName;
            body.AppendChild(elem);

            elem = xmlDoc.CreateElement(Get<SmsStatus, int>(x => x.SignalStrength));
            elem.InnerText = sms.SignalStrength.ToString();
            body.AppendChild(elem);

            xmlDoc.Save(path);
            return true;
        }

        private static string Get<T, TResult>(Expression<Func<T, TResult>> expression)
        {
            var me = expression.Body as MemberExpression;
            if (me != null)
            {
                return me.Member.Name;
            }
            return null;
        }
    }
}
