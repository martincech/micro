//******************************************************************************
//
//   SocketIfBootloaderMsd.h         MSD socket
//   Version 1.0           (c) Veit Electronics
//
//******************************************************************************


#include "BootloaderSocket.h"
#undef _MANAGED
#include "Remote\SocketIfBootloaderMsd.h"
#include "Remote\Rc.h"
#include "File\Efs.h"


using namespace System;
using namespace Usb::UMS;
using namespace System::Runtime::InteropServices;

//******************************************************************************
// Constructor
//******************************************************************************

BootloaderSocketIf::BootloaderSocketIf(System::String ^efsRoot)
{
   IntPtr strPtr = Marshal::StringToHGlobalAnsi(efsRoot);
   ::EfsRoot((const char *)strPtr.ToPointer());
}

bool BootloaderSocketIf::OpenForRead(void)
// Open socket for cmd receive
{
   if (IsOpenedForRead()){
      return true;
   }
   Close();
   return SocketIfBootloaderMsdOpenForReceiveCmd() == YES;
}

bool BootloaderSocketIf::OpenForWrite(void)
// Open socket for send cmd
{
   if (IsOpenedForWrite()){
      return true;
   }
   Close();
   return SocketIfBootloaderMsdOpenForSendCmd(this) == YES;
}

bool BootloaderSocketIf::IsOpenedForRead()
{
   return (Permission() & FILE_MODE_READ_ONLY) == FILE_MODE_READ_ONLY;
}
bool BootloaderSocketIf::IsOpenedForWrite()
{
   return (Permission() & FILE_MODE_WRITE_ONLY) == FILE_MODE_WRITE_ONLY;
}


byte BootloaderSocketIf::State(void)
// Gets state of socket
{
   return SocketIfBootloaderMsdState(this);
}

TYesNo BootloaderSocketIf::Receive(void *Buffer, int Size)
{
   if (!OpenForRead()){
      return NO;
   }
   return SocketIfBootloaderMsdReceive(this, Buffer, Size);
}

int BootloaderSocketIf::ReceiveSize(void)
// Gets number of received bytes
{
   return SocketIfBootloaderMsdReceiveSize(this);
}

TYesNo BootloaderSocketIf::Send(const void *Buffer, int Size)
// Send <Buffer> with <Size>
{
   if (!OpenForWrite()){
      return NO;
   }
   return SocketIfBootloaderMsdSend(this, Buffer, Size);
}

void BootloaderSocketIf::Close(void)
// Close socket
{
   SocketIfBootloaderMsdClose(this);
}

byte BootloaderSocketIf::Permission(void)
//
{
   return SocketIfBootloaderMsdPermission(this);
}
