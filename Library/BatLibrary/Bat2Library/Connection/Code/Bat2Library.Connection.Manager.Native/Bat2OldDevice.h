#pragma once
#include "Bat2OldINativeSocket.h"
#include "Bat2Device.h"

namespace Bat2Library
{
   namespace Connection
   {
      namespace Manager
      {
         namespace Native{

            private ref class Bat2OldDevice : public Bat2Device<BaseVersionInfo ^, OldConfiguration ^, Bat2OldINativeSocket^, OldBat2DeviceData ^>
            {
            protected:
               Bat2OldDevice(Bat2OldINativeSocket ^socket, OldBat2DeviceData ^deviceData)
                  : Bat2Device(socket, deviceData)
               {
               }
            };
         }
      }
   }
}