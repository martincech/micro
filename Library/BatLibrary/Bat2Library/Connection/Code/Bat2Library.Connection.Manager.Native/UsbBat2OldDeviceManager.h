#pragma once
#include "UsbBat2OldDevice.h"
#include "Bat2OldDeviceManager.h"
#include "Bat2OldDataRW.h"

namespace Bat2Library
{
   namespace Connection
   {
      namespace Manager
      {
         namespace Native{
            using namespace System;
            using namespace System::Collections::Generic;
            using namespace System::Collections::ObjectModel;
            using namespace Bat2Library::Connection::Interface::IO;
			   using namespace Usb;
            using namespace Usb::FTDI;

			   public ref class UsbBat2OldDeviceManager : Bat2OldDeviceManager
               {
               public:
                  static property UsbBat2OldDeviceManager ^Instance{
                     UsbBat2OldDeviceManager ^get(){
                        Lock l(UsbBat2OldDeviceManager::typeid);
                        if (_instance == nullptr){
                           _instance = gcnew UsbBat2OldDeviceManager();
                        }
                        return _instance;
                     }
                  }

               protected:
                  virtual Bat2OldDataRW ^CreateDataRwObject(FTDIDevice ^forDevice)
                  {
                     UsbBat2OldDevice ^b2d = CreateUsbBat2OldDevice(forDevice);
                     return CreateDataRwObject(b2d);
                  }

                  static UsbBat2OldDevice ^CreateUsbBat2OldDevice(FTDIDevice ^forDevice)
                  {
                     Bat2OldUsbSocket ^socket = gcnew Bat2OldUsbSocket(forDevice->Open());
                     return gcnew UsbBat2OldDevice(socket);
                  }

               private:
                  UsbBat2OldDeviceManager();
                  static UsbBat2OldDeviceManager ^_instance = nullptr;
			         Dictionary<FTDIDevice ^, Bat2OldDevice ^> ^existingDevices;

                  UsbDeviceManager ^usbWatcher;
                  void OnDeviceConnected(System::Object ^sender, UsbDevice ^e);
                  void OnDeviceDisconnected(System::Object ^sender, UsbDevice ^e);

#ifdef _DEBUG
               internal:
                  IBat2OldDataReader ^GetDataReader(FTDIDevice ^forDevice)
                  {
                     return CreateDataRwObject(forDevice);
                  }

                  IBat2OldDataWriter ^GetDataWriter(FTDIDevice ^forDevice)
                  {
                     return CreateDataRwObject(forDevice);
                  }

                  IBat2ActionCommander ^GetActionCommander(FTDIDevice ^forDevice)
                  {
                     return CreateDataRwObject(forDevice);
                  }
#endif
               };
         }
      }
   }
}