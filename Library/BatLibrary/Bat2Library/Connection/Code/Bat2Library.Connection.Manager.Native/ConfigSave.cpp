#include "Bat2NewDataRW.h"
using namespace System;
using namespace System::Collections::Generic;
using namespace Bat2Library::Connection::Manager::Native;
using namespace Bat2Library::Connection::Interface::Domain;
using namespace System::Runtime::InteropServices;
#include <assert.h>

#undef _MANAGED
#include "Config\Config.h"
#include "Config\ConfigDef.h"
#include "Display\DisplayConfigurationDef.h"
#include "Weighing\WeighingConfigurationDef.h"
#include "Curve\CurveList.h"
#include "Curve\CorrectionList.h"
#include "Scheduler\WeighingPlanList.h"
#include "Message\GsmMessageDef.h"
#include "Rs485\Rs485Config.h"
#include "Modbus\ModbusConfigDef.h"
#include "Megavi\MegaviConfigDef.h"
#include "SmsGate\SmsGateConfigDef.h"
#include "Dacs\DacsConfigDef.h"
#include "CurvesPlan.h"
#include "Communication\CommunicationDef.h"
#include "Weight\Weight.h"


void toNative(Connection::Interface::Domain::Configuration ^config);
void toNativeVersion(VersionInfo ^d);
void toNativeDevice(DeviceInfo ^d);
void toNativeCountry(Connection::Interface::Domain::Country ^d);
void toNativeWeightUnits(Bat2Library::Connection::Interface::Domain::WeightUnits ^d);
void toNativeDisplayConfiguration(DisplayConfiguration ^d);
void toNativeWeighingConfiguration(WeighingConfiguration ^d);
void toNativeRs485Configuration(List<Connection::Interface::Domain::Rs485Options^> ^d);
void toNativeModbusConfiguration(ModbusOptions ^d);
void toNativeMegaviConfiguration(MegaviOptions ^d);
void toNativeDacsConfiguration(DacsOptions ^d);
void toNativeGsmMessage(GsmMessage ^d);
void toNativeDataPublication(DataPublication ^d);
void toNativeCellularData(CellularData ^d);
void toNativeEthernet(Ethernet ^d);

bool Bat2NewDataRW::Save(Connection::Interface::Domain::Configuration ^config)
{
   toNative(config);
   ConfigSave();
   return device->Socket->ConfigRemoteSave() == YES;
}

void Bat2NewDataRW::toNative(Connection::Interface::Domain::Configuration ^config)
{	
   WeightUnitsSet(WEIGHT_UNITS_G);
   //WeightUnitsSet((byte)config->WeightUnits->Units);
   toNativeVersion(config->VersionInfo);
	toNativeDevice(config->DeviceInfo);
	toNativeCountry(config->Country);
	toNativeDisplayConfiguration(config->DisplayConfiguration);
	toNativeWeighingConfiguration(config->WeighingConfiguration);
	toNativeRs485Configuration(config->Rs485Options);
	toNativeModbusConfiguration(config->ModbusOptions);
	toNativeMegaviConfiguration(config->MegaviOptions);	
	toNativeDacsConfiguration(config->DacsOptions);	
	toNativeGsmMessage(config->GsmMessage);
	toNativeDataPublication(config->DataPublication);
	toNativeCellularData(config->CellularData);
	toNativeEthernet(config->Ethernet);
   toNativeWeightUnits(config->WeightUnits);	
}

void toNativeVersion(VersionInfo ^d)
{
   if (d == nullptr){
      throw gcnew ArgumentNullException("d", "VersionInfo can't be null!");
   }

   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TDeviceVersion) == storage->Items[CONFIG_VERSION].Size);
   TDeviceVersion *version = ((TDeviceVersion*)storage->Items[CONFIG_VERSION].Data);

   version->Class = d->Class;
   version->Modification = d->Modification;
   version->Software = DeviceVersionSet(d->SoftwareMajor, d->SoftwareMinor, d->SoftwareBuild);
   version->Hardware = DeviceVersionSet(d->HardwareMajor, d->HardwareMinor, d->HardwareBuild);

	/*array<Byte> ^serialNumber = BitConverter::GetBytes((unsigned int)d->SerialNumber);
	Array::Reverse(serialNumber);

	version->SerialNumber = BitConverter::ToUInt32(serialNumber, 0);*/
}

void toNativeDevice(DeviceInfo ^d)
{
   if (d == nullptr){
      throw gcnew ArgumentNullException("d", "DeviceInfo can't be null!");
   }
   if (String::IsNullOrEmpty(d->Name)){
      throw gcnew ArgumentOutOfRangeException("d", "DeviceInfo Name have to be non-zero length string!");
   }
   if (d->Password == nullptr){
      d->Password = "";
   }

   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TBat2Device) == storage->Items[CONFIG_DEVICE].Size);
   TBat2Device *device = ((TBat2Device*)storage->Items[CONFIG_DEVICE].Data);

   IntPtr str = Marshal::StringToHGlobalAnsi(d->Name);
   strncpy(device->Name, (char*)str.ToPointer(), DEVICE_NAME_SIZE);
   Marshal::FreeHGlobal(str);
   str = Marshal::StringToHGlobalAnsi(d->Password);
   strncpy(device->Password, (char*)str.ToPointer(), PASSWORD_SIZE);
}

void toNativeCountry(Connection::Interface::Domain::Country ^d)
{
   if (d == nullptr){
      throw gcnew ArgumentNullException("d", "Country can't be null!");
   }
   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TCountry) == storage->Items[CONFIG_COUNTRY].Size);
   TCountry *country = ((TCountry*)storage->Items[CONFIG_COUNTRY].Data);

   country->Country = (byte)d->CountryCode;
   country->Language = (byte)d->Language;

   country->Locale.CodePage = (byte)d->CodePage;
   country->Locale.DateFormat = (byte)d->DateFormat;
   country->Locale.DateSeparator1 = (char)d->DateSeparator1;
   country->Locale.DateSeparator2 = (char)d->DateSeparator2;
   country->Locale.DaylightSavingType = (byte)d->DaylightSavingType;
   country->Locale.TimeFormat = (char)d->TimeFormat;
   country->Locale.TimeSeparator = (char)d->TimeSeparator;

}

void toNativeWeightUnits(Bat2Library::Connection::Interface::Domain::WeightUnits ^d)
{
   if (d == nullptr){
      throw gcnew ArgumentNullException("d", "WeightUnits can't be null!");
   }
   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TWeightUnits) == storage->Items[CONFIG_WEIGHT_UNITS].Size);
   TWeightUnits *weightUnits = ((TWeightUnits*)storage->Items[CONFIG_WEIGHT_UNITS].Data);

   weightUnits->Decimals = d->Decimals;
   weightUnits->Division = d->Division;
   weightUnits->DivisionMax = d->DivisionMax;
   weightUnits->Range = d->Range;
   weightUnits->Units = (byte)d->Units;   
}


void toNativeDisplayConfiguration(DisplayConfiguration ^d)
{
   if (d == nullptr){
      throw gcnew ArgumentNullException("d", "DisplayConfiguration can't be null!");
   }
   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TDisplayConfiguration) == storage->Items[CONFIG_DISPLAY_CONFIGURATION].Size);
   TDisplayConfiguration *displayConfiguration = ((TDisplayConfiguration*)storage->Items[CONFIG_DISPLAY_CONFIGURATION].Data);

   displayConfiguration->Contrast = d->Contrast;
   displayConfiguration->Mode = (byte)d->Mode;
   displayConfiguration->Backlight.Duration = d->BacklightDuration;
   displayConfiguration->Backlight.Intensity = d->BacklightIntensity;
   displayConfiguration->Backlight.Mode = (byte)d->BacklightMode;
   displayConfiguration->PowerSave = (d->SavePower) ? YES : NO;
}

void toNativeWeighingConfiguration(WeighingConfiguration ^d)
{
   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TWeighingConfiguration) == storage->Items[CONFIG_WEIGHING_CONFIGURATION].Size);
   TWeighingConfiguration *weighingConfiguration = ((TWeighingConfiguration*)storage->Items[CONFIG_WEIGHING_CONFIGURATION].Data);
   toNativeWeighingConfiguration(weighingConfiguration, d);
}



void toNativeRs485Configuration(List<Connection::Interface::Domain::Rs485Options^> ^d)
{
   if (d == nullptr){
      throw gcnew ArgumentNullException("d", "Rs485Options can't be null!");
   }
   if (d->Count != RS485_INTERFACE_COUNT){
      throw gcnew ArgumentOutOfRangeException(String::Format("Rs485Options have to contain exactly {0} options!", RS485_INTERFACE_COUNT));
   }


   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TRs485Options)*RS485_INTERFACE_COUNT == storage->Items[CONFIG_RS485_OPTIONS].Size);
   TRs485Options *rsOpt = (TRs485Options*)storage->Items[CONFIG_RS485_OPTIONS].Data;

   Connection::Interface::Domain::Rs485Options ^c;
   for (int i = 0; i < RS485_INTERFACE_COUNT; i++)
   {
      if (i >= d->Count){
         break;
      }
      c = d[i];
      rsOpt[i].Enabled = c->Enabled ? YES : NO;
	  rsOpt[i].Mode = (byte)c->Mode;
   }
}

void toNativeModbusConfiguration(ModbusOptions ^d)
{
   if (d == nullptr){
      throw gcnew ArgumentNullException("d", "ModbusOptions can't be null!");
   }
   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TModbusModuleOptions) == storage->Items[CONFIG_MODBUS_OPTIONS].Size);
   TModbusModuleOptions *modbusOptions = (TModbusModuleOptions*)storage->Items[CONFIG_MODBUS_OPTIONS].Data;

   modbusOptions->Address = d->Address;
   modbusOptions->BaudRate = d->BaudRate;
   modbusOptions->Mode = (byte)d->Mode;
   modbusOptions->Parity = (byte)d->Parity;
}

void toNativeMegaviConfiguration(MegaviOptions ^d)
{
   if (d == nullptr){
      throw gcnew ArgumentNullException("d", "MegaviOptions can't be null!");
   }
   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TMegaviModuleOptions) == storage->Items[CONFIG_MEGAVI_OPTIONS].Size);
   TMegaviModuleOptions *megaviOptions = (TMegaviModuleOptions*)storage->Items[CONFIG_MEGAVI_OPTIONS].Data;

   megaviOptions->ID = ((d->Address << 4)&0xF0 ) | (d->Code & 0xF);
}

void toNativeDacsConfiguration(DacsOptions ^d)
{
   if (d == nullptr){
      throw gcnew ArgumentNullException("d", "DacsOptions can't be null!");
   }
   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TDacsModuleOptions) == storage->Items[CONFIG_DACS_OPTIONS].Size);
   TDacsModuleOptions *dacsOptions = (TDacsModuleOptions*)storage->Items[CONFIG_DACS_OPTIONS].Data;

   dacsOptions->Address = d->Address;
   dacsOptions->Version = (byte)d->Version;
}


void toNativeGsmMessage(GsmMessage ^d)
{
	if (d == nullptr){
		throw gcnew ArgumentNullException("d", "GsmMessage can't be null!");
	}

	UDateTime dt;

	const UStorageDescriptor *storage = &ConfigDescriptor;
	assert(sizeof(TGsmMessage) == storage->Items[CONFIG_GSM_MESSAGE].Size);
	TGsmMessage *gsmMessage = ((TGsmMessage*)storage->Items[CONFIG_GSM_MESSAGE].Data);	

	gsmMessage->Commands.CheckPhoneNumber = d->CommandsCheckPhoneNumber ? YES : NO;
	gsmMessage->Commands.Enabled = d->CommandsEnabled ? YES : NO;
	gsmMessage->Commands.Expiration = d->CommandsExpiration;

	gsmMessage->Events.EventMask = (byte)d->EventMask;

	gsmMessage->PowerOptions.Mode = (byte)d->Mode;
	gsmMessage->PowerOptions.SwitchOnDuration = d->SwitchOnDuration;
	gsmMessage->PowerOptions.SwitchOnPeriod = d->SwitchOnPeriod;
	gsmMessage->PowerOptions.TimesCount = d->SwitchOnTimes->Count;

	for (int i = 0; i < gsmMessage->PowerOptions.TimesCount; i++){
		//from
		dt.Time.Hour = d->SwitchOnTimes[i]->From.Hour;
		dt.Time.Min = d->SwitchOnTimes[i]->From.Minute;
		dt.Time.Sec = d->SwitchOnTimes[i]->From.Second;
		gsmMessage->PowerOptions.Times[i].From = uTimeGauge(&dt.Time);

		//to
		dt.Time.Hour = d->SwitchOnTimes[i]->To.Hour;
		dt.Time.Min = d->SwitchOnTimes[i]->To.Minute;
		dt.Time.Sec = d->SwitchOnTimes[i]->To.Second;
		gsmMessage->PowerOptions.Times[i].To = uTimeGauge(&dt.Time);
	}
}

void toNativeDataPublication(DataPublication ^d)
{
   if (d == nullptr){
      throw gcnew ArgumentNullException("d", "DataPublication can't be null!");
   }
   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TDataPublication) == storage->Items[CONFIG_DATA_PUBLICATION].Size);
   TDataPublication *dataPublication = (TDataPublication*)storage->Items[CONFIG_DATA_PUBLICATION].Data;

   dataPublication->AcceleratedPeriod = d->AcceleratedPeriod;
   dataPublication->AccelerateFromDay = d->AccelerateFromDay;
   dataPublication->Interface = (byte)d->Interface;

   IntPtr str = Marshal::StringToHGlobalAnsi(d->Password);
   strncpy(dataPublication->Password, (char*)str.ToPointer(),DATA_PUBLICATION_PASSWORD_SIZE);
   Marshal::FreeHGlobal(str);

   dataPublication->Period = d->Period;

   UTime time;
   time.Hour = d->SendAt.Hour;
   time.Min = d->SendAt.Minute;
   time.Sec = d->SendAt.Second;
   
   dataPublication->SendAt = uTimeGauge(&time);
   dataPublication->StartFromDay = d->StartFromDay;
   
   str = Marshal::StringToHGlobalAnsi(d->Url);
   strncpy(dataPublication->Url, (char*)str.ToPointer(),DATA_PUBLICATION_URL_SIZE);
   Marshal::FreeHGlobal(str);

   str = Marshal::StringToHGlobalAnsi(d->Username);
   strncpy(dataPublication->Username, (char*)str.ToPointer(),DATA_PUBLICATION_USERNAME_SIZE);
   Marshal::FreeHGlobal(str);
}

void toNativeCellularData(CellularData ^d)
{
	if (d == nullptr){
		throw gcnew ArgumentNullException("d", "CellularData can't be null!");
	}
	const UStorageDescriptor *storage = &ConfigDescriptor;
	assert(sizeof(TCellularData) == storage->Items[CONFIG_CELLULAR_DATA].Size);
	TCellularData *cellularData = (TCellularData*)storage->Items[CONFIG_CELLULAR_DATA].Data;	

	IntPtr str = Marshal::StringToHGlobalAnsi(d->Apn);
	strncpy(cellularData->Apn, (char*)str.ToPointer(),CELLULAR_DATA_APN_SIZE);
	Marshal::FreeHGlobal(str);

	str = Marshal::StringToHGlobalAnsi(d->Password);
	strncpy(cellularData->Password, (char*)str.ToPointer(),CELLULAR_DATA_PASSWORD_SIZE);
	Marshal::FreeHGlobal(str);

	str = Marshal::StringToHGlobalAnsi(d->Username);
	strncpy(cellularData->Username, (char*)str.ToPointer(),CELLULAR_DATA_USERNAME_SIZE);
	Marshal::FreeHGlobal(str);

}

void toNativeEthernet(Ethernet ^d)
{
	if (d == nullptr){
		throw gcnew ArgumentNullException("d", "Ethernet can't be null!");
	}
	const UStorageDescriptor *storage = &ConfigDescriptor;
	assert(sizeof(TEthernet) == storage->Items[CONFIG_ETHERNET].Size);	
	TEthernet *ethernet = (TEthernet*)storage->Items[CONFIG_ETHERNET].Data;
	
	ethernet->Dhcp = d->Dhcp ? YES : NO;
	array<Byte> ^adress = d->Gateway->GetAddressBytes();
	Array::Reverse(adress);
	ethernet->Gateway = BitConverter::ToUInt32(adress, 0);

	adress = d->Ip->GetAddressBytes();
	Array::Reverse(adress);
	ethernet->Ip = BitConverter::ToUInt32(adress, 0);

	adress = d->PrimaryDns->GetAddressBytes();
	Array::Reverse(adress);
	ethernet->PrimaryDns = BitConverter::ToUInt32(adress, 0);

	adress = d->SubnetMask->GetAddressBytes();
	Array::Reverse(adress);
	ethernet->SubnetMask = BitConverter::ToUInt32(adress, 0);
}