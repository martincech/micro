#pragma once
#include "Bat2INativeSocket.h"

namespace Bat2Library
{
   namespace Connection
   {
      namespace Manager
      {
         namespace Native{
            using namespace System;

            private ref class Bat2OldINativeSocket abstract : Bat2INativeSocket
            {
            public:
               property array<byte> ^Bat2Memory;
               
            };
         }
      }
   }
}



