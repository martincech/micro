#pragma once
#include "Bat2INativeSocket.h"

namespace Bat2Library
{
   namespace Connection
   {
      namespace Manager
      {
         namespace Native{
            using namespace System;
            using namespace Bat2Library::Connection::Interface::Domain::Old;
            using namespace ::Native;

            generic<typename R, typename S, typename T, typename U>
               where R: BaseVersionInfo
               where S: BaseConfiguration<R>
               where T: Bat2INativeSocket
               where U : BaseBat2DeviceData<R, S>
            private ref class Bat2Device abstract
            {
            private:
               T socket;
               U deviceData;

            protected:
            Bat2Device(T socket, U deviceData)
            {
               if (socket == nullptr)
               {
                  throw gcnew ArgumentNullException("socket", "Socket must be non-null!");
               }
               if (deviceData == nullptr)
               {
                  throw gcnew ArgumentNullException("deviceData", "deviceData must be non-null!");
               }
               this->socket = socket;
				   this->deviceData = deviceData;
            }

            ~Bat2Device()
            {
               this->!Bat2Device();
            }

            !Bat2Device()
            {
               if (socket != nullptr){
                  delete(socket);
                  socket = T();
               }
            }

            public:

               property T Socket{ T get(){ return socket; } }
               property U DeviceData
               {
                  virtual U get(){ return deviceData; }
			         internal:
                     void set(U value){ deviceData = value; }
               }
            };
         }
      }
   }
}