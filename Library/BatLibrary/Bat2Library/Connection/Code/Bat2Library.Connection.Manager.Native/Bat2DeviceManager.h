#pragma once
#include "Bat2INativeSocket.h"
#include "Bat2Device.h"
#include "Bat2DataRW.h"

namespace Bat2Library
{
   namespace Connection
   {
      namespace Manager
      {
         namespace Native{
            using namespace System;
            using namespace System::Collections::Generic;
            using namespace System::Collections::ObjectModel;
            using namespace System::Runtime::InteropServices;
            using namespace Bat2Library::Connection::Interface::IO;

            generic<typename S, typename I, typename T, class D, typename R, typename W, typename C>
               where S: Bat2INativeSocket
               where I : BaseVersionInfo
               where T : BaseConfiguration<I>
               where D: BaseBat2DeviceData<I, T>
               where R : IBat2DataReader<I, T>
               where W : IBat2DataWriter<I, T>
               where C : IBat2ActionCommander
            public ref class Bat2DeviceManager abstract : IBat2DeviceManager<I, T, D, R, W, C>
            {
            protected:
               Bat2DeviceManager()
               {
                  Devices = gcnew List<Bat2Device<I, T, S, D>^>();
               }

               // Create new instance of Bat2DataRwBase subtype object
               virtual Bat2DataRW<I, T, S, D> ^CreateDataRwObject(Bat2Device<I, T, S, D>^ dev) abstract;

            public:

               ~Bat2DeviceManager(){ this->!Bat2DeviceManager(); }
               !Bat2DeviceManager(){}

               virtual event EventHandler<D> ^DeviceConnected;
               virtual event EventHandler<D> ^DeviceDisconnected;

               property IEnumerable<D> ^ConnectedDevices
               {
                  virtual IEnumerable<D> ^get()
                  {
                     List<D> ^devs = gcnew List<D>();
                     for each (Bat2Device<I, T, S, D> ^dev in Devices)
                     {
                        D data = dev->DeviceData;
                        if (data == nullptr){ continue; }

                        if (data->IsValid){
                           devs->Add(data);
                        }
                     }
                     return devs;
                  }
               }

               virtual R GetDataReader(D forDevice)
               {
                  return static_cast<R>(getBat2Connection(forDevice));
               }

               virtual W GetDataWriter(D forDevice)
               {
                  return static_cast<W>(getBat2Connection(forDevice));
               }

               virtual C GetActionCommander(D forDevice)
               {
                  return static_cast<C>(getBat2Connection(forDevice));
               }

            internal:

               // List of connected devices
               property List<Bat2Device<I, T, S, D>^> ^Devices
               {
                  List<Bat2Device<I, T, S, D>^>^get(){ return devices; }
               private:
                  void set(List<Bat2Device<I, T, S, D>^> ^value){ devices = value; }
               }

            private:
               List<Bat2Device<I, T, S, D>^> ^devices;

               virtual Bat2DataRW<I, T, S, D> ^getBat2Connection(D forDevice) sealed
               {
                  for each (Bat2Device<I, T, S, D>^ dev in Devices)
                  {
                     D data = dev->DeviceData;
                     if (data == nullptr){ continue; }

                     if (dev->DeviceData == forDevice && 
                        data->IsValid)
                     {
                        return CreateDataRwObject(dev);
                     }
                  }
                  return nullptr;
               }
            };
         }
      }
   }
}