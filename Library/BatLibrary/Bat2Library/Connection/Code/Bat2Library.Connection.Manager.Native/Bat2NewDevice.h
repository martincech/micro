#pragma once
#include "Bat2NewINativeSocket.h"
#include "Bat2Device.h"

namespace Bat2Library
{
   namespace Connection
   {
      namespace Manager
      {
         namespace Native{
            using namespace Bat2Library::Connection::Interface::Domain;

            private ref class Bat2NewDevice : public Bat2Device<VersionInfo ^,Configuration ^, Bat2NewINativeSocket^, Bat2DeviceData^>
            {
            protected:
               Bat2NewDevice(Bat2NewINativeSocket ^socket, Bat2DeviceData ^deviceData)
                  : Bat2Device(socket, deviceData)
               {
               }
            };
         }
      }
   }
}