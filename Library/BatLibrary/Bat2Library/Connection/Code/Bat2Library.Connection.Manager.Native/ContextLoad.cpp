#include "Bat2NewDataRW.h"
using namespace System;
using namespace Bat2Library::Connection::Manager::Native;
using namespace Bat2Library::Connection::Interface;
using namespace Bat2Library::Connection::Interface::Domain;
#include <assert.h>

#undef _MANAGED
#include "Config\Context.h"
#include "Config\ConfigDef.h"
#include "Menu\Menu.h"
#include "Message\GsmMessageDef.h"
#include "Calibration\CalibrationDef.h"
#include "Weighing\WeighingConfigurationDef.h"
#include "Weight\Weight.h"
#include "CurvesPlan.h"

Context^ LoadContextData();
Domain::MenuContext ^fromNativeMenuContext();
CalibrationContext ^fromNativeCalibrationContext();
WeighingContext ^fromNativeWeighingContext();
PlatformCalibration ^fromNativeCalibration();

bool Bat2NewDataRW::Load(Context ^%Context)
{
   if (device->Socket->ContextRemoteLoad() == NO){
      return false;
   }
   ContextLoad();
   Context = LoadContextData();
   return true;
}

Context^ LoadContextData()
{	
	Context^ context = gcnew Context();
	context->MenuContext = fromNativeMenuContext();
	context->CalibrationContext = fromNativeCalibrationContext();
	context->WeighingContext = fromNativeWeighingContext();
   context->PlatformCalibration = fromNativeCalibration();
	return context;
}


Domain::MenuContext ^fromNativeMenuContext()
{
   const UStorageDescriptor *storage = &ContextDescriptor;
   assert(sizeof(TMenuContext) == storage->Items[CONTEXT_MENU].Size);

   TMenuContext *menuContext = (TMenuContext *)storage->Items[CONTEXT_MENU].Data;
   Domain::MenuContext ^d = gcnew Domain::MenuContext();
   d->SwitchedOff = menuContext->SwitchedOff == YES;
   return d;
}

CalibrationContext ^fromNativeCalibrationContext()
{
   UDateTime dt;
   const UStorageDescriptor *storage = &ContextDescriptor;
   assert(sizeof(TCalibration) == storage->Items[CONTEXT_CALIBRATION_COEFFICIENTS].Size);

   TCalibration *calibrationContext = (TCalibration*)storage->Items[CONTEXT_CALIBRATION_COEFFICIENTS].Data;
   CalibrationContext ^d = gcnew CalibrationContext();
   d->Inversion = calibrationContext->Inversion == YES;
   uDateTime(&dt, calibrationContext->Time);
   d->Time = DateTime(uYearGet(dt.Date.Year), dt.Date.Month, dt.Date.Day, dt.Time.Hour, dt.Time.Min, dt.Time.Sec);
   d->RawPoints = gcnew List<Int32>();
   d->WeightPoints = gcnew List<Int32>();
   for (int i = 0; i < calibrationContext->Points; i++)
   {
      ((List<Int32>^)d->WeightPoints)->Add(calibrationContext->Weight[i]);
      ((List<Int32>^)d->RawPoints)->Add(calibrationContext->Raw[i]);
   }
   return d;
}

PlatformCalibration ^fromNativeCalibration()
{
   const UStorageDescriptor *storage = &ContextDescriptor;
   assert(sizeof(TPlatformCalibration) == storage->Items[CONTEXT_CALIBRATION].Size);
   TPlatformCalibration *calibration = ((TPlatformCalibration*)storage->Items[CONTEXT_CALIBRATION].Data);

   PlatformCalibration ^d = gcnew PlatformCalibration();
   d->Points = gcnew List<Int32>();
   d->Delay = calibration->Delay;
   d->Duration = calibration->Duration;
   for (int i = 0; i < calibration->Points; i++)
   {
      d->Points->Add(WeightNumber(calibration->Weight[i]));
   }
   return d;
}

//toNativePlatformCalibration(config->PlatformCalibration);
//void toNativePlatformCalibration(PlatformCalibration ^d);
//void toNativePlatformCalibration(PlatformCalibration ^d)
//{
//   if (d == nullptr){
//      throw gcnew ArgumentNullException("d", "PlatformCalibration can't be null!");
//   }
//   const UStorageDescriptor *storage = &ConfigDescriptor;
//   assert(sizeof(TPlatformCalibration) == storage->Items[CONFIG_CALIBRATION].Size);
//   TPlatformCalibration *calibration = ((TPlatformCalibration*)storage->Items[CONFIG_CALIBRATION].Data);
//
//   calibration->Delay = d->Delay;
//   calibration->Duration = d->Duration;
//   calibration->Points = d->Points->Count > PLATFORM_CALIBRATION_POINTS_MAX ? PLATFORM_CALIBRATION_POINTS_MAX : d->Points->Count;
//   for (int i = 0; i < calibration->Points; i++)
//   {
//      calibration->Weight[i] = WeightGauge(d->Points[i]);
//   }
//}

WeighingContext ^fromNativeWeighingContext()
{
   UDateTime dt;
   const UStorageDescriptor *storage = &ContextDescriptor;
   assert(sizeof(TWeighingContext) == storage->Items[CONTEXT_WEIGHING].Size);

   TWeighingContext *weighingContext = (TWeighingContext*)storage->Items[CONTEXT_WEIGHING].Data;
   WeighingContext ^d = gcnew WeighingContext();
   d->Status = (WeighingStatusE)weighingContext->Status;
   d->LastStatus = (WeighingStatusE)weighingContext->LastStatus;
   d->DayActive = weighingContext->DayActive;
   d->Id = weighingContext->Id;
   d->Day = weighingContext->Day;
   d->TargetWeight = weighingContext->TargetWeight;
   d->TargetWeightFemale = weighingContext->TargetWeightFemale;
   d->Correction = weighingContext->Correction;
   try{
      uClock(&dt, weighingContext->StartAt);
	  d->StartAt = DateTime(uYearGet(1), 1, 1, dt.Time.Hour, dt.Time.Min, dt.Time.Sec);
   } catch(Exception ^ex){
      d->StartAt = DateTime(1, 1, 1);
   }
   try{
      uClock(&dt, weighingContext->DayZero);
	  d->DayZero = DateTime(uYearGet(1), 1, 1, dt.Time.Hour, dt.Time.Min, dt.Time.Sec);
   } catch (Exception ^ex){
      d->DayZero = DateTime(1, 1, 1);
   }
   try{
      uClock(&dt, weighingContext->DayCloseAt);
	  d->DayCloseAt = DateTime(uYearGet(1), 1, 1, dt.Time.Hour, dt.Time.Min, dt.Time.Sec);
   } catch (Exception ^ex){
      d->DayCloseAt = DateTime(1, 1, 1);
   }
   try{
      uTime(&dt.Time, weighingContext->DayDuration);
      d->DayDuration = TimeSpan(dt.Time.Hour, dt.Time.Min, dt.Time.Sec);
   } catch (Exception ^ex){
      d->DayDuration = TimeSpan(0, 0, 0);
   }

   try{
      d->CorrectionCurve = fromNativeCorrectionCurve(&weighingContext->CorrectionCurve);
   }
   catch (Exception ^ex){
      d->CorrectionCurve = nullptr;
   }
   try{
      d->GrowthCurveMale = fromNativeGrowthCurve(&weighingContext->GrowthCurveMale);
   }
   catch (Exception ^ex){
      d->GrowthCurveMale = nullptr;
   }
   try{
      d->GrowthCurveFemale = fromNativeGrowthCurve(&weighingContext->GrowthCurveFemale);
      }
   catch (Exception ^ex){
      d->GrowthCurveFemale = nullptr;
   }
   try{
      d->WeighingPlan = fromNativeWeighingPlan(&weighingContext->WeighingPlan);
   }
   catch (Exception ^ex){
      d->WeighingPlan = nullptr;
   }

   return d;
}
