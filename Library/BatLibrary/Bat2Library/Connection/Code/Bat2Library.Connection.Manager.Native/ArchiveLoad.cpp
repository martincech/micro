#include "Bat2NewDataRW.h"
#include <assert.h>
#undef _MANAGED
#include "Archive\Archive.h"
#include "Weight\Weight.h"

using namespace System;
using namespace System::Collections::Generic;
using namespace Bat2Library::Connection::Manager::Native;
using namespace Bat2Library::Connection::Interface::Domain;
static void UpdateItem(ArchiveItem ^%newItem, TArchiveItem item, ArchiveItem ^marker, DateTime timeStamp)
{
   // set properties
   newItem->Day = marker->Day;
   newItem->TargetWeight = WeightNumber(marker->TargetWeight);
   newItem->TargetWeightFemale = WeightNumber(marker->TargetWeightFemale);
   newItem->ZoneIdentifier = marker->ZoneIdentifier;
   newItem->Average = WeightNumber(item.Data.Average);
   newItem->Count = item.Data.Count;
   newItem->Gain = WeightNumber(item.Data.Gain);
   newItem->HourFrom = item.Data.HourFrom;
   newItem->HourTo = item.Data.HourTo;
   newItem->Sex = (SexE)item.Data.Sex;
   newItem->Sigma = WeightNumber(item.Data.Sigma);
   newItem->StatisticCalculate = item.Data.StatisticCalculate;
   newItem->Uniformity = item.Data.Uniformity;
   newItem->Timestamp = timeStamp;
   // histogram
   newItem->Histogram = gcnew List<byte>();
   for (int j = 0; j < HISTOGRAM_SLOTS; j++)
   {
      ((List<byte> ^)newItem->Histogram)->Add(item.Data.Histogram.Slot[j]);
   }
}
bool Bat2NewDataRW::Load(List<ArchiveItem^> ^%archiveList)
{
   if ((device->Socket)->ArchiveRemoteLoad() == NO){
      return false;
   }
   WeightUnitsSet(WEIGHT_UNITS_G);

   TArchive archive;
   TArchiveIndex itemCount;
   TArchiveItem item;
   ArchiveItem ^marker;
   UDateTime dt;

   ArchiveInit();
   if (ArchiveOpen(&archive) == NO){
      return false;
   }
   itemCount = ArchiveCount();
   for (TArchiveIndex i = 0; i < itemCount; i++)
   {
      if (ArchiveGet(&archive, i, &item) == NO){ break; }
      if (ArchiveIsMarker(&item)){
         marker = gcnew ArchiveItem();
         marker->TargetWeight = item.Marker.TargetWeight;
         marker->TargetWeightFemale = item.Marker.TargetWeightFemale;
         marker->Day = item.Marker.Day;
         marker->ZoneIdentifier = item.Marker.Zone;
         continue;
      }
      

      ArchiveItem ^newItem;
      bool justUpdate = false;
      uClock(&dt, item.Data.Timestamp);
      DateTime timeStamp = DateTime(uYearGet(dt.Date.Year), dt.Date.Month, dt.Date.Day, dt.Time.Hour, dt.Time.Min, dt.Time.Sec);
      for each (ArchiveItem ^aItem in archiveList)
      {
         if (aItem->Day == marker->Day &&
            aItem->HourFrom == item.Data.HourFrom &&
            aItem->HourTo == item.Data.HourTo &&
            aItem->Sex == (SexE)item.Data.Sex &&
            aItem->Timestamp.ToBinary() == timeStamp.ToBinary()){
               newItem = aItem;
               justUpdate = true;
               break;
         }
      }
      
      if (!justUpdate){
         newItem = gcnew ArchiveItem();
         archiveList->Add(newItem);
      }
      UpdateItem(newItem, item, marker, timeStamp);
   }

   ArchiveClose(&archive);
   return true;
}

