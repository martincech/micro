#pragma once
#include "Bat2OldINativeSocket.h"
#include "IStreamSocket.h"

namespace Bat2Library
{
	namespace Connection
	{
		namespace Manager
		{
			namespace Native{
				using namespace System;
            using namespace Usb::FTDI;

            private ref class Bat2OldUsbSocket : Bat2OldINativeSocket, IStreamSocket
				{
				public:		
               Bat2OldUsbSocket(Stream ^socket);
               virtual property Stream ^Socket;

               Bat2OldUsbSocket(String ^devName)
               {
                  FTDIDeviceLoader ^ftdiLoader = gcnew FTDIDeviceLoader();
                  FTDIDevice ^ftdiDevice = ftdiLoader->GetDevice(devName);
                  if (ftdiDevice == nullptr){
                     throw gcnew ArgumentException(String::Format("Device with name {0} not found on system!", devName));
                  }
                  Socket = ftdiDevice->Open();
               }

               virtual TYesNo ConfigRemoteLoad() override;
               virtual TYesNo ConfigRemoteSave() override;
               virtual TYesNo ArchiveRemoteLoad() override;

               virtual TYesNo ActionRemoteWeighingSchedulerStart(void) override;
               virtual TYesNo ActionRemoteWeighingSchedulerStartAt(UClockGauge DateTime) override;
               virtual TYesNo ActionRemoteWeighingSchedulerStop(void) override;
               virtual TYesNo ActionRemoteWeighingSchedulerSuspend(void) override;
               virtual TYesNo ActionRemoteWeighingSchedulerRelease(void) override;
               virtual TYesNo ActionRemoteTimeSet(UDateTimeGauge DateTime) override;
               virtual TYesNo ActionRemoteTimeGet(UDateTimeGauge *DateTime) override;
               virtual TYesNo ActionRemoteReboot() override;               
            };
			}
		}
	}
}