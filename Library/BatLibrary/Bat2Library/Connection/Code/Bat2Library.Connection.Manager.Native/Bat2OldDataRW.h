#pragma once
#include "Bat2OldDevice.h"
#include "Bat2DataRW.h"
#include "Bat2OldInativeSocket.h"

namespace Bat2Library
{
	namespace Connection
	{
		namespace Manager
		{
			namespace Native{
				using namespace Bat2Library::Connection::Interface::IO;
				using namespace Bat2Library::Connection::Interface::Domain::Old;
				using namespace System::Collections::Generic;
				using namespace System::Reflection;
				using namespace Bat2Library;
				using namespace System::Runtime::InteropServices;
				private ref class Bat2OldDataRW :
               Bat2DataRW<BaseVersionInfo ^, OldConfiguration ^, Bat2OldINativeSocket ^, OldBat2DeviceData ^>,
					IBat2OldDataReader,
					IBat2OldDataWriter
				{
				public:
               Bat2OldDataRW(Bat2OldDevice ^device) : Bat2DataRW(device){};
					// Load methods
               virtual OldConfiguration ^LoadConfig() override sealed{ return LoadConfig(true); }
               virtual IEnumerable<OldArchiveDailyInfo^> ^LoadArchive() sealed{ return LoadArchive(true); }

					// Write methods
               virtual bool SaveConfig(OldConfiguration ^config) override sealed;

				internal:
					virtual OldBat2DeviceData ^LoadAll() sealed;

				private:

					 //Actual load functions
					bool Load(OldConfiguration ^%config);				
					bool Load(List<OldArchiveDailyInfo^> ^%archive);

					// Actual save functions
					bool Save(OldConfiguration ^config);

               virtual OldConfiguration ^LoadConfig(bool withSetup) sealed;
               virtual IEnumerable<OldArchiveDailyInfo^> ^LoadArchive(bool withSetup) sealed;

				};
			}
		}
	}
}
