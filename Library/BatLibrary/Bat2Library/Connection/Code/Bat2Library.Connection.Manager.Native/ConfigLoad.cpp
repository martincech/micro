#include "Bat2NewDataRW.h"
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Net;
using namespace Bat2Library::Connection::Manager::Native;
using namespace Bat2Library::Connection::Interface::Domain;
#include <assert.h>

#undef _MANAGED
#include "Config\Config.h"
#include "Config\ConfigDef.h"
#include "Display\DisplayConfigurationDef.h"
#include "Weighing\WeighingConfigurationDef.h"
#include "Message\GsmMessageDef.h"
#include "Rs485\Rs485Config.h"
#include "Modbus\ModbusConfigDef.h"
#include "Megavi\MegaviConfigDef.h"
#include "SmsGate\SmsGateConfigDef.h"
#include "Dacs\DacsConfigDef.h"
#include "CurvesPlan.h"
#include "Communication\CommunicationDef.h"
#include "Weight\Weight.h"

bool fromNative(Bat2Library::Connection::Interface::Domain::Configuration ^%config);
VersionInfo ^fromNativeDeviceVersion();
DeviceInfo ^fromNativeDevice();
Bat2Library::Connection::Interface::Domain::Country ^fromNativeCountry();
Bat2Library::Connection::Interface::Domain::WeightUnits ^fromNativeWeightUnits();
DisplayConfiguration ^fromNativeDisplayConfiguration();
WeighingConfiguration ^fromNativeWeighingConfiguration();
List<Bat2Library::Connection::Interface::Domain::Rs485Options^> ^fromNativeRs485Configuration();
ModbusOptions ^fromNativeModbusConfiguration();
MegaviOptions ^fromNativeMegaviConfiguration();
DacsOptions ^fromNativeDacsConfiguration();

GsmMessage ^fromNativeGsmMessage();

DataPublication ^fromNativeDataPublication();
CellularData ^fromNativeCellularData();
Ethernet ^fromNativeEthernet();


#ifdef DeviceVersion
#undef DeviceVersion
#endif

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//  Load CONFIG object from native
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

bool Bat2NewDataRW::Load(Bat2Library::Connection::Interface::Domain::Configuration ^%config)
{
	
   if (device->Socket->ConfigRemoteLoad() == NO){
      return false;
   }
   ConfigLoad();
   return fromNative(config);
}

bool Bat2NewDataRW::fromNative(Bat2Library::Connection::Interface::Domain::Configuration ^%config)
{  
	config->WeightUnits = fromNativeWeightUnits();
   WeightUnitsSet(WEIGHT_UNITS_G);
   config->VersionInfo = fromNativeDeviceVersion();
   config->DeviceInfo = fromNativeDevice();
   config->Country = fromNativeCountry();
	config->DisplayConfiguration = fromNativeDisplayConfiguration();
	config->WeighingConfiguration = fromNativeWeighingConfiguration();
	config->Rs485Options = fromNativeRs485Configuration();
	config->ModbusOptions = fromNativeModbusConfiguration();
	config->MegaviOptions = fromNativeMegaviConfiguration();
	//config->SmsGateOptions = fromNativeSmsGateConfiguration();
	config->DacsOptions = fromNativeDacsConfiguration();
	//config->EthernetOptions = fromNativeEthernetConfiguration();
	//config->InternetOptions = fromNativeInternetConfiguration();
	config->GsmMessage = fromNativeGsmMessage();
	config->DataPublication = fromNativeDataPublication();
	config->CellularData = fromNativeCellularData();
	config->Ethernet = fromNativeEthernet();
	return true;
}





VersionInfo ^fromNativeDeviceVersion()
{
   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TDeviceVersion) == storage->Items[CONFIG_VERSION].Size);

   TDeviceVersion *deviceVersion = ((TDeviceVersion*)storage->Items[CONFIG_VERSION].Data);

   VersionInfo ^d = gcnew VersionInfo();

   word soft = deviceVersion->Software;
   word hard = deviceVersion->Hardware;

   d->Class = deviceVersion->Class;
   d->Modification = deviceVersion->Modification;
   d->SoftwareMajor = DeviceVersionMajorGet(deviceVersion->Software);
   d->SoftwareMinor = DeviceVersionMinorGet(deviceVersion->Software);
   d->SoftwareBuild = DeviceVersionBuilGet(deviceVersion->Software);
   d->HardwareMajor = DeviceVersionMajorGet(deviceVersion->Hardware);
   d->HardwareMinor = DeviceVersionMinorGet(deviceVersion->Hardware);
   d->HardwareBuild = DeviceVersionBuilGet(deviceVersion->Hardware);
   array<Byte> ^serialNumber = BitConverter::GetBytes((unsigned int)deviceVersion->SerialNumber);
   Array::Reverse(serialNumber);
   d->SerialNumber = BitConverter::ToUInt32(serialNumber, 0);

   return d;
}

DeviceInfo ^fromNativeDevice()
{
   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TBat2Device) == storage->Items[CONFIG_DEVICE].Size);
   TBat2Device *device = ((TBat2Device*)storage->Items[CONFIG_DEVICE].Data);

   DeviceInfo ^d = gcnew DeviceInfo();

   d->Name = gcnew String(device->Name);
   d->Password = gcnew String(device->Password, 0, PASSWORD_SIZE);
   return d;
}

Connection::Interface::Domain::Country ^fromNativeCountry()
{
   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TCountry) == storage->Items[CONFIG_COUNTRY].Size);
   TCountry *country = ((TCountry*)storage->Items[CONFIG_COUNTRY].Data);

   Bat2Library::Connection::Interface::Domain::Country ^d = gcnew Bat2Library::Connection::Interface::Domain::Country();

   d->Language = (LanguageE)country->Language;
   d->CountryCode = (CountryE)country->Country;
   
   d->CodePage = (CodePageE)country->Locale.CodePage;
   d->DateFormat = (DateFormatE)country->Locale.DateFormat;
   d->DateSeparator1 = country->Locale.DateSeparator1;
   d->DateSeparator2 = country->Locale.DateSeparator2;
   d->TimeFormat = (TimeFormatE)country->Locale.TimeFormat;
   d->TimeSeparator = country->Locale.TimeSeparator;
   d->DaylightSavingType = (DaylightSavingE)country->Locale.DaylightSavingType;
   return d;
}

Bat2Library::Connection::Interface::Domain::WeightUnits ^fromNativeWeightUnits()
{
   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TWeightUnits) == storage->Items[CONFIG_WEIGHT_UNITS].Size);
   TWeightUnits *weightUnits = ((TWeightUnits*)storage->Items[CONFIG_WEIGHT_UNITS].Data);

   Bat2Library::Connection::Interface::Domain::WeightUnits ^d = gcnew Bat2Library::Connection::Interface::Domain::WeightUnits();

   d->Range = weightUnits->Range;
   d->Units = (WeightUnitsE)weightUnits->Units;
   d->Decimals = weightUnits->Decimals;
   d->Division = weightUnits->Division;
   d->DivisionMax = weightUnits->DivisionMax;
   return d;
}

DisplayConfiguration ^fromNativeDisplayConfiguration()
{
   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TDisplayConfiguration) == storage->Items[CONFIG_DISPLAY_CONFIGURATION].Size);
   TDisplayConfiguration *displayConfiguration = ((TDisplayConfiguration*)storage->Items[CONFIG_DISPLAY_CONFIGURATION].Data);

   DisplayConfiguration ^d = gcnew DisplayConfiguration();
   d->Contrast = displayConfiguration->Contrast;
   d->Mode = (DisplayModeE)displayConfiguration->Mode;
   d->BacklightDuration = displayConfiguration->Backlight.Duration;
   d->BacklightIntensity = displayConfiguration->Backlight.Intensity;
   d->BacklightMode = (BacklightModeE)displayConfiguration->Backlight.Mode;
   d->SavePower = displayConfiguration->PowerSave == YES;
   return d;
}

WeighingConfiguration ^fromNativeWeighingConfiguration()
{
   UDateTime dt;

   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TWeighingConfiguration) == storage->Items[CONFIG_WEIGHING_CONFIGURATION].Size);
   TWeighingConfiguration *weighingConfiguration = ((TWeighingConfiguration*)storage->Items[CONFIG_WEIGHING_CONFIGURATION].Data);

   return fromNativeWeighingConfiguration(weighingConfiguration);
}

List<Bat2Library::Connection::Interface::Domain::Rs485Options^> ^fromNativeRs485Configuration()
{
   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TRs485Options)*RS485_INTERFACE_COUNT == storage->Items[CONFIG_RS485_OPTIONS].Size);
   TRs485Options *rsOpt = (TRs485Options*)storage->Items[CONFIG_RS485_OPTIONS].Data;

   List<Bat2Library::Connection::Interface::Domain::Rs485Options ^> ^d = gcnew List<Bat2Library::Connection::Interface::Domain::Rs485Options ^>();
   for (int i = 0; i < RS485_INTERFACE_COUNT; i++)
   {
      Bat2Library::Connection::Interface::Domain::Rs485Options ^c = gcnew Bat2Library::Connection::Interface::Domain::Rs485Options();
     
	  TYesNo yess = rsOpt[i].Enabled;
	  byte mode = rsOpt[i].Mode;
	  
	  c->Enabled = rsOpt[i].Enabled == YES;
	  c->Mode = (Rs485ModeE)rsOpt[i].Mode;
      d->Add(c);
   }
   return d;
}


ModbusOptions ^fromNativeModbusConfiguration()
{
   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TModbusModuleOptions) == storage->Items[CONFIG_MODBUS_OPTIONS].Size);
   TModbusModuleOptions *modbusOptions = (TModbusModuleOptions*)storage->Items[CONFIG_MODBUS_OPTIONS].Data;

   ModbusOptions ^d = gcnew ModbusOptions();
   d->Address = modbusOptions->Address;
   d->BaudRate = modbusOptions->BaudRate;
   d->Mode = (ModbusModeE)modbusOptions->Mode;
   d->Parity = (ModbusParityE)modbusOptions->Parity;
   return d;
}

MegaviOptions ^fromNativeMegaviConfiguration()
{
   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TMegaviModuleOptions) == storage->Items[CONFIG_MEGAVI_OPTIONS].Size);
   TMegaviModuleOptions *megaviOptions = (TMegaviModuleOptions*)storage->Items[CONFIG_MEGAVI_OPTIONS].Data;

   MegaviOptions ^d = gcnew MegaviOptions();
   d->Address = (megaviOptions->ID >> 4) & 0xF;
   d->Code = megaviOptions->ID & 0xF;
   return d;
}

DacsOptions ^fromNativeDacsConfiguration()
{
   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TDacsModuleOptions) == storage->Items[CONFIG_DACS_OPTIONS].Size);
   TDacsModuleOptions *dacsOptions = (TDacsModuleOptions*)storage->Items[CONFIG_DACS_OPTIONS].Data;

   DacsOptions ^d = gcnew DacsOptions();
   d->Address = dacsOptions->Address;
   d->Version = (DacsVersionE)dacsOptions->Version;
   return d;
}

GsmMessage ^fromNativeGsmMessage()
{
   //UTime time;
   UDateTime dateTime;
   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TGsmMessage) == storage->Items[CONFIG_GSM_MESSAGE].Size);
   TGsmMessage *gsmMessage = ((TGsmMessage*)storage->Items[CONFIG_GSM_MESSAGE].Data);

   GsmMessage ^d = gcnew GsmMessage();

   d->CommandsCheckPhoneNumber = gsmMessage->Commands.CheckPhoneNumber == YES;
   d->CommandsEnabled = gsmMessage->Commands.Enabled == YES;
   d->CommandsExpiration = gsmMessage->Commands.Expiration;

   d->EventMask = (GsmEventMaskE)gsmMessage->Events.EventMask;

   d->Mode = (GsmPowerModeE)gsmMessage->PowerOptions.Mode;
   d->SwitchOnDuration = gsmMessage->PowerOptions.SwitchOnDuration;
   d->SwitchOnPeriod = gsmMessage->PowerOptions.SwitchOnPeriod;
   d->SwitchOnTimes = gcnew List<TimeRange ^>();
   TimeRange ^poTime;
   for (int i = 0; i < gsmMessage->PowerOptions.TimesCount; i++){
      poTime = gcnew TimeRange();
      uClock(&dateTime, gsmMessage->PowerOptions.Times[i].From);
      poTime->From = DateTime(uYearGet(1), 1, 1, dateTime.Time.Hour, dateTime.Time.Min, dateTime.Time.Sec);
      uClock(&dateTime, gsmMessage->PowerOptions.Times[i].To);
      poTime->To = DateTime(uYearGet(1), 1, 1, dateTime.Time.Hour, dateTime.Time.Min, dateTime.Time.Sec);
      d->SwitchOnTimes->Add(poTime);
   }

   return d;
}

DataPublication ^fromNativeDataPublication()
{
   const UStorageDescriptor *storage = &ConfigDescriptor;
   assert(sizeof(TDataPublication) == storage->Items[CONFIG_DATA_PUBLICATION].Size);
   TDataPublication *dataPublication = (TDataPublication*)storage->Items[CONFIG_DATA_PUBLICATION].Data;

   DataPublication ^d = gcnew DataPublication();

   UTime time;
   uTime(&time, dataPublication->SendAt);

   d->AcceleratedPeriod = dataPublication->AcceleratedPeriod;
   d->AccelerateFromDay = dataPublication->AccelerateFromDay;
   d->Interface = (DataPublicationInterfaceE)dataPublication->Interface;
   d->Password = gcnew String(dataPublication->Password);
   d->Period = dataPublication->Period;
   d->SendAt = DateTime(uYearGet(1), 1, 1, time.Hour, time.Min, time.Sec);
   d->StartFromDay = dataPublication->StartFromDay;
   d->Url = gcnew String(dataPublication->Url);
   d->Username = gcnew String(dataPublication->Username);

   return d;
}

CellularData ^fromNativeCellularData()
{
	const UStorageDescriptor *storage = &ConfigDescriptor;
	assert(sizeof(TCellularData) == storage->Items[CONFIG_CELLULAR_DATA].Size);
	TCellularData *dataPublication = (TCellularData*)storage->Items[CONFIG_CELLULAR_DATA].Data;

	CellularData ^d = gcnew CellularData();
	d->Apn = gcnew String(dataPublication->Apn);
	d->Password = gcnew String(dataPublication->Password);
	d->Username = gcnew String(dataPublication->Username);
	
	return d;
}

Ethernet ^fromNativeEthernet()
{
	const UStorageDescriptor *storage = &ConfigDescriptor;
	assert(sizeof(TEthernet) == storage->Items[CONFIG_ETHERNET].Size);
	TEthernet *ethernet = (TEthernet*)storage->Items[CONFIG_ETHERNET].Data;

	Ethernet ^d = gcnew Ethernet();
	d->Dhcp = ethernet->Dhcp == YES;
	
	array<Byte> ^adress = BitConverter::GetBytes((unsigned int)ethernet->Gateway); 
	Array::Reverse(adress);
	d->Gateway = gcnew IPAddress(adress);
	
   adress = BitConverter::GetBytes((unsigned int)ethernet->Ip);
	Array::Reverse(adress);
	d->Ip = gcnew IPAddress(adress);
	
   adress = BitConverter::GetBytes((unsigned int)ethernet->PrimaryDns);
	Array::Reverse(adress);
	d->PrimaryDns = gcnew IPAddress(adress);
	
   adress = BitConverter::GetBytes((unsigned int)ethernet->SubnetMask);
	Array::Reverse(adress);
	d->SubnetMask = gcnew IPAddress(adress);
	
	return d;
}