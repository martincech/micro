#include "FirmwareCoder.h"
#include "FwCoder\FwCoder.h";
#include "BootloaderSocket.h"
#include "Remote\SocketIfMsdDef.h"
#include "Fw\FwStorage.h"
#include "Memory\FileRemote.h"
#include <stdio.h>
#include <stdlib.h>

using namespace Bat2Library::Connection::Manager::Native;
using namespace System::Runtime::InteropServices;
using namespace System::IO;

// encode byte array of data to output array of data
bool FirmwareCoder::Encode(array<System::Byte> ^inputData, [Out] array<System::Byte> ^%encodedData)
{
   String ^tmpFileName;
   try{
      tmpFileName = Path::GetTempFileName();
   }
   catch (Exception ^ex){
      return false;
   }

   try{
      String ^tmpPath = Path::GetTempPath();
      FileStream ^f = File::Open(tmpFileName, FileMode::OpenOrCreate);
      f->Write(inputData, 0, inputData->Length);
      f->Close();

      Encode(tmpFileName, tmpPath);

      tmpPath = Path::Combine(tmpPath, gcnew String(FIRMWARE_FILENAME));
      f = File::Open(tmpPath, FileMode::Open);
      encodedData = gcnew array<System::Byte>(f->Length);
      f->Read(encodedData, 0, f->Length);
      f->Close();
      File::Delete(tmpFileName);
   }
   catch (Exception ^ex){ 
      return false; 
   }
   return true;
}

bool FirmwareCoder::Encode(String ^InputFilePath, String ^OutputFilePath)
{
   String ^tmpFileName = Path::GetTempFileName();
   // convert managed string to native char*
   IntPtr pIn = Marshal::StringToHGlobalAnsi(InputFilePath);
   IntPtr pTmp = Marshal::StringToHGlobalAnsi(tmpFileName);
   char *inputFileC = static_cast<char*>(pIn.ToPointer());
   char *tmpFileC = static_cast<char*>(pTmp.ToPointer());

   TFwCoder *Coder = new TFwCoder();
   // encode input file to temporary file
   if (!Coder->Encode(inputFileC, tmpFileC)){
      Marshal::FreeHGlobal(pIn);
      Marshal::FreeHGlobal(pTmp);
      delete Coder;
      return false;
   }
   Marshal::FreeHGlobal(pIn);
   Marshal::FreeHGlobal(pTmp);
   delete Coder;
   
   // open just created temporary file and read data
   FileStream ^f = File::Open(tmpFileName, FileMode::OpenOrCreate);
   int length = f->Length;
   array<unsigned char> ^buf = gcnew array<unsigned char>(length);
   if (f->Read(buf, 0, length) != length){
      return false;
   }

   // create socket to surround the encoded FW with some commands
   BootloaderSocketIf *Socket = new BootloaderSocketIf(OutputFilePath);
   if (!Socket->OpenForWrite()){
      delete Socket;
      return false;
   }

   if (!FileRemoteSetup(Socket)){
      delete Socket;
      return false;
   }

   if (!FwStorageOpen()){
      delete Socket;
      return false;
   }

   pin_ptr<byte> pbuf = &buf[0];
   if (!FwStorageWrite(0, pbuf, length)){
      delete Socket;
      return false;
   }

   FwStorageConfirm();
   FwStorageClose();
   Socket->Close();

   delete Socket;
   f->Close();
   File::Delete(tmpFileName);
   return true;
}