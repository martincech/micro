#pragma once
#include "Bat2NewDevice.h"
#include "Bat2FlashSocket.h"

namespace Bat2Library
{
   namespace Connection
   {
      namespace Manager
      {
         namespace Native{
            using namespace System;
            using namespace Bat2Library::Connection::Interface::Domain;

            private ref class FlashBat2Device : Bat2NewDevice
            {
            public:

               FlashBat2Device(Bat2FlashSocket ^socket)
                  : Bat2NewDevice(socket, gcnew Bat2DeviceData())
               {   
               }

               property Bat2FlashSocket ^FlashSocket{ Bat2FlashSocket ^get(){ return static_cast<Bat2FlashSocket ^>(Socket); }}
            };
         }
      }
   }
}
