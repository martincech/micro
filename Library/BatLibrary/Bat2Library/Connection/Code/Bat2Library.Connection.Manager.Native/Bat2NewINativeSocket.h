#pragma once

#include "Bat2INativeSocket.h"
#include "Remote\SocketIf.h"

namespace Bat2Library
{
	namespace Connection
	{
		namespace Manager
		{
			namespace Native{

				private ref class Bat2NewINativeSocket abstract : Bat2INativeSocket
				{
				protected:
               void ExceptionCheck(array<byte> ^Buffer);

				public:
               virtual property SocketIf *NativeSocket {SocketIf  *get() abstract; };
               
               virtual Bat2Library::SocketStateE State(void);
               virtual bool Receive(array<byte> ^%Buffer);
               virtual int ReceiveSize(void);
               virtual bool Send(array<byte> ^Buffer);
               virtual void Close(void);
               virtual Bat2Library::FileModeE Permission(void);               

               virtual bool SetupConnection() override;
               virtual bool FreeConnection() override;

               virtual TYesNo ConfigRemoteLoad() override;
               virtual TYesNo ConfigRemoteSave() override;
               virtual TYesNo ArchiveRemoteLoad() override;

               virtual TYesNo ActionRemoteWeighingSchedulerStart(void) override;
               virtual TYesNo ActionRemoteWeighingSchedulerStartAt(UClockGauge DateTime) override;
               virtual TYesNo ActionRemoteWeighingSchedulerStop(void) override;
               virtual TYesNo ActionRemoteWeighingSchedulerSuspend(void) override;
               virtual TYesNo ActionRemoteWeighingSchedulerRelease(void) override;
               virtual TYesNo ActionRemoteTimeSet(UDateTimeGauge DateTime) override;
               virtual TYesNo ActionRemoteTimeGet(UDateTimeGauge *DateTime) override;
               virtual TYesNo ActionRemoteReboot() override;

               virtual TYesNo ContactListRemoteLoad();
               virtual TYesNo ContactListRemoteSave();

               virtual TYesNo ContextRemoteLoad();

               virtual TYesNo CurveListRemoteLoad();
               virtual TYesNo CurveListRemoteSave();

               virtual TYesNo CorrectionListRemoteLoad();
               virtual TYesNo CorrectionListRemoteSave();

               virtual TYesNo WeighingPlanListRemoteLoad();
               virtual TYesNo WeighingPlanListRemoteSave();

               virtual TYesNo PredefinedListRemoteLoad();
               virtual TYesNo PredefinedListRemoteSave();
				};
			}
		}
	}
}