#pragma once

#undef _MANAGED
#include "Remote\Frame.h"
#include "Remote\SocketIf.h"
#include <gcroot.h>

private class UsbSocketIf : SocketIf
{
public:

   UsbSocketIf(System::IO::Stream ^device);
   ~UsbSocketIf();
   virtual byte State(void);

   virtual TYesNo Receive(void *Buffer, int Size);

   virtual int ReceiveSize(void);

   virtual TYesNo Send(const void *Buffer, int Size);

   virtual void Close(void);

   virtual byte Permission(void);

private:
   gcroot<System::IO::Stream ^> device;

   TFrameState *state;
   int ReplySize;
   bool CheckOpen();
   bool isClosed;
};


#include "INativeSocket.h"
#include "Bat2NewINativeSocket.h"
#include "IStreamSocket.h"
#include "Remote\SocketIfUsbDef.h"

namespace Bat2Library
{
   namespace Connection
   {
      namespace Manager
      {
         namespace Native{
            using namespace System;
            using namespace Usb::HID;
            using namespace System::Diagnostics;
            using namespace System::Threading;
            using namespace System::IO;
            using namespace ::Native;

            private ref class Bat2UsbSocket : Bat2NewINativeSocket,
               IStreamSocket
            {
            public:
               static int TIME_CONSTANT_DEFAULT = 200;
               Bat2UsbSocket(String ^path)
               {
                  TimeConstant = TIME_CONSTANT_DEFAULT;
                  HidDeviceLoader ^hidLoader = gcnew HidDeviceLoader();
                  HidDevice ^deviceOnPath = nullptr;
                  for each (HidDevice ^hidDevice in hidLoader->GetDevices())
                  {
                     if (hidDevice->DevicePath->Equals(path)){
                        deviceOnPath = hidDevice;
                        break;
                     }
                  }
                  if (deviceOnPath == nullptr){
                     throw gcnew ArgumentException("Invalid path for socket specified!");
                  }
                  SetSocket(deviceOnPath->Open());
                  OuterHidDevice = false;
                  this->path = path;
               }

               Bat2UsbSocket(int vid, int pid)
               {
                  TimeConstant = TIME_CONSTANT_DEFAULT;
                  HidDeviceLoader ^hidLoader = gcnew HidDeviceLoader();
                  HidDevice ^hidDevice = hidLoader->GetDeviceOrDefault(Nullable<int>(vid), Nullable<int>(pid), Nullable<int>(), nullptr);
                  if (hidDevice == nullptr){
                     throw gcnew ArgumentException(String::Format("Device with vid {0} and pid {1} not found on system!", vid, pid));
                  }
                  SetSocket(hidDevice->Open());
                  OuterHidDevice = false;
                  path = hidDevice->DevicePath;
               }

               Bat2UsbSocket(Stream ^ioStream)
               {
                  TimeConstant = TIME_CONSTANT_DEFAULT;
                  SetSocket(ioStream);
                  OuterHidDevice = true;
                  HidStream ^hidStream = dynamic_cast<HidStream ^>(ioStream);
                  if (hidStream != nullptr){
                     path = hidStream->Device->DevicePath;
                  }
               }


               ~Bat2UsbSocket(){ this->!Bat2UsbSocket(); }
               !Bat2UsbSocket(){
                  free();
               }

               bool SetSocket(Stream ^ioStream)
               {
                  if (ioStream == nullptr){
                     throw gcnew ArgumentNullException("Invalid stream object!");
                  }
                  Socket = ioStream;
               }

               property Stream ^Socket
               {
                  virtual Stream ^get(){ return hidDevice; }
                  virtual void set(Stream ^value)
                  {
                     free();
                     hidDevice = value;
                     init();
                  }
               }

               property SocketIf *NativeSocket{virtual SocketIf *get() override { return (SocketIf*)socket; }}

               property int TimeConstant{ int get(){ return timeConstant; } void set(int value){
                  timeConstant = value;
               }
               }
            private:
               UsbSocketIf *socket = NULL;
               Stream ^hidDevice = nullptr;
               String ^path;
               int timeConstant;
               bool OuterHidDevice = false;

               void free()
               {
                  Lock lock(this);
                  try{
                     HidStream ^dev = dynamic_cast<HidStream ^>(hidDevice);
                     if (dev != nullptr){
                        TSocketIfUsbCmd cmd;
                        memset(&cmd, 0, sizeof(TSocketIfUsbCmd));
                        // close
                        cmd.Open = NO;
                        WriteFeature(dev, &cmd);
                        WaitForFeature(dev, &cmd);
                     }
                  }
                  catch (Exception ^ex){}


                  if (socket != NULL){
                     delete socket;
                     socket = NULL;
                  }
                  if (hidDevice != nullptr && !OuterHidDevice){
                     delete hidDevice;
                     hidDevice = nullptr;
                  }
               }
               void init()
               {
                  Lock lock(this);
                  socket = new UsbSocketIf(hidDevice);
                  // init native device
                  HidStream ^dev = dynamic_cast<HidStream ^>(hidDevice);
                  path = "";
                  if (dev != nullptr){
                     TSocketIfUsbCmd cmd;
                     memset(&cmd, 0, sizeof(TSocketIfUsbCmd));
                     // close first
                     cmd.Open = NO;
                     WriteFeature(dev, &cmd);
                     if (!WaitForFeature(dev, &cmd)){
                        throw gcnew IOException("Connection not closed on device");
                     }
                     // TODO - when updated in FW remove this waiting dummy
                     // here just to be sure everything is done in FW
                     Thread::Sleep(100);
                     // open
                     cmd.Open = YES;
                     WriteFeature(dev, &cmd);
                     if (!WaitForFeature(dev, &cmd)){
                        throw gcnew IOException("Connection not opened on device");
                     }
                     // same as above
                     Thread::Sleep(100);
                  }
               }

               void WriteFeature(HidStream ^dev, TSocketIfUsbCmd *cmd){
                  array<Byte> ^data = gcnew array<Byte>(sizeof(TSocketIfUsbCmd));
                  pin_ptr<byte> ptr = &data[0];
                  memcpy(ptr, cmd, sizeof(TSocketIfUsbCmd));
                  dev->SetFeature(data);
               }

               bool WaitForFeature(HidStream ^dev, TSocketIfUsbCmd *cmd){
                  int sleepConst = 5;
                  int waitMs = TimeConstant;
                  array<Byte> ^data = gcnew array<Byte>(sizeof(TSocketIfUsbCmd));
                  do{
                     dev->GetFeature(data);
                     if (data->Length == sizeof(TSocketIfUsbCmd)){
                        pin_ptr<byte> ptr = &data[0];
                        if (((TSocketIfUsbCmd *)ptr)->Open == cmd->Open){
                           return true;
                        }
                     }
                     Thread::Sleep(sleepConst);
                     waitMs -= sleepConst;
                  } while (waitMs > 0);

                  return false;
               }
            };
         }
      }
   }
}