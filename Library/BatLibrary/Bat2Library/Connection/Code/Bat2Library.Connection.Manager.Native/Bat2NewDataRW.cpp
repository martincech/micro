#include "Bat2NewDataRW.h"
#include "Config\FactoryDefault.h"

using namespace Bat2Library::Connection::Manager::Native;
using namespace Bat2Library::Connection::Interface::IO;
using namespace Bat2Library::Connection::Interface::Domain;
using namespace Native;
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Reflection;
using namespace System::Diagnostics;

//------------------------------------------------------------------------------
//  Load data
//------------------------------------------------------------------------------

Bat2DeviceData ^Bat2NewDataRW::LoadAll()
{
   Lock lock(dataLock);
   try{
      if (!device->Socket->SetupConnection())
      {
         return nullptr;
      }
   }
   catch (Exception ^ex)
   {
      return nullptr;
   }

   if (LoadConfig(false) == nullptr){
      return nullptr;
   }
   if (LoadContext(false) == nullptr){
      return nullptr;
   }
   if (LoadArchive(false) == nullptr){
      return nullptr;
   }
   if (LoadGrowthCurves(false) == nullptr){
      return nullptr;
   }
   if (LoadCorrectionCurves(false) == nullptr){
      return nullptr;
   }
   if (LoadWeighingPlans(false) == nullptr){
      return nullptr;
   }
   if (LoadContactList(false) == nullptr){
      return nullptr;
   }
   if (LoadPredefinedWeighings(false) == nullptr){
      return nullptr;
   }

   device->Socket->FreeConnection();

   return device->DeviceData;
}

Bat2DeviceData ^Bat2NewDataRW::LoadDefault()
{
	

	Bat2DeviceData ^b2Data = gcnew Bat2DeviceData();
	FactoryDefault();

	Bat2Library::Connection::Interface::Domain::Configuration ^config = gcnew Bat2Library::Connection::Interface::Domain::Configuration();
	fromNative(config);
	b2Data->Configuration = config;

	List<Contact^> ^contacts = gcnew List<Contact^>();	
	LoadList(contacts);
	b2Data->Contacts = contacts;

	List<Curve^> ^curveG = gcnew List<Curve^>();
	LoadGrowthList(curveG);
	b2Data->GrowthCurves = curveG;

	List<Curve^> ^curveC = gcnew List<Curve^>();
	LoadCorrectionList(curveC);
	b2Data->CorrectionCurves = curveC;

	List<WeighingConfiguration^> ^wConf = gcnew List<WeighingConfiguration^>();
	LoadList(wConf);
	b2Data->PredefinedWeighings = wConf;

	List<WeighingPlan^> ^wPlan = gcnew List<WeighingPlan^>();
	LoadList(wPlan);
	b2Data->WeighingPlans = wPlan;

	return b2Data;
}

Connection::Interface::Domain::Configuration ^Bat2NewDataRW::LoadConfig(bool withSetup)
{
   if (withSetup){
	   return DoLoad(gcnew LoadFuncRef<Configuration ^>(this, &Bat2NewDataRW::Load), Bat2DeviceData::typeid->GetProperty("Configuration"));
   }
   return DoLoadExecute(gcnew LoadFuncRef<Configuration ^>(this, &Bat2NewDataRW::Load), Bat2DeviceData::typeid->GetProperty("Configuration"));
}

Context ^Bat2NewDataRW::LoadContext(bool withSetup)
{
   if (withSetup){
      return DoLoad(gcnew LoadFuncRef<Context ^>(this, &Bat2NewDataRW::Load), Bat2DeviceData::typeid->GetProperty("Context"));
   }
   return DoLoadExecute(gcnew LoadFuncRef<Context ^>(this, &Bat2NewDataRW::Load), Bat2DeviceData::typeid->GetProperty("Context"));
}

IEnumerable<ArchiveItem^> ^Bat2NewDataRW::LoadArchive(bool withSetup)
{
   if (withSetup){
      return DoLoad(gcnew LoadFuncRef<List<ArchiveItem^> ^>(this, &Bat2NewDataRW::Load), Bat2DeviceData::typeid->GetProperty("Archive"));
   }
   return DoLoadExecute(gcnew LoadFuncRef<List<ArchiveItem^> ^>(this, &Bat2NewDataRW::Load), Bat2DeviceData::typeid->GetProperty("Archive"));
}

IEnumerable<Curve^> ^Bat2NewDataRW::LoadGrowthCurves(bool withSetup)
{
   if (withSetup){
      return DoLoad(gcnew LoadFuncRef<List<Curve^> ^>(this, &Bat2NewDataRW::LoadGrowth), Bat2DeviceData::typeid->GetProperty("GrowthCurves"));
   }
   return DoLoadExecute(gcnew LoadFuncRef<List<Curve^> ^>(this, &Bat2NewDataRW::LoadGrowth), Bat2DeviceData::typeid->GetProperty("GrowthCurves"));
}

IEnumerable<Curve^> ^Bat2NewDataRW::LoadCorrectionCurves(bool withSetup)
{
   if (withSetup){
      return DoLoad(gcnew LoadFuncRef<List<Curve^> ^>(this, &Bat2NewDataRW::LoadCorrection), Bat2DeviceData::typeid->GetProperty("CorrectionCurves"));
   }
   return DoLoadExecute(gcnew LoadFuncRef<List<Curve^> ^>(this, &Bat2NewDataRW::LoadCorrection), Bat2DeviceData::typeid->GetProperty("CorrectionCurves"));
   
}

IEnumerable<WeighingPlan^> ^Bat2NewDataRW::LoadWeighingPlans(bool withSetup)
{
   if (withSetup){
      return DoLoad(gcnew LoadFuncRef<List<WeighingPlan^> ^>(this, &Bat2NewDataRW::Load), Bat2DeviceData::typeid->GetProperty("WeighingPlans"));
   }
   return DoLoadExecute(gcnew LoadFuncRef<List<WeighingPlan^> ^>(this, &Bat2NewDataRW::Load), Bat2DeviceData::typeid->GetProperty("WeighingPlans"));
}

IEnumerable<Contact^> ^Bat2NewDataRW::LoadContactList(bool withSetup)
{
   if (withSetup){
      return DoLoad(gcnew LoadFuncRef<List<Contact^> ^>(this, &Bat2NewDataRW::Load), Bat2DeviceData::typeid->GetProperty("Contacts"));
   }
   return DoLoadExecute(gcnew LoadFuncRef<List<Contact^> ^>(this, &Bat2NewDataRW::Load), Bat2DeviceData::typeid->GetProperty("Contacts"));
}

IEnumerable<WeighingConfiguration ^> ^Bat2NewDataRW::LoadPredefinedWeighings(bool withSetup)
{
   if (withSetup){
      return DoLoad(gcnew LoadFuncRef<List<WeighingConfiguration^> ^>(this, &Bat2NewDataRW::Load), Bat2DeviceData::typeid->GetProperty("PredefinedWeighings"));
   }
   return DoLoadExecute(gcnew LoadFuncRef<List<WeighingConfiguration^> ^>(this, &Bat2NewDataRW::Load), Bat2DeviceData::typeid->GetProperty("PredefinedWeighings"));
}

//------------------------------------------------------------------------------
//  Save data
//------------------------------------------------------------------------------

bool Bat2NewDataRW::WriteAll(Bat2DeviceData ^data)
{
   if (!SaveConfig(data->Configuration)) return false;
   if (!SaveGrowthCurves(data->GrowthCurves)) return false;
   if (!SaveCorrectionCurves(data->CorrectionCurves)) return false;
   if (!SaveContactList(data->Contacts)) return false;
   if (!SaveWeighingPlans(data->WeighingPlans)) return false;
   return true;
}

bool Bat2NewDataRW::SaveConfig(Connection::Interface::Domain::Configuration ^config)
{
   if (config == nullptr){
      throw gcnew ArgumentNullException("config", "Configuration can't be null!");
   }
   return DoSave(gcnew SaveFuncRef<Configuration ^>(this, &Bat2NewDataRW::Save), config, Bat2DeviceData::typeid->GetProperty("Configuration"));
}

bool Bat2NewDataRW::SaveGrowthCurves(IEnumerable<Curve^> ^growthCurves)
{
   return DoSave(gcnew SaveFuncRef<IEnumerable<Curve^>^>(this, &Bat2NewDataRW::SaveGrowth), growthCurves, Bat2DeviceData::typeid->GetProperty("GrowthCurves"));
}
bool Bat2NewDataRW::SaveCorrectionCurves(IEnumerable<Curve^> ^correctionCurves)
{
   return DoSave(gcnew SaveFuncRef<IEnumerable<Curve^>^>(this, &Bat2NewDataRW::SaveCorrection), correctionCurves, Bat2DeviceData::typeid->GetProperty("CorrectionCurves"));
}
bool Bat2NewDataRW::SaveContactList(IEnumerable<Contact^> ^contacts)
{
   return DoSave(gcnew SaveFuncRef<IEnumerable<Contact^>^>(this, &Bat2NewDataRW::Save), contacts, Bat2DeviceData::typeid->GetProperty("Contacts"));
}
bool Bat2NewDataRW::SaveWeighingPlans(IEnumerable<WeighingPlan^>^ weighingPlans)
{
   return DoSave(gcnew SaveFuncRef<IEnumerable<WeighingPlan^>^>(this, &Bat2NewDataRW::Save), weighingPlans, Bat2DeviceData::typeid->GetProperty("WeighingPlans"));
}

bool Bat2NewDataRW::SavePredefinedWeighings(IEnumerable<WeighingConfiguration^>^ predefinedWeighings)
{
   return DoSave(gcnew SaveFuncRef<IEnumerable<WeighingConfiguration^>^>(this, &Bat2NewDataRW::Save), predefinedWeighings, Bat2DeviceData::typeid->GetProperty("PredefinedWeighings"));
}
