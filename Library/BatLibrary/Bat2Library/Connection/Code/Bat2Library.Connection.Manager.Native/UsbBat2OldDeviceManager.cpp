#include "UsbBat2OldDeviceManager.h"

using namespace Bat2Library::Connection::Manager::Native;
using namespace System::Threading;

UsbBat2OldDeviceManager::UsbBat2OldDeviceManager()
{
	existingDevices = gcnew Dictionary<FTDIDevice ^, Bat2OldDevice ^>();
	usbWatcher = UsbDeviceManager::Instance;
	usbWatcher->DeviceConnected += gcnew System::EventHandler<Usb::UsbDevice ^>(this, &Connection::Manager::Native::UsbBat2OldDeviceManager::OnDeviceConnected);
	usbWatcher->DeviceDisconnected += gcnew System::EventHandler<Usb::UsbDevice ^>(this, &Connection::Manager::Native::UsbBat2OldDeviceManager::OnDeviceDisconnected);
}

void UsbBat2OldDeviceManager::OnDeviceConnected(System::Object ^sender, UsbDevice ^e)
{
	FTDIDevice ^dev = dynamic_cast<FTDIDevice ^>(e);
	if (dev == nullptr || !UsbBat2OldDevice::IsValid(dev->ProductName)){ return; }
	UsbBat2OldDevice ^b2d = CreateUsbBat2OldDevice(dev);
   Bat2OldDataRW ^dLoader = CreateDataRwObject(b2d);

   bool validLoad = false;
   for (int i = 0; i < 3; i++)
   {
      OldConfiguration ^config = dLoader->LoadConfig();
      // try to load identification with 3 tries
      if (config != nullptr && config->VersionInfo->IsValid){
         validLoad = true;
         break;
      }
      // sleep for awhile
      Thread::CurrentThread->Sleep(20);
   }
   if (!validLoad)
   {
      return;
   }
	OldBat2DeviceData ^data = b2d->DeviceData;
	if (data == nullptr){ return; }

#ifdef _DEBUG
	if (data->Configuration->VersionInfo->SerialNumber == 0){
		data->Configuration->VersionInfo->SerialNumber = b2d->UsbSocket->GetHashCode();
	}
#endif

	existingDevices->Add(dev, b2d);
	if (data->IsValid){
		Devices->Add(b2d);
		DeviceConnected(this, data);
	}
}

void UsbBat2OldDeviceManager::OnDeviceDisconnected(System::Object ^sender, UsbDevice ^e)
{
	FTDIDevice ^dev = dynamic_cast<FTDIDevice ^>(e);
	if (dev == nullptr || !UsbBat2OldDevice::IsValid(dev->ProductName)){ return; }
	if (existingDevices->ContainsKey(dev)){
		Bat2OldDevice ^b2d = existingDevices[dev];

		OldBat2DeviceData ^data = b2d->DeviceData;
		if (data == nullptr){ return; }

		DeviceDisconnected(this, data);
		Devices->Remove(b2d);
		delete b2d;
	}
}
