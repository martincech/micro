#include <string.h>
#include "Efs.h"

using namespace System;
using namespace System::IO;
using namespace System::Collections::Generic;
using namespace System::Runtime::InteropServices;
using namespace Bat2Library::Connection::Manager::Native;

static char _EfsRoot[EFS_PATH_MAX + 1];
static char _EfsDirectory[EFS_PATH_MAX + 1];


void EfsRoot(const char *Directory)
// Setup external filesystem root to <Directory>
{
   strcpy(_EfsRoot, Directory);
} // EfsRoot

//-----------------------------------------------------------------------------
//   Initialize
//-----------------------------------------------------------------------------

TYesNo EfsInit(void)
// Initialisation
{
   if (strlen(_EfsRoot) == 0){
      return NO;
   }
   strcpy(_EfsDirectory, _EfsRoot);
   return YES;
} // EfsInit

//-----------------------------------------------------------------------------
//   Format
//-----------------------------------------------------------------------------

void DeleteDirectoryContext(String ^path){
   // delete subdirectories
   for each (String ^fileName in Directory::EnumerateDirectories(path))
   {
      DeleteDirectoryContext(fileName);
      Directory::Delete(fileName);
   }
   // delete files
   for each (String ^fileName in Directory::EnumerateFiles(path))
   {
      File::Delete(fileName);
   }
}

TYesNo EfsFormat(void)
// Format
{
   try{
      DeleteDirectoryContext(gcnew String(_EfsRoot));
   }
   catch (Exception ^)
   {
      return NO;
   }
   return YES;
}

String ^DirectoryPath(const char *Name)
{
   return Path::GetFullPath(Path::Combine(gcnew String(_EfsDirectory), gcnew String(Name)));
}

//-----------------------------------------------------------------------------
//   Directory create
//-----------------------------------------------------------------------------

TYesNo EfsDirectoryCreate(const char *Name)
// Create new directory with <Name>
{
   String ^path = DirectoryPath(Name);
   DirectoryInfo ^info;
   try{
      info = Directory::CreateDirectory(path);
   }
   catch (Exception ^)
   {
      return NO;
   }
   if (info->Exists){
      EfsDirectoryChange(Name);
   }
   return info->Exists ? YES : NO;
} // EfsDirectoryCreate

//-----------------------------------------------------------------------------
//   Directory exists
//-----------------------------------------------------------------------------

TYesNo EfsDirectoryExists(const char *Name)
// Check for directory <Name>
{
   return Directory::Exists(DirectoryPath(Name)) ? YES : NO;
} // EfsDirectoryExists

//-----------------------------------------------------------------------------
//   Directory rename
//-----------------------------------------------------------------------------

TYesNo EfsDirectoryRename(const char *Name, const char *NewName)
// Renames selected directory
{
   String ^curPath = DirectoryPath(Name);
   String ^newPath = DirectoryPath(NewName);
   if (curPath->Equals(newPath)){
      return YES;
   }
   if (!Directory::Exists(curPath)){ return NO; }
   try{
      Directory::Move(curPath, newPath);
   }
   catch (Exception ^ex)
   {
      return NO;
   }
   return YES;
} // EfsDirectoryRename

//-----------------------------------------------------------------------------
//   Directory change
//-----------------------------------------------------------------------------

TYesNo EfsDirectoryChange(const char *Name)
// Change current directory to <Name>
{
   String ^newPath = DirectoryPath(Name);
   if (!newPath->Contains(gcnew String(_EfsRoot)))
   {
      return NO;
   }
   if (!Directory::Exists(newPath)){
      return NO;
   }
   IntPtr str = Marshal::StringToHGlobalAnsi(newPath);
   strcpy(_EfsDirectory, (char*)str.ToPointer());
   Marshal::FreeHGlobal(str);
   return YES;
} // EfsDirectoryChange

//-----------------------------------------------------------------------------
//   Directory delete
//-----------------------------------------------------------------------------

TYesNo EfsDirectoryDelete(const char *Name)
// Delete directory <Name>
{
   try{
      DeleteDirectoryContext(DirectoryPath(Name));
      Directory::Delete(DirectoryPath(Name));
   }
   catch (Exception ^ex)
   {
      return NO;
   }
   return Directory::Exists(DirectoryPath(Name))? NO : YES;
} // EfsDirectoryDelete

//-----------------------------------------------------------------------------
//   File exists
//-----------------------------------------------------------------------------

TYesNo EfsFileExists(const char *Name)
// Check for file <Name>
{
   return File::Exists(DirectoryPath(Name)) ? YES : NO;
} // EfsFileExists

//-----------------------------------------------------------------------------
//   File create
//-----------------------------------------------------------------------------

TYesNo EfsFileCreate(TEfsFile *File, const char *FileName, dword Size)
// Create <FileName>
{
   if (!File::Exists(DirectoryPath(FileName))){
      FileStream ^fs = File::Create(DirectoryPath(FileName));
      fs->Close();
   }
   
   return EfsFileOpen(File, FileName);
} // EfsFileCreate

//-----------------------------------------------------------------------------
//   File open
//-----------------------------------------------------------------------------

TYesNo EfsFileOpen(TEfsFile *File, const char *FileName)
// Open <FileName> for read/write
{
   TEfsFile fdesc;
   if (!Efs::AddFile(DirectoryPath(FileName), fdesc)){
      return NO;
   }
   Efs::GetFile(fdesc);
   *File = fdesc;
   return YES;
} // EfsFileOpen

//-----------------------------------------------------------------------------
//   File delete
//-----------------------------------------------------------------------------

TYesNo EfsFileDelete(const char *FileName)
// Delete <FileName>
{
   return Efs::DeleteFile(DirectoryPath(FileName)) ? YES : NO;
} // EfsFileDelete

dword EfsFileSize(TEfsFile *File)
{
   return Efs::FileSize(*File);
}

//-----------------------------------------------------------------------------
//   File close
//-----------------------------------------------------------------------------

void EfsFileClose(TEfsFile *File)
// Close opened file
{
   Efs::RemoveFile(*File);
} // EfsFileClose

//-----------------------------------------------------------------------------
//   File write
//-----------------------------------------------------------------------------

word EfsFileWrite(TEfsFile *File, const void *Data, word Count)
// Write <Data> with <Count> bytes, returns number of bytes written
{
   FileStream ^fs = Efs::GetFile(*File);
   if (fs == nullptr){
      return 0;
   }
   array<byte> ^buf = gcnew array<byte>(Count);
   pin_ptr<byte> ptr = &buf[0];
   memcpy(ptr,((byte*)Data), Count);
   try{
      fs->Write(buf, 0, buf->Length);
   }
   catch(Exception ^){
      Count = 0;
   }
   return Count;
} // EfsFileWrite

//-----------------------------------------------------------------------------
//   File read
//-----------------------------------------------------------------------------

word EfsFileRead(TEfsFile *File, void *Data, word Count)
// Read <Data> with <Count> bytes, returns number of bytes read
{
   FileStream ^fs = Efs::GetFile(*File);
   if (fs == nullptr){
      return 0;
   }
   array<byte> ^buf = gcnew array<byte>(Count);
   pin_ptr<byte> ptr = &buf[0];
   try{
      Count = fs->Read(buf, 0, Count);
   }
   catch (Exception ^){
      Count = 0;
   }
   memcpy(Data, ptr, Count);
   return Count;
} // EfsFileRead

//-----------------------------------------------------------------------------
//   File seek
//-----------------------------------------------------------------------------

TYesNo EfsFileSeek(TEfsFile *File, int32 Pos, EEfsSeekMode Whence)
// Seek at <Pos> starting from <Whence>
{
   FileStream ^fs = Efs::GetFile(*File);
   if (fs == nullptr){
      return NO;
   }
   if (!fs->CanSeek){
      return NO;
   }
   switch (Whence){
   case  EFS_SEEK_START:
      fs->Seek(Pos, SeekOrigin::Begin);
      break;
   case  EFS_SEEK_END:
      fs->Seek(Pos, SeekOrigin::End);
      break;
   case  EFS_SEEK_CURRENT:
      fs->Seek(Pos, SeekOrigin::Current);
      break;
   }
   return YES;
} // EfsFileSeek

//-----------------------------------------------------------------------------
//   Unmount
//-----------------------------------------------------------------------------

void EfsSwitchOff(void)
// Safely unmount drive
{
} // EfsUnmount


#ifdef _DEBUG
#undef _MANAGED
#include "Remote\SocketIfMsd.h"

bool EfsTest::TestSocket(){
   return SocketIfMsdTest() == YES;
}

EfsTest::EfsTest(String ^Directory)
// Setup external filesystem root to <Directory>
{
   IntPtr strPtr = Marshal::StringToHGlobalAnsi(Directory);
   ::EfsRoot((const char *)strPtr.ToPointer());
}

bool EfsTest::EfsInit(void)
// Initialisation
{
   return ::EfsInit() == YES;
}

bool EfsTest::EfsFormat(void)
// Format
{
   return ::EfsFormat() == YES;
}

bool EfsTest::EfsDirectoryExists(String ^Path)
// Check for directory <Name>
{
   IntPtr strPtr = Marshal::StringToHGlobalAnsi(Path);
   return ::EfsDirectoryExists((const char *)strPtr.ToPointer()) == YES;
}

bool EfsTest::EfsDirectoryCreate(String ^Path)
// Create new directory with <Name>
{
   IntPtr strPtr = Marshal::StringToHGlobalAnsi(Path);
   return ::EfsDirectoryCreate((const char *)strPtr.ToPointer()) == YES;
}

bool EfsTest::EfsDirectoryChange(String ^Path)
// Change current directory to <Name>
{
   IntPtr strPtr = Marshal::StringToHGlobalAnsi(Path);
   return ::EfsDirectoryChange((const char *)strPtr.ToPointer()) == YES;
}

bool EfsTest::EfsDirectoryDelete(String ^Path)
// Delete directory <Name>
{
   IntPtr strPtr = Marshal::StringToHGlobalAnsi(Path);
   return ::EfsDirectoryDelete((const char *)strPtr.ToPointer()) == YES;
}

bool EfsTest::EfsDirectoryRename(String ^Path, String ^NewName)
// Renames selected directory
{
   IntPtr strPtr = Marshal::StringToHGlobalAnsi(Path);
   IntPtr strPtr2 = Marshal::StringToHGlobalAnsi(NewName);
   return ::EfsDirectoryRename((const char *)strPtr.ToPointer(), (const char *)strPtr2.ToPointer()) == YES;
}

bool EfsTest::EfsFileExists(String ^Name)
// Check for file <Name>
{
   IntPtr strPtr = Marshal::StringToHGlobalAnsi(Name);
   return ::EfsFileExists((const char *)strPtr.ToPointer()) == YES;
}

bool EfsTest::EfsFileCreate(TEfsFile %File, String ^FileName, dword Size)
// Create <FileName>
{
   IntPtr strPtr = Marshal::StringToHGlobalAnsi(FileName);
   pin_ptr<TEfsFile> ptr = &File;
   return ::EfsFileCreate(ptr, (const char *)strPtr.ToPointer(), Size) == YES;
}

bool EfsTest::EfsFileOpen(TEfsFile %File, String ^FileName)
// Open <FileName> for read/write
{
   IntPtr strPtr = Marshal::StringToHGlobalAnsi(FileName);
   pin_ptr<TEfsFile> ptr = &File;
   return ::EfsFileOpen(ptr, (const char *)strPtr.ToPointer()) == YES;
}

bool EfsTest::EfsFileDelete(String ^FileName)
// Delete <FileName>
{
   IntPtr ptr = Marshal::StringToHGlobalAnsi(FileName);
   return ::EfsFileDelete((const char *)ptr.ToPointer()) == YES;
}

void EfsTest::EfsFileClose(TEfsFile %File)
// Close opened file
{
   pin_ptr<TEfsFile> ptr = &File;
   return ::EfsFileClose(ptr);
}

word EfsTest::EfsFileWrite(TEfsFile %File, array<byte> ^Data, word Count)
// Write <Data> with <Count> bytes, returns number of bytes written
{
   pin_ptr<TEfsFile> ptr = &File;
   pin_ptr<byte> dataPtr = &Data[0];
   return ::EfsFileWrite(ptr, dataPtr, Count);
}

word EfsTest::EfsFileRead(TEfsFile %File, array<byte> ^%Data, word Count)
// Read <Data> with <Count> bytes, returns number of bytes read
{
   pin_ptr<TEfsFile> ptr = &File;
   pin_ptr<byte> dataPtr = &Data[0];
   return ::EfsFileRead(ptr, dataPtr, Count);
}

bool EfsTest::EfsFileSeek(TEfsFile %File, int32 Pos, SeekOrigin Whence)
// Seek at <Pos> starting from <Whence>
{
   pin_ptr<TEfsFile> ptr = &File;
   EEfsSeekMode sm;
   switch (Whence){
   case  SeekOrigin::Begin:
      sm = EFS_SEEK_START;
      break;
   case  SeekOrigin::End:
      sm = EFS_SEEK_END;
      break;
   case  SeekOrigin::Current:
      sm = EFS_SEEK_CURRENT;
      break;
   }
   return ::EfsFileSeek(ptr, Pos, sm) == YES;
}
#endif