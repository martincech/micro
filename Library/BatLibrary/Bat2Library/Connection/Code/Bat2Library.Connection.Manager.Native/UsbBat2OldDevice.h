#pragma once
#include "Bat2OldDevice.h"
#include "Bat2OldUsbSocket.h"
#include "Hardware.h"

namespace Bat2Library
{
	namespace Connection
	{
		namespace Manager
		{
			namespace Native{
				using namespace System;
				using namespace Bat2Library::Connection::Interface::Domain::Old;
				using namespace Bat2Old::Constants;

				private ref class UsbBat2OldDevice : Bat2OldDevice
				{
				public:

					static bool IsValid(String ^deviceName)
					{
						return Bat2Ftdi::USB_DEVICE_NAME == deviceName || Bat2Ftdi::USB_DUTCHMAN_NAME == deviceName;
					}

					UsbBat2OldDevice(Bat2OldUsbSocket ^socket)
                  : Bat2OldDevice(socket, gcnew OldBat2DeviceData())
					{
					}

					property Bat2OldUsbSocket ^UsbSocket{ Bat2OldUsbSocket ^get(){ return static_cast<Bat2OldUsbSocket ^>(Socket); }}
				};
			}
		}
	}
}