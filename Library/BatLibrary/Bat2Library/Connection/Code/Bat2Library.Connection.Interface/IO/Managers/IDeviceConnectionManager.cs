﻿using System;
using System.Collections.Generic;

namespace Bat2Library.Connection.Interface.IO
{
   public interface IDeviceConnectionManager<T> : IDisposable
   {
      /// <summary>
      /// Event raised when new device connected
      /// </summary> 
      event EventHandler<T> DeviceConnected;
      /// <summary>
      /// Event raised when device 
      /// </summary> 
      event EventHandler<T> DeviceDisconnected;

      /// <summary>
      /// Return collection of all connected devices
      /// </summary> 
      IEnumerable<T> ConnectedDevices { get; }
   }
}
