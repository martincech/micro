﻿using System.Collections.Generic;
using Bat2Library.Connection.Interface.Domain;

namespace Bat2Library.Connection.Interface.IO
{
   public interface IBat2NewDataReader : IBat2DataReader<VersionInfo, Configuration>
   {
      /// <summary>
      /// Load context from device
      /// </summary>
      /// <returns>A loaded <see cref="Context"/> object 
      /// or null when imposible to load</returns>
      Context LoadContext();

      /// <summary>
      /// Load archive from device
      /// </summary>
      /// <returns>A loaded <see cref="IEnumerable{ArchiveItem}"/> of <see cref="ArchiveItem"/>s
      /// or null when imposible to load</returns>
      IEnumerable<ArchiveItem> LoadArchive();

      /// <summary>
      /// Load saved growth curves from device
      /// </summary>
      /// <returns>A loaded <see cref="IEnumerable{Curve}"/> of <see cref="Curve"/>s
      /// or null when imposible to load</returns>
      IEnumerable<Curve> LoadGrowthCurves();

      /// <summary>
      /// Load saved correction curves from device
      /// </summary>
      /// <returns>A loaded <see cref="IEnumerable{Curve}"/> of <see cref="Curve"/>s
      /// or null when imposible to load</returns>
      IEnumerable<Curve> LoadCorrectionCurves();

      /// <summary>
      /// Load saved weighing plans from device
      /// </summary>
      /// <returns>A loaded <see cref="IEnumerable{WeighingPlan}"/> of <see cref="WeighingPlan"/>s
      /// or null when imposible to load</returns>
      IEnumerable<WeighingPlan> LoadWeighingPlans();

      /// <summary>
      /// Load saved contacts from device
      /// </summary>
      /// <returns>A loaded <see cref="IEnumerable{Contact}"/> of <see cref="Contact"/>s
      /// or null when imposible to load</returns>
      IEnumerable<Contact> LoadContactList();

      /// <summary>
      /// Load saved predefined weighings configurations from device
      /// </summary>
      /// <returns>A loaded <see cref="IEnumerable{WeighingConfiguration}"/> of <see cref="WeighingConfiguration"/>s
      /// or null when imposible to load</returns>
      IEnumerable<WeighingConfiguration> LoadPredefinedWeighings();
   };
}