﻿using Bat2Library.Connection.Interface.Domain.Old;

namespace Bat2Library.Connection.Interface.IO
{
   public interface IBat2OldDataWriter : IBat2DataWriter<BaseVersionInfo, OldConfiguration>
    {
    }
}
