﻿using System;
using Bat2Library.Connection.Interface.Domain.Old;

namespace Bat2Library.Connection.Interface.IO
{
   public interface IBat2DataReader<S, out T> : IDisposable
      where S : BaseVersionInfo
      where T : BaseConfiguration<S> 
   {      /// <summary>
      /// Load configuration from device
      /// </summary>
      /// <returns>A loaded <see cref="BaseConfiguration{S}"/> object or null when imposible to load</returns>
      T LoadConfig();
   };
}
