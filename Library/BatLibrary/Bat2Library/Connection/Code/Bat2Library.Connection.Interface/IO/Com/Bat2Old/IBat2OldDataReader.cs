﻿using System.Collections.Generic;
using Bat2Library.Connection.Interface.Domain.Old;

namespace Bat2Library.Connection.Interface.IO
{
   public interface IBat2OldDataReader : IBat2DataReader<BaseVersionInfo, OldConfiguration>
    {
        /// <summary>
        /// Load archive from device
        /// </summary>
        /// <returns>A loaded <see cref="IEnumerable{OldArchiveDailyInfo}"/> of <see cref="OldArchiveDailyInfo"/>s
        /// or null when imposible to load</returns>
        IEnumerable<OldArchiveDailyInfo> LoadArchive();
    }
}
