﻿using System.Collections.Generic;
using Bat2Library.Connection.Interface.Domain;

namespace Bat2Library.Connection.Interface.IO
{
   public interface IBat2NewDataWriter : IBat2DataWriter<VersionInfo, Configuration>
   {      
      /// <summary>
      /// Save growth curves collection to device
      /// </summary>
      /// <param name="growthCurves">Data description object of data to be writen, <see cref="Curve"/></param>
      /// <returns>true/false whether save was succesfull or not</returns>
      bool SaveGrowthCurves(IEnumerable<Curve> growthCurves);

      /// <summary>
      /// Save correction curves collection to device
      /// </summary>
      /// <param name="correctionCurves">Data description object of data to be writen, <see cref="Curve"/></param>
      /// <returns>true/false whether save was succesfull or not</returns>
      bool SaveCorrectionCurves(IEnumerable<Curve> correctionCurves);

      /// <summary>
      /// Save contacts collection to device
      /// </summary>
      /// <param name="contacts">Data description object of data to be writen, <see cref="Contact"/></param>
      /// <returns>true/false whether save was succesfull or not</returns>
      bool SaveContactList(IEnumerable<Contact> contacts);

      /// <summary>
      /// Save weighing plans to device
      /// </summary>
      /// <param name="weighingPlans">Data description object of data to be writen, <see cref="WeighingPlan"/></param>
      /// <returns>true/false whether save was succesfull or not</returns>
      bool SaveWeighingPlans(IEnumerable<WeighingPlan> weighingPlans);

      /// <summary>
      /// Save predefined weighings to device
      /// </summary>
      /// <param name="predefinedWeighings">Data description object of data to be writen, <see cref="WeighingConfiguration"/></param>
      /// <returns>true/false whether save was succesfull or not</returns>
      bool SavePredefinedWeighings(IEnumerable<WeighingConfiguration> predefinedWeighings);
   };
}