﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Connection.Interface.Domain
{
   /// <summary>
   /// All data a single BAT2 device contains.
   /// </summary>
   [DataContract]
   public class Bat2DeviceData
   {
      /// <summary>
      /// Construct empty data structure, <see cref="DataMember"/>s 
      /// required properties are initialized, others are null.
      /// </summary>
      public Bat2DeviceData()
      {
         Configuration = new Configuration();
      }
      /// <summary>
      /// Device configuration
      /// </summary>
      [DataMember(IsRequired = true, Order = 1)]
      public Configuration Configuration { get; set; } 
      /// <summary>
      /// Device context
      /// </summary>
      [DataMember(IsRequired = false, Order = 2)]
      public Context Context { get; set; }
      /// <summary>
      /// Archive of sttistics
      /// </summary>
      [DataMember(IsRequired = false, Order = 3)]
      public IEnumerable<ArchiveItem> Archive { get; set; }
      /// <summary>
      /// List of growth curves
      /// </summary>
      [DataMember(IsRequired = false, Order = 4)]
      public IEnumerable<Curve> GrowthCurves { get; set; }
      /// <summary>
      /// List of  correction curves
      /// </summary>
      [DataMember(IsRequired = false, Order = 5)]
      public IEnumerable<Curve> CorrectionCurves { get; set; }
      /// <summary>
      /// List of contacts
      /// </summary>
      [DataMember(IsRequired = false, Order = 6)]
      public IEnumerable<Contact> Contacts { get; set; }
      /// <summary>
      /// List of weighing plans
      /// </summary>
      [DataMember(IsRequired = false, Order = 7)]
      public IEnumerable<WeighingPlan> WeighingPlans { get; set; }
   }
}
