﻿using System.Runtime.Serialization;

namespace Connection.Interface.Domain
{
   /// <summary>
   /// Configuration of RS485 line
   /// </summary>
   [DataContract]
   public class Rs485Options
   {
      /// <summary>
      /// Enabling/disabling
      /// </summary>
      [DataMember]
      public bool Enabled { get; set; }
      /// <summary>
      /// Mode of operation
      /// </summary>
      [DataMember]
      public byte Mode { get; set; }
      /// <summary>
      /// Address of RS485 device
      /// </summary>
      [DataMember]
      public byte PhysicalDeviceAddress { get; set; }
   }
}