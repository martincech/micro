using System.Runtime.Serialization;

namespace Connection.Interface.Domain
{
   /// <summary>
   /// Ethernet port configuration
   /// </summary>
   [DataContract]
   public class EthernetOptions
   {
      /// <summary>
      /// Port to connect to
      /// </summary>
      [DataMember]
      public short Port { get; set; }
      /// <summary>
      /// Address to connect to
      /// </summary>
      [DataMember]
      public string Address { get; set; }
      /// <summary>
      /// Enabling/disabling module
      /// </summary>
      [DataMember]
      public bool Enabled { get; set; }
   }
}