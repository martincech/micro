﻿using System.Runtime.Serialization;

namespace Connection.Interface.Domain
{
   /// <summary>
   /// Language and country settings
   /// </summary>
   [DataContract]
   public class Country
   {
      /// <summary>
      /// Language to be used
      /// </summary>
      [DataMember]
      public byte Language { get; set; }
      /// <summary>
      /// Code of country
      /// </summary>
      [DataMember]
      public byte CountryCode { get; set; }
      /// <summary>
      /// Code page of texts
      /// </summary>
      [DataMember]
      public byte CodePage { get; set; }
      /// <summary>
      /// Format of date
      /// </summary>
      [DataMember]
      public byte DateFormat { get; set; }
      /// <summary>
      /// Separator between days and months
      /// </summary>
      [DataMember]
      public char DateSeparator1 { get; set; }
      /// <summary>
      /// Separator between months and years
      /// </summary>
      [DataMember]
      public char DateSeparator2 { get; set; }
      /// <summary>
      /// Format of time
      /// </summary>
      [DataMember]
      public byte TimeFormat { get; set; }
      /// <summary>
      /// Separator betwenn hours, mins, secs
      /// </summary>
      [DataMember]
      public char TimeSeparator { get; set; }
      /// <summary>
      /// Type of dayling savings (winter/summer time switching)
      /// </summary>
      [DataMember]
      public byte DaylightSavingType { get; set; }
   }
}