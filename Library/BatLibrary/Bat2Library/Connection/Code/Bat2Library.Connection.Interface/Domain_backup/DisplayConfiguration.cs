using System.Runtime.Serialization;

namespace Connection.Interface.Domain
{
   /// <summary>
   /// Display settings
   /// </summary>
   [DataContract]
   public class DisplayConfiguration
   {
      /// <summary>
      /// Contrast of display
      /// </summary>
      [DataMember]
      public byte Contrast { get; set; }
      /// <summary>
      /// Mode of operation
      /// </summary>
      [DataMember]
      public byte Mode { get; set; }
      /// <summary>
      /// Duration of backlight
      /// </summary>
      [DataMember]
      public short BacklightDuration { get; set; }
      /// <summary>
      /// Intensity of backlight
      /// </summary>
      [DataMember]
      public byte BacklightIntensity { get; set; }
      /// <summary>
      /// Mode of backlight operation
      /// </summary>
      [DataMember]
      public byte BacklightMode { get; set; }
   }
}