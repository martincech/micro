﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Connection.Interface.Domain
{
   /// <summary>
   /// Weighing plan configuraiton
   /// </summary>
   [DataContract]
   public class WeighingPlan
   {
      /// <summary>
      /// Plan name
      /// </summary>
      [DataMember]
      public String Name { get; set; }
      /// <summary>
      /// Synchronize scheduler with Day Start/Closed time
      /// </summary>
      [DataMember]
      public bool SyncWithDayStart { get; set; }             
      /// <summary>
      /// Mode of weighing days
      /// </summary>
      [DataMember]
      public byte WeighingDaysMode { get; set; }
      /// <summary>
      /// Mask for days of week mode
      /// </summary>
      [DataMember]
      public byte WeighingDaysDays { get; set; }
      /// <summary>
      /// Start day of weighing
      /// </summary>
      [DataMember]
      public short WeighingDaysStartDay { get; set; }
      /// <summary>
      /// Suspended days for specific mode only.
      /// </summary>
      [DataMember]
      public short WeighingDaysSuspendedDays { get; set; }
      /// <summary>
      /// Online days for specific mode only.
      /// </summary>
      [DataMember]
      public short WeighingDaysOnlineDays { get; set; }
      /// <summary>
      /// Weighing times configuration
      /// </summary>
      [DataMember]
      public IEnumerable<TimeRange> WeighingTimes { get; set; }     // Weighing times list
   }

}