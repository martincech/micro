﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Connection.Interface.Domain
{
   /// <summary>
   /// Single archive item with statistic calculation
   /// </summary>
   [DataContract]
   public class ArchiveItem
   {
      /// <summary>
      /// Zone identifier (record identifier)
      /// </summary>
      public short ZoneIdentifier { get; set; }
      /// <summary>
      /// Target weight for males or sexless
      /// </summary>
      public Int32 TargetWeight { get; set; }
      /// <summary>
      /// Target weight for females
      /// </summary>
      public Int32 TargetWeightFemale { get; set; }
      /// <summary>
      /// Statistic identifier (record originator)
      /// </summary>
      [DataMember]
      public short StatisticCalculate { get; set; }
      /// <summary>
      /// Technological day
      /// </summary>
      [DataMember]
      public short Day { get; set; }                     
      /// <summary>
      /// Statistic timestamp
      /// </summary>
      [DataMember]
      public DateTime Timestamp { get; set; }               
      /// <summary>
      /// Weight samples count
      /// </summary>
      [DataMember]
      public Int32 Count { get; set; }                   
      /// <summary>
      /// Average weight
      /// </summary>
      [DataMember]
      public Int32 Average { get; set; }                 
      /// <summary>
      /// Daily Gain (may be negative)
      /// </summary>
      [DataMember]
      public Int32 Gain { get; set; }                    
      /// <summary>
      /// Weight standard deviation
      /// </summary>
      [DataMember]
      public Int32 Sigma { get; set; }                   
      /// <summary>
      /// Weight uniformity [0.1%]
      /// </summary>
      [DataMember]
      public short Uniformity { get; set; }              
      /// <summary>
      /// Item Hour from
      /// </summary>
      [DataMember]
      public byte HourFrom { get; set; }                
      /// <summary>
      /// Item Hour to
      /// </summary>
      [DataMember]
      public byte HourTo { get; set; }
      /// <summary>
      /// Statistic sex
      /// </summary>
      [DataMember]
      public byte Sex { get; set; }                    
      /// <summary>
      /// Compressed histogram
      /// </summary>
      [DataMember]
      public IEnumerable<byte> Histogram { get; set; }              
   }

}
