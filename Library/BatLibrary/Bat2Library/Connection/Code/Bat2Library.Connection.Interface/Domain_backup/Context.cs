﻿using System.Runtime.Serialization;

namespace Connection.Interface.Domain
{
   /// <summary>
   /// Weighing context in device
   /// </summary>
   [DataContract]
   public class Context
   {
      /// <summary>
      /// Construct new empty context.
      /// </summary>
      public Context()
      {
         WeighingContext = new WeighingContext();
      }

      /// <summary>
      /// Running weighing context
      /// </summary>
      [DataMember(IsRequired = true, Order = 1)]
      public WeighingContext WeighingContext { get; set; }
      /// <summary>
      /// Current menu context
      /// </summary>
      [DataMember(IsRequired = false, Order = 2)]
      public MenuContext MenuContext { get; set; }
      /// <summary>
      /// Calibration context
      /// </summary>
      [DataMember(IsRequired = false, Order = 4)]
      public CalibrationContext CalibrationContext { get; set; }
   }
}
