﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Connection.Interface.Domain
{
   /// <summary>
   /// Single curve point
   /// </summary>
   [DataContract]
   public class CurvePoint
   {
      /// <summary>
      /// Y value of curve point
      /// </summary>
      public int ValueY { get; set; }
      /// <summary>
      /// X value of curve point
      /// </summary>
      public int ValueX { get; set; }
   }

   /// <summary>
   /// Single curve
   /// </summary>
   [DataContract]
   public class Curve
   {
      /// <summary>
      /// Construct new empty curve
      /// </summary>
      public Curve()
      {
         Points = new List<CurvePoint>();
      }
      /// <summary>
      /// Curve name
      /// </summary>
      public string Name { get; set; }
      /// <summary>
      /// Curve points
      /// </summary>
      public ICollection<CurvePoint> Points { get; set; }
   }
}