﻿using System.Runtime.Serialization;

namespace Connection.Interface.Domain
{
   /// <summary>
   /// Configuration for Modbus protocol
   /// </summary>
   [DataContract]
   public class ModbusOptions
   {
      /// <summary>
      /// Modbus mode
      /// </summary>
      [DataMember]
      public byte Mode { get; set; }
      /// <summary>
      /// Address of RS485 line to use
      /// </summary>
      [DataMember]
      public byte DeviceAddress { get; set; }
      /// <summary>
      /// Modbus protocol address
      /// </summary>
      [DataMember]
      public byte Address { get; set; }
      /// <summary>
      /// Baud rate of line
      /// </summary>
      [DataMember]
      public int BaudRate { get; set; }
      /// <summary>
      /// Data bits on line
      /// </summary>
      [DataMember]
      public byte DataBits { get; set; }
      /// <summary>
      /// Parity to be used
      /// </summary>
      [DataMember]
      public byte Parity { get; set; }
      /// <summary>
      /// Delay between request and reply
      /// </summary>
      [DataMember]
      public short ReplyDelay { get; set; }
   }
}