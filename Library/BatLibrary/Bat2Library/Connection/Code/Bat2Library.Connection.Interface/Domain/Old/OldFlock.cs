﻿using System;
using System.Collections.Generic;

namespace Bat2Library.Connection.Interface.Domain.Old
{
    public class OldFlock : BaseCurve
    {
        public OldFlockHeader Header { get; set; }

        // Rustova krivka pro samce
        public IEnumerable<OldCurvePoint> GrowthCurveMale { get; set; }

        // Rustova krivka pro samice
        public IEnumerable<OldCurvePoint> GrowthCurveFemale { get; set; }   
    }

    public class OldCurvePoint
    {
        // 0..CURVE_MAX_DAY (999)
        public UInt16 Day { get; set; }         

        // 0..CURVE_MAX_WEIGHT (0xFFFF)
        public UInt16 Weight { get; set; }      
    }  

}