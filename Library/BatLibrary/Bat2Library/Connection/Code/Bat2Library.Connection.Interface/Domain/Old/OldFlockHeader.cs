using System.ComponentModel.DataAnnotations;

namespace Bat2Library.Connection.Interface.Domain.Old
{
   public class OldFlockHeader
   {
      // Cislo hejna - pokud je hejno definovane, je rovno indexu hejna (0 - 9)
      [Range(0, 9)]
      public byte Number { get; set; }

      // Nazev hejna
      [MaxLength(8)]    
      public string Title { get; set; }

      // Zda se maji pouzivat rustove krivky nebo automaticke zjistovani cilove hmotnosti bez pouziti rustovych krivek  
      public bool UseCurves { get; set; }

      // Pokud krivky nepouziva, jsou zadane pocatecni hmotnosti ulozene jako 1. bod ve krivce
      // Flag, zda se ma pouzivat rozliseni na samce a samice
      public bool UseBothGenders { get; set; }

      // Umoznuje omezit ukladani az od zadane hodiny vcetne. Pokud je vyplneno FLOCK_TIME_LIMIT_EMPTY, neomezuje se          
      [Range(0, 23)]
      public byte WeighFrom { get; set; }

      [Range(0, 23)]
      // Umoznuje omezit ukladani pouze do zadane hodiny vcetne. Pokud je vyplneno FLOCK_TIME_LIMIT_EMPTY, neomezuje se            
      public byte WeighTill { get; set; }
   }
}