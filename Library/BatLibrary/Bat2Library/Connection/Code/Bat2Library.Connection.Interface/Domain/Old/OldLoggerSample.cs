﻿using System;

namespace Bat2Library.Connection.Interface.Domain.Old
{
    public class OldLoggerSample
    {
        // naraznik
        public byte Flag { get; set; }

        // vaha nebo hodina (vaha je na 17 bitu)
        public UInt16 Value { get; set; }
    }
}