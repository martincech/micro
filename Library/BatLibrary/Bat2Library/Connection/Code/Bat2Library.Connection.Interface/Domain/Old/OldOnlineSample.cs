﻿using System;

namespace Bat2Library.Connection.Interface.Domain.Old
{
    public class OldOnlineSample
    {
        // Naraznik
        public byte Flag { get; set; }

        // Hmotnost nebo hodina
        public UInt16 Value { get; set; }       
    }
}