﻿using System;

namespace Bat2Library.Connection.Interface.Domain.Old
{  
    public class OldStatistic
    {
        public Single XSuma { get; set; }
        public Single X2Suma { get; set; }
        public UInt16 Count { get; set; }

    }
}