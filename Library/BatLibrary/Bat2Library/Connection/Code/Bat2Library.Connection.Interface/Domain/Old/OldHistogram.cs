﻿using System;
using System.Collections.Generic;

namespace Bat2Library.Connection.Interface.Domain.Old
{
    public class OldHistogram
    {
        // Stredni hodnota, kolem ktere se histogram vytvari - je treba zadat pri inicializaci
        public Int32 Center { get; set; }

        // Krok histogramu - je treba zadat pri inicializaci         
        public Int32 Step { get; set; }  

        public IEnumerable<UInt16> Slot { get; set; }
    }
}