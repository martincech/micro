﻿namespace Bat2Library.Connection.Interface.Domain.Old
{
    // Typ automatickeho zjistovani cilove hmotnosti
    public enum TAutoMode : byte
    {
        AUTOMODE_WITHOUT_GAIN,    // Cilova hmotnost se vypocte jako prumer ze vcerejska
        AUTOMODE_WITH_GAIN,       // Cilova hmotnost se vypocte jako prumer + denni prirustek ze vcerejska
        _AUTOMODE_COUNT
    }

    // Rychlost komunikace RS-485 - pouziva se pouze pri editaci v menu
    public enum TSpeed : byte
    {
        RS485_SPEED_1200,
        RS485_SPEED_2400,
        RS485_SPEED_4800,
        RS485_SPEED_9600,
        RS485_SPEED_19200,
        RS485_SPEED_38400,
        RS485_SPEED_57600,
        RS485_SPEED_115200,
        _RS485_SPEED_COUNT
    }

    public enum THwVersion : byte
    {
        HW_VERSION_GSM,           // GSM modul
        HW_VERSION_DACS_LW1,      // Emulace vahy LW1 pripojene k Dacs ACS pres RS-485
        HW_VERSION_MODBUS,        // Modbus pres RS-485
        HW_VERSION_DACS_ACS6,     // Vaha pripojena k Dacs ACS6 pres RS-485
        _HW_VERSION_COUNT
    }


    public class OldConstats
    {

        public const int FLOCKS_MAX_COUNT = 10;          // Pocet hejn, ktere muze zadat
        //typedef TFlock TFlocks[FLOCKS_MAX_COUNT];       // Cisla hejn odpovidaji pozici, tj. na pozici 0 je hejno cislo 0, na pozici 1 hejno cislo 1 atd.

        // Parametry GSM
        public const int GSM_NUMBER_MAX_LENGTH = 15;        // Maximalni delka telefonniho cisla bez zakoncovaci nuly, pozor, musi byt mensi nez GSM_MAX_NUMBER v Hardware.h
        public const int GSM_NUMBER_MAX_COUNT = 5;         // Maximalni pocet definovanych telefonnich cisel

        public const byte FL_ARCHIVE_EMPTY = 0xFE;
        public const byte FL_ARCHIVE_FULL = 0xFF;

        public const int HISTOGRAM_SLOTS = 39;       // pocet sloupcu - sude i liche cislo (max.254)

        public const byte LG_SAMPLE_EMPTY = 0x7F;        // nenaplnene FIFO
        public const byte LG_SAMPLE_FULL = 0xFF;        // naplnene FIFO
        public const byte LG_SAMPLE_VALUE = 0x20;        // zaznam hodnoty
        public const byte LG_SAMPLE_GENDER = 0x10;        // bit pohlavi
        public const byte LG_SAMPLE_WEIGHT_MSB = 0x01;        // 17. bit hmotnosti ve flagu

        public const uint LG_SAMPLE_MASK_WEIGHT = 0x01FFFF;    // Hmotnost je ulozena ve spodnich 17bit, MSB byte je flag (hmotnost tedy zabira 1 spodni bit z flagu)
        public const uint LG_SAMPLE_MASK_HI_WEIGHT = 0x010000;    // 17. bit hmotnosti
        public const ushort LG_SAMPLE_MASK_HOUR = 0x001F;      // Hodina je ulozena ve spodnich 5 bitech

        // zaznam jednotlivych vazeni :
        public const int FL_LOGGER_SAMPLES = 1801;
        public const int FL_LOGGER_SAMPLE_SIZE = 3;                       // 3 bajty

        // Naraznikove znaky :
        public const byte ON_SAMPLE_EMPTY = 0x7F;        // nenaplnene FIFO
        public const byte ON_SAMPLE_FULL = 0xFF;        // naplnene FIFO
        public const byte ON_SAMPLE_WEIGHT = 0x08;        // zaznam hmotnosti
        public const byte ON_SAMPLE_SAVED = 0x20;        // Vaha by prave ulozila vzorek do pameti
        public const byte ON_SAMPLE_STABLE = 0x10;        // Bit ustalene hmotnosti
        public const byte ON_SAMPLE_SIGN = 0x04;        // Znamenko hmotnosti
        public const byte ON_SAMPLE_WEIGHT_MSB = 0x03;        // 17. a 18. bit hmotnosti ve flagu

        public const uint ON_SAMPLE_MASK_WEIGHT = 0x03FFFF;    // Hmotnost je ulozena ve spodnich 18bit, MSB byte je flag (hmotnost tedy zabira 2 spodni bity z flagu)
        public const ushort ON_SAMPLE_MASK_HOUR = 0x001F;      // Hodina je ulozena ve spodnich 5 bitech
      
        // zaznam jednotlivych vazeni :
        public const int FL_ONLINE_SAMPLES = 694265;
        public const int FL_ONLINE_SAMPLE_SIZE = 3;    // 3 bajty

        // konfiguracni sekce :
        public const int FL_CONFIG_BASE = 0;
        public const int FL_CONFIG_SIZE = 4096;

        // archiv statistik & histogramu :
        public const int FL_ARCHIVE_BASE = (FL_CONFIG_BASE + FL_CONFIG_SIZE);
        public const int FL_ARCHIVE_DAY_SIZE = 5614;               // sizeof( TArchiveDailyInfo) velikost polozky
        public const int FL_ARCHIVE_DAYS = 371;                                         // pocet polozek
        public const int FL_ARCHIVE_SIZE = (FL_ARCHIVE_DAY_SIZE * FL_ARCHIVE_DAYS);      // celkem bytu (2082794), konec pameti 2086890

        // Online mereni :
        public const int FL_ONLINE_BASE = FL_ARCHIVE_BASE;                              // Stejne jako archiv
        public const int FL_ONLINE_SIZE = (FL_ONLINE_SAMPLE_SIZE * FL_ONLINE_SAMPLES);  // celkem bytu (2082795), konec pameti 2086891
    }
}
