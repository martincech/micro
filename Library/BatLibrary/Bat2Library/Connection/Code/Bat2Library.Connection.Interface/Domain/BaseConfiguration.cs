﻿namespace Bat2Library.Connection.Interface.Domain.Old
{
   public class BaseConfiguration<T>
      where T : BaseVersionInfo
   {
      /// <summary>
      /// Info about device version, <see cref="BaseVersionInfo"/>
      /// </summary>
      public virtual T VersionInfo { get; set; }

      public bool IsValid
      {
         get { return VersionInfo != null && VersionInfo.IsValid; }
      }
   }
}
