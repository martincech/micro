using System.Runtime.Serialization;

namespace Bat2Library.Connection.Interface.Domain
{
   /// <summary>
   /// Display settings
   /// </summary>
   [DataContract]
   public class DisplayConfiguration
   {
      #region Private fields

       #endregion


      /// <summary>
      /// Contrast of display
      /// </summary>
      [DataMember]
      public byte Contrast { get; set; }

       /// <summary>
      /// Mode of operation
      /// </summary>
      [DataMember]
      public DisplayModeE Mode { get; set; }

       /// <summary>
      /// Duration of backlight
      /// </summary>
      [DataMember]
      public short BacklightDuration { get; set; }

       /// <summary>
      /// Intensity of backlight
      /// </summary>
      [DataMember]
      public byte BacklightIntensity { get; set; }

       /// <summary>
      /// Mode of backlight operation
      /// </summary>
      [DataMember]
      public BacklightModeE BacklightMode { get; set; }

       /// <summary>
      /// Power save mode enabled?
      /// </summary>
      [DataMember]
      public bool SavePower { get; set; }
   }
}