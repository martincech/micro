﻿using System.Runtime.Serialization;

namespace Bat2Library.Connection.Interface.Domain
{
   /// <summary>
   /// Configuration for Modbus protocol
   /// </summary>
   [DataContract]
   public class ModbusOptions
   {
      #region Private fields

       #endregion


      /// <summary>
      /// Modbus mode
      /// </summary>
      [DataMember]
      public ModbusModeE Mode { get; set; }

       /// <summary>
      /// Modbus protocol address
      /// </summary>
      [DataMember]
      public byte Address { get; set; }

       /// <summary>
      /// Baud rate of line
      /// </summary>
      [DataMember]
      public int BaudRate { get; set; }

      /// <summary>
      /// Parity to be used
      /// </summary>
      [DataMember]
      public ModbusParityE Parity { get; set; }
   }
}