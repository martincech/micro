﻿using System.Runtime.Serialization;

namespace Bat2Library.Connection.Interface.Domain
{
   /// <summary>
   /// Dacs protocol options
   /// </summary>
   [DataContract]
   public class DacsOptions
   {
      #region Private fields

       #endregion


      /// <summary>
      /// Address of device
      /// </summary>
      [DataMember]
      public byte Address { get; set; }

       /// <summary>
      /// Protocol version
      /// </summary>
      [DataMember]
      public DacsVersionE Version { get; set; }

       /// <summary>
      /// Address of RS485 line to use
      /// </summary>
      [DataMember]
      public byte DeviceAddress { get; set; }
   }
}