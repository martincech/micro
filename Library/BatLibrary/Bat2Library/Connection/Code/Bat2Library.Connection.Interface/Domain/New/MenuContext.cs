﻿using System.Runtime.Serialization;

namespace Bat2Library.Connection.Interface.Domain
{
   /// <summary>
   /// Menu context
   /// </summary>
   [DataContract]
   public class MenuContext
   {
      #region Private fields

       #endregion


      /// <summary>
      /// Is menu switched off?
      /// </summary>
      [DataMember]
      public bool SwitchedOff { get; set; }
   }
}