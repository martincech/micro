using System;
using System.Runtime.Serialization;

namespace Bat2Library.Connection.Interface.Domain
{
   /// <summary>
   /// Configuration of weighing
   /// </summary>
   [DataContract]
   public class WeighingConfiguration
   {
      #region Private fields

       #endregion


      /// <summary>
      /// Name of configuration
      /// </summary>
      [DataMember]
      public string Name { get; set; }

       /// <summary>
      /// Flock name
      /// </summary>
      [DataMember]
      public string Flock { get; set; }

       /// <summary>
      /// Mask of menu items
      /// </summary>
      [DataMember]
      public WeighingConfigurationMenuMaskE MenuMask { get; set; }

       /// <summary>
      /// First day of weighing
      /// </summary>
      [DataMember]
      public short InitialDay { get; set; }

       /// <summary>
      /// Index to correction curve list, <see cref="Curve"/>
      /// </summary>
      [DataMember]
      public int CorrectionCurveIndex { get; set; }

       /// <summary>
      /// Index to predefined weighings list, this weighing index inside predefined list
      /// </summary>
      [DataMember]
      public int PredefinedIndex { get; set; }

       /// <summary>
      /// Time to start a new day
      /// </summary>
      [DataMember]
      public DateTime DayStart { get; set; }

       /// <summary>
      /// initial weight of male birds
      /// </summary>
      [DataMember]
      public Int32 MaleInitialWeight { get; set; }

       /// <summary>
      /// Index to growth curve list, <see cref="Curve"/>
      /// </summary>
      [DataMember]
      public int MaleGrowthCurveIndex { get; set; }


       /// <summary>
      /// Initial weight of female birds
      /// </summary>
      [DataMember]
      public Int32 FemaleInitialWeight { get; set; }

       /// <summary>
      /// Index to growth curve list, <see cref="Curve"/>
      /// </summary>
      [DataMember]
      public int FemaleGrowthCurveIndex { get; set; }

       /// <summary>
      /// Enable online adjustment?
      /// </summary>
      [DataMember]
      public OnlineAdjustmentE AdjustTargetWeights { get; set; }

       /// <summary>
      /// Sex configuration
      /// </summary>
      [DataMember]
      public SexE Sex { get; set; }

       /// <summary>
      /// Diferentiate sex?
      /// </summary>
      [DataMember]
      public SexDifferentiationE SexDifferentiation { get; set; }

       /// <summary>
      /// Growth parameters setting
      /// </summary>
      [DataMember]
      public PredictionGrowthE Growth { get; set; }

       /// <summary>
      /// Mode of weighing
      /// </summary>
      [DataMember]
      public byte Mode { get; set; }

       /// <summary>
      /// Filter value to be used
      /// </summary>
      [DataMember]
      public byte Filter { get; set; }

       /// <summary>
      /// Platform stabilization time
      /// </summary>
      [DataMember]
      public byte StabilizationTime { get; set; }

       /// <summary>
      /// Platform stabilization range
      /// </summary>
      [DataMember]
      public byte StabilizationRange { get; set; }

       /// <summary>
      /// Step mode
      /// </summary>
      [DataMember]
      public PlatformStepModeE Step { get; set; }

       /// <summary>
      /// Margin above for male birds
      /// </summary>
      [DataMember]
      public byte MaleMarginAbove { get; set; }

       /// <summary>
      /// Margin below for male birds
      /// </summary>
      [DataMember]
      public byte MaleMarginBelow { get; set; }

       /// <summary>
      /// Margin above for female birds
      /// </summary>
      [DataMember]
      public byte FemaleMarginAbove { get; set; }

       /// <summary>
      /// Margin below for female birds
      /// </summary>
      [DataMember]
      public byte FemaleMarginBelow { get; set; }

       /// <summary>
      /// Statistics short period
      /// </summary>
      [DataMember]
      public byte ShortPeriod { get; set; }

       /// <summary>
      /// Statistics type of short period
      /// </summary>
      [DataMember]
      public StatisticShortTypeE ShortType { get; set; }

       /// <summary>
      /// Range of uniformity
      /// </summary>
      [DataMember]
      public byte UniformityRange { get; set; }

       /// <summary>
      /// Step in histogram
      /// </summary>
      [DataMember]
      public short HistogramStep { get; set; }

       /// <summary>
      /// Range of histogram
      /// </summary>
      [DataMember]
      public byte HistogramRange { get; set; }

       /// <summary>
      /// Mode of histogram to be used
      /// </summary>
      [DataMember]
      public HistogramModeE HistogramMode { get; set; }

       /// <summary>
      /// Plan weighing?
      /// </summary>
      [DataMember]
      public bool Planning { get; set; }

       /// <summary>
      /// Index to plan list, <see cref="WeighingPlan"/>
      /// </summary>
      [DataMember]
      public int WeighingPlanIndex { get; set; }
   }
}