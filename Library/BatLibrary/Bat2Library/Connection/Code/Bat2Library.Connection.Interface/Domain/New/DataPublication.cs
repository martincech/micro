﻿using System;
using System.Runtime.Serialization;

namespace Bat2Library.Connection.Interface.Domain
{
    /// <summary>
    /// SmsGate protocol options
    /// </summary>
    [DataContract]
    public class DataPublication
    {
        #region Private fields

        #endregion

        public DataPublicationInterfaceE @Interface { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Url { get; set; }

        public ushort StartFromDay { get; set; }

        public ushort Period { get; set; }

        public ushort AccelerateFromDay { get; set; }

        public ushort AcceleratedPeriod { get; set; }

        public DateTime SendAt { get; set; }
    }
}