﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Bat2Library.Connection.Interface.Domain.Old;

namespace Bat2Library.Connection.Interface.Domain
{
   /// <summary>
   /// All data a single BAT2 device contains.
   /// </summary>
   [DataContract]
   public class Bat2DeviceData : BaseBat2DeviceData<VersionInfo, Configuration>
   {
      #region Private fields

      #endregion

      /// <summary>
      /// Construct empty data structure, <see cref="DataMember"/>s 
      /// required properties are initialized, others are null.
      /// </summary>
      public Bat2DeviceData()
      {
         Configuration = new Configuration();
      }

      /// <summary>
      /// Device context
      /// </summary>
      [DataMember(IsRequired = false)]
      public virtual Context Context { get; set; }

      /// <summary>
      /// Archive of statistics
      /// </summary>
      [DataMember(IsRequired = false)]
      public virtual IEnumerable<ArchiveItem> Archive { get; set; }

      /// <summary>
      /// List of growth curves
      /// </summary>
      [DataMember(IsRequired = false)]
      public virtual IEnumerable<Curve> GrowthCurves { get; set; }

      /// <summary>
      /// List of  correction curves
      /// </summary>
      [DataMember(IsRequired = false)]
      public virtual IEnumerable<Curve> CorrectionCurves { get; set; }

      /// <summary>
      /// List of contacts
      /// </summary>
      [DataMember(IsRequired = false)]
      public virtual IEnumerable<Contact> Contacts { get; set; }

      /// <summary>
      /// List of weighing plans
      /// </summary>
      [DataMember(IsRequired = false)]
      public virtual IEnumerable<WeighingPlan> WeighingPlans { get; set; }

      /// <summary>
      /// List of predefined weighing configurations
      /// </summary>
      [DataMember(IsRequired = false)]
      public virtual IEnumerable<WeighingConfiguration> PredefinedWeighings { get; set; }

      #region Implementation of BaseBat2DeviceData

      public bool IsValid
      {
         get
         {
            return Configuration != null && Configuration.VersionInfo != null &&
                   Configuration.VersionInfo.IsValid;
         }
      }

      #endregion
   }
}
