﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Bat2Library.Connection.Interface.Domain.Old;

namespace Bat2Library.Connection.Interface.Domain
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public sealed class Configuration : BaseConfiguration<VersionInfo>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public Configuration()
        {
            VersionInfo = new VersionInfo();
            DeviceInfo = new DeviceInfo();
        }

       
        /// <summary>
        /// Info about current device, <see cref="DeviceInfo"/>
        /// </summary>
        [DataMember(IsRequired = true, Order = 2)]
        public DeviceInfo DeviceInfo { get; set; }

        /// <summary>
        /// Country and Language settings in device, <see cref="Country"/>
        /// </summary>
        [DataMember(IsRequired = true, Order = 3)]
        public Country Country { get; set; }

        /// <summary>
        /// Weighing units selection, <see cref="WeightUnits"/>
        /// </summary>
        [DataMember(IsRequired = true, Order = 4)]
        public WeightUnits WeightUnits { get; set; }

        /// <summary>
        /// Configuration of display, <see cref="DisplayConfiguration"/>
        /// </summary>
        [DataMember(IsRequired = true, Order = 6)]
        public DisplayConfiguration DisplayConfiguration { get; set; }

        /// <summary>
        /// Configuration of current weighing, <see cref="WeighingConfiguration"/>
        /// </summary>
        [DataMember(IsRequired = true, Order = 7)]
        public WeighingConfiguration WeighingConfiguration { get; set; }

        /// <summary>
        /// Configuration of RS485 lines, <see cref="Rs485Options"/>
        /// </summary>
        [DataMember(IsRequired = true, Order = 8)]
        public List<Rs485Options> Rs485Options { get; set; }

        /// <summary>
        /// Configuration of modbus line, <see cref="ModbusOptions"/>
        /// </summary>
        [DataMember(IsRequired = true, Order = 9)]
        public ModbusOptions ModbusOptions { get; set; }

        /// <summary>
        /// Configuration of megavi line, <see cref="MegaviOptions"/>
        /// </summary>
        [DataMember(IsRequired = true, Order = 10)]
        public MegaviOptions MegaviOptions { get; set; }


        /// <summary>
        /// Configuration of dacs line, <see cref="DacsOptions"/>
        /// </summary>
        [DataMember(IsRequired = true, Order = 11)]
        public DacsOptions DacsOptions { get; set; }

        /// <summary>
        /// Configuration of GSM module, <see cref="GsmMessage"/>
        /// </summary>
        [DataMember(IsRequired = true, Order = 12)]
        public GsmMessage GsmMessage { get; set; }

        [DataMember(IsRequired = true, Order = 13)]
        public DataPublication DataPublication { get; set; }

        [DataMember(IsRequired = true, Order = 14)]
        public CellularData CellularData { get; set; }

        [DataMember(IsRequired = true, Order = 15)]
        public Ethernet Ethernet { get; set; }
    }
}
