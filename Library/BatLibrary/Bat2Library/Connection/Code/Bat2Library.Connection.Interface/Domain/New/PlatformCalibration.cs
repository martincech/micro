﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bat2Library.Connection.Interface.Domain
{
   /// <summary>
   /// Calibration of platform
   /// </summary>
   [DataContract]
   public class PlatformCalibration
   {
      #region Private fields

       #endregion


      /// <summary>
      /// Calibration points
      /// </summary>
      [DataMember]
      public List<int> Points { get; set; }

       /// <summary>
      /// Delay
      /// </summary>
      [DataMember]
      public short Delay { get; set; }

       /// <summary>
      /// Duration
      /// </summary>
      [DataMember]
      public short Duration { get; set; }
   }
}