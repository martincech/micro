﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bat2Library.Connection.Interface.Domain
{
   /// <summary>
   /// Context of calibration
   /// </summary>
   [DataContract]
   public class CalibrationContext
   {
      #region Private fields

       #endregion


      /// <summary>
      /// Last calibration time
      /// </summary>
      [DataMember]
      public DateTime Time { get; set; }

       /// <summary>
      /// range calibration
      /// </summary>
      [DataMember]
      public IEnumerable<Int32> RawPoints { get; set; }

       /// <summary>
      /// physical range
      /// </summary>
      [DataMember]
      public IEnumerable<Int32> WeightPoints { get; set; }

       /// <summary>
      /// reversed bridge polarity
      /// </summary>
      [DataMember]
      public bool Inversion { get; set; }
   }
}