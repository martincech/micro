﻿using System.Runtime.Serialization;

namespace Bat2Library.Connection.Interface.Domain
{
    /// <summary>
    /// Error message data
    /// </summary>
    [DataContract]
    public class ErrorMessage
    {
        public ErrorMessage(string message, string description, string exceptionType) 
        {
            Message = message;
            Description = description;
            ExceptionType = exceptionType;
        }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string ExceptionType { get; set; }
    }
}
