﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bat2Library.Connection.Interface.Domain
{
   /// <summary>
   /// Single curve point
   /// </summary>
   [DataContract]
   public class CurvePoint
   {
      #region Private fields

       #endregion


      /// <summary>
      /// Y value of curve point
      /// </summary>
      [DataMember]
      public int ValueY { get; set; }

       /// <summary>
      /// X value of curve point
      /// </summary>
      [DataMember]
      public int ValueX { get; set; }
   }

   /// <summary>
   /// Single curve
   /// </summary>
   [DataContract]
   public class Curve
   {
      #region Private fields

       #endregion


      /// <summary>
      /// Construct new empty curve
      /// </summary>
      public Curve()
      {
         Points = new List<CurvePoint>();
      }

      /// <summary>
      /// Curve name
      /// </summary>
      [DataMember]
      public string Name { get; set; }

       /// <summary>
      /// Curve points
      /// </summary>
      [DataMember]
      public ICollection<CurvePoint> Points { get; set; }
   }
}