﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Threading;
using Bat2Library.Connection.Interface.Domain.Old;
using Bat2Library.Connection.Manager;
using Connection.Bat2.Annotations;

namespace Connection.Bat2
{
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow : INotifyPropertyChanged
   {

      public MainWindow()
      {
         Devices = new ObservableCollection<IBaseBat2DeviceData>();
         ThreadPool.QueueUserWorkItem(param =>
         {
            manager = Bat2ConnectionManager.Instance;
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (Action) (() =>
            {

               foreach (var connectedDevice in manager.ConnectedDevices)
               {
                  Devices.Add(connectedDevice);
               }
            }));

            manager.DeviceConnected += manager_DeviceConnected;
            manager.DeviceDisconnected += manager_DeviceDisconnected;
         });

         InitializeComponent();
      }


      private void Window_Loaded(object sender, System.Windows.RoutedEventArgs e)
      {
      }

      void manager_DeviceConnected(object sender, IBaseBat2DeviceData e)
      {
         Dispatcher.BeginInvoke(DispatcherPriority.Background, (Action) (() => Devices.Add(e)));
      }

      void manager_DeviceDisconnected(object sender, IBaseBat2DeviceData e)
      {
         Dispatcher.BeginInvoke(DispatcherPriority.Background, (Action) (() => Devices.Remove(e)));
      }

      private Bat2ConnectionManager manager;
      private ObservableCollection<IBaseBat2DeviceData> devices;

      public ObservableCollection<IBaseBat2DeviceData> Devices
      {
         get { return devices; }
         set
         {
            if (Equals(value, devices)) return;
            devices = value;
            OnPropertyChanged();
         }
      }

      public event PropertyChangedEventHandler PropertyChanged;

      [NotifyPropertyChangedInvocator]
      protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
      {
         var handler = PropertyChanged;
         if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
      }
   }
}
