﻿using System;
using System.IO;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;
using Connection.Manager.Tests.Bat2Old.Sockets.BaseClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.Bat2Old.Sockets.UsbSocket
{
   [TestClass]
   public class UsbBat2FakeDataReadTests : Bat2OldDataReadTests
   {
      private const string FILE_NAME = "tmpFile.txt";
      private const string MEMORY_FILE_NAME_V111 = "Bat2Old_v1.11_scale.bin";
      private const string MEMORY_FILE_NAME_V150 = "Bat2Old_v1.50_scale.bin";

      [TestInitialize]
      public void Init()
      {
         Assert.IsTrue(File.Exists(MEMORY_FILE_NAME_V111));
         if (File.Exists(MEMORY_FILE_NAME_V111 + "tmp"))
         {
            File.Delete(MEMORY_FILE_NAME_V111 + "tmp");
         }
         File.Copy(MEMORY_FILE_NAME_V111, MEMORY_FILE_NAME_V111 + "tmp");
         
         Assert.IsTrue(File.Exists(MEMORY_FILE_NAME_V150));
         if (File.Exists(MEMORY_FILE_NAME_V150 + "tmp"))
         {
            File.Delete(MEMORY_FILE_NAME_V150 + "tmp");
         }
         File.Copy(MEMORY_FILE_NAME_V150, MEMORY_FILE_NAME_V150 + "tmp");
      }

      [TestCleanup]
      public void Clean()
      {
         File.Delete(MEMORY_FILE_NAME_V111);
         File.Copy(MEMORY_FILE_NAME_V111 + "tmp", MEMORY_FILE_NAME_V111);
         File.Delete(MEMORY_FILE_NAME_V150);
         File.Copy(MEMORY_FILE_NAME_V150 + "tmp", MEMORY_FILE_NAME_V150);
      }

      protected override void LoadMethod_ReturnNull_WhenNotConnected<T>(Func<IBat2OldDataReader, T> loadAction)
      {
         using (var file = new FileStream(FILE_NAME, FileMode.Create, FileAccess.ReadWrite))
         {
            using (var socket = new Bat2OldUsbSocket(file))
            {
               using (var b2D = new UsbBat2OldDevice(socket))
               {
                  LoadMethod_ReturnNull_WhenNotConnected(loadAction, b2D);
               }
            }
         }
      }

      protected override void LoadMethod_ReturnNonNull_WhenConnected<T>(Func<IBat2OldDataReader, T> loadAction)
      {
         // simulated device v1.11
         using (var file = new FileStream(MEMORY_FILE_NAME_V111, FileMode.Open, FileAccess.Read))
         {
            Assert.AreNotEqual(0, file.Length);
            using (var socket = new Bat2OldUsbSocket(file))
            {
               using (var b2D = new UsbBat2OldDevice(socket))
               {
                  LoadMethod_ReturnNonNull_WhenConnected(loadAction, b2D);
               }
            }
         }

         // simulated device 1.50
         using (var file = new FileStream(MEMORY_FILE_NAME_V150, FileMode.Open, FileAccess.Read))
         {
            Assert.AreNotEqual(0, file.Length);
            using (var socket = new Bat2OldUsbSocket(file))
            {
               using (var b2D = new UsbBat2OldDevice(socket))
               {
                  LoadMethod_ReturnNonNull_WhenConnected(loadAction, b2D);
               }
            }
         }
      }
   }
}