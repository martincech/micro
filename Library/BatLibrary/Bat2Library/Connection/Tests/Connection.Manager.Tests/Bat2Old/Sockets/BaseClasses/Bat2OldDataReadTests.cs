using System;
using System.Linq;
using Bat2Library.Connection.Interface.Domain.Old;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.Bat2Old.Sockets.BaseClasses
{
   public abstract class Bat2OldDataReadTests
   {
      protected abstract void LoadMethod_ReturnNull_WhenNotConnected<T>(Func<IBat2OldDataReader, T> loadAction)
         where T : class;

      protected abstract void LoadMethod_ReturnNonNull_WhenConnected<T>(Func<IBat2OldDataReader, T> loadAction)
         where T : class;

      #region Data transfers

      #region Config

      [TestMethod]
      public void Config_Load_ReturnNull_WhenNotConnected()
      {
         LoadMethod_ReturnNull_WhenNotConnected(reader => reader.LoadConfig());
      }

      [TestMethod]
      public void Config_Load_ReturnNonNull_WhenConnected()
      {
         LoadMethod_ReturnNonNull_WhenConnected(reader => reader.LoadConfig());
      }

      #endregion

      #region Archive

      [TestMethod]
      public void Archive_Load_ReturnNull_WhenNotConnected()
      {
         LoadMethod_ReturnNull_WhenNotConnected(reader => reader.LoadArchive());
      }

      [TestMethod]
      public void Archive_Load_ReturnNonNull_WhenConnected()
      {
         LoadMethod_ReturnNonNull_WhenConnected(reader => reader.LoadArchive());
      }

      #endregion

      #endregion

      #region Load methods

      internal static void LoadMethod_ReturnNull_WhenNotConnected<T>(Func<IBat2OldDataReader, T> loadAction,
         Bat2OldDevice b2D)
         where T : class
      {
         using (var rw = new Bat2OldDataRW(b2D))
         {
            Assert.IsNull(loadAction(rw));
         }
      }

      internal static void LoadMethod_ReturnNonNull_WhenConnected<T>(Func<IBat2OldDataReader, T> loadAction,
         Bat2OldDevice b2D)
         where T : class
      {
         T loadResult;
         using (var rw = new Bat2OldDataRW(b2D))
         {
            loadResult = loadAction(rw);
         }
         Assert.IsNotNull(loadResult);
         T dPropValue;
         if (typeof (T) == typeof (OldBat2DeviceData))
         {
            dPropValue = b2D.DeviceData as T;
         }
         else
         {
            var dProp =
               b2D.DeviceData.GetType().GetProperties().Where(prop =>
                  prop.PropertyType == typeof (T) || prop.PropertyType.IsAssignableFrom(typeof (T))).ToList();
            Assert.IsNotNull(dProp);
            dPropValue =
               dProp.Select(prop => prop.GetMethod.Invoke(b2D.DeviceData, null) as T)
                  .FirstOrDefault(v => v == loadResult);
         }
         Assert.AreSame(loadResult, dPropValue, "Property on inner data not set to valid value!");
      }

      #endregion
   }
}