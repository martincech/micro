using System;
using System.Linq;
using Bat2Library.Connection.Interface.Domain;
using Bat2Library.Connection.Interface.Domain.Old;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ploeh.AutoFixture;

namespace Connection.Manager.Tests.Bat2Old.Sockets.BaseClasses
{
   public abstract class Bat2OldDataWriteTests
   {
      protected abstract void SaveMethod_ReturnFalse_WhenNotConnected<T>(Func<T, IBat2OldDataWriter, bool> saveAction)
         where T : class;

      protected abstract void SaveMethod_ReturnTrue_WhenConnected<T>(Func<T, IBat2OldDataWriter, bool> saveAction)
         where T : class;

      #region Config

      [TestMethod]
      public void Config_Save_ReturnFalse_WhenNotConnected()
      {
         SaveMethod_ReturnFalse_WhenNotConnected<OldConfiguration>(
            (data, writer) => writer.SaveConfig(data));
      }

      [TestMethod]
      public void Config_Save_ReturnTrue_WhenConnected()
      {
         SaveMethod_ReturnTrue_WhenConnected<OldConfiguration>(
            (data, writer) => writer.SaveConfig(data));
      }

      #endregion

      #region Save methods

      internal static void SaveMethod_ReturnFalse_WhenNotConnected<T>(Func<T, IBat2OldDataWriter, bool> saveAction,
         Bat2OldDevice b2D) where T : class
      {
         using (var rw = new Bat2OldDataRW(b2D))
         {
            var gen = new Fixture();
            gen.CustomizeData();
            var data = gen.Create<T>();
            Assert.IsFalse(saveAction(data, rw));
         }
      }

      internal static void SaveMethod_ReturnTrue_WhenConnected<T>(Func<T, IBat2OldDataWriter, bool> saveAction,
         Bat2OldDevice b2D) where T : class
      {
         using (var rw = new Bat2OldDataRW(b2D))
         {
            var allData = rw.LoadAll();
            Assert.IsNotNull(allData);
            T dPropValue;
            var dProp =
               typeof (OldBat2DeviceData).GetProperties().FirstOrDefault(prop =>
                  prop.PropertyType == typeof (T) || prop.PropertyType.IsAssignableFrom(typeof (T)));
            if (typeof (T) == typeof (OldBat2DeviceData))
            {
               dPropValue = allData as T;
            }
            else
            {
               Assert.IsNotNull(dProp);
               dPropValue = dProp.GetMethod.Invoke(allData, null) as T;
            }
            Assert.IsNotNull(dPropValue);
            Assert.IsTrue(saveAction(dPropValue, rw));

            T newDpropValue;
            if (typeof (T) == typeof (OldBat2DeviceData))
            {
               newDpropValue = allData as T;
            }
            else
            {
               Assert.IsNotNull(dProp);
               newDpropValue = dProp.GetMethod.Invoke(b2D.DeviceData, null) as T;
            }
            Assert.AreSame(newDpropValue, dPropValue, "Property on inner data not set to valid value!");
         }
      }

      #endregion
   }
}