﻿using System.Linq;
using System.Threading;
using Bat2Library.Connection.Interface.Domain;
using Bat2Library.Connection.Interface.Domain.Old;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager;
using Connection.Manager.Tests.Bat2New.Managers.FlashManager;
using Connection.Manager.Tests.Bat2New.Managers.UsbManager;
using Connection.Manager.Tests.Bat2Old.Managers.UsbManager;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.OverallManager
{
   [TestClass]
   public abstract class BaseOverallBat2ConnectionManagerTests<S, T, D, R, W, C> :
      BaseIBat2DeviceManagerTests<S, T, D, R, W, C>
      where S : BaseVersionInfo
      where T : BaseConfiguration<S>
      where D : BaseBat2DeviceData<S, T>
      where R : IBat2DataReader<S, T>
      where W : IBat2DataWriter<S, T>
      where C : IBat2ActionCommander
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      protected BaseOverallBat2ConnectionManagerTests(IBat2DeviceManager<S, T, D, R, W, C> manager)
         : base(manager)
      {
         Thread.Sleep(5000);
      }

      [TestInitialize]
      public void Init()
      {
         var flashTests = new FlashBat2DeviceManagerTests();
         flashTests.Init();
      }

      [TestCleanup]
      public void Clean()
      {
         var flashTests = new FlashBat2DeviceManagerTests();
         flashTests.Clean();
      }

      [TestMethod]
      public void Overall_HasSameDevices_As_Singles()
      {
         var bat2NewFlashTests = new FlashBat2DeviceManagerTests();
         var bat2NewUsbTests = new UsbBat2DeviceManagerTests();
         var bat2OldUsbTests = new UsbBat2OldDeviceManagerTests();

         var manager = Manager;
         var bat2NewFlashManager = bat2NewFlashTests.Manager;
         var bat2NewUsbManager = bat2NewUsbTests.Manager;
         var bat2OldUsbManager = bat2OldUsbTests.Manager;

         Assert.AreEqual(manager.ConnectedDevices.Count(),
            bat2NewFlashManager.ConnectedDevices.Count()
            + bat2NewUsbManager.ConnectedDevices.Count()
            + bat2OldUsbManager.ConnectedDevices.Count()
            );
         foreach (var connectedDevice in manager.ConnectedDevices)
         {
            Assert.IsTrue(
               (connectedDevice is Bat2DeviceData && (
                  bat2NewFlashManager.ConnectedDevices.Contains(connectedDevice as Bat2DeviceData)
                  || bat2NewUsbManager.ConnectedDevices.Contains(connectedDevice as Bat2DeviceData)
                  ))
               ||
               (connectedDevice is OldBat2DeviceData &&
                (bat2OldUsbManager.ConnectedDevices.Contains(connectedDevice as OldBat2DeviceData)))
               );
         }
      }

      private Bat2ConnectionManager ConManager
      {
         get
         {
            Assert.IsInstanceOfType(Manager, typeof (Bat2ConnectionManager));
            return Manager as Bat2ConnectionManager;
         }
      }

      [TestMethod]
      public void SameCountBothInterfaces()
      {
         Assert.AreEqual(ConManager.ConnectedDevices.Count(),
            (Manager as IBat2OldDeviceManager).ConnectedDevices.Count()
            + (Manager as IBat2NewDeviceManager).ConnectedDevices.Count()
            );
      }
   }
}