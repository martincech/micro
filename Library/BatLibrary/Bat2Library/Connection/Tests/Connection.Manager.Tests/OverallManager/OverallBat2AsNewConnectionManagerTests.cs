﻿using Bat2Library.Connection.Interface.Domain;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.OverallManager
{
   [TestClass]
   public class OverallBat2AsNewConnectionManagerTests :
      BaseOverallBat2ConnectionManagerTests<
         VersionInfo,
         Configuration,
         Bat2DeviceData,
         IBat2NewDataReader,
         IBat2NewDataWriter,
         IBat2ActionCommander>
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public OverallBat2AsNewConnectionManagerTests()
         : base(Bat2ConnectionManager.Instance)
      {
      }
   }
}