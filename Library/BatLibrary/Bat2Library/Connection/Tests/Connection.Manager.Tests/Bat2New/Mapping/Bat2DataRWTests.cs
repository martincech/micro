﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Bat2Library.Connection.Interface.Domain;
using Bat2Library.Connection.Manager.Native;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.Kernel;
using Utilities.Extensions;

namespace Connection.Manager.Tests.Bat2New.Mapping
{
   [TestClass]
   public class Bat2DataRwTests
   {
      private List<Type> skipTypeList; // List of types to skip
      private Fixture fixture;
      private List<Type> typeList; // List of types in Bat2Library.Connection.Interface.Domain
      private List<Type> addType; // List of adede types(collections) to test

      [TestInitialize]
      public void Init()
      {
         typeList = Assembly.Load("Bat2Library.Connection.Interface").GetTypes()
            .Where(x => x.Namespace == @"Bat2Library.Connection.Interface.Domain").ToList();

         skipTypeList = new List<Type>
         {
            typeof (ArchiveItem),
            typeof (Bat2DeviceData),
            typeof (Contact),
            typeof (CurvePoint),
            typeof (Curve),
            typeof (ErrorMessage),
            typeof (TimeRange),
            typeof (Rs485Options),
            typeof (WeighingPlan)
         };

         addType = new List<Type>
         {
            typeof (List<WeighingPlan>),
            typeof (List<WeighingConfiguration>),
            typeof (List<Rs485Options>),
            typeof (List<Contact>)
         };

         fixture = new Fixture();
         fixture.CustomizeData();
      }

      [TestMethod]
      public void Mapping_Ok()
      {
         typeList.AddRange(addType);

         foreach (var model in typeList)
         {
            if (skipTypeList.Contains(model))
            {
               continue;
            }

            var generatedData = new SpecimenContext(fixture).Resolve(model);
            Assert.IsTrue(Bat2NewDataRW.toNativeGeneric(generatedData), "\nCannot map to native : " + model.Name);
            var parsedData = Activator.CreateInstance(model);
            var resultBool = Bat2NewDataRW.fromNativeGeneric(ref parsedData);
            Assert.AreNotSame(parsedData, generatedData);
            Assert.IsTrue(resultBool, "\nCannot map from native : " + model.Name);
            generatedData.Compare(parsedData);
         }
      }
   }
}