﻿using System.Threading;
using Bat2Library.Connection.Interface.Domain;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;
using Connection.Manager.Tests.Bat2New.Sockets.FlashSocket;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.Bat2New.Managers.FlashManager
{
   [TestClass]
   public class FlashBat2DeviceManagerTests :
      BaseIBat2DeviceManagerTests<
         VersionInfo,
         Configuration,
         Bat2DeviceData,
         IBat2NewDataReader,
         IBat2NewDataWriter,
         IBat2ActionCommander>
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public FlashBat2DeviceManagerTests() :
         base(FlashBat2NewDeviceManager.Instance)
      {
         Thread.Sleep(2000);
      }

      [TestInitialize]
      public void Init()
      {
         FlashFiles.Init();
      }

      [TestCleanup]
      public void Clean()
      {
         FlashFiles.Free();
      }
   }
}