﻿using System.IO;
using Bat2Library;
using Bat2Library.Connection.Manager.Native;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Usb.HID;

namespace Connection.Manager.Tests.Bat2New.Sockets.UsbSocket
{
   [TestClass]
   public class Bat2UsbSocketTests
   {
      private Bat2UsbSocket socket;
      internal const int VID = 0xA600;
      internal const int PID = 0xE2A0;
      private const string FILE_NAME = "tmpFile.txt";

      [TestInitialize]
      public void Init()
      {
         socket = null;
      }

      [TestCleanup]
      public void Clean()
      {
         if (socket != null)
         {
            socket.Dispose();
         }

         if (File.Exists(FILE_NAME))
         {
            File.Delete(FILE_NAME);
         }
      }

      public HidDevice HidDevice
      {
         get
         {
            var manager = new HidDeviceLoader();
            var device = manager.GetDeviceOrDefault(VID, PID);
            return device;
         }
      }


      [TestMethod]
      public void HidDevice_IsOk()
      {
         Assert.IsNotNull(HidDevice);
      }

      [TestMethod]
      public void Socket_Created_ByPath()
      {
         var path = HidDevice.DevicePath;
         socket = new Bat2UsbSocket(path);
         Assert.IsNotNull(socket);
         Assert.AreEqual(SocketStateE.SOCKET_STATE_CONNECTED, socket.State());
      }

      [TestMethod]
      public void Socket_Created_ByVidPid()
      {
         socket = new Bat2UsbSocket(VID, PID);
         Assert.IsNotNull(socket);
         Assert.AreEqual(SocketStateE.SOCKET_STATE_CONNECTED, socket.State());
      }

      [TestMethod]
      public void Socket_Closed()
      {
         socket = new Bat2UsbSocket(VID, PID);
         Assert.AreEqual(SocketStateE.SOCKET_STATE_CONNECTED, socket.State());
         socket.Close();
         Assert.AreEqual(SocketStateE.SOCKET_STATE_DISCONNECTED, socket.State());
      }

      [TestMethod]
      public void Socket_PermissionCheck()
      {
         socket = new Bat2UsbSocket(VID, PID);
         Assert.AreEqual(FileModeE.FILE_MODE_READ_WRITE, socket.Permission());
      }

      [TestMethod]
      public void Socket_DataSended()
      {
         using (var file = new FileStream(FILE_NAME, FileMode.Create, FileAccess.ReadWrite))
         {
            socket = new Bat2UsbSocket(file);
            file.Seek(0, SeekOrigin.Begin);
            var data = new byte[] {0, 0};
            Assert.IsTrue(socket.Send(data));
            Assert.IsTrue(socket.State() == SocketStateE.SOCKET_STATE_SEND_DONE);

            file.Seek(0, SeekOrigin.Begin);
            var readBuf = new byte[200];
            var rLen = file.Read(readBuf, 0, readBuf.Length);
            Assert.IsFalse(rLen == 0);
            Assert.IsTrue(rLen == 6 + data.Length);
         }
      }

      [TestMethod]
      public void Socket_SendAndReaded_IsDataValid()
      {
         using (var file = new FileStream(FILE_NAME, FileMode.Create, FileAccess.ReadWrite))
         {
            socket = new Bat2UsbSocket(file);
            file.Seek(0, SeekOrigin.Begin);
            var data = new byte[] {1, 2, 3, 4, 5};
            Assert.IsTrue(socket.Send(data));
            Assert.IsTrue(socket.State() == SocketStateE.SOCKET_STATE_SEND_DONE);
            file.Flush();

            file.Seek(0, SeekOrigin.Begin);
            var rcv = new byte[data.Length];
            Assert.IsTrue(socket.Receive(ref rcv));
            Assert.IsTrue(socket.State() == SocketStateE.SOCKET_STATE_RECEIVE_DONE);

            Assert.AreEqual(rcv.Length, socket.ReceiveSize());
            for (var i = 0; i < data.Length; i++)
            {
               Assert.AreEqual(data[i], rcv[i]);
            }
         }
      }
   }
}