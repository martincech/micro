﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Bat2Library.Connection.Manager.Native;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Usb.UMS;

namespace Connection.Manager.Tests.Bat2New.Sockets.FlashSocket
{
   public static class FlashFiles
   {
      private static UmsDevice umsDevice;

      public static UmsDevice UmsDevice
      {
         get
         {
            if (umsDevice == null)
            {
               var manager = new UmsDeviceLoader();
               umsDevice = manager.GetDevices().FirstOrDefault();
            }
            return umsDevice;
         }
      }

      public static String BasePath
      {
         get { return UmsDevice.BasePaths.First(); }
      }

      public static String BaseDir
      {
         get { return Path.Combine(BasePath, Bat2FlashSocket.BaseDirectory); }
      }

      public static int? FirstSn
      {
         get
         {
            var num = Directory.GetDirectories(BaseDir)
               .Select(Path.GetFileName)
               .First(
                  dirName =>
                  {
                     var reg = new Regex(@"[\da-fA-F]*");
                     var match = reg.Match(dirName);
                     return match.Success;
                  });
            return int.Parse(num);
         }
      }

      public static String FileScaleToPc
      {
         get
         {
            return Path.Combine(BaseDir, FirstSn.Value.ToString(),
               Bat2FlashSocket.FileName + Bat2FlashSocket.FileExtenssion);
         }
      }

      public static String FilePcToScale
      {
         get
         {
            return Path.Combine(BaseDir, FirstSn.Value.ToString(),
               Bat2FlashSocket.FileName + Bat2FlashSocket.ClientSuffix + Bat2FlashSocket.FileExtenssion);
         }
      }

      public static void FilesCheck()
      {
         var resultDir = Path.Combine(BaseDir, FirstSn.Value.ToString());
         Assert.AreEqual(Directory.GetFiles(resultDir).Count(
            d =>
            {
               var reg = new Regex(@"[\d]*.bin");
               var match = reg.Match(d);
               return !match.Success && !d.Equals(FilePcToScale);
            }), 0);
      }

      public static void Init()
      {
         var fileIndex = 0;
         var sn = FirstSn.Value;
         var baseDir = BaseDir;
         while (true)
         {
            var curFile = Path.Combine(baseDir.ToString(), sn.ToString(),
               fileIndex.ToString() + Bat2FlashSocket.FileExtenssion);
            var tmpFileName = "tmpFile" + fileIndex.ToString();
            if (!File.Exists(curFile))
            {
               break;
            }
            if (File.Exists(tmpFileName))
            {
               File.Delete(tmpFileName);
            }
            File.Copy(curFile, tmpFileName);
            fileIndex++;
         }
         Assert.AreNotEqual(fileIndex, 0, "No scale to PC file");
         if (File.Exists(FilePcToScale))
         {
            File.Delete(FilePcToScale);
         }
      }

      public static void Free()
      {
         var fileIndex = 0;
         var sn = FirstSn.Value;
         var baseDir = BaseDir;
         if (File.Exists(FilePcToScale))
         {
            File.Delete(FilePcToScale);
         }
         while (true)
         {
            var curFile = Path.Combine(baseDir.ToString(), sn.ToString(),
               fileIndex.ToString() + Bat2FlashSocket.FileExtenssion);
            var tmpFileName = "tmpFile" + fileIndex.ToString();
            if (File.Exists(curFile))
            {
               File.Delete(curFile);
            }
            if (!File.Exists(tmpFileName))
            {
               break;
            }
            File.Copy(tmpFileName, curFile);
            fileIndex++;
         }
      }
   }
}