﻿using System;
using System.Linq;
using Bat2Library.Connection.Interface.Domain;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.Bat2New.Sockets.BaseClasses
{
   public abstract class Bat2DataReadTests
   {
      protected abstract void LoadMethod_ReturnNull_WhenNotConnected<T>(Func<IBat2NewDataReader, T> loadAction)
         where T : class;

      protected abstract void LoadMethod_ReturnNonNull_WhenConnected<T>(Func<IBat2NewDataReader, T> loadAction)
         where T : class;

      #region Data transfers

      #region Config

      [TestMethod]
      public void Config_Load_ReturnNull_WhenNotConnected()
      {
         LoadMethod_ReturnNull_WhenNotConnected(reader => reader.LoadConfig());
      }

      [TestMethod]
      public void Config_Load_ReturnNonNull_WhenConnected()
      {
         LoadMethod_ReturnNonNull_WhenConnected(reader => reader.LoadConfig());
      }

      #endregion

      #region Context

      [TestMethod]
      public void Context_Load_ReturnNull_WhenNotConnected()
      {
         LoadMethod_ReturnNull_WhenNotConnected(reader => reader.LoadContext());
      }

      [TestMethod]
      public void Context_Load_ReturnNonNull_WhenConnected()
      {
         LoadMethod_ReturnNonNull_WhenConnected(reader => reader.LoadContext());
      }

      #endregion

      #region Archive

      [TestMethod]
      public void Archive_Load_ReturnNull_WhenNotConnected()
      {
         LoadMethod_ReturnNull_WhenNotConnected(reader => reader.LoadArchive());
      }

      [TestMethod]
      public void Archive_Load_ReturnNonNull_WhenConnected()
      {
         LoadMethod_ReturnNonNull_WhenConnected(reader => reader.LoadArchive());
      }

      #endregion

      #region Growth curves

      [TestMethod]
      public void GrowthCurves_Load_ReturnNull_WhenNotConnected()
      {
         LoadMethod_ReturnNull_WhenNotConnected(reader => reader.LoadGrowthCurves());
      }

      [TestMethod]
      public void GrowthCurves_Load_ReturnNonNull_WhenConnected()
      {
         LoadMethod_ReturnNonNull_WhenConnected(reader => reader.LoadGrowthCurves());
      }

      #endregion

      #region Correction curves

      [TestMethod]
      public void CorrectionCurves_Load_ReturnNull_WhenNotConnected()
      {
         LoadMethod_ReturnNull_WhenNotConnected(reader => reader.LoadCorrectionCurves());
      }

      [TestMethod]
      public void CorrectionCurves_Load_ReturnNonNull_WhenConnected()
      {
         LoadMethod_ReturnNonNull_WhenConnected(reader => reader.LoadCorrectionCurves());
      }

      #endregion

      #region Weighing plan

      [TestMethod]
      public void WeighingPlan_Load_ReturnNull_WhenNotConnected()
      {
         LoadMethod_ReturnNull_WhenNotConnected(reader => reader.LoadWeighingPlans());
      }

      [TestMethod]
      public void WeighingPlan_Load_ReturnNonNull_WhenConnected()
      {
         LoadMethod_ReturnNonNull_WhenConnected(reader => reader.LoadWeighingPlans());
      }

      #endregion

      #region ContactList

      [TestMethod]
      public void ContactList_Load_ReturnNull_WhenNotConnected()
      {
         LoadMethod_ReturnNull_WhenNotConnected(reader => reader.LoadContactList());
      }

      [TestMethod]
      public void ContactList_Load_ReturnNonNull_WhenConnected()
      {
         LoadMethod_ReturnNonNull_WhenConnected(reader => reader.LoadContactList());
      }

      #endregion

      #region Predefined weighings

      [TestMethod]
      public void PredefinedWeighings_Load_ReturnNull_WhenNotConnected()
      {
         LoadMethod_ReturnNull_WhenNotConnected(reader => reader.LoadPredefinedWeighings());
      }

      [TestMethod]
      public void PredefinedWeighings_Load_ReturnNonNull_WhenConnected()
      {
         LoadMethod_ReturnNonNull_WhenConnected(reader => reader.LoadPredefinedWeighings());
      }

      #endregion

      #endregion

      #region Load methods

      internal static void LoadMethod_ReturnNull_WhenNotConnected<T>(Func<IBat2NewDataReader, T> loadAction,
         Bat2NewDevice b2D)
         where T : class
      {
         using (var rw = new Bat2NewDataRW(b2D))
         {
            Assert.IsNull(loadAction(rw));
         }
      }

      internal static void LoadMethod_ReturnNonNull_WhenConnected<T>(Func<IBat2NewDataReader, T> loadAction,
         Bat2NewDevice b2D)
         where T : class
      {
         T loadResult;
         using (var rw = new Bat2NewDataRW(b2D))
         {
            loadResult = loadAction(rw);
         }
         Assert.IsNotNull(loadResult);
         T dPropValue;
         if (typeof (T) == typeof (Bat2DeviceData))
         {
            dPropValue = b2D.DeviceData as T;
         }
         else
         {
            var dProp =
               b2D.DeviceData.GetType().GetProperties().Where(prop =>
                  prop.PropertyType == typeof (T) || prop.PropertyType.IsAssignableFrom(typeof (T))).ToList();
            Assert.IsNotNull(dProp);
            dPropValue =
               dProp.Select(prop => prop.GetMethod.Invoke(b2D.DeviceData, null) as T)
                  .FirstOrDefault(v => v == loadResult);
         }
         Assert.AreSame(loadResult, dPropValue, "Property on inner data not set to valid value!");
      }

      #endregion
   }
}