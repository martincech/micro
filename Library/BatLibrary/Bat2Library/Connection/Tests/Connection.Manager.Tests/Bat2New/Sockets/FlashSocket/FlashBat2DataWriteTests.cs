﻿using System;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;
using Connection.Manager.Tests.Bat2New.Sockets.BaseClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.Bat2New.Sockets.FlashSocket
{
   [TestClass]
   public class FlashBat2DataWriteTests : Bat2DataWriteTests
   {
      [TestInitialize]
      public void Init()
      {
         FlashFiles.Init();
      }

      [TestCleanup]
      public void Clean()
      {
         FlashFiles.Free();
      }

      #region Overrides of Bat2DataWriteTests

      protected override void SaveMethod_ReturnFalse_WhenNotConnected<T>(Func<T, IBat2NewDataWriter, bool> saveAction)
      {
         using (var socket = new Bat2FlashSocket("w:\\", FlashFiles.FirstSn.Value))
         {
            using (var b2D = new FlashBat2Device(socket))
            {
               SaveMethod_ReturnFalse_WhenNotConnected(saveAction, b2D);
            }
         }
         FlashFiles.FilesCheck();
      }

      protected override void SaveMethod_ReturnTrue_WhenConnected<T>(Func<T, IBat2NewDataWriter, bool> saveAction)
      {
         using (var socket = new Bat2FlashSocket(FlashFiles.BasePath, FlashFiles.FirstSn.Value))
         {
            using (var b2D = new FlashBat2Device(socket))
            {
               SaveMethod_ReturnTrue_WhenConnected(saveAction, b2D);
            }
         }
         FlashFiles.FilesCheck();
      }

      #endregion
   }
}