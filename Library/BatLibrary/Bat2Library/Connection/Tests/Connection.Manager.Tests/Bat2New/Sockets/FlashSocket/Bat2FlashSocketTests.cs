﻿using System.IO;
using System.Linq;
using Bat2Library;
using Bat2Library.Connection.Manager.Native;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.Bat2New.Sockets.FlashSocket
{
   [TestClass]
   public class Bat2FlashSocketTests
   {
      private Bat2FlashSocket socket;

      [TestInitialize]
      public void Init()
      {
         socket = null;
         FlashFiles.Init();
      }

      [TestCleanup]
      public void Clean()
      {
         if (socket != null)
         {
            socket.Close();
         }

         FlashFiles.Free();
      }

      [TestMethod]
      public void UmsDevice_IsOk()
      {
         Assert.IsNotNull(FlashFiles.UmsDevice);
      }

      [TestMethod]
      public void UmsDevice_ContainsAnyDevice()
      {
         Assert.IsNotNull(FlashFiles.BasePath);
         Assert.IsTrue(Directory.Exists(FlashFiles.BaseDir));
         Assert.IsTrue(Directory.GetDirectories(FlashFiles.BaseDir).Any());
         Assert.IsNotNull(FlashFiles.FirstSn);
         Assert.IsTrue(File.Exists(FlashFiles.FileScaleToPc));
      }

      [TestMethod]
      public void Socket_Closed()
      {
         socket = new Bat2FlashSocket(FlashFiles.BasePath, FlashFiles.FirstSn.Value);
         Assert.AreEqual(SocketStateE.SOCKET_STATE_DISCONNECTED, socket.State());
      }

      [TestMethod]
      public void Socket_PermissionCheck()
      {
         socket = new Bat2FlashSocket(FlashFiles.BasePath, FlashFiles.FirstSn.Value);
         Assert.AreEqual(FileModeE.FILE_MODE_READ_WRITE, socket.Permission());
      }

      [TestMethod]
      public void Socket_DataSended()
      {
         socket = new Bat2FlashSocket(FlashFiles.BasePath, FlashFiles.FirstSn.Value);
         var data = new byte[] {0, 0};
         Assert.IsTrue(socket.Send(data));
         Assert.IsTrue(socket.State() == SocketStateE.SOCKET_STATE_SEND_DONE);
         socket.Close();
         Assert.IsTrue(File.Exists(FlashFiles.FilePcToScale));

         var file = File.Open(FlashFiles.FilePcToScale, FileMode.Open);
         file.Seek(0, SeekOrigin.Begin);
         var readBuf = new byte[200];
         var rLen = file.Read(readBuf, 0, readBuf.Length);
         Assert.IsFalse(rLen == 0);
         Assert.IsTrue(rLen == 6 + data.Length);
         file.Close();
      }

      [TestMethod]
      public void Socket_SendAndReaded_IsDataValid()
      {
         socket = new Bat2FlashSocket(FlashFiles.BasePath, FlashFiles.FirstSn.Value);
         var data = new byte[] {1, 2, 3, 4, 5};
         Assert.IsTrue(socket.Send(data));
         Assert.IsTrue(socket.State() == SocketStateE.SOCKET_STATE_SEND_DONE);
         socket.Close();
         Assert.IsTrue(File.Exists(FlashFiles.FilePcToScale));

         // copy content to test it
         File.Delete(FlashFiles.FileScaleToPc);
         File.Copy(FlashFiles.FilePcToScale, FlashFiles.FileScaleToPc);

         var rcv = new byte[data.Length];
         Assert.IsTrue(socket.Receive(ref rcv));
         Assert.IsTrue(socket.State() == SocketStateE.SOCKET_STATE_RECEIVE_DONE);
         Assert.AreEqual(rcv.Length, socket.ReceiveSize());
         for (var i = 0; i < data.Length; i++)
         {
            Assert.AreEqual(data[i], rcv[i]);
         }
      }
   }
}