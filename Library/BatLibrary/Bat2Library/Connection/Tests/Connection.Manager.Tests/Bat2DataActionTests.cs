﻿using System;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests
{
   [TestClass]
   public abstract class Bat2DataActionTests
   {
      internal abstract void ActionMethod_ReturnTrue_WhenConnected(Func<IBat2ActionCommander, bool> action);

      internal abstract void ActionMethod_ReturnFalse_WhenNotConnected(Func<IBat2ActionCommander, bool> action);

      #region Action transfer

      #region Connected

      [TestMethod]
      public void _0_TimeGet_Action_ReturnTrue_WhenConnected()
      {
         ActionMethod_ReturnTrue_WhenConnected(commander =>
         {
            DateTime? dt;
            if (commander.TimeGet(out dt))
            {
               Assert.IsTrue(dt.HasValue);
               return true;
            }
            Assert.IsFalse(dt.HasValue);
            return false;
         });
      }

      [TestMethod]
      public void _1_TimeSet_Action_ReturnTrue_WhenConnected()
      {
         ActionMethod_ReturnTrue_WhenConnected(commander => commander.TimeSet(DateTime.Now));
      }

      [TestMethod]
      public void _2_WeighingStart_Action_ReturnTrue_WhenConnected()
      {
         ActionMethod_ReturnTrue_WhenConnected(commander => commander.WeighingStart());
      }

      [TestMethod]
      public void _3_WeighingSuspend_Action_ReturnTrue_WhenConnected()
      {
         ActionMethod_ReturnTrue_WhenConnected(commander => commander.WeighingSuspend());
      }

      [TestMethod]
      public void _4_WeighingRelease_Action_ReturnTrue_WhenConnected()
      {
         ActionMethod_ReturnTrue_WhenConnected(commander => commander.WeighingRelease());
      }

      [TestMethod]
      public void _5_WeighingStop_Action_ReturnTrue_WhenConnected()
      {
         ActionMethod_ReturnTrue_WhenConnected(commander => commander.WeighingStop());
      }

      [TestMethod]
      public void _6_WeighingStart_At_Action_ReturnTrue_WhenConnected()
      {
         ActionMethod_ReturnTrue_WhenConnected(commander => commander.WeighingStart(DateTime.Now.AddMinutes(1)));
      }

      #endregion

      #region Not connected

      [TestMethod]
      public void WeighingStart_Action_ReturnFalse_WhenNotConnected()
      {
         ActionMethod_ReturnFalse_WhenNotConnected(commander => commander.WeighingStart());
      }

      [TestMethod]
      public void WeighingStart_At_Action_ReturnFalse_WhenNotConnected()
      {
         ActionMethod_ReturnFalse_WhenNotConnected(commander => commander.WeighingStart(DateTime.Now.AddMinutes(1)));
      }

      [TestMethod]
      public void WeighingStop_Action_ReturnFalse_WhenNotConnected()
      {
         ActionMethod_ReturnFalse_WhenNotConnected(commander => commander.WeighingStop());
      }

      [TestMethod]
      public void WeighingSuspend_Action_ReturnFalse_WhenNotConnected()
      {
         ActionMethod_ReturnFalse_WhenNotConnected(commander => commander.WeighingSuspend());
      }

      [TestMethod]
      public void WeighingRelease_Action_ReturnFalse_WhenNotConnected()
      {
         ActionMethod_ReturnFalse_WhenNotConnected(commander => commander.WeighingRelease());
      }

      [TestMethod]
      public void TimeSet_Action_ReturnFalse_WhenNotConnected()
      {
         ActionMethod_ReturnFalse_WhenNotConnected(commander => commander.TimeSet(DateTime.Now));
      }

      [TestMethod]
      public void TimeGet_Action_ReturnFalse_WhenNotConnected()
      {
         ActionMethod_ReturnFalse_WhenNotConnected(commander =>
         {
            DateTime? dt;
            if (commander.TimeGet(out dt))
            {
               Assert.IsTrue(dt.HasValue);
               return true;
            }
            Assert.IsFalse(dt.HasValue);
            return false;
         });
      }

      #endregion

      #endregion

      internal static void ActionMethod_ReturnTrue_WhenConnected(Func<IBat2ActionCommander, bool> action,
         Bat2NewDevice b2D)
      {
         using (var rw = new Bat2NewDataRW(b2D))
         {
            Assert.IsTrue(action(rw));
         }
      }

      internal static void ActionMethod_ReturnFalse_WhenNotConnected(Func<IBat2ActionCommander, bool> action,
         Bat2NewDevice b2D)
      {
         using (var rw = new Bat2NewDataRW(b2D))
         {
            Assert.IsFalse(action(rw));
         }
      }

      internal static void ActionMethod_ReturnTrue_WhenConnected(Func<IBat2ActionCommander, bool> action,
         Bat2OldDevice b2D)
      {
         using (var rw = new Bat2OldDataRW(b2D))
         {
            Assert.IsTrue(action(rw));
         }
      }

      internal static void ActionMethod_ReturnFalse_WhenNotConnected(Func<IBat2ActionCommander, bool> action,
         Bat2OldDevice b2D)
      {
         using (var rw = new Bat2OldDataRW(b2D))
         {
            Assert.IsFalse(action(rw));
         }
      }
   }
}