﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Security;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Windows;
using Common.Services.Hosting;
using Connection.Server;

namespace Services
{
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow
   {
      private List<ServiceHost> hosts;

      private static Type[] GetContracts<S>()
      {
         Debug.Assert(typeof (S).IsClass);

         var interfaces = typeof (S).GetInterfaces();

         return
            interfaces.Where(type => type.GetCustomAttributes(typeof (ServiceContractAttribute), false).Any()).ToArray();
      }

      private void StartHost<S>()
         where S : class
      {
         const string baseAddress = "net.pipe://localhost/";
         var mexAddress = baseAddress + typeof (S).Name;
         var binding = new NetNamedPipeBinding { TransactionFlow = true };
         var host = new ServiceHost<S>(mexAddress);
         var contracts = GetContracts<S>();
         Debug.Assert(contracts.Any());
         host.EnableMetadataExchange();

         foreach (var contract in contracts)
         {
            var address = baseAddress + contract;
            host.AddServiceEndpoint(contract, binding, address);
         }
         host.Open();
         hosts.Add(host);
         HostsAddresses.Add(mexAddress);
      }

      private void StopHosts()
      {
         foreach (var serviceHost in hosts)
         {
            serviceHost.Close();
         }
      }
      public MainWindow()
      {
         HostsAddresses = new ObservableCollection<string>();
         hosts = new List<ServiceHost>();
         InitializeComponent();
      }

      public ObservableCollection<string> HostsAddresses { get; private set; } 

      //start
      private void Button_Click(object sender, RoutedEventArgs e)
      {
         StartHost<Bat2CommandContractService>();
         StartHost<Bat2EventsSubscribtionService>();
         StartHost<Bat2EventsPublisherService>();
      }

      //stop
      private void Button_Click_1(object sender, RoutedEventArgs e)
      {
         StopHosts();
         HostsAddresses.Clear();
      }
   }
}