using System;
using Connection.Interface.Contract;
using Connection.Interface.Domain;
using Connection.Server;

namespace Connection.Client
{

   /// <summary>
   /// Event publisher client
   /// </summary>
   public class Bat2EventsPublisherClient :
      ServiceClient<Bat2EventsPublisherService, IBat2EventsContract>,
      IBat2EventsContract
   {
      #region Implementation of IBat2ConnectionEventsContract

      /// <summary>
      /// Event is invoked when device is connected.
      /// </summary>
      /// <param name="deviceData">device data according to connected device</param>
      public void DeviceConnected(Bat2DeviceData deviceData)
      {
         ExecuteServiceMethod(x=> x.DeviceConnected(deviceData));
      }

      /// <summary>
      /// Event is invoked when device is disconnected.
      /// </summary>
      /// <param name="deviceData">device data according to disconnected device</param>
      public void DeviceDisconnected(Bat2DeviceData deviceData)
      {
         ExecuteServiceMethod(x => x.DeviceDisconnected(deviceData));
      }

      #endregion

      #region Implementation of IBat2DataReadEventsContract

      /// <summary>
      /// Event is invoked when new data are readed from device.
      /// </summary>
      /// <param name="data">Data readed from device</param>
      public void DataRead(Bat2DeviceData data)
      {
         ExecuteServiceMethod(x => x.DataRead(data));
      }

      public void TimeGet(Bat2DeviceData deviceData, DateTime time)
      {
         ExecuteServiceMethod(x => x.TimeGet(deviceData, time));
      }

      #endregion

      #region Implementation of IBat2DataWriteEventsContract

      /// <summary>
      /// Event is invoked when new data are writen to device
      /// </summary>
      /// <param name="data">Data writen to device</param>
      public void DataWriten(Bat2DeviceData data)
      {
         ExecuteServiceMethod(x => x.DataWriten(data));
      }

      public void TimeSet(Bat2DeviceData deviceData, DateTime time)
      {
         ExecuteServiceMethod(x => x.TimeSet(deviceData, time));
      }

      #endregion

      #region Implementation of IBat2ActionEventsContract

      public void WeighingStarted(Bat2DeviceData deviceData)
      {
         ExecuteServiceMethod(x => x.WeighingStarted(deviceData));
      }

      public void WeighingPlanned(Bat2DeviceData deviceData, DateTime at)
      {
         ExecuteServiceMethod(x => x.WeighingPlanned(deviceData, at));
      }

      public void WeighingStopped(Bat2DeviceData deviceData)
      {
         ExecuteServiceMethod(x => x.WeighingStopped(deviceData));
      }

      public void WeighingSuspended(Bat2DeviceData deviceData)
      {
         ExecuteServiceMethod(x => x.WeighingSuspended(deviceData));
      }

      public void WeighingReleased(Bat2DeviceData deviceData)
      {
         ExecuteServiceMethod(x => x.WeighingReleased(deviceData));
      }

      #endregion
   }
}