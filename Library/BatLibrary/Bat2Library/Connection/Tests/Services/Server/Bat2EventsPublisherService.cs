﻿using System;
using System.ServiceModel;
using Common.Services.PublishSubscribe;
using Connection.Interface.Contract;
using Connection.Interface.Domain;

namespace Connection.Server
{
   /// <summary>
   /// Publish event to subscribted clients managed by <see cref="Bat2EventsSubscribtionService"/>
   /// </summary>
   [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
   public class Bat2EventsPublisherService : 
      PublishService<IBat2EventsContract>, IBat2EventsContract
   {
      #region Implementation of IBat2ConnectionEventsContract

      /// <summary>
      /// Event is invoked when device is connected.
      /// </summary>
      /// <param name="deviceData">device data according to connected device</param>
      public void DeviceConnected(Bat2DeviceData deviceData)
      {
         FireEvent(deviceData);
      }

      /// <summary>
      /// Event is invoked when device is disconnected.
      /// </summary>
      /// <param name="deviceData">device data according to disconnected device</param>
      public void DeviceDisconnected(Bat2DeviceData deviceData)
      {
         FireEvent(deviceData);
      }

      #endregion

      #region Implementation of IBat2DataReadEventsContract

      /// <summary>
      /// Event is invoked when new data are readed from device.
      /// </summary>
      /// <param name="data">Data readed from device</param>
      public void DataRead(Bat2DeviceData data)
      {
         FireEvent(data);
      }

      public void TimeGet(Bat2DeviceData deviceData, DateTime time)
      {
         FireEvent(deviceData, time);
      }

      #endregion

      #region Implementation of IBat2DataWriteEventsContract

      /// <summary>
      /// Event is invoked when new data are writen to device
      /// </summary>
      /// <param name="data">Data writen to device</param>
      public void DataWriten(Bat2DeviceData data)
      {
         FireEvent(data);
      }

      public void TimeSet(Bat2DeviceData deviceData, DateTime time)
      {
         FireEvent(deviceData, time);
      }

      #endregion

      #region Implementation of IBat2ActionEventsContract

      public void WeighingStarted(Bat2DeviceData deviceData)
      {
         FireEvent(deviceData);
      }

      public void WeighingPlanned(Bat2DeviceData deviceData, DateTime at)
      {
         FireEvent(deviceData, at);
      }

      public void WeighingStopped(Bat2DeviceData deviceData)
      {
         FireEvent(deviceData);
      }

      public void WeighingSuspended(Bat2DeviceData deviceData)
      {
         FireEvent(deviceData);
      }

      public void WeighingReleased(Bat2DeviceData deviceData)
      {
         FireEvent(deviceData);
      }

      #endregion
   }
}
