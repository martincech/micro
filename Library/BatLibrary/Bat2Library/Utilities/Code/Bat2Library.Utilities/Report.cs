﻿using System;
using Bat2Library.Utilities.Sms.Models;

namespace Bat2Library.Utilities
{
    public class Report : StatData
    {
        public int Day;
        public SexE Sex;
        public DateTime Date;
        public string ScaleName;
        public string Note;       
    }
}
