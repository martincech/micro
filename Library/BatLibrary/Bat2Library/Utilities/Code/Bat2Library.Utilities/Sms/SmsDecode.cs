﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Utilities;
using Bat2Library.Utilities.Sms.Models;

namespace Bat2Library.Utilities.Sms
{
   public class SmsDecode
   {
      private const int SHORT_MESSAGE_LENGHT = 18;
      private const int SHORT_MESSAGE_LENGHT_EXTENDED = 13;
      private const int LONG_MESSAGE_LENGHT_EXTENDED = 19;

      private static void DeleteWord(StringBuilder text, out string word)
      {
         int spaceIndex;
         spaceIndex = text.ToString().IndexOf(' ');
         if (spaceIndex == 0)
         {
            word = text.ToString();
            throw new ArgumentException();
         }
         word = text.ToString().Substring(0, spaceIndex);
         text.Remove(0, spaceIndex + 1);
      }

      /// <summary>
      /// Decode SMS text to a struct
      /// </summary>
      /// <param name="text">SMS text</param>
      /// <param name="smsData">DecodedSms instance</param>
      /// <returns>True if successful</returns>
      public static bool Decode(string text, out SmsData smsData)
      {
         int sn;
         char[] delimeters = {' ', '\t', '\n'};
         if (Int32.TryParse(text.Split(delimeters).First(), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out sn))
         {
            return DecodeExtended(text, out smsData);
         }
         return DecodeBasic(text, out smsData);
      }

      private static bool DecodeExtended(string text, out SmsData smsData)
      {
         // 7FFFFFFF 999 12/31/9999 99999 99.999 -99.999 9.999 99.9 99.9 -999.99 9999999 9999999 99.999
         // 7FFFFFFF 999 12/31/9999 99999 99.999 -99.999 9.999 99.9 99.9 99999 99.999 -99.999 9.999 99.9 99.9 -999.99 9999999 9999999 99.999
         smsData = new SmsData();
         char[] delimeters = {' ', '\t', '\n'};
         var lenght = text.ToLower().Split(delimeters).Count();

         if (!(lenght == SHORT_MESSAGE_LENGHT_EXTENDED || lenght == LONG_MESSAGE_LENGHT_EXTENDED))
         {
            return false;
         }

         StringBuilder textBuilder = new StringBuilder(text);
         string str;
         try
         {
            //Scale number
            DeleteWord(textBuilder, out str);
            smsData.ScaleNumber = int.Parse(str, NumberStyles.HexNumber);
            //Day
            DeleteWord(textBuilder, out str);
            smsData.DayNumber = int.Parse(str);

            //Date
            DeleteWord(textBuilder, out str);
            smsData.Date = DateTime.ParseExact(str, "MM/dd/yyyy", CultureInfo.InvariantCulture);

            //Count
            smsData.FemaleData = GetStatData(textBuilder);

            if (lenght > SHORT_MESSAGE_LENGHT_EXTENDED)
            {
               smsData.MaleData = GetStatData(textBuilder);
            }

            //Temperature
            double temperature;
            DeleteWord(textBuilder, out str);
            smsData.FemaleData.Temperature = double.TryParse(str, NumberStyles.Float,
               CultureInfo.InvariantCulture, out temperature)
               ? (double?)temperature
               : null;

            //Carbon dioxide
            int carbonDioxide;
            DeleteWord(textBuilder, out str);
            smsData.FemaleData.CarbonDioxide = int.TryParse(str, out carbonDioxide) ? (int?)carbonDioxide : null;

            //Ammonia
            int ammonia;
            DeleteWord(textBuilder, out str);
            smsData.FemaleData.Ammonia = int.TryParse(str,out ammonia) ? (int?)ammonia : null;

            //Humidity
            double humidity;
            smsData.FemaleData.Humidity = double.TryParse(textBuilder.ToString(), NumberStyles.Float,
               CultureInfo.InvariantCulture, out humidity)
               ? (double?)humidity
               : null;

            if (lenght > SHORT_MESSAGE_LENGHT_EXTENDED)
            {
               smsData.MaleData.Temperature = smsData.FemaleData.Temperature;
               smsData.MaleData.CarbonDioxide = smsData.FemaleData.CarbonDioxide;
               smsData.MaleData.Ammonia = smsData.FemaleData.Ammonia;
               smsData.MaleData.Humidity = smsData.FemaleData.Humidity;
            }

            return true;
         }
         catch
         {
            return false;
         }
      }

      private static StatData GetStatData(StringBuilder textBuilder)
      {
         string str;

         StatData statData = new StatData();
         var cult = CultureInfo.InvariantCulture;

         // Cnt
         DeleteWord(textBuilder, out str);
         statData.Count = int.Parse(str);

         // Avg
         DeleteWord(textBuilder, out str);
         statData.Average = double.Parse(str, cult);

         // Gain
         DeleteWord(textBuilder, out str);
         statData.Gain = double.Parse(str, cult);

         // Sigma
         DeleteWord(textBuilder, out str);
         statData.Sigma = double.Parse(str, cult);

         // CV
         DeleteWord(textBuilder, out str);
         statData.Cv = double.Parse(str, cult);

         // Uni
         DeleteWord(textBuilder, out str);
         statData.Uniformity = double.Parse(str, cult);

         return statData;
      }

      private static bool DecodeBasic(string text, out SmsData smsData)
      {
         // SCALE 123456789012345 DAY 999 31.12.9999 23:59:59PM Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999
         // SCALE 123456789012345 DAY 999 31.12.9999 23:59:59PM FEMALES: Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999 MALES: Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999
         smsData = new SmsData();
         char[] delimeters = { ' ', '\t', '\n' };
         var lenght = text.ToLower().Split(delimeters).Count();
         StringBuilder textBuilder = new StringBuilder(text);
         string str;
         try
         {
            // Delete SCALES
            DeleteWord(textBuilder, out str);

            // Delete vah
            DeleteWord(textBuilder, out str);
            smsData.ScaleName = str;

            // Delete DAY
            DeleteWord(textBuilder, out str);

            // Delete day
            DeleteWord(textBuilder, out str);
            smsData.DayNumber = int.Parse(str);

            // Delete date
            DeleteWord(textBuilder, out str);
            Dictionary<int, char> separators = new Dictionary<int, char>();
            DateTime date = new DateTime();

            date = DateTimeParser.ParseDate(str);

            DeleteWord(textBuilder, out str);

            if (Char.IsNumber(str[0]))
            {
               DateTime time = DateTimeParser.ParseTime(str);
               date = new DateTime(date.Year, date.Month, date.Day, time.Hour, time.Minute, time.Second);
               DeleteWord(textBuilder, out str);
            }

            smsData.Date = date;


            if (lenght > SHORT_MESSAGE_LENGHT)
            {
               DeleteWord(textBuilder, out str);
               smsData.FemaleData = GetSmsData(textBuilder);
               DeleteWord(textBuilder, out str);
               DeleteWord(textBuilder, out str);
               smsData.MaleData = GetSmsData(textBuilder);
            }
            else
            {
               smsData.FemaleData = GetSmsData(textBuilder);
            }
            return CheckSmsDataFormat(smsData);
         }
         catch
         {
            // Neco se nepovedlo, neplatny format SMS
            return false;
         }
      }

      private static bool CheckSmsDataFormat(SmsData d)
      {
         if (d.DayNumber < 0 ||
             !CheckStatDataFormat(d.MaleData) || !CheckStatDataFormat(d.FemaleData))
         {
            return false;
         }
         return true;
      }

      private static bool CheckStatDataFormat(StatData d)
      {
         if (d == null)
         {
            return true;
         }
         if (d.Average < 0 || d.Count < 0 || d.Cv < 0 || d.Sigma < 0 || d.Uniformity < 0)
         {
            return false;
         }
         return true;
      }

      private static StatData GetSmsData(StringBuilder textBuilder)
      {

         string str;

         StatData statData = new StatData();

         // Cnt
         // DeleteWord(textBuilder, out str);
         DeleteWord(textBuilder, out str);
         statData.Count = int.Parse(str);

         CultureInfo culture = new CultureInfo("en-US");

         // Avg
         DeleteWord(textBuilder, out str);
         DeleteWord(textBuilder, out str);
         culture = CheckLanguage(str);
         statData.Average = double.Parse(str, culture);

         // Gain
         DeleteWord(textBuilder, out str);
         DeleteWord(textBuilder, out str);
         culture = CheckLanguage(str);
         statData.Gain = double.Parse(str, culture);

         // Sigma
         DeleteWord(textBuilder, out str);
         DeleteWord(textBuilder, out str);
         culture = CheckLanguage(str);
         statData.Sigma = double.Parse(str, culture);

         // CV vypoctu na desetinu (v SMS je to jen na cele cislo)
         DeleteWord(textBuilder, out str);
         DeleteWord(textBuilder, out str);
         culture = CheckLanguage(str);
         statData.Cv = double.Parse(str, culture);

         // Uni
         DeleteWord(textBuilder, out str);
         if (textBuilder.ToString().Split(' ').Count() > 1)
         {
            DeleteWord(textBuilder, out str);
            statData.Uniformity = int.Parse(str);
         }
         else
         {
            statData.Uniformity = int.Parse(textBuilder.ToString());
         }

         return statData;
      }

      private static CultureInfo CheckLanguage(string number)
      {
         if (number.Contains(','))
         {
            return new CultureInfo("de-DE");
         }
         else
         {
            return new CultureInfo("en-US");
         }
      }
   }
}
