﻿namespace Bat2Library.Utilities.Sms
{
    public class SmsTextFormat
    {
        public const string FEMALES_KEY = "females:";
        public const string MALES_KEY = "males:";
        public const string SCALE_KEY = "scale";
        public const string DAY_KEY = "day";
        public const string CNT_KEY = "cnt";
        public const string AVG_KEY = "avg";
        public const string GAIN_KEY = "gain";
        public const string SIG_KEY = "sig";
        public const string UNI_KEY = "uni";
        public const string CV_KEY = "cv";
    }
}
