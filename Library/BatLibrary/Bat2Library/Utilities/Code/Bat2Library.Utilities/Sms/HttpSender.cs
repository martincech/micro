﻿using System;
using System.IO;
using System.Net;

namespace Bat2Library.Utilities.Sms
{
   public class HttpSender
   {
      /// <summary>
      /// Send message over http
      /// </summary>
      /// <param name="message">body of message</param>
      /// <param name="userName"></param>
      /// <param name="password"></param>
      /// <returns>byte with result</returns>
      public static HttpStatusCode SendMessage(string message, string userName, string password, string url)
      {

         url = url.ToLower();
         if (!url.StartsWith("http://") && !url.StartsWith("https://"))
         {
            url = "http://" + url;
         }

         try
         {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.CookieContainer = new CookieContainer();

            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(userName + ":" + password));
            request.Headers.Add("Authorization", "Basic " + encoded);
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
               streamWriter.Write(message);
            }

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8);
            String resultData = reader.ReadToEnd();

            if (resultData.Contains("DOCTYPE"))
            {
               return HttpStatusCode.Forbidden;
            }
            if (resultData.Contains("StatusCode") && resultData.Contains("406"))
            {
               return HttpStatusCode.NotAcceptable;
            }
            if (resultData.Contains("StatusCode") && resultData.Contains("403"))
            {
               return HttpStatusCode.Forbidden;
            }

            return response.StatusCode;
         }
         catch (WebException ex)
         {
            if (ex.Response != null)
            {
               return ((HttpWebResponse)ex.Response).StatusCode;
            }
            else
            {
               return HttpStatusCode.NotFound;
            }
         }
      }

   }
}
