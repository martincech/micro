﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Bat2Library.ModbusWorker.Worker;

namespace Bat2Library.ModbusWorker
{
   /// <summary>
   /// Modbus manger for reading scale data using ELO
   /// </summary>
   public class ModbusManager
   {
      private List<EloTask> elos;
      private List<EloTask> scheduler;

      public delegate void ProcessHandler(object sender, EloTaskEventArgs data);
      public ProcessHandler ReadHandler;

      public ModbusManager()
      {
         elos = new List<EloTask>();
         scheduler = new List<EloTask>();
      }

      /// <summary>
      /// Read scale statistics
      /// </summary>
      /// <param name="elo">elo settings</param>
      /// <param name="addresses">list of connected scale addresses</param>
      /// <returns>true if task to read statistic started</returns>
      public bool Read(Setup elo, List<int> addresses)
      {
         return ReadNow(elo, EloOperation.Read, addresses);
      }

      /// <summary>
      /// Read scale statistics at specific time
      /// </summary>
      /// <param name="elo">elo settings</param>
      /// <param name="addresses">list of connected scale addresses</param>
      /// <param name="hour">read at hour (0-23)</param>
      /// <param name="minute">read at minute (0-59)</param>
      /// <returns>true if task to read statistic was added to scheduler</returns>
      public bool Read(Setup elo, List<int> addresses, int hour, int minute)
      {
         return ReadDelayed(elo, addresses, hour, minute);
      }

      /// <summary>
      /// Search connected scales
      /// </summary>
      /// <param name="elo">elo settings</param>
      /// <returns>true if task to search connected scales started</returns>
      public bool Scan(Setup elo)
      {
         return ReadNow(elo, EloOperation.Scan);
      }

      /// <summary>
      /// Check if scale is connected at specific adresses
      /// </summary>
      /// <param name="elo">elo settings</param>
      /// <param name="addresses">list of connected scale addresses</param>
      /// <returns>true if task to check connected scales started</returns>
      public bool Scan(Setup elo, List<int> addresses)
      {
         return ReadNow(elo, EloOperation.Check, addresses);
      }

      /// <summary>
      /// Change existing scheduler staring time 
      /// </summary>
      /// <param name="port">elo port</param>
      /// <param name="hour">hour to start scheduler</param>
      /// <param name="minute">minute to start scheduler</param>
      public void UpdateScheduler(int port, int hour, int minute)
      {
         var elo = scheduler.FirstOrDefault(f => f.Setup.ModbusScales.PortNumber == port);
         if (elo != null)
         {
            elo.TokenSource.Cancel();
            scheduler.Remove(elo);
            ReadDelayed(elo.Setup, elo.Addresses, hour, minute);
         }
      }

      /// <summary>
      /// Change existing scheduler addresses
      /// </summary>
      /// <param name="port">elo port</param>
      /// <param name="adresses">scale addresses</param>
      public void UpdateScheduler(int port, List<int> adresses)
      {
         var elo = scheduler.FirstOrDefault(f => f.Setup.ModbusScales.PortNumber == port);
         if (elo != null)
         {
            elo.Addresses = adresses;
         }
      }

      /// <summary>
      /// Stop reading from elo on specific port
      /// </summary>
      /// <param name="port">elo port</param>
      public void Stop(int port)
      {
         var elo = elos.FirstOrDefault(f => f.Setup.ModbusScales.PortNumber == port);
         if (elo != null)
         {
            elo.TokenSource.Cancel();
         }
      }

      /// <summary>
      /// Stop existing scheduler for elo on specific port 
      /// </summary>
      /// <param name="port">elo port</param>
      public void StopScheduler(int port)
      {
         var elo = scheduler.FirstOrDefault(f => f.Setup.ModbusScales.PortNumber == port);
         if (elo != null)
         {
            elo.TokenSource.Cancel();
            scheduler.Remove(elo);
         }
      }
      
      private bool ReadNow(Setup setup, EloOperation operation, List<int> addresses = null)
      {
         var runningElo = elos.FirstOrDefault(a => a.Setup.ModbusScales.PortNumber == setup.ModbusScales.PortNumber);
         if (runningElo != null)
         {
            //Stop active elo reading
            Stop(runningElo.Setup.ModbusScales.PortNumber);
         }

         EloTask newElo = new EloTask(setup, new CancellationTokenSource(), addresses);
         elos.Add(newElo);
         ModbusElo modbus = new ModbusElo(setup, addresses, operation);
       
         modbus.Start((data) =>
         {
            var last = false;
            //Check if readed address is last
            if (addresses == null)
            {
               if (data.Address == setup.ModbusScales.LastAddress)
                  last = true;
            }
            else
            {
               if (data.Address == addresses.LastOrDefault())
                  last = true;
            }

           if (data.Event == WorkerEvent.CANCEL)
            {
               operation = EloOperation.Canceled;
               last = true;
            }

            ReadHandler(this, new EloTaskEventArgs(operation, data, last));
            if (last)
            {
               elos.Remove(newElo);
            }

            if (newElo.TokenSource.IsCancellationRequested)
            {
               return false;
            }

            return true;
         });

         return true;
      }

      
      private bool ReadDelayed(Setup setup, List<int> addresses, int hour, int minute)
      {
         var delayedElo = scheduler.FirstOrDefault(a => a.Setup.ModbusScales.PortNumber == setup.ModbusScales.PortNumber);
         if (delayedElo != null)
         {
            delayedElo.TokenSource.Cancel();
            scheduler.Remove(delayedElo);
         }
         EloTask newElo = new EloTask(setup, new CancellationTokenSource(), addresses, hour, minute);
         scheduler.Add(newElo);
         ReadDelayed(newElo.Setup.ModbusScales.PortNumber, newElo.Hour, newElo.Minute, newElo.TokenSource);
         return true;
      }
      
      private async void ReadDelayed(int port,int hour, int minute,CancellationTokenSource tokenSource)
      {
         var dateNow = DateTime.Now;
         var date = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, hour, minute, 0);
         TimeSpan ts;
         if (date > dateNow)
            ts = date - dateNow;
         else
         {
            date = date.AddDays(1);
            ts = date - dateNow;
         }

         try
         {
            await Task.Delay(ts, tokenSource.Token);
         }
         catch (TaskCanceledException)
         {
           return;
         }

         var actualTask = scheduler.FirstOrDefault(f => f.Setup.ModbusScales.PortNumber == port);
         if (actualTask != null)
         {
            //Read data from scale
            ReadNow(actualTask.Setup, EloOperation.Read, actualTask.Addresses);
            actualTask.TokenSource = new CancellationTokenSource();
            //Repeat reading at same time next day
            ReadDelayed(actualTask.Setup.ModbusScales.PortNumber, actualTask.Hour, actualTask.Minute, actualTask.TokenSource);
         }
      }
   }

   public enum EloOperation
   {
      Read,
      Check,
      Scan,
      Canceled
   }

   public class EloTaskEventArgs : EventArgs
   {
      public EloOperation Operation { get; internal set; }
      public WorkerData Data { get; internal set; }
      public bool Last { get; internal set; }

      public EloTaskEventArgs(EloOperation Operation, WorkerData Data, bool Last)
      {
         this.Data = Data;
         this.Operation = Operation;
         this.Last = Last;
      }
   }

   internal class EloTask
   {
      public EloTask(Setup setup, CancellationTokenSource tokenSource)
         : this(setup, tokenSource, null, 0, 0, false)
      {

      }

      public EloTask(Setup setup, CancellationTokenSource tokenSource, List<int> addresses)
         : this(setup, tokenSource, addresses, 0, 0, false)
      {

      }

      public EloTask(Setup setup, CancellationTokenSource tokenSource, int hour, int minute)
         : this(setup, tokenSource, null, hour, minute, true)
      {
      }

      public EloTask(Setup setup, CancellationTokenSource tokenSource, List<int> addresses, int hour, int minute)
         : this(setup, tokenSource, addresses, hour, minute, true)
      {
      }

      private EloTask(Setup setup, CancellationTokenSource tokenSource, List<int> addresses, int hour, int minute, bool isDelayed)
      {
         this.Setup = setup;
         this.TokenSource = tokenSource;
         this.Addresses = addresses;
         this.Hour = hour;
         this.Minute = minute;
         this.IsDelayed = isDelayed;
      }

      public Setup Setup { get; set; }
      public CancellationTokenSource TokenSource { get; set; }
      public bool IsDelayed { get; set; }
      public int Hour { get; set; }
      public int Minute { get; set; }
      public List<int> Addresses { get; set; }
   }
}
