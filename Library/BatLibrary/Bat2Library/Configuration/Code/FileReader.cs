﻿using System.Collections.Generic;
using Bat2Library.Connection.Interface.Domain;
using Bat2Library.Connection.Manager.Native;

namespace Bat2Library.Configuration
{
   public class FileReader
   {
      private string BasePath { get; set; }
      private int SerialNumber { get; set; }

      public FileReader(string basePath, int serialNumber)
      {
         BasePath = basePath;
         SerialNumber = serialNumber;
      }

      public Connection.Interface.Domain.Bat2DeviceData LoadDefault()
      {
         using (var socket = new Bat2FlashSocket(BasePath, SerialNumber))
         {
            using (var b2D = new FlashBat2Device(socket))
            {
               using (var rw = new Bat2NewDataRW(b2D))
               {
                  return rw.LoadDefault();
               }
            }
         }
      }

      public Connection.Interface.Domain.Configuration LoadConfig()
      {
         using (var socket = new Bat2FlashSocket(BasePath, SerialNumber))
         {
            using (var b2D = new FlashBat2Device(socket))
            {
               using (var rw = new Bat2NewDataRW(b2D))
               {
                  return rw.LoadConfig();
               }
            }
         }
      }

      public IEnumerable<Contact> LoadContactList()
      {
         using (var socket = new Bat2FlashSocket(BasePath, SerialNumber))
         {
            using (var b2D = new FlashBat2Device(socket))
            {
               using (var rw = new Bat2NewDataRW(b2D))
               {
                  return rw.LoadContactList();
               }
            }
         }
      }

      public IEnumerable<Curve> LoadCorrectionCurves()
      {
         using (var socket = new Bat2FlashSocket(BasePath, SerialNumber))
         {
            using (var b2D = new FlashBat2Device(socket))
            {
               using (var rw = new Bat2NewDataRW(b2D))
               {
                  return rw.LoadCorrectionCurves();
               }
            }
         }
      }

      public IEnumerable<Curve> LoadGrowthCurves()
      {
         using (var socket = new Bat2FlashSocket(BasePath, SerialNumber))
         {
            using (var b2D = new FlashBat2Device(socket))
            {
               using (var rw = new Bat2NewDataRW(b2D))
               {
                  return rw.LoadGrowthCurves();
               }
            }
         }
      }

      public IEnumerable<WeighingConfiguration> LoadPredefinedWeighings()
      {
         using (var socket = new Bat2FlashSocket(BasePath, SerialNumber))
         {
            using (var b2D = new FlashBat2Device(socket))
            {
               using (var rw = new Bat2NewDataRW(b2D))
               {
                  return rw.LoadPredefinedWeighings();
               }
            }
         }
      }

      public IEnumerable<WeighingPlan> LoadWeighingPlans()
      {
         using (var socket = new Bat2FlashSocket(BasePath, SerialNumber))
         {
            using (var b2D = new FlashBat2Device(socket))
            {
               using (var rw = new Bat2NewDataRW(b2D))
               {
                  return rw.LoadWeighingPlans();
               }
            }
         }
      }
   }
}
