﻿using System;
using System.Globalization;
using Bat2Library.Desktop.Localization;

namespace Bat2Library.Desktop.Presentation.Converters
{
   public static class EnumConverterExtension
   {
      /// <summary>
      /// Handle XAML Conversion from this type to other types
      /// </summary>
      /// <param name="converter">a converter to be extended</param>
      /// <param name="value">The value to convert</param>
      /// <param name="targetType">The target type</param>
      /// <param name="parameter">not used</param>
      /// <param name="culture">The culture to convert</param>
      /// <returns>The converted value</returns>
      public static object Convert(this ResourceEnumConverter converter, object value, Type targetType, object parameter, CultureInfo culture)
      {
         return converter.ConvertTo(null, culture, value, targetType);
      }

      /// <summary>
      /// Handle XAML Conversion from other types back to this type
      /// </summary>
      /// <param name="converter">a converter to be extended</param>
      /// <param name="value">The value to convert</param>
      /// <param name="targetType">The target type</param>
      /// <param name="parameter">not used</param>
      /// <param name="culture">The culture to convert</param>
      /// <returns>The converted value</returns>
      public static object ConvertBack(this ResourceEnumConverter converter, object value, Type targetType, object parameter, CultureInfo culture)
      {
         return converter.ConvertFrom(null, culture, value);
      }

      /// <summary>
      /// Handle XAML Conversion from this type to other types
      /// </summary>
      /// <param name="converter">a converter to be extended</param>
      /// <param name="value">The value to convert</param>
      /// <param name="targetType">The target type</param>
      /// <param name="parameter">not used</param>
      /// <param name="culture">The culture to convert</param>
      /// <returns>The converted value</returns>
      public static object Convert(this ReverseResourceEnumConverter converter, object value, Type targetType, object parameter, CultureInfo culture)
      {
         if (value == null)
         {
            return null;
         }
         return converter.ConvertFrom(null, culture, value);
      }

      /// <summary>
      /// Handle XAML Conversion from other types back to this type
      /// </summary>
      /// <param name="converter">a converter to be extended</param>
      /// <param name="value">The value to convert</param>
      /// <param name="targetType">The target type</param>
      /// <param name="parameter">not used</param>
      /// <param name="culture">The culture to convert</param>
      /// <returns>The converted value</returns>
      public static object ConvertBack(this ReverseResourceEnumConverter converter, object value, Type targetType, object parameter, CultureInfo culture)
      {
         if (value == null)
         {
            return null;
         }
         return converter.ConvertTo(null, culture, value, targetType);
      }
   }
}
