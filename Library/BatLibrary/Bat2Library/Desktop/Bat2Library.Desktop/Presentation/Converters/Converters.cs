﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Bat2Library.Desktop.Properties;

namespace Bat2Library.Desktop.Presentation.Converters
{
   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.DeviceClassEConverter"/>.
   /// </summary>
   public class DeviceClassEConverter : Localization.Converters.DeviceClassEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.DeviceModificationEConverter"/>.
   /// </summary>
   public class DeviceModificationEConverter : Localization.Converters.DeviceModificationEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.CountryEConverter"/>.
   /// </summary>
   public class CountryEConverter : Localization.Converters.CountryEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }
   public class CodePageEConverter : Localization.Converters.CodePageEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }
   
   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.LanguageEConverter"/>.
   /// </summary>
   public class LanguageEConverter : Localization.Converters.LanguageEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.DateFormatEConverter"/>.
   /// </summary>
   public class DateFormatEConverter : Localization.Converters.DateFormatEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.TimeFormatEConverter"/>.
   /// </summary>
   public class TimeFormatEConverter : Localization.Converters.TimeFormatEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.GsmSmsFormatEConverter"/>.
   /// </summary>
   public class GsmSmsFormatEConverter : Localization.Converters.GsmSmsFormatEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
          return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.GsmPowerModeEConverter"/>.
   /// </summary>
   public class GsmPowerModeEConverter : Localization.Converters.GsmPowerModeEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.GsmPowerModeEReverseConverter"/>.
   /// </summary>
   public class GsmPowerModeEReverseConverter : Localization.Converters.GsmPowerModeEReverseConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.GsmEventMaskEConverter"/>.
   /// </summary>
   public class GsmEventMaskEConverter : Localization.Converters.GsmEventMaskEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.DaylightSavingEConverter"/>.
   /// </summary>
   public class DaylightSavingEConverter : Localization.Converters.DaylightSavingEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.BacklightModeEConverter"/>.
   /// </summary>
   public class BacklightModeEConverter : Localization.Converters.BacklightModeEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.BacklightModeEReverseConverter"/>.
   /// </summary>
   public class BacklightModeEReverseConverter : Localization.Converters.BacklightModeEReverseConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.DisplayModeEConverter"/>.
   /// </summary>
   public class DisplayModeEConverter : Localization.Converters.DisplayModeEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.WeightUnitsEConverter"/>.
   /// </summary>
   public class WeightUnitsEConverter : Localization.Converters.WeightUnitsEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.WeightCapacityEConverter"/>.
   /// </summary>
   public class WeightCapacityEConverter : Localization.Converters.WeightCapacityEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.PredictionModeEConverter"/>.
   /// </summary>
   public class PredictionModeEConverter : Localization.Converters.PredictionModeEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.PredictionModeEReverseConverter"/>.
   /// </summary>
   public class PredictionModeEReverseConverter : Localization.Converters.PredictionModeEReverseConverter, IValueConverter
   {
       #region Implementation of IValueConverter

       public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
       {
           return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
       }

       public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
       {
           return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
       }

       #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.SexEConverter"/>.
   /// </summary>
   public class SexEConverter : Localization.Converters.SexEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.SexDifferentiationEConverter"/>.
   /// </summary>
   public class SexDifferentiationEConverter : Localization.Converters.SexDifferentiationEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.SexDifferentiationEReverseConverter"/>.
   /// </summary>
   public class SexDifferentiationEReverseConverter : Localization.Converters.SexDifferentiationEReverseConverter, IValueConverter
   {
       #region Implementation of IValueConverter

       public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
       {
           return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
       }

       public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
       {
           return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
       }

       #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.PredictionGrowthEConverter"/>.
   /// </summary>
   public class PredictionGrowthEConverter : Localization.Converters.PredictionGrowthEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.OnlineAdjustmentEConverter"/>.
   /// </summary>
   public class OnlineAdjustmentEConverter : Localization.Converters.OnlineAdjustmentEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.PlatformStepModeEConverter"/>.
   /// </summary>
   public class PlatformStepModeEConverter : Localization.Converters.PlatformStepModeEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.WeighingConfigurationMenuMaskEConverter"/>.
   /// </summary>
   public class WeighingConfigurationMenuMaskEConverter : Localization.Converters.WeighingConfigurationMenuMaskEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.HistogramModeEConverter"/>.
   /// </summary>
   public class HistogramModeEConverter : Localization.Converters.HistogramModeEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.HistogramModeEReverseConverter"/>.
   /// </summary>
   public class HistogramModeEReverseConverter : Localization.Converters.HistogramModeEReverseConverter, IValueConverter
   {
       #region Implementation of IValueConverter

       public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
       {
           return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
       }

       public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
       {
           return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
       }

       #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.StatisticShortTypeEConverter"/>.
   /// </summary>
   public class StatisticShortTypeEConverter : Localization.Converters.StatisticShortTypeEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.Rs485ModeEConverter"/>.
   /// </summary>
   public class Rs485ModeEConverter : Localization.Converters.Rs485ModeEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }


   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.GsmPowerModeEReverseConverter"/>.
   /// </summary>
   public class Rs485ModeEReverseConverter : Localization.Converters.Rs485ModeEReverseConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }
   ///// <summary>
   ///// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.SmsGateParityEConverter"/>.
   ///// </summary>
   //public class SmsGateParityEConverter : Localization.Converters.SmsGateParityEConverter, IValueConverter
   //{
   //   #region Implementation of IValueConverter

   //   public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
   //   {
   //      return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
   //   }

   //   public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
   //   {
   //      return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
   //   }

   //   #endregion
   //}

   ///// <summary>
   ///// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.SmsGateDataBitsEConverter"/>.
   ///// </summary>
   //public class SmsGateDataBitsEConverter : Localization.Converters.SmsGateDataBitsEConverter, IValueConverter
   //{
   //   #region Implementation of IValueConverter

   //   public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
   //   {
   //      return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
   //   }

   //   public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
   //   {
   //      return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
   //   }

   //   #endregion
   //}

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.ModbusParityEConverter"/>.
   /// </summary>
   public class ModbusParityEConverter : Localization.Converters.ModbusParityEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   ///// <summary>
   ///// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.ModbusDataBitsEConverter"/>.
   ///// </summary>
   //public class ModbusDataBitsEConverter : Localization.Converters.ModbusDataBitsEConverter, IValueConverter
   //{
   //   #region Implementation of IValueConverter

   //   public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
   //   {
   //      return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
   //   }

   //   public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
   //   {
   //      return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
   //   }

   //   #endregion
   //}

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.ModbusModeEConverter"/>.
   /// </summary>
   public class ModbusModeEConverter : Localization.Converters.ModbusModeEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.DacsVersionEConverter"/>.
   /// </summary>
   public class DacsVersionEConverter : Localization.Converters.DacsVersionEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.WeighingStatusEConverter"/>.
   /// </summary>
   public class WeighingStatusEConverter : Localization.Converters.WeighingStatusEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.DataPublicationInterfaceEConverter"/>.
   /// </summary>
   public class DataPublicationInterfaceEConverter : Localization.Converters.DataPublicationInterfaceEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.GsmPowerModeEReverseConverter"/>.
   /// </summary>
   public class DataPublicationInterfaceEReverseConverter : Localization.Converters.DataPublicationInterfaceEReverseConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }






   /// <summary>
   /// Convert <see cref="System.Drawing.Bitmap"/> to WPF <see cref="System.Windows.Media.Imaging.BitmapImage"/> and back.
   /// </summary>
   public class Bitmap2BitmapImageConverter : IValueConverter
   {

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         var bitmap = value as Bitmap;
         if (bitmap == null || targetType != typeof(ImageSource)) { return null; }
         using (var outStream = new MemoryStream())
         {
            bitmap.Save(outStream, ImageFormat.Bmp);
            outStream.Position = 0;

            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = outStream;
            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
            bitmapImage.EndInit();
            return bitmapImage;
         }
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         var bitmapImage = value as BitmapImage;
         if (bitmapImage == null || targetType != typeof(Bitmap)) { return null; }
         using (var outStream = new MemoryStream())
         {
            BitmapEncoder enc = new BmpBitmapEncoder();
            enc.Frames.Add(BitmapFrame.Create(bitmapImage));
            enc.Save(outStream);
            var bitmap = new Bitmap(outStream);

            return new Bitmap(bitmap);
         }
      }
   }

   /// <summary>
   /// Convert <see cref="Bat2Library.KeyboardKeyE"/> to <see cref="System.Windows.Media.Imaging.BitmapImage"/>  representation.
   /// </summary>
   public class PasswordImageConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         if (targetType != typeof(ImageSource)) { return null; }
         var b2BiConverter = new Bitmap2BitmapImageConverter();
         if (value is KeyboardKeyE)
         {
            switch ((KeyboardKeyE)value)
            {
               case KeyboardKeyE.K_UP:
                  return b2BiConverter.Convert(Resources.KeyUp, targetType, parameter, culture);
               case KeyboardKeyE.K_DOWN:
                  return b2BiConverter.Convert(Resources.KeyDown, targetType, parameter, culture);
               case KeyboardKeyE.K_LEFT:
                  return b2BiConverter.Convert(Resources.KeyLeft, targetType, parameter, culture);
               case KeyboardKeyE.K_RIGHT:
                  return b2BiConverter.Convert(Resources.KeyRight, targetType, parameter, culture);
               case KeyboardKeyE.K_ESC:
                  return b2BiConverter.Convert(Resources.KeyEsc, targetType, parameter, culture);
               case KeyboardKeyE.K_ENTER:
                  return b2BiConverter.Convert(Resources.KeyEnter, targetType, parameter, culture);
            }
         }
         return b2BiConverter.Convert(Resources.KeyNull, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         var b = value as BitmapImage;
         if (b == null) return KeyboardKeyE.K_NULL;
         
         var src = b.UriSource.OriginalString;
         if (src.Contains("KeyUp"))
         {
            return KeyboardKeyE.K_UP;
         }
         if (src.Contains("KeyDown"))
         {
            return KeyboardKeyE.K_DOWN;
         }
         if (src.Contains("KeyLeft"))
         {
            return KeyboardKeyE.K_LEFT;
         }
         if (src.Contains("KeyRight"))
         {
            return KeyboardKeyE.K_RIGHT;
         }
         if (src.Contains("KeyEsc"))
         {
            return KeyboardKeyE.K_ESC;
         }
         if (src.Contains("KeyEnter"))
         {
            return KeyboardKeyE.K_ENTER;
         }
         return KeyboardKeyE.K_NULL;
      }
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.WeighingDaysMaskEConverter"/>.
   /// </summary>
   public class WeighingDaysMaskEConverter : Localization.Converters.WeighingDaysMaskEConverter, IValueConverter
   {
       #region Implementation of IValueConverter

       public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
       {
           return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
       }

       public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
       {
           return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
       }

       #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.WeighingDaysMaskEConverter"/>.
   /// </summary>
   public class WeighingDaysModeEConverter : Localization.Converters.WeighingDaysModeEConverter, IValueConverter
   {
       #region Implementation of IValueConverter

       public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
       {
           return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
       }

       public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
       {
           return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
       }

       #endregion
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.WeighingDaysModeEToBoolConverter"/>.
   /// </summary>
   public class WeighingDaysModeEToBoolConverter : Localization.Converters.WeighingDaysModeEConverter, IValueConverter
   {
       #region Implementation of IValueConverter

       public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
       {
           object o = System.Convert.ToBoolean(value);
           return o;
       }

       public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
       {
           return (WeighingDaysModeE)System.Convert.ToInt32(value);
       }

       #endregion
   }








   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Common.Localization.Converters.SexDifferentiationEConverterToVisibilityConverter"/>.
   /// </summary>
   public class SexDifferentiationEConverterToVisibilityConverter : Localization.Converters.SexDifferentiationEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         var visible = "Visible";
         switch ((int)value) 
         {
            case 1:
               visible = "Visible";
               break;
            case 0:
            case 2:
               visible = "Collapsed";
               break;
            default:
               visible = "Collapsed";
               break;
         }
         return visible;
      }
      #endregion

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }

   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Common.Localization.Converters.SexDifferentiationEConverterToVisibilityConverterInverted"/>.
   /// </summary>
   public class SexDifferentiationEConverterToVisibilityConverterInverted : Localization.Converters.SexDifferentiationEConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         var visible = "Collapsed";
         switch ((int)value)
         {
            case 1:
               visible = "Collapsed";
               break;
            case 0:
            case 2:
               visible = "Visible";
               break;
            default:
               visible = "Visible";
               break;
         }
         return visible;
      }
      #endregion

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }

   public class DecimalDownConverter : IValueConverter 
   {

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
          if (value == null) 
         {
            return 0;
         }
         return System.Convert.ToDouble(((byte)value)) / 10;
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         if (value == null) 
         {
            return 0;
         }
         return ((double)value * 10);
      }
   }

}
