﻿using Bat2Library.Desktop.Properties;

namespace Bat2Library.Desktop.Localization.Converters
{
   /// <summary>
   /// Define the type converter for the DeviceClass Enum
   /// </summary>

   public class DeviceClassEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public DeviceClassEConverter()
         : base(typeof(DeviceClassE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the DeviceClass Enum
   /// </summary>

   public class DeviceModificationEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public DeviceModificationEConverter()
         : base(typeof(DeviceModificationE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the DeviceCoutryE Enum
   /// </summary>

   public class CountryEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public CountryEConverter()
         : base(typeof(CountryE), Resources.ResourceManager)
      {
      }
   }

   public class CodePageEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public CodePageEConverter()
         : base(typeof(CodePageE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the LanguageE Enum
   /// </summary>

   public class LanguageEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public LanguageEConverter()
         : base(typeof(LanguageE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the DateFormatE Enum
   /// </summary>

   public class DateFormatEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public DateFormatEConverter()
         : base(typeof(DateFormatE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the TimeFormatE Enum
   /// </summary>

   public class TimeFormatEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public TimeFormatEConverter()
         : base(typeof(TimeFormatE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the GsmSmsFormatE Enum
   /// </summary>

   public class GsmSmsFormatEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public GsmSmsFormatEConverter()
         : base(typeof(GsmSmsFormatE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the GsmPowerModeE Enum
   /// </summary>

   public class GsmPowerModeEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public GsmPowerModeEConverter()
         : base(typeof(GsmPowerModeE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the GsmPowerModeE Enum
   /// </summary>

   public class GsmPowerModeEReverseConverter : ReverseResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public GsmPowerModeEReverseConverter()
         : base(typeof(GsmPowerModeE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the GsmEventMaskE Enum
   /// </summary>

   public class GsmEventMaskEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public GsmEventMaskEConverter()
         : base(typeof(GsmEventMaskE), Resources.ResourceManager)
      {
      }
   }

   ///// <summary>
   ///// Define the type converter for the TimeSuffixE Enum
   ///// </summary>
   // public class TimeSuffixEConverter : ResourceEnumConverter
   //{
   //   /// <summary>
   //   /// Create a new instance of the converter using translations from the given resource manager
   //   /// </summary>
   //   public TimeSuffixEConverter()
   //      : base(typeof(Bat2Library.TimeSuffixE), Properties.Resources.ResourceManager)
   //   {
   //   }
   //}

   /// <summary>
   /// Define the type converter for the DaylightSavingE Enum
   /// </summary>

   public class DaylightSavingEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public DaylightSavingEConverter()
         : base(typeof(DaylightSavingE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the BacklightModeE Enum
   /// </summary>

   public class BacklightModeEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public BacklightModeEConverter()
         : base(typeof(BacklightModeE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the reverse type converter for the BacklightModeE Enum
   /// </summary>

   public class BacklightModeEReverseConverter : ReverseResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public BacklightModeEReverseConverter()
         : base(typeof(BacklightModeE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the DisplayModeE Enum
   /// </summary>

   public class DisplayModeEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public DisplayModeEConverter()
         : base(typeof(DisplayModeE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the WeightUnitsE Enum
   /// </summary>

   public class WeightUnitsEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public WeightUnitsEConverter()
         : base(typeof(WeightUnitsE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the WeightCapacityE Enum
   /// </summary>

   public class WeightCapacityEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public WeightCapacityEConverter()
         : base(typeof(WeightCapacityE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the PredictionModeE Enum
   /// </summary>

   public class PredictionModeEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public PredictionModeEConverter()
         : base(typeof(PredictionModeE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the PredictionModeE Enum
   /// </summary>

   public class PredictionModeEReverseConverter : ReverseResourceEnumConverter
   {
       /// <summary>
       /// Create a new instance of the converter using translations from the given resource manager
       /// </summary>
       public PredictionModeEReverseConverter()
           : base(typeof(PredictionModeE), Resources.ResourceManager)
       {
       }
   }

   /// <summary>
   /// Define the type converter for the SexE Enum
   /// </summary>

   public class SexEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public SexEConverter()
         : base(typeof(SexE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the SexDifferentiationE Enum
   /// </summary>

   public class SexDifferentiationEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public SexDifferentiationEConverter()
         : base(typeof(SexDifferentiationE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the SexDifferentiationE Enum
   /// </summary>

   public class SexDifferentiationEReverseConverter : ReverseResourceEnumConverter
   {
       /// <summary>
       /// Create a new instance of the converter using translations from the given resource manager
       /// </summary>
       public SexDifferentiationEReverseConverter()
           : base(typeof(SexDifferentiationE), Resources.ResourceManager)
       {
       }
   }

   /// <summary>
   /// Define the type converter for the PredictionGrowthE Enum
   /// </summary>

   public class PredictionGrowthEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public PredictionGrowthEConverter()
         : base(typeof(PredictionGrowthE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the OnlineAdjustmentE Enum
   /// </summary>

   public class OnlineAdjustmentEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public OnlineAdjustmentEConverter()
         : base(typeof(OnlineAdjustmentE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the PlatformStepModeE Enum
   /// </summary>

   public class PlatformStepModeEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public PlatformStepModeEConverter()
         : base(typeof(PlatformStepModeE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the WeighingConfigurationMenuMaskE Enum
   /// </summary>

   public class WeighingConfigurationMenuMaskEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public WeighingConfigurationMenuMaskEConverter()
         : base(typeof(WeighingConfigurationMenuMaskE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the HistogramModeE Enum
   /// </summary>

   public class HistogramModeEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public HistogramModeEConverter()
         : base(typeof(HistogramModeE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the HistogramModeE Enum
   /// </summary>

   public class HistogramModeEReverseConverter : ReverseResourceEnumConverter
   {
       /// <summary>
       /// Create a new instance of the converter using translations from the given resource manager
       /// </summary>
       public HistogramModeEReverseConverter()
           : base(typeof(HistogramModeE), Resources.ResourceManager)
       {
       }
   }

   /// <summary>
   /// Define the type converter for the StatisticShortTypeE Enum
   /// </summary>

   public class StatisticShortTypeEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public StatisticShortTypeEConverter()
         : base(typeof(StatisticShortTypeE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the WeighingDaysMaskE Enum
   /// </summary>

   public class WeighingDaysMaskEConverter : ResourceEnumConverter
   {
       /// <summary>
       /// Create a new instance of the converter using translations from the given resource manager
       /// </summary>
       public WeighingDaysMaskEConverter()
           : base(typeof(WeighingDaysMaskE), Resources.ResourceManager)
       {
       }
   }

    
    /// <summary>
   /// Define the type converter for the WeighingDaysModeE Enum
   /// </summary>

   public class WeighingDaysModeEConverter : ResourceEnumConverter
   {
       /// <summary>
       /// Create a new instance of the converter using translations from the given resource manager
       /// </summary>
       public WeighingDaysModeEConverter()
           : base(typeof(WeighingDaysModeE), Resources.ResourceManager)
       {
       }
   }

   /// <summary>
   /// Define the type converter for the WeighingDaysModeE enum to bool
   /// </summary>

   public class WeighingDaysModeEToBoolConverter : ResourceEnumConverter
   {
       /// <summary>
       /// Create a new instance of the converter using translations from the given resource manager
       /// </summary>
       public WeighingDaysModeEToBoolConverter()
           : base(typeof(WeighingDaysModeE), Resources.ResourceManager)
       {
       }
   }

   /// <summary>
   /// Define the type converter for the Rs485ModeE Enum
   /// </summary>

   public class Rs485ModeEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public Rs485ModeEConverter()
         : base(typeof(Rs485ModeE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the Rs485ModeE Enum
   /// </summary>

   public class Rs485ModeEReverseConverter : ReverseResourceEnumConverter 
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public Rs485ModeEReverseConverter()
         : base(typeof(Rs485ModeE), Resources.ResourceManager)
      {
      }
   }

   ///// <summary>
   ///// Define the type converter for the SmsGateParityE Enum
   ///// </summary>

   //public class SmsGateParityEConverter : ResourceEnumConverter
   //{
   //   /// <summary>
   //   /// Create a new instance of the converter using translations from the given resource manager
   //   /// </summary>
   //   public SmsGateParityEConverter()
   //      : base(typeof(SmsGateParityE), Resources.ResourceManager)
   //   {
   //   }
   //}

   ///// <summary>
   ///// Define the type converter for the SmsGateDataBitsE Enum
   ///// </summary>

   //public class SmsGateDataBitsEConverter : ResourceEnumConverter
   //{
   //   /// <summary>
   //   /// Create a new instance of the converter using translations from the given resource manager
   //   /// </summary>
   //   public SmsGateDataBitsEConverter()
   //      : base(typeof(SmsGateDataBitsE), Resources.ResourceManager)
   //   {
   //   }
   //}

   /// <summary>
   /// Define the type converter for the ModbusParityE Enum
   /// </summary>

   public class ModbusParityEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public ModbusParityEConverter()
         : base(typeof(ModbusParityE), Resources.ResourceManager)
      {
      }
   }

   ///// <summary>
   ///// Define the type converter for the ModbusDataBitsE Enum
   ///// </summary>

   //public class ModbusDataBitsEConverter : ResourceEnumConverter
   //{
   //   /// <summary>
   //   /// Create a new instance of the converter using translations from the given resource manager
   //   /// </summary>
   //   public ModbusDataBitsEConverter()
   //      : base(typeof(ModbusDataBitsE), Resources.ResourceManager)
   //   {
   //   }
   //}

   /// <summary>
   /// Define the type converter for the ModbusTransmissionModeE Enum
   /// </summary>

   public class ModbusModeEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public ModbusModeEConverter()
         : base(typeof(ModbusModeE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the DacsVersionE Enum
   /// </summary>

   public class DacsVersionEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public DacsVersionEConverter()
         : base(typeof(DacsVersionE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the WeighingStatusE Enum
   /// </summary>

   public class WeighingStatusEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public WeighingStatusEConverter()
         : base(typeof(WeighingStatusE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the DataPublicationInterfaceE Enum
   /// </summary>

   public class DataPublicationInterfaceEConverter : ResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public DataPublicationInterfaceEConverter()
         : base(typeof(DataPublicationInterfaceE), Resources.ResourceManager)
      {
      }
   }

   /// <summary>
   /// Define the type converter for the DataPublicationInterfaceE Enum
   /// </summary>

   public class DataPublicationInterfaceEReverseConverter : ReverseResourceEnumConverter
   {
      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      public DataPublicationInterfaceEReverseConverter()
         : base(typeof(DataPublicationInterfaceE), Resources.ResourceManager)
      {
      }
   }

}
