﻿namespace Bat2Library.Bat2Old.DB
{
   public class Record
   {
      public short DayNumber { get; set; }
      public short TimeHour { get; set; }
      public double Weight { get; set; }
      public bool Gender { get; set; }
   }
}
