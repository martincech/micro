﻿using System.Collections.Generic;

namespace Bat2Library.Bat2Old.DB
{
   public class GrowthCurve
   {
      public GrowthCurve()
      {
         Records = new Dictionary<int, double>();
      }

      public string Name { get; set; }

      /// <summary>
      /// Key - Day00 - Day29
      /// Value - Weight00 - Weight29
      /// </summary>
      public IDictionary<int, double> Records { get; set; }
   }
}
