﻿namespace Bat2Library.Bat2Old.DB
{
   public class SetupF : Flock
   {    
      public string Set { get; set; }

      /// <summary>
      /// Get flock name. Put together flock number and name which is optional.
      /// </summary>
      /// <param name="flock">flock</param>
      /// <returns>flock name</returns>
      public static string GetName(SetupF flock)
      {
         var name = "";
         if (flock.Set != null && !flock.Set.Equals(""))
         {
            name += flock.Set + "_";
         }
         name += flock.Number.ToString();

         if (flock.Name != null && !flock.Name.Equals(""))
         {
            name += "_" + flock.Name;
         }
         return name;
      }
   }
}
