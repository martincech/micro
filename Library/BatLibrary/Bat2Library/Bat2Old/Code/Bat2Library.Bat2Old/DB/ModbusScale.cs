﻿namespace Bat2Library.Bat2Old.DB
{
   public class ModbusScale
   {
      public short Address { get; set; }
      public short Port { get; set; }
      public string Name { get; set; }
   }
}
