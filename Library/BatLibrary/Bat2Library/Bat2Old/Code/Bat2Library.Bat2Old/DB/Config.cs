﻿using System;
using Bat2Library.Bat2Old.DB.Helpers;

namespace Bat2Library.Bat2Old.DB
{
   public class Config : Configuration
   {
      public int VersionFw { get; set; }
      public short Build { get; set; }
      public short HwVersion { get; set; }
      public short Id { get; set; }
      public short Language { get; set; }
      public bool StartProgress { get; set; }
      public bool StartWaiting { get; set; }
      public bool StartUseFlock { get; set; }
      public short StartFlock { get; set; }
      public short StartCurveMove { get; set; }
      public DateTime StartDate { get; set; }
      public bool StartFastUseGender { get; set; }
      public double StartFastInitWeightF { get; set; }
      public double StartFastInitWeightM { get; set; }
      public short StartUniRange { get; set; }
      public bool StartOnline { get; set; }
      public short Rs485Address { get; set; }
      public short Compare { get; set; }      
   }
}
