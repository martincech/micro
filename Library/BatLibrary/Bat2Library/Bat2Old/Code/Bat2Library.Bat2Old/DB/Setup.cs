﻿namespace Bat2Library.Bat2Old.DB
{
   public class Setup
   {
      public int Language { get; set; }
      public string ModemName { get; set; }
      public int ModbusSpeed { get; set; }
      public short ModbusParity { get; set; }
      public short ModbusReplayDelay { get; set; }
      public short ModbusSilentInterval { get; set; }
      public short ModbusProtocol { get; set; }
      public short ModbusRepetitions { get; set; }
   }
}
