﻿namespace Bat2Library.Bat2Old.DB.Enums
{
   public enum GrowthCurveE
   {
      NAME,
      DAY00,
      WEIGHT00,
      DAY29 = 59,
      WEIGHT29
   }
}
