﻿namespace Bat2Library.Bat2Old.DB.Enums
{
   public enum SetupFE
   {
      SET,
      NUMBER,
      NAME,
      USE_CURVES,
      USE_GENDER,
      WEIGH_FROM,
      WEIGH_TO,
      FEMALE_DAY00,
      FEMALE_WEIGHT00,
      FEMALE_DAY29 = 65,
      FEMALE_WEIGHT29,
      MALE_DAY00 = 67,
      MALE_WEIGHT00,
      MALE_DAY29 = 125,
      MALE_WEIGHT29
   }
}
