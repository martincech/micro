﻿using System;

namespace Bat2Library.Bat2Old.DB.Helpers
{
   /// <summary>
   /// Flock created from data (Sms database file)
   /// </summary>
   public class DataFlock
   {
      public string Name { get; set; }
      public DateTime StartDate { get; set; }
      public DateTime? EndDate { get; set; }
      public int Scale { get; set; }

      /// <summary>
      /// Create flock name from datetime and serial number.
      /// </summary>
      /// <param name="date">flock's start date</param>
      /// <param name="scale">scale number</param>
      /// <returns>flock name (e.g. "2015_09_20_8124")</returns>
      public static string GetFlockName(DateTime date, int scale)
      {
         var month = date.Month.ToString();
         var day = date.Day.ToString();
         if (month.Length == 1)
         {
            month = month.Insert(0, "0");
         }
         if (day.Length == 1)
         {
            day = day.Insert(0, "0");
         }

         var name = date.Year + "_" + month + "_" + day + "_" + scale;
         return name;
      }
   }
}
