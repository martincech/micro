﻿using System.Collections.Generic;

namespace Bat2Library.Bat2Old.DB.Helpers
{
   public class Configuration
   {
      public Configuration()
      {
         GsmNumbers = new List<string>();
      }

      public short FMarginAbove { get; set; }
      public short FMargineUnder { get; set; }
      public short MMarginAbove { get; set; }
      public short MMarginUnder { get; set; }
      public short FilterValue { get; set; }
      public double Stabilization { get; set; }
      public short StabilizationTime { get; set; }
      public short AutoMode { get; set; }
      public short JumpMode { get; set; }
      public short Units { get; set; }
      public short HistRange { get; set; }
      public short UniRange { get; set; }
      public bool GsmUse { get; set; }
      public bool GsmSendMidnight { get; set; }
      public short GsmPeriodMidnight { get; set; }
      public bool GsmSendRequest { get; set; }
      public bool GsmExecRequest { get; set; }
      public bool GsmCheckNumbers { get; set; }
      public short GsmCount { get; set; }

      /// <summary>
      /// GsmNumber00 - GsmNumber04
      /// </summary>
      public ICollection<string> GsmNumbers { get; set; }
      public short Backlight { get; set; }
      public int Rs485Speed { get; set; }
      public short Rs485Parity { get; set; }
      public short Rs485ReplyDelay { get; set; }
      public short Rs485SilentInterval { get; set; }
      public short Rs485Protocol { get; set; }
      public short CorrectionDay1 { get; set; }
      public short CorrectionDay2 { get; set; }
      public double CorrectionCorrection { get; set; }
   }
}
