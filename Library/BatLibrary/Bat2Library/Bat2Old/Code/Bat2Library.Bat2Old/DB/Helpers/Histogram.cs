﻿namespace Bat2Library.Bat2Old.DB.Helpers
{
   public class Histogram
   {
      public const int Capacity = 39;

      public double Value { get; set; }
      public int Count { get; set; }
   }
}
