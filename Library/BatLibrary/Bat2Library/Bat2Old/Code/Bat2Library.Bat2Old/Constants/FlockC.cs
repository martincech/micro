﻿namespace Bat2Library.Bat2Old.Constants
{
   public class FlockC
   {
      public const int FLOCK_NAME_MAX_LENGTH = 8; // Maximalni delka
      public const byte FLOCK_EMPTY_NUMBER = 0xFF; // Pokud je cislo hejna rovno teto hodnote, hejno neni definovane

      public const byte FLOCK_TIME_LIMIT_EMPTY = 0xFF;
         // Pokud je casove omezeni vazeni rovno teto hodnote, tak se casove neomezuje a vazi se stale

      public const int FLOCK_TIME_LIMIT_MAX_HOUR = 23; // Maximalni hodnota hodiny pro omezeni (0-23hod)
   }
}