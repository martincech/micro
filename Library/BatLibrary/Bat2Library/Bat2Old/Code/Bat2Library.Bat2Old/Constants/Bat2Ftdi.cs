﻿namespace Bat2Library.Bat2Old.Constants
{
   public static class Bat2Ftdi
   {
      public static readonly string USB_DEVICE_NAME = "VEIT USB Reader A";
      public static readonly string USB_DUTCHMAN_NAME = "ComScale USB Reader A";
   }
}