﻿namespace Bat2Library.Bat2Old.Constants
{
   public class GrowthCurveC
   {
      public const int CURVE_MAX_POINTS = 30; // Max pocet bodu s definici rustove krivky
      public const ushort CURVE_MAX_DAY = 999; // Max cislo dne
      public const ushort CURVE_MAX_WEIGHT = 0xFFFF; // Max hmotnost (aby se vlezla do 2 bajtu)
      public const int CURVE_END_WEIGHT = 0; // Tato hmotnost ukoncuje krivku (nulova hmotnost nemuze nikdy byt)

      public const ushort CURVE_FIRST_DAY_NUMBER = 0;
         // Cislo prvniho dne, od ktereho se zacina krmit (= minimalni cislo dne, ktere lze pri zahajeni vazeni zadat)
   }
}