﻿using Bat2Library.Bat2Old.Flash;

namespace Bat2Library.Bat2Old.Constants
{
   public class TConstants
   {
      public const int CURVE_MAX_POINTS = 30; // Max pocet bodu s definici rustove krivky
      public const ushort CURVE_MAX_DAY = 999; // Max cislo dne
      public const ushort CURVE_MAX_WEIGHT = 0xFFFF; // Max hmotnost (aby se vlezla do 2 bajtu)
      public const int CURVE_END_WEIGHT = 0; // Tato hmotnost ukoncuje krivku (nulova hmotnost nemuze nikdy byt)

      public const ushort CURVE_FIRST_DAY_NUMBER = 0;
         // Cislo prvniho dne, od ktereho se zacina krmit (= minimalni cislo dne, ktere lze pri zahajeni vazeni zadat)


      public const int FLOCK_NAME_MAX_LENGTH = 8; // Maximalni delka
      public const byte FLOCK_EMPTY_NUMBER = 0xFF; // Pokud je cislo hejna rovno teto hodnote, hejno neni definovane

      public const byte FLOCK_TIME_LIMIT_EMPTY = 0xFF;
         // Pokud je casove omezeni vazeni rovno teto hodnote, tak se casove neomezuje a vazi se stale

      public const int FLOCK_TIME_LIMIT_MAX_HOUR = 23; // Maximalni hodnota hodiny pro omezeni (0-23hod)

      public const int GENDER_COUNT = (int) TGender._GENDER_COUNT;

      public const int FLOCKS_MAX_COUNT = 10; // Pocet hejn, ktere muze zadat

      // Parametry GSM
      public const int GSM_NUMBER_MAX_LENGTH = 15;
         // Maximalni delka telefonniho cisla bez zakoncovaci nuly, pozor, musi byt mensi nez GSM_MAX_NUMBER v Hardware.h

      public const int GSM_NUMBER_MAX_COUNT = 5; // Maximalni pocet definovanych telefonnich cisel

      public const int CONFIG_RESERVED_SIZE_V150 = 5;

      public const int CONFIG_RESERVED_SIZE_V111 = 11; // Pocet rezervovanych bajtu v TConfig

      public const byte FL_ARCHIVE_EMPTY = 0xFE;
      public const byte FL_ARCHIVE_FULL = 0xFF;

      public const int HISTOGRAM_SLOTS = 39; // pocet sloupcu - sude i liche cislo (max.254)

      // Naraznikove znaky :
      public const byte LG_SAMPLE_EMPTY = 0x7F; // nenaplnene FIFO
      public const byte LG_SAMPLE_FULL = 0xFF; // naplnene FIFO
      public const byte LG_SAMPLE_VALUE = 0x20; // zaznam hodnoty
      public const byte LG_SAMPLE_GENDER = 0x10; // bit pohlavi
      public const byte LG_SAMPLE_WEIGHT_MSB = 0x01; // 17. bit hmotnosti ve flagu

      public const uint LG_SAMPLE_MASK_WEIGHT = 0x01FFFF;
         // Hmotnost je ulozena ve spodnich 17bit, MSB byte je flag (hmotnost tedy zabira 1 spodni bit z flagu)

      public const uint LG_SAMPLE_MASK_HI_WEIGHT = 0x010000; // 17. bit hmotnosti
      public const ushort LG_SAMPLE_MASK_HOUR = 0x001F; // Hodina je ulozena ve spodnich 5 bitech

      // zaznam jednotlivych vazeni :
      public const int FL_LOGGER_SAMPLES = 1801;
      public const int FL_LOGGER_SAMPLE_SIZE = 3; // 3 bajty

      // Naraznikove znaky :
      public const byte ON_SAMPLE_EMPTY = 0x7F; // nenaplnene FIFO
      public const byte ON_SAMPLE_FULL = 0xFF; // naplnene FIFO
      public const byte ON_SAMPLE_WEIGHT = 0x08; // zaznam hmotnosti
      public const byte ON_SAMPLE_SAVED = 0x20; // Vaha by prave ulozila vzorek do pameti
      public const byte ON_SAMPLE_STABLE = 0x10; // Bit ustalene hmotnosti
      public const byte ON_SAMPLE_SIGN = 0x04; // Znamenko hmotnosti
      public const byte ON_SAMPLE_WEIGHT_MSB = 0x03; // 17. a 18. bit hmotnosti ve flagu

      public const uint ON_SAMPLE_MASK_WEIGHT = 0x03FFFF;
         // Hmotnost je ulozena ve spodnich 18bit, MSB byte je flag (hmotnost tedy zabira 2 spodni bity z flagu)

      public const ushort ON_SAMPLE_MASK_HOUR = 0x001F; // Hodina je ulozena ve spodnich 5 bitech

      // zaznam jednotlivych vazeni :
      public const int FL_ONLINE_SAMPLES = 694265;
      public const int FL_ONLINE_SAMPLE_SIZE = 3; // 3 bajty

      // konfiguracni sekce :
      public const int FL_CONFIG_BASE = 0;
      public const int FL_CONFIG_SIZE = 4096;

      public const int ID_MAX_LENGTH = 10; // Maximalni delka identifikacniho cisla bez zakoncovaci nuly

      // archiv statistik & histogramu :
      public const int FL_ARCHIVE_BASE = (FL_CONFIG_BASE + FL_CONFIG_SIZE);
      public const int FL_ARCHIVE_DAY_SIZE = 5614; // sizeof( TArchiveDailyInfo) velikost polozky
      public const int FL_ARCHIVE_DAYS = 371; // pocet polozek

      public const int FL_ARCHIVE_SIZE = (FL_ARCHIVE_DAY_SIZE*FL_ARCHIVE_DAYS);
         // celkem bytu (2082794), konec pameti 2086890

      // Online mereni :
      public const int FL_ONLINE_BASE = FL_ARCHIVE_BASE; // Stejne jako archiv

      public const int FL_ONLINE_SIZE = (FL_ONLINE_SAMPLE_SIZE*FL_ONLINE_SAMPLES);
         // celkem bytu (2082795), konec pameti 2086891
   }
}