using System.Runtime.InteropServices;
using Bat2Library.Bat2Old.Constants;

namespace Bat2Library.Bat2Old.Flash.v111
{
   // struktura konfiguracni sekce :
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct TConfigSection
   {
      // Konfigurace
      public TConfig Config; 

      // Definice jednotlivych hejn
      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.FLOCKS_MAX_COUNT)] 
      public TFlock[] Flocks;

      // Kalibrace 
      public TCalibration Calibration; 

      public void Swap()
      {
         Config.Swap();
         for (var i = 0; i < TConstants.FLOCKS_MAX_COUNT; i++)
         {
            Flocks[i].Swap();
         }
         Calibration.Swap();
      }
   } // 2693 bajtu
}