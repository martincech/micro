using System.Runtime.InteropServices;
using Bat2Library.Bat2Old.Constants;

namespace Bat2Library.Bat2Old.Flash.v111
{
   // Struktura cele Flash
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct TFlash
   {
      public TConfigSection ConfigSection;

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.FL_CONFIG_SIZE - 2693)] 
      public byte[] Reserved;

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.FL_ARCHIVE_DAYS)] 
      public TArchiveDailyInfo[] Archive;

      public void Swap()
      {
         ConfigSection.Swap();
         for (var i = 0; i < TConstants.FL_ARCHIVE_DAYS; i++)
         {
            Archive[i].Swap();
         }
      }
   }
}