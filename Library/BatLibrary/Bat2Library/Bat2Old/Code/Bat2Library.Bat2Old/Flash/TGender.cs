namespace Bat2Library.Bat2Old.Flash
{
   // Rozdeleni pohlavi
   public enum TGender : byte
   {
      GENDER_FEMALE = 0, // Pohlavi samice (pokud se pouziva jen 1 pohlavi, jsou vzdy platne parametry pro samice)
      GENDER_MALE, // Pohlavi samec
      _GENDER_COUNT
   }
}