using System.Runtime.InteropServices;
using Utilities;

namespace Bat2Library.Bat2Old.Flash
{
   // Parametry, ktere zadal pri zahajeni vykrmu. Tyto parametry se v celem prubehu vazeni nemeni.
   // Po skonceni vykrmu se zde drzi vsechny parametry posledniho vykrmu az do doby odstartovani dalsiho vykrmu. Parametry odtut muze tedy vyuzivat
   // historie i po ukonceni vykrmu.
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct TWeighingStart
   {
      public byte Running; // Flag, ze prave probiha krmeni

      public byte WaitingForStart;
         // Flag, ze se prave ceka na opozdeny start krmeni. Ostatni polozky TWeighingStart jsou vyplneny, staci inicializovat archiv atd.

      public byte UseFlock;
         // Flag, zda zvolil krmeni podle urciteho hejna. Pokud ne, zadal zakladni parametry primo a vazi se bez pouziti hejna, za pouziti parametru v TRychlyVykrm.

      public byte CurrentFlock;
         // Pokud se krmi podle hejna, je zde cislo hejna, podle ktereho se prave krmi. Pri ukonceni vykrmu zde zustane cislo hejna, podle ktereho se krmilo.

      public System.UInt16 CurveDayShift;
         // V kolikatem dnu vykrm zacal minus 1, tj. o kolik se ma krivka posunout. Standardne je to 0.

      public TLongDateTime DateTime; // Datum zahajeni vykrmu
      public TQuickWeighing QuickWeighing; // Zde jsou parametry vykrmu pri PouzivatHejno=NO
      public byte UniformityRange; // Rozsah uniformity v +- %
      public byte Online; // Flag, ze probiha online mereni

      public void Swap()
      {
         CurveDayShift = Endian.SwapUInt16(CurveDayShift);
         DateTime.Swap();
         QuickWeighing.Swap();
      }
   } // 19 bajtu
}