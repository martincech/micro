namespace Bat2Library.Bat2Old.Flash
{
   public enum TBacklight : byte
   {
      BACKLIGHT_OFF,
      BACKLIGHT_ON,
      BACKLIGHT_AUTO,
      _BACKLIGHT_COUNT
   }
}