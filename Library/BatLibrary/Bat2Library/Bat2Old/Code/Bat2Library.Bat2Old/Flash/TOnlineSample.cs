using System.Runtime.InteropServices;
using Utilities;

namespace Bat2Library.Bat2Old.Flash
{
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct TOnlineSample
   {
      public byte Flag; // Naraznik
      public System.UInt16 Value; // Hmotnost nebo hodina

      public void Swap()
      {
         Value = Endian.SwapUInt16(Value);
      }
   } // 3 bajty
}