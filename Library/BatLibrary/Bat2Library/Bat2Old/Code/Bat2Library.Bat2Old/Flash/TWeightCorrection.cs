using System.Runtime.InteropServices;
using Utilities;

namespace Bat2Library.Bat2Old.Flash
{
   // Korekcni krivka
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct TWeightCorrection
   {
      public System.UInt16 Day1; // Den prvniho zlomu, do tohoto dne je korekce nulova
      public System.UInt16 Day2; // Den druheho zlomu, v tomto dni a dale se uplatni zadana korekce <Correction>

      public byte Correction;
         // Korekce v desetinach procenta (max. 25.5%) v den Day2. Pokud je hodnota 0, korekce se neuplatnuje.

      public void Swap()
      {
         Day1 = Endian.SwapUInt16(Day1);
         Day2 = Endian.SwapUInt16(Day2);
      }
   } // 5 bajtu
}