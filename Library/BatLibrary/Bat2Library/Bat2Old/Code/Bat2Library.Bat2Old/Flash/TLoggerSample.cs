using System.Runtime.InteropServices;
using Utilities;

namespace Bat2Library.Bat2Old.Flash
{
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct TLoggerSample
   {
      public byte Flag; // naraznik
      public System.UInt16 Value; // vaha nebo hodina (vaha je na 17 bitu)

      public void Swap()
      {
         Value = Endian.SwapUInt16(Value);
      }
   } // 3 bajty
}