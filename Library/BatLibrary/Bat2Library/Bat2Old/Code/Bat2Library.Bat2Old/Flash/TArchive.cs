using System.Runtime.InteropServices;
using Bat2Library.Bat2Old.Constants;
using Utilities;

namespace Bat2Library.Bat2Old.Flash
{
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct TArchive
   {
      public System.UInt16 DayNumber; // Cislo dne od pocatku vykrmu
      public TLongDateTime DateTime; // datum a cas porizeni

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.GENDER_COUNT)] public TStatistic[] Stat;
         // statistika samic a samcu

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.GENDER_COUNT)] public THistogram[] Hist;
         // histogram samic a samcu

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.GENDER_COUNT)] public System.UInt16[] LastAverage;
         // Prumerna vcerejsi hmotnost samic a samcu (pro vypocet daily gain)

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.GENDER_COUNT)] public System.UInt16[] TargetWeight;
         // Normovana hmotnost samic a samcu pro tento den

      public byte RealUniformityUsed;
         // YES/NO, zda se pouzil presny vypocet uniformity, ktery je v promennych RealnUniformitaSamice a RealnUniformitaSamci

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.GENDER_COUNT)] public byte[] RealUniformity;
         // Uniformita samic a samcu vypoctena presne z jednotlivych vzorku

      public void Swap()
      {
         DayNumber = Endian.SwapUInt16(DayNumber);
         DateTime.Swap();
         for (var i = 0; i < TConstants.GENDER_COUNT; i++)
         {
            Stat[i].Swap();
            Hist[i].Swap();
            LastAverage[i] = Endian.SwapUInt16(LastAverage[i]);
            TargetWeight[i] = Endian.SwapUInt16(TargetWeight[i]);
         }
      }
   } // 211 bajtu - pozor, pri prekroceni 255 bajtu predefinovat promennou v SaveContext() a LoadContext()
}