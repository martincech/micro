﻿using System;
using System.Linq;
using Bat2Library.Connection.Interface.Domain.Old;
using System.Collections.Generic;
using System.Text;
using Bat2Library.Bat2Old.Constants;
using Bat2Library.Bat2Old.Flash;

namespace Bat2Library.Bat2Old.Map
{
   public static class MapFromBat2Old
   {
      #region Bat v1.11

      public static Flash.v111.TFlash MapV111(this OldBat2DeviceData flash)
      {
         if (flash == null)
         {
            return new Flash.v111.TFlash();
         }

         try
         {
            var tFlash = new Flash.v111.TFlash
            {
               Archive =
                  flash.Archive != null
                     ? flash.Archive.Select(x => x.Map()).ToArray()
                     : new List<TArchiveDailyInfo>().ToArray(),
               ConfigSection = flash.Configuration.MapV111()
            };

            return tFlash;
         }
         catch (Exception)
         {
            return new Flash.v111.TFlash();
         }
      }

      public static Flash.v111.TConfigSection MapV111(this OldConfiguration config)
      {
         if (config == null)
         {
            return new Flash.v111.TConfigSection();
         }
         try
         {
            var tConfigSection = new Flash.v111.TConfigSection
            {
               Calibration = config.Calibration.Map(),
               Config = new Flash.v111.TConfig
               {
                  AutoMode = config.AutoMode,
                  Backlight = config.Backlight.Map(),
                  Battery = config.Battery,
                  Build = config.Build,
                  ComparisonFlock = config.ComparisonFlock,
                  Filter = config.Filter,
                  Gsm = config.Gsm.MapV111(),
                  HistogramRange = config.HistogramRange,
                  HwVersion = config.HwVersion,
                  IdentificationNumber = (ushort) config.VersionInfo.SerialNumber,
                  JumpMode = (TJumpMode) (byte) config.JumpMode,
                  Language = config.Language,
                  MarginAbove = new[] {config.MarginAboveFemale, config.MarginAboveMale},
                  MarginBelow = new[] {config.MarginBelowFemale, config.MarginBelowMale},
                  Rs485 = config.Rs485.Map(),
                  StabilizationRange = config.StabilizationRange,
                  StabilizationTime = config.StabilizationTime,
                  UniformityRange = config.UniformityRange,
                  Units = config.Units.Map(),
                  Version =
                     (ushort)
                        ((config.VersionInfo.SoftwareBuild << 8) | (config.VersionInfo.SoftwareMajor << 4) |
                         config.VersionInfo.SoftwareMinor),
                  WeighingStart = config.WeighingStart.Map(),
                  WeightCorrection = config.WeightCorrection.Map()
               },
               Flocks = ToFlashFlocks(config.Flocks)
            };

            return tConfigSection;
         }
         catch (Exception)
         {
            return new Flash.v111.TConfigSection();
         }
      }

      public static Flash.v111.TGsm MapV111(this OldGsm gsm)
      {
         if (gsm == null)
         {
            return new Flash.v111.TGsm();
         }

         try
         {
            var tGsm = new Flash.v111.TGsm
            {
               ExecuteOnRequest = gsm.ExecuteOnRequest ? (byte) 1 : (byte) 0,
               CheckNumbers = gsm.CheckNumbers ? (byte) 1 : (byte) 0,
               NumberCount = gsm.NumberCount,
               Numbers = gsm.Numbers.GetNumbers(),
               PeriodMidnight = gsm.PeriodMidnight,
               SendMidnight = gsm.SendMidnight ? (byte) 1 : (byte) 0,
               SendOnRequest = gsm.SendOnRequest ? (byte) 1 : (byte) 0,
               Use = gsm.Use ? (byte) 1 : (byte) 0
            };

            return tGsm;
         }
         catch (Exception)
         {
            return new Flash.v111.TGsm();
         }
      }

      #endregion

      #region Bat v1.50

      public static Flash.v150.TFlash MapV150(this OldBat2DeviceData flash)
      {
         if (flash == null)
         {
            return new Flash.v150.TFlash();
         }

         try
         {
            var tFlash = new Flash.v150.TFlash
            {
               Archive =
                  flash.Archive != null
                     ? flash.Archive.Select(x => x.Map()).ToArray()
                     : new List<TArchiveDailyInfo>().ToArray(),
               ConfigSection = flash.Configuration.MapV150()
            };

            return tFlash;
         }
         catch (Exception)
         {
            return new Flash.v150.TFlash();
         }
      }

      public static Flash.v150.TConfigSection MapV150(this OldConfiguration config)
      {
         if (config == null)
         {
            return new Flash.v150.TConfigSection();
         }
         try
         {
            var tConfigSection = new Flash.v150.TConfigSection
            {
               Calibration = config.Calibration.Map(),
               Config = new Flash.v150.TConfig
               {
                  AutoMode = config.AutoMode,
                  Backlight = config.Backlight.Map(),
                  Build = config.Build,
                  ComparisonFlock = config.ComparisonFlock,
                  Filter = config.Filter,
                  Gsm = config.Gsm.MapV150(),
                  HistogramRange = config.HistogramRange,
                  HwVersion = config.HwVersion,
                  IdentificationNumber = config.VersionInfo.SerialNumberArray,
                  JumpMode = (TJumpMode) (byte) config.JumpMode,
                  Language = config.Language,
                  MarginAbove = new[] {config.MarginAboveFemale, config.MarginAboveMale},
                  MarginBelow = new[] {config.MarginBelowFemale, config.MarginBelowMale},
                  Rs485 = config.Rs485.Map(),
                  StabilizationRange = config.StabilizationRange,
                  StabilizationTime = config.StabilizationTime,
                  UniformityRange = config.UniformityRange,
                  Units = config.Units.Map(),
                  Version =
                     (ushort)
                        ((config.VersionInfo.SoftwareBuild << 8) | (config.VersionInfo.SoftwareMajor << 4) |
                         config.VersionInfo.SoftwareMinor),
                  WeighingStart = config.WeighingStart.Map(),
                  WeightCorrection = config.WeightCorrection.Map()
               },
               Flocks = ToFlashFlocks(config.Flocks)
            };

            return tConfigSection;
         }
         catch (Exception)
         {
            return new Flash.v150.TConfigSection();
         }
      }


      public static Flash.v150.TGsm MapV150(this OldGsm gsm)
      {
         if (gsm == null)
         {
            return new Flash.v150.TGsm();
         }

         try
         {
            var tGsm = new Flash.v150.TGsm
            {
               DayMidnight1 = gsm.DayMidnight1,
               CheckNumbers = gsm.CheckNumbers ? (byte) 1 : (byte) 0,
               NumberCount = gsm.NumberCount,
               Numbers = gsm.Numbers.GetNumbers(),
               SendMidnight = gsm.SendMidnight ? (byte) 1 : (byte) 0,
               SendOnRequest = gsm.SendOnRequest ? (byte) 1 : (byte) 0,
               Use = gsm.Use ? (byte) 1 : (byte) 0,
               MidnightSendHour = gsm.MidnightSendHour,
               MidnightSendMin = gsm.MidnightSendMin,
               PeriodMidnight1 = gsm.PeriodMidnight1,
               PeriodMidnight2 = gsm.PeriodMidnight2
            };

            return tGsm;
         }
         catch (Exception)
         {
            return new Flash.v150.TGsm();
         }
      }

      #endregion

      public static TArchive Map(this OldArchive archive)
      {
         if (archive == null)
         {
            return new TArchive();
         }

         try
         {
            archive.ArchiveItemFemale = archive.ArchiveItemFemale ?? new OldArchiveItem();
            archive.ArchiveItemMale = archive.ArchiveItemMale ?? new OldArchiveItem();

            var tArchive = new TArchive
            {
               DateTime = archive.DateTime.Map(),
               DayNumber = archive.DayNumber,
               Hist = new[] {archive.ArchiveItemFemale.Hist.Map(), archive.ArchiveItemMale.Hist.Map()},
               LastAverage = new[] {archive.ArchiveItemFemale.LastAverage, archive.ArchiveItemMale.LastAverage},
               RealUniformity = new[] {archive.ArchiveItemFemale.RealUniformity, archive.ArchiveItemMale.RealUniformity},
               RealUniformityUsed = archive.RealUniformityUsed ? (byte) 1 : (byte) 0,
               Stat = new[] {archive.ArchiveItemFemale.Stat.Map(), archive.ArchiveItemMale.Stat.Map()},
               TargetWeight = new[] {archive.ArchiveItemFemale.TargetWeight, archive.ArchiveItemMale.TargetWeight}
            };

            return tArchive;
         }
         catch (Exception)
         {
            return new TArchive();
         }
      }

      public static TArchiveDailyInfo Map(this OldArchiveDailyInfo archive)
      {
         if (archive == null)
         {
            return new TArchiveDailyInfo();
         }

         try
         {
            var tArchiveDailyInfo = new TArchiveDailyInfo
            {
               Archive = archive.Archive.Map(),
               Samples = archive.Samples.Select(x => x.Map()).ToArray()
            };

            return tArchiveDailyInfo;
         }
         catch (Exception)
         {
            return new TArchiveDailyInfo();
         }
      }

      public static TCalibration Map(this OldCalibration calibration)
      {
         if (calibration == null)
         {
            return new TCalibration();
         }

         try
         {
            var tCalibration = new TCalibration
            {
               Division = calibration.Division,
               Range = calibration.Range,
               RangeCalibration = calibration.RangeCalibration,
               ZeroCalibration = calibration.ZeroCalibration
            };

            return tCalibration;
         }
         catch (Exception)
         {
            return new TCalibration();
         }
      }

      public static TFlock Map(this OldFlock flock)
      {
         if (flock == null)
         {
            return new TFlock();
         }

         try
         {
            //TODO: How to separate male from female points in collection growthcurve 

            OldCurvePoint[] curve;


            if (Equal(flock.GrowthCurveFemale, flock.GrowthCurveMale))
            {
               curve = flock.GrowthCurveFemale.ToArray();
            }
            else
            {
               curve = flock.GrowthCurveFemale.ToArray();
               curve = curve.Concat(flock.GrowthCurveMale).ToArray();
            }
            var tFlock = new TFlock
            {
               GrowthCurve = curve.Select(x => x.Map()).ToArray(),
               Header = flock.Header.Map()
            };

            return tFlock;
         }
         catch (Exception)
         {
            return new TFlock();
         }
      }

      public static TFlockHeader Map(this OldFlockHeader header)
      {
         if (header == null)
         {
            return new TFlockHeader();
         }

         try
         {
            header.Title = !string.IsNullOrEmpty(header.Title)
               ? new string(header.Title.Take(TConstants.FLOCK_NAME_MAX_LENGTH).ToArray())
               : "";

            header.Title += new string(' ', TConstants.FLOCK_NAME_MAX_LENGTH - header.Title.Length);

            var tHeader = new TFlockHeader
            {
               Number = header.Number,
               Title = Encoding.UTF8.GetBytes(header.Title),
               UseBothGenders = header.UseBothGenders ? (byte) 1 : (byte) 0,
               UseCurves = header.UseCurves ? (byte) 1 : (byte) 0,
               WeighFrom = header.WeighFrom,
               WeighTill = header.WeighTill
            };

            return tHeader;
         }
         catch (Exception)
         {
            return new TFlockHeader();
         }
      }

      public static TCurvePoint Map(this OldCurvePoint curve)
      {
         if (curve == null)
         {
            return new TCurvePoint();
         }

         try
         {
            var tCurve = new TCurvePoint
            {
               Day = curve.Day,
               Weight = curve.Weight
            };

            return tCurve;
         }
         catch (Exception)
         {
            return new TCurvePoint();
         }
      }

      public static THistogram Map(this OldHistogram histogram)
      {
         if (histogram == null)
         {
            return new THistogram();
         }

         try
         {
            var tHistogram = new THistogram
            {
               Center = histogram.Center,
               Slot = histogram.Slot.ToArray(),
               Step = histogram.Step
            };

            return tHistogram;
         }
         catch (Exception)
         {
            return new THistogram();
         }
      }

      public static TLoggerSample Map(this OldLoggerSample logger)
      {
         if (logger == null)
         {
            return new TLoggerSample();
         }

         try
         {
            var tLogger = new TLoggerSample
            {
               Flag = logger.Flag,
               Value = logger.Value
            };

            return tLogger;
         }
         catch (Exception)
         {
            return new TLoggerSample();
         }
      }

      public static TRs485 Map(this OldRs485 rs485)
      {
         if (rs485 == null)
         {
            return new TRs485();
         }

         try
         {
            var tRs485 = new TRs485
            {
               Address = rs485.Address,
               Parity = rs485.Parity,
               Protocol = rs485.Protocol,
               ReplyDelay = rs485.ReplyDelay,
               SilentInterval = rs485.SilentInterval,
               Speed = rs485.Speed
            };

            return tRs485;
         }
         catch (Exception)
         {
            return new TRs485();
         }
      }

      public static TStatistic Map(this OldStatistic statistic)
      {
         if (statistic == null)
         {
            return new TStatistic();
         }

         try
         {
            var tStat = new TStatistic
            {
               Count = statistic.Count,
               X2Suma = statistic.X2Suma,
               XSuma = statistic.XSuma
            };

            return tStat;
         }
         catch (Exception)
         {
            return new TStatistic();
         }
      }

      public static TWeightCorrection Map(this OldWeightCorrection weightCorr)
      {
         if (weightCorr == null)
         {
            return new TWeightCorrection();
         }

         try
         {
            var tWeightCorrection = new TWeightCorrection
            {
               Correction = weightCorr.Correction,
               Day1 = weightCorr.Day1,
               Day2 = weightCorr.Day2
            };

            return tWeightCorrection;
         }
         catch (Exception)
         {
            return new TWeightCorrection();
         }
      }

      public static TWeighingStart Map(this OldWeighingStart weighingStart)
      {
         if (weighingStart == null)
         {
            return new TWeighingStart();
         }

         try
         {
            var tWeighingStart = new TWeighingStart
            {
               CurrentFlock = weighingStart.CurrentFlock,
               CurveDayShift = weighingStart.CurveDayShift,
               DateTime = weighingStart.DateTime.Map(),
               Online = weighingStart.Online,
               QuickWeighing = weighingStart.QuickWeighing.Map(),
               Running = weighingStart.Running,
               UniformityRange = weighingStart.UniformityRange,
               UseFlock = weighingStart.UseFlock,
               WaitingForStart = weighingStart.WaitingForStart
            };

            return tWeighingStart;
         }
         catch (Exception)
         {
            return new TWeighingStart();
         }
      }

      public static TQuickWeighing Map(this OldQuickWeighing weighing)
      {
         if (weighing == null)
         {
            return new TQuickWeighing();
         }

         try
         {
            var tWeighing = new TQuickWeighing
            {
               InitialWeight = new[] {weighing.InitialWeightFemale, weighing.InitialWeightMale},
               UseBothGenders = weighing.UseBothGenders ? (byte) 1 : (byte) 0
            };

            return tWeighing;
         }
         catch (Exception)
         {
            return new TQuickWeighing();
         }
      }

      public static TOnlineSample Map(this OldOnlineSample sample)
      {
         if (sample == null)
         {
            return new TOnlineSample();
         }

         try
         {
            var tOnlineSample = new TOnlineSample
            {
               Flag = sample.Flag,
               Value = sample.Value
            };

            return tOnlineSample;
         }
         catch (Exception)
         {
            return new TOnlineSample();
         }
      }

      public static TWeighing Map(this OldWeighing flash)
      {
         if (flash == null)
         {
            return new TWeighing();
         }

         try
         {
            var tWeighing = new TWeighing();

            return tWeighing;
         }
         catch (Exception)
         {
            return new TWeighing();
         }
      }

      #region Enum mapping

      public static TBacklight Map(this BacklightModeE backlight)
      {
         var tBacklight = TBacklight.BACKLIGHT_ON;

         switch (backlight)
         {
            case BacklightModeE.BACKLIGHT_MODE_AUTO:
               tBacklight = TBacklight.BACKLIGHT_AUTO;
               break;
            case BacklightModeE.BACKLIGHT_MODE_OFF:
               tBacklight = TBacklight.BACKLIGHT_OFF;
               break;
            case BacklightModeE.BACKLIGHT_MODE_ON:
               tBacklight = TBacklight.BACKLIGHT_ON;
               break;
         }
         return tBacklight;
      }

      public static TUnits Map(this WeightUnitsE weightUnits)
      {
         var tUnits = TUnits.UNITS_KG;

         switch (weightUnits)
         {
            case WeightUnitsE.WEIGHT_UNITS_KG:
               tUnits = TUnits.UNITS_KG;
               break;
            case WeightUnitsE.WEIGHT_UNITS_LB:
               tUnits = TUnits.UNITS_LB;
               break;
            case WeightUnitsE.WEIGHT_UNITS_G:
               tUnits = TUnits.UNITS_KG;
               break;
         }
         return tUnits;
      }

      #endregion

      #region Helpers

      private static TLongDateTime Map(this DateTime dateTime)
      {
         return new TLongDateTime
         {
            Year = (ushort) dateTime.Year,
            Month = (byte) dateTime.Month,
            Day = (byte) dateTime.Day,
            Hour = (byte) dateTime.Hour,
            Min = (byte) dateTime.Minute,
         };
      }

      private static bool Equal<T>(IEnumerable<T> a, IEnumerable<T> b)
      {
         var areEqual = true;
         if (a == null && b == null)
         {
            return true;
         }

         if (a == null || b == null)
         {
            return false;
         }

         if (a.Count() != b.Count())
         {
            return false;
         }

         for (var i = 0; i < a.Count(); i++)
         {
            areEqual = areEqual && a.ElementAt(i).Equals(b.ElementAt(i));
         }
         return areEqual;
      }

      private static TFlock[] ToFlashFlocks(IEnumerable<OldFlock> flocks)
      {
         var tFlocks = new TFlock[10];
         for (var i = 0; i < tFlocks.Length; i++)
         {
            tFlocks[i].GrowthCurve = new TCurvePoint[TConstants.CURVE_MAX_POINTS*TConstants.GENDER_COUNT];
            tFlocks[i].Header.Title = new byte[TConstants.FLOCK_NAME_MAX_LENGTH];
         }


         if (flocks == null)
         {
            return tFlocks;
         }

         foreach (var flock in flocks)
         {
            if (flock.Header.Number < 10)
            {
               tFlocks[flock.Header.Number] = flock.Map();
            }
         }

         return tFlocks;
      }

      private static byte[] GetNumbers(this IEnumerable<string> numbers)
      {
         const int size = TConstants.GSM_NUMBER_MAX_COUNT*TConstants.GSM_NUMBER_MAX_LENGTH;

         var byteNumbers = new byte[0];

         if (numbers == null)
         {
            return Enumerable.Repeat((byte) ' ', size).ToArray();
         }

         if (numbers.Count() > TConstants.GSM_NUMBER_MAX_COUNT)
         {
            numbers = numbers.Take(TConstants.GSM_NUMBER_MAX_COUNT);
         }


         foreach (var number in numbers)
         {
            var newNumber = new string(number.Take(TConstants.GSM_NUMBER_MAX_LENGTH).ToArray());
            newNumber += new string(' ', TConstants.GSM_NUMBER_MAX_LENGTH - newNumber.Length);
            byteNumbers = byteNumbers.Concat(Encoding.UTF8.GetBytes(newNumber)).ToArray();
         }

         if (byteNumbers.Length < size)
         {
            byteNumbers = byteNumbers.Concat(Enumerable.Repeat((byte) ' ', size - byteNumbers.Length)).ToArray();
         }


         return byteNumbers;
      }

      #endregion
   }
}