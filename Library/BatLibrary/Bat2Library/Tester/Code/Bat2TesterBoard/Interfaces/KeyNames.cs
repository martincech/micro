namespace Bat2TesterBoard.Interfaces
{
   internal enum KeyNames : byte
   {
      Esc = 0,
      Up,
      Enter,
      Left,
      Down,
      Right
   }
}