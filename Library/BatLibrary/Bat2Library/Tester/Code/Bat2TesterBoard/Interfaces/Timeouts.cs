﻿namespace Bat2TesterBoard.Interfaces
{
   /// <summary>
   /// Values are at miliseconds.
   /// </summary>
   public enum Timeouts
   {
      Classic = 500,
      Extended = 1000 * 60 * 2   // 2 min
   }
}
