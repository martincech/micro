﻿namespace Bat2TesterBoard.Interfaces
{
   public enum Commands : byte
   {
      Keyboard = 0x00,
      Adc = 0x01,
      Rtc = 0x02,
      NwmWrite = 0x03,
      NwmCheck = 0x04,
      Display = 0x05
   }
}
