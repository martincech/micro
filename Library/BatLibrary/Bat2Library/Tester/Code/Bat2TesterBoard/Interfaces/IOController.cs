﻿namespace Bat2TesterBoard.Interfaces
{
   public interface IOController
   {
      IKey[] Keys { get;}
      bool LoadResistorPin { get; set; }
      bool PolarityPin { get; set; }
      bool LoadCellPin { get; set; }
      bool UsbAuxiliaryPowerPin { get; set; }
      bool UsbPowerSwitchPin { get; set; }
      bool UsbDataPin { get; set; }
   }
}
