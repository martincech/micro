using System.Linq;
using Bat2TesterBoard.Interfaces;
using Ftdi;

namespace Bat2TesterBoard.Controllers
{
   public class FtdiIoController : IOController
   {
      #region Private fields

      private readonly FtxxGpio.Pin loadResistorPin;
      private bool loadResistorPinValue;
      private readonly FtxxGpio.Pin polarityPin;
      private bool polarityPinValue;
      private readonly FtxxGpio.Pin loadCellPin;
      private bool loadCellPinValue;
      private readonly FtxxGpio.Pin usbAuxiliaryPowerPin;
      private bool usbAuxiliaryPowerPinValue;
      private readonly FtxxGpio.Pin usbPowerSwitchPin;
      private bool usbPowerSwitchPinValue;
      private readonly FtxxGpio.Pin usbDataPin;
      private bool usbDataPinValue;
      

      private readonly IKey[] keys;
      

      #endregion

      /// <summary>
      ///  Represents a single key of Bat2 keyboard simulator
      /// </summary>
      public class FtdiKey : IKey
      {
         #region Private fields

         private readonly FtxxGpio.Pin pin;

         #endregion

         /// <summary>
         ///  Constructor
         /// </summary>
         /// <param name="pin">pin controlling the key</param>
         public FtdiKey(FtxxGpio.Pin pin)
         {
            this.pin = pin;
         }

         /// <summary>
         ///  Pushes key
         /// </summary>
         public void Push()
         {
            pin.Clear();
         }

         /// <summary>
         ///  Releases key
         /// </summary>
         public void Release()
         {
            pin.Set();
         }
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public FtdiIoController(FtxxGpio gpio)
      {
         var keyBoardpins = new[] {gpio.ADBUS0, gpio.ADBUS1, gpio.ADBUS2, gpio.ADBUS3, gpio.ADBUS4, gpio.ADBUS5};
         keys = (IKey[]) keyBoardpins.Select(pin => new FtdiKey(pin));

         loadResistorPin = gpio.ADBUS3;
         polarityPin = gpio.ADBUS4;
         loadCellPin = gpio.ADBUS7;
         usbAuxiliaryPowerPin = gpio.BDBUS0;
         usbPowerSwitchPin = gpio.BDBUS1;
         usbDataPin = gpio.BDBUS2;

         usbAuxiliaryPowerPin.OutputSet();
         usbPowerSwitchPin.OutputSet();
         usbDataPin.OutputSet();

      }

      #region IODrivers

      public bool LoadResistorPin
      {
         get { return loadResistorPinValue; }
         set
         {
            loadResistorPinValue = value;
            if (value)
            {
               loadResistorPin.Set();
            }
            else
            {
               loadResistorPin.Clear();
            }
         }
      }

      public bool PolarityPin
      {
         get { return polarityPinValue; }
         set
         {
            polarityPinValue = value;
            if (value)
            {
               polarityPin.Set();
            }
            else
            {
               polarityPin.Clear();
            }
         }
      }

      public bool LoadCellPin
      {
         get { return loadCellPinValue; }
         set
         {
            loadCellPinValue = value;
            if (value)
            {
               loadCellPin.Set();
            }
            else
            {
               loadCellPin.Clear();
            }
         }
      }

      public bool UsbAuxiliaryPowerPin
      {
         get { return usbAuxiliaryPowerPinValue; }
         set
         {
            usbAuxiliaryPowerPinValue = value;
            if (value)
            {
               usbAuxiliaryPowerPin.Set();
            }
            else
            {
               usbAuxiliaryPowerPin.Clear();
            }
         }
      }
      public bool UsbPowerSwitchPin
      {
         get { return usbPowerSwitchPinValue; }
         set
         {
            usbPowerSwitchPinValue = value;
            if (value)
            {
               usbPowerSwitchPin.Set();
            }
            else
            {
               usbPowerSwitchPin.Clear();
            }
         }
      }
      public bool UsbDataPin
      {
         get { return usbDataPinValue; }
         set
         {
            usbDataPinValue = value;
            if (value)
            {
               usbDataPin.Set();
            }
            else
            {
               usbDataPin.Clear();
            }
         }
      }


      public IKey[] Keys
      {
         get { return keys; }
      }

      #endregion
   }
}