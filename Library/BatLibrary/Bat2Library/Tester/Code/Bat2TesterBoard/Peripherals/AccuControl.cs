using Bat2TesterBoard.Interfaces;
using Keysight;

namespace Bat2TesterBoard.Peripherals
{
   public class AccuControl
   {
      #region Private fields

      private readonly Keysight34410A multimeter;
      private readonly KeysightE36xx powerSupply;
      private readonly IOController gpio;
      #endregion

      /// <summary>
      ///  Constructor
      /// </summary>
      /// <param name="gpio">Gpio controller</param>
      public AccuControl(IOController gpio)
      {
         this.gpio = gpio;
         LoadResistorConnect();
         PolarityNormal();
         SwitchOff();
         multimeter = new Keysight34410A("USB0::0x0957::0x0607::MY47022753::0::INSTR");
         powerSupply = new KeysightE36xx("ASRL10::INSTR");
         powerSupply.Outputs[0].Current = 3.0;
      }

      public double Voltage
      {
         set
         {
            SwitchOff();
            if (value > -0.1 && value < 0.1) // voltage under threshold, switch off
            {
               return;
            } // otherwise normal operation

            powerSupply.Outputs[0].Voltage = value;
            if (value < 0)
            {
               PolarityReverse();
            }
            else
            {
               PolarityNormal();
            }
            SwitchOn();
         }
      }

      public double Current
      {
         get { return multimeter.Current; }
      }

      #region Private methods

      private void SwitchOn()
      {
         powerSupply.Outputs.TurnOn();
      }

      private void SwitchOff()
      {
         powerSupply.Outputs.TurnOff();
      }

      private void PolarityNormal()
      {
         gpio.PolarityPin = true;
      }

      private void PolarityReverse()
      {
         gpio.PolarityPin = false;
      }

      private void LoadResistorConnect()
      {
         gpio.LoadResistorPin = true;
      }

      private void LoadResistorDisconnect()
      {
         gpio.LoadResistorPin = false;
      }

      #endregion
   }
}