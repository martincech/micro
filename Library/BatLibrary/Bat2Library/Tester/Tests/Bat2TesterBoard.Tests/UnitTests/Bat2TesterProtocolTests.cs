﻿using System.IO;
using System.Threading.Tasks;
using Bat2TesterBoard.Controllers;
using Bat2TesterBoard.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bat2TesterBoard.Tests.UnitTests
{
   [TestClass]
   public class Bat2TesterProtocolTests
   {
      private const int Delay = 100; //ms

      [TestMethod]
      public async Task Rtc_Ok()
      {
         using (var stream = new MemoryStream())
         {
            var tester = new ProtocolController(stream);
            var writeData = new[] {(byte) Replies.Ok};
            var b = tester.ReadRtcAsync();
            stream.Write(writeData, 0, 1);
            // make the stream read the last writen data - simulate loopback
            stream.Seek(-writeData.Length, SeekOrigin.Current);
            var response = await b;
            Assert.AreNotEqual(0, response.Length);
            Assert.AreEqual(1, response.Length);
            Assert.AreEqual((int)Replies.Ok, response[0]);
         }
      }


      [TestMethod]
      public async Task Rtc_TimeoutRaised()
      {
         using (var stream = new MemoryStream())
         {
            var tester = new ProtocolController(stream);
            var writeData = new[] { (byte)Replies.Ok };
            var b = tester.ReadRtcAsync();

            System.Threading.Thread.Sleep((int)Timeouts.Classic + Delay);
            stream.Write(writeData, 0, 1);
            // make the stream read the last writen data - simulate loopback
            stream.Seek(-writeData.Length, SeekOrigin.Current);
            var response = await b;
            Assert.AreEqual(0, response.Length);
         }
      }


      [TestMethod]
      public async Task Display_Ok()
      {
         using (var stream = new MemoryStream())
         {
            var tester = new ProtocolController(stream);
            var writeData = new[] { (byte)Replies.Ok };
            var b = tester.ReadDisplayAsync();
            stream.Write(writeData, 0, 1);
            // make the stream read the last writen data - simulate loopback
            stream.Seek(-writeData.Length, SeekOrigin.Current);
            var response = await b;
            Assert.AreNotEqual(0, response.Length);
            Assert.AreEqual(1, response.Length);
            Assert.AreEqual((int)Replies.Ok, response[0]);
         }
      }

      [TestMethod]
      public async Task Display_TimeoutRaised()
      {
         using (var stream = new MemoryStream())
         {
            var tester = new ProtocolController(stream);
            var writeData = new[] { (byte)Replies.Ok };
            var b = tester.ReadDisplayAsync();

            System.Threading.Thread.Sleep((int)Timeouts.Classic + Delay);
            stream.Write(writeData, 0, 1);
            // make the stream read the last writen data - simulate loopback
            stream.Seek(-writeData.Length, SeekOrigin.Current);
            var response = await b;
            Assert.AreEqual(0, response.Length);
         }
      }

      [TestMethod]
      public async Task Keyboard_Ok()
      {
         using (var stream = new MemoryStream())
         {
            var tester = new ProtocolController(stream);
            var writeData = new byte[] { (byte)Replies.Ok, 4 };
            var b = tester.ReadKeyboardAsync();
            stream.Write(writeData, 0, writeData.Length);
            // make the stream read the last writen data - simulate loopback
            stream.Seek(-writeData.Length, SeekOrigin.Current);
            var response = await b;
            Assert.AreNotEqual(0, response.Length);
            Assert.AreEqual(2, response.Length);
            Assert.AreEqual((int)Replies.Ok, response[0]);
         }
      }

      [TestMethod]
      public async Task Keyboard_TimeoutRaised()
      {
         using (var stream = new MemoryStream())
         {
            var tester = new ProtocolController(stream);
            var writeData = new byte[] { (byte)Replies.Ok, 4 };
            var b = tester.ReadKeyboardAsync();

            System.Threading.Thread.Sleep((int)Timeouts.Classic + Delay);
            stream.Write(writeData, 0, writeData.Length);
            // make the stream read the last writen data - simulate loopback
            stream.Seek(-writeData.Length, SeekOrigin.Current);
            var response = await b;
            Assert.AreEqual(0, response.Length);
         }  
      }

      [TestMethod]
      public async Task Adc_Ok()
      {
         using (var stream = new MemoryStream())
         {
            var tester = new ProtocolController(stream);
            var writeData = new byte[] { (byte)Replies.Ok, 1, 1, 1, 1 };
            var b = tester.ReadAdcAsync();
            stream.Write(writeData, 0, writeData.Length);
            // make the stream read the last writen data - simulate loopback
            stream.Seek(-writeData.Length, SeekOrigin.Current);
            var response = await b;
            Assert.AreNotEqual(0, response.Length);
            Assert.AreEqual(5, response.Length);
            Assert.AreEqual((int)Replies.Ok, response[0]);
         }
      }

      [TestMethod]
      public async Task Adc_TimeoutRaised()
      {
         using (var stream = new MemoryStream())
         {
            var tester = new ProtocolController(stream);
            var writeData = new byte[] { (byte)Replies.Ok, 1, 1, 1, 1 };
            var b = tester.ReadAdcAsync();

            System.Threading.Thread.Sleep((int)Timeouts.Classic + Delay);
            stream.Write(writeData, 0, writeData.Length);
            // make the stream read the last writen data - simulate loopback
            stream.Seek(-writeData.Length, SeekOrigin.Current);
            var response = await b;
            Assert.AreEqual(0, response.Length);
         }
      }

      [TestMethod]
      public async Task Nwm_Write_Ok()
      {
         using (var stream = new MemoryStream())
         {
            var tester = new ProtocolController(stream);
            var writeData = new[] { (byte)Replies.Ok };
            var b = tester.ReadNwmWriteAsync();
            stream.Write(writeData, 0, 1);
            // make the stream read the last writen data - simulate loopback
            stream.Seek(-writeData.Length, SeekOrigin.Current);
            var response = await b;
            Assert.AreNotEqual(0, response.Length);
            Assert.AreEqual(1, response.Length);
            Assert.AreEqual((int)Replies.Ok, response[0]);
         }
      }

      [TestMethod]
      public async Task Nwm_Write_TimeoutRaised()
      {
         using (var stream = new MemoryStream())
         {
            var tester = new ProtocolController(stream);
            var writeData = new byte[] { (byte)Replies.Ok };
            var b = tester.ReadNwmWriteAsync();

            System.Threading.Thread.Sleep((int)Timeouts.Extended + Delay);
            stream.Write(writeData, 0, 1);
            // make the stream read the last writen data - simulate loopback
            stream.Seek(-writeData.Length, SeekOrigin.Current);
            var response = await b;
            Assert.AreEqual(0, response.Length);
         }
      }

      [TestMethod]
      public async Task Nwm_Check_Ok()
      {
         using (var stream = new MemoryStream())
         {
            var tester = new ProtocolController(stream);
            var writeData = new[] { (byte)Replies.Ok };
            var b = tester.ReadNwmCheckAsync();
            stream.Write(writeData, 0, 1);
            // make the stream read the last writen data - simulate loopback
            stream.Seek(-writeData.Length, SeekOrigin.Current);
            var response = await b;
            Assert.AreNotEqual(0, response.Length);
            Assert.AreEqual(1, response.Length);
            Assert.AreEqual((int)Replies.Ok, response[0]);
         }
      }

      [TestMethod]
      public async Task Nwm_Check_TimeoutRaised()
      {
         using (var stream = new MemoryStream())
         {
            var tester = new ProtocolController(stream);
            var writeData = new byte[] { (byte)Replies.Ok };
            var b = tester.ReadNwmCheckAsync();

            System.Threading.Thread.Sleep((int)Timeouts.Extended + Delay);
            stream.Write(writeData, 0, 1);
            // make the stream read the last writen data - simulate loopback
            stream.Seek(-writeData.Length, SeekOrigin.Current);
            var response = await b;
            Assert.AreEqual(0, response.Length);
         }
      }    
   }
}
