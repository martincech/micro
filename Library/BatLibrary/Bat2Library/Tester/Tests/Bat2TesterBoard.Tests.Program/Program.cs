﻿using System.Threading;

namespace Bat2TesterBoard.Tests.Program
{
   class Program
   {
      static void Main(string[] args)
      {
         var testerBoard = new Bat2TesterBoard();

         while (true)
         {
            testerBoard.Keyboard.Esc.Push();
            Thread.Sleep(1000);
            testerBoard.Keyboard.Esc.Release();
            Thread.Sleep(1000);
            testerBoard.Keyboard.Up.Push();
            Thread.Sleep(1000);
            testerBoard.Keyboard.Up.Release();
            Thread.Sleep(1000);
            testerBoard.Keyboard.Enter.Push();
            Thread.Sleep(1000);
            testerBoard.Keyboard.Enter.Release();
            Thread.Sleep(1000);
            testerBoard.Keyboard.Left.Push();
            Thread.Sleep(1000);
            testerBoard.Keyboard.Left.Release();
            Thread.Sleep(1000);
            testerBoard.Keyboard.Down.Push();
            Thread.Sleep(1000);
            testerBoard.Keyboard.Down.Release();
            Thread.Sleep(1000);
            testerBoard.Keyboard.Right.Push();
            Thread.Sleep(1000);
            testerBoard.Keyboard.Right.Release();
            Thread.Sleep(1000);

            testerBoard.LoadCell.Level1();
            testerBoard.LoadCell.Level2();
         }
      }
   }
}
