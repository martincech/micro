#pragma once

#ifndef _MsdSocket_H__
   #define _MsdSocket_H__

#include "SocketIf.h"

public class MsdSocket : virtual public SocketIf
{
public:
   MsdSocket();
   // Constructor

   ~MsdSocket();
   // Destructor

   TYesNo OpenForReceiveCmd(void);
   // Open socket for cmd receive

   TYesNo OpenForSendCmd(void);
   // Open socket for send cmd

   byte State(void);
   // Gets state of socket

   TYesNo Receive(void *Buffer, int Size);
   // Receive into <Buffer> with <Size>

   int ReceiveSize(void);
   // Gets number of received bytes

   TYesNo Send(const void *Buffer, int Size);
   // Send <Buffer> with <Size>

   void Close(void);
   // Close socket

   byte Permission(void);
   //

private:
   byte _State;
};

#endif // _MsdSocket_H__
