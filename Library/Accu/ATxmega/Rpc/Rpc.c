//*****************************************************************************
//
//    Rpc.c    Accu Rpc
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Rpc.h"
#include "Rpc/RpcDef.h"
#include "Max17047/Max17047.h"
#include "Bq2425x/Bq2425x.h"
#include "Cpu/Cpu.h"
#include <string.h>

static TYesNo DataReady( void);
// Data ready

void HidReportIn( void *Data);
// Report in callback

static TAccuRpcCmd Cmd;
static TYesNo DataFlag;

//-----------------------------------------------------------------------------
// Init
//-----------------------------------------------------------------------------

void RpcInit( void)
// Init
{
	DataFlag = NO;
} // Init

//-----------------------------------------------------------------------------
// Start
//-----------------------------------------------------------------------------

void RpcStart( void)
// Start
{
   udc_start();
} // Start

//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void RpcStop( void)
// Stop
{
   udc_stop();
} // Stop

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

void RpcExecute( void)
// Execute
{
TAccuRpcReply Reply;
   
   if(!DataReady()) {
      return;
   }
   memset(&Reply, 0, sizeof(Reply));
   Reply.Reply = AccuRpcReply( Cmd.Cmd, YES);

   switch(Cmd.Cmd) {
      case CMD_MAX17047_READ:
         if(Cmd.Max17047Read.Count > ACCU_RPC_MAX17047_COUNT_MAX) {
            Reply.Reply = AccuRpcReply( Cmd.Cmd, NO);
            break;
         }
         Reply.Max17047Read.Success = Max17047ReadMultiple( Reply.Max17047Read.Data, Cmd.Max17047Read.Address, Cmd.Max17047Read.Count);
         break;
         
      case CMD_MAX17047_WRITE:
         if(Cmd.Max17047Read.Count > ACCU_RPC_MAX17047_COUNT_MAX) {
            Reply.Reply = AccuRpcReply( Cmd.Cmd, NO);
            break;
         }
         Reply.Max17047Write.Success = Max17047WriteMultiple( Cmd.Max17047Write.Data, Cmd.Max17047Write.Address, Cmd.Max17047Write.Count);
         break;
         
      case CMD_BQ2425X_READ:
         Reply.Bq2425xRead.Success = Bq2425xReadMultiple( &Reply.Bq2425xRead.Data);
         break;
      
      case CMD_BQ2425X_WRITE:
         Reply.Bq2425xWrite.Success = Bq2425xWriteMultiple( &Cmd.Bq2425xWrite.Data);
         break;
               
      default:
         Reply.Reply = AccuRpcReply( Cmd.Cmd, NO);
         break;
   }
   
   udi_hid_generic_send_report_in( &Reply);
   InterruptDisable();
   DataFlag = NO;
   InterruptEnable();
} // Execute

//-----------------------------------------------------------------------------
// Ready
//-----------------------------------------------------------------------------

static TYesNo DataReady( void)
// Data ready
{
   TYesNo Ret;
   InterruptDisable();
   Ret = DataFlag;
   InterruptEnable();
   return Ret;
} // Ready

//-----------------------------------------------------------------------------
// Report in
//-----------------------------------------------------------------------------

void HidReportIn( void *Data)
// Report in callback
{
   if(DataReady()) {
      return;
   }
   memcpy(&Cmd, Data, sizeof(Cmd));
   DataFlag = YES;
} // HidReportIn