//*****************************************************************************
//
//    Rpc.h    Accu Rpc
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Rpc_H__
   #define __Rpc_H__

#include "Unisys/Uni.h"

void RpcInit( void);
// Init

void RpcStart( void);
// Start

void RpcStop( void);
// Stop

void RpcExecute( void);
// Execute

#endif