//*****************************************************************************
//
//    RpcDef.h      Accu Rpc protocol definitions
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __RpcDef_H__
   #define __RpcDef_H__

#include "Unisys/Uni.h"
#include "Bq2425x/Bq2425x.h"

typedef enum {
   CMD_MAX17047_READ,
   CMD_MAX17047_WRITE,
   CMD_BQ2425X_READ,
   CMD_BQ2425X_WRITE,
} EAccuRpcCmd;

#ifdef __WIN32__
   #pragma pack( push, 1)              // byte alignment
#endif

//-----------------------------------------------------------------------------
// Cmd
//-----------------------------------------------------------------------------

#define ACCU_RPC_MAX17047_COUNT_MAX    24

// Max17047 Read
typedef struct {
   byte Address;
   byte Count;
} __packed TAccuRpcMax17047ReadCmd;

// Max17047 write
typedef struct {
   byte Address;
   byte Count;
   word Data[ACCU_RPC_MAX17047_COUNT_MAX];
} __packed TAccuRpcMax17047WriteCmd;

// Bq2425x write
typedef struct {
   TBq2425xMemoryMap Data;
} __packed TAccuRpBq2425xWriteCmd;


typedef struct {
   byte Cmd;
   union {
       TAccuRpcMax17047ReadCmd Max17047Read;
       TAccuRpcMax17047WriteCmd Max17047Write;
       TAccuRpBq2425xWriteCmd Bq2425xWrite;
   };
} __packed TAccuRpcCmd;

//-----------------------------------------------------------------------------
// Reply
//-----------------------------------------------------------------------------

// Max17047 Read
typedef struct {
   byte Success;
   word Data[ACCU_RPC_MAX17047_COUNT_MAX];
} __packed TAccuRpcMax17047ReadReply;

// Max17047 write
typedef struct {
   byte Success;
} __packed TAccuRpcMax17047WriteReply;

// Bq2425x read
typedef struct {
   byte Success;
   TBq2425xMemoryMap Data;
} __packed TAccuRpcBq2425xReadReply;

// Bq2425x write
typedef struct {
   byte Success;
} __packed TAccuRpcBq2425xWriteReply;

typedef struct {
   byte Reply;
   union {
      TAccuRpcMax17047ReadReply Max17047Read;
      TAccuRpcMax17047WriteReply Max17047Write;
      TAccuRpcBq2425xReadReply Bq2425xRead;
      TAccuRpcBq2425xWriteReply Bq2425xWrite;
   };
} __packed TAccuRpcReply;

#ifdef __WIN32__
   #pragma pack( pop)                  // original alignment
#endif

#define AccuRpcReply( Cmd, Success)    (Cmd | (Success ? 0x80 : 0x00))

#endif