//*****************************************************************************
//
//    Random.h         Random number
//    Version 1.0      (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Random_H__
   #define __Random_H__

#include "Hardware.h"

#define RandomGet( void)      SysTick->VAL
// Random get

#endif