//******************************************************************************
//
//    SystemTimer.c  System timer
//    Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Hardware.h"
#include "Cpu/Cpu.h"

// timer interrupt :
#define timerEnable()        SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk
#define timerDisable()       SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk
/*
   Reading SysTick->CTRL clears COUNTFLAG => pending interrupt lost
   To be sure no interrupt lose, don't enable/disable timer by setting/clearing TICKINT in SysTick->CTRL
*/
// timer executive :
#define timerExecute()            SysTimerExecute()                            // run user executive

// timer initialization :
#define timerInit()          CpuIrqAttach(SysTick_IRQn, 255, SysTick_Handler);timerPeriodSet();SysTick->VAL = 0;SysTick->CTRL |= SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_TICKINT_Msk

#define SysTickCycles() (1LL * TIMER_PERIOD * F_CPU / 1000)

#if SysTickCycles() > 0x00FFFFFF
   #error Too long timer period
#endif

// timer period :
#define timerPeriodSet()     SysTick->LOAD = SysTickCycles()

// timer handler header :
#define timerHandler()       void __irq SysTick_Handler( void)

// timer interrupt confirm :
#define timerInterruptConfirm()    (void)SysTick->CTRL

timerHandler();