﻿//******************************************************************************
//
//   34410.cs     Keysight 34410A multimeter
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System.Threading;
using Agilent.Agilent34410.Interop;

namespace Keysight
{
   /// <summary>
   /// Keysight 34410A multimter
   /// </summary>
   public class Keysight34410A
   {
      #region Private fields

      private readonly Agilent34410 multimeter;

      #endregion

      #region Public interaface

      /// <summary>
      /// Constructor
      /// </summary>
      public Keysight34410A(string address)
      {
         multimeter = new Agilent34410();
         multimeter.Initialize(address, false, true);
         multimeter.Current.DCCurrent.Configure(-1, Agilent34410ResolutionEnum.Agilent34410ResolutionDefault);
      }

      /// <summary>
      /// Finalize
      /// </summary>
      ~Keysight34410A()
      {
         multimeter.Close();
      }

      /// <summary>
      /// Read current
      /// </summary>
      public double Current
      {
         get
         {
            // Get one sample
            const short sampleCount = 1;

            // Immediate trigger
            multimeter.Trigger.TriggerSource = Agilent34410TriggerSourceEnum.Agilent34410TriggerSourceImmediate;

            // No delay
            multimeter.Trigger.TriggerDelay = 0;

            // Number of readings
            multimeter.Trigger.SampleCount = sampleCount;

            multimeter.Measurement.Initiate();

            // Wait for readings complete
            while (
               multimeter.Measurement.ReadingCount[Agilent34410MemoryTypeEnum.Agilent34410MemoryTypeReadingMemory] !=
               sampleCount)
            {
            }

            Thread.Sleep(1); // multimetr občas hodil error - Query unterminated nebo Data Stale

            // Get Readings
            var readings = multimeter.Measurement.RemoveReadings(sampleCount);

            return readings[0];
         }
      }

      #endregion

   }
}