﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Input;
using System.Windows.Forms;
using System.Windows.Interop;

namespace OnScreenKeyboard.Presentations
{
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow : Window
   {
      [DllImport("User32.dll")]
      public static extern int GetWindowLong(IntPtr hWnd, int nIdex);

      [DllImport("User32.dll")]
      public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

      public MainWindow()
      {
         InitializeComponent();
      }

      private void Window_MouseDown(object sender, MouseButtonEventArgs e)
      {
         if (e.LeftButton == MouseButtonState.Pressed)
         {
            DragMove();
         }
      }

      private void Window_Loaded(object sender, RoutedEventArgs e)
      {
         var a = new WindowInteropHelper(this).Handle;
         var temp = GetWindowLong(a, -20);
         SetWindowLong(a, -20, temp | 0x08000000);
         var hwndSource = HwndSource.FromHwnd(a);
         if (hwndSource != null)
         {
            hwndSource.AddHook(WndProc);
         }

         // Set location for keyboard
         var taskbarHeight = Screen.PrimaryScreen.Bounds.Bottom - Screen.PrimaryScreen.WorkingArea.Bottom;
         var screenHeight = Screen.PrimaryScreen.Bounds.Height;
         var screenWidth = Screen.PrimaryScreen.Bounds.Width;
         Left = (screenWidth - Width) / 2; 
         Top = screenHeight - Height - taskbarHeight;
         
         WindowStartupLocation = WindowStartupLocation.Manual;
         ShowInTaskbar = false;
      }

      private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
      {
         if (msg == 0x0216)
         {
            var rect = (System.Drawing.Rectangle)Marshal.PtrToStructure(lParam, typeof(System.Drawing.Rectangle));
            Left = rect.Left;
            Top = rect.Top;
         }
         return IntPtr.Zero;
      }
   }
}
