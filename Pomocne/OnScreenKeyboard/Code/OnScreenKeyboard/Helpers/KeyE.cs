﻿namespace OnScreenKeyboard.Helpers
{
   public enum KeyE
   {
      Num0 = 0x60,
      Num1 = 0x61,
      Num2 = 0x62,
      Num3 = 0x63,
      Num4 = 0x64,
      Num5 = 0x65,
      Num6 = 0x66,
      Num7 = 0x67,
      Num8 = 0x68,
      Num9 = 0x69,
      Backspace = 0x08,
      //Escape = 0x1b,
      Scan = 0x74,         // is map on F5
      Back = 0x08,
      Menu = 0x4d,
      Home = 0x24,
      CallOn = 0xff,       //TODO Research functionality on real device
      CallOff = 0xff,      //TODO
      Ok = 0x0d,
      ArrowLeft = 0x25,
      ArrowUp = 0x26,
      ArrowRight = 0x27,
      ArrowDown = 0x28,
      Dot = 0xbe,
      F1 = 0x44,  //0x70,
      F2 = 0x4b,  //0x71,
      F3 = 0x42,  //0x72,
      F4 = 0x55,  //0x73
      Ctrl = 0x11
   }
}
