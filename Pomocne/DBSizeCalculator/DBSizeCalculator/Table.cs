﻿using System.Collections.Generic;

namespace DBSizeCalculator
{
   public class Table
   {
       public string Name { get; set; }

       public long RowSize { get; set; }

       public long RowSizeComputed { get; set; }

       public ICollection<Column> Columns { get; set; }

       public int RowsCount { get; set; }

       public long TableSize { get; set; }

       public long TableSizeComputed { get; set; }
   }
}
