﻿namespace DBSizeCalculator
{
   public class Column
   {
       //Name of column
      public string Name { get; set; }

       //Datatype of column
      public string DataType { get; set; }

       //Size of datatype of column in bytes
      public long BaseSizeOfColumn { get; set; }

       //Size of datatype of column defined by user in bytes
       public long EditedSizeOfColumn { get; set; }
   }
}
