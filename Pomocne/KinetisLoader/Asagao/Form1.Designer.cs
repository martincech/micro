﻿namespace KinetisLoader {
   partial class KinetisLoader {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.OpenFileButton = new System.Windows.Forms.Button();
         this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
         this.WriteButton = new System.Windows.Forms.Button();
         this.DeviceComboBox = new System.Windows.Forms.ComboBox();
         this.EraseButton = new System.Windows.Forms.Button();
         this.CancelButton = new System.Windows.Forms.Button();
         this.ProgressBar = new Desktop.WinForms.ProgressBar();
         this.TextBox = new Desktop.WinForms.TextBox();
         this.RefreshButton = new System.Windows.Forms.Button();
         this.NoConnectedLabel = new System.Windows.Forms.TextBox();
         this.MemoryReadButton = new System.Windows.Forms.Button();
         this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
         this.CheckBlankRom = new System.Windows.Forms.Button();
         this.EzPortConnectedButton = new System.Windows.Forms.Button();
         this.SuspendLayout();
         // 
         // OpenFileButton
         // 
         this.OpenFileButton.Location = new System.Drawing.Point(12, 12);
         this.OpenFileButton.Name = "OpenFileButton";
         this.OpenFileButton.Size = new System.Drawing.Size(75, 23);
         this.OpenFileButton.TabIndex = 3;
         this.OpenFileButton.Text = Properties.Resources.OpenFile;
         this.OpenFileButton.UseVisualStyleBackColor = true;
         this.OpenFileButton.Click += new System.EventHandler(this.OpenFileButton_Click);
         // 
         // WriteButton
         // 
         this.WriteButton.Location = new System.Drawing.Point(12, 39);
         this.WriteButton.Name = "WriteButton";
         this.WriteButton.Size = new System.Drawing.Size(156, 23);
         this.WriteButton.TabIndex = 4;
         this.WriteButton.Text = Properties.Resources.EraseAndWrite;
         this.WriteButton.UseVisualStyleBackColor = true;
         this.WriteButton.Click += new System.EventHandler(this.WriteButton_Click);
         // 
         // DeviceComboBox
         // 
         this.DeviceComboBox.FormattingEnabled = true;
         this.DeviceComboBox.Location = new System.Drawing.Point(174, 14);
         this.DeviceComboBox.Name = "DeviceComboBox";
         this.DeviceComboBox.Size = new System.Drawing.Size(118, 21);
         this.DeviceComboBox.TabIndex = 5;
         // 
         // EraseButton
         // 
         this.EraseButton.Location = new System.Drawing.Point(93, 12);
         this.EraseButton.Name = "EraseButton";
         this.EraseButton.Size = new System.Drawing.Size(75, 23);
         this.EraseButton.TabIndex = 7;
         this.EraseButton.Text = Properties.Resources.Erase; 
         this.EraseButton.UseVisualStyleBackColor = true;
         this.EraseButton.Click += new System.EventHandler(this.EraseButton_Click);
         // 
         // CancelButton
         // 
         this.CancelButton.Enabled = false;
         this.CancelButton.Location = new System.Drawing.Point(298, 233);
         this.CancelButton.Name = "CancelButton";
         this.CancelButton.Size = new System.Drawing.Size(100, 27);
         this.CancelButton.TabIndex = 8;
         this.CancelButton.Text = Properties.Resources.Cancel;
         this.CancelButton.UseVisualStyleBackColor = true;
         this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
         // 
         // ProgressBar
         // 
         this.ProgressBar.Location = new System.Drawing.Point(12, 233);
         this.ProgressBar.Name = "ProgressBar";
         this.ProgressBar.Size = new System.Drawing.Size(280, 27);
         this.ProgressBar.TabIndex = 1;
         this.ProgressBar.Value = 0D;
         // 
         // TextBox
         // 
         this.TextBox.Location = new System.Drawing.Point(12, 95);
         this.TextBox.Multiline = true;
         this.TextBox.Name = "TextBox";
         this.TextBox.Size = new System.Drawing.Size(280, 132);
         this.TextBox.TabIndex = 0;
         // 
         // RefreshButton
         // 
         this.RefreshButton.Location = new System.Drawing.Point(299, 11);
         this.RefreshButton.Name = "RefreshButton";
         this.RefreshButton.Size = new System.Drawing.Size(99, 23);
         this.RefreshButton.TabIndex = 10;
         this.RefreshButton.Text = Properties.Resources.Refresh;
         this.RefreshButton.UseVisualStyleBackColor = true;
         this.RefreshButton.Click += new System.EventHandler(this.RefreshButton_Click);
         // 
         // NoConnectedLabel
         // 
         this.NoConnectedLabel.Location = new System.Drawing.Point(174, 15);
         this.NoConnectedLabel.Name = "NoConnectedLabel";
         this.NoConnectedLabel.Size = new System.Drawing.Size(118, 20);
         this.NoConnectedLabel.TabIndex = 11;
         this.NoConnectedLabel.Text = Properties.Resources.NoProgrammers;
         this.NoConnectedLabel.Visible = false;
         // 
         // MemoryReadButton
         // 
         this.MemoryReadButton.Location = new System.Drawing.Point(174, 66);
         this.MemoryReadButton.Name = "MemoryReadButton";
         this.MemoryReadButton.Size = new System.Drawing.Size(118, 23);
         this.MemoryReadButton.TabIndex = 12;
         this.MemoryReadButton.Text = Properties.Resources.ReadMemToFile;
         this.MemoryReadButton.UseVisualStyleBackColor = true;
         this.MemoryReadButton.Click += new System.EventHandler(this.ReadButton_Click);
         // 
         // CheckBlankRom
         // 
         this.CheckBlankRom.Location = new System.Drawing.Point(12, 66);
         this.CheckBlankRom.Name = "CheckBlankRom";
         this.CheckBlankRom.Size = new System.Drawing.Size(156, 23);
         this.CheckBlankRom.TabIndex = 13;
         this.CheckBlankRom.Text = Properties.Resources.TestRomEmptines;
         this.CheckBlankRom.UseVisualStyleBackColor = true;
         this.CheckBlankRom.Click += new System.EventHandler(this.CheckBlankRom_Click);
         // 
         // EzPortConnectedButton
         // 
         this.EzPortConnectedButton.Location = new System.Drawing.Point(174, 39);
         this.EzPortConnectedButton.Name = "EzPortConnectedButton";
         this.EzPortConnectedButton.Size = new System.Drawing.Size(118, 23);
         this.EzPortConnectedButton.TabIndex = 14;
         this.EzPortConnectedButton.Text = Properties.Resources.EzPortConnected;
         this.EzPortConnectedButton.UseVisualStyleBackColor = true;
         this.EzPortConnectedButton.Click += new System.EventHandler(this.button1_Click);
         // 
         // KinetisLoader
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(446, 362);
         this.Controls.Add(this.EzPortConnectedButton);
         this.Controls.Add(this.CheckBlankRom);
         this.Controls.Add(this.MemoryReadButton);
         this.Controls.Add(this.NoConnectedLabel);
         this.Controls.Add(this.RefreshButton);
         this.Controls.Add(this.CancelButton);
         this.Controls.Add(this.EraseButton);
         this.Controls.Add(this.DeviceComboBox);
         this.Controls.Add(this.WriteButton);
         this.Controls.Add(this.OpenFileButton);
         this.Controls.Add(this.ProgressBar);
         this.Controls.Add(this.TextBox);
         this.Name = "KinetisLoader";
         this.Text = "KinetisLoader";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private Desktop.WinForms.TextBox TextBox;
      private Desktop.WinForms.ProgressBar ProgressBar;
      private System.Windows.Forms.Button OpenFileButton;
      private System.Windows.Forms.OpenFileDialog openFileDialog;
      private System.Windows.Forms.Button WriteButton;
      private System.Windows.Forms.ComboBox DeviceComboBox;
      private System.Windows.Forms.Button EraseButton;
      private System.Windows.Forms.Button CancelButton;
      private System.Windows.Forms.Button RefreshButton;
      private System.Windows.Forms.TextBox NoConnectedLabel;
      private System.Windows.Forms.Button MemoryReadButton;
      private System.Windows.Forms.SaveFileDialog saveFileDialog;
      private System.Windows.Forms.Button CheckBlankRom;
      private System.Windows.Forms.Button EzPortConnectedButton;
   }
}

