Upravy StrParser
----------------


1. Rozbit definice Str.txt do jednotlivych
   adresaru
   
2. Ve Str.txt povolit direktivu #include <file>

//3. Zavest formalni konstatnu ENUM_xxx rovnou
//   prvni polozce enumu. Napr. pomoci klicoveho
//   slova ENUM xxx v Str.txt
//   Pozn. Upraveno vcetne UnicodeParser, v radku
//   kde je prvni string prazdny se negeneruje
//   text ale jen konstanta ENUM_ stejna jako
//   nasledujici radek
   
4. Pokud mozno zavest rekurzivni direktivu #include

5. Upravit StringGlue jako parser :
   - eliminace duplicitnich stringu
   - pozor, enumy definovat jako prvni
     (stringy v enumu nelze eliminovat)
     