﻿using Bat2WebTransfer.ModelViews.Applications;

namespace Bat2WebTransfer.SampleData
{
   public class SampleMainWindowViewModel : MainWindowViewModel
   {
      public SampleMainWindowViewModel()
      {
         DbFolderPath = "C:/Users/Vyvoj5/AppData/Local/Bat2";
         //Files.Add(new File {FileName = "Cal", IsChecked = false});
         //Files.Add(new File { FileName = "Config", IsChecked = true });
         //Files.Add(new File { FileName = "Curves", IsChecked = true });
         //Files.Add(new File { FileName = "FCal", IsChecked = false });
         //Files.Add(new File { FileName = "FConfig", IsChecked = false });
         //Files.Add(new File { FileName = "FFlocks", IsChecked = true });
         //Files.Add(new File { FileName = "FStat", IsChecked = false });
         //Files.Add(new File { FileName = "Records", IsChecked = false });
         //Files.Add(new File { FileName = "Setup", IsChecked = false });

         Logs.Add("Load 9 db files");
         Logs.Add("Everything is OK");
      }
   }
}
