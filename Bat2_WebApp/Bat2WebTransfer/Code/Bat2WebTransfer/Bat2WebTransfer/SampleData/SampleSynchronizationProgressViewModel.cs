﻿using Bat2WebTransfer.ModelViews.Applications;

namespace Bat2WebTransfer.SampleData
{
   public class SampleSynchronizationProgressViewModel : SynchronizationProgressViewModel
   {
      public SampleSynchronizationProgressViewModel() : base(null)
      {
         ActualFile = "Curves";
         TotalProgress = 23;
      }
   }
}
