﻿using System.Collections.Generic;
using System.Linq;

namespace Bat2WebTransfer.ModelViews.Helpers
{
   /// <summary>
   /// Check if list contains defined type.
   /// </summary>
   public class DataTypesChecker
   {
      private readonly IEnumerable<string> _files; 

      public DataTypesChecker(IEnumerable<string> files)
      {
         _files = files;
      }

      /// <summary>
      /// List contains flock data.
      /// </summary>
      /// <returns></returns>
      public bool ContainFlocks()
      {
         return _files.Contains("Sms");
      }

      /// <summary>
      /// List contains scale data.
      /// </summary>
      /// <returns></returns>
      public bool ContainScales()
      {
         return _files.Contains("Sms");
      }

      /// <summary>
      /// List contains data (statistic).
      /// </summary>
      /// <returns></returns>
      public bool ContainData()
      {
         return _files.Contains("Sms");
      }

      /// <summary>
      /// List contains curves.
      /// </summary>
      /// <returns></returns>
      public bool ContainCurves()
      {
         return _files.Contains("Flocks") || _files.Contains("FFlocks") ||
                _files.Contains("SetupF") || _files.Contains("Curves");
      }
   }
}
