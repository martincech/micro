﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Threading;
using Bat2WebTransfer.ModelViews.Helpers;
using Bat2WebTransfer.ModelViews.Presentation;
using Desktop.Wpf.Applications;
using Services.WebApi;
using Utilities.Extensions;
using Utilities.Observable;
using MessageBox = System.Windows.MessageBox;

namespace Bat2WebTransfer.ModelViews.Applications
{
   public class MainWindowViewModel : ObservableObject
   {
      #region Private fields

      private string _dbFolderPath;
      private ICommand _selectDbFolderCommand;      
      private ObservableCollection<SynchroFile> _synchronizationTypes;
      private ObservableCollection<string> _files;
      private ObservableCollection<string> _logs;
      private string _serverUrl;
      private ICommand _synchronizeCommand;
      private bool _selectAll;
      private bool _isAllTypesEnabled;
      private bool _synchronizationExecute;
      private ICommand _selectAllCommand;
      private ICommand _defaultFolderCommand;  

      private const string DefaultServer = @"http://www.veit.cz";
      private const string DbFileFilter = "*.db";
      private readonly IEnumerable<string> _defaultFolders = new List<string> {"Bat2", "Bat2s"};

      private string _userName;
      private string _password;

      private event EventHandler<bool> TestLoginFinished;
      private Window _owner;

      #endregion

      public MainWindowViewModel()
      {
         SynchronizationTypes = new ObservableCollection<SynchroFile>
         {
            new SynchroFile(Properties.Resources.Flocks),
            new SynchroFile(Properties.Resources.Scales),
            new SynchroFile(Properties.Resources.Data),
            new SynchroFile(Properties.Resources.Curves)
         };
         SynchronizationTypes.ForEach(f => f.PropertyChanged += SynchroObjectOnPropertyChanged);

         Files = new ObservableCollection<string>();
         Logs = new ObservableCollection<string>();

         ServerUrl = DefaultServer;
         SelectAll = true;
         SetDefaultDbPath();
         TestLoginFinished += OnTestLoginFinished;            
         PropertyChanged += OnPropertyChanged;
      }

      #region Public properties

      public string DbFolderPath
      {
         get
         {
            return _dbFolderPath;
         }
         set
         {
            SetProperty(ref _dbFolderPath, value);
            LoadFilesName(DbFolderPath);
            CheckSynchronizationTypesIsEnabled();
         }
      }

      public ObservableCollection<SynchroFile> SynchronizationTypes { get { return _synchronizationTypes; } private set { SetProperty(ref _synchronizationTypes, value); } }
      public ObservableCollection<string> Files { get { return _files; } set { SetProperty(ref _files, value); } }
      public ObservableCollection<string> Logs { get { return _logs; } set { SetProperty(ref _logs, value); } }
      public string ServerUrl { get { return _serverUrl; } set { SetProperty(ref _serverUrl, value); } }
      public bool SelectAll { get { return _selectAll; } set { SetProperty(ref _selectAll, value); } }
      public bool IsAllTypesEnabled { get { return _isAllTypesEnabled; } set { SetProperty(ref _isAllTypesEnabled, value); } }
      public bool SynchronizationExecute { get { return _synchronizationExecute; } set { SetProperty(ref _synchronizationExecute, value); } }
      public string UserName { get { return _userName; } set { SetProperty(ref _userName, value); } }
      public string Password { get { return _password; } set { SetProperty(ref _password, value); } }

      #endregion

      #region Commands

      /// <summary>
      /// Select database folder command.
      /// </summary>
      public ICommand SelectDbFolderCommand
      {
         get
         {
            if (_selectDbFolderCommand == null)
            {
               _selectDbFolderCommand = new RelayCommand(
                  //execute
                  () =>
                  {
                     var dialog = new FolderBrowserDialog
                     {
                        SelectedPath = DbFolderPath 
                     };                 
                     if (dialog.ShowDialog() == DialogResult.OK)
                     {
                        DbFolderPath = dialog.SelectedPath;
                     }
                  },
                  //can execute
                  () => true
                  );
            }
            return _selectDbFolderCommand;
         }
      }

      /// <summary>
      /// Read database from Bat2 old program and save compatible data to web.
      /// </summary>
      public ICommand SynchronizeCommand
      {
         get
         {
            if (_synchronizeCommand == null)
            {
               _synchronizeCommand = new RelayCommand<Window>(
                  //execute
                  w =>
                  {
                     if (ServerUrl.Equals(""))
                     {
                        MessageBox.Show(Properties.Resources.ServerUrlEmpty, Properties.Resources.Error);
                        return;
                     }

                     _owner = w;
                     Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
                     ServerConnectTest(); //Check user credentials
                  },
                  //can execute
                  w =>
                  {
                     var canExecute = ServerUrl != null && !ServerUrl.Equals("") &&
                         UserName != null && !UserName.Equals("") &&
                         Password != null && !Password.Equals("") &&
                         IsAllTypesEnabled && SynchronizationTypes.Any(type => type.IsChecked);

                     SynchronizationExecute = !canExecute;
                     return canExecute;
                  });
            }
            return _synchronizeCommand;
         }
      }

      /// <summary>
      /// Select/deselect files command.
      /// </summary>
      public ICommand SelectAllCommand
      {
         get
         {
            if (_selectAllCommand == null)
            {
               _selectAllCommand = new RelayCommand(
                  //execute
                  () =>
                  {
                     foreach (var file in SynchronizationTypes)
                     {
                        file.IsChecked = file.Enabled && SelectAll;
                     }
                  },
                  //can execute
                  () => true
                  );
            }
            return _selectAllCommand;
         }
      }

      /// <summary>
      /// Set default database folder.
      /// </summary>
      public ICommand DefaultFolderCommand
      {
         get
         {
            if (_defaultFolderCommand == null)
            {
               _defaultFolderCommand = new RelayCommand(
                  //execute
                  SetDefaultDbPath,
                  //can execute
                  () => true
                  );
            }
            return _defaultFolderCommand;
         }
      }

      #endregion

      #region Private helpers   

      private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
      {         
         ((RelayCommand<Window>)SynchronizeCommand).RaiseCanExecuteChanged();
      }

      /// <summary>
      /// Resolve property dependency (e.g. data can't be synchronize without scales)
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void SynchroObjectOnPropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         var obj = sender as SynchroFile;
         if (obj == null) return;
         OnPropertyChanged(null, null);

         if (e.PropertyName == "IsChecked" && obj.Name.Equals(Properties.Resources.Data) && obj.IsChecked)
         {
            var item = SynchronizationTypes.FirstOrDefault(f => f.Name.Equals(Properties.Resources.Scales));
            if (item == null) return;
            item.IsChecked = true;
         }
         if (e.PropertyName == "IsChecked" && obj.Name.Equals(Properties.Resources.Scales) && !obj.IsChecked)
         {
            var item = SynchronizationTypes.FirstOrDefault(f => f.Name.Equals(Properties.Resources.Data));
            if (item == null) return;
            item.IsChecked = false;
         }
      }

      /// <summary>
      /// Try connect to server and insert message to Log.
      /// </summary>
      private void ServerConnectTest()
      {
         var client = new WebApiClient(ServerUrl, UserName, Password);        
         ThreadPool.QueueUserWorkItem(param =>
         {
            var result = client.TestConnect();

            var dispatcher = System.Windows.Application.Current.Dispatcher;
            dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate
            {
               var connectMsg = Properties.Resources.ConnectServerFail;
               if (result)
               {
                  connectMsg = Properties.Resources.ConnectServerOK;
               }
               Logs.Add(connectMsg);
               InvokeTestLogin(result);
            });
         });
      }

      /// <summary>
      /// Set default database folder.
      /// </summary>
      private void SetDefaultDbPath()
      {
         var path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
         var finalPath = CheckIfSubFolderExist(path, _defaultFolders);
         if (finalPath == null)
         {
            finalPath = "";
            Logs.Add(Properties.Resources.DefaultDbFolderNotExist);
         }
         DbFolderPath = finalPath;
      }

      /// <summary>
      /// Check if any folder exist.
      /// </summary>
      /// <param name="path">Root path</param>
      /// <param name="folders">list of sub folders</param>
      /// <returns>full path if exists, else return null</returns>
      private string CheckIfSubFolderExist(string path, IEnumerable<string> folders)
      {
         return folders.Select(folder => Path.Combine(path, folder)).FirstOrDefault(Directory.Exists);
      }

      /// <summary>
      /// Load files name.
      /// </summary>
      /// <param name="folder">searching folder</param>
      /// <param name="filter">files filter</param>   
      private void LoadFilesName(string folder, string filter = DbFileFilter)
      {
         Files.Clear();
         if (!Directory.Exists(folder)) { return; }

         var loadFiles = Directory.GetFiles(folder, filter);
         foreach (var item in loadFiles)
         {
            var index = item.LastIndexOf(Path.DirectorySeparatorChar);
            var name = item.Substring(index + 1, item.Length - index - 4);

            Files.Add(name);
         }
         Logs.Add(string.Format(Properties.Resources.LoadedFiles, loadFiles.Count(), folder));     
      }

      /// <summary>
      /// Check if synchronization type can be enabled 
      /// (the right db files are founded).
      /// </summary>
      private void CheckSynchronizationTypesIsEnabled()
      {
         var checker = new DataTypesChecker(Files);
         foreach (var item in SynchronizationTypes)
         {
            if (item.Name.Equals(Properties.Resources.Flocks))
            {
               item.Enabled = checker.ContainFlocks();
            }
            else if (item.Name.Equals(Properties.Resources.Scales))
            {
               item.Enabled = checker.ContainScales();
            }
            else if (item.Name.Equals(Properties.Resources.Data))
            {
               item.Enabled = checker.ContainData();
            }
            else if (item.Name.Equals(Properties.Resources.Curves))
            {
               item.Enabled = checker.ContainCurves();
            }
            item.IsChecked = SelectAll;
         }

         // check if all types are enabled or not
         IsAllTypesEnabled = SynchronizationTypes.Any(type => type.Enabled);
      }

      private void InvokeTestLogin(bool result)
      {
         if (TestLoginFinished != null)
         {
            TestLoginFinished(this, result);
         }
      }

      private void OnTestLoginFinished(object sender, bool b)
      {
         Mouse.OverrideCursor = null;
         var selectedFiles = SynchronizationTypes.Where(f => f.IsChecked).Select(x => x.Name).ToList();
         if (selectedFiles.Count == 0)
         {
            Logs.Add(Properties.Resources.NoFileSelected);
            return;
         }       
         if (!b) return;

         var synchro = new SynchronizationProgressView();
         var synchroVm = new SynchronizationProgressViewModel(this);
         synchro.DataContext = synchroVm;
         synchro.Owner = _owner;
         synchro.ShowDialog();   
      }    

      #endregion
   }
}
