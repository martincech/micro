﻿using System;
using System.Collections.Generic;

namespace DataContext
{
   /// <summary>
   /// Simple model for filtering data from database
   /// </summary>
   public class FilterModel
   {
      /// <summary>
      /// First record that should be shown(used for paging)
      /// </summary>
      public int DisplayStart { get; set; }

      /// <summary>
      /// Number of records that should be shown in table
      /// </summary>
      public int DisplayLength { get; set; }

      /// <summary>
      /// Selected flocks array
      /// </summary>
      public IEnumerable<int> SelectedFlocks { get; set; }

      /// <summary>
      /// Date from for filtering
      /// </summary>
      public DateTime From { get; set; }

      /// <summary>
      /// Date to for filtering
      /// </summary>
      public DateTime To { get; set; }

      /// <summary>
      /// Sex for filtering
      /// </summary>
      public Bat2Library.SexE Sex { get; set; }

      /// <summary>
      /// Curve id for filtering
      /// </summary>
      public int CurveId { get; set; }

      /// <summary>
      /// Scale id for filtering
      /// </summary>
      public int ScaleId { get; set; }

      /// <summary>
      /// Flock id for filtering
      /// </summary>
      public int? FlockId { get; set; }
   }
}
