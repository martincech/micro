﻿using System;
using DataModel;
using Microsoft.AspNet.Identity;

namespace DataContext
{
   public class RoleManager : RoleManager<Role, Guid>
   {
      public RoleManager()
         : this(new BatModelContainer())
      {
      }

      public RoleManager(BatModelContainer db)
         : this(new RoleStore(db))
      {
      }

      public RoleManager(IRoleStore<Role, Guid> store)
         : base(store)
      {
      }
   }
}