﻿
//-------------------------------------------------------------------------- ---- 
// <auto-generated>
//     This code was generated from a template.
// 
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;

namespace DataModel
{
    
    public partial class BatModelContainer : DbContext
    {
        public BatModelContainer()
            : base("name=BatModelContainer")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public override int SaveChanges()
        {
           try
           {
              return base.SaveChanges();
           }
           catch (DbEntityValidationException ex)
           {
              var errorMessages = new List<string>();
              foreach (var validationResult in ex.EntityValidationErrors)
              {
                 var entityName = validationResult.Entry.Entity.GetType().Name;
                 foreach (var error in validationResult.ValidationErrors)
                 {
                    errorMessages.Add(entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                 }
              }
    
              var exceptionMessage =
                  string.Format("Exception {0}, {1}", 
                  ex.GetType().ToString(), 
                  "Error list: " + string.Join("\n", errorMessages));
    
              throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
           }
        }
    
        //------------------------------------------------------------------------------
        //  DataSets
        //------------------------------------------------------------------------------
    
        public virtual DbSet<Flock> Flocks { get; set; }
        public virtual DbSet<CurveValue> CurveValues { get; set; }
        public virtual DbSet<Stat> Stats { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Claim> Claims { get; set; }
        public virtual DbSet<Login> Logins { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Configuration> Configurations { get; set; }
        public virtual DbSet<SwitchOnTimes> SwitchOnTimes { get; set; }
        public virtual DbSet<GsmMessage> GsmMessages { get; set; }
        public virtual DbSet<ConfigurationsV2> ConfigurationsV2 { get; set; }
        public virtual DbSet<Curve> Curves { get; set; }
        public virtual DbSet<FlockV2> FlockV2 { get; set; }
        public virtual DbSet<Scale> Scales { get; set; }
    }
}
