//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
namespace DataModel
{
   
   using System;
   using Newtonsoft.Json;
   
   public partial class Correction
   {
      
      public virtual bool UseCurve { get; set; }
      public virtual short Day1 { get; set; }
      public virtual short Day2 { get; set; }
      public virtual double UniformityRange { get; set; }
   }
}
