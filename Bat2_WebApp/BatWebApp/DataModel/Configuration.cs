//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
namespace DataModel
{
    
   
   using System;
   using Newtonsoft.Json;
   
   public partial class Configuration
   {
      #region Constructors
   
      
      
      public Configuration()
      {
         this.Country = new Country();
         this.Display = new Display();
         this.WeightUnit = new WeightUnit();
         this.DataPublication = new DataPublication();
         this.Rs485Options = new Rs485Options();
         this.WeighingConfiguration = new WeighingConfiguration();
         AditionalConstructor();
      }
   
      partial void AditionalConstructor();
   
      #endregion
   
      #region Primary keys
   
      /// <summary>
      /// This is primary key property!
      /// </summary>
      public virtual int Id { get; set; }
   
      #endregion
      public virtual string Password { get; set; }
      public virtual Nullable<int> ScaleId { get; set; }
   
      public virtual Country Country { get; set; }
      public virtual Display Display { get; set; }
      public virtual WeightUnit WeightUnit { get; set; }
      public virtual DataPublication DataPublication { get; set; }
      public virtual Rs485Options Rs485Options { get; set; }
      public virtual WeighingConfiguration WeighingConfiguration { get; set; }
   
      [JsonIgnore]
      public virtual GsmMessage GsmMessage { get; set; }
      [JsonIgnore]
      public virtual Scale Scale { get; set; }
   }
}
