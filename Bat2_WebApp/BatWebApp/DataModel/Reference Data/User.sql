﻿USE [BatApp];
GO

MERGE INTO [dbo].[Users] AS Target
USING (VALUES 
   /*name: admin, password Admin_123, company: veit*/
   (
   N'34175b51-189f-4cbf-a9d9-27adef3c3789',  -- ID
   N'admin',                                 -- NAME
   N'admin@veit.cz',                         -- EMAIL
   0,                                        -- Email confirmed
   N'AFvF/VX+na2ePxTjK3EWWdag4+xWIGLmta/ozU0yYayOoe/FDnyqlAGELj2rTMjOvw==', -- PasswordHash
   N'a3decdf4-4505-405c-8d83-06a0a0999a2d',  -- SecurityStamp
   NULL, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, 1, CURRENT_TIMESTAMP, NULL, NULL)
) AS Source (
    [Id],
    [UserName],
    [Email],
    [EmailConfirmed], 
    [PasswordHash], 
    [SecurityStamp], 
    [PhoneNumber], 
    [PhoneNumberConfirmed], 
    [TwoFactorEnabled], 
    [LockoutEndDateUtc], 
    [LockoutEnabled], 
    [AccessFailedCount], 
    [IsAnonymous], 
    [LastLoginDate], 
    [ApplicationId],
    [ParentId], 
    [CompanyId],
    [CreationDate],
    [Comment],
    [LastPasswordChangedDate]
)
ON Target.[Id] = Source.[Id]
-- Update matched rows
WHEN MATCHED THEN
UPDATE SET
    [UserName] = Source.[UserName],
    [Email] = Source.[Email],
    [EmailConfirmed] = Source.[EmailConfirmed],
    [PasswordHash] = Source.[PasswordHash],
    [SecurityStamp] = Source.[SecurityStamp],
    [PhoneNumber] = Source.[PhoneNumber],
    [PhoneNumberConfirmed] = Source.[PhoneNumberConfirmed],
    [TwoFactorEnabled] = Source.[TwoFactorEnabled],
    [LockoutEndDateUtc] = Source.[LockoutEndDateUtc],
    [LockoutEnabled] = Source.[LockoutEnabled],
    [AccessFailedCount] = Source.[AccessFailedCount],
    [CompanyId] = Source.[CompanyId] ,
    [ParentId]  = Source.[ParentId], 
    [IsAnonymous] = Source.[IsAnonymous], 
    [LastLoginDate] = Source.[LastLoginDate], 
    [CreationDate] = Source.[CreationDate],
    [Comment] = Source.[Comment],
    [LastPasswordChangedDate] = Source.[LastPasswordChangedDate]
-- Insert new rows
WHEN NOT MATCHED BY TARGET THEN
INSERT (
    [Id],
    [UserName],
    [Email],
    [EmailConfirmed], 
    [PasswordHash], 
    [SecurityStamp], 
    [PhoneNumber], 
    [PhoneNumberConfirmed], 
    [TwoFactorEnabled], 
    [LockoutEndDateUtc], 
    [LockoutEnabled], 
    [AccessFailedCount], 
    [IsAnonymous], 
    [LastLoginDate], 
    [ApplicationId],
    [ParentId], 
    [CompanyId],
    [CreationDate],
    [Comment],
    [LastPasswordChangedDate]
)
VALUES (
    [Id],
    [UserName],
    [Email],
    [EmailConfirmed], 
    [PasswordHash], 
    [SecurityStamp], 
    [PhoneNumber], 
    [PhoneNumberConfirmed], 
    [TwoFactorEnabled], 
    [LockoutEndDateUtc], 
    [LockoutEnabled], 
    [AccessFailedCount], 
    [IsAnonymous], 
    [LastLoginDate], 
    [ApplicationId],
    [ParentId], 
    [CompanyId],
    [CreationDate],
    [Comment],
    [LastPasswordChangedDate]
);
GO
