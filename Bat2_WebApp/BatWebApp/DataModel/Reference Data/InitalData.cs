﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Reference_Data
{
   public class InitalData
   {
      public static User GetUser()
      {

         return new User()
         {
            Id = Guid.NewGuid(),
            UserName = "admin",
            PasswordHash = "AFvF/VX+na2ePxTjK3EWWdag4+xWIGLmta/ozU0yYayOoe/FDnyqlAGELj2rTMjOvw==",
            SecurityStamp = "a3decdf4-4505-405c-8d83-06a0a0999a2d",  
            LockoutEnabled = false,
            EmailConfirmed = false,
            PhoneNumberConfirmed = false,
            AccessFailedCount = 0,
            TwoFactorEnabled = false,
            CreationDate = DateTime.Now
         };
      }

      public static IEnumerable<Role> GetRoles()
      {
       
         List<Role> roleList =new List<Role>()
         {
            new Role(){Id = Guid.NewGuid(), Name = "Admin"},
            new Role(){Id = Guid.NewGuid(), Name = "CompanyAdmin"},
            new Role(){Id = Guid.NewGuid(), Name = "FlockManagement"},
            new Role(){Id = Guid.NewGuid(), Name = "User"},
         };
         return roleList;
      }
   }
}
