﻿using System;

namespace DataModel
{
   public partial class User
   {
      #region Constructors

      partial void AditionalConstructor()
      {
         Id = Guid.NewGuid();
      }

      #endregion
   }
}
