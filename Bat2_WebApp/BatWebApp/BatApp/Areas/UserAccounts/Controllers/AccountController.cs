﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BatApp.Areas.UserAccounts.Helpers;
using BatApp.Areas.UserAccounts.Models.Account;
using BatApp.Controllers;
using BatApp.Infrastructure;
using DataModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using IdentityExtensions = BatApp.Areas.UserAccounts.Helpers.IdentityExtensions;

namespace BatApp.Areas.UserAccounts.Controllers
{
   [BatApp.Infrastructure.Filters.Authorize]
   public class AccountController : BaseController
   {
      #region Private Fields & properties


      public AccountController()
         : this(null, null)
      {
      }

      public AccountController(ApplicationSignInManager signInManager, ApplicationUserManager userManager)
      {
         this.signInManager = signInManager;
         this.userManager = userManager;
      }

      private ApplicationSignInManager signInManager;
      private ApplicationUserManager userManager;

      private ApplicationSignInManager SignInManager
      {
         get { return signInManager ?? (signInManager = HttpContext.GetOwinContext().Get<ApplicationSignInManager>()); }
      }

      private ApplicationUserManager UserManager
      {
         get
         {
            return userManager ?? (userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>());
         }
      }

      #endregion

      //
      // GET: /Account/Login
      [AllowAnonymous]
      public ActionResult Login(string returnUrl)
      {
         ViewBag.ReturnUrl = returnUrl;
         return View();
      }

      //
      // POST: /Account/Login
      [HttpPost]
      [AllowAnonymous]
      [ValidateAntiForgeryToken]
      public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
      {
         if (!ModelState.IsValid)
         {
            return View(model);
         }
         
         var result = await
            SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe,
               shouldLockout: true);
         switch (result)
         {
            case SignInStatus.Success:
               return RedirectToLocal(returnUrl ?? "");
            case SignInStatus.LockedOut:
               return View("Lockout");
            default:
               ModelState.AddModelError("", Resources.Resources.InvalidLoginAttempt);
               return View(model);
         }
      }

      // POST: /Account/LogOff
      [HttpPost]
      [ValidateAntiForgeryToken]
      public ActionResult LogOff()
      {
         AuthenticationManager.SignOut();
         //TODO: Cannot login when used this logout
         //return RedirectToAction("Login");
         return RedirectToAction("", "", new { area = "" });
      }

      // GET: /Account/Forbidden
      public ViewResult Forbidden()
      { 
         return View();
      }

      //
      // GET: /Account/Index
      public async Task<ActionResult> Index(ManageMessageId? message)
      {
         var user = await CurrentUser;
         var model = new IndexViewModel
         {
            Name = user.UserName,
            Email = user.Email,
            CreationDate = user.CreationDate,
         };
         return DetailView(message, model);
      }


      //
      // POST: /Account/Index
      [HttpPost]
      //[ValidateAntiForgeryToken]
      public async Task<ActionResult> Index(IndexViewModel model)
      {
         if (!ModelState.IsValid)
         {
            return DetailView(ManageMessageId.Error, model);
         }

         var user = await CurrentUser;
         if (user == null)
         {
            return DetailView(ManageMessageId.Error, model);
         }

         if (!ChangeUserDetails(model, user)) return RedirectToAction("Index");

         var result = await UserManager.UpdateAsync(user);
         if (!result.Succeeded)
         {
            AddErrors(result);
            return View(model);
         }
         await SignInAsync(user, isPersistent: false);
         return RedirectToAction("Index", new {message = ManageMessageId.ChangeSuccess});
      }

      //
      // GET: /Manage/ChangePassword
      public async Task<ActionResult> ChangePassword()
      {
         var user = await CurrentUser;
         var model = new ChangePasswordViewModel
         {
            LastChangedDate = user.LastPasswordChangedDate
         };
         return View(model);
      }

      //
      // POST: /Manage/ChangePassword
      [HttpPost]
      [ValidateAntiForgeryToken]
      public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
      {
         if (!ModelState.IsValid) return View(model);

         var result =
            await
               UserManager.ChangePasswordAsync(IdentityExtensions.GetUserId(User.Identity), model.OldPassword,
                  model.NewPassword);
         if (result.Succeeded)
         {
            var user = await CurrentUser;
            if (user != null)
            {
               user.LastPasswordChangedDate = DateTime.Now;
               await SignInAsync(user, isPersistent: false);
               await UserManager.UpdateAsync(user);
            }
            return RedirectToAction("Index", new {Message = ManageMessageId.ChangePasswordSuccess});
         }
         AddErrors(result);
         return View(model);
      }

      #region Helpers

      private new Task<User> CurrentUser
      {
         get { return UserManager.FindByIdAsync(CurrentUserId); }
      }

      private static bool ChangeUserDetails(IndexViewModel model, User user)
      {
         if (string.Compare(user.Email, model.Email, StringComparison.InvariantCulture) == 0 &&
             string.Compare(user.UserName, model.Name, StringComparison.InvariantCulture) == 0)
         {
            return false;
         }
         user.Email = model.Email;
         user.UserName = model.Name;
         return true;
      }

      private ActionResult DetailView(ManageMessageId? message, IndexViewModel model)
      {
         ViewBag.StatusCode = message;
         switch (message)
         {
            case ManageMessageId.ChangePasswordSuccess:
               ViewBag.StatusMessage = Resources.Resources.YourPasswordChanged;
               break;
            case ManageMessageId.SetPasswordSuccess:
               ViewBag.StatusMessage = Resources.Resources.YourPasswordSet;
               break;
            case ManageMessageId.ChangeSuccess:
               ViewBag.StatusMessage = Resources.Resources.SuccesfulChanged;
               break;
            case ManageMessageId.Error:
               ViewBag.StatusMessage = Resources.Resources.AnErrorOccurred;
               break;
            default:
               ViewBag.StatusMessage = "";
               break;
         }
         return View(model);
      }

      private IAuthenticationManager AuthenticationManager
      {
         get { return HttpContext.GetOwinContext().Authentication; }
      }

      private async Task SignInAsync(User user, bool isPersistent)
      {
         AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie,
            DefaultAuthenticationTypes.TwoFactorCookie);
         AuthenticationManager.SignIn(new AuthenticationProperties {IsPersistent = isPersistent},
            await user.GenerateUserIdentityAsync(UserManager));
      }

      private ActionResult RedirectToLocal(string returnUrl)
      {
         if ( string.IsNullOrEmpty(returnUrl))
         {
            returnUrl = "~/";
         }
         return Redirect(returnUrl);
      }

      public enum ManageMessageId
      {
         ChangeSuccess,
         ChangePasswordSuccess,
         SetPasswordSuccess,
         Error
      }

      #endregion
   }
}