﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BatApp.Areas.UserAccounts.Models.Manage;
using BatApp.Controllers;
using BatApp.Infrastructure;
using DataModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PagedList;
using Constants = DataModel.Constants;

namespace BatApp.Areas.UserAccounts.Controllers
{
   /// <summary>
   /// Manage own user account
   /// </summary>
   [BatApp.Infrastructure.Filters.Authorize(Roles = Constants.ADMIN + ", " + Constants.COMPANY_ADMIN)]
   public class ManageController : BaseController
   {
      #region Private fields and properties

      private const int PAGE_SIZE = 10;


      private ApplicationUserManager userManager;

      private ApplicationUserManager UserManager
      {
         get
         {
            return userManager ?? (userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>());
         }
      }

      private ApplicationRoleManager roleManager;

      public ApplicationRoleManager RoleManager
      {
         get { return roleManager ?? (roleManager = HttpContext.GetOwinContext().Get<ApplicationRoleManager>()); }
      }

      #endregion

      //
      // GET: /Manage/Index
      public ActionResult Index(int? page, IEnumerable<int> selectedCompanies, string search)
      {
         var users = User.IsInRole(Constants.ADMIN)
            ? UserManager.Users.ToList()
            : UserManager.Users.Where(u => u.CompanyId == CurrentUser.CompanyId).ToList();

         if (selectedCompanies != null && selectedCompanies.Count() > 0 && selectedCompanies.First() != 0)
         {
            users = users.Where(x => x.CompanyId == selectedCompanies.First()).ToList();
         }

         if (!string.IsNullOrWhiteSpace(search))
         {
            users = search.Contains("@")
               ? users.Where(u => u.Email.Contains(search)).ToList()
               : users.Where(u => u.UserName.Contains(search)).ToList();
         }
         var viewModel = new IndexViewModel
         {
            Search = search,
            Users = new PagedList<User>(users.OrderBy(u => u.UserName), page ?? 1, PAGE_SIZE),
            Companies = Context.Companies.Select(s=> new SelectListItem(){Text = s.Name,Value = s.Id.ToString()}),
            SelectedCompanies = selectedCompanies
         };
         //viewModel.Companies.(new SelectListItem(){Text = Resources.Resources.SelectCompany,Value = "0"});
         return View(viewModel);
      }

      //
      // GET: /Manage/CreateUser
      public ViewResult CreateUser()
      {
         ViewBag.company = new SelectList(Context.Companies.OrderBy(c => c.Name), "Id", "Name", -1);

         var model = new CreateUserViewModel
         {
            InitialRoles =
               GetRolesRes(FilterRoles(RoleManager.Roles.Select(r => r.Name).ToList())).ToDictionary(k => k, v => false),
            Scales = GetAllScales()
         };
         return View(model);
      }

      [HttpPost]
      public JsonResult GetCompanyScales(int? id)
      {
         if (id == null)
         {
            return Json(Context.Scales.Select(x => new { Value = x.Id.ToString(), Text = x.Name }), JsonRequestBehavior.AllowGet);
         }
         return Json(Context.Scales.Where(s => s.CompanyId == id).Select(x => new { Value = x.Id.ToString(), Text = x.Name }), JsonRequestBehavior.AllowGet);
      }

      //
      // POST: /Manage/CreateUser
      [HttpPost]
      public async Task<ActionResult> CreateUser(CreateUserViewModel createUserViewModel)
      {
         ViewBag.company = new SelectList(Context.Companies.OrderBy(c => c.Name), "Id", "Name",
            createUserViewModel.Company);
         createUserViewModel.Scales = GetAllScales();
         if (!ModelState.IsValid)
         {
            return View(createUserViewModel);
         }

         var user = new User
         {
            UserName = createUserViewModel.Username,
            Email = createUserViewModel.Email,
            CompanyId = createUserViewModel.Company,
            CreationDate = DateTime.Now,
            Comment = createUserViewModel.Comment
         };

         if (!UserManager.IsInRole(CurrentUserId, Constants.ADMIN))
         {
            user.CompanyId = CurrentUser.CompanyId;
            user.ParentId = CurrentUserId;
         }
         else
         {
            if ((createUserViewModel.InitialRoles.Where(x => x.Value)
               .Select(x => x.Key.Split(',').Last()))
               .Contains(Constants.ADMIN))
            {
               user.CompanyId = null;
            }
         }

         var result = await UserManager.CreateAsync(user, createUserViewModel.Password);
         if (!result.Succeeded)
         {
            AddErrors(result);
            return View(createUserViewModel);
         }

         var rolesToAddUserTo =
             createUserViewModel.InitialRoles.Where(x => x.Value).Select(x => x.Key.Split(',').Last()).ToList();


         if (rolesToAddUserTo.Contains(Constants.USER))
         {
            Context.ReassignScales(user.Id, createUserViewModel.SelectedScales ?? new List<int>());
         }

         if (createUserViewModel.InitialRoles != null)
         {

            foreach (var role in rolesToAddUserTo)
            {
               result = await UserManager.AddToRoleAsync(user.Id, role);
               if (result.Succeeded) continue;
               AddErrors(result);
               await UserManager.DeleteAsync(user);
               return View(createUserViewModel);
            }
         }

         return RedirectToAction("Index");
      }

      //
      // GET: /Manage/EditUser
      public async Task<ActionResult> EditUser(Guid id)
      {
         var user = await UserManager.FindByIdAsync(id);
         if (user == null)
         {
            return RedirectToAction("Index");
         }
         ViewBag.company = new SelectList(Context.Companies.OrderBy(c => c.Name), "Id", "Name", user.CompanyId);
         var model = new EditUserViewModel
         {
            UserId = id,
            Username = user.UserName,
            Comment = user.Comment,
            Company = user.CompanyId,
            Scales = GetAllScales(),
            SelectedScales = Context.GetScalesForUser(id).Select(x => x.Id).ToList(),
            Roles =
               GetRolesRes(FilterRoles(RoleManager.Roles.Select(r => r.Name)))
                  .ToDictionary(k => k, v => user.Roles.Any(r => r.Name == v.Split(',').Last()))
         };
         return View(model);
      }

      //
      // POST: /Manage/EditUser
      [HttpPost]
      public async Task<ActionResult> EditUser(EditUserViewModel editUserViewModel)
      {
         editUserViewModel.Scales = GetAllScales();
         if (!ModelState.IsValid)
         {
            return View(editUserViewModel);
         }
         var user = await UserManager.FindByIdAsync(editUserViewModel.UserId);
         if (user == null)
         {
            return RedirectToAction("Index");
         }
         user.Comment = editUserViewModel.Comment;
         user.CompanyId = editUserViewModel.Company;
         user.UserName = editUserViewModel.Username;

         if (!((editUserViewModel.Roles.Where(x => x.Value)
              .Select(x => x.Key.Split(',').Last()))
              .Contains(Constants.USER)))
         {
            editUserViewModel.SelectedScales = null;
         }
        

         // remove from roles
         IdentityResult result;
         foreach (var role in editUserViewModel.Roles.Where(r => !r.Value))
         {
            if (!await UserManager.IsInRoleAsync(user.Id, role.Key.Split(',').Last())) continue;

            result = await UserManager.RemoveFromRoleAsync(user.Id, role.Key.Split(',').Last());
            if (result.Succeeded) continue;
            AddErrors(result);
            return View(editUserViewModel);
         }
         // add to roles
         foreach (var role in editUserViewModel.Roles.Where(r => r.Value))
         {
            if (await UserManager.IsInRoleAsync(user.Id, role.Key.Split(',').Last())) continue;

            result = await UserManager.AddToRoleAsync(user.Id, role.Key.Split(',').Last());
            if (result.Succeeded) continue;
            AddErrors(result);
            return View(editUserViewModel);
         }
         Context.ReassignScales(user.Id, editUserViewModel.SelectedScales ?? new List<int>());

         if (!UserManager.IsInRole(CurrentUserId, Constants.ADMIN))
         {
            user.CompanyId = CurrentUser.CompanyId;
         }

         if ((editUserViewModel.Roles.Where(x => x.Value)
              .Select(x => x.Key.Split(',').Last()))
              .Contains(Constants.ADMIN))
         {
            user.CompanyId = null;
         }

         result = await UserManager.UpdateAsync(user);
         if (result.Succeeded) return RedirectToAction("Index");
         AddErrors(result);
         return View(editUserViewModel);
      }

      public ActionResult Delete(Guid id)
      {
         var user = UserManager.Users.FirstOrDefault(c => c.Id == id);
         if (user == null)
         {
            return HttpNotFound();
         }
         return View(new EditUserViewModel {Username = user.UserName, UserId = user.Id});
      }

      //
      // POST: /Manage/DeleteUser
      [HttpPost, ActionName("Delete")]
      public RedirectToRouteResult DeleteConfirmed(Guid id)
      {
         DeleteUser(id);
         return RedirectToAction("Index");
      }

      private void DeleteUser(Guid id)
      {
         var user = UserManager.FindById(id);
         user.Scales.Clear();
         user.SelectedFlocks.Clear();
         var usersChilds = Context.Users.Where(x => x.ParentId == id);
         foreach (var child in usersChilds)
         {
            DeleteUser(child.Id);
         }
         var result = UserManager.Delete(user);
         if (!result.Succeeded)
         {
            AddErrors(result);
         }
      }

      #region Helpers

      protected IEnumerable<string> FilterRoles(IEnumerable<string> roles)
      {
         if (User.IsInRole(Constants.ADMIN))
         {
            return roles;
         }
         if (User.IsInRole(Constants.COMPANY_ADMIN))
         {
            return roles.Where(x => !x.Equals(Constants.ADMIN));
         }
         return roles.Where(x => !x.Equals(Constants.ADMIN) && !x.Equals(Constants.COMPANY_ADMIN));
      }

      protected IQueryable<SelectListItem> GetAllScales()
      {
         return Context.Scales.Select(x => new SelectListItem {Value = x.Id.ToString(), Text = x.Name});
      }

      protected List<string> GetRolesRes(IEnumerable<string> roles)
      {
         return roles.Select(r => Resources.Resources.ResourceManager.GetString(r) + "," + r).ToList();
      }

      #endregion
   }
}