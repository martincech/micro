﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BatApp.Areas.UserAccounts.Models.Account
{
   public class IndexViewModel
   {
      [Required(ErrorMessage = null, ErrorMessageResourceName = "UserNameRequired", ErrorMessageResourceType = typeof(Resources.Resources))]
      [StringLength(256, ErrorMessage = null, ErrorMessageResourceName = "MaxLength", ErrorMessageResourceType = typeof(Resources.Resources))]
      [Display(ResourceType = typeof (Resources.Resources), Name = "UserName")]
      [DataType(DataType.Text)]
      public String Name { get; set; }

      [StringLength(256, ErrorMessage = null, ErrorMessageResourceName = "MaxLength", ErrorMessageResourceType = typeof(Resources.Resources))]
      [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "EmailFormat", ErrorMessageResourceType = typeof(Resources.Resources))]
      [DataType(DataType.EmailAddress)]
      [Display(ResourceType = typeof (Resources.Resources), Name = "Email")]
      public String Email { get; set; }
     
      [HiddenInput(DisplayValue = false)]
      public bool HasPassword { get; set; }

      [HiddenInput(DisplayValue = false)]
      public DateTime CreationDate { get; set; }
   }
}