﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BatApp.Areas.UserAccounts.Models.Manage
{
   public class CreateUserViewModel
   {
      [Display(ResourceType = typeof (Resources.Resources), Name = "UserName")]
      [Required]
      public string Username { get; set; }

      [Display(ResourceType = typeof (Resources.Resources), Name = "Password")]
      [Required, DataType(DataType.Password)]
      public string Password { get; set; }

      [Display(ResourceType = typeof (Resources.Resources), Name = "PasswordAgain")]
      [Required, DataType(DataType.Password)]
      [Compare("Password", ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "PasswordConfirmNotMatch")]
      public string ConfirmPassword { get; set; }

      [Display(ResourceType = typeof (Resources.Resources), Name = "Company")]
      public int? Company { get; set; }

      [Display(ResourceType = typeof (Resources.Resources), Name = "Email")]
      [EmailAddress]
      public string Email { get; set; }

      [Display(ResourceType = typeof(Resources.Resources), Name = "Comment")]
      [DataType(DataType.MultilineText)]
      public String Comment { get; set; }


      [Display(ResourceType = typeof(Resources.Resources), Name = "InitialRoles")]
      public IDictionary<string, bool> InitialRoles { get; set; }

      [Display(ResourceType = typeof(Resources.Resources), Name = "SelectedScales")]
      public ICollection<int> SelectedScales { get; set; }

      [Display(ResourceType = typeof(Resources.Resources), Name = "Scales")]
      public IEnumerable<System.Web.Mvc.SelectListItem> Scales { get; set; }
   }
}