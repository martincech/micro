using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BatApp.Areas.UserAccounts.Models.Manage
{
   public class EditUserViewModel
   {
      [Required]
      [HiddenInput(DisplayValue = false)]
      public Guid UserId { get; set; }
      
      public string Username { get; set; }
      public int? Company { get; set; }
      public String Comment { get; set; }
      public IDictionary<string, bool> Roles { get; set; }
      private IDictionary<string, string> RolesList { get; set; }
      public ICollection<int> SelectedScales { get; set; }
      public IEnumerable<SelectListItem> Scales { get; set; }
   }
}