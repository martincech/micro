﻿using System;
using System.Security.Claims;
using System.Security.Principal;

namespace BatApp.Areas.UserAccounts.Helpers
{
   public static class IdentityExtensions
   {
      public static Guid GetUserId(this IIdentity identity)
      {
         Guid result;

         var id = Microsoft.AspNet.Identity.IdentityExtensions.GetUserId(identity);

         Guid.TryParse(id, out result);

         return result;
      }

      public static string GetUserName(this IIdentity identity)
      {
         var result = Microsoft.AspNet.Identity.IdentityExtensions.GetUserName(identity);

         return result;
      }

      public static string FindFirstValue(this ClaimsIdentity identity, string claimType)
      {
         var result = Microsoft.AspNet.Identity.IdentityExtensions.FindFirstValue(identity, claimType);

         return result;
      }
   }
}