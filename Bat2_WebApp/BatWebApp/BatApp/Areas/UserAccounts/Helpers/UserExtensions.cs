﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using DataModel;
using Microsoft.AspNet.Identity;

namespace BatApp.Areas.UserAccounts.Helpers
{
   public static class UserExtensions
   {
      public static async Task<ClaimsIdentity> GenerateUserIdentityAsync(this User user, UserManager<User, Guid> manager)
      {
         // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
         var userIdentity = await manager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
         // Add custom user claims here
         return userIdentity;
      }
   }
}