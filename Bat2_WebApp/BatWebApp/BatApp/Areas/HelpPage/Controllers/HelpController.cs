using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Mvc;
using BatApp.Areas.HelpPage.ModelDescriptions;
using BatApp.Areas.HelpPage.Models;

namespace BatApp.Areas.HelpPage.Controllers
{
    /// <summary>
    /// The controller that will handle requests for the help page.
    /// </summary>
    public class HelpController : Controller
    {
        private const string ErrorViewName = "Error";
        private const string ApiVersion = "apiVersion";

        public HelpController()
           : this(GlobalConfiguration.Configuration)
        {
        }

        public HelpController(HttpConfiguration config)
        {
           Configuration = config;
        }   

        public HttpConfiguration Configuration { get; private set; }
       
        public ActionResult Index()
        {
           ViewBag.DocumentationProvider = Configuration.Services.GetDocumentationProvider();
           var models = Configuration.Services.GetApiExplorer().ApiDescriptions;
         
           // Remove apiVersion from documentation and replace by api version number
           foreach (var model in models)
           {
              const string pattern = @"v|V\d+";
              var m = Regex.Match(model.RelativePath, pattern);
              var surroundedStr = "{" + ApiVersion + "}";
              if (m.Success && model.RelativePath.Contains(surroundedStr))
              {
                 model.RelativePath = model.RelativePath.Replace(m.Value + "/", "");
                 model.RelativePath = model.RelativePath.Replace(surroundedStr, m.Value);
              }

              var param = model.ParameterDescriptions.FirstOrDefault(p => p.Name.Equals(ApiVersion));
              if (param != null)
              {
                 model.ParameterDescriptions.Remove(param);
              }
           }
                  
           return View(models);
        }

        public ActionResult Api(string apiId)
        {
            if (!String.IsNullOrEmpty(apiId))
            {
                HelpPageApiModel apiModel = Configuration.GetHelpPageApiModel(apiId);
                if (apiModel != null)
                {
                    return View(apiModel);
                }
            }

            return View(ErrorViewName);
        }

        public ActionResult ResourceModel(string modelName)
        {
            if (!String.IsNullOrEmpty(modelName))
            {
                ModelDescriptionGenerator modelDescriptionGenerator = Configuration.GetModelDescriptionGenerator();
                ModelDescription modelDescription;
                if (modelDescriptionGenerator.GeneratedModels.TryGetValue(modelName, out modelDescription))
                {
                    return View(modelDescription);
                }
            }

            return View(ErrorViewName);
        }
    }
}