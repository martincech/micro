﻿using DataModel;
// ReSharper disable InconsistentNaming

namespace BatApp.Models.DTO
{
   /// <summary>
   /// Data transfer object for FlockV2
   /// </summary>
   public class FlockV2DTO : FlockV2
   {
      /// <summary>
      /// Transfer object for male curve
      /// </summary>
      public CurveDTO CurveMaleDTO { get; set; }
      /// <summary>
      /// Transfer object for female curve
      /// </summary>
      public CurveDTO CurveFemaleDTO { get; set; }
   }
}
