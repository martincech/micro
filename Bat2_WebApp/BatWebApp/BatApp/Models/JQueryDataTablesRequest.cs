﻿using System;
using Bat2Library;
using DataContext;

namespace BatApp.Models
{
   /// <summary>
   /// Class that encapsulates most common parameters sent by DataTables plugin
   /// </summary>
   public class JQueryDataTablesRequest
   {
      // ReSharper disable InconsistentNaming

      /// <summary>
      /// Request sequence number sent by DataTable,
      /// same value must be returned in response
      /// </summary>       
      public int sEcho { get; set; }

      /// <summary>
      /// Text used for filtering
      /// </summary>
      public string sSearch { get; set; }

      /// <summary>
      /// Number of records that should be shown in table
      /// </summary>
      public int iDisplayLength { get; set; }

      /// <summary>
      /// First record that should be shown(used for paging)
      /// </summary>
      public int iDisplayStart { get; set; }

      /// <summary>
      /// Number of columns in table
      /// </summary>
      public int iColumns { get; set; }

      /// <summary>
      /// Number of columns that are used in sorting
      /// </summary>
      public int iSortingCols { get; set; }

      /// <summary>
      /// Comma separated list of column names
      /// </summary>
      public string sColumns { get; set; }

      /// <summary>
      /// Selected flocks array
      /// </summary>
      public int[] selectedFlocks { get; set; }

      /// <summary>
      /// Comma separated list of selected flocks
      /// </summary>
      public DateTime from { get; set; }

      /// <summary>
      /// Comma separated list of selected flocks
      /// </summary>
      public DateTime to { get; set; }

      /// <summary>
      /// Comma separated list of selected flocks
      /// </summary>
      public string sex { get; set; }

      public int curveId { get; set; }

      public int scaleId { get; set; }

      public int? flockId { get; set; }

      // ReSharper restore InconsistentNaming

      public FilterModel ToFilterModel()
      {
         return new FilterModel
         {
            DisplayStart = iDisplayStart,
            DisplayLength = iDisplayLength,
            SelectedFlocks = selectedFlocks,
            From = from,
            To = to,
            Sex = sex == Resources.Resources.SexMale ? SexE.SEX_MALE : sex == Resources.Resources.SexFemale ? SexE.SEX_FEMALE : SexE.SEX_UNDEFINED,
            CurveId = curveId,
            ScaleId = scaleId,
            FlockId = flockId
         };
      }
   }
}