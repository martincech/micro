﻿using System;
using System.Web.Mvc;
using BatApp.Helpers.ModelBinders;

namespace BatApp
{
   public class BindersConfig
   {
      public static void Bind(ModelBinderDictionary binders)
      {
         binders.Add(typeof(DateTime), new DateTimeBinder());
         binders.Add(typeof(DateTime?), new DateTimeBinder());
         binders.Add(typeof(int[]), new IntArrayModelBinder());
         //binders.Add(typeof(JQueryDataTablesRequest), new JQueryDataTablesRequestBinder());
      }
   }
}