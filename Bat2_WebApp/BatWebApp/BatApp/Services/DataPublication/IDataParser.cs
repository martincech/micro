﻿using DataModel;

namespace BatApp.Services.DataPublication
{
    /// <summary>
    /// Interface for parsing data
    /// </summary>
    public interface IDataParser
    {
        Stat Male { get; set; }
        Stat Female { get; set; }

       string PhoneNumber { get; set; }
       string ScaleName { get; set; }
       int ScaleNumber { get; set; }

       /// <summary>
        /// Parse data to stats
        /// </summary>
        /// <param name="data">data to parse</param>
        /// <returns>true if parsing finished successfully</returns>
        bool ParseData(object data);
    }
}
