﻿$(document).ready(function () {
   $("#fromDate").datepicker({ dateFormat: "dd.mm.yy" });
   $("#toDate").datepicker({ dateFormat: "dd.mm.yy" });
   
   CreateMouseWheelBinding("#sexSelection");
   CreateMouseWheelBinding("#scaleSelection");
   CreateMouseWheelBinding("#flockSelection");
});

function CreateMouseWheelBinding(node) {
   $(node).unbind("mousewheel DOMMouseScroll");
   $(node).bind("mousewheel DOMMouseScroll", function (event) {
      var index = $(node)[0].selectedIndex;
      if (IsScrollUp(event)) {
         if (index < $(node)[0].length - 1) {
            index++;
         }
      } else {
         if (index > 0) {
            index--;
         }
      }
      $(node)[0].selectedIndex = index;
      $(node).trigger("change");
      event.preventDefault();
   });
}

function IsScrollUp(event) {
   var up = false;
   if (!(typeof event.originalEvent.detail === "undefined" || event.originalEvent.detail === null)) {
      if (event.originalEvent.detail >= 0) {
         up = true;
      } else {
         up = false;
      }
   }

   if (!(typeof event.originalEvent.wheelDelta === "undefined" || event.originalEvent.wheelDelta === null)) {
      if (event.originalEvent.wheelDelta >= 0) {
         up = false;
      } else {
         up = true;
      }
   }
   return up;
}
