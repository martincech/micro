﻿var statTable;
/* FUNCTIONS */

var createStatTable = function () {
   //show column diff if groth curve is selected
   var diffVis = false;
   var growthCurveIndex = $.cookie("growthCurveIndex");
   if (growthCurveIndex > 0) {
      diffVis = true;
   }

   if (typeof statTable == 'undefined') {
       initDataTable(diffVis);
       GraphVM.clearing(false);
   } else {
      $(window).trigger('resize');

      var table = $('#statsTable').DataTable();
      var column = table.column(15, { order: 'index' });
      column.visible(diffVis);

      statTable.fnDraw();
      $(window).trigger('resize');
   }
};

var initDataTable = function (isGrowthCurveSelected) {
   statTable = $('#statsTable').dataTable({
      "language": {
          "url": baseURL + "Content/DataTables/Internationalisation/" + $.cookie("_culture") + ".txt"
      },
      "aaSorting": [[1, "asc"]],
      "sScrollY": calcDataTableHeight(),
      "sScrollX": "100%",
      "sScrollXInner": "99.99%",
      "bServerSide": true,
      "sAjaxSource": baseURL + "Flocks/StatisticsHandler",
      "bProcessing": true,
      "sDom": "frtiS",
      "fnServerParams": function (aoData) {
         aoData.push(
         {
            "name": "selectedFlocks",
            "value": GraphVM.flocksIds().toString()//GraphVM.flocksIds().toString()
         },
         {
            "name": "from",
            "value": GraphVM.from()
         },
         {
            "name": "to",
            "value": GraphVM.to()
         },
         {
            "name": "sex",
            "value": GraphVM.sex()
         },
         {
            "name": "flockId",
            "value": GraphVM.flockId()
         },
         {
            "name": "growthCurveId",
            "value": $.cookie("growthCurveId")
         });
      },
      "aoColumns": [
         {
            "mDataProp": "Stat.Id",
            "bVisible": false,
            "bSearchable": false,
            "bSortable": false
         },
         {
            "render": function (val) {
               var x = new Date(val);
               return $.datepicker.formatDate('dd.mm.yy', x);
            },
            "data": "Stat.Date",
            "bSearchable": false,
            "bSortable": true,
         },
         { "mDataProp": "Flock.Name" },
         { "mDataProp": "Stat.Day" },
         { "mDataProp": "Stat.Count" },
         { "mDataProp": "Stat.Average" },
         { "mDataProp": "Stat.Gain" },
         { "mDataProp": "Stat.Sigma" },
         { "mDataProp": "Stat.Cv" },
         {
            "bSearchable": false,
            "bSortable": false,
            "mDataProp": "Stat.Uni"
         },
         { "mDataProp": "Stat.Temperature" },
         { "mDataProp": "Stat.CarbonDioxide" },
         { "mDataProp": "Stat.Ammonia" },
         { "mDataProp": "Stat.Humidity" },
         {
            "render": function (data, type, row) {
               if (data === 0) {
                  return $("#SexMale").val();
               }

               if (data === 1) {
                  return $("#SexFemale").val();
               }
               return "";
            },
            "bSearchable": false,
            "bSortable": false,
            "mDataProp": "Stat.Sex"
         },
         {
            "bSearchable": false,
            "bSortable": false,
            "mDataProp": "Diff",
            "bVisible": isGrowthCurveSelected
         }
      ]
   });
}

/* OnDOM ready */
//export selected stats to csv file
$("#exportData").click(function() {
    var params = statTable.oApi._fnAjaxParameters(statTable.dataTable().fnSettings());
    params.push(
    {
        "name": "selectedFlocks",
        "value": GraphVM.flocksIds().toString()
    },
    {
        "name": "from",
        "value": GraphVM.from()
    },
    {
        "name": "to",
        "value": GraphVM.to()
    },
    {
        "name": "sex",
        "value": GraphVM.sex()
    },
    {
        "name": "isFlock",
        "value": "true"
    });
    location.href = baseURL + "Flocks/ExportStats?" + $.param(params); //
});

var calcDataTableHeight = function () {
    var height = $(window).height() - 333;
    if (height < 464) {
        height = 464;
    }
    return height;
};

$(window).resize(function () {
    $('.dataTables_scrollBody').css('height', calcDataTableHeight());
});

$('#selectedStat a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
    createStatTable();
});

$('.dropdown-toggle').dropdown();