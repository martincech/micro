﻿using DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bat2Library;

namespace BatApp.Helpers
{
    public class FilterParser
    {
        /// <summary>
        /// Parse filter parametrs from query string and convert it into right data type or theirs default values
        /// </summary>
        /// <param name="scaleId">id of scale</param>
        /// <param name="from">from</param>
        /// <param name="to">to</param>
        /// <param name="sex">sex</param>
        /// <returns>parsed data</returns>
        public static FilterModel Parse(string scaleId, string from, string to, string sex)
        {

            int ScaleId;
            DateTime From;
            DateTime To;
            SexE Sex;

            if (!int.TryParse(scaleId, out ScaleId))
            {
                ScaleId = 0;
            }

            if (!DateTime.TryParse(from, out From))
            {
                From = DateTime.MinValue;
            }

            if (!DateTime.TryParse(to, out To))
            {
                To = DateTime.MinValue;
            }

            if (sex == Resources.Resources.SexMale)
            {
                Sex = SexE.SEX_MALE;
            }
            else if (sex == Resources.Resources.SexFemale)
            {
                Sex = SexE.SEX_FEMALE;
            }
            else
            {
                Sex = SexE.SEX_UNDEFINED;
            }

            return new FilterModel
            {
                ScaleId = ScaleId,
                From = From,
                To= To,
                Sex = Sex,
                CurveId = 0,
                DisplayLength = int.MaxValue,
                DisplayStart = 0,
                SelectedFlocks = new List<int>()
            };
        }
    }
}