﻿using System;
using System.Globalization;
using System.Web.Mvc;

namespace BatApp.Helpers.ModelBinders
{
   public class DateTimeBinder : IModelBinder
   {
      public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
      {
         var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

         if (value.AttemptedValue == "undefined") return value;

         var culture = CultureInfo.GetCultureInfo("de-DE");
         var date = value.ConvertTo(typeof (DateTime), culture);
         return date;
      }
   }
}