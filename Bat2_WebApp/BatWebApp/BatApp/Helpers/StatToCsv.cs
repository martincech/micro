﻿using System.Collections.Generic;
using System.Text;
using Bat2Library;
using DataContext;
using Utilities;

namespace BatApp.Helpers
{
    public class StatToCsv
    {
        /// <summary>
        /// Creta csv file and convert it into byte array
        /// </summary>
        /// <param name="stats">statistics to be stored in csv file</param>
        /// <param name="isWeighing">is weighing stats or flock stats</param>
        /// <returns>csv file as byte array</returns>
        public static byte[] GetCsvInBytes(IEnumerable<StatFlock> stats, bool isWeighing)
        {
            Csv csv = new Csv(",", ".", Encoding.Default);
            int column = 0;
            csv.AddString(column++, Resources.Resources.CsvDateTime);
            csv.AddString(column++, !isWeighing ? Resources.Resources.CsvFlockName : Resources.Resources.CsvScaleName);
            csv.AddString(column++, Resources.Resources.CsvDay);
            csv.AddString(column++, Resources.Resources.CsvCount);
            csv.AddString(column++, Resources.Resources.CsvAverage);
            csv.AddString(column++, Resources.Resources.CsvGain);
            csv.AddString(column++, Resources.Resources.CsvSigma);
            csv.AddString(column++, Resources.Resources.CsvCv);
            csv.AddString(column++, Resources.Resources.CsvUniformity);
            csv.AddString(column++, Resources.Resources.CsvSex);
            if (!isWeighing)
            {
                csv.AddString(column, Resources.Resources.CsvDiff);
            }
            csv.SaveLine();
            foreach (var stat in stats)
            {
                column = 0;
                csv.AddDate(column++, stat.Stat.Date);
                csv.AddString(column++, !isWeighing ? stat.Flock.Name : stat.Stat.Scale.Name);
                csv.AddInteger(column++, stat.Stat.Day);
                csv.AddInteger(column++, stat.Stat.Count);
                csv.AddDouble(column++, stat.Stat.Average);
                csv.AddDouble(column++, stat.Stat.Gain);
                csv.AddDouble(column++, stat.Stat.Sigma);
                csv.AddDouble(column++, stat.Stat.Cv);
                csv.AddDouble(column++, stat.Stat.Uni);
                csv.AddString(column++, stat.Stat.Sex == SexE.SEX_MALE ?
                  Resources.Resources.SexMale
                  : stat.Stat.Sex == SexE.SEX_FEMALE ?
                  Resources.Resources.SexFemale
                  : Resources.Resources.SexUndefined);
                if (!isWeighing)
                {
                    if (stat.Diff != null)
                    {
                        csv.AddDouble(column, (double)stat.Diff);
                    }
                    else
                    {
                        csv.AddString(column, "null");
                    }
                }
                csv.SaveLine();
            }
            return csv.SaveToBytes();
        }
    }
}