﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bat2Library;
using DataModel;

namespace BatApp.Controllers.V1
{
   public class Bat2OldStatisticsController : BaseApiController
   {
      /// <summary>
      /// Create new statistic.
      /// </summary>
      /// <param name="statistic">Bat2 old statistic</param>
      /// <returns>list of new created statistics (parameter statistic can contains 
      /// records for both sex therefore method returns list)</returns>
      public IEnumerable<Stat> Post(Bat2Library.Bat2Old.DB.Sms statistic)
      {
         if (statistic.FemaleStat == null) return null;

         var stats = new List<Stat>();
         if (statistic.MaleStat == null)
         {
            stats.Add(CreateStat(statistic.FemaleStat, SexE.SEX_UNDEFINED));
         }
         else
         {
            stats.Add(CreateStat(statistic.MaleStat, SexE.SEX_MALE));
            stats.Add(CreateStat(statistic.FemaleStat, SexE.SEX_FEMALE));
         }

         var scale = Context.GetScaleIfExist(statistic.Id.ToString()) ?? 
               Context.CreateScale(0, statistic.Id.ToString());
         
         if (scale == null) return null;
         if (statistic.GsmNumber != null && !statistic.GsmNumber.Equals(""))
         {
            scale.PhoneNumber = statistic.GsmNumber;
         }

         // add rest of records
         for (var i = stats.Count - 1; i >= 0; i--)
         {
            stats[i].Day = statistic.DayNumber;
            stats[i].Date = statistic.DayDate;

            //if scale contain statistic, don´t add it again
            if (!ContainStat(scale.Stat, stats[i]))
            {
               scale.Stat.Add(stats[i]);
            }
            else
            {
               stats.RemoveAt(i);
            }
         }

         Context.SaveChanges();
         return stats.Count > 0 ? stats : null;
      }

      #region Private helpers

      /// <summary>
      /// Check if list of statistic contains this (Some items are different,
      /// therefore we can't use {collection}.Contains(xxx))
      /// </summary>
      /// <param name="list">collection of statistic</param>
      /// <param name="stat">testing statistic</param>
      /// <returns>true - statistic is included at list</returns>
      private bool ContainStat(IEnumerable<Stat> list, Stat stat)
      {
         const double epsilon = 0.0001;
         return list.Any(item => 
               item.Count == stat.Count && 
               Math.Abs(item.Average - stat.Average) < epsilon && 
               Math.Abs(item.Gain - stat.Gain) < epsilon && 
               Math.Abs(item.Sigma - stat.Sigma) < epsilon && 
               Math.Abs(item.Cv - stat.Cv) < epsilon && 
               Math.Abs(item.Uni - stat.Uni) < epsilon && 
               item.Sex == stat.Sex && 
               item.Day == stat.Day && 
               item.Date == stat.Date
         );
      }


      /// <summary>
      /// Create and partial fill <see cref="Stat"/>.
      /// </summary>
      /// <param name="statistic">Bat2 old statistic</param>
      /// <param name="sex">sex</param>
      private Stat CreateStat(Bat2Library.Bat2Old.DB.Helpers.Statistic statistic, SexE sex)
      {
         return new Stat
         {
            Count = statistic.Count,
            Average = statistic.Average,
            Gain = statistic.Gain,
            Sigma = statistic.Sigma,
            Cv = statistic.Cv,
            Uni = statistic.Uni,
            Sex = sex
         };
      }

      #endregion
   }
}
