﻿using System.Linq;
using BatApp.Extensions;
using DTOs;

namespace BatApp.Controllers.V1
{
   /// <summary>
   /// Controller for check update of bat2 old configuration
   /// </summary>
   public class ConfigurationV2UpdatesController : BaseApiController
   {
      /// <summary>
      /// Get configuration if was update.
      /// </summary>
      /// <param name="id">scale name</param>
      /// <returns>configuration - was update, null - wasn't update</returns>
      public ConfigurationV2 Get(string id)
      {
         var scale = Context.Scales.FirstOrDefault(f => f.Name.Equals(id));
         if (scale == null || !scale.ConfigurationChanged) return null;

         // configuration is changed, can set to false
         scale.ConfigurationChanged = false;
         Context.SaveChanges();

         var scaleDto = scale.MapTo();
         return scaleDto.ConfigurationsV2.FirstOrDefault(c => c.Active);
      }
   }
}
