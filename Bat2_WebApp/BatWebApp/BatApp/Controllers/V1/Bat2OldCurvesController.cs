﻿using System.Collections.Generic;
using System.Linq;
using DataModel;

namespace BatApp.Controllers.V1
{
    public class Bat2OldCurvesController : BaseApiController
    {
       /// <summary>
       /// Create new curve
       /// </summary>
       /// <param name="curve">Bat2 old program curve format</param>
       /// <returns>Created curves or null if method fails</returns>
       public Curve Post(Bat2Library.Bat2Old.DB.GrowthCurve curve)
       {
          if (curve.Name.Equals("") || Context.Curves.Any(x => x.Name.Equals(curve.Name)))
          {
             return null;
          }

          var c = new Curve {Name = curve.Name};
          if (curve.Records != null && curve.Records.Count > 0)
          {
             c.CurveValues = new List<CurveValue>();
             foreach (var record in curve.Records)
             {
                var value = new CurveValue
                {
                   Day = record.Key,
                   Weight = record.Value
                };
                c.CurveValues.Add(value);
             }
          }

          Context.SaveCurve(c);
          return c;
       }
    }
}
