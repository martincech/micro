﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Web;
using JsAction;
using System.Web.Mvc;
using BatApp.Helpers;
using BatApp.Models;
using BatApp.Models.DTO;
using DataModel;

namespace BatApp.Controllers
{
   /// <summary>
   /// Scale controler
   /// </summary>
   [BatApp.Infrastructure.Filters.Authorize]
   public class ScalesController : BaseController
   {
      private const string FILE_NAME = "scales.csv";

      /// <summary>
      /// Get page with list of scales
      /// </summary>
      /// <returns></returns>
      public ActionResult Index()
      {
         return View();
      }
      /// <summary>
      /// Get page for configuration of Bat2
      /// </summary>
      /// <returns></returns>
      public ActionResult Bat2()
      {
         return View();
      }

      /// <summary>
      /// Get list of all scales
      /// </summary>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpGet]
      public JsonResult GetAllScales()
      {
         var scales =
            Context.Scales.Select(
               x =>
                  new
                  {
                     serialNumber = x.SerialNumber,
                     scaleName = x.Name ?? "",
                     farmName = x.FarmName ?? "",
                     phoneNumber = x.PhoneNumber ?? "",
                     id = x.Id
                  }).OrderBy(x => x.id);
         return new JsonNetResult { Data = scales.ToArray(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
      }

      /// <summary>
      /// jQuery Datatables handler
      /// </summary>
      /// <param name="param"></param>
      /// <returns></returns>
      [HttpGet]
      public JsonResult ScalesHandler(JQueryDataTablesRequest param)
      {
         if (Context.Scales == null)
         {
            return new JsonNetResult
            {
               Data = new JQueryDataTablesResponse(param.sEcho, 0, 0),
               JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
         }

         var curveData = Context.Scales.ToList().
            Select(c =>
               new[]
               {
                  Convert.ToString(c.Id),
                  c.SerialNumber.ToString("X"),
                  c.Name ?? "",
                  c.PhoneNumber ?? "",
                  c.FarmName ?? "",
                  User.IsInRole(Constants.ADMIN) || User.IsInRole(Constants.COMPANY_ADMIN ) ? Convert.ToString(c.Users.Count()) : null ,
                  User.IsInRole(Constants.ADMIN) || User.IsInRole(Constants.COMPANY_ADMIN ) ?  (c.Configurations.Any()) ? Convert.ToString(2) : (c.ConfigurationsV2.Any()) ? Convert.ToString(1) : Convert.ToString(0) : null,
                  User.IsInRole(Constants.ADMIN) ? c.Company == null ? Resources.Resources.CompanyNotSelected :  c.Company.Name : null
               }).ToArray();

         return new JsonNetResult
         {
            Data = new JQueryDataTablesResponse(param.sEcho, curveData.Count(), curveData.Count(), curveData),
            JsonRequestBehavior = JsonRequestBehavior.AllowGet
         };
      }

      /// <summary>
      /// Delete scales
      /// </summary>
      /// <param name="ids"></param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult DeleteScales(int[] ids)
      {
         Context.DeleteScales(ids);
         return Json(new { success = true });
      }

      /// <summary>
      /// Get user of scale
      /// </summary>
      /// <param name="id"></param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult GetScaleUsers(int? id)
      {
         if (id == null)
         {
            return Json(new { success = false });
         }
         var scale = Context.Scales.First(x => x.Id == id);
         var allUsers = scale.CompanyId == null ? null : Context.GetCompanyUsers((int)scale.CompanyId).Select(s => new { id = s.Id, name = s.UserName });
         var scaleUsers = scale.Users.Select(s => s.Id);

         return Json(new { success = true, data = new { users = allUsers, usersSelected = scaleUsers } });
      }

      /// <summary>
      /// Change assigned scale users
      /// </summary>
      /// <param name="id"></param>
      /// <param name="usersSelected"></param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult SetScaleUsers(int? id, string[] usersSelected)
      {
         if (id == null)
         {
            return Json(new { success = false });
         }
         usersSelected = usersSelected ?? new string[0];

         if (!Context.UpdateScale((int)id, usersSelected.Select(Guid.Parse)))
         {
            return Json(new { success = false });
         }
         return Json(new { success = true });
      }

      /// <summary>
      /// Get list companies for scale
      /// </summary>
      /// <param name="id"></param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult GetScaleCompanies(int? id)
      {
         if (id == null)
         {
            return Json(new { success = false });
         }

         var allCompanies = Context.Companies.Select(s => new { id = s.Id, name = s.Name });
         var scaleComapnies = Context.Scales.Where(x => x.Id == id).Select(s => s.CompanyId);

         return Json(new { success = true, data = new { companies = allCompanies, companiesSelected = scaleComapnies } });
      }

      /// <summary>
      /// Set scale company
      /// </summary>
      /// <param name="id"></param>
      /// <param name="companiesSelected"></param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult SetScaleCompanies(int? id, string[] companiesSelected)
      {
         if (id == null)
         {
            return Json(new { success = false });
         }
         companiesSelected = companiesSelected ?? new string[0];

         if (!Context.UpdateScaleCompanies((int)id, companiesSelected.Select(Int32.Parse)))
         {
            return Json(new { success = false });
         }
         return Json(new { success = true });
      }

      /// <summary>
      /// Update scale parametrs
      /// </summary>
      /// <param name="id"></param>
      /// <param name="serialNumber"></param>
      /// <param name="scaleName"></param>
      /// <param name="farmName"></param>
      /// <param name="phoneNumber"></param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult UpdateScale(int id, int? serialNumber, string scaleName, string farmName, string phoneNumber)
      {
         if (!Context.UpdateScale(id, serialNumber, scaleName, farmName, phoneNumber))
         {
            return Json(new { success = false });
         }
         return Json(new { success = true });
      }

      /// <summary>
      /// Change scale type
      /// </summary>
      /// <param name="id"></param>
      /// <param name="scaleType"></param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult ChangeScaleType(int id, string scaleType)
      {
         if (!Context.ChangeScaleType(id, scaleType))
         {
            return Json(new { success = false });
         }
         return Json(new { success = true });
      }

      /// <summary>
      /// Create new scale
      /// </summary>
      /// <param name="serialNumber"></param>
      /// <param name="scaleName"></param>
      /// <param name="farmName"></param>
      /// <param name="phoneNumber"></param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult CreateScale(string serialNumber, string scaleName, string farmName, string phoneNumber)
      {
         var sn = int.Parse(serialNumber, NumberStyles.HexNumber);
         if (Context.CreateScale(sn, scaleName, phoneNumber, farmName) == null)
         {
            return Json(new { success = false });
         }
         return Json(new { success = true });
      }

      /// <summary>
      /// Get configurations and tempaltes for scale
      /// </summary>
      /// <param name="id"></param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult GetScaleConfigs(int id)
      {
         var scale = Context.Scales.FirstOrDefault(x => x.Id == id);
         if (scale == null)
         {
            return Json(new { success = false });
         }
         var data = new
         {
            configs = scale.ConfigurationsV2,
            templates = Context.ConfigurationsV2.Where(w => w.ScaleId == null).Select(s => new TemplateDTO() { Name = s.Name, Id = s.Id }).ToList()
         };
         return new JsonNetResult { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
      }

      /// <summary>
      /// Create new configuration with tempate
      /// </summary>
      /// <param name="id"></param>
      /// <param name="templateId"></param>
      /// <param name="name"></param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult CreateConfig(int id, int? templateId, string name)
      {
         var settings = Context.CreateScaleConfig(id, templateId, name);
         if (settings == null)
         {
            return Json(new { success = false });
         }
         var data = new
         {
            config = settings,
            flocks = Bat2FlockDTO(settings.FlockV2)
         };
         return new JsonNetResult { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
      }

      /// <summary>
      /// Delete configuration
      /// </summary>
      /// <param name="id"></param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult DeleteConfig(int id)
      {
         if (!Context.DeleteScaleConfig(id))
         {
            return Json(new { success = false });
         }
         return Json(new { success = true });
      }

      /// <summary>
      /// Get list of all flocks for specific scale
      /// </summary>
      /// <param name="id"></param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult GetAllFlocks(int id)
      {
         var scale = Context.Scales.AsNoTracking().FirstOrDefault(x => x.Id == id);
         if (scale == null || scale.ConfigurationsV2 == null)
         {
            return Json(new { success = false });
         }

         var flocks = scale.ConfigurationsV2.SelectMany(s => s.FlockV2);

         var data = new
         {
            flocks = Bat2FlockDTO(flocks)
         };

         return new JsonNetResult { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
      }

      /// <summary>
      /// Save all configuraions
      /// </summary>
      /// <param name="scaleId"></param>
      /// <param name="configurations"></param>
      /// <param name="flocks"></param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult SaveConfigs(int scaleId, IEnumerable<ConfigurationsV2> configurations, IEnumerable<FlockV2DTO> flocks)
      {
         flocks = flocks ?? new List<FlockV2DTO>();
         if (configurations == null)
         {
            return Json(new { success = false });
         }

         Context.SaveScaleConfigs(scaleId, Bat2ConfigDTOConverter(configurations, flocks));

         return Json(new { success = true });
      }

      /// <summary>
      /// Change configuration name
      /// </summary>
      /// <param name="configId"></param>
      /// <param name="name"></param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult ChangeConfigName(int configId, string name)
      {
         Context.ScaleConfigName(configId, name);
         return Json(new { success = true });
      }

      /// <summary>
      /// Create/Delete templates
      /// </summary>
      /// <param name="configurations"></param>
      /// <param name="flocks"></param>
      /// <param name="deleteTempl"></param>
      /// <returns></returns>
      [JsAction(CacheRequest = false)]
      [HttpPost]
      public JsonResult ChangeConfigTemplates(IEnumerable<ConfigurationsV2> configurations, IEnumerable<FlockV2DTO> flocks, IEnumerable<TemplateDTO> deleteTempl)
      {
         Context.SaveTemplateConfigs(Bat2ConfigDTOConverter(configurations, flocks));
         if (deleteTempl != null)
         {
            foreach (var delete in deleteTempl)
            {
               Context.DeleteScaleConfig(delete.Id);
            }
         }
         
         var templates = Context.ConfigurationsV2.Where(w => w.ScaleId == null)
               .Select(s => new TemplateDTO() { Name = s.Name, Id = s.Id })
               .ToList();
         return new JsonNetResult { Data = templates, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
      }

      /// <summary>
      /// Load list of curves
      /// </summary>
      /// <returns>list of curves</returns>
      [JsAction(CacheRequest = false)]
      [HttpGet]
      public JsonResult LoadCurves()
      {
         var configCurves = Context.GetConfigCurveIds();
         var data = Context.Curves.ToList().Where(w => !configCurves.Contains(w.Id)).Select(s => new CurveDTO()
         {
            Name = s.Name,
            CurveValues = s.CurveValues.ToList().Select(c => new CurveValue() { Day = c.Day, Weight = c.Weight }).ToList()
         }).ToList();
         return new JsonNetResult
         {
            Data = data,
            JsonRequestBehavior = JsonRequestBehavior.AllowGet
         };
      }

      /// <summary>
      /// Export sales in csv format
      /// </summary>
      /// <param name="column"></param>
      /// <param name="orientation"></param>
      /// <param name="search"></param>
      /// <returns></returns>
      [HttpGet]
      public FileResult ExportScales(int? column, string orientation, string search)
      {
         string propertyName = PropertyName<Scale>(x => x.Id);
         switch (column)
         {
            case 0:
               propertyName = PropertyName<Scale>(x => x.Id);

               break;
            case 1:
               propertyName = PropertyName<Scale>(x => x.SerialNumber);
               break;
            case 2:
               propertyName = PropertyName<Scale>(x => x.Name);
               break;
            case 3:
               propertyName = PropertyName<Scale>(x => x.FarmName);
               break;
            case 4:
               propertyName = PropertyName<Scale>(x => x.PhoneNumber);
               break;
         }
         var scales = Context.Scales.ToList();

         if (!string.IsNullOrEmpty(search))
         {
            search = search.ToLower();
            scales = scales.Where(x => x.SerialNumber.ToString().Contains(search)
                                       || (x.Name ?? "").ToLower().Contains(search)
                                       || (x.FarmName ?? "").ToLower().Contains(search)
                                       || (x.PhoneNumber ?? "").ToLower().Contains(search)).ToList();
         }
         if (orientation == "ASC")
         {
            scales = scales.OrderBy(x => x.GetType().GetProperty(propertyName).GetValue(x, null)).ToList();
         }
         else
         {
            scales = scales.OrderByDescending(x => x.GetType().GetProperty(propertyName).GetValue(x, null)).ToList();
         }

         var dataResult = ScaleToCsv.GetCsvInBytes(scales, CultureInfo.CurrentCulture.TextInfo.ListSeparator,
            CultureInfo.CurrentUICulture.NumberFormat.NumberDecimalSeparator);
         return File(dataResult, "text/csv", FILE_NAME);
      }

      /// <summary>
      /// Get property name
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <param name="expression"></param>
      /// <returns></returns>
      public static string PropertyName<T>(Expression<Func<T, object>> expression)
      {
         var body = expression.Body as MemberExpression;

         if (body == null)
         {
            body = ((UnaryExpression)expression.Body).Operand as MemberExpression;
         }

         return body.Member.Name;
      }

      private List<FlockV2DTO> Bat2FlockDTO(IEnumerable<FlockV2> flocks)
      {
         if (flocks == null)
         {
            return new List<FlockV2DTO>();
         }
         return flocks.Select(flock => new FlockV2DTO()
         {
            ConfigurationV2_Id = flock.ConfigurationV2_Id,
            CurveFemale_Id = flock.CurveFemale_Id,
            CurveFemaleDTO = flock.CurveFemale == null ? null : new CurveDTO()
            {
               CompanyId = flock.CurveFemale.CompanyId,
               CurveValues = flock.CurveFemale.CurveValues,
               Name = flock.CurveFemale.Name
            },
            CurveMale_Id = flock.CurveMale_Id,
            CurveMaleDTO = flock.CurveMale == null ? null : new CurveDTO()
            {
               CompanyId = flock.CurveMale.CompanyId,
               CurveValues = flock.CurveMale.CurveValues,
               Name = flock.CurveMale.Name
            },
            Id = flock.Id,
            InitialFemale = flock.InitialFemale,
            InitialMale = flock.InitialMale,
            WeighFrom = flock.WeighFrom,
            WeighTo = flock.WeighTo,
            UseCurves = flock.UseCurves,
            UseGender = flock.UseGender,
            Name = flock.Name,
            Index = flock.Index
         }).ToList();
      }

      private List<ConfigurationsV2> Bat2ConfigDTOConverter(IEnumerable<ConfigurationsV2> configurations, IEnumerable<FlockV2DTO> flocks)
      {
         if (configurations == null)
         {
            return new List<ConfigurationsV2>();
         }
         var confs = configurations as ConfigurationsV2[] ?? configurations.ToArray();
         flocks = flocks ?? new List<FlockV2DTO>();
         foreach (var flock in flocks)
         {
            var config = confs.FirstOrDefault(f => f.Id == flock.ConfigurationV2_Id);
            if (config == null)
            {
               continue;
            }
            if (config.FlockV2 == null)
            {
               config.FlockV2 = new List<FlockV2>();
            }

            var curveFemale = flock.CurveFemaleDTO == null ? null : new Curve()
            {
               Name = config.Name + "_" + flock.Index + "_" + Resources.Resources.SexFemale,
               CompanyId = config.CompanyId,
               CurveValues = flock.CurveFemaleDTO.CurveValues == null ? null : flock.CurveFemaleDTO.CurveValues.ToList().Select(s => new CurveValue() { Day = s.Day, Weight = s.Weight }).ToList(),
            };

            var curveMale = flock.CurveMaleDTO == null ? null : new Curve()
            {
               Name = config.Name + "_" + flock.Index + "_" + Resources.Resources.SexMale,
               CompanyId = config.CompanyId,
               CurveValues = flock.CurveMaleDTO.CurveValues == null ? null : flock.CurveMaleDTO.CurveValues.ToList().Select(s => new CurveValue() { Day = s.Day, Weight = s.Weight }).ToList(),
            };

            FlockV2 fl = new FlockV2()
            {
               ConfigurationV2_Id = flock.ConfigurationV2_Id,
               CurveFemale = curveFemale,
               CurveMale = curveMale,
               Index = flock.Index,
               InitialFemale = flock.InitialFemale,
               InitialMale = flock.InitialMale,
               Name = flock.Name ?? " ",
               UseCurves = flock.UseCurves,
               UseGender = flock.UseGender,
               WeighFrom = flock.WeighFrom,
               WeighTo = flock.WeighTo
            };
            config.FlockV2.Add((FlockV2)fl);

         }
         return confs.ToList();
      }

   }
}