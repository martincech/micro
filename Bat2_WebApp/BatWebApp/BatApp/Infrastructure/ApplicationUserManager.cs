﻿using System;
using DataContext;
using DataModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace BatApp.Infrastructure
{
   public class ApplicationUserManager : UserManager
   {
      public ApplicationUserManager(BatModelContainer db)
         : base(db)
      {
      }

      public ApplicationUserManager(IUserStore<User, Guid> store)
         : base(store)
      {
      }
      public static ApplicationUserManager Create(BatModelContainer db)
      {
         var manager = new ApplicationUserManager(db);
         // Configure validation logic for usernames
         manager.UserValidator = new UserValidator<User, Guid>(manager)
         {
            AllowOnlyAlphanumericUserNames = false,
            RequireUniqueEmail = false
         };

         // Configure validation logic for passwords
         manager.PasswordValidator = new PasswordValidator
         {
            RequiredLength = 6,
            RequireNonLetterOrDigit = true,
            RequireDigit = true,
            RequireLowercase = true,
            RequireUppercase = true,
         };
         return manager;
      }

      public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options,
         IOwinContext context)
      {
         var manager = Create(context.Get<ApplicationDbContext>());

         // Configure user lockout defaults
         manager.UserLockoutEnabledByDefault = false;
         manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
         manager.MaxFailedAccessAttemptsBeforeLockout = 5;

         // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
         // You can write your own provider and plug it in here.
         manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<User,Guid>
         {
            MessageFormat = Resources.Resources.YourSecurityCodeIs
         });
         manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<User, Guid>
         {
            Subject = "Security Code",
            BodyFormat = Resources.Resources.YourSecurityCodeIs
         });
         //manager.EmailService = new EmailService();
         //manager.SmsService = new SmsService();
         var dataProtectionProvider = options.DataProtectionProvider;
         if (dataProtectionProvider != null)
         {
            manager.UserTokenProvider =
               new DataProtectorTokenProvider<User,Guid>(dataProtectionProvider.Create("ASP.NET Identity"));
         }
         return manager;
      }
   }
}