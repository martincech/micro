using DataModel;

namespace BatApp.Infrastructure
{
   public class ApplicationDbContext : BatModelContainer
   {
      public static ApplicationDbContext Create()
      {
         return new ApplicationDbContext();
      }
   }
}