﻿using System;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using BatApp.Security;
using DataModel;
using Microsoft.AspNet.Identity;
using Utilities.IOC;

namespace BatApp.Infrastructure.Filters
{
   /// <summary>
   /// Generic Basic Authentication filter that checks for basic authentication
   /// headers and challenges for authentication if no authentication is provided
   /// Sets the Thread Principle with a GenericAuthenticationPrincipal.
   /// 
   /// You can override the OnAuthorize method for custom auth logic that
   /// might be application specific.    
   /// </summary>
   /// <remarks>Always remember that Basic Authentication passes username and passwords
   /// from client to server in plain text, so make sure SSL is used with basic auth
   /// to encode the Authorization header on all requests (not just the login).
   /// </remarks>
   [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
   public class BasicAuthenticationFilter : AuthorizationFilterAttribute
   {
      private readonly bool _active = true;
      private UserManager<User, Guid> UserManager { get; set; }

      public BasicAuthenticationFilter()
      {
      }

      /// <summary>
      /// Overriden constructor to allow explicit disabling of this
      /// filter's behavior. Pass false to disable (same as no filter
      /// but declarative)
      /// </summary>
      /// <param name="active"></param>
      public BasicAuthenticationFilter(bool active)
      {
         _active = active;
      }

      /// <summary>
      /// Override to Web API filter method to handle Basic Auth check
      /// </summary>
      /// <param name="actionContext"></param>
      public override void OnAuthorization(HttpActionContext actionContext)
      {
         if (_active)
         {
            var currentUser = HttpContext.Current.User.Identity.GetUserId();
            if (currentUser != null)
            {  // user is already authorizate
               return;
            }

            var identity = ParseAuthorizationHeader(actionContext);
            if (identity == null)
            {
               Challenge(actionContext);
               return;
            }

            var principal = OnAuthorizeUser(identity.Name, identity.Password, actionContext);
            if (principal == null)
            {
               Challenge(actionContext);
               return;
            }
       
            Thread.CurrentPrincipal = principal;       
            // inside of ASP.NET this is required
            if (HttpContext.Current != null)
               HttpContext.Current.User = principal;

            base.OnAuthorization(actionContext);
         }
      }

      /// <summary>
      /// Base implementation for user authentication - you probably will
      /// want to override this method for application specific logic.
      /// 
      /// The base implementation merely checks for username and password
      /// present and set the Thread principal.
      /// 
      /// Override this method if you want to customize Authentication
      /// and store user data as needed in a Thread Principle or other
      /// Request specific storage.
      /// </summary>
      /// <param name="username"></param>
      /// <param name="password"></param>
      /// <param name="actionContext"></param>
      /// <returns></returns>
      protected virtual IPrincipal OnAuthorizeUser(string username, string password, HttpActionContext actionContext)
      {
         if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password)) return null;

         UserManager = IocContainer.Container.Get<UserManager<User, Guid>>();
         var user = UserManager.Find(username, password);
         if (user == null)
         {  // No user with userName/password exists.
            return null;
         }
         // Create a ClaimsIdentity with all the claims for this user.
         var identity = UserManager.ClaimsIdentityFactory.CreateAsync(UserManager, user, "Basic").Result;
         return new ClaimsPrincipal(identity);      
      }

      /// <summary>
      /// Parses the Authorization header and creates user credentials
      /// </summary>
      /// <param name="actionContext"></param>
      protected virtual BasicAuthenticationIdentity ParseAuthorizationHeader(HttpActionContext actionContext)
      {
         string authHeader = null;
         var auth = actionContext.Request.Headers.Authorization;
         if (auth != null && auth.Scheme == "Basic")
            authHeader = auth.Parameter;

         if (string.IsNullOrEmpty(authHeader))
            return null;

         authHeader = Encoding.UTF8.GetString(Convert.FromBase64String(authHeader));

         var tokens = authHeader.Split(':');
         if (tokens.Length < 2)
            return null;

         return new BasicAuthenticationIdentity(tokens[0], tokens[1]);
      }


      /// <summary>
      /// Send the Authentication Challenge request
      /// </summary>
      /// <param name="message"></param>
      /// <param name="actionContext"></param>
      private void Challenge(HttpActionContext actionContext)
      {
         var host = actionContext.Request.RequestUri.DnsSafeHost;
         actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
         actionContext.Response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", host));
      }
   }
}
