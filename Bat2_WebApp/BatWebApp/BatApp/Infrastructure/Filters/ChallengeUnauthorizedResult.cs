﻿using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace BatApp.Infrastructure.Filters
{
   public class ChallengeUnauthorizedResult : ActionResult
   {
      public ChallengeUnauthorizedResult(string challenge, ActionResult innerResult)
      {
         Challenge = challenge;
         InnerResult = innerResult;
      }

      public string Challenge { get; private set; }

      public ActionResult InnerResult { get; private set; }

      public override void ExecuteResult(ControllerContext context)
      {
         if (context.HttpContext.Response.IsRequestBeingRedirected)
         {
            context.HttpContext.Response.Clear();
            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
         }
         InnerResult.ExecuteResult(context);
         var response = context.HttpContext.Response;

         if (response.StatusCode == (decimal) HttpStatusCode.Unauthorized)
         {
            var existingChallenges = response.Headers.GetValues("WWW-Authenticate");
            if (existingChallenges == null || !existingChallenges.Any(c => c == Challenge))
            {
               response.Headers.Add("WWW-Authenticate", Challenge);
            }
            context.RequestContext.HttpContext.Response.End();
         }
         if (response.StatusCode == (decimal) HttpStatusCode.Forbidden)
         {
           
            context.RequestContext.HttpContext.Response.End();
         }
      }
   }
}