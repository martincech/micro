﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace JsAction.ext
{
   public class JsBeautifyOptions
   {
      public int? IndentSize { get; set; }
      public char? IndentChar { get; set; }
      public int? IndentLevel { get; set; }
      public bool? PreserveNewlines { get; set; }
   }

   public class JsBeautify
   {
      private readonly StringBuilder output;
      private readonly string indentString;
      private int indentLevel;
      private readonly string tokenText;
      private readonly Stack<string> modes;
      private string currentMode;
      private readonly bool optPreserveNewlines;
      private bool ifLineFlag;
      private bool doBlockJustClosed;
      private readonly string input;


      private void trim_output()
      {
         while ((output.Length > 0) &&
                ((output[output.Length - 1] == ' ') ||
                 (output[output.Length - 1].ToString(CultureInfo.InvariantCulture) == indentString)))
         {
            output.Remove(output.Length - 1, 1);
         }
      }

      private void print_newline(bool? ignoreRepeated)
      {
         ignoreRepeated = ignoreRepeated ?? true;

         ifLineFlag = false;
         trim_output();

         if (output.Length == 0)
            return;

         if ((output[output.Length - 1] != '\n') || !ignoreRepeated.Value)
         {
            output.Append(Environment.NewLine);
         }

         for (var i = 0; i < indentLevel; i++)
         {
            output.Append(indentString);
         }
      }

      private void print_space()
      {
         var lastOutput = " ";
         if (output.Length > 0)
            lastOutput = output[output.Length - 1].ToString(CultureInfo.InvariantCulture);
         if ((lastOutput != " ") && (lastOutput != "\n") && (lastOutput != indentString))
         {
            output.Append(' ');
         }
      }


      private void print_token()
      {
         output.Append(tokenText);
      }

      private void Indent()
      {
         indentLevel++;
      }

      private void Unindent()
      {
         if (indentLevel > 0)
            indentLevel--;
      }

      private void remove_indent()
      {
         if ((output.Length > 0) && (output[output.Length - 1].ToString(CultureInfo.InvariantCulture) == indentString))
         {
            output.Remove(output.Length - 1, 1);
         }
      }

      private void set_mode(string mode)
      {
         modes.Push(currentMode);
         currentMode = mode;
      }

      private void restore_mode()
      {
         doBlockJustClosed = (currentMode == "DO_BLOCK");
         currentMode = modes.Pop();
      }

      private bool is_ternary_op()
      {
         var level = 0;
         var colonCount = 0;
         for (var i = output.Length - 1; i >= 0; i--)
         {
            switch (output[i])
            {
               case ':':
                  if (level == 0)
                     colonCount++;
                  break;
               case '?':
                  if (level == 0)
                  {
                     if (colonCount == 0)
                     {
                        return true;
                     }
                     colonCount--;
                  }
                  break;
               case '{':
                  if (level == 0) return false;
                  level--;
                  break;
               case '(':
               case '[':
                  level--;
                  break;
               case ')':
               case ']':
               case '}':
                  level++;
                  break;
            }
         }
         return false;
      }

      private readonly string whitespace;
      private readonly string wordchar;
      private readonly string lastType;
      private readonly string lastText;
      private readonly string digits;
      private readonly string[] punct;

      private string[] get_next_token(ref int parserPos)
      {
         var nNewlines = 0;

         if (parserPos >= input.Length)
         {
            return new[] {"", "TK_EOF"};
         }

         var c = input[parserPos].ToString(CultureInfo.InvariantCulture);
         parserPos++;

         while (whitespace.Contains(c))
         {
            if (parserPos >= input.Length)
            {
               return new[] {"", "TK_EOF"};
            }

            if (c == "\n")
               nNewlines++;

            c = input[parserPos].ToString(CultureInfo.InvariantCulture);
            parserPos++;
         }

         var wantedNewline = false;

         if (optPreserveNewlines)
         {
            if (nNewlines > 1)
            {
               for (var i = 0; i < 2; i++)
               {
                  print_newline(i == 0);
               }
            }
            wantedNewline = (nNewlines == 1);
         }

         if (wordchar.Contains(c))
         {
            if (parserPos < input.Length)
            {
               while (wordchar.Contains(input[parserPos]))
               {
                  c += input[parserPos];
                  parserPos++;
                  if (parserPos == input.Length)
                     break;
               }
            }


            if ((parserPos != input.Length) && (Regex.IsMatch(c, "^[0-9]+[Ee]$")) &&
                ((input[parserPos] == '-') || (input[parserPos] == '+')))
            {
               var sign = input[parserPos];
               parserPos++;

               var t = get_next_token(ref parserPos);
               c += sign + t[0];
               return new[] {c, "TK_WORD"};
            }

            if (c == "in")
            {
               return new[] {c, "TK_OPERATOR"};
            }

            if (wantedNewline && lastType != "TK_OPERATOR" && !ifLineFlag)
            {
               print_newline(null);
            }
            return new[] {c, "TK_WORD"};
         }

         if ((c == "(") || (c == "["))
            return new[] {c, "TK_START_EXPR"};

         if (c == ")" || c == "]")
         {
            return new[] {c, "TK_END_EXPR"};
         }

         if (c == "{")
         {
            return new[] {c, "TK_START_BLOCK"};
         }

         if (c == "}")
         {
            return new[] {c, "TK_END_BLOCK"};
         }

         if (c == ";")
         {
            return new[] {c, "TK_SEMICOLON"};
         }

         if (c == "/")
         {
            var comment = "";
            if (input[parserPos] == '*')
            {
               parserPos++;
               if (parserPos < input.Length)
               {
                  while (
                     !((input[parserPos] == '*') && (input[parserPos + 1] > '\0') && (input[parserPos + 1] == '/') &&
                       (parserPos < input.Length)))
                  {
                     comment += input[parserPos];
                     parserPos++;
                     if (parserPos >= input.Length)
                     {
                        break;
                     }
                  }
               }

               parserPos += 2;
               return new[] {"/*" + comment + "*/", "TK_BLOCK_COMMENT"};
            }

            if (input[parserPos] == '/')
            {
               comment = c;
               while ((input[parserPos] != '\x0d') && (input[parserPos] != '\x0a'))
               {
                  comment += input[parserPos];
                  parserPos++;
                  if (parserPos >= input.Length)
                  {
                     break;
                  }
               }

               parserPos++;
               if (wantedNewline)
               {
                  print_newline(null);
               }
               return new[] {comment, "TK_COMMENT"};
            }
         }

         if ((c == "'") || (c == "\"") || ((c == "/")
                                           &&
                                           ((lastType == "TK_WORD" && lastText == "return") ||
                                            ((lastType == "TK_START_EXPR") || (lastType == "TK_START_BLOCK") ||
                                             (lastType == "TK_END_BLOCK")
                                             || (lastType == "TK_OPERATOR") || (lastType == "TK_EOF") ||
                                             (lastType == "TK_SEMICOLON"))))
            )
         {
            var sep = c;
            var esc = false;
            var resultingString = c;

            if (parserPos < input.Length)
            {
               if (sep == "/")
               {
                  var inCharClass = false;
                  while ((esc) || (inCharClass) || (input[parserPos].ToString(CultureInfo.InvariantCulture) != sep))
                  {
                     resultingString += input[parserPos];
                     if (!esc)
                     {
                        esc = input[parserPos] == '\\';
                        if (input[parserPos] == '[')
                        {
                           inCharClass = true;
                        }
                        else if (input[parserPos] == ']')
                        {
                           inCharClass = false;
                        }
                     }
                     else
                     {
                        esc = false;
                     }
                     parserPos++;
                     if (parserPos >= input.Length)
                     {
                        return new[] {resultingString, "TK_STRING"};
                     }
                  }
               }
               else
               {
                  while ((esc) || (input[parserPos].ToString(CultureInfo.InvariantCulture) != sep))
                  {
                     resultingString += input[parserPos];
                     if (!esc)
                     {
                        esc = input[parserPos] == '\\';
                     }
                     else
                     {
                        esc = false;
                     }
                     parserPos++;
                     if (parserPos >= input.Length)
                     {
                        return new[] {resultingString, "TK_STRING"};
                     }
                  }
               }
            }

            parserPos += 1;

            resultingString += sep;

            if (sep == "/")
            {
               // regexps may have modifiers /regexp/MOD , so fetch those, too
               while ((parserPos < input.Length) && (wordchar.Contains(input[parserPos])))
               {
                  resultingString += input[parserPos];
                  parserPos += 1;
               }
            }
            return new[] {resultingString, "TK_STRING"};
         }

         if (c == "#")
         {
            var sharp = "#";
            if ((parserPos < input.Length) && (digits.Contains(input[parserPos])))
            {
               do
               {
                  c = input[parserPos].ToString(CultureInfo.InvariantCulture);
                  sharp += c;
                  parserPos += 1;
               } while ((parserPos < input.Length) && (c != "#") && (c != "="));
               if (c == "#")
               {
                  return new[] {sharp, "TK_WORD"};
               }
               return new[] {sharp, "TK_OPERATOR"};
            }
         }


         if ((c == "<") && (input.Substring(parserPos - 1, 3) == "<!--"))
         {
            parserPos += 3;
            return new[] {"<!--", "TK_COMMENT"};
         }

         if ((c == "-") && (input.Substring(parserPos - 1, 2) == "-->"))
         {
            parserPos += 2;
            if (wantedNewline)
            {
               print_newline(null);
            }
            return new[] {"-->", "TK_COMMENT"};
         }

         if (punct.Contains(c))
         {
            while ((parserPos < input.Length) && (punct.Contains(c + input[parserPos])))
            {
               c += input[parserPos];
               parserPos += 1;
               if (parserPos >= input.Length)
               {
                  break;
               }
            }

            return new[] {c, "TK_OPERATOR"};
         }

         return new[] {c, "TK_UNKNOWN"};
      }

      public string GetResult()
      {
         if (addScriptTags)
         {
            output.AppendLine().AppendLine("</script>");
         }

         return output.ToString();
      }

      private readonly bool addScriptTags;

      public JsBeautify(string jsSourceText, JsBeautifyOptions options)
      {
         var optIndentSize = options.IndentSize ?? 4;
         var optIndentChar = options.IndentChar ?? ' ';
         var optIndentLevel = options.IndentLevel ?? 0;
         optPreserveNewlines = options.PreserveNewlines ?? true;
         output = new StringBuilder();
         modes = new Stack<string>();


         indentString = "";

         while (optIndentSize > 0)
         {
            indentString += optIndentChar;
            optIndentSize -= 1;
         }

         indentLevel = optIndentLevel;


         input = jsSourceText.Replace("<script type=\"text/javascript\">", "").Replace("</script>", "");
         if (input.Length != jsSourceText.Length)
         {
            output.AppendLine("<script type=\"text/javascript\">");
            addScriptTags = true;
         }

         string lastWord = "";
         lastType = "TK_START_EXPR"; // last token type
         lastText = ""; // last token text

         doBlockJustClosed = false;
         bool varLine = false;
         bool varLineTainted = false;

         whitespace = "\n\r\t ";
         wordchar = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_$";
         digits = "0123456789";

         // <!-- is a special case (ok, it's a minor hack actually)
         punct =
            "+ - * / % & ++ -- = += -= *= /= %= == === != !== > < >= <= >> << >>> >>>= >>= <<= && &= | || ! !! , : ? ^ ^= |= ::"
               .Split(' ');

         // words which should always start on new line.
         string[] lineStarters =
            "continue,try,throw,return,var,if,switch,case,default,for,while,break,function".Split(',');

         // states showing if we are currently in expression (i.e. "if" case) - 'EXPRESSION', or in usual block (like, procedure), 'BLOCK'.
         // some formatting depends on that.
         currentMode = "BLOCK";
         modes.Push(currentMode);

         int parserPos = 0;
         bool inCase = false;

         while (true)
         {
            var t = get_next_token(ref parserPos);
            tokenText = t[0];
            string tokenType = t[1];
            if (tokenType == "TK_EOF")
            {
               break;
            }

            switch (tokenType)
            {
               case "TK_START_EXPR":
                  varLine = false;
                  set_mode("EXPRESSION");
                  if ((lastText == ";") || (lastType == "TK_START_BLOCK"))
                  {
                     print_newline(null);
                  }
                  else if ((lastType == "TK_END_EXPR") || (lastType == "TK_START_EXPR"))
                  {
                     // do nothing on (( and )( and ][ and ]( ..
                  }
                  else if ((lastType != "TK_WORD") && (lastType != "TK_OPERATOR"))
                  {
                     print_space();
                  }
                  else if (lineStarters.Contains(lastWord))
                  {
                     print_space();
                  }
                  print_token();
                  break;

               case "TK_END_EXPR":
                  print_token();
                  restore_mode();
                  break;

               case "TK_START_BLOCK":

                  set_mode(lastWord == "do" ? "DO_BLOCK" : "BLOCK");
                  if ((lastType != "TK_OPERATOR") && (lastType != "TK_START_EXPR"))
                  {
                     if (lastType == "TK_START_BLOCK")
                     {
                        print_newline(null);
                     }
                     else
                     {
                        print_space();
                     }
                  }
                  print_token();
                  Indent();
                  break;

               case "TK_END_BLOCK":
                  if (lastType == "TK_START_BLOCK")
                  {
                     // nothing
                     trim_output();
                     Unindent();
                  }
                  else
                  {
                     Unindent();
                     print_newline(null);
                  }
                  print_token();
                  restore_mode();
                  break;

               case "TK_WORD":

                  if (doBlockJustClosed)
                  {
                     // do {} ## while ()
                     print_space();
                     print_token();
                     print_space();
                     doBlockJustClosed = false;
                     break;
                  }

                  if ((tokenText == "case") || (tokenText == "default"))
                  {
                     if (lastText == ":")
                     {
                        // switch cases following one another
                        remove_indent();
                     }
                     else
                     {
                        // case statement starts in the same line where switch
                        Unindent();
                        print_newline(null);
                        Indent();
                     }
                     print_token();
                     inCase = true;
                     break;
                  }

                  string prefix = "NONE";

                  if (lastType == "TK_END_BLOCK")
                  {
                     if (!(new[] {"else", "catch", "finally"}).Contains(tokenText.ToLower()))
                     {
                        prefix = "NEWLINE";
                     }
                     else
                     {
                        prefix = "SPACE";
                        print_space();
                     }
                  }
                  else if ((lastType == "TK_SEMICOLON") && ((currentMode == "BLOCK") || (currentMode == "DO_BLOCK")))
                  {
                     prefix = "NEWLINE";
                  }
                  else if ((lastType == "TK_SEMICOLON") && (currentMode == "EXPRESSION"))
                  {
                     prefix = "SPACE";
                  }
                  else if (lastType == "TK_STRING")
                  {
                     prefix = "NEWLINE";
                  }
                  else if (lastType == "TK_WORD")
                  {
                     prefix = "SPACE";
                  }
                  else if (lastType == "TK_START_BLOCK")
                  {
                     prefix = "NEWLINE";
                  }
                  else if (lastType == "TK_END_EXPR")
                  {
                     print_space();
                     prefix = "NEWLINE";
                  }

                  if ((lastType != "TK_END_BLOCK") &&
                      ((new[] {"else", "catch", "finally"}).Contains(tokenText.ToLower())))
                  {
                     print_newline(null);
                  }
                  else if ((lineStarters.Contains(tokenText)) || (prefix == "NEWLINE"))
                  {
                     if (lastText == "else")
                     {
                        // no need to force newline on else break
                        print_space();
                     }
                     else if (((lastType == "TK_START_EXPR") || (lastText == "=") || (lastText == ",")) &&
                              (tokenText == "function"))
                     {
                        // no need to force newline on "function": (function
                        // DONOTHING
                     }
                     else if ((lastType == "TK_WORD") && ((lastText == "return") || (lastText == "throw")))
                     {
                        // no newline between "return nnn"
                        print_space();
                     }
                     else if (lastType != "TK_END_EXPR")
                     {
                        if (((lastType != "TK_START_EXPR") || (tokenText != "var")) && (lastText != ":"))
                        {
                           // no need to force newline on "var": for (var x = 0...)
                           if ((tokenText == "if") && (lastType == "TK_WORD") && (lastWord == "else"))
                           {
                              // no newline for } else if {
                              print_space();
                           }
                           else
                           {
                              print_newline(null);
                           }
                        }
                     }
                     else
                     {
                        if ((lineStarters.Contains(tokenText)) && (lastText != ")"))
                        {
                           print_newline(null);
                        }
                     }
                  }
                  else if (prefix == "SPACE")
                  {
                     print_space();
                  }
                  print_token();
                  lastWord = tokenText;

                  if (tokenText == "var")
                  {
                     varLine = true;
                     varLineTainted = false;
                  }

                  if (tokenText == "if" || tokenText == "else")
                  {
                     ifLineFlag = true;
                  }

                  break;

               case "TK_SEMICOLON":

                  print_token();
                  varLine = false;
                  break;

               case "TK_STRING":

                  if ((lastType == "TK_START_BLOCK") || (lastType == "TK_END_BLOCK") || (lastType == "TK_SEMICOLON"))
                  {
                     print_newline(null);
                  }
                  else if (lastType == "TK_WORD")
                  {
                     print_space();
                  }
                  print_token();
                  break;

               case "TK_OPERATOR":

                  var startDelim = true;
                  var endDelim = true;
                  if (varLine && (tokenText != ","))
                  {
                     varLineTainted = true;
                     if (tokenText == ":")
                     {
                        varLine = false;
                     }
                  }
                  if (varLine && (tokenText == ",") && (currentMode == "EXPRESSION"))
                  {
                     // do not break on comma, for(var a = 1, b = 2)
                     varLineTainted = false;
                  }

                  if (tokenText == ":" && inCase)
                  {
                     print_token(); // colon really asks for separate treatment
                     print_newline(null);
                     inCase = false;
                     break;
                  }

                  if (tokenText == "::")
                  {
                     // no spaces around exotic namespacing syntax operator
                     print_token();
                     break;
                  }

                  if (tokenText == ",")
                  {
                     if (varLine)
                     {
                        if (varLineTainted)
                        {
                           print_token();
                           print_newline(null);
                           varLineTainted = false;
                        }
                        else
                        {
                           print_token();
                           print_space();
                        }
                     }
                     else if (lastType == "TK_END_BLOCK")
                     {
                        print_token();
                        print_newline(null);
                     }
                     else
                     {
                        if (currentMode == "BLOCK")
                        {
                           print_token();
                           print_newline(null);
                        }
                        else
                        {
                           // EXPR od DO_BLOCK
                           print_token();
                           print_space();
                        }
                     }
                     break;
                  }
                  if ((tokenText == "--") || (tokenText == "++"))
                  {
                     // unary operators special case
                     if (lastText == ";")
                     {
                        if (currentMode == "BLOCK")
                        {
                           // { foo; --i }
                           print_newline(null);
                           startDelim = true;
                           endDelim = false;
                        }
                        else
                        {
                           // space for (;; ++i)
                           startDelim = true;
                           endDelim = false;
                        }
                     }
                     else
                     {
                        if (lastText == "{")
                        {
                           // {--i
                           print_newline(null);
                        }
                        startDelim = false;
                        endDelim = false;
                     }
                  }
                  else if (((tokenText == "!") || (tokenText == "+") || (tokenText == "-")) &&
                           ((lastText == "return") || (lastText == "case")))
                  {
                     startDelim = true;
                     endDelim = false;
                  }
                  else if (((tokenText == "!") || (tokenText == "+") || (tokenText == "-")) &&
                           (lastType == "TK_START_EXPR"))
                  {
                     // special case handling: if (!a)
                     startDelim = false;
                     endDelim = false;
                  }
                  else if (lastType == "TK_OPERATOR")
                  {
                     startDelim = false;
                     endDelim = false;
                  }
                  else if (lastType == "TK_END_EXPR")
                  {
                     startDelim = true;
                     endDelim = true;
                  }
                  else if (tokenText == ".")
                  {
                     // decimal digits or object.property
                     startDelim = false;
                     endDelim = false;
                  }
                  else if (tokenText == ":")
                  {
                     startDelim = is_ternary_op();
                  }
                  if (startDelim)
                  {
                     print_space();
                  }

                  print_token();

                  if (endDelim)
                  {
                     print_space();
                  }
                  break;

               case "TK_BLOCK_COMMENT":

                  print_newline(null);
                  print_token();
                  print_newline(null);
                  break;

               case "TK_COMMENT":

                  // print_newline();
                  print_space();
                  print_token();
                  print_newline(null);
                  break;

               case "TK_UNKNOWN":
                  print_token();
                  break;
            }

            lastType = tokenType;
            lastText = tokenText;
         }
      }
   }
}