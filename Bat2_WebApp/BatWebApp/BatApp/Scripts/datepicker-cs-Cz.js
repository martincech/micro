﻿/* Czech initialisation for the jQuery UI date picker plugin. */

(function (factory) {
   if (typeof define === "function" && define.amd) {

      // AMD. Register as an anonymous module.
      define(["../datepicker"], factory);
   } else {

      // Browser globals
      factory(jQuery.datepicker);
   }
}(function (datepicker) {

   datepicker.regional['cs-CZ'] = {
      closeText: 'Hotovo',
      prevText: 'Předchozí',
      nextText: 'Další',
      currentText: 'Dnes',
      monthNames: ['Leden', 'Únor', 'Březen', 'Duben', 'Květen', 'Červen',
      'Červenec', 'Srpen', 'Září', 'Říjen', 'Listopad', 'Prosinec'],
      monthNamesShort: ['Led', 'Úno', 'Břez', 'Dub', 'Kvě', 'Čvn',
      'Čvc', 'Srp', 'Zář', 'Říj', 'Lis', 'Pro'],
      dayNames: ['Neděle', 'Pondělí', 'Úterý', 'Středa', 'Čtvrtek', 'Pátek', 'Sobota'],
      dayNamesShort: ['Ned', 'Pon', 'Úte', 'Stř', 'Čtv', 'Pát', 'Sob'],
      dayNamesMin: ['Ne', 'Po', 'Út', 'St', 'Čt', 'Pá', 'So'],
      weekHeader: 'Td',
      dateFormat: 'dd/mm/rr',
      firstDay: 1,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: ''
   };
   datepicker.setDefaults(datepicker.regional['cs-CZ']);

   return datepicker.regional['cs-CZ'];

}));
