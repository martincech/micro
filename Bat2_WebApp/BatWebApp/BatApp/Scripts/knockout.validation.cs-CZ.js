﻿/************************************************
* This is an example localization page. All of these
* messages are the default messages for ko.validation
*
* Currently ko.validation does multiple parameter replacement
* on your message (indicated by the {0}, {1}, etc.).
*
* The parameters that you provide in your validation extender
* are what are passed to your message to do the {0}, {1} etc. replacements.
*
* eg: myProperty.extend({ minLength: 5 });
* ... will provide a message of "Please enter at least 5 characters"
* when validated
*
* eg: myProperty.extend({ between: [1, 5] });
* ... will provide a message of "Please enter between 1 and 5 characters"
* when validated
*
* This message replacement obviously only works with primitives
* such as numbers and strings. We do not stringify complex objects
* or anything like that currently.
*/
(function (factory) {
   // Module systems magic dance.
   /*global require,ko.validation,define,module*/
   if (typeof require === 'function' && typeof exports === 'object' && typeof module === 'object') {
      // CommonJS or Node
      module.exports = factory(require('../'));
   } else if (typeof define === 'function' && define['amd']) {
      // AMD anonymous module
      define(['knockout.validation'], factory);
   } else {
      // <script> tag: use the global `ko.validation` object
      factory(ko.validation);
   }
}(function (kv) {
   if (!kv || typeof kv.defineLocale !== 'function') {
      throw new Error('Knockout-Validation is required, please ensure it is loaded before this localization file');
   }
   return kv.defineLocale('cs-CZ', {
      required: 'Tato položka je vyžadována.',
      min: 'Prosím vložte hodnotu větší nebo rovno než {0}.',
      max: 'Prosím vložte hodnotu menší nebo rovno než  {0}.',
      minLength: 'Prosím vložte nejméně {0} znaků.',
      maxLength: 'Prosím nevkládejte více než {0} znaků.',
      pattern: 'Prosím zkontrolujte hodnotu.',
      step: 'Hodnota musí být větší o {0}.',
      email: 'Prosím zadejte emailovou adresu.',
      date: 'Prosím zadejte datum.',
      dateISO: 'Prosím zadejte datum.',
      number: 'Prosím vložte číslo.',
      digit: 'Prosím zadejte číslice.',
      phoneUS: 'Prosím zadejte telefonní číslo.',
      equal: 'Hodnota musí být stejná.',
      notEqual: 'Prosím vyberte jinou hodnotu.',
      unique: 'Prosím ujistěte se, že hodnota je jedinečná.'
   });
}));