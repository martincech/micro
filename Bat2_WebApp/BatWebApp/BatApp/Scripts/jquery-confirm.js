﻿/*!
 * jquery.confirm
 *
 * @version 2.3.1
 *
 * @author My C-Labs
 * @author Matthieu Napoli <matthieu@mnapoli.fr>
 * @author Russel Vela
 * @author Marcus Schwarz <msspamfang@gmx.de>
 *
 * @license MIT
 * @url http://myclabs.github.io/jquery.confirm/
 */
(function($) {
   $.fn.extend({
      /**
    * Show a confirmation dialog
    * @param [options] {{title, text, confirm, cancel, confirmButton, cancelButton, post, confirmButtonClass}}
    */
      confirm: function(options) {
         // Do nothing when active confirm modal.
         if ($('.confirmation-modal').length > 0)
            return;


         // Default options
         var defaults = {
            text: "Are you sure?",
            title: "",
            confirmButton: "Yes",
            cancelButton: "Cancel",
            post: false,
            confirmButtonClass: "btn-primary",
            cancel: function (){},
            confirm: function (){}
         }
         var settings = $.extend(defaults, options);

         // Modal
         var modalHeader = '';
         if (settings.title !== '') {
            modalHeader =
               '<div class=modal-header>' +
               '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
               '<h4 class="modal-title">' + settings.title + '</h4>' +
               '</div>';
         }
         var modalHTML =
            '<div class="confirmation-modal modal" tabindex="-1" role="dialog">' +
               '<div class="modal-dialog">' +
               '<div class="modal-content">' +
               modalHeader +
               '<div class="modal-body">' + settings.text + '</div>' +
               '<div class="modal-footer">' +
               '<button class="confirm btn ' + settings.confirmButtonClass + '" type="button" data-dismiss="modal">' +
               settings.confirmButton +
               '</button>' +
               '<button class="cancel btn btn-default" type="button" data-dismiss="modal">' +
               settings.cancelButton +
               '</button>' +
               '</div>' +
               '</div>' +
               '</div>' +
               '</div>';

         var modal = $(modalHTML);

         modal.on('shown.bs.modal', function() {
            modal.find(".btn-primary:first").focus();
         });
         modal.on('hidden.bs.modal', function() {
            modal.remove();
         });
         modal.find(".confirm").click(function() {
            settings.confirm(settings.button);
         });
         modal.find(".cancel").click(function() {
            settings.cancel(settings.button);
         });

         // Show the modal
         $("body").append(modal);
         modal.modal('show');
      }
   });
})(jQuery);