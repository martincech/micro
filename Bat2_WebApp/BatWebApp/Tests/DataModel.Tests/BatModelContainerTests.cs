﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataModel.Tests
{
   [TestClass]
   public class BatModelContainerTests
   {
      [TestMethod]
      public void CanInstantiate()
      {
         using (var dbContext = new BatModelContainer())
         {
            Assert.IsNotNull(dbContext);
         }
      }
   }
}