﻿using System.Net.Http;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BatApp.Tests.Routes
{
   [TestClass]
   public class WebApiRouteTests
   {
      private const string BasePath = "http://localhost:49942/api/" + ApiVersion1 + "/";
      private const string ApiVersion1 = "v1";
      private static HttpConfiguration _config;

      #region Class Initialize

      [ClassInitialize]
      public static void Init(TestContext context)
      {
         // arrange
         _config = new HttpConfiguration();
         WebApiConfig.Register(_config);

         //Web Api routes
         _config.Routes.MapHttpRoute(
            name: "DefaultApi",
            routeTemplate: "api/{apiVersion}/{controller}/{id}",
            defaults: new { id = RouteParameter.Optional }
            );

         _config.Routes.MapHttpRoute(
            name: "Stats2",
            routeTemplate: "api/{apiVersion}/Bat2s/{key}/{controller}/{id}",
            defaults: new { controller = "Stats", id = RouteParameter.Optional },
            constraints: new { controller = @".*Stats" }
            );

         _config.EnsureInitialized();
      }

      #endregion

      [TestMethod]
      public void WebApi_Test_Routes()
      {
         TestRoute(BasePath + "Bat2s", "GET", "Bat2s", ApiVersion1);
         TestRoute(BasePath + "Bat2s/139", "GET", "Bat2s", ApiVersion1, id: "139");
       
         TestRoute(BasePath + "Bat2s/139/Stats", "GET", "Stats", ApiVersion1, "139");
         TestRoute(BasePath + "Bat2s/139/Stats/3059", "GET", "Stats", ApiVersion1, "139", "3059");
         TestRoute(BasePath + "Stats?key=139", "GET", "Stats", ApiVersion1, "139");
         TestRoute(BasePath + "Stats/3059?key=142", "GET", "Stats", ApiVersion1, "142", "3059");

         TestRoute(BasePath + "Bat2OldStatistics", "POST", "Bat2OldStatistics", ApiVersion1);
         TestRoute(BasePath + "Bat2OldFlocks", "POST", "Bat2OldFlocks", ApiVersion1);
         TestRoute(BasePath + "Bat2OldCurves", "POST", "Bat2OldCurves", ApiVersion1);
      }

      #region Private helpers

      private void TestRoute(string url, string method, string controller, string apiVersion, string key = "", string id = "")
      {
         var request = new HttpRequestMessage(new HttpMethod(method), url);

         // act
         var routeData = _config.Routes.GetRouteData(request);
         Assert.IsNotNull(routeData);
         Assert.AreEqual(apiVersion, routeData.Values["apiVersion"]);
         Assert.AreEqual(controller, routeData.Values["controller"]);
         Assert.AreEqual(id, routeData.Values["id"].ToString());
         if (routeData.Values.ContainsKey("key"))
         {
            Assert.AreEqual(key, routeData.Values["key"]);
         }
      }

      #endregion
   }
}
