﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using DataModel;
using Bat2Library;
using BatApp.Controllers.V1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BatApp.Tests.Controllers.V1
{
   [TestClass]
   public class StatsControllerTests
   {
      [TestInitialize]
      public void Init()
      {
         ICollection<User> users = new Collection<User>();
         var baseController = new MockController();
         users = SampleData.GetCreatedUsers();

         using (var dataContext = new BatModelContainer())
         {
            if (dataContext.Database.Exists())
            {
               dataContext.Database.Delete();
            }
            dataContext.Database.Create();
            dataContext.Users.AddRange(users);
            var scale1 = new Scale { Name = "scale01", SerialNumber = 123456 };
            var stat = new Stat
            {
               Count = 5,
               Average = 20,
               Gain = 1,
               Sigma = 100,
               Cv = 50,
               Uni = 88,
               Sex = SexE.SEX_UNDEFINED,
               Date = DateTime.Now
            };
            scale1.Stat = new List<Stat> { stat };
            dataContext.Scales.Add(scale1);
            var user = users.FirstOrDefault();
            dataContext.SaveChanges();
            baseController.SetExecutiveUser(user);
         }
      }

      [TestMethod]
      public void GetAllStatistics()
      {
         var controller = new StatsController();
         using (var dataContext = new BatModelContainer())
         {
            var scale = dataContext.Scales.FirstOrDefault(x => x.SerialNumber == 123456);
            Assert.IsNotNull(scale);

            var s = controller.Get(scale.Id);
            var stats = s as IList<Stat> ?? s.ToList();
            Assert.AreEqual(1, stats.Count());
            Assert.AreEqual(5, stats.First().Count);
            Assert.AreEqual(20, stats.First().Average);
            Assert.AreEqual(1, stats.First().Gain);
            Assert.AreEqual(100, stats.First().Sigma);
            Assert.AreEqual(50, stats.First().Cv);
            Assert.AreEqual(88, stats.First().Uni);
            Assert.AreEqual(SexE.SEX_UNDEFINED, stats.First().Sex);
         }
      }

      [TestMethod]
      public void GetStatisticById_StatisticExist()
      {
         var controller = new StatsController();
         using (var dataContext = new BatModelContainer())
         {
            var scale = dataContext.Scales.FirstOrDefault(x => x.SerialNumber == 123456);
            Assert.IsNotNull(scale);
            var dbStat = scale.Stat.FirstOrDefault(x => x.Sex == SexE.SEX_UNDEFINED);
            Assert.IsNotNull(dbStat);

            var stat = controller.Get(scale.Id, dbStat.Id);
            Assert.AreEqual(5, stat.Count);
            Assert.AreEqual(20, stat.Average);
            Assert.AreEqual(1, stat.Gain);
            Assert.AreEqual(100, stat.Sigma);
            Assert.AreEqual(50, stat.Cv);
            Assert.AreEqual(88, stat.Uni);
            Assert.AreEqual(SexE.SEX_UNDEFINED, stat.Sex);
         }
      }

      [TestMethod]
      public void GetStatisticById_StatisticNonExist()
      {
         var controller = new StatsController();
         using (var dataContext = new BatModelContainer())
         {
            var scale = dataContext.Scales.FirstOrDefault(x => x.SerialNumber == 123456);
            Assert.IsNotNull(scale);
            var maxId = dataContext.Stats.Max(s => s.Id);

            var stat = controller.Get(scale.Id, maxId + 1);
            Assert.IsNull(stat);
         }
      }
   }
}
