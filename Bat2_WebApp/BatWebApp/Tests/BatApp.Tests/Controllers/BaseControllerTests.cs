﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using BatApp.Controllers;
using DataContext;
using DataModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BatApp.Tests.Controllers
{
   [TestClass]
   public class BaseControllerTests
   {

      private MockController controller;
      private User user;

      [TestInitialize]
      public void Init()
      {
         controller = new MockController();
         var company = new Company {Id = 0, Name = "VEIT"};
         user = new User {UserName = "Pepa", Id = Guid.NewGuid(), CreationDate = DateTime.Now, Company = company,CompanyId = company.Id};
         company.Users = new Collection<User> {user};
         user.Roles = new Collection<Role>
         {
            new Role {Id = Guid.NewGuid(), Name = Constants.ADMIN, Users = new Collection<User> {user}},
            new Role {Id = Guid.NewGuid(), Name = Constants.COMPANY_ADMIN, Users = new Collection<User> {user}}
         };
         controller.SetExecutiveUser(user);

         using (var dataContext = new BatModelContainer())
         {
            if (dataContext.Database.Exists())
            {
               dataContext.Database.Delete();
            }
            dataContext.Database.Create();
            Assert.IsTrue(dataContext.Database.Exists());
            dataContext.Users.Add(user);
            dataContext.SaveChanges();
         }
      }

      [TestCleanup]
      public void Clean()
      {
      }
      [TestMethod]
      public void HasIdAsSpecified()
      {
         Assert.AreEqual(user.Id, controller.UserGuid());
      }

      [TestMethod]
      public void HasContext()
      {
         Assert.IsNotNull(controller.BatContext());
      }

      [TestMethod]
      public void HasUser()
      {
         Assert.IsNotNull(controller.GetCurrentUser());
         Assert.AreEqual(user.Id, controller.GetCurrentUser().Id);
         Assert.AreEqual(user.UserName, controller.GetCurrentUser().UserName);
         CollectionAssert.AreEquivalent(user.Roles.Select(r => r.Name).ToList(), controller.GetCurrentUser().Roles.Select(r => r.Name).ToList());
      }
   }

   internal class MockController : BaseController
   {
      public Guid UserGuid()
      {
         return CurrentUserId;
      }

      public BatContext BatContext()
      {
         return Context;
      }

      public User GetCurrentUser()
      {
         return CurrentUser;
      }
   }
}
