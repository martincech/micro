﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;


namespace BatApp.Tests.Controllers
{
   public static class SampleData
   {
      private static Random randomGen;
      private static Random RandomGen 
      { 
         get
         {
            if(randomGen == null)
            {
               randomGen = new Random();
            }
            return randomGen;
         }
      }

      public static void CreateDatabase()
      {
         ICollection<User> users = GetCreatedUsers();
         using (var db = new BatModelContainer())
         {
            if (db.Database.Exists())
            {
               db.Database.Delete();
            }
            db.Database.Create();


            db.Users.AddRange(users);
            db.SaveChanges();
            List<Scale> scales = CreateScales(users.First().Company, 5).ToList();
            scales.AddRange(CreateScales(users.Last().Company, 5));
           
            foreach (var scale in scales)
            {
               scale.Flocks = CreateFlocks(scale, 5);
               scale.Stat = CreateStats(scale, 5);
            }
            db.Scales.AddRange(scales);
            db.SaveChanges();

            //db.Users.AddRange(users);
           
            db.SaveChanges();
         }

      }

      public static ICollection<User> GetCreatedUsers() 
      {
         var users = new Collection<User>();
         var admin = SampleData.CreateRoles(new string[] { Constants.ADMIN });
         var farmer_admin =  SampleData.CreateRoles(new string[] { Constants.COMPANY_ADMIN });
         var user = SampleData.CreateRoles(new string[] { Constants.USER });
         var flock_management =  SampleData.CreateRoles(new string[] { Constants.FLOCK_MANAGEMENT });

         var company = SampleData.CreateCompany("Company_1");
         users.Add(SampleData.CreateUser(company, "Petr_C1_A", admin));
         users.Add(SampleData.CreateUser(company, "Pavel_C1_FA", farmer_admin, users.FirstOrDefault()));
         users.Add(SampleData.CreateUser(company, "Jan_C1_U", user, users.FirstOrDefault()));
         users.Add(SampleData.CreateUser(company, "Tomas_C1_FM", flock_management, users.FirstOrDefault()));

         company = SampleData.CreateCompany("Company_2");
         users.Add(SampleData.CreateUser(company, "Petr_C2_A", admin));
         users.Add(SampleData.CreateUser(company, "Pavel_C2_FA", farmer_admin));
         users.Add(SampleData.CreateUser(company, "Karel_U", user, users[5]));
         users.Add(SampleData.CreateUser(company, "Tomas_C2_FM", flock_management));
         return users;
      }

      public static User CreateUser(Company company, string userName, ICollection<Role> privileges, User parent = null)
      {
         return new User()
         {
            UserName = userName,
            Id = Guid.NewGuid(),
            CreationDate = DateTime.Now,
            Company = company,
            EmailConfirmed = true,
            Roles = privileges,
            Parent = parent,
            PasswordHash = "AFvF/VX+na2ePxTjK3EWWdag4+xWIGLmta/ozU0yYayOoe/FDnyqlAGELj2rTMjOvw==",//Admin_123
            SecurityStamp = "a3decdf4-4505-405c-8d83-06a0a0999a2d"
         };
      }

      public static Company CreateCompany(string name) 
      {
         return new Company() { Name = name };
      }

      public static ICollection<Role> CreateRoles(string[] roles)
      {
         Collection<Role> rolesColl = new Collection<Role>();
         Collection<string> existingRoles = new Collection<string>()
         {
            Constants.ADMIN,
            Constants.COMPANY_ADMIN,
            Constants.USER,
            Constants.FLOCK_MANAGEMENT
         };

         foreach (string role in roles)
         {
            if (existingRoles.Contains(role))
            {
               rolesColl.Add(new Role() { Id = Guid.NewGuid(), Name = role });
            }
            else
            {
               throw new Exception(role + " : This privilige do not exists!");
            }
         }
         return rolesColl;
      }

      public static ICollection<Curve> CreateCurves(Company comapny, int count)
      {
         var list = new List<Curve>();

         for (int i = 0; i < count; i++)
         {
            list.Add(new Curve()
            {
               Name = "Curve_" + i,
               CompanyId = comapny.Id,
               CurveValues = CreateCurveValues(5)
            });
         }
         return list;
      }

      public static ICollection<CurveValue> CreateCurveValues(int count)
      {
         var list = new List<CurveValue>();

         for (int i = 0; i < count; i++)
         {
            list.Add(new CurveValue()
            { 
               Day = i,
               Weight = RandomGen.NextDouble() * 10
            });
         }
         return list;
      }

      public static ICollection<Scale> CreateScales(Company company, int count) 
      {
         ICollection<Scale> scaleList = new Collection<Scale>();
        
         for(int i = 0;i<count;i++)
         {
            scaleList.Add(new Scale() { Company = company, Name = "Scale_" + i, SerialNumber = RandomGen.Next(10000) + i, Users = company.Users });
         }

         return scaleList;
      }

      public static ICollection<Flock> CreateFlocks(Scale scale, int count)
      {
         ICollection<Flock> flockList = new Collection<Flock>();
         for (int i = 0; i < count; i++)
         {
            flockList.Add(new Flock() { Name = "Flock_" + i, Scale = scale, InitialAge = 1, StartDate = DateTime.Now});
         }
         return flockList;
      }

      public static ICollection<Stat> CreateStats(Scale scale,int count) 
      {
        
         ICollection<Stat> stats = new Collection<Stat>();

         for(int i =0; i < count; i++) 
         {
            stats.Add(new Stat() 
            {
               Average = RandomGen.NextDouble(),
               Count = RandomGen.Next(1000),
               Cv = RandomGen.Next(20),
               Scale = scale,
               Uni = RandomGen.Next(100),
               Date = DateTime.Now,
               Day = RandomGen.Next(100),
               Sex = (RandomGen.Next(2) % 2) == 1 ? Bat2Library.SexE.SEX_MALE : Bat2Library.SexE.SEX_FEMALE,
               Gain =  RandomGen.NextDouble(),
               Sigma = RandomGen.NextDouble()
            });
         }
         return stats;
      }
   }
}
