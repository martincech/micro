﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using BatApp.Infrastructure;
using DataModel;
using Moq;
using Ninject;
using Utilities.IOC;
using Claim = System.Security.Claims.Claim;

namespace BatApp.Tests
{
   public static class BaseControllerFake
   {
      private static ControllerContext CreateControllerContext(
         string userName, string userId, string[] roles = null,
         string targetUrl = null)
      {
         var controllerContext = new Mock<ControllerContext>();
         var request = new Mock<HttpRequestBase>();
         var response = new Mock<HttpResponseBase>();
         var httpContext = new Mock<HttpContextBase>();
         var identity = new GenericIdentity(userName);
         var principal = new GenericPrincipal(identity, roles);
         var stream = new MemoryStream();

         var claims = new List<Claim>
         {
            new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name", userName),
            new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", userId)
         };
         identity.AddClaims(claims);

         httpContext.Setup(m => m.Request).Returns(request.Object);
         httpContext.Setup(m => m.Response).Returns(response.Object);
         httpContext.Setup(t => t.User).Returns(principal);
         //Added to set current principal to active user
         Thread.CurrentPrincipal = principal;
         request.Setup(m => m.AppRelativeCurrentExecutionFilePath).Returns(targetUrl);
         request.Setup(m => m.IsAuthenticated).Returns(true);

         request.Setup(m => m.InputStream).Returns(stream);

         response.Setup(m => m.ApplyAppPathModifier(It.IsAny<string>())).Returns<string>(s => s);
         controllerContext.Setup(t => t.HttpContext).Returns(httpContext.Object);
         return controllerContext.Object;
      }

      /// <summary>
      /// Sets user to current controller instance.
      /// </summary>
      /// <param name="controller"></param>
      /// <param name="user"><see cref="User"/> to be set as executive user.
      ///  Its <see cref="User.UserName"/>, <see cref="User.Id"/>,
      /// <see cref="User.Roles"/> should be specified to get valid results</param>
      public static void SetExecutiveUser(this Controller controller, User user)
      {
         SetExecutiveUser(controller, user.UserName, user.Id, user.Roles.Select(r => r.Name).ToArray());
      }

      public static void SetExecutiveUser(this Controller controller, string userName, Guid userId,
         string[] roles = null)
      {
         var controllerName = controller.GetType().Name;
         controller.ControllerContext = CreateControllerContext(
            userName, userId.ToString(), roles,
            controllerName.Substring(0, controllerName.IndexOf("Controller", StringComparison.Ordinal)));
      }

      public static HttpRequestBase CreateHttpRequest(string targetUrl = null, string httpMethod = "GET",
         string userName = "", string password = "")
      {
         var request = new Mock<HttpRequestBase>();
         request.Setup(m => m.AppRelativeCurrentExecutionFilePath).Returns(targetUrl);
         request.Setup(m => m.HttpMethod).Returns(httpMethod);
         if (!String.IsNullOrEmpty(userName))
         {
            request.Setup(m => m.Headers).Returns(() =>
            {
               var headers = new NameValueCollection
               {
                  {
                     // Authorization
                     HttpRequestHeader.Authorization.ToString(),
                     String.Format("{0} {1}", "Basic",
                        Convert.ToBase64String(Encoding.ASCII.GetBytes(String.Format("{0}:{1}", userName, password))))
                  }
               };
               return headers;
            });
         }
         request.SetupAllProperties();
         return request.Object;
      }


      public static HttpContextBase CreateHttpContext(string targetUrl = null, string httpMethod = "GET",
         string userName = "", string password = "")
      {
         var request = CreateHttpRequest(targetUrl, httpMethod, userName, password);

         var mockResponse = new Mock<HttpResponseBase>();
         mockResponse.SetupAllProperties();
         mockResponse.Setup(m => m.ApplyAppPathModifier(It.IsAny<string>())).Returns<string>(s => s);
         var mockContext = new Mock<HttpContextBase>();
         mockContext.SetupAllProperties();
         mockContext.Setup(m => m.Request).Returns(request);
         mockContext.Setup(m => m.Response).Returns(mockResponse.Object);
         return mockContext.Object;
      }

      public static void InitNinject()
      {
         IocContainer.Container = new NinjectIocContainer(new StandardKernel());
         DependencyResolver.SetResolver(new NinjectDependencyResolver(IocContainer.Container as IKernel));
      }
   }
}