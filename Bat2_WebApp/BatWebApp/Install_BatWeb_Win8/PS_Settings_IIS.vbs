Set objShell = CreateObject("Shell.Application")
'*----------------------------------------------
'*Create BatWebPool
Dim createPool
Dim assignPool
Dim startWebSite

createPool="cd IIS:\AppPools;$appPool=New-Item ""BatWebPool"";$appPool | Set-ItemProperty -Name ""managedRuntimeVersion"" -value ""v4.0"";$appPool.processModel.identityType=0;$appPool | Set-Item;"
assignPool="Set-ItemProperty ""IIS:\Sites\BatWeb"" ApplicationPool BatWebPool;"
startWebSite="Stop-WebSite -Name 'Default Web Site';Start-WebSite -Name 'BatWeb';"
objShell.ShellExecute "powershell","Import-Module WebAdministration;"+createPool+assignPool+startWebSite, "", "runas", 1
