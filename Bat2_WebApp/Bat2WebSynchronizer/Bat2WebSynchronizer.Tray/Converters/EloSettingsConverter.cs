﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using Bat2WebSynchronizer.Tray.Model;

namespace Bat2WebSynchronizer.Tray.Converters
{
   public class EloSettingsConverter : IMultiValueConverter
   {
      public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
      {

         if (values.Length != 2)
         {
            return null;
         }

         var com = values[0] as string;
         var settings = values[1] as ObservableCollection<EloSetting>;

         if (com == null || settings == null)
         {
            return null;
         }

         var elo = settings.FirstOrDefault(f => f.PortName == com);
         if (elo == null)
         {
            return null;
         }

         return elo.Scales;
      }

      public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
