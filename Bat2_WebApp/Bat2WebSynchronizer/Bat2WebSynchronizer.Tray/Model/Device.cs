﻿using Bat2Library.Sms;

namespace Bat2WebSynchronizer.Tray.Model
{
   public class Device
   {
      public string ModemName { get; set; }
      public string ProductName { get; set; }
      public int SignalStrength { get; set; }
      public SmsEvent Event { get; set; }
      public DeviceType Type { get; set; }
      public string SerialNumber { get; set; }
      public string PortName { get; set; }
   }

   public enum DeviceType
   {
      ELO,
      Modem
   }
}
