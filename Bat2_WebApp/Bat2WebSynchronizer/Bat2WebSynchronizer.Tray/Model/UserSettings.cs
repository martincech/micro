﻿using System.Configuration;
using Utilities;

namespace Bat2WebSynchronizer.Tray.Model
{
    /// <summary>
    /// User settings with default values
    /// </summary>
    public class UserSettings : ApplicationSettingsBase
    {
        private static UserSettings instance;

        public static UserSettings Instance
        {
            get { return instance ?? (instance = new UserSettings()); }
        }

        private const string ENCIPHER_HASH_STRING = "AxBy123";

        [UserScopedSetting()]
        [DefaultSettingValue("")]
        public string UserName
        {
            get
            {
                return ((string)this["UserName"]);
            }
            set
            {
                this["UserName"] = value;
            }
        }

        [UserScopedSetting()]
        [DefaultSettingValue("")]
        public string Password
        {
            get
            {
                if (string.IsNullOrEmpty((string)this["Password"]))
                {
                    return "";
                }
                return Encipher.Decrypt((string)this["Password"], ENCIPHER_HASH_STRING);
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    this["Password"] = "";
                }
                else
                {
                    this["Password"] = Encipher.Encrypt(value, ENCIPHER_HASH_STRING);
                }

            }
        }

        [UserScopedSetting()]
        [DefaultSettingValue("http://www.veit.cz/")]
        public string ServerUrl
        {
            get
            {
                return ((string)this["ServerUrl"]);
            }
            set
            {
                this["ServerUrl"] = value;
            }
        }
       
        [UserScopedSetting()]
        [DefaultSettingValue("true")]
        public bool ShowNotification
        {
            get
            {
                return ((bool)this["ShowNotification"]);
            }
            set
            {
                this["ShowNotification"] = value;
            }
        }

        /// <summary>
        /// Load setting from persistent config file
        /// </summary>
        /// <returns>stored configuration</returns>
        public static Settings Load()
        {
            return new Settings
            {
                Name = Instance.UserName,
                Password = Instance.Password,
                ServerUrl = Instance.ServerUrl,
                ShowNotification = Instance.ShowNotification
            };
        }

        /// <summary>
        /// Save new setting to user persistent config file
        /// </summary>
        /// <param name="settings"></param>
        public static void Save(Settings settings)
        {
            if (!IsChanged(settings))
            {
                return;
            }
           
            Instance.UserName = settings.Name;
            Instance.Password = settings.Password;
            Instance.ServerUrl = settings.ServerUrl;
            Instance.ShowNotification = settings.ShowNotification;
            Instance.Save();
        }

        /// <summary>
        /// Check if some settings is changed
        /// </summary>
        /// <param name="settings">new settings</param>
        /// <returns>true if some setting changed</returns>
        public static bool IsChanged(Settings settings)
        {
           return
              Instance.UserName != settings.Name ||
              Instance.Password != settings.Password ||
              Instance.ServerUrl != settings.ServerUrl ||
              Instance.ShowNotification != settings.ShowNotification;
        }
    }
}
