﻿using System.Windows;
using System.Windows.Controls;
using Bat2WebSynchronizer.Tray.Model;
using Bat2WebSynchronizer.Tray.ViewModel;

namespace Bat2WebSynchronizer.Tray.View
{
   /// <summary>
   /// Interaction logic for EloView.xaml
   /// </summary>
   public partial class EloView : UserControl
   {
      public SettingsViewModel Model { get; set; }

      public EloView(SettingsViewModel model)
      {
         Model = model;
         InitializeComponent();
      }

      private void Scan_Click(object sender, RoutedEventArgs e)
      {
         var setting = (EloSetting) DataContext;
         Model.StartScanElo.Execute(setting.PortNumber);
      }

      private void Stop_Click(object sender, RoutedEventArgs e)
      {
         var setting = (EloSetting)DataContext;
         Model.StopScanElo.Execute(setting.PortNumber);
      }
   }
}
