﻿using Bat2WebSynchronizer.Tray.Model;
using Bat2WebSynchronizer.Tray.ViewModel;
using Usb;
using Usb.ELO;
using Usb.MODEM;

namespace Bat2WebSynchronizer.Tray.Code
{
   public class CodeApp
   {
      private readonly CodeModbus modbus;
      private readonly CodeSms sms;
      private readonly CodeSync sync;
      private readonly SettingsViewModel model;

      public CodeApp(SettingsViewModel model)
      {
         this.model = model;
         //Load user settings from file
         this.model.UserSettings = UserSettings.Load();

         modbus = new CodeModbus(model);
         sms = new CodeSms(model);
         sync = new CodeSync(model);

         UsbDeviceManager.Instance.DeviceConnected += InstanceOnDeviceConnected;
         UsbDeviceManager.Instance.DeviceDisconnected += InstanceOnDeviceDisconnected;
      }

      /// <summary>
      /// Device connected
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="usbDevice"></param>
      private void InstanceOnDeviceConnected(object sender, UsbDevice usbDevice)
      {
         var elo = usbDevice as EloDevice;
         if (elo != null && modbus != null)
         {
            model.EloList.Add(elo);
         }

         var device = usbDevice as ModemDevice;
         if (device != null && sms != null)
         {
            model.ModemList.Add(device);
         }
      }

      /// <summary>
      /// Device disconnected
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="usbDevice"></param>
      private void InstanceOnDeviceDisconnected(object sender, UsbDevice usbDevice)
      {
         var elo = usbDevice as EloDevice;
         if (elo != null)
         {
            model.EloList.Remove(elo);
         }

         var device = usbDevice as ModemDevice;
         if (device != null)
         {
            model.ModemList.Remove(device);
         }
      }
   }
}
