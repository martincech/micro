﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Threading;
using System.Timers;
using System.Windows.Input;
using Bat2Library.Sms;
using Bat2Library.Utilities.Sms;
using Bat2Library.Utilities.Sms.Models;
using Bat2WebSynchronizer.Tray.Properties;
using Bat2WebSynchronizer.Tray.ViewModel;
using Desktop.Wpf.Applications;
using Desktop.Wpf.Presentation;
using Newtonsoft.Json;
using Services.WebApi;
using Timer = System.Timers.Timer;

namespace Bat2WebSynchronizer.Tray.Code
{
   public class CodeSync
   {
      private static readonly object LOCK_OBJECT = new object();

      private readonly SettingsViewModel model;
      //Data file name for messages in right format
      private const string TOSYNC = @"\ToSync.xml";
      //Path to server controller 
      private const string CONTROLLER = "/api/V1/Stats";
      //Dump file name for messages in wrong format
      private const string DUMP_FILE = @"\Dump.xml";
      //Data file name for synced messages in right format
      private const string SYNCED = @"\Synced.xml";

      private const int SYNC_TIMER_INTERVAL = 30 * 1000; // 30s

      private Timer syncTimer;

      public CodeSync(SettingsViewModel model)
      {
         this.model = model;
         this.model.UserSettings.PropertyChanged += UserSettingsOnPropertyChanged;

         model.LoginCommand = LoginCommand;
         InitData();
         StartTimer();

         this.model.UserSettings.ToSyncSms.CollectionChanged += ToSyncSmsOnCollectionChanged;
      }

      /// <summary>
      /// Load not synced data from file
      /// </summary>
      private void InitData()
      {
         var toSync = XmlSerializer.Load<SmsStatus>(model.Folder + TOSYNC);
         foreach (var smsStatus in toSync.Where(w => w.Synced == false))
         {
            model.UserSettings.ToSyncSms.Add(new KeyValuePair<DateTime, SmsStatus>(DateTime.Now, smsStatus));
            model.UserSettings.RecievedSmsCount++;
         }
      }

      /// <summary>
      /// Create timer to periodicaly try to sync new data
      /// </summary>
      private void StartTimer()
      {
         //Try to sync sms and check connected elo scales
         syncTimer = new Timer(SYNC_TIMER_INTERVAL);
         syncTimer.Elapsed += syncTimer_Elapsed;
         syncTimer.Start();
         syncTimer_Elapsed(null, null);
      }

      /// <summary>
      /// Timer tick to sync new data
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void syncTimer_Elapsed(object sender, ElapsedEventArgs e)
      {
         LoginTest(false);
         if (model.UserSettings.ToSyncSms.Where(w => w.Value.Synced == false).ToList().Count > 0)
         {
            Sync();
         }
      }

      /// <summary>
      /// Try to sync new data
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="notifyCollectionChangedEventArgs"></param>
      private void ToSyncSmsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
      {
         if (notifyCollectionChangedEventArgs.Action == NotifyCollectionChangedAction.Add)
         {
            Sync();
         }
      }

      /// <summary>
      /// Try to sync data after changes in user settings
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void UserSettingsOnPropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         if (e.PropertyName.Equals("Name") || e.PropertyName.Equals("Password") || e.PropertyName.Equals("ServerUrl"))
         {
            model.Logged = false;
            if (model.UserSettings.ToSyncSms.Where(w => w.Value.Synced == false).ToList().Count > 0)
            {
               syncTimer.Stop();
               Sync();
               syncTimer.Start();
            }
         }
      }

      /// <summary>
      /// Sync messages from collection ToSyncSms with server
      /// </summary>
      private void Sync()
      {
         if (string.IsNullOrEmpty(model.UserSettings.ServerUrl)
               || string.IsNullOrEmpty(model.UserSettings.Name)
               || string.IsNullOrEmpty(model.UserSettings.Password))
         {
            return;
         }

         ThreadPool.QueueUserWorkItem(param =>
         {
            lock (LOCK_OBJECT)
            {
               List<KeyValuePair<DateTime, SmsStatus>> collection = null;
               DispatcherHelper.RunAsync(() =>
               {
                  collection = model.UserSettings.ToSyncSms.Where(x => x.Value.Synced == false).ToList();
               }).Wait();

               foreach (var message in collection)
               {
                  Thread.Sleep(200); // if not delayed server throw internal error 
                  SyncMessage(message);
               }
            }
         });
      }

      /// <summary>
      /// Test if user can sync data to server
      /// </summary>
      /// <param name="showMessage"></param>
      private void LoginTest(bool showMessage)
      {
         ThreadPool.QueueUserWorkItem(param =>
         {
            var client = new WebApiClient(model.UserSettings.ServerUrl, model.UserSettings.Name, model.UserSettings.Password);
            string msg = "";
            var role = client.UserRole();
            if (role == null || role.Equals(string.Empty))
            {
               msg = client.LastErrorMsg;
               model.Logged = false;
            }
            else if (role.Equals("company admin"))
            {
               model.Logged = true;
            }
            else
            {
               model.Logged = false;
               msg = "Only company admin can perform synchronization";
            }

            if (showMessage && !model.Logged)
            {
               CodeMessageBox.Instance.Show(msg, model.StayOpened);
            }
         });
      }

      private ICommand LoginCommand
      {
         get
         {
            return new RelayCommand(
                  () =>
                  {
                     LoginTest(true);
                  },
                  () => !model.UserSettings.Name.Equals(string.Empty) &&
                        !model.UserSettings.Password.Equals(string.Empty) &&
                        !model.UserSettings.ServerUrl.Equals(string.Empty) &&
                        !model.Logged);
         }

      }

      /// <summary>
      /// Sync data to server
      /// </summary>
      /// <param name="message">data to sync</param>
      private void SyncMessage(KeyValuePair<DateTime, SmsStatus> message)
      {
         SmsData data;
         if (SmsDecode.Decode(message.Value.SmsText, out data))
         {
            //Correct server url
            var url = model.UserSettings.ServerUrl.EndsWith("/")
                ? model.UserSettings.ServerUrl.Remove(model.UserSettings.ServerUrl.Length - 1)
                : model.UserSettings.ServerUrl;

            HttpStatusCode result;
            try
            {
               data.PhoneNumber = message.Value.SmsNumber;
               result = HttpSender.SendMessage(JsonConvert.SerializeObject(data), model.UserSettings.Name, model.UserSettings.Password, url + CONTROLLER);
            }
            catch (Exception)
            {
               result = HttpStatusCode.NotFound;

            }
            var logMessage = "";

            var syncOk = false;
            switch (result)
            {
               case HttpStatusCode.OK:
                  syncOk = true;
                  model.Logged = true;
                  break;
               case HttpStatusCode.Forbidden:
                  logMessage = Resources.SYNC_RESULT_FORBIDDEN;
                  break;
               case HttpStatusCode.Unauthorized:
                  logMessage = Resources.SYNC_RESULT_UNAUTHORIZATED;
                  break;
               case HttpStatusCode.NotAcceptable:
                  logMessage = Resources.SYNC_RESULT_NOTACCEPTABLE;
                  break;
               case HttpStatusCode.NotFound:
               case HttpStatusCode.InternalServerError:
                  logMessage = Resources.SYNC_RESULT_NOTFOUND;
                  model.Logged = false;
                  break;
               default:
                  logMessage = Resources.SYNC_RESULT_UNKNOWNERROR;
                  break;
            }

            DispatcherHelper.RunAsync(() =>
            {
               if (syncOk)
               {
                  model.UserSettings.SyncedSms.Add(data);
                  message.Value.Synced = true;
                  model.UserSettings.LogMessage.Add(new KeyValuePair<DateTime, string>(message.Key, String.Format(Resources.LOG_SMS_SYNCED, message.Value.SmsText)));
                  XmlSerializer.Append(message.Value, model.Folder + SYNCED);
               }
               else
               {
                  message.Value.Synced = false;
                  model.UserSettings.LogMessage.Add(new KeyValuePair<DateTime, string>(message.Key, logMessage));
               }
               XmlSerializer.Update(model.UserSettings.ToSyncSms.Where(w => w.Value.Synced == false).Select(s => s.Value).ToList(), model.Folder + TOSYNC);
            });
         }
         else
         {
            DispatcherHelper.RunAsync(() =>
            {
               model.UserSettings.LogMessage.Add(new KeyValuePair<DateTime, string>(message.Key, Resources.LOG_WRONG_FORMAT));
               model.UserSettings.ToSyncSms.Remove(message);
               XmlSerializer.Append(message.Value, model.Folder + DUMP_FILE);
               XmlSerializer.Update(model.UserSettings.ToSyncSms.Where(w => w.Value.Synced == false).Select(s => s.Value).ToList(), model.Folder + TOSYNC);
            });
         }
         
      }
   }
}
