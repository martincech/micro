﻿using System.IO;
using Bat2Library.Sms;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bat2WebSynchronizer.Tests.Bat2Library.Sms
{
    [TestClass]
    public class SmsXmlDocumentTests
    {
        private SmsStatus smsMessage;
        private string fileName = "file.xml";

        [TestInitialize]
        public void Init()
        {
            smsMessage = new SmsStatus()
            {
                SignalStrength = 25,
                Event = SmsEvent.SMS_READ_OK,
                ModemName = "Modem 1",
                OperatorName = "T Mobile",
                PortName = "COM1",
                SmsNumber = "442336889",
                SmsText = ""
            };
        }

        [TestMethod]
        public void Xml_Save_When_FileNotExist()
        {
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
            SaveSmsXml.Save(fileName,smsMessage);
        }

        [TestMethod]
        public void Xml_Save_When_FileExist()
        {
            if (!File.Exists(fileName))
            {
                Xml_Save_When_FileNotExist();
            }
            SaveSmsXml.Save(fileName, smsMessage);
        }
    }
}
