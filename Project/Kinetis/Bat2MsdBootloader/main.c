//******************************************************************************
//
//    Main.c        HID bootloader
//    Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "Cpu/Cpu.h"
#include "Bootloader.h"
#include "Fw/FwLoader.h"
#include "Fw/Fw.h"
#include "Cpu/Pwm.h"
#include "Memory/Nvm.h"
#include "Kbd/Kbx.h"
#include "System/SysTimer.h"
#include "Usb/UsbModule.h"
#include "Power/Power.h"

// Version :
static const TDeviceVersion VersionDefault __attribute__ ((section (".bootloader_version"))) = {
   /* Class */        DEVICE_SCALE_COMPACT,
   /* Modification */ 0,
   /* Software */     0,
   /* Hardware */     BAT2_HW_VERSION,
   /* SerialNumber */ 0L
};
TDeviceVersion BootloaderVersion;
volatile TBootloaderData BootloaderData __attribute__ ((section (".bootloader_data")));

#include "Fw/FwDef.h"

TYesNo FwVersionCheck( TFwVersion *_Version)
// Check 
{
   TDeviceVersion *Version = &_Version->Version;

   if(Version->Class != BootloaderVersion.Class) {
      return NO;
   }
   if(Version->Hardware != BootloaderVersion.Hardware) {
      return NO;
   }
   return YES;
}


void main(void)
{
TYesNo HwTriggered;
TYesNo SwTriggered;
TYesNo Success;
TYesNo User;

   if(!CpuIsSoftwareReset()) {
      BootloaderData.Cmd = BOOT_CMD_INVALID;
   }

   User = NO;

   switch(BootloaderData.Cmd) {
      case BOOT_CMD_BOOTLOADER:
         CpuInit();
         break;

      case BOOT_CMD_APP:
         if(FwValidate()) {
            FwApplicationRun(); // nesm� b�t inicializov�n absolutn� ��dn� HW!
         } else {
            // asi vypni
            forever {
               WatchDog();
            }
         }
         break;

      default:
         CpuInit();
         KbxInit();
         if(KbxRead() == K_ESC) {
            while(KbxRead() != K_RELEASED);
            User = YES;
         } else {
            BootloaderData.Cmd = BOOT_CMD_APP;
            BootloaderData.Status = BOOT_STATUS_NORMAL;
            CpuReset();
         }
         break;
   }

   PowerInit();
   NvmInit();
   InterruptEnable();

   PwmInit( BACKLIGHT_PWM_FREQUENCY);
   PwmDutySet( BACKLIGHT_CHANNEL, 20);
   PwmStart( BACKLIGHT_CHANNEL);

   BootloaderVersion.Class = VersionDefault.Class;
   BootloaderVersion.Modification = VersionDefault.Modification;
   BootloaderVersion.Hardware = VersionDefault.Hardware;
   BootloaderVersion.Software = 0x0100;


   if( User) {
      UsbModuleInit();
      UsbPortInit();
      SysTimerStart();
      PowerPortInit();
      BootloaderInit();

      Success = NO;
      forever {
         BootloaderExecute();
         switch(BootloaderState()) {
            case BOOTLOADER_BUSY:
               break;

            case BOOTLOADER_DONE:
               Success = YES;
               break;

            default:
               // oznameni o erroru
               BootloaderData.Cmd = BOOT_CMD_APP;
               BootloaderData.Status = BOOT_STATUS_ERROR_UPDATE;
               CpuReset();
               break;
         }

         WatchDog();

         if(Success) {
            break;
         }
      }
   }

   FwLoaderInit();

   if(!FwLoaderReady()) {
      BootloaderData.Cmd = BOOT_CMD_APP;
      BootloaderData.Status = BOOT_STATUS_ERROR_UPDATE;
      CpuReset();
   }

   forever {
      FwLoaderExecute();
      switch(FwLoaderState()) {
         case FW_LOADER_BUSY:
            break;

         case FW_LOADER_DONE:
            BootloaderData.Cmd = BOOT_CMD_APP;
            BootloaderData.Status = BOOT_STATUS_NORMAL;
            CpuReset();
            break;

         default:
            BootloaderData.Cmd = BOOT_CMD_APP;
            BootloaderData.Status = BOOT_STATUS_ERROR_UPDATE;
            CpuReset();
            break;
      }
      WatchDog();
   }
}

#include "Memory/File.h"
#include "Memory/Nvm.h"
#include "Memory/NvmLayout.h"

TYesNo FileLoad( TFile *File, TFileAddress Address, void *Data, int Size) {
   if(Address + Size > NVM_FIRMWARE_SIZE) {
      return NO;
   }
   NvmLoad(NVM_FIRMWARE_START + Address, Data, Size);
   return YES;
}

byte FileByteRead( TFile *File, TFileAddress Address)
// Returns value from <Address>
{
byte Data;
   if(!FileLoad( File, Address, &Data, sizeof(byte))) {
      return 0;
   }
   return Data;
} // FileByteRead

TYesNo FileSave( TFile *File, TFileAddress Address, const void *Data, int Size) {
   if(Address + Size > NVM_FIRMWARE_SIZE) {
      return NO;
   }
   return NvmSave(NVM_FIRMWARE_START + Address, Data, Size);
}

void FileByteWrite( TFile *File, TFileAddress Address, byte Value)
// Returns value from <Address>
{
   FileSave( File, Address, &Value, sizeof(byte));
} // FileByteRead

int ConfigurationSerialNumber( void) {
   return 0;
}

void SysTimerExecute( void) {}

#include "Multitasking/Multitasking.h"

void MultitaskingReschedule( void) {
}

void MultitaskingMutexSet( TMutex *Mutex) {
}

void MultitaskingMutexRelease( TMutex *Mutex) {
}

TYesNo UsbHostEnable(TYesNo Enable)
// Enable Host
{
   UsbModuleSwitchOn();
   UsbModulePullDownEnable(YES);
   PowerUsbHostEn();
   UsbSwitchEnable();
   UsbSwitchSelectHost();
   return YES;
} // UsbHostEnable

int KbdPowerUpKey( void)
// Returns key hold after power up, or K_RELEASED
{
int Key;

   Key = KbxRead();
   SysDelay( 10);
   return( KbxRead());
} // KbdPowerUpKey