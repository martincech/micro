#include "Cpu/Cpu.h"
#include "System/System.h"
#include "Graphic/Graphic.h"
#include "Display/Backlight.h"
#include "Display/DisplayConfiguration.h"
#include "Graphic/Gpu.h"
#include "Fonts.h"
#include "Console/conio.h"
#include "Ads1232/Ads1232.h"
#include "Hardware.h"

void main(void)
{
TAdcValue Value;
   CpuInit();
   SysInit();
   BacklightInit();
   AdcInit();
   GInit();
   GSetFont( TAHOMA16);
   BacklightTest(BACKLIGHT_INTENSITY_MAX);

   PowerPortInit();
   AdcStart();

   forever {
      SysDelay(1000);
      GClear();
      GFlush();
      SysDelay(50);
      GTextAt(0, 0);
      
      Value = AdcRawRead();
      
      cprintf("Value: %08x\n", Value);

      GFlush();     
   }
}

void SysTimerExecute( void) {}