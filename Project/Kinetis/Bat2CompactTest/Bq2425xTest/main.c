#include "Cpu/Cpu.h"
#include "System/System.h"
#include "Bq2425x/Bq2425x.h"
#include "Graphic/Graphic.h"
#include "Display/Backlight.h"
#include "Display/DisplayConfiguration.h"
#include "Graphic/Gpu.h"
#include "Fonts.h"
#include "Console/conio.h"

void main(void)
{
TBq2425xMemoryMap ChargerData;
byte *Value;
   CpuInit();
   SysInit();
   BacklightInit();
   Bq2425xInit();
   GInit();
   GSetFont( TAHOMA16);
   BacklightTest(BACKLIGHT_INTENSITY_MAX);

   cprintf("Charger init");
   GFlush();
   
   Bq2425xWrite(Bq2425xAddressOf(Register2), 0x3C);

   forever {
      SysDelay(1000);
      GClear();
      GFlush();
      SysDelay(50);
      GTextAt(0, 0);
      
      if(!Bq2425xReadMultiple( &ChargerData)) {
         cprintf("Error reading");
         GFlush();
         continue;
      }

      Value = (byte *)&ChargerData;
      
      cprintf("Reg1: %02x\n", *Value++);
      cprintf("Reg2: %02x\n", *Value++);
      cprintf("Reg3: %02x\n", *Value++);
      cprintf("Reg4: %02x\n", *Value++);
      cprintf("Reg5: %02x\n", *Value++);
      cprintf("Reg6: %02x\n", *Value++);
      cprintf("Reg7: %02x\n", *Value++);

      Bq2425xCeAssert();
      cputs("CE: LOW\n");   
      
      cputs("PG: ");     
      if(Bq2425xPg()) {
         cputs("HIGH");
      } else {
         cputs("LOW");
      }
      cputch('\n');

      GFlush();

   }
}

void SysTimerExecute( void) {}