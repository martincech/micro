//*****************************************************************************
//
//    Main.c       Bat2 test main module
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Hardware.h"
#include "Cpu/Cpu.h"
#include "System/System.h"
#include "Graphic/Graphic.h"   // graphic
#include "Fonts.h"
#include "COnsole/conio.h"     // Display
#include "Ads1232/Ads1232.h"   // A/D convertor
#include "Flash/NorFlashTest.h"
#include "Fram/FramTest.h"

#include "Uart/Uart.h"

#include "Usb/Usb.h"
#include "File/Efs.h"

typedef enum {
   TEST_KEYBOARD,
   TEST_ADC,
   TEST_RTC,
   TEST_NVM_WRITE,
   TEST_NVM_CHECK,
   TEST_DISPLAY,
   TEST_IADC,
   TEST_FUEL_GAUGE,
   TEST_MSD,
   TEST_USB,
   TEST_ETHERNET,
   TEST_RS485_0,
   TEST_RS485_1,
   TEST_WIFI,
   TEST_CELLULAR
} ETest;

typedef struct {
   byte Cmd;
} TTestCmd;

typedef struct {
   struct {
      TYesNo Button0 : 1;
      TYesNo Button1 : 1;
      TYesNo Button2 : 1;
      TYesNo Button3 : 1;
      TYesNo Button4 : 1;
      TYesNo Button5 : 1;
   };
} TTestAdcReply;

typedef struct {
   dword Value;
} TTestKbdReply;

typedef struct {
   byte Result;
   union {
      TTestAdcReply Adc;
      TTestKbdReply Kbd;
   };
} TTestReply;

#define TestReplySimpleSize( )      (1)
#define TestReplySize( S)    (TestReplySimpleSize() + sizeof(TTest##S##Reply))

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

#define TEST_UART          UART_UART4
#define TEST_RECEIVE_SIZE  32
int main( void)
{
TTestReply Reply;
int ReplySize;
byte ReceiveBuffer[TEST_RECEIVE_SIZE];
TTestCmd *Cmd = &ReceiveBuffer;
   CpuInit();
   SysInit();
   PowerPortInit();
   AdcInit();
   GInit();
   GSetFont( TAHOMA16);
   FramInit();
   FlashInit();
   UsbInit();
   MultitaskingInit();
   InterruptEnable();
   /*RtcInit();
   NvmInit();
   Max17047Init();*/

   UartInit( TEST_UART);
   UartSetup( TEST_UART, 57600, UART_8BIT);
   UartTimeoutSet( TEST_UART, 0, 50);
   UartBufferSet( TEST_UART, ReceiveBuffer, TEST_RECEIVE_SIZE);
   UartModeSet( TEST_UART, UART_MODE_BINARY_SLAVE);



   forever {
      switch( UartReceiveStatus( TEST_UART)){
         case UART_RECEIVE_ACTIVE:
            continue;

         case UART_RECEIVE_TIMEOUT:
            break;

         default :
            UartReceive(TEST_UART);
            continue;
      }

      Reply.Result = NO;
      ReplySize = TestReplySimpleSize();

      switch(Cmd->Cmd) { // prijaty prikaz
         case TEST_KEYBOARD:
            Reply.Result = YES;
            Reply.Kbd.Button0 = NO;
            Reply.Kbd.Button1 = NO;
            Reply.Kbd.Button2 = NO;
            Reply.Kbd.Button3 = NO;
            Reply.Kbd.Button4 = NO;
            Reply.Kbd.Button5 = NO;
            ReplySize = TestReplySize( Kbd);
            break;

         case TEST_ADC:
            Reply.Result = YES;
            AdcStart();
            SysDelay(1000);
            Reply.Adc.Value = AdcRawRead();
            ReplySize = TestReplySize( Adc);
            AdcStop();
            break;

         case TEST_RTC:
            Reply.Result = NO;
            break;

         case TEST_NVM_WRITE:
            if(!FlashEraseAll()) {
               break;
            }
            if(!FramPageTestWrite()) {
               break;
            }
            if(!NorFlashPageTestWrite()) {
               break;
            }            
            Reply.Result = YES;
            break;

         case TEST_NVM_CHECK:
            if(!FramPageTestCheck()) {
               break;
            }
            if(!NorFlashPageTestCheck()) {
               break;
            }
            Reply.Result = YES;
            break;

         case TEST_DISPLAY:
            GSetColor( COLOR_BLACK);
            GBox( 0, 0, G_WIDTH, G_HEIGHT);
            GFlush();
            SysDelay( 500);
            cclear();
            cputs( "Display : OK\n");
            Reply.Result = YES;
            break;

         case TEST_FUEL_GAUGE:
            Reply.Result = NO;
            break;

         case TEST_MSD:
            if(!EfsInit()) {
               break;
            }
            EfsSwitchOff();
            Reply.Result = YES;
            break;

         case TEST_USB:
            break;

         case TEST_ETHERNET:
            Reply.Result = NO;
            break;

         case TEST_RS485_0:
            Reply.Result = NO;
            break;

         case TEST_RS485_1:
            Reply.Result = NO;
            break;

         case TEST_WIFI:
            Reply.Result = NO;
            break;

         case TEST_CELLULAR:
            Reply.Result = NO;
            break;

         default:
            Reply.Result = NO;
            break;
      }

      // odesli Reply
      UartSend(TEST_UART, &Reply, ReplySize);
      GFlush();

      while( UartSendStatus( TEST_UART) == UART_SEND_ACTIVE);
   }

} // main

void AdcCallback() {}

void SysTimerExecute() {
   UartTimer();
}

TYesNo SocketIfUsbConnected() {
   return NO;
}

void SocketIfUsbDeinit() {}