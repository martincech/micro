#include "Cpu/Cpu.h"
#include "System/System.h"
#include "Graphic/Graphic.h"
#include "Display/Backlight.h"
#include "Display/DisplayConfiguration.h"
#include "Graphic/Gpu.h"
#include "Fonts.h"
#include "Console/conio.h"
#include "Power/Power.h"
#include "Usb/Usb.h"
#include "Kbd/Kbd.h"

void main(void)
{
int Key;
   CpuInit();
   SysInit();
   BacklightInit();
   KbdInit();
   GInit();
   GSetFont( TAHOMA16);
   PowerInit();
   UsbInit();

   UsbDevicePowerEnable();

   forever {
      Key = SysSchedulerDefault();
      switch(Key) {
         case K_ESC:
            GTextAt(0, 0);
            GClear();
            cputs("Starting\n");
            GFlush();
            PowerBankStartPullupHigh();
            SysDelay(400);
            PowerBankStartPullupLow();
            PowerBankStartPulldownEnable();
            PowerBankEnable();
            SysDelay(400);
            PowerBankStartPulldownDisable();
            cputs("Started\n");
            cputs("POWERBANK_STAT: ");
            if(PoverBankConnected()) {
               cputs("Y");
            } else {
               cputs("N");
            }
            cputch('\n');
            PowerBankDisable();
            GFlush();
            break;

      }
   }
}

void SysTimerExecute( void) {}