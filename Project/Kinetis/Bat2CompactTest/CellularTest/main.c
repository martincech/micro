#include "Cpu/Cpu.h"
#include "System/System.h"
#include "Graphic/Graphic.h"
#include "Display/Backlight.h"
#include "Display/DisplayConfiguration.h"
#include "Graphic/Gpu.h"
#include "Fonts.h"
#include "Console/conio.h"
#include "Remote/SocketIfMsd.h"
#include "Usb/Usb.h"
#include "Multitasking/Multitasking.h"
#include "Gsm/Gsm.h"
#include "Uart/Uart.h"
#include "Power/Power.h"
#include "Usb/Usb.h"

void main(void)
{
   CpuInit();
   SysInit();
   MultitaskingInit();
   BacklightInit();
   GInit();
   GSetFont( TAHOMA16);
   BacklightTest(BACKLIGHT_INTENSITY_MAX);
   PowerInit();
   UsbInit();
   GsmInit();
   
   SysDelay(1000);
   cputs("Power on, please wait\n");
   GFlush();
   SysDelay(1000);
   GsmPowerOn();
   SysDelay(20000);
   if(GsmReset()) {
      cputs("Reset OK\n");
   } else {
      cputs("Reset fail\n");
   }
   GFlush();

   forever {
      SysDelay(1000);
      GClear();
      GTextAt(0, 0);
      if(GsmPinReady()) {
         cputs("Pin ready\n");
      } else {
         cputs("Pin fail\n");
      }
      GFlush();
      SysDelay(3000);
      if(GsmRegistered()) {
         cputs("Registered\n");
      } else {
         cputs("Not registered\n");
      }
      GFlush();
   }
}

void SysTimerExecute( void) {
   UartTimer();
}