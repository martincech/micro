#include "Cpu/Cpu.h"
#include "System/System.h"
#include "Graphic/Graphic.h"
#include "Display/Backlight.h"
#include "Display/DisplayConfiguration.h"
#include "Graphic/Gpu.h"
#include "Fonts.h"
#include "Console/conio.h"
#include "Remote/SocketIfMsd.h"
#include "Usb/Usb.h"
#include "Multitasking/MUltitasking.h"
#include "Rtc/Rtc.h"

void main(void)
{
   CpuInit();
   SysInit();
   BacklightInit();
   GInit();
   GSetFont( TAHOMA16);
   BacklightTest(BACKLIGHT_INTENSITY_MAX);
   PowerPortInit();
   UsbInit();
   MultitaskingInit();
   RtcInit();

   forever {
      SysDelay(1000);
      GClear();
      GFlush();
      GTextAt(0, 0);
      cputs("Testing...\n");
      GFlush();
      if(SocketIfMsdTest()) {
         cputs("OK");
      } else {
         cputs("ERROR");
      }
      GFlush();
   }
}

void SysTimerExecute( void) {}