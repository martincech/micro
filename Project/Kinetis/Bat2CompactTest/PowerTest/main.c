#include "Cpu/Cpu.h"
#include "System/System.h"
#include "Graphic/Graphic.h"
#include "Display/Backlight.h"
#include "Display/DisplayConfiguration.h"
#include "Graphic/Gpu.h"
#include "Fonts.h"
#include "Console/conio.h"
#include "Power/Power.h"
#include "Usb/Usb.h"

void main(void)
{
   CpuInit();
   SysInit();
   BacklightInit();
   GInit();
   GSetFont( TAHOMA16);
   BacklightTest(BACKLIGHT_INTENSITY_MAX);
   PowerInit();
   UsbInit();

   UsbDevicePowerEnable();


   forever {
      SysDelay(1000);
      GClear();
      GFlush();
      GTextAt(0, 0);

      cputs("POWERGOOD: ");
      if(PowerGood()) {
         cputs("Y");
      } else {
         cputs("N");
      }
      cputch('\n');
      cputs("CHARGER_PG: ");
      if(Bq2425xPg()) {
         cputs("Y");
      } else {
         cputs("N");
      }
      cputch('\n');
      cputs("POWERBANK_STAT: ");
      if(PoverBankConnected()) {
         cputs("Y");
      } else {
         cputs("N");
      }
      cputch('\n');
      GFlush();
   }
}

void SysTimerExecute( void) {}