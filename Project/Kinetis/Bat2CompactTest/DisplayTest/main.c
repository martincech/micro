#include "Cpu/Cpu.h"
#include "System/System.h"
#include "Graphic/Graphic.h"
#include "Display/Backlight.h"
#include "Display/DisplayConfiguration.h"
#include "Graphic/Gpu.h"
#include "Fonts.h"
#include "Console/conio.h"

void main(void)
{
   CpuInit();
   SysInit();
   BacklightInit();
   GInit();
   GSetFont( TAHOMA16);
   BacklightTest(BACKLIGHT_INTENSITY_MAX);

   cprintf("Init");
   GFlush();
   SysDelay(1000);

   forever {
      GClear();
      GFlush();
      SysDelay(1000);
      GBox(0, 0, G_MAX_X, G_MAX_Y);
      GFlush();
      SysDelay(1000);
   }
}

void SysTimerExecute( void) {}