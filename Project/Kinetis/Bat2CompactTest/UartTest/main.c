#include "Cpu/Cpu.h"
#include "System/System.h"
#include "Display/Backlight.h"
#include "Display/DisplayConfiguration.h"
#include "Uart/Uart.h"
#include "Hardware.h"

#define UART_COUNT     6
#define BUFFER_SIZE  128

const int Uarts[UART_COUNT] = {UART_UART0, UART_UART1, UART_UART2, UART_UART3, UART_UART4, UART_UART5};              
byte Buffers[UART_COUNT][BUFFER_SIZE];
TYesNo Ready;

/*
   Echo na v�ech portech
*/

void main(void)
{
int Size;
   CpuInit();
   SysInit();
   BacklightInit();
   BacklightTest(BACKLIGHT_INTENSITY_MAX);

   for(int i = 0 ; i < UART_COUNT ; i++) {
      UartInit( Uarts[i]);
      UartSetup( Uarts[i], 19200, UART_8BIT);
      UartTimeoutSet( Uarts[i], 10, 50);
      UartSendDelaySet( Uarts[i], 0);
      UartModeSet( Uarts[i], UART_MODE_BINARY_MASTER);
      UartBufferSet( Uarts[i], Buffers[i], BUFFER_SIZE);
   }

   forever {
      for(int i = 0 ; i < UART_COUNT ; i++) {
         UartReceive( Uarts[i]);
      }

      forever {
         Ready = YES;
         for(int i = 0 ; i < UART_COUNT ; i++) {
            if(UartReceiveStatus( Uarts[i]) == UART_RECEIVE_ACTIVE) {
               Ready = NO;
               break;
            }
         }
         if(Ready) {
            break;
         }
      }

      for(int i = 0 ; i < UART_COUNT ; i++) {
         Size = UartReceiveSize( Uarts[i]);
         if( Size) {
            UartSend( Uarts[i], Buffers[i], Size);
         }
      }

      forever {
         Ready = YES;
         for(int i = 0 ; i < UART_COUNT ; i++) {
            if(UartSendStatus( Uarts[i]) == UART_SEND_ACTIVE) {
               Ready = NO;
               break;
            }
         }
         if(Ready) {
            break;
         }
      }

   }
}

void SysTimerExecute( void) {
   UartTimer();
}