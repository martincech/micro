#include "Cpu/Cpu.h"
#include "System/System.h"
#include "Max17047/Max17047.h"
#include "Graphic/Graphic.h"
#include "Display/Backlight.h"
#include "Display/DisplayConfiguration.h"
#include "Graphic/Gpu.h"
#include "Fonts.h"
#include "Console/conio.h"

void main(void)
{
word Data;
   CpuInit();
   SysInit();
   BacklightInit();
   Max17047Init();
   GInit();
   GSetFont( TAHOMA16);
   BacklightTest(BACKLIGHT_INTENSITY_MAX);

   cprintf("Max17047 init");
   GFlush();
   
   forever {
      SysDelay(1000);
      GClear();
      GFlush();
      SysDelay(50);
      GTextAt(0, 0);
      
      if(!Max17047Read( Max17047AddressOf(Current), &Data)) {
         cprintf("Error reading");
         GFlush();
         continue;
      }

      cprintf("Current: %04x\n", Data);

      if(!Max17047Read( Max17047AddressOf(VCELL), &Data)) {
         cprintf("Error reading");
         GFlush();
         continue;
      }

      cprintf("Voltage: %04x\n", Data);

      if(!Max17047Read( Max17047AddressOf( Temperature), &Data)) {
         cprintf("Error reading");
         GFlush();
         continue;
      }

      cprintf("Temperature: %04x\n", Data);

      GFlush();

   }
}

void SysTimerExecute( void) {}