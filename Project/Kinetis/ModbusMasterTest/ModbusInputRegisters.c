//******************************************************************************
//
//   ModbusInputRegisters.c     Modbus process reply - Input registers commands
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "mb.h"

eMBErrorCode eMBRegInputCB ( USHORT *pucRegBuffer, USHORT usAddress,USHORT usNRegs) 	
{
   // currently same as holding registers
   return eMBRegHoldingCB(pucRegBuffer, usAddress, usNRegs, MB_REG_READ);
}
