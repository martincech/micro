//*****************************************************************************
//
//    Hardware.h   Default hardware definitions
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "Cpu/uMK70F12.h"
#include "Cpu/Io.h"
#include "Unisys/Uni.h"
#include "Cpu/Cpu.h"

#define printf (void)

//#define WATCHDOG_ENABLE
#define WATCHDOG_PERIOD    2000
#define SYSTEM_RTC
#define SLEEP_ENABLE    1              // Enable power save

//------------------------------------------------------------------------------
//   CPU parameters
//------------------------------------------------------------------------------

#include "Clock/Pll.h"

#define CLK0_FREQ_HZ        50000000
#define CLK0_TYPE           CANNED_OSC

#define PLL0_PRDIV      5
#define PLL0_VDIV       24

static inline void ClockInit( void) {


   // p�i MCG_C4_DRST_DRS == 3 za chv�li hod� hardfault, ostatn� volby jdou
   /*MCG->C4 &= ~MCG_C4_DRST_DRS_MASK;
   MCG->C4 |= MCG_C4_DRST_DRS(1);
   while((MCG->C4 & MCG_C4_DRST_DRS_MASK) != MCG_C4_DRST_DRS(1));*/
   return;
	SIM->CLKDIV1 = ( 0
			| SIM_CLKDIV1_OUTDIV1(0)
			| SIM_CLKDIV1_OUTDIV2(1)
			| SIM_CLKDIV1_OUTDIV3(1)
			| SIM_CLKDIV1_OUTDIV4(5) );

	/* Initialize PLL0 */
	/* PLL0 will be the source for MCG CLKOUT so the core, system, FlexBus, and flash clocks are derived from it */ 
	pll_init(OSCINIT,   /* Initialize the oscillator circuit */
			OSC_0,     /* Use CLKIN0 as the input clock */
			CLK0_FREQ_HZ,  /* CLKIN0 frequency */
			LOW_POWER,     /* Set the oscillator for low power mode */
			CLK0_TYPE,     /* Crystal or canned oscillator clock input */
			PLL_0,         /* PLL to initialize, in this case PLL0 */
			PLL0_PRDIV,    /* PLL predivider value */
			PLL0_VDIV,     /* PLL multiplier */
			MCGOUT);       /* Use the output from this PLL as the MCGOUT */
}

#define F_CPU 21000000ull
#define F_SYSTEM 21000000ull
#define F_BUS 21000000ull

/*#define F_CPU 120000000ull
#define F_SYSTEM 120000000ull
#define F_BUS 15000000ull
*/
//------------------------------------------------------------------------------
//   USB
//------------------------------------------------------------------------------

/* USB Fractional Divider value for 120MHz input */
/** USB Clock = PLL0 x (FRAC +1) / (DIV+1)       */
/** USB Clock = 120MHz x (1+1) / (4+1) = 48 MHz    */
#define USB_FS_FRAC    SIM_CLKDIV2_USBFSFRAC_MASK
#define USB_FS_DIV     SIM_CLKDIV2_USBFSDIV(4)

//-----------------------------------------------------------------------------
// System timer
//-----------------------------------------------------------------------------

#define TIMER_PERIOD       1                      // System timer period [ms]
#define TIMER_PERIOD_SLEEP 1000
#define TIMER_FAST_PERIOD  50                     // System timer 'fast' tick period [ms]
#define TIMER_SLOW_PERIOD  500                    // System timer 'slow' tick period [ms]
#define TIMER_FLASH1       300                    // Flash 1 delay [ms]
#define TIMER_FLASH2       200                    // Flash 2 delay [ms]
#define TIMER_TIMEOUT      10                     // Inactivity timeout [s]

//-----------------------------------------------------------------------------
// Interrupt priority
//-----------------------------------------------------------------------------

#define UART0_PRIORITY       3
#define UART1_PRIORITY       3
#define UART2_PRIORITY       3
#define UART3_PRIORITY       3

//-----------------------------------------------------------------------------
// Keyboard / Events
//-----------------------------------------------------------------------------

#define _K_SYSTEM_BASE   0
#include "System/SystemKey.h"
#define _K_SLEEP_BASE          _K_SYSTEM_LAST
#include "Sleep/SleepKey.h"
//-----------------------------------------------------------------------------
// UART common
//-----------------------------------------------------------------------------

// conditional compilation :
#define UART_BINARY  1                      // enable binary mode
#define UART_ASCII   1                      // enable ASCII mode
#define UART_NATIVE  1                      // enable native mode

//-----------------------------------------------------------------------------
// UART0
//-----------------------------------------------------------------------------

#define Uart0PortInit()      

#define UART0_BAUD           19200           // baud rate
#define UART0_FORMAT         UART_8BIT      // default format
#define UART0_TIMEOUT        10             // intercharacter timeout [ms]

#define UART0_TX_ENABLE_REGISTER   &GpioRegisterSet( UART0_RTS_PORT)
#define UART0_TX_DISABLE_REGISTER  &GpioRegisterClr( UART0_RTS_PORT)
#define UART0_TX_ENABLE_MASK       GpioMask( UART0_RTS_PORT)

//-----------------------------------------------------------------------------
// UART1
//-----------------------------------------------------------------------------

#define Uart1PortInit()      

#define UART1_BAUD           19200           // baud rate
#define UART1_FORMAT         UART_8BIT      // default format
#define UART1_TIMEOUT        10             // intercharacter timeout [ms]
#define UART1_REPLY_TIMEOUT  3000           // reply timeout [ms]

#define UART1_TX_ENABLE_REGISTER    0
#define UART1_TX_DISABLE_REGISTER   0
#define UART1_TX_ENABLE_MASK        0

//-----------------------------------------------------------------------------
// UART2
//-----------------------------------------------------------------------------

#define Uart2PortInit()     PORTE->PCR[16] = PORT_PCR_MUX(0x3); \
                           PORTE->PCR[17] = PORT_PCR_MUX(0x3)

#define UART2_BAUD           19200           // baud rate
#define UART2_FORMAT         UART_8BIT      // default format
#define UART2_TIMEOUT        10             // intercharacter timeout [ms]

#define UART2_TX_ENABLE_REGISTER   0
#define UART2_TX_DISABLE_REGISTER  0
#define UART2_TX_ENABLE_MASK       0

//-----------------------------------------------------------------------------
// UART3
//-----------------------------------------------------------------------------

#define Uart3PortInit()      

#define UART3_BAUD           19200           // baud rate
#define UART3_FORMAT         UART_8BIT      // default format
#define UART3_TIMEOUT        10             // intercharacter timeout [ms]

//-----------------------------------------------------------------------------
// Console
//-----------------------------------------------------------------------------

#include "Uart/Uart.h"
#define CONSOLE_CHANNEL    UART_UART2
#define CONSOLE_BAUD       19200

//-----------------------------------------------------------------------------
// LLWU
//-----------------------------------------------------------------------------

#define LLWU_PINS    0x1000
#define LLWU_RISE    1
#define LLWU_FALL    2
#define LLWU_ANY     3
#define LLWU_PIN_DIRECTION  LLWU_RISE
#define LLWU_MODULES  1 // LPTMR
#define LLWU_PIN        KINETIS_PIN_PORTD00

#define PIN_INTERRUPT_NUMBER       PORTD_IRQn
#define PIN_INTERRUPT_HANDLER       PORTD_IRQHandler

#define PinInterruptHandler()       void __irq PIN_INTERRUPT_HANDLER( void)
PinInterruptHandler();

static inline void PinInit( void)  {
   PinFunction(LLWU_PIN, KINETIS_GPIO_FUNCTION);
   PinPullup( LLWU_PIN);
   GpioInput(LLWU_PIN); 
   EintEnable(LLWU_PIN, EINT_SENSE_FALLING_EDGE);
   CpuIrqAttach(PIN_INTERRUPT_NUMBER, 2, PIN_INTERRUPT_HANDLER);
   CpuIrqEnable(PIN_INTERRUPT_NUMBER);
}

static inline void PinFallingIntEnable( void)  {
   EintDisable(LLWU_PIN);
   EintClearFlag(LLWU_PIN);
   EintEnable(LLWU_PIN, EINT_SENSE_FALLING_EDGE);
}

static inline void PinLowLevelIntEnable( void)  {
   EintDisable(LLWU_PIN);
   EintClearFlag(LLWU_PIN);
   EintEnable(LLWU_PIN, EINT_SENSE_LOW_LEVEL);
}

#define PinIntClear()      EintClearFlag(LLWU_PIN)
#define PinAsserted()      (!GpioGet(LLWU_PIN));
//-----------------------------------------------------------------------------
// LED
//-----------------------------------------------------------------------------

#define LED_ORANGE         KINETIS_PIN_PORTA11
#define LedOrangeInit()    PinFunction(LED_ORANGE, KINETIS_GPIO_FUNCTION); GpioOutput(LED_ORANGE)
#define LedOrangeOn()      GpioClr(LED_ORANGE)
#define LedOrangeToggle()  GpioToggle(LED_ORANGE)
#define LedOrangeOff()     GpioSet(LED_ORANGE)

#define LED_YELLOW         KINETIS_PIN_PORTA28
#define LedYellowInit()    PinFunction(LED_YELLOW, KINETIS_GPIO_FUNCTION); GpioOutput(LED_YELLOW)
#define LedYellowOn()      GpioClr(LED_YELLOW)
#define LedYellowToggle()  GpioToggle(LED_YELLOW)
#define LedYellowOff()     GpioSet(LED_YELLOW)

#define LED_GREEN          KINETIS_PIN_PORTA29
#define LedGreenInit()     PinFunction(LED_GREEN, KINETIS_GPIO_FUNCTION); GpioOutput(LED_GREEN)
#define LedGreenOn()       GpioClr(LED_GREEN)
#define LedGreenToggle()   GpioToggle(LED_GREEN)
#define LedGreenOff()      GpioSet(LED_GREEN)

#define LED_BLUE           KINETIS_PIN_PORTA10
#define LedBlueInit()      PinFunction(LED_BLUE, KINETIS_GPIO_FUNCTION); GpioOutput(LED_BLUE)
#define LedBlueOn()        GpioClr(LED_BLUE)
#define LedBlueToggle()    GpioToggle(LED_BLUE)
#define LedBlueOff()       GpioSet(LED_BLUE)

//-----------------------------------------------------------------------------
// Button
//-----------------------------------------------------------------------------

#define BUTTON         KINETIS_PIN_PORTE26
#define ButtonInit()   PinFunction(BUTTON, KINETIS_GPIO_FUNCTION); PinPullup( BUTTON); GpioInput(BUTTON)
#define ButtonPushed() !GpioGet(BUTTON)

//-----------------------------------------------------------------------------
// Sleep
//-----------------------------------------------------------------------------

#define WAKE_UP_PERIOD        5 // seconds

// LLWU pin 12 - PTD0
#define LlwuAdcRdyInit()         (LLWU->PE4 = LLWU_PE4_WUPE12(2)) // falling edge
#define LlwuAdcRdyFlag()         (LLWU->F2 & LLWU_F2_WUF12_MASK)
#define LlwuAdcRdyFlagClear()    (LLWU->F2 |= LLWU_F2_WUF12_MASK)


#endif