//*****************************************************************************
//
//    Hardware.h   
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

//#include <stddef.h>           // macro offsetof

#include "Bat2Hardware.h"

/*static inline void ctl_handle_error(CTL_ERROR_CODE_t e) {
}*/

//------------------------------------------------------------------------------
//   CPU parameters
//------------------------------------------------------------------------------

#include "Clock/Pll.h"

static inline void ClockInit( void) {
	SIM->CLKDIV1 = ( 0
			| SIM_CLKDIV1_OUTDIV1(0)
			| SIM_CLKDIV1_OUTDIV2(0)
			| SIM_CLKDIV1_OUTDIV3(1)
			| SIM_CLKDIV1_OUTDIV4(1));

   // p�i MCG_C4_DRST_DRS == 3 za chv�li hod� hardfault, ostatn� volby jdou
   MCG->C4 &= ~MCG_C4_DRST_DRS_MASK;
   MCG->C4 |= MCG_C4_DRST_DRS(1);
   while((MCG->C4 & MCG_C4_DRST_DRS_MASK) != MCG_C4_DRST_DRS(1));



	/* Initialize PLL0 */
	/* PLL0 will be the source for MCG CLKOUT so the core, system, FlexBus, and flash clocks are derived from it */ 
	pll_init(OSCINIT,   // Initialize the oscillator circuit
			12000000,  // CLKIN0 frequency
			HIGH_GAIN,     // Set the oscillator for low power mode
			CRYSTAL,     // Crystal or canned oscillator clock input
			6,    // PLL predivider value
			24,     // PLL multiplier
			PLL_ONLY);       // Use the output from this PLL as the MCGOUT
}

#define F_USB     48000000ull
#define F_CPU     42000000ull
#define F_SYSTEM  42000000ull
#define F_BUS     42000000ull

//------------------------------------------------------------------------------
//   Board test
//------------------------------------------------------------------------------

#define TEST_MAIN_UART  UART_UART1     // main control UART for board tests
#define UCONIO_UART     TEST_MAIN_UART // UART conio destination

//------------------------------------------------------------------------------
//   System parameters
//------------------------------------------------------------------------------

#define WATCHDOG_PERIOD   3000         // Watchdog period [ms]
#define WATCHDOG_ENABLE
#define TIMER_PERIOD      1            // System timer period [ms]
#define TIMER_FAST_PERIOD 50           // System timer 'fast' tick period [ms]
#define TIMER_SLOW_PERIOD 500          // System timer 'slow' tick period [ms]
#define TIMER_FLASH1      300          // Flash 1 delay [ms]
#define TIMER_FLASH2      200          // Flash 2 delay [ms]
#define TIMER_TIMEOUT     25          // Inactivity timeout [s]

//#define WATCHDOG_ENABLE 1              // Enable watchdog
//#define SLEEP_ENABLE    1              // Enable power save

#define TIMER_SOUND       1            // Enable sound trigger
#define TIMER_KBD         1            // Enable keyboard trigger
#define TIMER_NO_TIMEOUT  1            // Disable inactivity timeout processing

// Interrupt priority :
#define SYS_TIMER_PRIORITY   10         // System timer IRQ
#define UART0_PRIORITY       11         // UART0 IRQ
#define UART1_PRIORITY       12         // UART1 IRQ
#define UART2_PRIORITY       13         // UART1 IRQ
#define UART3_PRIORITY       14         // UART1 IRQ

//-----------------------------------------------------------------------------
// Status LED
//-----------------------------------------------------------------------------

#define STATUS_LED0  0
#define STATUS_LED1  0

#define StatusLedInit()       //PinFunction(STATUS_LED0, KINETIS_GPIO_FUNCTION); \
                              //PinFunction(STATUS_LED1, KINETIS_GPIO_FUNCTION); \
                              //GpioOutput( STATUS_LED0); \
                              //GpioOutput( STATUS_LED1)

#define StatusLed0On()       //GpioClr( STATUS_LED0)
#define StatusLed0Off()      //GpioSet( STATUS_LED0)
#define StatusLed0Toggle()   //GpioToggle( STATUS_LED0)

#define StatusLed1On()       //GpioClr( STATUS_LED1)
#define StatusLed1Off()      //GpioSet( STATUS_LED1)
#define StatusLed1Toggle()   //GpioToggle( STATUS_LED1)

//------------------------------------------------------------------------------
// IADC parameters
//------------------------------------------------------------------------------

// ADC channels / used inputs :
#define IADC_AD3   3
//#define IADC_AD7   7

// ADC conversion prescaler :
#define IADC_PRESCALER  0x01

// ADC maximum value :
#define IADC_VALUE_MAX  0x3FF

// ADC reference voltage :
#define IADC_REFERENCE_VOLTAGE 3300      // [mV]

#define IADC_POWER_VOLTAGE  IADC_AD3
//#define IADC_AUX_VOLTAGE    IADC_AD7

// conversion averaging :
#define IADC_AVERAGE_SHIFT  2
#define IADC_AVERAGE_COUNT (1 << IADC_AVERAGE_SHIFT)

//-----------------------------------------------------------------------------
// UART common
//-----------------------------------------------------------------------------

// conditional compilation :
#define UART_BINARY  1                      // enable binary mode
#define UART_ASCII   1                      // enable ASCII mode
#define UART_NATIVE  1                      // enable native mode

//-----------------------------------------------------------------------------
// UART0
//-----------------------------------------------------------------------------

#define UART0_RTS_PORT       0
#define UART0_CTS_PORT       0

#define Uart0PortInit()       PinFunction( KINETIS_PIN_UART0_RX_PORTD06, KINETIS_PIN_UART0_RX_PORTD06_FUNCTION);\
                              PinFunction( KINETIS_PIN_UART0_TX_PORTD07, KINETIS_PIN_UART0_TX_PORTD07_FUNCTION)

#define UART0_BAUD           9600           // baud rate
#define UART0_FORMAT         UART_8BIT      // default format
#define UART0_TIMEOUT        10             // intercharacter timeout [ms]

#define UART0_TX_ENABLE_REGISTER   &GpioRegisterSet( UART0_RTS_PORT)
#define UART0_TX_DISABLE_REGISTER  &GpioRegisterClr( UART0_RTS_PORT)
#define UART0_TX_ENABLE_MASK       GpioMask( UART0_RTS_PORT)

//-----------------------------------------------------------------------------
// UART1
//-----------------------------------------------------------------------------

#define UART1_RTS_PORT       0
#define UART1_CTS_PORT       0

#define Uart1PortInit()

#define UART1_BAUD           9600           // baud rate
#define UART1_FORMAT         UART_8BIT      // default format
#define UART1_TIMEOUT        10             // intercharacter timeout [ms]
#define UART1_REPLY_TIMEOUT  3000           // reply timeout [ms]

#define UART1_TX_ENABLE_REGISTER     &GpioRegisterSet( UART2_RTS_PORT)
#define UART1_TX_DISABLE_REGISTER    &GpioRegisterClr( UART2_RTS_PORT)
#define UART1_TX_ENABLE_MASK         GpioMask( UART2_RTS_PORT)

//-----------------------------------------------------------------------------
// UART2
//-----------------------------------------------------------------------------

#define UART2_RTS_PORT       KINETIS_PIN_UART0_RTS_b_PORTD04

#define Uart2PortInit()       PinFunction( KINETIS_PIN_UART2_RX_PORTD02, KINETIS_PIN_UART2_RX_PORTD02_FUNCTION);\
                              PinFunction( KINETIS_PIN_UART2_TX_PORTD03, KINETIS_PIN_UART2_TX_PORTD03_FUNCTION);\
                              PinFunction( UART2_RTS_PORT, KINETIS_GPIO_FUNCTION)

#define UART2_BAUD           9600           // baud rate
#define UART2_FORMAT         UART_8BIT      // default format
#define UART2_TIMEOUT        10             // intercharacter timeout [ms]

#define UART2_TX_ENABLE_REGISTER   &GpioRegisterSet( UART2_RTS_PORT)
#define UART2_TX_DISABLE_REGISTER  &GpioRegisterClr( UART2_RTS_PORT)
#define UART2_TX_ENABLE_MASK       GpioMask( UART2_RTS_PORT)

//-----------------------------------------------------------------------------
// UART3
//-----------------------------------------------------------------------------

#define UART3_RTS_PORT       0
#define UART3_CTS_PORT       0

#define Uart3PortInit()      

#define UART3_BAUD           9600           // baud rate
#define UART3_FORMAT         UART_8BIT      // default format
#define UART3_TIMEOUT        10             // intercharacter timeout [ms]

#define UART3_TX_ENABLE_REGISTER   &GpioRegisterSet( UART3_RTS_PORT)
#define UART3_TX_DISABLE_REGISTER  &GpioRegisterClr( UART3_RTS_PORT)
#define UART3_TX_ENABLE_MASK       GpioMask( UART3_RTS_PORT)

//------------------------------------------------------------------------------
// Keyboard
//------------------------------------------------------------------------------

#define KBD_K0	    KINETIS_PIN_PORTE01
#define KBD_K1      KINETIS_PIN_PORTE02
#define KBD_K2      KINETIS_PIN_PORTE03

#define KbdPortInit()   PinFunction(KBD_K0, KINETIS_GPIO_FUNCTION); \
                        PinFunction(KBD_K1, KINETIS_GPIO_FUNCTION); \
                        PinFunction(KBD_K2, KINETIS_GPIO_FUNCTION); \
                        GpioInput( KBD_K0);  GpioInput( KBD_K1);  GpioInput( KBD_K2);\
                        PinPullup( KBD_K0);  PinPullup( KBD_K1);  PinPullup( KBD_K2);\
			GpioClr( KBD_K0);    GpioClr( KBD_K1);    GpioClr( KBD_K2)

#define KbdK0Set()      GpioOutput( KBD_K0)
#define KbdK0Release()  GpioInput( KBD_K0)
#define KbdK1Set()      GpioOutput( KBD_K1)
#define KbdK1Release()  GpioInput( KBD_K1)
#define KbdK2Set()      GpioOutput( KBD_K2)
#define KbdK2Release()  GpioInput( KBD_K2)

#define KbdK0()         (!GpioGet( KBD_K0))
#define KbdK1()         (!GpioGet( KBD_K1))
#define KbdK2()         (!GpioGet( KBD_K2))

//------------------------------------------------------------------------------

// keyboard/event codes :
#define _K_SYSTEM_BASE            1    // skip K_NULL
#include "System/SystemKey.h"          // system events
#define _K_KEYBOARD_BASE          _K_SYSTEM_LAST
#include "Kbd/KbxKey.h"                // keyboard and user events
#define _K_SLEEP_BASE             _K_KEY_LAST
#include "Sleep/SleepKey.h"
#define _K_SLEEP_BASE             _K_KEY_LAST
#include "Sleep/SleepKey.h"

// timing constants [ms] :
#define KBD_DEBOUNCE           20      // delay after first touch
#define KBD_AUTOREPEAT_START   300     // autorepeat delay
#define KBD_AUTOREPEAT_SPEED   200     // autorepeat repeat speed

#define SYSTEM_KEYBOARD        1       // service keyboard with default scheduler

//-----------------------------------------------------------------------------
//  I2C by software
//-----------------------------------------------------------------------------

#define IicPortInit() //GpioInput( RTC_SCL); GpioInput( RTC_SDA);\
                      //GpioClr(   RTC_SDA); GpioClr(   RTC_SCL);

#define IicSclSet()   //GpioInput(  RTC_SCL)
#define IicSclClr()   //GpioOutput( RTC_SCL)
#define IicSdaSet()   //GpioInput(  RTC_SDA)
#define IicSdaClr()   //GpioOutput( RTC_SDA)
#define IicSda()      0//GpioGet(    RTC_SDA)

#define IIC_WAIT        20             // I2C half period [us]
#define IIC_READ        1              // IicRead compilation
#define IIC_WRITE       1              // IicWrite compilation

//-----------------------------------------------------------------------------
//  Data Flash AT25DF641
//-----------------------------------------------------------------------------

// SPI1, NPCS0
#define SPI_FLASH_CHANNEL         SPI_SPI3

#define __AT25DF641__             1              // NOR Flash chip type
#define FLASH_TIMEOUT             500            // NOR Flash write (sector erase) timeout [ms]
#define FLASH_ERASE_TIMEOUT       120000         // NOR Flash chip erase timeout [ms]

//-----------------------------------------------------------------------------
// FRAM FM25V10
//-----------------------------------------------------------------------------

// SPI2, NPCS0
#define SPI_FRAM_CHANNEL          SPI_SPI5

#define FRAM_SIZE                (256 * 1024L)   // memory total capacity

// conditional compilation :
#define FRAM_PRESENT              1              // check for presence
#define FRAM_BYTE_READ            1              // read single byte
#define FRAM_BYTE_WRITE           1              // write single byte

//------------------------------------------------------------------------------
//  GSM module
//------------------------------------------------------------------------------

#define GSM_BAUD_RATE                      9600
#define UART_MODEM_CHANNEL                 UART_UART0
#define UART_MODEM_FORMAT                  UART_8BIT
#define UART_MODEM_INTERCHARACTER_TIMEOUT  50

// SMS expiration :
#define GsmSmsExpirationMins( m)   ((m) / 5)               // min 0  max 720 (0..143)
#define GsmSmsExpirationHours( h)  (((h) - 12) * 2 + 143)  // min 13 max 24  (144..167)
#define GsmSmsExpirationDays( d)   ((d) + 166)             // min 2  max 30  (168..196)
#define GsmSmsExpirationWeeks( w)  ((w) + 192)             // min 5  max 58  (197..255)
#define GsmSmsExpirationMax()      255                     // maximum expiration

#define GSM_SMS_EXPIRATION    GsmSmsExpirationDays( 4)     // requested expiration

#define GsmPowerOn()        PowerGsmOn()
#define GsmPowerOff()       PowerGsmOff()

//-----------------------------------------------------------------------------
// ADC filtering
//-----------------------------------------------------------------------------

#define FILTER_MAX_AVERAGING  50       // max. width of averaging window
#define FILTER_SLOW_RESTART    1       // slow stabilisation after restart

// Basic data types :
typedef byte TSamplesCount;            // samples counter

//-----------------------------------------------------------------------------
// Samples FIFO
//-----------------------------------------------------------------------------

#define FIFO_SIZE       1024            // samples FIFO capacity

//-----------------------------------------------------------------------------
// Diagnostic
//-----------------------------------------------------------------------------

#define DIAGNOSTIC_FRAME_COUNT  10    // Frame FIFO capacity

//-----------------------------------------------------------------------------
// Platform
//-----------------------------------------------------------------------------

#define OPTION_SIGMA_DELTA
#define OPTION_PICOSTRAIN
#define OPTION_BAT2_COMPACT

//-----------------------------------------------------------------------------
// Sleep
//-----------------------------------------------------------------------------

#define WAKE_UP_PERIOD        1 // seconds

#define ADC_WAKEUP_PIN        KINETIS_PIN_PORTA13

// LLWU pin 4 - PTA13
#define LlwuPinInit()         LLWU->PE2 = LLWU_PE2_WUPE4(2); LLWU->PE1 = LLWU_PE1_WUPE0(2) | LLWU_PE1_WUPE1(2) // falling edge
#define LlwuKeyboardFlag()        (LLWU->F1 & (LLWU_F1_WUF0_MASK | LLWU_F1_WUF1_MASK))
#define LlwuAdcFlag()             (LLWU->F1 & LLWU_F1_WUF4_MASK)
#define LlwuKeyboardFlagClear()   (LLWU->F1 = LLWU_F1_WUF0_MASK | LLWU_F1_WUF1_MASK)
#define LlwuAdcFlagClear()        (LLWU->F1 = LLWU_F1_WUF4_MASK)
//-----------------------------------------------------------------------------
// IIC0
//-----------------------------------------------------------------------------

#define IIC0_SDA_PIN             0//KINETIS_PIN_I2C0_SDA_PORTB01
#define IIC0_SDA_PIN_FUNCTION    0//KINETIS_PIN_I2C0_SDA_PORTB01_FUNCTION
#define IIC0_SCL_PIN             0//KINETIS_PIN_I2C0_SCL_PORTB00
#define IIC0_SCL_PIN_FUNCTION    0//KINETIS_PIN_I2C0_SCL_PORTB00_FUNCTION
#define IIC0_CLOCK               100000

//-----------------------------------------------------------------------------
// IIC1
//-----------------------------------------------------------------------------

#define IIC1_SDA_PIN             0//KINETIS_PIN_I2C1_SDA_PORTC11
#define IIC1_SDA_PIN_FUNCTION    0//KINETIS_PIN_I2C1_SDA_PORTC11_FUNCTION
#define IIC1_SCL_PIN             0//KINETIS_PIN_I2C1_SCL_PORTC10
#define IIC1_SCL_PIN_FUNCTION    0//KINETIS_PIN_I2C1_SCL_PORTC10_FUNCTION
#define IIC1_CLOCK               100000

//-----------------------------------------------------------------------------
// SPI0 module
//-----------------------------------------------------------------------------

#define SPI_M0_CLK_PIN            0//KINETIS_PIN_SPI0_SCK_PORTD01
#define SPI_M0_CLK_PIN_FUNCTION   0//KINETIS_PIN_SPI0_SCK_PORTD01_FUNCTION
#define SPI_M0_DIN_PIN            0//KINETIS_PIN_SPI0_SIN_PORTD03
#define SPI_M0_DIN_PIN_FUNCTION   0//KINETIS_PIN_SPI0_SIN_PORTD03_FUNCTION
#define SPI_M0_DOUT_PIN           0//0//KINETIS_PIN_SPI0_SOUT_PORTD02
#define SPI_M0_DOUT_PIN_FUNCTION  0//KINETIS_PIN_SPI0_SOUT_PORTD02_FUNCTION
#define SPI_M0_S0_CLOCK           1000000
#define SPI_M0_S1_CLOCK           1000000

//-----------------------------------------------------------------------------
// SPI1 module
//-----------------------------------------------------------------------------

#define SPI_M1_CLK_PIN            KINETIS_PIN_SPI1_SCK_PORTB11
#define SPI_M1_CLK_PIN_FUNCTION   KINETIS_PIN_SPI1_SCK_PORTB11_FUNCTION
#define SPI_M1_DIN_PIN            KINETIS_PIN_SPI1_SIN_PORTB17
#define SPI_M1_DIN_PIN_FUNCTION   KINETIS_PIN_SPI1_SIN_PORTB17_FUNCTION
#define SPI_M1_DOUT_PIN           KINETIS_PIN_SPI1_SOUT_PORTB16
#define SPI_M1_DOUT_PIN_FUNCTION  KINETIS_PIN_SPI1_SOUT_PORTB16_FUNCTION
#define SPI_M1_S0_CLOCK           100000
#define SPI_M1_S1_CLOCK           100000

//-----------------------------------------------------------------------------
// SPI2 module
//-----------------------------------------------------------------------------

#define SPI_M2_CLK_PIN            KINETIS_PIN_SPI2_SCK_PORTB21
#define SPI_M2_CLK_PIN_FUNCTION   KINETIS_PIN_SPI2_SCK_PORTB21_FUNCTION
#define SPI_M2_DIN_PIN            KINETIS_PIN_SPI2_SIN_PORTB23
#define SPI_M2_DIN_PIN_FUNCTION   KINETIS_PIN_SPI2_SIN_PORTB23_FUNCTION
#define SPI_M2_DOUT_PIN           KINETIS_PIN_SPI2_SOUT_PORTB22
#define SPI_M2_DOUT_PIN_FUNCTION  KINETIS_PIN_SPI2_SOUT_PORTB22_FUNCTION
#define SPI_M2_S0_CLOCK           100000
#define SPI_M2_S1_CLOCK           100000

#define SPI_CHANNELS_PER_SUBMODULE  1

#define SPI3_CS_PIN     KINETIS_PIN_PORTB10
#define SPI5_CS_PIN     KINETIS_PIN_PORTB20

//-----------------------------------------------------------------------------
// Platform Display
//-----------------------------------------------------------------------------

// chicken icon :
#define DisplayWeighingOn()
#define DisplayWeighingOff()

// accumulator icon :
#define DisplayChargerOn()
#define DisplayChargerOff()

// antenna icon :
#define DisplaySignalOn()
#define DisplaySignalOff()

// load icon :
#define DisplayCalibrationOn()
#define DisplayCalibrationOff()

// all icon off :
#define DisplayModeOff()

// red LED :
#define DisplayErrorOn()
#define DisplayErrorOff()

// green LED :
#define DisplayOkOn()
#define DisplayOkOff()

// blue LED :
#define DisplayCommunicationOn()
#define DisplayCommunicationOff()

#define DisplayStatusOff()

//-----------------------------------------------------------------------------
// AD7192
//-----------------------------------------------------------------------------

#define ADC_INTERRUPT_HANDLER       PORTA_IRQHandler
#define ADC_INTERRUPT_NUMBER        PORTA_IRQn
#define ADC_INTERRUPT_PRIORITY      5

#define AdcInterruptHandler()       void __irq ADC_INTERRUPT_HANDLER( void)
#define AdcInitInt()                CpuIrqAttach(ADC_INTERRUPT_NUMBER, ADC_INTERRUPT_PRIORITY, ADC_INTERRUPT_HANDLER); \
                                    CpuIrqEnable(ADC_INTERRUPT_NUMBER)
#define AdcClearInt()               EintClearFlag( ADC_DOUT_RDY_PIN)
#define AdcEnableInt()              EintEnable( ADC_DOUT_RDY_PIN, EINT_SENSE_FALLING_EDGE)
#define AdcDisableInt()             EintDisable( ADC_DOUT_RDY_PIN)

#define ADC_CS_PIN         KINETIS_PIN_PORTA14
#define ADC_SCLK_PIN       KINETIS_PIN_PORTA15
#define ADC_DIN_PIN        KINETIS_PIN_PORTA16
#define ADC_DOUT_RDY_PIN   KINETIS_PIN_PORTA13

#define AdcPortInit()      PinFunction(ADC_CS_PIN, KINETIS_GPIO_FUNCTION); \
                           PinFunction(ADC_SCLK_PIN, KINETIS_GPIO_FUNCTION); \
                           PinFunction(ADC_DIN_PIN, KINETIS_GPIO_FUNCTION); \
                           PinFunction(ADC_DOUT_RDY_PIN, KINETIS_GPIO_FUNCTION); \
                           GpioOutput(ADC_CS_PIN); \
                           GpioOutput(ADC_SCLK_PIN); \
                           GpioOutput(ADC_DIN_PIN); \
                           GpioInput(ADC_DOUT_RDY_PIN);

#define AdcSetCS()       GpioSet(ADC_CS_PIN)
#define AdcClrCS()       GpioClr(ADC_CS_PIN)
#define AdcSetSCLK()     GpioSet(ADC_SCLK_PIN)
#define AdcClrSCLK()     GpioClr(ADC_SCLK_PIN)
#define AdcSetDIN()      GpioSet(ADC_DIN_PIN)
#define AdcClrDIN()      GpioClr(ADC_DIN_PIN)

#define AdcGetDOUT()     GpioGet(ADC_DOUT_RDY_PIN)
#define AdcGetRDY()      !GpioGet(ADC_DOUT_RDY_PIN)

// Parameters :
#define ADC_CONVERSION_RATE   10      // default conversion rate [Hz]

//-----------------------------------------------------------------------------

#include "DisplayLayout.h"
#include "Country/Country.h"

//-----------------------------------------------------------------------------

//------------------------------------------------------------------------------
//   USB
//------------------------------------------------------------------------------

#define USB_PLL                  0
#define USB_CLOCK                F_PLL0
#define USB_INTERRUPT_NUMBER     USB0_IRQn
#define UsbFsClockStart()
#define UsbFsClockStop()

#endif
