/*
 * File:  start.h
 * Purpose:  Kinetis start up routines. 
 *
 * Notes:     
 */


// Function prototypes
void startC(void);
void cpu_identify(void);
void flash_identify(void);
