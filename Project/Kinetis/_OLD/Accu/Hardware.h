//*****************************************************************************
//
//    Hardware.h   Bat2 wired platform hardware descriptions
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

//#include "Cpu/uMKL02Z32VFM4.h"
#include "Cpu/uMKL25Z128.h"
#include "Cpu/Io.h"
#include "Unisys/Uni.h"

//-----------------------------------------------------------------------------
// CPU parameters
//-----------------------------------------------------------------------------

#define F_CPU      21000000L                      // Main clock by synthesizer [Hz]
#define F_SYSTEM   21000000L
#define F_BUS      21000000L

#define TIMER_PERIOD    10000

typedef enum {
   K_TIMER_SLOW,
   K_BUTTON,
   K_COM,
} ESystemKey;

//-----------------------------------------------------------------------------
// IIC slave
//-----------------------------------------------------------------------------

#define IIC_SLAVE_ADDRESS     0x55

/*#define IIC_SLAVE_SDA_PIN             KINETIS_PIN_I2C0_SDA_PORTB04
#define IIC_SLAVE_SDA_PIN_FUNCTION    KINETIS_PIN_I2C0_SDA_PORTB04_FUNCTION
#define IIC_SLAVE_SCL_PIN             KINETIS_PIN_I2C0_SCL_PORTB03
#define IIC_SLAVE_SCL_PIN_FUNCTION    KINETIS_PIN_I2C0_SCL_PORTB03_FUNCTION*/

#define IIC_SLAVE_SDA_PIN             KINETIS_PIN_I2C0_SDA_PORTB01
#define IIC_SLAVE_SDA_PIN_FUNCTION    KINETIS_PIN_I2C0_SDA_PORTB01_FUNCTION
#define IIC_SLAVE_SCL_PIN             KINETIS_PIN_I2C0_SCL_PORTB00
#define IIC_SLAVE_SCL_PIN_FUNCTION    KINETIS_PIN_I2C0_SCL_PORTB00_FUNCTION

//-----------------------------------------------------------------------------
// IIC SW master
//-----------------------------------------------------------------------------

/*#define IIC_SDA_PIN   KINETIS_PIN_PORTA10
#define IIC_SCL_PIN   KINETIS_PIN_PORTA12*/

#define IIC_SDA_PIN   KINETIS_PIN_PORTB03
#define IIC_SCL_PIN   KINETIS_PIN_PORTB02

#define IicPortInit() PinFunction(IIC_SDA_PIN, KINETIS_GPIO_FUNCTION); \
                      PinFunction(IIC_SCL_PIN, KINETIS_GPIO_FUNCTION); \
                      PinPullup(IIC_SDA_PIN); \
                      PinPullup(IIC_SCL_PIN); \
                      GpioClr(IIC_SDA_PIN); \
                      GpioClr(IIC_SCL_PIN)

#define IicSda()      GpioGet(IIC_SDA_PIN)
#define IicScl()      GpioGet(IIC_SCL_PIN)
#define IicSdaSet()   GpioInput(IIC_SDA_PIN)
#define IicSclSet()   GpioInput(IIC_SCL_PIN)
#define IicSdaClr()   GpioOutput(IIC_SDA_PIN)
#define IicSclClr()   GpioOutput(IIC_SCL_PIN)

#define IIC_WAIT     1

#define SysUDelay( t)  for(int i = 0 ; i < 1000 ; i++ ) {__NOP();}


#define MAX17047_IIC_CHANNEL        0


#endif
