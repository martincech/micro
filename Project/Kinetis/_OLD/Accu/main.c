//*****************************************************************************
//
//    main.c       Accu
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Hardware.h"
#include "Cpu/Cpu.h"
#include "Iic/IicSlave.h"
#include "Accu/Accu.h"
#include "Sleep/Sleep.h"
#include "System/SysTimer.h"


void main(void) {
   CpuInit();
   //SysTimerStart();
   AccuInit();
   SleepInit();
   IicSlaveInit();
   InterruptEnable();
   SleepEnable(YES);
   forever {
      SleepScheduler();
      /*SysUDelay(100000);
      SysUDelay(100000);
      SysUDelay(100000);
      SysUDelay(100000);
      SysUDelay(100000);*/
      //SleepEnable(NO);
      //IicSlaveExecute();
      /*if(!IicSlaveBusy()) {
         SleepEnable(YES); // p�echod do sleep modu musim bezpodminecne stihnout max. za dobu prenosu start bytu
      }*/
   }
}

void ClockInit( void) {

}


void SysTimerExecute( void) {

}