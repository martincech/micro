//*****************************************************************************
//
//    Fonts.c      Project fonts
//    Version 1.1  (c) VEIT Electronics
//
//*****************************************************************************

#include "Fonts.h"
#include "Graphic/Graphic.h"

#include "Tahoma16.c"

const TFontDescriptor const *Fonts[] = {
   /* Tahoma16      */   &FontTahoma16,
   /* last          */   0
};   

//------------------------------------------------------------------------------
//  Set font
//------------------------------------------------------------------------------

void SetFont( int FontNumber)
// Set font
{
   GSetFont( FontNumber);              // direct font
} // SetFont
