//*****************************************************************************
//
//    Font.h        Font services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Font_H__
   #define __Font_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif
   
// font types :

typedef enum {
   FONT_UNKNOWN,                       // unknown
   FONT_FIXED,                         // ASCII nonproportional
   FONT_PROPORTIONAL,                  // ASCII proportional
   FONT_NUMERIC,                       // number proportional
   FONT_SYMBOL                         // symbols
} TFontType;

// numeric font characters :

typedef enum {
   NCHAR_0,                            // numbers 0..9
   NCHAR_1,
   NCHAR_2,
   NCHAR_3,
   NCHAR_4,
   NCHAR_5,
   NCHAR_6,
   NCHAR_7,
   NCHAR_8,
   NCHAR_9,
   NCHAR_SPACE,                        // space
   NCHAR_MINUS,                        // minus signum
   NCHAR_DOT,                          // decimal dot
   NCHAR_COLON,                        // colon
   NCHAR_PLUS,                         // plus signum
   _NCHAR_COUNT
} TNumericCharacter;
   
// font descriptor :

typedef struct {
   byte  Type;                         // font type
   byte  Width;                        // pixel width (proportional - max. width)
   byte  Height;                       // pixel height
   byte  Offset;                       // first character code
   word  Size;                         // bytes per character
   byte  AvgWidth;                     // average width (for column conversion)
   byte  NumWidth;                     // numeric width (for numbers only)
   byte  HSpace;                       // additinal horizontal space between characters
   byte  VSpace;                       // additional vertical space between rows
   const byte *Array;                  // fonts array
} TFontDescriptor;

#endif
