//*****************************************************************************
//
//    main.c       Main
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "Hardware.h"
#include "System/System.h"
#include "File/Efs.h"
#include "FatFs/ff.h"
#include "File/diskio.h"
#include <stdio.h>

static void Test(void);

#define HELP      \
"--- HELP ---\n"  \
"Confirm by ENTER\n"  \
"DirectoryCreate\n"    \
"DirectoryExists\n"   \
"DirectoryRename\n"   \
"DirectoryChange\n"         \
"DirectoryDelete\n"          \
"\n"                 \
"FileCreate\n"\
"FileCreate\n"\
"FileExists\n"\
"FileOpen\n"\
"FileDelete\n"\
"FileClose\n"\
"FileWrite\n"\
"FileRead\n"\
"FileSeek\n"\
"\n"                 \
"Init\n"\
"Shutdown\n"\
"--- END HELP ---\n" 

static void PrintHelp( void) {
   printf(HELP);
}

#define BUFFER_LENGTH   1024

void main( void) {
byte Buffer[BUFFER_LENGTH];
   CpuInit();
   SysInit();
   RtcInit();
   UartInit(UART_UART2);
   UartSetup( UART_UART2, CONSOLE_BAUD, UART_8BIT);
   PowerPortInit();

   PrintHelp();

   printf("Attach Mass Storage Device\n");

   forever {
         if(kbhit()) {
            scanf("%s", Buffer);
            printf("\n");

// DirectoryCreate
            if(!strcmp(Buffer, "DirectoryCreate")) {
               printf("Path: ");
               scanf("%s", Buffer);
               if(!EfsDirectoryCreate(Buffer)) {
                  printf("Error");
               } else {
                  printf("--- OK ---");
               }
               printf("\n");
// DirectoryExists
            } else if(!strcmp(Buffer, "DirectoryExists")) {
               printf("Path: ");
               scanf("%s", Buffer);
               if(!EfsDirectoryExists(Buffer)) {
                  printf("Error or non existent");
               } else {
                  printf("Exists");
               }
               printf("\n");
// DirectoryRename
            } else  if(!strcmp(Buffer, "DirectoryRename")) {
               byte Path[1024];
               printf("Path: ");
               scanf("%s", Path);
               printf("New name: ");
               scanf("%s", Buffer);
               if(!EfsDirectoryRename(Path, Buffer)) {
                  printf("Error");
               } else {
                  printf("--- OK ---");
               }
               printf("\n");
// DirectoryChange
            } else if(!strcmp(Buffer, "DirectoryChange")) {
               printf("Path: ");
               scanf("%s", Buffer);
               if(!EfsDirectoryChange(Buffer)) {
                  printf("Error");
               } else {
                  printf("--- OK ---");
               }
               printf("\n");
// DirectoryDelete
            } else if(!strcmp(Buffer, "DirectoryDelete")) {
               printf("Path: ");
               scanf("%s", Buffer);
               if(!EfsDirectoryDelete(Buffer)) {
                  printf("Error");
               } else {
                  printf("--- OK ---");
               }
               printf("\n");
// FileCreate
            } else if(!strcmp(Buffer, "FileCreate")) {
               printf("Path: ");
               scanf("%s", Buffer);
               if(!EfsFileCreate(Buffer, 0)) {
                  printf("Error");
               } else {
                  printf("--- OK ---");
               }
               printf("\n");
// FileExists
            } else if(!strcmp(Buffer, "FileExists")) {
               printf("Path: ");
               scanf("%s", Buffer);
               if(!EfsFileExists(Buffer)) {
                  printf("Error or non existent");
               } else {
                  printf("Exists");
               }
               printf("\n");
// FileOpen
            } else if(!strcmp(Buffer, "FileOpen")) {
               printf("Path: ");
               scanf("%s", Buffer);
               if(!EfsFileOpen(Buffer)) {
                  printf("Error");
               } else {
                  printf("--- OK ---");
               }
               printf("\n");
// FileDelete
            } else if(!strcmp(Buffer, "FileDelete")) {
               printf("Path: ");
               scanf("%s", Buffer);
               if(!EfsFileDelete(Buffer)) {
                  printf("Error");
               } else {
                  printf("--- OK ---");
               }
               printf("\n");
// FileClose
            } else if(!strcmp(Buffer, "FileClose")) {
               EfsFileClose();
               printf("--- OK ---");
               printf("\n");
// FileWrite
            } else if(!strcmp(Buffer, "FileWrite")) {
               printf("Data (no whitespace characters, max %d bytes): ", BUFFER_LENGTH - 1);
               scanf("%s", Buffer);
               printf("\n");
               int Repeat = -1;
               while(Repeat == -1) {
                  printf("Repeat: ");
                  scanf("%d", &Repeat);
                  printf("\n");

                  if(Repeat < 1 || Repeat > 0xFFFFFFF) {
                     printf("<1; %d>\n", 0xFFFFFFF);
                     Repeat = -1;
                  }
               }
               printf("\n");
               TYesNo Result = YES;
               dword TimeBefore = SysClock();
               dword Bytes;
               while(Repeat-- && Result) {
                  Result = EfsFileWrite(Buffer, strlen(Buffer));
                  Bytes += strlen(Buffer);
               }
               if(!Result) {
                  printf("Error at repetition %d", Repeat + 1);
               } else {
                  dword TimeAfter = SysClock();
                  dword ElapsedTime = TimeAfter - TimeBefore;
                  dword Speed = Bytes / ElapsedTime;
                  printf("Elapsed time: %d s, speed: %d B/s\n", ElapsedTime, Speed);
                  printf("--- OK ---");
               }
               printf("\n");
// FileRead
            } else if(!strcmp(Buffer, "FileRead")) {
               int Num = -1;
               while(Num == -1) {
                  printf("Num bytes: ");
                  scanf("%d", &Num);
                  printf("\n");

                  if(Num < 0 || Num > BUFFER_LENGTH - 1) {
                     printf("<0; %d>\n", BUFFER_LENGTH - 1);
                     Num = -1;
                  }
               }
               int Repeat = -1;
               while(Repeat == -1) {
                  printf("Repeat: ");
                  scanf("%d", &Repeat);
                  printf("\n");

                  if(Repeat < 1 || Repeat > 0xFFFFFFF) {
                     printf("<1; %d>\n", 0xFFFFFFF);
                     Repeat = -1;
                  }
               }
               printf("Data:\n");
               dword BytesRead = 0;
               dword Read;
               dword TimeBefore = SysClock();
               while(Repeat--) {
                  Read = EfsFileRead(Buffer, Num);

                  Buffer[Read] = '\0';
                  puts(Buffer);
                  BytesRead += Read;

                  if(Read != Num) {
                     break;
                  }
               }
               dword TimeAfter = SysClock();
               dword ElapsedTime = TimeAfter - TimeBefore;
               dword Speed = BytesRead / ElapsedTime;
               printf("Elapsed time: %d s, speed: %d B/s\n", ElapsedTime, Speed);
               printf("\n");
               printf("Read %d bytes", BytesRead);
               printf("\n");
// FileSeek
            } else if(!strcmp(Buffer, "FileSeek")) {
               printf("Num: ");
               int32 Num;
               scanf("%d", &Num);
               printf("Whence [Start, End, Current]: ");
               scanf("%s", Buffer);
               printf("\n");

               TYesNo Result;
               if(!strcmp(Buffer, "Start")) {
                  Result = EfsFileSeek(Num, EFS_SEEK_START);
               } else if(!strcmp(Buffer, "Current")) {
                  Result = EfsFileSeek(Num, EFS_SEEK_CURRENT);
               } else if(!strcmp(Buffer, "End")) {
                  Result = EfsFileSeek(Num, EFS_SEEK_END);
               } else {
                  printf("Parameter error\n");
                  continue;
               }
               if(!Result) {
                  printf("Error");
               } else {
                  printf("--- OK ---");
               }
               printf("\n");
// Init
            }  else if(!strcmp(Buffer, "Init")) {
               if(!EfsInit()) {
                  printf("Error\n");
               } else {
                  printf("Initialised\n");
               }
               SIM->SOPT1 &= ~ SIM_SOPT1_USBREGEN_MASK;
// Shutdown
            } else if(!strcmp(Buffer, "Shutdown")) {
               EfsSwitchOff();
               printf("--- OK ---\n");
            } else {
               printf("Unknown command\n");
               PrintHelp();
            }
         }
      Poll();
   }
}

void SysTimerExecute( void) {

}

static void TestWrite( void) {
   #define BUFFER_SIZE  512
   #define REPEAT_NUM   2048
   #define TOTAL_BYTES     BUFFER_SIZE * REPEAT_NUM
   byte Buffer[BUFFER_SIZE] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012345689";
   dword BytesWritten;

   printf("Writing %d bytes\n", TOTAL_BYTES);
   printf("Write Test in progress...\n");

   EfsFileCreate("file.txt", 0);

   dword TimeBefore = SysClock();
   dword i;

   for(i = 0 ; i < REPEAT_NUM ; i++) {
      BytesWritten = EfsFileWrite(Buffer, BUFFER_SIZE);

      if(BytesWritten != BUFFER_SIZE) {
         printf("Error writing at buffer no. %d\n", i);
         EfsFileClose();
         return;
      }
   }

   dword TimeAfter = SysClock();
   dword ElapsedTime = TimeAfter - TimeBefore;
   dword Speed = TOTAL_BYTES / ElapsedTime;
   printf("Elapsed time: %d s, speed: %d B/s\n", ElapsedTime, Speed);
   EfsFileClose();
}



static void TestRead( void) {
   #define BUFFER_SIZE  512
   #define REPEAT_NUM   1024
   #define TOTAL_BYTES     BUFFER_SIZE * REPEAT_NUM
   byte Buffer[BUFFER_SIZE];
   dword BytesRead;

   printf("Reading %d bytes\n", TOTAL_BYTES);
   printf("Read Test in progress...\n");

   EfsFileOpen("file.txt");

   dword TimeBefore = SysClock();
   dword i;

   for(i = 0 ; i < REPEAT_NUM ; i++) {
      BytesRead = EfsFileRead(Buffer, BUFFER_SIZE);

      if(BytesRead != BUFFER_SIZE) {
         printf("Error reading at buffer no. %d\n", i);
         EfsFileClose();
         return;
      }
   }

   dword TimeAfter = SysClock();
   dword ElapsedTime = TimeAfter - TimeBefore;
   dword Speed = TOTAL_BYTES / ElapsedTime;
   printf("Elapsed time: %d s, speed: %d B/s\n", ElapsedTime, Speed);
   EfsFileClose();
}




static void FfResultGet(FRESULT Result, char *StringResult) {
   switch(Result) {
      case FR_OK:
         strcpy(StringResult, "OK");
         break;
      case FR_DISK_ERR:
         strcpy(StringResult, "DISK_ERR");
         break;
      case FR_INT_ERR:
         strcpy(StringResult, "INT_ERR");
         break;
      case FR_NOT_READY:
         strcpy(StringResult, "NOT_READY");
         break;
      case FR_NO_FILE:
         strcpy(StringResult, "NO_FILE");
         break;
      case FR_NO_PATH:
         strcpy(StringResult, "NO_PATH");
         break;
      case FR_INVALID_NAME:
         strcpy(StringResult, "INVALID_NAME");
         break;
      case FR_DENIED:
         strcpy(StringResult, "DENIED");
         break;
      case FR_EXIST:
         strcpy(StringResult, "EXIST");
         break;
      case FR_INVALID_OBJECT:
         strcpy(StringResult, "INVALID_OBJECT");
         break;
      case FR_WRITE_PROTECTED:
         strcpy(StringResult, "WRITE_PROTECTED");
         break;
      case FR_INVALID_DRIVE:
         strcpy(StringResult, "INVALID_DRIVE");
         break;
      case FR_NOT_ENABLED:
         strcpy(StringResult, "NOT_ENABLED");
         break;
      case FR_NO_FILESYSTEM:
         strcpy(StringResult, "NO_FILESYSTEM");
         break;
      case FR_MKFS_ABORTED:
         strcpy(StringResult, "MKFS_ABORTED");
         break;
      case FR_TIMEOUT:
         strcpy(StringResult, "TIMEOUT");
         break;
      case FR_LOCKED:
         strcpy(StringResult, "LOCKED");
         break;
      case FR_NOT_ENOUGH_CORE:
         strcpy(StringResult, "NOT_ENOUGH_CORE");
         break;
      case FR_TOO_MANY_OPEN_FILES:
         strcpy(StringResult, "TOO_MANY_OPEN_FILES");
         break;
      case FR_INVALID_PARAMETER:
         strcpy(StringResult, "INVALID_PARAMETER");
         break;
      default:
         strcpy(StringResult, "_UNKNOWN_");
         break;
   }
}
#define FF_RESULT_LENGTH  50
static void Format( void) {
   FRESULT Result;
   char ResultString[FF_RESULT_LENGTH + 1];

   Result = f_mkfs (0, 0, 0);
   FfResultGet(Result, ResultString);

   printf("Result %s\n", ResultString);
}

static void TestSwitchOff( void) {
   EfsSwitchOff();
   printf("Efs switched off, please initialise it again\n");
}


static void DiskIo( void) {
int Buffer[50];

   Buffer[0] = 0;
   disk_ioctl(0, UFI_READ_FORMAT_CAPACITY_CMD, Buffer);
   printf("UFI_READ_FORMAT_CAPACITY_CMD: %d\n", Buffer[0]);


   Buffer[0] = 0;
   disk_ioctl(0, UFI_INQUIRY_CMD, Buffer);
   printf("UFI_INQUIRY_CMD: %d\n", Buffer[0]);

   Buffer[0] = 0;
   disk_ioctl(0, REQUEST_SENSE_CMD, Buffer);
   printf("REQUEST_SENSE_CMD: %d\n", Buffer[0]);

   Buffer[0] = 0;
   disk_ioctl(0, GET_SECTOR_SIZE, Buffer);
   printf("GET_SECTOR_SIZE: %d\n", Buffer[0]);
   Buffer[0] = 0;
   disk_ioctl(0, GET_SECTOR_COUNT, Buffer);
   printf("GET_SECTOR_COUNT: %d\n", Buffer[0]);


}



void prvGetRegistersFromStack( uint32_t *pulFaultStackAddress )
{
/* These are volatile to try and prevent the compiler/linker optimising them
away as the variables never actually get used.  If the debugger won't show the
values of the variables, make them global my moving their declaration outside
of this function. */
volatile uint32_t r0;
volatile uint32_t r1;
volatile uint32_t r2;
volatile uint32_t r3;
volatile uint32_t r12;
volatile uint32_t lr; /* Link register. */
volatile uint32_t pc; /* Program counter. */
volatile uint32_t psr;/* Program status register. */

    r0 = pulFaultStackAddress[ 0 ];
    r1 = pulFaultStackAddress[ 1 ];
    r2 = pulFaultStackAddress[ 2 ];
    r3 = pulFaultStackAddress[ 3 ];

    r12 = pulFaultStackAddress[ 4 ];
    lr = pulFaultStackAddress[ 5 ];
    pc = pulFaultStackAddress[ 6 ];
    psr = pulFaultStackAddress[ 7 ];
}



void __abort NMI_Handler(void) {
   while(1);
}

void __abort HardFault_Handler(void) {
    __asm volatile
    (
        " tst lr, #4                                                \n"
        " ite eq                                                    \n"
        " mrseq r0, msp                                             \n"
        " mrsne r0, psp                                             \n"
        " ldr r1, [r0, #24]                                         \n"
        " ldr r2, handler2_address_const                            \n"
        " bx r2                                                     \n"
        " handler2_address_const: .word prvGetRegistersFromStack    \n"
    );
}

void __abort MemManage_Handler(void) {
   while(1);
}

void __abort BusFault_Handler(void) {
   while(1);
}

void __abort UsageFault_Handler(void) {
   while(1);
}
