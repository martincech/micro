#include "MainClass.h"
#include "Memory/FileRemote.h"
#include "Action/ActionRemote.h"

#include "Scheduler/WeighingPlanList.h"
#include "Message/ContactList.h"
#include "Archive/Archive.h"
#include "Storage/Sample.h"
#include "Config/Config.h"
#include "Curve/CorrectionList.h"
#include "Curve/CurveList.h"
#include "Predefined/PredefinedList.h"

MainClass::MainClass(QObject *parent) :
   QObject(parent)
{
}

void MainClass::connectClicked(TcpClient &rem)
{
   remote = new Bat2TcpRemote((Bat2TcpRemote &)rem);
   w = new MainWindow();
   w->setWindowTitle( "Bat2 Simulator");
   w->setAttribute(Qt::WA_QuitOnClose);
   // nonvolatile memory initialization :
   FileRemoteSetup( remote);
   ActionRemoteSetup( remote);
   w->show();
}
