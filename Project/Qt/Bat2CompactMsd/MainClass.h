#ifndef MAINCLASS_H
#define MAINCLASS_H

#include <QObject>
#include "Bat2/Remote/Bat2Remote.h"
#include "uSimulator/mainwindow.h"

class MainClass : public QObject
{
   Q_OBJECT
public:
   explicit MainClass(QObject *parent = 0);
   
signals:
   
public slots:
   void connectClicked(TcpClient &rem);

private :
   Bat2TcpRemote *remote;
   MainWindow *w;
};

#endif // MAINCLASS_H
