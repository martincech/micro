#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "fnet_user_config.h"
#include "Unisys/Uni.h"
#include <QtConcurrentRun>
#include <QTimer>
#include <QThread>
#include <QScrollBar>

#define SOCK_CONNECTED_TIMER (5000)

static byte _fnetStackHeap[FNET_CFG_HEAP_SIZE];

void MainWindow::initFnet()
{
struct fnet_init_params fnetInitParams;

   memset( _fnetStackHeap, 0xCC, FNET_CFG_HEAP_SIZE);
   fnetInitParams.netheap_ptr = _fnetStackHeap;
   fnetInitParams.netheap_size = FNET_CFG_HEAP_SIZE;
   // init stack
   if( fnet_init(&fnetInitParams) == FNET_ERR){
      showStatus("Fnet initalization failed");
   }else{
      showStatus("Fnet initalization successfull.");
   }
   activeSocket = SOCKET_INVALID;
   QtConcurrent::run(this, &MainWindow::polling);
   QtConcurrent::run(this, &MainWindow::readSocketData);
}

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{

   ui->setupUi(this);
   ui->logWindow->setWindowFlags(Qt::Widget);
   QString ip0_199 = "[0-1]?[0-9]?[0-9]";
   QString ip2xx_249 = "2[0-4][0-9]";
   QString ip250_255 = "25[0-5]";
   QString ipRange = "(?:[ ][0-9][0-9]|[ ][ ][0-9]|[0-9][ ][ ]|[0-9][0-9][ ]|"+ip0_199 + "|" + ip2xx_249 + "|" + ip250_255 + ")";
   // You may want to use QRegularExpression for new code with Qt 5, however.
   QRegExp ipRegex ("^" + ipRange
                    + "\\." + ipRange
                    + "\\." + ipRange
                    + "\\." + ipRange + "$");
   QRegExpValidator *ipValidator = new QRegExpValidator(ipRegex, this);
   ui->ipAddress->setValidator(ipValidator);
   ui->ipAddress->setInputMask("000.000.000.000; ");
   QIntValidator *qiv = new QIntValidator ( 1, 65535, this );
   ui->port->setValidator(qiv);
   ui->connectionList->setModel(&connectionIps);
   connect(ui->connectionList->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(on_connectedSocket_selectionChanged(QItemSelection,QItemSelection)));
   showStatus("");
   ui->statusBar->addPermanentWidget(&status);
   breakup = false;
   initFnet();
}

MainWindow::~MainWindow()
{
   endMutex.lock();
   breakup = true;
   waitEnd.wait(&endMutex);
   endMutex.unlock();

   readMutex.lock();
   waitRead.wakeAll();
   activeSocket = SOCKET_INVALID;
   readMutex.unlock();

   QHash<QString, SOCKET>::iterator i;
   for (i = connections.begin(); i != connections.end(); i++){
      closesocket(i.value());
   }

   fnet_release();
   qApp->processEvents();
   delete ui;
}

void MainWindow::polling()
{
#if FNET_CFG_DHCP
struct fnet_dhcp_params dhcpParams;

   dhcpParams.requested_ip_address.s_addr = 0;
   dhcpParams.requested_lease_time = 0; // dont't request any specified lease time
   fnet_dhcp_init( fnet_netif_get_default(), &dhcpParams);
#endif

   while(!breakup) {
      fnet_poll_services();
   }
#if FNET_CFG_DHCP
   fnet_dhcp_release();
#endif
   fnet_poll_services_release();

   endMutex.lock();
   waitEnd.wakeAll();
   endMutex.unlock();
}

void MainWindow::readSocketData()
{
unsigned long rcvnum;
int optlen = sizeof(rcvnum);

   while(!breakup){
      readMutex.lock();
      if(activeSocket != SOCKET_INVALID){
         if(getsockopt(activeSocket,  SOL_SOCKET, SO_RCVNUM, (char*)&rcvnum, &optlen) == FNET_OK){
            if(rcvnum != 0){
               char *buf = new char[rcvnum];
               rcvnum = recv(activeSocket, buf, rcvnum, 0);
               if( rcvnum == SOCKET_ERROR){
                  showStatus("Socket data recived ERROR");
                  ui->rxData->insertPlainText(":RCV ERROR:\n");
               } else {
                  ui->rxData->insertPlainText(QString(QByteArray(buf, rcvnum)));
               }
            }
         }
      }else{
         waitRead.wait(&readMutex);
      }
      readMutex.unlock();
   }
}

void MainWindow::showStatus(QString s)
{
   status.setText(s);
}

void MainWindow::connectionCreated(SOCKET s)
{
   connections.insert(ui->ipAddress->text(), s);
   QStringList conList;

   QHash<QString, SOCKET>::iterator i;
   for (i = connections.begin(); i != connections.end(); i++){
      conList << i.key();
   }
   connectionIps.setStringList(conList);
}


void MainWindow::on_ipAddress_textChanged(const QString &arg1)
{

   ui->connectButton->setEnabled(!arg1.isEmpty() && !connections.contains(ui->ipAddress->text())
                                 && ui->ipAddress->hasAcceptableInput() && ui->port->hasAcceptableInput());
}

void MainWindow::on_port_textChanged(const QString &arg1)
{
    on_ipAddress_textChanged(arg1);
}

void MainWindow::on_connectedSocket_selectionChanged(const QItemSelection &, const QItemSelection &)
{
   if(ui->connectionList->selectionModel()->selectedRows().isEmpty()){
      ui->txData->setEnabled(false);
      ui->sendButton->setEnabled(false);
      ui->disconnectButton->setEnabled(false);
//      ui->communication->setEnabled(false);
      return;
   }
   QModelIndex row = ui->connectionList->selectionModel()->selectedRows().first();
   QString ip = connectionIps.data(row, Qt::DisplayRole).toString();
   readMutex.lock();
   ui->rxData->clear();
   ui->txData->clear();
   activeSocket = connections[ip];
   waitRead.wakeAll();
   readMutex.unlock();
   ui->txData->setEnabled(true);
   ui->sendButton->setEnabled(true);
   ui->disconnectButton->setEnabled(true);
}

void MainWindow::on_connectButton_clicked()
{
   SOCKET s;
   ui->connection->setEnabled(false);
   struct sockaddr_in addr;
   s = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
   if(s == SOCKET_INVALID){
      showStatus("Creating socket failed");
      return;
   }
   addr.sin_family = AF_INET;
   addr.sin_port = FNET_HTONS(ui->port->text().toInt());
   QString ipText = ui->ipAddress->text();
   ipText.replace(' ', QString());
   QStringList ipList = ipText.split('.');
   addr.sin_addr.s_addr = FNET_IP4_ADDR_INIT(ipList[0].toInt(), ipList[1].toInt(), ipList[2].toInt(), ipList[3].toInt());

   if( ::connect(s, (sockaddr*)&addr, sizeof(struct sockaddr)) == SOCKET_ERROR){
      showStatus("Connecting socket failed");
      ::closesocket(s);
      ui->connection->setEnabled(true);
      return;
   }

   fnet_socket_state_t state;
   int length = sizeof(fnet_socket_state_t);
   int retVal = -1;

   showStatus("Connecting");
   do{
      retVal = ::getsockopt(s, SOL_SOCKET, SO_STATE, (char*)&state, &length);
      qApp->processEvents();
   } while((retVal == FNET_OK) && (state == SS_CONNECTING));

   if (state == SS_CONNECTED){

      showStatus("Connection successfull");
      connectionCreated(s);
   }
   else{
      showStatus("Connection failed");
      ::closesocket(s);
   }
   ui->connection->setEnabled(true);
}

void MainWindow::on_disconnectButton_clicked()
{
   if(activeSocket == SOCKET_INVALID){
      return;
   }

   readMutex.lock();
   if(::closesocket(activeSocket) == FNET_ERR){
      showStatus("socket not disconnected");
      return;
   }
   ui->rxData->clear();
   ui->txData->clear();
   QString ip;
   QHash<QString, SOCKET>::iterator i;
   for (i = connections.begin(); i != connections.end(); i++){
      if(i.value() == activeSocket){
         ip = i.key();
         break;
      }
   }

   if(!ip.isEmpty()){
      connections.remove(ip);
   }
   activeSocket = SOCKET_INVALID;
   waitRead.wakeAll();
   readMutex.unlock();

   QStringList conList;
   for (i = connections.begin(); i != connections.end(); i++){
      conList << i.key();
   }
   connectionIps.setStringList(conList);
   QItemSelection dumy;
   on_connectedSocket_selectionChanged(dumy, dumy);
   showStatus("socket disconnected");
}

void MainWindow::on_sendButton_clicked()
{
   QString data = ui->txData->toPlainText();
   send(activeSocket, (char*)data.toStdString().c_str(), data.length(), 0);
}
