//******************************************************************************
//
//   Samples.cpp   Weighing samples loader
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "Samples.h"
#include <QFile>
#include <QFileInfo>
#include <QStringList>
#include <QTextStream>

#define CSV_SEPARATOR      QChar( ',')
#define CSV_LINE_MAX       512       // maximum characters per line

#define SAMPLES_COUNT_MAX  (4 * 24 * 3600 * 10)

//------------------------------------------------------------------------------
//   Constructor
//------------------------------------------------------------------------------

Samples::Samples()
{
   _samples      = new TWeightSample[ SAMPLES_COUNT_MAX];
   _samplesCount = 0;
} // MainWindow

//------------------------------------------------------------------------------
//   Destructor
//------------------------------------------------------------------------------

Samples::~Samples()
{
   if( _samples){
      delete [] _samples;
   }
} // ~MainWindow

//------------------------------------------------------------------------------
//   Load
//------------------------------------------------------------------------------

bool Samples::load( QString fileName)
{
   _samplesCount = 0;                            // clear samples count
   // load file :
   QFile file( fileName);
   if( !file.open( QIODevice::ReadOnly | QIODevice::Text)){
      return( false);
   }
   QTextStream stream( &file);
   QString     line;
   QStringList items;
   while( !stream.atEnd()){
      if( _samplesCount >= SAMPLES_COUNT_MAX){
         break;                                  // stop reading at limit
      }
      line  = stream.readLine( CSV_LINE_MAX);
      items = line.split( CSV_SEPARATOR);
      if( items.count() == 0){
         continue;                               // empty line
      }
      if( items.count() < 2){
         return( false);                         // wrong line
      }
      if( items[ 0].isEmpty() || items[ 1].isEmpty()){
         return( false);                         // empty item
      }
      _samples[ _samplesCount].Timestamp = items[ 0].toLongLong();
      _samples[ _samplesCount].Weight    = items[ 1].toInt();
      _samplesCount++;
   }
   if( _samplesCount == 0){
      return( false);
   }
   return( true);
} // load

//------------------------------------------------------------------------------
//   LoadBinary
//------------------------------------------------------------------------------
/*
#include "Diagnostic/DiagnosticDef.h"
int BurstCount;
QDataStream stream;
static void ReadSampleSigmaDelta0(TWeightGauge *Weight, int64 *Timestamp);

bool Samples::loadBinary( QString fileName)
{
   _samplesCount = 0;                            // clear samples count
   // load file :
   QFile file( fileName);
   if( !file.open( QIODevice::ReadOnly)){
      return( false);
   }
   //QDataStream stream( &file);

   stream.setDevice(&file);

   TDiagnosticFileFormat FileContent;

   stream.readRawData((char *)&FileContent.Header, sizeof(FileContent.Header));

   switch(FileContent.Header.Version) {
      case DIAGNOSTICS_FILE_VERSION_0:
         switch(FileContent.Header.AdcType) {
            case ADC_TYPE_SIGMA_DELTA:
               stream.readRawData((char *)&FileContent.ScaleSettings.SigmaDelta.V0, sizeof(FileContent.ScaleSettings.SigmaDelta.V0));
               BurstCount = FileContent.ScaleSettings.SigmaDelta.V0.Diagnostics.BurstCount;
               break;
            case ADC_TYPE_PICOSTRAIN:
               stream.readRawData((char *)&FileContent.ScaleSettings.Picostrain.V0, sizeof(FileContent.ScaleSettings.Picostrain.V0));
               BurstCount = FileContent.ScaleSettings.Picostrain.V0.Diagnostics.BurstCount;
               break;
            default:
               return false;
         }
         break;

      default:
         return false;
   }

   while( !stream.atEnd()){
      if( _samplesCount >= SAMPLES_COUNT_MAX){
         break;                                  // stop reading at limit
      }

      ReadSampleSigmaDelta0(&_samples[ _samplesCount].Weight, &_samples[ _samplesCount].Timestamp);
      _samplesCount++;
   }
   if( _samplesCount == 0){
      return( false);
   }
   return( true);
} // load



static void ReadSampleSigmaDelta0(TWeightGauge *Weight, int64 *Timestamp) {
   static TPlatformDiagnosticFrameId CurrentFrameId = 0;
   static TPlatformDiagnosticFrameSigmaDelta Frame = {0};
   static int InFrameSamplePointer = BurstCount;
   TPlatformDiagnosticWeightSigmaDelta DiagnosticWeight;

   if(InFrameSamplePointer == BurstCount) {
      if(CurrentFrameId == Frame.FrameId) {
         stream.readRawData((char *)&Frame, PlatformDiagnosticFrameSizeSigmaDelta(BurstCount));
      }

      if(CurrentFrameId == 0) { // don't check frame continuity
         CurrentFrameId = Frame.FrameId;
      } else {
         CurrentFrameId++;
      }

      InFrameSamplePointer = 0;
   }

   if(CurrentFrameId != Frame.FrameId) {
      *Weight = 0;
      *Timestamp = -1;
   } else {
      *Weight = Frame.Weight[InFrameSamplePointer];
      *Timestamp = Frame.Time * 1000;
   }

   InFrameSamplePointer++;
}
*/
//------------------------------------------------------------------------------
//   Count
//------------------------------------------------------------------------------

int Samples::count()
{
   return( _samplesCount);
} // count

//------------------------------------------------------------------------------
//   Samples
//------------------------------------------------------------------------------

TWeightSample *Samples::samples()
{
   return( _samples);
} // samples
