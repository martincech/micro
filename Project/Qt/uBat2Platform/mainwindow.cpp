//******************************************************************************
//
//   MainWindow.cpp  Bat2Platform simulator window
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "Bat2Platform/RcsUart/Rcs.h"

#define UART_SAMPLING_TIME 100

//------------------------------------------------------------------------------
//   Constructor
//------------------------------------------------------------------------------

MainWindow::MainWindow(QWidget *parent) :
   QMainWindow(parent),
   ui(new Ui::MainWindow)
{
   ui->setupUi(this);
   _dump = new CrtDump( ui->crt);      // initialize CrtDump utility
   _dump->setMode( CrtDump::MIXED);    // show mixed dump
   _dump->setEnable( ui->actionDump->isChecked());
   // Simulator
   _PlatformSimulator = new PlatformSimulator();
   _PlatformSimulator->show();
   // activate server :
   _parameters = new UartParameters( this);
   _parameters->setPortName( "COM1");
   _parameters->setBaudRate( 9600);
   // create read timer :
   _timer = new QTimer( this);
   connect( _timer, SIGNAL( timeout()), this, SLOT( timerTimeout()));
   _timer->start( UART_SAMPLING_TIME);
   // initialize platform :
   RcsInit(_dump, _parameters);
} // MainWindow

//------------------------------------------------------------------------------
//   Destructor
//------------------------------------------------------------------------------

MainWindow::~MainWindow()
{
   delete ui;
   if( _dump){
      delete _dump;
   }
   if( _timer){
      delete _timer;
   }
   if( _parameters){
      delete _parameters;
   }
} // ~MainWindow

//------------------------------------------------------------------------------
//  Timer
//------------------------------------------------------------------------------

void MainWindow::timerTimeout()
{
   RcsExecute();
} // timerTimeout

//------------------------------------------------------------------------------
//   Dump
//------------------------------------------------------------------------------

void MainWindow::on_actionDump_triggered()
{
   _dump->setEnable( ui->actionDump->isChecked());
} // on_actionDump_triggered

//------------------------------------------------------------------------------
//   Com settings
//------------------------------------------------------------------------------

void MainWindow::on_actionCOM_settings_triggered()
{
   _parameters->setWindowModality(Qt::ApplicationModal);
   _parameters->execute();
   RcsInit(_dump, _parameters);
} // on_actionCOM_settings_triggered
