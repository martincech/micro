//*****************************************************************************
//
//    Hardware.h  Bat2 platform simulator definitions
//    Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "Unisys/Uni.h"

//-----------------------------------------------------------------------------
// Timers
//-----------------------------------------------------------------------------

#define TIMER_FAST_PERIOD 50                     // System timer 'fast' tick period [ms]
#define TIMER_SLOW_PERIOD 500                    // System timer 'slow' tick period [ms]
#define TIMER_FLASH1      200                    // Flash 1 delay [ms]
#define TIMER_FLASH2      500                    // Flash 2 delay [ms]
#define TIMER_TIMEOUT     10                     // Inactivity timeout [s]

//------------------------------------------------------------------------------
// UART0
//------------------------------------------------------------------------------

#define UART0_BAUD           9600               // baud rate
#define UART0_DOUBLE         1                  // double baudrate

#define UART0_SEND_DELAY             50         // Tx delay [ms]
#define UART0_INTERCHARACTER_TIMEOUT 50         // intercharacter timeout [ms]

#define UART0_RX_TIMEOUT             50         // poll Rx timeout

//-----------------------------------------------------------------------------
// Samples FIFO
//-----------------------------------------------------------------------------

#define FIFO_SIZE       16384            // samples FIFO capacity

//-----------------------------------------------------------------------------
// ADC filtering
//-----------------------------------------------------------------------------

#define FILTER_MAX_AVERAGING  50       // max. width of averaging window
#define FILTER_SLOW_RESTART    1       // slow stabilisation after restart

// Basic data types :
typedef byte TSamplesCount;            // samples counter

//******************************************************************************

#endif
