/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Fri 12. Jul 13:32:50 2013
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "Crt/crt.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionDump;
    QAction *actionCOM_settings;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    Crt *crt;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(639, 482);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/image/platform.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        actionDump = new QAction(MainWindow);
        actionDump->setObjectName(QString::fromUtf8("actionDump"));
        actionDump->setCheckable(true);
        actionDump->setChecked(true);
        actionCOM_settings = new QAction(MainWindow);
        actionCOM_settings->setObjectName(QString::fromUtf8("actionCOM_settings"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        crt = new Crt(centralWidget);
        crt->setObjectName(QString::fromUtf8("crt"));

        verticalLayout->addWidget(crt);

        MainWindow->setCentralWidget(centralWidget);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        mainToolBar->addSeparator();
        mainToolBar->addSeparator();
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionDump);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionCOM_settings);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Bat2 Platform Simulator", 0, QApplication::UnicodeUTF8));
        actionDump->setText(QApplication::translate("MainWindow", "Dump", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionDump->setToolTip(QApplication::translate("MainWindow", "Enable dump", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionCOM_settings->setText(QApplication::translate("MainWindow", "COM settings", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
