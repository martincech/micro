#-----------------------------------   --------------
#
# Project created by QtCreator 2014-01-29T14:42:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SMSpdu
TEMPLATE = app
QMAKE_CFLAGS_DEBUG     += -Wall
QMAKE_CFLAGS_RELEASE   += -Wall
QMAKE_CXXFLAGS_DEBUG   += -Wall
QMAKE_CXXFLAGS_RELEASE += -Wall

DEFINES += __GSM_SMS_STATISTICS__

INCLUDEPATH  = ../../../Library/Bat2/Compact
INCLUDEPATH += ../../../Library/Qt
INCLUDEPATH += ../../../Library/Qt/Bat2Platform
INCLUDEPATH += ../../../Library/Bat2
INCLUDEPATH += ../../../Library/Bat2Platform/Compact
INCLUDEPATH += ../../../Library/Bat2Platform
INCLUDEPATH += ../../../Library/Bat
INCLUDEPATH += ../../../Library/Device
INCLUDEPATH += ../../../Library/Device/Ethernet
INCLUDEPATH += ../../../Library/Device/Ethernet/dhcp
INCLUDEPATH += ../../../Library/Device/Ethernet/dns
INCLUDEPATH += ../../../Library/Device/Ethernet/serial
INCLUDEPATH += ../../../Library/Micro
INCLUDEPATH += ../../../Library/Win32

SOURCES += main.cpp\
        MainWindow.cpp \
    ../../../Library/Device/Gsm/Gsm.c \
    ../../../Library/Qt/System/SysClock.c \
    ../../../Library/Win32/Uart/UartModem.cpp \
    ../../../Library/Micro/Parse/uParse.c \
    ../../../Library/Win32/Uart/WinUart.cpp \
    ../../../Library/Win32/Utility/Performance.c \
    ../../../Library/Qt/Uart/uartparameters.cpp \
    ../../../Library/Bat2/Dummy/Multitasking.cpp \
    ../../../Library/Micro/Convert/uBcd.c \
    ../../../Library/Device/Gsm/GsmDefPrv.c \
    ../../../Library/Device/Memory/Mempool.c \
    ../../../Library/Device/Gsm/GsmChannel.c \
    ../../../Library/Qt/uSimulator/Power.cpp \
    ../../../Library/Qt/Model/ArrayModel.cpp \
    ../../../Library/Qt/Bat2/Remote/Threads.cpp
SOURCES += ../../../Library/Qt/Crt/crt.cpp
SOURCES += ../../../Library/Qt/Crt/crtdump.cpp
SOURCES += ../../../Library/Micro/Time/uClock.c
SOURCES += ../../../Library/Micro/Time/uDate.c
SOURCES += ../../../Library/Micro/Time/uTime.c
SOURCES += ../../../Library/Micro/Time/uDateTime.c
SOURCES += ../../../Library/Bat/Country/Country.c

HEADERS  += MainWindow.h \
    ../../../Library/Device/Gsm/GsmDef.h \
    ../../../Library/Device/Gsm/Gsm.h \
    ../../../Library/Device/Uart/UartModem.h \
    Hardware.h \
    ../../../Library/Device/Multitasking/Multitasking.h \
    ../../../Library/Bat/System/System.h \
    ../../../Library/Micro/Parse/uParse.h \
    ../../../Library/Win32/Uart/WinUart.h \
    ../../../Library/Win32/Utility/Performance.h \
    ../../../Library/Qt/Uart/UartParameters.h \
    ../../../Library/Micro/Convert/uBcd.h \
    ../../../Library/Device/Memory/mempool.h \
    ../../../Library/Device/Ethernet/fnet_comp.h \
    ../../../Library/Device/Gsm/GsmDefPrv.h \
    ../../../Library/Bat2/Message/MGsm.h \
    ../../../Library/Bat2/Power/Power.h \
    ../../../Library/Qt/Model/arraymodel.h \
    ../../../Library/Qt/Bat2/Remote/Threads.h \
    ../../../Library/Device/Gsm/GsmChannel.h
HEADERS += ../../../Library/Qt/Crt/crt.h
HEADERS += ../../../Library/Qt/Crt/crtdump.h
HEADERS += ../../../Library/Bat/Country/CountryDef.h
HEADERS += ../../../Library/Micro/Time/uClock.h
HEADERS += ../../../Library/Micro/Time/uDate.h
HEADERS += ../../../Library/Micro/Time/uTime.h
HEADERS += ../../../Library/Micro/Time/uDateTime.h
HEADERS += ../../../Library/Bat/Country/Country.h
HEADERS += ../../../Library/Bat/Country/CountrySet.h
HEADERS += ../../../Library/Bat2/Compact/Persistent.h

FORMS    += MainWindow.ui \
    ../../../Library/Qt/Uart/UartParameters.ui

RESOURCES += \
    Resource.qrc
