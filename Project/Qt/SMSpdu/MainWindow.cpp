#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "Uart/UartModem.h"
#include <QMessageBox>
#include <typeinfo>       // std::bad_cast
#include <QDebug>
#include "Gsm/Gsm.h"

#define STATUS_NCONN "Closed"
#define STATUS_CONN  "Opened"

extern TYesNo _GsmSmsSubmit(TSmsSubmit *request);
extern TYesNo _GsmSmsCommand( TSmsId *request);
extern TYesNo _AddressToPhone(byte *Address, char *PhoneNumber);
extern TYesNo _PhoneToAddress(char *PhoneNumber, byte *Address);
extern void _GsmToText( char *Text, int Size);
extern int _TextToGsm( char *Text);

MainWindow::MainWindow(QWidget *parent) :
   QMainWindow(parent),
   ui(new Ui::MainWindow)
{
   ui->setupUi(this);
   // uart setup
   ui->UartParams->setWindowFlags(Qt::Widget);
   ui->UartParams->setVisible(true);
   ui->UartParams->setPortName("COM43");
   ui->UartParams->setFlowControl(WinUart::FLOW_RTS_CTS);
   ui->UartParams->setBaudRate(115200);
   connect(ui->UartParams, SIGNAL(accepted()), this, SLOT(UartConnected()));
   connect(ui->UartParams, SIGNAL(rejected()), this, SLOT(UartDisconnected()));
   _uart = NULL;
   _status.setText(STATUS_NCONN);
   //status bar
   ui->statusBar->addPermanentWidget(&_status);
   ui->statusBar->addWidget(&_progress);
   _progress.setVisible(false);
   //log
   _log = new CrtDump( ui->log);
   _log->setMode( CrtDump::TEXT);
   _log->setEnableOffset(false);
   _log->setEnableHeader(false);
   //threads
   currentThread = NULL;
   smsChannelThread = NULL;
   //toolbar
   ui->actionStop->setIcon(qApp->style()->standardIcon(QStyle::SP_DialogCancelButton));
   //sms view
   for(int i = 0; i < ui->smsMemory->columnCount(); i++){
      ui->smsMemory->hideColumn(i);
   }
   ui->smsMemory->showColumn(0);
   //initialization
   disableItems();
   on_actionStop_triggered();
   on_radioButtonSmsSend_clicked();
   on_smsText_textChanged();
   // statistics timer
   statisticsTimer.setInterval(500);
   statisticsTimer.setSingleShot(false);
   connect(&statisticsTimer, SIGNAL(timeout()), this, SLOT(updateModemStatistics()));
}

MainWindow::~MainWindow()
{
   delete ui;
   if( workerThread.isRunning()){
      workerThread.terminate();
   }
   if(currentThread){
      delete(currentThread);
   }
   if(smsChannelThread){
      smsChannelWorker.terminate();
      smsChannelThread->deleteLater();
   }
   if( _uart){
      delete _uart;
   }
}

void MainWindow::on_registerStatus_clicked()
{
   if( ui->registerStatus->isChecked()){
      ui->registerStatus->setChecked(false);
   } else {
      ui->registerStatus->setChecked(true);
   }
}
void MainWindow::on_smsReadyStatus_clicked()
{
   if( ui->smsReadyStatus->isChecked()){
      ui->smsReadyStatus->setChecked(false);
   } else {
      ui->smsReadyStatus->setChecked(true);
   }
}

void MainWindow::on_smsChannelReadyStatus_clicked()
{
   if( ui->smsChannelReadyStatus->isChecked()){
      if( !smsChannelThread){
         smsChannelThread = new SmsChannelExecutive();
         smsChannelThread->moveToThread(&smsChannelWorker);
         connect(this, SIGNAL(doWork()), smsChannelThread, SLOT(doWork()));
         smsChannelWorker.start();
         while( !smsChannelWorker.isRunning()){}
         emit(doWork());
         ui->SmsOutputChannels->setModel(((SmsChannelExecutive*)smsChannelThread)->getOutputChannelModel());
         ui->SmsInputChannels->setModel(((SmsChannelExecutive*)smsChannelThread)->getInputChannelModel());
         ui->SmsActualChannels->setModel(((SmsChannelExecutive*)smsChannelThread)->getChannelsStatusModel());
         ui->SmsActualChannels->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
         statisticsTimer.start();
      }
      ui->smsStatistics->setEnabled(true);
      ui->radioButtonDataSend->setEnabled(true);
      ui->radioButtonDataSend->setChecked(true);
      on_radioButtonDataSend_clicked();
      ui->radioButtonSmsSend->setEnabled(false);
      ui->radioButtonSmsCommand->setEnabled(false);
      ui->memoryControls->setEnabled(false);
   } else {
      if(smsChannelThread){
         smsChannelWorker.terminate();
         while( smsChannelWorker.isRunning()){
            QThread::yieldCurrentThread();
         }
         smsChannelThread->deleteLater();
         smsChannelThread = NULL;
         statisticsTimer.stop();
      }
      ui->smsStatistics->setEnabled(false);
      ui->radioButtonDataSend->setEnabled(false);
      ui->radioButtonSmsSend->setEnabled(true);
      ui->radioButtonSmsSend->setChecked(true);
      on_radioButtonSmsSend_clicked();
      ui->radioButtonSmsCommand->setEnabled(true);
      ui->memoryControls->setEnabled(true);

   }
}
void MainWindow::enableItems()
{
   ui->buttonReset->setEnabled(true);
   ui->channelControls->setEnabled(true);
   ui->channelControlsInput->setEnabled(true);
   ui->channelControlsOutput->setEnabled(true);
}

void MainWindow::disableItems()
{
   ui->buttonReset->setEnabled(false);
   ui->readButton->setEnabled(false);
   ui->pduControls->setEnabled(false);
   ui->memoryControls->setEnabled(false);
}

void MainWindow::UartConnected()
{
   ui->UartParams->show();
   if( _uart){
      QMessageBox::warning(this, tr("Warning"),
                            tr("Allready connected"));
      return;
   }
   _uart = new WinUart();
   ui->UartParams->toUart(_uart);
   if( !(_uart->checkOpen())){
      QMessageBox::critical(this, tr("Error"),
                            tr("Can't connect"));
      delete( _uart);
      _uart = NULL;
      _status.setText(STATUS_NCONN);
      return;
   }
   UartModemSetup( _uart, _log);
   GsmInit();
   _status.setText(ui->UartParams->toString());
   enableItems();
}

void MainWindow::UartDisconnected()
{
   ui->UartParams->show();
   ui->smsChannelReadyStatus->setChecked(false);
   on_smsChannelReadyStatus_clicked();
   ui->smsChannelReadyStatus->setEnabled(false);
   if( _uart){
      on_actionStop_triggered();
      GsmDeinit();
      _uart->disconnect();
      delete _uart;
      _uart = NULL;
   }
   _status.setText(STATUS_NCONN);
   disableItems();
}

void MainWindow::startThread( Thread *th, const char* doneSlot)
{
   disconnect(this, SIGNAL(doWork()));
   currentThread = th;
   currentThread->moveToThread(&workerThread);
   connect(this, SIGNAL(doWork()), currentThread, SLOT(doWork()));
   connect(currentThread, SIGNAL(done()), this, doneSlot);
   connect(currentThread, SIGNAL(progressRange(int,int)), &_progress, SLOT(setRange(int,int)));
   connect(currentThread, SIGNAL(progress(int)), &_progress, SLOT(setValue(int)));
   workerThread.start();
   while(!workerThread.isRunning()){}
   emit(doWork());
}

void MainWindow::on_actionStop_triggered()
{
   if( currentThread){
      workerThread.terminate();
      delete(currentThread);
      currentThread = NULL;
   }
   ui->actionStop->setEnabled(false);
}

void MainWindow::on_buttonReset_clicked()
{

   if( !_uart){
      return;
   }
   ui->actionStop->setEnabled(true);
   _progress.setVisible(true);
   ui->buttonReset->setEnabled(false);
   startThread( new InitThread(), SLOT(initDone()));

}
void MainWindow::initDone(){
   try
   {
     InitThread *th = dynamic_cast<InitThread*>(currentThread);
     ui->operatorName->setText(th->getOperator());
     ui->signalStrength->setText(QString::number(th->getSignalStrength()));
     ui->registerStatus->setChecked(th->getRegistered());
     ui->smsReadyStatus->setChecked(th->getSmsServiceReady());
     if( th->getSmsServiceReady()){
        ui->readButton->setEnabled(true);
        ui->pduControls->setEnabled(true);
        ui->smsChannelReadyStatus->setEnabled(true);
        ui->smsChannelReadyStatus->setChecked(true);
        on_smsChannelReadyStatus_clicked();
     }else {
        ui->smsChannelReadyStatus->setEnabled(false);
        on_smsChannelReadyStatus_clicked();
     }
     _progress.setVisible(false);
     delete(th);
     currentThread = NULL;
     ui->actionStop->setEnabled(false);
     ui->buttonReset->setEnabled(true);
   }catch (std::bad_cast&){
   }
}

void MainWindow::on_lipsumButton_clicked()
{
   QString lipsum("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ac mattis nunc. Fusce id ipsum sed nunc pretium blandit nec ut mauris. Vestibulum ante ipsum primis in faucibus orci luctus et cras amet.");
   if(ui->radioButtonSmsSend->isChecked()){
      ui->smsText->clear();
      ui->smsText->appendPlainText(lipsum.left(160));
      ui->textLength->setNum(160);
   }else {
      ui->smsText->appendPlainText(lipsum.left(134));
      ui->textLength->setNum( ui->smsText->toPlainText().length());
   }
}

void MainWindow::on_readButton_clicked()
{
   if( !_uart){
      return;
   }
   ui->actionStop->setEnabled(true);
   _progress.setVisible(true);
   ui->readButton->setEnabled(false);
   startThread( new SmsReadThread(), SLOT(readDone()));
}

void MainWindow::readDone()
{
   try
   {
     SmsReadThread *th = dynamic_cast<SmsReadThread*>(currentThread);
     if(th->getStatus() == GSM_SMS_READ_OK){
        QTableWidgetItem *text = new QTableWidgetItem(th->getText());
        QTableWidgetItem *ph = new QTableWidgetItem(th->getPhone());
        QTableWidgetItem *ts = new QTableWidgetItem(th->getTimeStamp().toString(QString("dd/MM/yyyy hh:mm:ss")));
        int row;
        row = ui->smsMemory->rowCount();
        ui->smsMemory->insertRow(row);
        ui->smsMemory->setItem(row, 0, text);
        ui->smsMemory->setItem(row, 1, ph);
        ui->smsMemory->setItem(row, 2, ts);
        ui->smsMemory->resizeColumnToContents(0);
        ui->smsMemory->resizeRowToContents(row);
     }
     _progress.setVisible(false);
     delete(th);
     currentThread = NULL;
     ui->actionStop->setEnabled(false);
     ui->readButton->setEnabled(true);
   }catch (std::bad_cast&){
   }
}

void MainWindow::on_readStatusButton_clicked()
{
   if( !_uart){
      return;
   }
   ui->actionStop->setEnabled(true);
   _progress.setVisible(true);
   ui->readStatusButton->setEnabled(false);
   startThread( new StatusReadThread(), SLOT(statusReadDone()));
}

void MainWindow::statusReadDone()
{
   try
   {
     StatusReadThread *th = dynamic_cast<StatusReadThread*>(currentThread);
     if(th->getStatus() != "SMS_ST_UNKNOWN"){
        QTableWidgetItem *text   = new QTableWidgetItem(th->getText());
        QTableWidgetItem *ph     = new QTableWidgetItem(th->getPhone());
        QTableWidgetItem *ts     = new QTableWidgetItem(th->getTimeStamp().toString(QString("dd/MM/yyyy hh:mm:ss")));
        QTableWidgetItem *mr     = new QTableWidgetItem(QString::number(th->getMessageReference()));
        QTableWidgetItem *status = new QTableWidgetItem(th->getStatus());
        int row;
        row = ui->smsMemory->rowCount();
        ui->smsMemory->insertRow(row);
        ui->smsMemory->setItem(row, 0, text);
        ui->smsMemory->setItem(row, 1, ph);
        ui->smsMemory->setItem(row, 2, ts);
        ui->smsMemory->setItem(row, 3, mr);
        ui->smsMemory->setItem(row, 4, status);
        ui->smsMemory->resizeColumnToContents(0);
        ui->smsMemory->resizeRowToContents(row);
     }
     _progress.setVisible(false);
     delete th;
     currentThread = NULL;
     ui->actionStop->setEnabled(false);
     ui->readStatusButton->setEnabled(true);
   }catch (std::bad_cast&){
   }
}
void MainWindow::on_sendButton_clicked()
{
   if( !_uart){
      return;
   }
   ui->actionStop->setEnabled(true);
   if( ui->phoneNumber->text().isEmpty()){
      QMessageBox::warning(this, tr("Warning"),
                            tr("Phone number not inserted"));
      return;
   }
   if(ui->radioButtonSmsSend->isChecked()){
      _progress.setVisible(true);
      SmsSendThread *s = new SmsSendThread();
      s->setText(ui->smsText->toPlainText());
      s->setPhone(ui->phoneNumber->text());
      startThread( s, SLOT(sendDone()));
      ui->sendButton->setEnabled(false);
      return;
   }
   if(ui->radioButtonSmsCommand->isChecked()){
      if( ui->MR->text().isEmpty()){
         QMessageBox::warning(this, tr("Warning"),
                               tr("Empty MR field"));
         ui->sendButton->setEnabled(true);
         return;
      }
      _progress.setVisible(true);
      CommandSendThread *s = new CommandSendThread();
      s->setPhone(ui->phoneNumber->text());
      s->setMessageReference(ui->MR->text().toInt());
      s->setCommandType((ESmsCommandType)ui->smsCommandCB->currentIndex());
      startThread( s, SLOT(sendDone()));
      ui->sendButton->setEnabled(false);
      return;
   }
   if( ui->radioButtonDataSend->isChecked()){
      if( ui->smsText->toPlainText().isEmpty()){
         QMessageBox::warning(this, tr("Warning"),
                               tr("Empty text field"));
         return;
      }
      TSmsChannel channel
            = GsmSmsChannelSend( (char*)ui->phoneNumber->text().toStdString().c_str(),
                                 (byte*)ui->smsText->toPlainText().toStdString().c_str(),
                                 ui->smsText->toPlainText().length());
      if( channel == SMS_INVALID_CHANNEL){
         QMessageBox::warning(this, tr("ERROR"),
                               tr("Data send problem"));

      }else {
         if(smsChannelThread){
            ((SmsChannelExecutive*)smsChannelThread)->RegisterStatusCheck(channel);
         }
      }

      return;
   }
   QMessageBox::warning(this, tr("Warning"),
                         tr("Not implemented yet"));
}

void MainWindow::sendDone()
{
  _progress.setVisible(false);
  delete currentThread;
  currentThread = NULL;
  ui->actionStop->setEnabled(false);
  ui->sendButton->setEnabled(true);
}

void MainWindow::on_radioButtonSmsSend_clicked()
{
    ui->MR->setEnabled(false);
    ui->encoding->setCurrentIndex(0);
    ui->encoding->setEnabled(false);
    ui->smsText->setEnabled(true);
    QString text = ui->smsText->toPlainText();
    ui->smsText->clear();
    ui->smsText->insertPlainText(text.left(160));
    ui->smsCommandCB->setEnabled(false);
}

void MainWindow::on_radioButtonDataSend_clicked()
{
   ui->MR->setEnabled(false);
   ui->encoding->setEnabled(false);
   ui->encoding->setCurrentIndex(1);
   ui->smsText->setEnabled(true);
   ui->smsCommandCB->setEnabled(false);
}

void MainWindow::on_radioButtonSmsCommand_clicked()
{
   ui->MR->setEnabled(true);
   ui->encoding->setEnabled(false);
   ui->smsText->setEnabled(false);
   ui->smsCommandCB->setEnabled(true);
}

void MainWindow::on_smsText_textChanged()
{
    ui->textLength->setNum( ui->smsText->toPlainText().length());
}

void MainWindow::on_smsMemory_clicked(const QModelIndex &index)
{
   if(ui->smsMemory->item(index.row(), 1)){
      ui->smsSender->setText( ui->smsMemory->item(index.row(), 1)->text());
   }
   if(ui->smsMemory->item(index.row(), 2)){
      ui->smsTimeStamp->setText( ui->smsMemory->item(index.row(), 2)->text());
   }
   if(ui->smsMemory->item(index.row(), 3)){
     ui->smsMr->setText(ui->smsMemory->item(index.row(), 3)->text());
   }
   if(ui->smsMemory->item(index.row(), 4)){
      ui->smsStatus->setText(ui->smsMemory->item(index.row(), 4)->text());
   }
}

#include "Gsm/Gsm.h"
void MainWindow::updateModemStatistics()
{
UTime dt;

   ui->SmsBSendCount->setText(QString("%1").arg( GsmStatistics.SmsBSendCount));
   ui->SmsBLostCount->setText(QString("%1").arg( GsmStatistics.SmsBLostCount));
   ui->SmsBRcvCount->setText(QString("%1").arg( GsmStatistics.SmsBRcvCount));

   ui->SmsASendCount->setText(QString("%1").arg( GsmStatistics.SmsASendCount));
   ui->SmsALostCount->setText(QString("%1").arg( GsmStatistics.SmsALostCount));
   ui->SmsARcvCount->setText(QString("%1").arg( GsmStatistics.SmsARcvCount));

   ui->SmsDeliveryCount->setText(QString("%1").arg( GsmStatistics.SmsDeliveryCount));
   uTime(&dt, GsmStatistics.AverageDeliverTime);
   ui->AverageDeliverTime->setText(QTime(0,dt.Min, dt.Sec).toString("mm:ss"));
   ui->AverageDeliverCount->setText(QString("%1").arg( GsmStatistics.AverageDeliverCount));
}






