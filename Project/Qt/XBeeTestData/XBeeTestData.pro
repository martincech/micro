#-------------------------------------------------
#
# Project created by QtCreator 2015-03-19T09:44:47
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = XBeeTest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH  += ../../../Library/Device/xbee_ansic_library-master/include
INCLUDEPATH  += ../../../Library/Device/xbee_ansic_library-master/samples/common

DEFINES += XBEE_DEVICE_ENABLE_ATMODE
DEFINES += XBEE_DEVICE_VERBOSE
DEFINES -= UNICODE

SOURCES += \
    ../../../Library/Device/xbee_ansic_library-master/src/zigbee/zcl_basic.c \
    ../../../Library/Device/xbee_ansic_library-master/src/zigbee/zcl_client.c \
    ../../../Library/Device/xbee_ansic_library-master/src/zigbee/zcl_commissioning.c \
    ../../../Library/Device/xbee_ansic_library-master/src/zigbee/zcl_identify.c \
    ../../../Library/Device/xbee_ansic_library-master/src/zigbee/zcl_onoff.c \
    ../../../Library/Device/xbee_ansic_library-master/src/zigbee/zcl_time.c \
    ../../../Library/Device/xbee_ansic_library-master/src/zigbee/zcl_types.c \
    ../../../Library/Device/xbee_ansic_library-master/src/zigbee/zigbee_zcl.c \
    ../../../Library/Device/xbee_ansic_library-master/src/zigbee/zigbee_zdo.c \
    ../../../Library/Device/xbee_ansic_library-master/src/xbee/xbee_atcmd.c \
    ../../../Library/Device/xbee_ansic_library-master/src/xbee/xbee_atmode.c \
    ../../../Library/Device/xbee_ansic_library-master/src/xbee/xbee_cbuf.c \
    ../../../Library/Device/xbee_ansic_library-master/src/xbee/xbee_commissioning.c \
    ../../../Library/Device/xbee_ansic_library-master/src/xbee/xbee_device.c \
    ../../../Library/Device/xbee_ansic_library-master/src/xbee/xbee_discovery.c \
    ../../../Library/Device/xbee_ansic_library-master/src/xbee/xbee_ebl_file.c \
    ../../../Library/Device/xbee_ansic_library-master/src/xbee/xbee_firmware.c \
    ../../../Library/Device/xbee_ansic_library-master/src/xbee/xbee_io.c \
    ../../../Library/Device/xbee_ansic_library-master/src/xbee/xbee_ota_client.c \
    ../../../Library/Device/xbee_ansic_library-master/src/xbee/xbee_ota_server.c \
    ../../../Library/Device/xbee_ansic_library-master/src/xbee/xbee_reg_descr.c \
    ../../../Library/Device/xbee_ansic_library-master/src/xbee/xbee_route.c \
    ../../../Library/Device/xbee_ansic_library-master/src/xbee/xbee_sxa.c \
    ../../../Library/Device/xbee_ansic_library-master/src/xbee/xbee_time.c \
    ../../../Library/Device/xbee_ansic_library-master/src/xbee/xbee_transparent_serial.c \
    ../../../Library/Device/xbee_ansic_library-master/src/xbee/xbee_wpan.c \
    ../../../Library/Device/xbee_ansic_library-master/src/xbee/xbee_xmodem.c \
    ../../../Library/Device/xbee_ansic_library-master/src/wpan/wpan_aps.c \
    ../../../Library/Device/xbee_ansic_library-master/src/wpan/wpan_types.c \
    ../../../Library/Device/xbee_ansic_library-master/src/win32/xbee_platform_win32.c \
    ../../../Library/Device/xbee_ansic_library-master/src/win32/xbee_serial_win32.c \
    ../../../Library/Device/xbee_ansic_library-master/src/util/hexdump.c \
    ../../../Library/Device/xbee_ansic_library-master/src/util/hexstrtobyte.c \
    ../../../Library/Device/xbee_ansic_library-master/src/util/jslong.c \
    ../../../Library/Device/xbee_ansic_library-master/src/util/memcheck.c \
    ../../../Library/Device/xbee_ansic_library-master/src/util/swapbytes.c \
    ../../../Library/Device/xbee_ansic_library-master/src/util/swapcpy.c \
    ../../../Library/Device/xbee_ansic_library-master/src/util/xmodem_crc16.c \
    xbee.c \
    main.c \
    ../../../Library/Device/xbee_ansic_library-master/samples/common/_atinter.c \
    ../../../Library/Device/xbee_ansic_library-master/samples/common/_nodetable.c \
    ../../../Library/Device/xbee_ansic_library-master/samples/common/_sxa_select.c

HEADERS += \
    ../../../Library/Device/xbee_ansic_library-master/include/wpan/aps.h \
    ../../../Library/Device/xbee_ansic_library-master/include/wpan/types.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/atcmd.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/atmode.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/byteorder.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/cbuf.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/commissioning.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/device.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/discovery.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/ebl_file.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/firmware.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/io.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/jslong.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/jslong_glue.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/ota_client.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/ota_server.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/platform.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/platform_dos.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/platform_hcs08.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/platform_posix.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/platform_rabbit.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/platform_win32.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/reg_descr.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/route.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/serial.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/sxa.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/sxa_socket.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/time.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/transparent_serial.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/wpan.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/xmodem.h \
    ../../../Library/Device/xbee_ansic_library-master/include/xbee/xmodem_crc16.h \
    ../../../Library/Device/xbee_ansic_library-master/include/zigbee/zcl.h \
    ../../../Library/Device/xbee_ansic_library-master/include/zigbee/zcl_bacnet.h \
    ../../../Library/Device/xbee_ansic_library-master/include/zigbee/zcl_basic.h \
    ../../../Library/Device/xbee_ansic_library-master/include/zigbee/zcl_basic_attributes.h \
    ../../../Library/Device/xbee_ansic_library-master/include/zigbee/zcl_client.h \
    ../../../Library/Device/xbee_ansic_library-master/include/zigbee/zcl_commissioning.h \
    ../../../Library/Device/xbee_ansic_library-master/include/zigbee/zcl_identify.h \
    ../../../Library/Device/xbee_ansic_library-master/include/zigbee/zcl_onoff.h \
    ../../../Library/Device/xbee_ansic_library-master/include/zigbee/zcl_time.h \
    ../../../Library/Device/xbee_ansic_library-master/include/zigbee/zcl_types.h \
    ../../../Library/Device/xbee_ansic_library-master/include/zigbee/zcl64.h \
    ../../../Library/Device/xbee_ansic_library-master/include/zigbee/zdo.h \
    ../../../Library/Device/xbee_ansic_library-master/samples/common/_atinter.h \
    ../../../Library/Device/xbee_ansic_library-master/samples/common/_commission_client.h \
    ../../../Library/Device/xbee_ansic_library-master/samples/common/_commission_server.h \
    ../../../Library/Device/xbee_ansic_library-master/samples/common/_nodetable.h \
    ../../../Library/Device/xbee_ansic_library-master/samples/common/_pxbee_ota_update.h \
    ../../../Library/Device/xbee_ansic_library-master/samples/common/_sxa_select.h \
    ../../../Library/Device/xbee_ansic_library-master/samples/common/_xbee_term.h \
    ../../../Library/Device/xbee_ansic_library-master/samples/common/_zigbee_walker.h \
    ../../../Library/Device/xbee_ansic_library-master/samples/common/parse_serial_args.h \
    ../../../Library/Device/xbee_ansic_library-master/samples/win32/parse_serial_args.h

OTHER_FILES +=
