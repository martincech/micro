#include "xbee/platform.h"
#include "xbee/atcmd.h"
#include "xbee/device.h"
#include "xbee/ota_server.h"
#include "xbee/wpan.h"

#include "wpan/types.h"

#include "zigbee/zcl.h"
#include "zigbee/zcl_commissioning.h"

const zcl_comm_startup_param_t zcl_comm_default_sas = {};

void ZCL_FACTORY_RESET_FN( void)
{
}

const char *xbee_update_firmware_ota( const wpan_envelope_t FAR *envelope,
        void FAR *context)
{
    return NULL;
}
