//*****************************************************************************
//
//    Hardware.h      Hardware
//    Version 1.0     (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "Unisys/Uni.h"

#include "Usb/UsbHardware.h"

#define USB_POWER    500 // mA

#endif
