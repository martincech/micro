#include <QtCore/QCoreApplication>

extern "C" {
   #include "Unity/unity_fixture.h"
   void TestRunner( void);
}

int main(int argc, char *argv[])
{
   QCoreApplication a(argc, argv);

   UnityMain(argc, argv, TestRunner);

   return a.exec();
}
