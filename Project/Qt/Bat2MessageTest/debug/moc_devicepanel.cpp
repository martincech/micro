/****************************************************************************
** Meta object code from reading C++ file 'devicepanel.h'
**
** Created: Fri 9. Aug 12:30:36 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../Library/Qt/uSimulator/devicepanel.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'devicepanel.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DevicePanel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      20,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x0a,
      29,   12,   12,   12, 0x0a,
      46,   12,   12,   12, 0x0a,
      53,   12,   12,   12, 0x08,
      66,   12,   12,   12, 0x08,
      91,   12,   12,   12, 0x08,
     117,   12,   12,   12, 0x08,
     140,   12,   12,   12, 0x08,
     164,   12,   12,   12, 0x08,
     188,   12,   12,   12, 0x08,
     213,   12,   12,   12, 0x08,
     238,   12,   12,   12, 0x08,
     264,   12,   12,   12, 0x08,
     286,   12,   12,   12, 0x08,
     309,   12,   12,   12, 0x08,
     333,   12,   12,   12, 0x08,
     358,   12,   12,   12, 0x08,
     379,   12,   12,   12, 0x08,
     398,   12,   12,   12, 0x08,
     417,   12,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_DevicePanel[] = {
    "DevicePanel\0\0powerOnSwitch()\0"
    "powerOffSwitch()\0copy()\0powerStart()\0"
    "on_buttonEnter_pressed()\0"
    "on_buttonEnter_released()\0"
    "on_buttonEsc_pressed()\0on_buttonEsc_released()\0"
    "on_buttonLeft_pressed()\0"
    "on_buttonLeft_released()\0"
    "on_buttonRight_pressed()\0"
    "on_buttonRight_released()\0"
    "on_buttonUp_pressed()\0on_buttonUp_released()\0"
    "on_buttonDown_pressed()\0"
    "on_buttonDown_released()\0on_powerOn_clicked()\0"
    "timerFastTimeout()\0timerSlowTimeout()\0"
    "timerKeyboardTimeout()\0"
};

void DevicePanel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DevicePanel *_t = static_cast<DevicePanel *>(_o);
        switch (_id) {
        case 0: _t->powerOnSwitch(); break;
        case 1: _t->powerOffSwitch(); break;
        case 2: _t->copy(); break;
        case 3: _t->powerStart(); break;
        case 4: _t->on_buttonEnter_pressed(); break;
        case 5: _t->on_buttonEnter_released(); break;
        case 6: _t->on_buttonEsc_pressed(); break;
        case 7: _t->on_buttonEsc_released(); break;
        case 8: _t->on_buttonLeft_pressed(); break;
        case 9: _t->on_buttonLeft_released(); break;
        case 10: _t->on_buttonRight_pressed(); break;
        case 11: _t->on_buttonRight_released(); break;
        case 12: _t->on_buttonUp_pressed(); break;
        case 13: _t->on_buttonUp_released(); break;
        case 14: _t->on_buttonDown_pressed(); break;
        case 15: _t->on_buttonDown_released(); break;
        case 16: _t->on_powerOn_clicked(); break;
        case 17: _t->timerFastTimeout(); break;
        case 18: _t->timerSlowTimeout(); break;
        case 19: _t->timerKeyboardTimeout(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData DevicePanel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DevicePanel::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_DevicePanel,
      qt_meta_data_DevicePanel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DevicePanel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DevicePanel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DevicePanel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DevicePanel))
        return static_cast<void*>(const_cast< DevicePanel*>(this));
    return QWidget::qt_metacast(_clname);
}

int DevicePanel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
