#-------------------------------------------------
#
# Project created by QtCreator 2013-10-14T10:12:46
#
#-------------------------------------------------

QT       += core gui

TARGET = mGenerator
TEMPLATE = app

INCLUDEPATH  = ../../../Library/Qt           # zakladni vyvojove prostredi
INCLUDEPATH += ../../../Library/Micro        # obecne knihovny pro mikrokontrolery
INCLUDEPATH += ../../../Library/Win32        # utility pro OS nezavisle na vyvojovem prostredi

# kopírování souborů generátoru do výstupního adresáře
FROM = \"../../../Library/Bat/Template\"
TO= \"$${OUT_PWD}/Template\"
FROM ~= s,/,\\,g
TO ~= s,/,\\,g
copydata.commands = $(COPY_DIR) $${FROM} $${TO}
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata



SOURCES += main.cpp\
        mainwindow.cpp \
    ../../../Library/Qt/Model/arraymodel.cpp \
    ../../../Library/Qt/Parse/csv.cpp \
    ../../../Library/Qt/Parse/nameTransformation.cpp \
    ../../../Library/Qt/Parse/TextFile.cpp \
    ../../../Library/Qt/mGenerator/mGenerator.cpp \
    ../../../Library/Qt/mGenerator/ModbusRegParser.cpp \
    ../../../Library/Qt/uGenerator/uGenerator.cpp \
    ../../../Library/Qt/uGenerator/SourceTemplate.cpp \
    ../../../Library/Qt/uGenerator/EmbeddedParser.cpp \
    ../../../Library/Qt/uGenerator/DictionaryParser.cpp \
    ../../../Library/Qt/uGenerator/DataParser.cpp \
    ../../../Library/Qt/uGenerator/CodeStrings.cpp \
    ../../../Library/Qt/uGenerator/CodeSeparator.cpp \
    ../../../Library/Qt/uGenerator/CodeNumber.cpp \
    ../../../Library/Qt/uGenerator/CodeMenu.cpp \
    ../../../Library/Qt/uGenerator/CodeHeader.cpp \
    ../../../Library/Qt/uGenerator/CodeDefaults.cpp \
    ../../../Library/Win32/Parse/Enum.cpp \
    ../../../Library/Qt/mGenerator/CodeSource.cpp \
    ../../../Library/Qt/mGenerator/CodeGroup.cpp \
    ../../../Library/Qt/mGenerator/GeneratorDef.cpp



HEADERS  += mainwindow.h \
    ../../../Library/Qt/Model/arraymodel.h \
    ../../../Library/Qt/Parse/csv.h \
    ../../../Library/Qt/Parse/nameTransformation.h \
    ../../../Library/Qt/Parse/TextFile.h \
    ../../../Library/Qt/uGenerator/SourceTemplate.h \
    ../../../Library/Qt/mGenerator/ModbusRegParser.h \
    ../../../Library/Qt/mGenerator/GeneratorDef.h \
    ../../../Library/Qt/mGenerator/mGenerator.h \
    ../../../Library/Qt/uGenerator/uGenerator.h \
    ../../../Library/Qt/uGenerator/SourceTemplate.h \
    ../../../Library/Qt/uGenerator/GeneratorDef.h \
    ../../../Library/Qt/uGenerator/EmbeddedParser.h \
    ../../../Library/Qt/uGenerator/DictionaryParser.h \
    ../../../Library/Qt/uGenerator/DataParser.h \
    ../../../Library/Qt/uGenerator/CodeStrings.h \
    ../../../Library/Qt/uGenerator/CodeSeparator.h \
    ../../../Library/Qt/uGenerator/CodeNumber.h \
    ../../../Library/Qt/uGenerator/CodeMenu.h \
    ../../../Library/Qt/uGenerator/CodeHeader.h \
    ../../../Library/Qt/uGenerator/CodeDefaults.h \
    ../../../Library/Win32/Parse/Enum.h \
    ../../../Library/Qt/mGenerator/CodeSource.h \
    ../../../Library/Qt/mGenerator/CodeGroup.h


FORMS    += \
    mainwindow.ui

RESOURCES += \
    mGenerator.qrc
