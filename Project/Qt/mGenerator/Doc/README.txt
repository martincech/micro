gener�tor p��jm� na vstupu soubor .csv odd�lova� ,

Popis form�tu souboru v MB_Gen.xls

V�stupem je kompletn� modbus stack bez nejni��� fyzick� vrstvy, tu je pot�eba 
doimplementovat do ka�d�ho projektu a to tak, �e se implementuj� v�echny 
funkce ve zdrojov�m souboru Modbus.h, kter� gener�tor generuje:

void ModbusInit( void);
// Init according to modbus options

void ModbusDeinit( void);
// Deinit

TModbusStatus ModbusStatus( void);
// Get actual status

void ModbusListenStart( void);
// Start receiving

void ModbusListenStop( void);
// Stop receiving

word ModbusReceive( TModbusPduRequest **Pdu, TYesNo *IsBroadcast);
// Read incomming data from receive buffer, return NULL when no data avaible

TYesNo ModbusSend( void);
// Send reply

!!- toto slou�� jako buffer pro ukl�d�n� a sestavov�n� paketu, funkce ModbusSend ode�le pot� tento buffer !! 
TModbusPduReply *ModbusGetReplyBuffer();  
// Returns reply buffer, which should be set accordingly to previous received request and then send
