#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QFile>
#include <QDir>
#include <QDirIterator>
#include "Parse/TextFile.h"

#define DEFAULT_FILE_NAME "MB_Gen.csv"
#define DEFAULT_PATH      ".." + QString(QDir::separator()) \
                        + ".." + QString(QDir::separator()) \
                        + ".." + QString(QDir::separator()) \
                        + "Library" + QString(QDir::separator()) \
                        + "Bat2" + QString(QDir::separator()) \
                        + "Generator" + QString(QDir::separator()) \
                        + "ModbusGenerator" + QString(QDir::separator())
#define DEFAULT_FILE      DEFAULT_PATH + DEFAULT_FILE_NAME

#define ROW_HEIGHT 16                // pixels


// page indexes :
typedef enum {
   PAGE_INPUT_FILE,
   PAGE_REGISTERS,
   PAGE_GLOBAL_HEADERS,
   PAGE_GLOBAL_SOURCES,
   PAGE_GROUPS_HEADERS,
   PAGE_GROUPS_SOURCES,
   _PAGE_LAST
} EPageIndex;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QDir dir;
    ui->actionOpenDefault->setToolTip(QDir::cleanPath(dir.absoluteFilePath(DEFAULT_PATH + DEFAULT_FILE_NAME)));
    // generator :
    generator = new mGenerator();
    // input file table :
    csvRegisters = new Csv( REGISTER_COLUMNS_MAX);                            // create input file table
    ui->tableInputFile->setModel( csvRegisters->array());                     // set input file  table model
    ui->tableInputFile->verticalHeader()->setDefaultSectionSize( ROW_HEIGHT); // input file  table row height
    // register table
    ui->tableRegisters->setModel( new ArrayModel(1, 1));
    ui->tableRegisters->verticalHeader()->setDefaultSectionSize( ROW_HEIGHT);  // register table row height

    ui->actionPrevious->setEnabled( false);
    ui->actionNext->setEnabled( false);
}

MainWindow::~MainWindow()
{
    delete ui;
    if( csvRegisters){
       delete csvRegisters;
    }
}
void MainWindow::openFile( QString fName)
{
   this->fileName = fName;
   if( fileName.isNull()){
      return;
   }
   if( fileName.isNull()){
      return;
   }
   csvRegisters->array()->setRowsCount( 0);     // clear contents
   if( !csvRegisters->load( fileName)){
      QString msg;
      msg.sprintf( "Unable parse file '%s'", qPrintable( fileName));
      QMessageBox::warning( this, "Error", msg, QMessageBox::Ok);
      return;
   }

   ui->textGlobalH->clear();
   if( !generator->parse( csvRegisters->array())){
      QMessageBox::warning( this, "Error", "Unable generate files", QMessageBox::Ok);
      return;
   }

   pageRegistersIndex = 0;
   genFileGlobalIndex  = 0;
   genFileGlobalHValues = generator->globalHeaders();
   genFileGlobalHFileNames = generator->globalHeadersFileNames();
   genFileGlobalCValues = generator->globalSources();
   genFileGlobalCFileNames = generator->globalSourcesFileNames();
   genFileGroupIndex = 0;
   genFileGroupHValues = generator->groupHeaders();
   genFileGroupHFileNames = generator->groupHeadersFileNames();
   genFileGroupCValues = generator->groupSources();
   genFileGroupCFileNames = generator->groupSourcesFileNames();
   on_actionInputFile_triggered();

}

void MainWindow::on_actionOpen_triggered()
{
    QString name = QFileDialog::getOpenFileName( this, "Open file",
                                             QDir::currentPath(),
                                             "CSV files (*.csv);;All files (*.*)");
    openFile( name);
}

void MainWindow::on_actionInputFile_triggered()
{
   ui->pageSelector->setCurrentIndex( PAGE_INPUT_FILE);
   ui->actionPrevious->setEnabled( false);
   ui->actionNext->setEnabled( false);
}

void MainWindow::on_actionRegisters_triggered()
{
    ui->pageSelector->setCurrentIndex( PAGE_REGISTERS);
    ui->actionPrevious->setEnabled( false);
    ui->actionNext->setEnabled( false);
    if( !generator->getRegParser()){
       return;
    }
    const ArrayModel *model = generator->getRegParser()->regGroupToArrayModel(pageRegistersIndex);
    if( model){
       delete ui->tableRegisters->selectionModel();
       ui->labelRegisters->setText(generator->getRegParser()->groupAt(pageRegistersIndex)->name
                                   + "(" + QString::number(generator->getRegParser()->groupAt(pageRegistersIndex)->item.count())
                                   + ")");
       ui->tableRegisters->setModel( (ArrayModel *)model);
       if( generator->getRegParser()->groupAt(pageRegistersIndex + 1)){
         ui->actionNext->setEnabled( true);
       }
    }
    if( pageRegistersIndex > 0){
       ui->actionPrevious->setEnabled( true);
    }

}

void MainWindow::on_actionGlobalHeaders_triggered()
{
   ui->pageSelector->setCurrentIndex( PAGE_GLOBAL_HEADERS);
   ui->actionPrevious->setEnabled( false);
   ui->actionNext->setEnabled( false);

   ui->textGlobalH->clear();
   if( genFileGlobalHValues.isEmpty()){
      return;
   }
   ui->textGlobalH->insertPlainText( genFileGlobalHValues[ genFileGlobalIndex]);
   ui->labelGlobalH->setText( genFileGlobalHFileNames[ genFileGlobalIndex]);
   if( genFileGlobalIndex + 1 < genFileGlobalHValues.count()){
      ui->actionNext->setEnabled( true);
   }
   if( genFileGlobalIndex > 0){
      ui->actionPrevious->setEnabled( true);
   }

}

void MainWindow::on_actionGlobalSources_triggered()
{
   ui->pageSelector->setCurrentIndex( PAGE_GLOBAL_SOURCES);
   ui->actionPrevious->setEnabled( false);
   ui->actionNext->setEnabled( false);

   ui->textGlobalC->clear();
   if( genFileGlobalCValues.isEmpty()){
      return;
   }
   ui->textGlobalC->insertPlainText( genFileGlobalCValues[ genFileGlobalIndex]);
   ui->labelGlobalC->setText( genFileGlobalCFileNames[ genFileGlobalIndex]);
   if( genFileGlobalIndex + 1 < genFileGlobalCValues.count()){
      ui->actionNext->setEnabled( true);
   }
   if( genFileGlobalIndex > 0){
      ui->actionPrevious->setEnabled( true);
   }
}

void MainWindow::on_actionRegisterGroupsH_triggered()
{
   ui->pageSelector->setCurrentIndex( PAGE_GROUPS_HEADERS);
   ui->actionPrevious->setEnabled( false);
   ui->actionNext->setEnabled( false);

   if( genFileGroupHValues.isEmpty()){
      return;
   }

   ui->textGroupsH->clear();
   ui->textGroupsH->insertPlainText( genFileGroupHValues[ genFileGroupIndex]);
   ui->labelGroupsH->setText( genFileGroupHFileNames[ genFileGroupIndex]);
   if( genFileGroupIndex + 1 < genFileGroupHValues.count()){
      ui->actionNext->setEnabled( true);
   }
   if( genFileGroupIndex > 0){
      ui->actionPrevious->setEnabled( true);
   }
}

void MainWindow::on_actionRegisterGroupsC_triggered()
{
   ui->pageSelector->setCurrentIndex( PAGE_GROUPS_SOURCES);
   ui->actionPrevious->setEnabled( false);
   ui->actionNext->setEnabled( false);

   if( genFileGroupCValues.isEmpty()){
      return;
   }

   ui->textGroupsC->clear();
   ui->textGroupsC->insertPlainText( genFileGroupCValues[ genFileGroupIndex]);
   ui->labelGroupsC->setText( genFileGroupCFileNames[ genFileGroupIndex]);
   if( genFileGroupIndex + 1 < genFileGroupCValues.count()){
      ui->actionNext->setEnabled( true);
   }
   if( genFileGroupIndex > 0){
      ui->actionPrevious->setEnabled( true);
   }
}


void MainWindow::on_actionPrevious_triggered()
{
   if( ui->pageSelector->currentIndex() == PAGE_REGISTERS){
      pageRegistersIndex--;
      on_actionRegisters_triggered();
      return;
   }

   if( ui->pageSelector->currentIndex() == PAGE_GLOBAL_HEADERS){
      genFileGlobalIndex--;
      on_actionGlobalHeaders_triggered();
      return;
   }
   if( ui->pageSelector->currentIndex() == PAGE_GLOBAL_SOURCES){
      genFileGlobalIndex--;
      on_actionGlobalSources_triggered();
      return;
   }
   if( ui->pageSelector->currentIndex() == PAGE_GROUPS_HEADERS){
      genFileGroupIndex--;
      on_actionRegisterGroupsH_triggered();
      return;
   }
   if( ui->pageSelector->currentIndex() == PAGE_GROUPS_SOURCES){
      genFileGroupIndex--;
      on_actionRegisterGroupsC_triggered();
      return;
   }
}

void MainWindow::on_actionNext_triggered()
{
   if( ui->pageSelector->currentIndex() == PAGE_REGISTERS){
      pageRegistersIndex++;
      on_actionRegisters_triggered();
   }
   if( ui->pageSelector->currentIndex() == PAGE_GLOBAL_HEADERS){
      genFileGlobalIndex++;
      on_actionGlobalHeaders_triggered();
      return;
   }
   if( ui->pageSelector->currentIndex() == PAGE_GLOBAL_SOURCES){
      genFileGlobalIndex++;
      on_actionGlobalSources_triggered();
      return;
   }
   if( ui->pageSelector->currentIndex() == PAGE_GROUPS_HEADERS){
      genFileGroupIndex++;
      on_actionRegisterGroupsH_triggered();
      return;
   }
   if( ui->pageSelector->currentIndex() == PAGE_GROUPS_SOURCES){
      genFileGroupIndex++;
      on_actionRegisterGroupsC_triggered();
      return;
   }
}


void MainWindow::on_actionSave_triggered()
{
   QFileInfo fileInfo;
   QString path;
   QDir dir;
   path = QFileDialog::getExistingDirectory(this, tr("Select output directory"), fileInfo.path(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
   if( path.isNull()){
      return;
   }
   TextFile  file;
   QString  fileName;
   // save global header files :
   for( int i = 0; i < genFileGlobalHValues.count(); i++){
      fileName = genFileGlobalHFileNames[ i];
      fileName = path + QString( QDir::separator()) + fileName;
      file.setFileName(fileName);
      file.save( genFileGlobalHValues[ i]);
   }
   // save global source files
   for( int i = 0; i < genFileGlobalCValues.count(); i++){
      fileName = genFileGlobalCFileNames[ i];
      fileName = path + QString( QDir::separator()) + fileName;
      file.setFileName(fileName);
      file.save( genFileGlobalCValues[ i]);
   }
   // save group source files
   for( int i = 0; i < genFileGroupCValues.count(); i++){
      fileName = genFileGroupCFileNames[ i];
      fileName = path + QString( QDir::separator()) + fileName;
      file.setFileName(fileName);
      file.save( genFileGroupCValues[ i]);
   }
   // save group header files :
   for( int i = 0; i < genFileGroupHValues.count(); i++){
      fileName = genFileGroupHFileNames[ i];
      fileName = path + QString( QDir::separator()) + fileName;
      file.setFileName(fileName);
      file.save( genFileGroupHValues[ i]);
   }

}

void MainWindow::on_actionOpenDefault_triggered()
{
   QFile file(DEFAULT_FILE);
   if( file.exists()){
      openFile( file.fileName());
   } else {
      on_actionOpen_triggered();
   }
}

void MainWindow::on_actionSave_Stack_triggered()
{
   QFileInfo fileInfo;
   QDir dir;
   QString path = QFileDialog::getExistingDirectory(this, tr("Select output directory"), fileInfo.path(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
   if( path.isNull()){
      return;
   }
   // save stack files
   QString folderPath = dir.absoluteFilePath(MODBUS_STACK_PATH);
   QDirIterator dirIt(folderPath, QDirIterator::Subdirectories);

   while (dirIt.hasNext()) {
      dirIt.next();
      fileInfo = QFileInfo(dirIt.filePath());
      if (fileInfo.isFile()){
         fileName = path + QString( QDir::separator()) + fileInfo.fileName();
         if(QFile::exists(fileName)){
            QFile::remove(fileName);
         }
         QFile::copy(fileInfo.absoluteFilePath(), fileName);
      }
   }
}
