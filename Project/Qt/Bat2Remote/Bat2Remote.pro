#-------------------------------------------------
#
# Project created by QtCreator 2013-11-15T11:19:01
#
#-------------------------------------------------

QT       += core gui network

TARGET = Bat2Remote
TEMPLATE = app

DEFINES += __GSM_SMS_STATISTICS__
DEFINES += OPTION_REMOTE

INCLUDEPATH  = ../../../Library/Bat2/Compact
INCLUDEPATH += ../../../Library/Bat2Platform
INCLUDEPATH += ../../../Library/Bat
INCLUDEPATH += ../../../Library/Qt
INCLUDEPATH += ../../../Library/Bat2
INCLUDEPATH += ../../../Library/Bat2Platform
INCLUDEPATH += ../../../Library/Micro
INCLUDEPATH += ../../../Library/Device
INCLUDEPATH += ../../../Library/Win32


SOURCES += main.cpp\
        MainWindow.cpp \
    ../../../Library/Qt/Socket/tcpclient.cpp \
    ../../../Library/Qt/Crt/crtdump.cpp \
    ../../../Library/Qt/Crt/crt.cpp \
    ../../../Library/Qt/Bat2/Remote/Bat2RemoteInterface.cpp \
    ../../../Library/Qt/Socket/TcpRawClient.cpp \
    ../../../Library/Win32/Uart/WinUart.cpp \
    ../../../Library/Device/Remote/Frame.c \
    ../../../Library/Qt/Uart/uartparameters.cpp \
    ../../../Library/Qt/Socket/tcpserver.cpp \
    ../../../Library/Bat2/Dummy/Multitasking.cpp \
    ../../../Library/Qt/Bat2/Remote/Threads.cpp \
    ../../../Library/Device/Gsm/GsmDefPrv.c \
    ../../../Library/Device/Gsm/GsmChannel.c \
    ../../../Library/Device/Gsm/Gsm.c \
    ../../../Library/Qt/System/SysClock.c \
    ../../../Library/Win32/Uart/UartModem.cpp \
    ../../../Library/Micro/Parse/uParse.c \
    ../../../Library/Win32/Utility/Performance.c \
    ../../../Library/Device/Memory/Mempool.c \
    ../../../Library/Qt/uSimulator/Power.cpp \
    ../../../Library/Qt/Model/ArrayModel.cpp \
    ../../../Library/Micro/Convert/uBcd.c \
    ../../../Library/Bat2/Remote/Cmd.c \
    ../../../Library/Qt/Remote/SocketIfEthernetQ.cpp \
    ../../../Library/Qt/Remote/Socket.cpp \
    ../../../Library/Qt/Remote/SocketIfUartQ.cpp
SOURCES += ../../../Library/Micro/Time/uClock.c
SOURCES += ../../../Library/Micro/Time/uDate.c
SOURCES += ../../../Library/Micro/Time/uTime.c
SOURCES += ../../../Library/Micro/Time/uDateTime.c
SOURCES += ../../../Library/Bat/Country/Country.c

HEADERS  += MainWindow.h \
    ../../../Library/Device/Multitasking/Multitasking.h \
    ../../../Library/Qt/Socket/tcpclient.h \
    ../../../Library/Qt/Crt/crtdump.h \
    ../../../Library/Qt/Crt/crt.h \
    ../../../Library/Bat2/Remote/RcDef.h \
    ../../../Library/Win32/Uart/WinUart.h \
    Hardware.h \
    ../../../Library/Device/Memory/NvmDef.h \
    ../../../Library/Bat/Device/VersionDef.h \
    ../../../Library/Bat2/Action/ActionDef.h \
    ../../../Library/Qt/Socket/TcpRawClient.h \
    ../../../Library/Device/Remote/Frame.h \
    ../../../Library/Qt/Bat2/Remote/Bat2RemoteInterface.h \
    ../../../Library/Qt/Uart/UartParameters.h \
    ../../../Library/Qt/Socket/tcpserver.h \
    ../../../Library/Qt/Bat2/Remote/Threads.h \
    ../../../Library/Device/Gsm/GsmDefPrv.h \
    ../../../Library/Device/Gsm/GsmDef.h \
    ../../../Library/Qt/Model/arraymodel.h \
    ../../../Library/Device/Gsm/Gsm.h \
    ../../../Library/Bat2/Remote/Cmd.h \
    ../../../Library/Qt/Remote/SocketIfEthernetQ.h \
    ../../../Library/Qt/Remote/SocketIf.h \
    ../../../Library/Qt/Remote/SocketDef.h \
    ../../../Library/Qt/Remote/SocketIfUartQ.h
HEADERS += ../../../Library/Bat/Country/CountryDef.h
HEADERS += ../../../Library/Micro/Time/uClock.h
HEADERS += ../../../Library/Micro/Time/uDate.h
HEADERS += ../../../Library/Micro/Time/uTime.h
HEADERS += ../../../Library/Micro/Time/uDateTime.h
HEADERS += ../../../Library/Bat/Country/Country.h
HEADERS += ../../../Library/Bat/Country/CountrySet.h
HEADERS += ../../../Library/Bat2/Compact/Persistent.h
HEADERS += ../../../Library/Bat2Platform/Platform/PlatformDef.h

FORMS    += MainWindow.ui \
    ../../../Library/Qt/Uart/UartParameters.ui
