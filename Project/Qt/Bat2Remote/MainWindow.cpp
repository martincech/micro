#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QInputDialog>
#include <QHostAddress>
#include <QMessageBox>
#include <QTime>
#include "Remote/SocketIfEthernetQ.h"
#include "Remote/SocketIfUartQ.h"

MainWindow::MainWindow(QWidget *parent) :
   QMainWindow(parent),
   ui(new Ui::MainWindow)
{
   ui->setupUi(this);
   dump = new CrtDump(ui->logger);
   dump->setMode( CrtDump::MIXED);
   pbar = new QProgressBar(ui->statusBar);
   pbar->setRange(0, 100);
   ui->statusBar->addPermanentWidget(pbar);
   bat2 = NULL;
   bat2Con = NULL;

   ui->UartParams->setWindowFlags(Qt::Widget);
   ui->UartParams->setVisible(true);
   connect(ui->UartParams, SIGNAL(accepted()), this, SLOT(UartConnect()));
   connect(ui->UartParams, SIGNAL(rejected()), this, SLOT(UartDisconnect()));
   ui->ModemParams->setWindowFlags(Qt::Widget);
   ui->ModemParams->setVisible(true);
   ui->ModemParams->setPortName("COM43");
   ui->ModemParams->setFlowControl(WinUart::FLOW_RTS_CTS);
   ui->ModemParams->setBaudRate(115200);
   connect(ui->ModemParams, SIGNAL(accepted()), this, SLOT(ModemConnect()));
   connect(ui->ModemParams, SIGNAL(rejected()), this, SLOT(ModemDisconnect()));
   on_RB_TCP_toggled(true);
   on_RB_Uart_toggled(false);
   on_RB_SMS_toggled(false);
   // Workaround - this two stays disabled from creator settings, so enable them
   ui->RB_GsmSend->setEnabled(true);
   ui->PB_GsmSend->setEnabled(true);
   on_SmsText_textChanged(ui->SmsText->text());
   // statistics timer
   statisticsTimer.setInterval(500);
   statisticsTimer.setSingleShot(false);
   connect(&statisticsTimer, SIGNAL(timeout()), this, SLOT(updateModemStatistics()));
   statisticsTimer.start();
   operationDone = true;
}

MainWindow::~MainWindow()
{
   delete bat2;
   delete bat2Con;
   delete dump;
   delete pbar;
   delete ui;
}

void MainWindow::on_ClearLogButton_clicked()
{
   ui->logger->clear();
}

//******************************************************************************
// Gsm send
//******************************************************************************

void MainWindow::on_RB_GsmSend_toggled(bool checked)
{
   ui->GsmSendParams->setVisible(checked);
   ui->GsmSendParams->setEnabled(checked);
   ui->GsmSendParams->setEnabled(checked);
   ui->PB_GsmSend->setEnabled(checked);
   ui->RB_GsmSend->setEnabled(true);
   ui->SmsText->setEnabled(true);
   ui->PhoneNumberText->setEnabled(false);
   ui->GsmSmsSlot_edit->setVisible(false);
   ui->GsmSmsSlot_label->setVisible(true);
}

void MainWindow::on_PB_GsmSend_clicked()
{
//   ui->GsmSmsSlot_label->setText(
//            QString::number(
//               bat2->GsmSend((char*)ui->SmsText->text().toStdString().c_str())));
//   ui->GsmSmsSlot_edit->setText(ui->GsmSmsSlot_label->text());
//   on_PB_GsmStatus_clicked();
//   on_GsmPermanentStatusRead_clicked();
}

//******************************************************************************
// Gsm send
//******************************************************************************

void MainWindow::on_RB_GsmSendTo_toggled(bool checked)
{
   ui->GsmSendParams->setVisible(checked);
   ui->GsmSendParams->setEnabled(checked);
   ui->PB_GsmSendTo->setEnabled(checked);
   ui->SmsText->setEnabled(true);
   ui->PhoneNumberText->setEnabled(true);
   ui->GsmSmsSlot_edit->setVisible(false);
   ui->GsmSmsSlot_label->setVisible(true);
}


void MainWindow::on_PB_GsmSendTo_clicked()
{
//   ui->GsmSmsSlot_label->setText(
//            QString::number(
//               bat2->GsmSendTo(
//                  (char*)ui->SmsText->text().toStdString().c_str(),
//                  (char*)ui->PhoneNumberText->text().toStdString().c_str())));
//   ui->GsmSmsSlot_edit->setText(ui->GsmSmsSlot_label->text());
//   on_PB_GsmStatus_clicked();
//   on_GsmPermanentStatusRead_clicked();
}

//******************************************************************************
// Gsm status
//******************************************************************************

void MainWindow::on_RB_GsmStatus_toggled(bool checked)
{
   ui->GsmSendParams->setVisible(checked);
   ui->GsmSendParams->setEnabled(checked);
   ui->PB_GsmStatus->setEnabled(checked);
   ui->PhoneNumberText->setEnabled(false);
   ui->SmsText->setEnabled(false);
   ui->GsmSmsSlot_edit->setVisible(true);
   ui->GsmSmsSlot_label->setVisible(false);

}

void MainWindow::on_PB_GsmStatus_clicked()
{
//ESmsGateSmsStatus status;

//   status = (ESmsGateSmsStatus)bat2->GsmStatus(ui->GsmSmsSlot_edit->text().toInt());
//   showSmsStatus(status);
//   if(status != SMS_GATE_SMS_STATUS_PENDING){
//      timer.stop();
//   }
}

//******************************************************************************
// Action weighing start
//******************************************************************************

void MainWindow::on_RB_WeighingStart_toggled(bool checked)
{
   ui->PB_WeighingStart->setEnabled(checked);
}

void MainWindow::on_PB_WeighingStart_clicked()
{
TActionCmd cmd;
TActionReply reply;
int size;

   if( !operationDone ){
      return;
   }
   operationDone = false;
   cmd.Cmd = ACTION_WEIGHING_SCHEDULER_START;
   bat2->ActionExecute(&cmd, ActionCmdSimpleSize(), &reply, &size);
   operationDone = true;
}

//******************************************************************************
// Action weighing stop
//******************************************************************************

void MainWindow::on_RB_WeighingStop_toggled(bool checked)
{
   ui->PB_WeighingStop->setEnabled(checked);
}

void MainWindow::on_PB_WeighingStop_clicked()
{
TActionCmd cmd;
TActionReply reply;
int size;

   if( !operationDone ){
      return;
   }
   operationDone = false;
   cmd.Cmd = ACTION_WEIGHING_SCHEDULER_STOP;
   bat2->ActionExecute(&cmd, ActionCmdSimpleSize(), &reply, &size);
   operationDone = true;
}

//******************************************************************************
// Sms status show
//******************************************************************************

void MainWindow::showSmsStatus( ESmsGateSmsStatus status)
{
   switch(status){
      case SMS_GATE_SMS_STATUS_UNDEFINED :
         ui->LB_SmsStatus->setText("UNDEFINED");
         break;
      case SMS_GATE_SMS_STATUS_EMPTY :
         ui->LB_SmsStatus->setText("EMPTY SLOT");
         break;
      case SMS_GATE_SMS_STATUS_PENDING :
         ui->LB_SmsStatus->setText("PENDING");
         break;
      case SMS_GATE_SMS_STATUS_SENDED :
         ui->LB_SmsStatus->setText("SENDED");
         break;
      case SMS_GATE_SMS_STATUS_ERROR :
         ui->LB_SmsStatus->setText("ERROR");
         break;
   }
}

void MainWindow::on_GsmPermanentStatusRead_clicked()
{
   if( ui->GsmPermanentStatusRead->isChecked()){
      connect(&timer, SIGNAL(timeout()), this, SLOT(on_PB_GsmStatus_clicked()));
      timer.setSingleShot(false);
      timer.start(2000);
   } else {
      timer.stop();
   }
}

void MainWindow::updatePBar()
{
   if(pbar->value() == pbar->maximum()){
      pbar->setValue(pbar->minimum());
   } else {
      pbar->setValue(pbar->value()+1);
   }

}

void MainWindow::on_TCPConnect_pressed()
{
   if( bat2){ return;}
   SocketIfEthernet *b2 = new SocketIfEthernet(dump);
   bat2Con = b2;
   quint16 port = ui->PortEdit->value();
   if( !b2->start("", port)){
      QMessageBox::critical(this, tr("Error"),
                            tr("Can't listen"));
      delete( b2);
      bat2Con = NULL;
      return;
   }
   bat2 = new TBat2RemoteInterface(bat2Con);
   connect( b2, SIGNAL(newConnection()), this, SLOT(ClientConnected()));
   connect( b2, SIGNAL(clientDisconnected()), this, SLOT(ClientDisconnected()));
   ui->TCPDisconnect->setEnabled(true);
   ui->TCPConnect->setEnabled(false);
   ui->RB_Uart->setEnabled(false);
   ui->RB_SMS->setEnabled(false);
}

void MainWindow::on_TCPDisconnect_clicked()
{
   SocketIfEthernet *b2 = dynamic_cast<SocketIfEthernet *>(bat2Con);
   delete( b2);
   bat2Con = NULL;
   delete bat2;
   bat2 = NULL;

   ClientDisconnected();
   ui->TCPDisconnect->setEnabled(false);
   ui->TCPConnect->setEnabled(true);
   ui->RB_Uart->setEnabled(true);
   ui->RB_SMS->setEnabled(true);
}

void MainWindow::ClientConnected(){
   ui->MainArea->setEnabled(true);
   ui->ParamsWidget->setEnabled(true);
   if( ui->RB_GsmSend->isChecked()){
      on_RB_GsmSend_toggled(true);
   }
}

void MainWindow::ClientDisconnected(){
   ui->MainArea->setEnabled(false);
   ui->ParamsWidget->setEnabled(false);
}

//******************************************************************************
// UART
//******************************************************************************

void MainWindow::UartConnect()
{
   ui->UartParams->show();
   if(bat2){
      return;
   }
   SocketIfUart *b2 = new SocketIfUart();
   bat2Con = b2;
   ui->UartParams->toUart( b2);
   if( !(b2->connected())){
      QMessageBox::critical(this, tr("Error"),
                            tr("Can't connect"));
      delete( b2);
      bat2Con = NULL;
      return;
   }
   bat2 = new TBat2RemoteInterface(bat2Con);
   ui->RB_TCP->setEnabled(false);
   ui->RB_SMS->setEnabled(false);
   ui->MainArea->setEnabled(true);
   ui->ParamsWidget->setEnabled(true);
   if( ui->RB_GsmSend->isChecked()){
      on_RB_GsmSend_toggled(true);
   }
}

void MainWindow::UartDisconnect()
{
   ui->UartParams->show();
   if(bat2Con){
      WinUart *b2 = dynamic_cast<WinUart*>(bat2Con);
      b2->disconnect();
      delete(bat2Con);
      bat2Con = NULL;
      delete(bat2);
      bat2 = NULL;
   }
   ui->RB_TCP->setEnabled(true);
   ui->RB_SMS->setEnabled(true);
   ui->MainArea->setEnabled(false);
   ui->ParamsWidget->setEnabled(false);
}

//******************************************************************************
// MODEM
//******************************************************************************

void MainWindow::ModemConnect()
{
   ui->ModemParams->show();
//   if(bat2){
//      return;
//   }
//   QString phone = ui->SmsChannelPhone->text().remove(' ');
//   ui->SmsChannelPhone->setEnabled(false);
//   Bat2SmsRemote *b2 = new Bat2SmsRemote((char *)phone.toStdString().c_str(), dump, this);
//   bat2 = b2;
//   ui->ModemParams->toUart( b2);
//   if( !(b2->connected())){
//      QMessageBox::critical(this, tr("Error"),
//                            tr("Can't connect"));
//      delete( b2);
//      bat2 = NULL;
//      return;
//   }
//   while( bat2 && !b2->inited()){
//      qApp->processEvents();
//   }
//   if( !bat2){return;}
//   ui->modemReady->setChecked(true);
//   ui->RB_TCP->setEnabled(false);
//   ui->RB_Uart->setEnabled(false);
//   ui->MainArea->setEnabled(true);
//   ui->ParamsWidget->setEnabled(true);
//   ui->ModemStatus->setEnabled(true);
//   ui->ChannelView->setModel(b2->channels());
//   if( ui->RB_GsmSend->isChecked()){
//      on_RB_GsmSend_toggled(true);
//   }
//   operationDone = true;
}

void MainWindow::ModemDisconnect()
{
   ui->ModemParams->show();
//   if(bat2){
//      Bat2SmsRemote *b2 = dynamic_cast<Bat2SmsRemote*>(bat2);
//      delete b2;
//      bat2 = NULL;
//   }
//   ui->RB_TCP->setEnabled(true);
//   ui->RB_Uart->setEnabled(true);
//   ui->MainArea->setEnabled(false);
//   ui->ParamsWidget->setEnabled(false);
//   ui->ModemStatus->setEnabled(false);
//   ui->modemReady->setChecked(false);
//   ui->SmsChannelPhone->setEnabled(true);
//   operationDone = false;
}

void MainWindow::on_RB_TCP_toggled(bool checked)
{
   if( checked){
      ui->connectionParams->setCurrentIndex(0);
   }
}

void MainWindow::on_RB_Uart_toggled(bool checked)
{
   if( checked){
      ui->connectionParams->setCurrentIndex(1);
   }
}

void MainWindow::on_RB_SMS_toggled(bool checked)
{
   if( checked){
      ui->connectionParams->setCurrentIndex(2);
   }
   ui->ModemStatus->setVisible(checked);
}

void MainWindow::on_SmsText_textChanged(const QString &arg1)
{
    ui->SmsSize->setText(QString::number(160-arg1.size()));

}

void MainWindow::on_modemReady_clicked()
{
   if( ui->ModemStatus->isChecked()){
      ui->ModemStatus->setChecked(false);
   } else {
      ui->ModemStatus->setChecked(true);
   }
}
#include "Gsm/Gsm.h"
void MainWindow::updateModemStatistics()
{
UTime dt;

   ui->SmsBSendCount->setText(QString("%1").arg( GsmStatistics.SmsBSendCount));
   ui->SmsBLostCount->setText(QString("%1").arg( GsmStatistics.SmsBLostCount));
   ui->SmsBRcvCount->setText(QString("%1").arg( GsmStatistics.SmsBRcvCount));

   ui->SmsASendCount->setText(QString("%1").arg( GsmStatistics.SmsASendCount));
   ui->SmsALostCount->setText(QString("%1").arg( GsmStatistics.SmsALostCount));
   ui->SmsARcvCount->setText(QString("%1").arg( GsmStatistics.SmsARcvCount));

   ui->SmsDeliveryCount->setText(QString("%1").arg( GsmStatistics.SmsDeliveryCount));
   uTime(&dt, GsmStatistics.AverageDeliverTime);
   ui->AverageDeliverTime->setText(QTime(0,dt.Min, dt.Sec).toString("mm:ss"));
   ui->AverageDeliverCount->setText(QString("%1").arg( GsmStatistics.AverageDeliverCount));
}
