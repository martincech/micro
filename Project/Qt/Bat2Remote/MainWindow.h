#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProgressBar>
#include <QTimer>
#include "SmsGate/SmsGateDef.h"
#include "Crt/crtdump.h"
#include "Remote/SocketIf.h"
#include "Bat2/Remote/Bat2RemoteInterface.h"

namespace Ui {
   class MainWindow;
}

class MainWindow : public QMainWindow
{
   Q_OBJECT
   
public:
   explicit MainWindow(QWidget *parent = 0);
   ~MainWindow();
   
private slots:
   void on_ClearLogButton_clicked();

   void on_RB_GsmSend_toggled(bool checked);

   void on_PB_GsmSend_clicked();

   void on_PB_GsmStatus_clicked();

   void on_GsmPermanentStatusRead_clicked();

   void on_RB_GsmStatus_toggled(bool checked);

   void updatePBar();

   void on_RB_GsmSendTo_toggled(bool checked);

   void on_PB_GsmSendTo_clicked();

   void on_TCPConnect_pressed();

   void on_TCPDisconnect_clicked();

   void ClientConnected();
   void ClientDisconnected();

   void UartConnect();

   void UartDisconnect();

   void ModemConnect();

   void ModemDisconnect();

   void on_RB_TCP_toggled(bool checked);

   void on_RB_Uart_toggled(bool checked);

   void on_RB_SMS_toggled(bool checked);

   void on_SmsText_textChanged(const QString &arg1);

   void on_RB_WeighingStart_toggled(bool checked);

   void on_RB_WeighingStop_toggled(bool checked);

   void on_PB_WeighingStart_clicked();

   void on_PB_WeighingStop_clicked();


   void on_modemReady_clicked();

   void updateModemStatistics();
private:
   QTimer  timer;
   QTimer  statisticsTimer;
   CrtDump *dump;
   Ui::MainWindow *ui;
   TBat2RemoteInterface *bat2;
   SocketIf *bat2Con;
   QProgressBar *pbar;
   bool operationDone;
   void showSmsStatus( ESmsGateSmsStatus status);
};

#endif // MAINWINDOW_H
