/********************************************************************************
** Form generated from reading UI file 'devicepanel.ui'
**
** Created: Mon 14. Apr 07:34:12 2014
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DEVICEPANEL_H
#define UI_DEVICEPANEL_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "uSimulator/lcddisplay.h"

QT_BEGIN_NAMESPACE

class Ui_DevicePanel
{
public:
    QVBoxLayout *verticalLayout;
    QCheckBox *powerOn;
    LcdDisplay *lcdDisplay;
    QGridLayout *gridLayout;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *button4;
    QPushButton *button9;
    QPushButton *button3;
    QPushButton *button8;
    QPushButton *buttonEsc;
    QPushButton *button7;
    QPushButton *button0;
    QPushButton *button5;
    QPushButton *buttonEnter;
    QPushButton *button2;
    QPushButton *button6;
    QSpacerItem *horizontalSpacer;
    QPushButton *button1;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *DevicePanel)
    {
        if (DevicePanel->objectName().isEmpty())
            DevicePanel->setObjectName(QString::fromUtf8("DevicePanel"));
        DevicePanel->resize(498, 625);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(DevicePanel->sizePolicy().hasHeightForWidth());
        DevicePanel->setSizePolicy(sizePolicy);
        DevicePanel->setMinimumSize(QSize(498, 625));
        verticalLayout = new QVBoxLayout(DevicePanel);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        powerOn = new QCheckBox(DevicePanel);
        powerOn->setObjectName(QString::fromUtf8("powerOn"));
        powerOn->setFocusPolicy(Qt::NoFocus);

        verticalLayout->addWidget(powerOn);

        lcdDisplay = new LcdDisplay(DevicePanel);
        lcdDisplay->setObjectName(QString::fromUtf8("lcdDisplay"));
        lcdDisplay->setMinimumSize(QSize(480, 320));
        lcdDisplay->setMaximumSize(QSize(480, 320));
        lcdDisplay->setFocusPolicy(Qt::NoFocus);

        verticalLayout->addWidget(lcdDisplay);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 0, 4, 1, 1);

        button4 = new QPushButton(DevicePanel);
        button4->setObjectName(QString::fromUtf8("button4"));
        sizePolicy.setHeightForWidth(button4->sizePolicy().hasHeightForWidth());
        button4->setSizePolicy(sizePolicy);
        button4->setMinimumSize(QSize(55, 55));
        button4->setMaximumSize(QSize(55, 55));
        button4->setFocusPolicy(Qt::NoFocus);

        gridLayout->addWidget(button4, 1, 1, 1, 1);

        button9 = new QPushButton(DevicePanel);
        button9->setObjectName(QString::fromUtf8("button9"));
        sizePolicy.setHeightForWidth(button9->sizePolicy().hasHeightForWidth());
        button9->setSizePolicy(sizePolicy);
        button9->setMinimumSize(QSize(55, 55));
        button9->setMaximumSize(QSize(55, 55));
        button9->setFocusPolicy(Qt::NoFocus);

        gridLayout->addWidget(button9, 2, 3, 1, 1);

        button3 = new QPushButton(DevicePanel);
        button3->setObjectName(QString::fromUtf8("button3"));
        sizePolicy.setHeightForWidth(button3->sizePolicy().hasHeightForWidth());
        button3->setSizePolicy(sizePolicy);
        button3->setMinimumSize(QSize(55, 55));
        button3->setMaximumSize(QSize(55, 55));
        button3->setFocusPolicy(Qt::NoFocus);

        gridLayout->addWidget(button3, 0, 3, 1, 1);

        button8 = new QPushButton(DevicePanel);
        button8->setObjectName(QString::fromUtf8("button8"));
        sizePolicy.setHeightForWidth(button8->sizePolicy().hasHeightForWidth());
        button8->setSizePolicy(sizePolicy);
        button8->setMinimumSize(QSize(55, 55));
        button8->setMaximumSize(QSize(55, 55));
        button8->setFocusPolicy(Qt::NoFocus);

        gridLayout->addWidget(button8, 2, 2, 1, 1);

        buttonEsc = new QPushButton(DevicePanel);
        buttonEsc->setObjectName(QString::fromUtf8("buttonEsc"));
        sizePolicy.setHeightForWidth(buttonEsc->sizePolicy().hasHeightForWidth());
        buttonEsc->setSizePolicy(sizePolicy);
        buttonEsc->setMinimumSize(QSize(55, 55));
        buttonEsc->setMaximumSize(QSize(55, 55));
        buttonEsc->setFocusPolicy(Qt::NoFocus);

        gridLayout->addWidget(buttonEsc, 3, 1, 1, 1);

        button7 = new QPushButton(DevicePanel);
        button7->setObjectName(QString::fromUtf8("button7"));
        sizePolicy.setHeightForWidth(button7->sizePolicy().hasHeightForWidth());
        button7->setSizePolicy(sizePolicy);
        button7->setMinimumSize(QSize(55, 55));
        button7->setMaximumSize(QSize(55, 55));
        button7->setFocusPolicy(Qt::NoFocus);

        gridLayout->addWidget(button7, 2, 1, 1, 1);

        button0 = new QPushButton(DevicePanel);
        button0->setObjectName(QString::fromUtf8("button0"));
        sizePolicy.setHeightForWidth(button0->sizePolicy().hasHeightForWidth());
        button0->setSizePolicy(sizePolicy);
        button0->setMinimumSize(QSize(55, 55));
        button0->setMaximumSize(QSize(55, 55));
        button0->setFocusPolicy(Qt::NoFocus);

        gridLayout->addWidget(button0, 3, 2, 1, 1);

        button5 = new QPushButton(DevicePanel);
        button5->setObjectName(QString::fromUtf8("button5"));
        sizePolicy.setHeightForWidth(button5->sizePolicy().hasHeightForWidth());
        button5->setSizePolicy(sizePolicy);
        button5->setMinimumSize(QSize(55, 55));
        button5->setMaximumSize(QSize(55, 55));
        button5->setFocusPolicy(Qt::NoFocus);

        gridLayout->addWidget(button5, 1, 2, 1, 1);

        buttonEnter = new QPushButton(DevicePanel);
        buttonEnter->setObjectName(QString::fromUtf8("buttonEnter"));
        sizePolicy.setHeightForWidth(buttonEnter->sizePolicy().hasHeightForWidth());
        buttonEnter->setSizePolicy(sizePolicy);
        buttonEnter->setMinimumSize(QSize(55, 55));
        buttonEnter->setMaximumSize(QSize(55, 55));
        buttonEnter->setFocusPolicy(Qt::NoFocus);

        gridLayout->addWidget(buttonEnter, 3, 3, 1, 1);

        button2 = new QPushButton(DevicePanel);
        button2->setObjectName(QString::fromUtf8("button2"));
        sizePolicy.setHeightForWidth(button2->sizePolicy().hasHeightForWidth());
        button2->setSizePolicy(sizePolicy);
        button2->setMinimumSize(QSize(55, 55));
        button2->setMaximumSize(QSize(55, 55));
        button2->setFocusPolicy(Qt::NoFocus);

        gridLayout->addWidget(button2, 0, 2, 1, 1);

        button6 = new QPushButton(DevicePanel);
        button6->setObjectName(QString::fromUtf8("button6"));
        sizePolicy.setHeightForWidth(button6->sizePolicy().hasHeightForWidth());
        button6->setSizePolicy(sizePolicy);
        button6->setMinimumSize(QSize(55, 55));
        button6->setMaximumSize(QSize(55, 55));
        button6->setFocusPolicy(Qt::NoFocus);

        gridLayout->addWidget(button6, 1, 3, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 0, 1, 1);

        button1 = new QPushButton(DevicePanel);
        button1->setObjectName(QString::fromUtf8("button1"));
        sizePolicy.setHeightForWidth(button1->sizePolicy().hasHeightForWidth());
        button1->setSizePolicy(sizePolicy);
        button1->setMinimumSize(QSize(55, 55));
        button1->setMaximumSize(QSize(55, 55));
        button1->setFocusPolicy(Qt::NoFocus);

        gridLayout->addWidget(button1, 0, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 4, 2, 1, 1);


        verticalLayout->addLayout(gridLayout);


        retranslateUi(DevicePanel);

        QMetaObject::connectSlotsByName(DevicePanel);
    } // setupUi

    void retranslateUi(QWidget *DevicePanel)
    {
        DevicePanel->setWindowTitle(QApplication::translate("DevicePanel", "Form", 0, QApplication::UnicodeUTF8));
        powerOn->setText(QApplication::translate("DevicePanel", "Power On", 0, QApplication::UnicodeUTF8));
        button4->setText(QApplication::translate("DevicePanel", "\342\206\220 4 GHI", 0, QApplication::UnicodeUTF8));
        button9->setText(QApplication::translate("DevicePanel", "9 WXYZ\n"
"bck", 0, QApplication::UnicodeUTF8));
        button3->setText(QApplication::translate("DevicePanel", "3 DEF", 0, QApplication::UnicodeUTF8));
        button8->setText(QApplication::translate("DevicePanel", "  8 TUV\n"
"  \342\206\223", 0, QApplication::UnicodeUTF8));
        buttonEsc->setText(QApplication::translate("DevicePanel", "Esc", 0, QApplication::UnicodeUTF8));
        button7->setText(QApplication::translate("DevicePanel", "7 PQRS\n"
"del", 0, QApplication::UnicodeUTF8));
        button0->setText(QApplication::translate("DevicePanel", "0 \313\275", 0, QApplication::UnicodeUTF8));
        button5->setText(QApplication::translate("DevicePanel", "5 JKL", 0, QApplication::UnicodeUTF8));
        buttonEnter->setText(QApplication::translate("DevicePanel", "Enter", 0, QApplication::UnicodeUTF8));
        button2->setText(QApplication::translate("DevicePanel", "\342\206\221\n"
" 2 ABC", 0, QApplication::UnicodeUTF8));
        button6->setText(QApplication::translate("DevicePanel", "6 MNO \342\206\222", 0, QApplication::UnicodeUTF8));
        button1->setText(QApplication::translate("DevicePanel", "  1 #?!...", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class DevicePanel: public Ui_DevicePanel {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DEVICEPANEL_H
