//******************************************************************************
//
//    main.cpp     Bat2compact simulator main
//    Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include <QtGui/QApplication>
#include <QMessageBox>
#include <QDir>
#include "File/Efs.h"
#include "Remote/SocketIfUsbQ.h"
#include "uSimulator/mainwindow.h"
#include "Usb/Hid/Hid.h"
#include "Memory/FileRemote.h"
#include "Action/ActionRemote.h"

#define EFS_NAME  "Root"

#define BAT2_VID  0x15A2
#define BAT2_PID  0x0400

int main(int argc, char *argv[])
{
   QApplication a(argc, argv);

   // external file system :
   QString efsPath;
   efsPath  = QDir::currentPath();         // get working directory
   efsPath += QString( "/" EFS_NAME);
   EfsRoot( efsPath.toAscii());

   THid *Hid = THid::Open(BAT2_VID, BAT2_PID);
   if(Hid == NULL) {
      QMessageBox::critical ( NULL, "Bat2Compact USB", "Device not connected");
      return 1;
   }
   SocketIfUsb *Socket = new SocketIfUsb(Hid);
   
   MainWindow *w = new MainWindow();
   w->setWindowTitle( "Bat2 Simulator");
   w->setAttribute(Qt::WA_QuitOnClose);
   // nonvolatile memory initialization :
   w->show();

   FileRemoteSetup(Socket);
   ActionRemoteSetup(Socket);

   a.exec();         // Qt main loop
   Socket->Close();
   return( 0);
} // main
