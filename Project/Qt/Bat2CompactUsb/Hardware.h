//*****************************************************************************
//
//    Hardware.h - Hardware definitions
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "D:/MICRO/Project/Qt/uBat2Compact/Hardware.h"

//MenuWeighing.c
#define STR_DIAGNOSTICS_START "Diagnostics start"

// MenuUsb.c
#define STR_LOAD_CONFIGURATION   "Load configuration"
#define STR_SAVE_CONFIGURATION   "Save configuration"
#define STR_TEST_MEMORY      "Test"
#define STR_FORMAT      "Format"
#define STR_STATUS      "Status"

#define STR_DONE  "Done"
#define STR_USB_NOT_PRESENT_INCOMPATIBLE     "Drive not present/incompatible"
#define STR_USB_CONFIG_LOADED                "Configuration loaded"
#define STR_USB_CONFIG_NONEXISTENT           "File doesn't exist"
#define STR_USB_CONFIG_CORRUPTED_BAD_VERSION "Corrupted or bad version"
#define STR_USB_ERROR                        "Error"
#define STR_USB_OK                           "Ok"

#define STR_ALL_DATA_LOST                    "All data will be lost!"

#define STR_UNABLE_WRITE_USB              "Unable write USB memory."
#define STR_CONTINUE_ANYWAY              "Continue anyway?"

#define STR_DIAGNOSTICS    "Diagnostics"

// Menu.c
#define STR_SET_DATE_TIME          "Set date & time"
#define STR_CHECK_RTC_BATTERY "Check backup battery"

#define STR_POWER_OFF         "Really power off?"
#define STR_REALLY_POWER_OFF  "Power off"

// MenuCharger
#define STR_CHARGING   "Charging"
#define STR_CHARGING_OFF    "Charging off"

// Screen.c
#define STR_CHARGED    "Charged"

// MenuMaintenance
#define STR_POWER      "Power"
#define STR_BOOTLOADER "Bootloader"

#define BAT2_HW_VERSION    0x0402

#define OPTION_REMOTE