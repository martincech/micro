/********************************************************************************
** Form generated from reading UI file 'SocketParameters.ui'
**
** Created: Mon 14. Apr 07:34:12 2014
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SOCKETPARAMETERS_H
#define UI_SOCKETPARAMETERS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>
#include "Crt/Crt.h"

QT_BEGIN_NAMESPACE

class Ui_SocketParameters
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLabel *IpLabel;
    QLineEdit *IpEdit;
    QLabel *PortLabel;
    QSpinBox *PortEdit;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer_2;
    Crt *logger;

    void setupUi(QDialog *SocketParameters)
    {
        if (SocketParameters->objectName().isEmpty())
            SocketParameters->setObjectName(QString::fromUtf8("SocketParameters"));
        SocketParameters->resize(258, 285);
        verticalLayout = new QVBoxLayout(SocketParameters);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        IpLabel = new QLabel(SocketParameters);
        IpLabel->setObjectName(QString::fromUtf8("IpLabel"));

        gridLayout->addWidget(IpLabel, 0, 0, 1, 1);

        IpEdit = new QLineEdit(SocketParameters);
        IpEdit->setObjectName(QString::fromUtf8("IpEdit"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(IpEdit->sizePolicy().hasHeightForWidth());
        IpEdit->setSizePolicy(sizePolicy);
        IpEdit->setInputMask(QString::fromUtf8("000\\.000\\.000\\.000; "));
        IpEdit->setText(QString::fromUtf8("192.168.0.1"));
        IpEdit->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(IpEdit, 0, 1, 1, 1);

        PortLabel = new QLabel(SocketParameters);
        PortLabel->setObjectName(QString::fromUtf8("PortLabel"));

        gridLayout->addWidget(PortLabel, 1, 0, 1, 1);

        PortEdit = new QSpinBox(SocketParameters);
        PortEdit->setObjectName(QString::fromUtf8("PortEdit"));
        PortEdit->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        PortEdit->setMinimum(20000);
        PortEdit->setMaximum(65535);
        PortEdit->setValue(50000);

        gridLayout->addWidget(PortEdit, 1, 1, 1, 1);


        verticalLayout->addLayout(gridLayout);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton = new QPushButton(SocketParameters);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout->addWidget(pushButton);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout);

        logger = new Crt(SocketParameters);
        logger->setObjectName(QString::fromUtf8("logger"));

        verticalLayout->addWidget(logger);


        retranslateUi(SocketParameters);

        QMetaObject::connectSlotsByName(SocketParameters);
    } // setupUi

    void retranslateUi(QDialog *SocketParameters)
    {
        SocketParameters->setWindowTitle(QApplication::translate("SocketParameters", "Dialog", 0, QApplication::UnicodeUTF8));
        IpLabel->setText(QApplication::translate("SocketParameters", "IP:", 0, QApplication::UnicodeUTF8));
        PortLabel->setText(QApplication::translate("SocketParameters", "Port:", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("SocketParameters", "Connect", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SocketParameters: public Ui_SocketParameters {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SOCKETPARAMETERS_H
