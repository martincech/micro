//******************************************************************************
//
//   MainWindow.h    Bat2Platform simulator window
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include "Crt/CrtDump.h"
#include "Socket/LocalServer.h"
#include "Bat2Platform/PlatformSimulator/PlatformSimulator.h"

namespace Ui {
   class MainWindow;
}

class MainWindow : public QMainWindow
{
   Q_OBJECT
   
public:
   explicit MainWindow(QWidget *parent = 0);
   ~MainWindow();

private slots:
   void on_actionDump_triggered();
   void timerTimeout();
   void dataReceived();

private:
   Ui::MainWindow *ui;
   CrtDump        *_dump;
   LocalServer    *_server;
   QTimer         *_timer;
   PlatformSimulator    *_PlatformSimulator;
};

#endif // MAINWINDOW_H
