//******************************************************************************
//
//   Samples.h   Weighing samples loader
//   Version 1.0 (c) VEIT Electronics
//
//******************************************************************************

#ifndef SAMPLES_H
#define SAMPLES_H

#include "Parse/Csv.h"
#include "Weight/WeightDef.h"

//------------------------------------------------------------------------------
//   Samples
//------------------------------------------------------------------------------

class Samples
{
public:
   Samples();
   ~Samples();

   bool load( QString fileName);
   int  count();

private:
   Csv          *_csv;
   TWeightGauge *_samples;
   int           _samplesCount;
}; // Samples

//------------------------------------------------------------------------------

#endif // MAINWINDOW_H
