//******************************************************************************
//
//   MainWindow.h uGenerator main window
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include "Parse/Csv.h"
#include "uGenerator/uGenerator.h"

//------------------------------------------------------------------------------

namespace Ui {
class MainWindow;
}

//------------------------------------------------------------------------------

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
   void on_actionOpen_triggered();
   void on_actionSave_triggered();

   void on_actionObject_triggered();
   void on_actionDictionary_triggered();

   void on_actionHeader_triggered();
   void on_actionDefaults_triggered();
   void on_actionMenu_triggered();
   void on_actionMenuHeader_triggered();
   void on_actionStrings_triggered();

   void on_actionPrevious_triggered();
   void on_actionNext_triggered();


private:
    Ui::MainWindow *ui;

    QString     fileName;
    uGenerator *generator;
    int         currentMenu;
    QStringList menuSource;
    QStringList menuHeader;

    Csv *csvObject;
    Csv *csvDictionary;
}; // MainWindow

#endif // MAINWINDOW_H
