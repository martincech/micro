//******************************************************************************
//
//   MainWindow.cpp uGenerator main window
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QDir>
#include <QFile>
#include <QByteArray>
#include <QSettings>
#include "Parse/TextFile.h"

#define ROW_HEIGHT 16                // pixels

#define DICTIONARY_SUFFIX       ".dic"
#define HEADER_SUFFIX           "Def.h"
#define DEFAULTS_SUFFIX         "Default.c"
#define MENU_SUFFIX             ".c"
#define MENU_HEADER_SUFFIX      ".h"
#define STRINGS_SUFFIX          ".str"

// page indexes :
typedef enum {
   PAGE_OBJECT,
   PAGE_DICTIONARY,
   PAGE_HEADER,
   PAGE_DEFAULTS,
   PAGE_MENU,
   PAGE_MENU_HEADER,
   PAGE_STRINGS,
   _PAGE_LAST
} EPageIndex;



//------------------------------------------------------------------------------
//   Constructor
//------------------------------------------------------------------------------

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    // generator :
    generator = new uGenerator();
    // object table :
    csvObject = new Csv( OBJECT_COLUMNS_MAX);                   // create object table
    ui->tableObject->setModel( csvObject->array());             // set object table model
    ui->tableObject->verticalHeader()->setDefaultSectionSize( ROW_HEIGHT);  // object table row height
    // dictionary table :
    csvDictionary = new Csv( DICTIONARY_COLUMNS_MAX);           // create dictionary table
    ui->tableDictionary->setModel( csvDictionary->array());     // set dictionary table model
    ui->tableDictionary->verticalHeader()->setDefaultSectionSize( ROW_HEIGHT);  // dictionary table row height
    currentMenu = 0;
    menuSource.append( QString());                              // add empty page
    menuHeader.append( QString());                              // add empty page
    ui->actionPrevious->setEnabled( false);
    ui->actionNext->setEnabled( false);
} // MainWindow

//------------------------------------------------------------------------------
//   Destructor
//------------------------------------------------------------------------------

MainWindow::~MainWindow()
{
   delete ui;
   if( csvObject){
      delete csvObject;
   }
   if( csvDictionary){
      delete csvDictionary;
   }
} // ~MainWindow

//------------------------------------------------------------------------------
//   Open
//------------------------------------------------------------------------------

void MainWindow::on_actionOpen_triggered()
{
   QSettings settings("Veit", "uGenerator");
   fileName = settings.value("FilePath").toString();
   fileName = QFileDialog::getOpenFileName( this, "Open file",
                                            fileName,
                                            "CSV files (*.csv);;All files (*.*)");
   if( fileName.isNull()){
      return;
   }
   settings.setValue("FilePath", fileName);
   csvObject->array()->setRowsCount( 0);     // clear contents
   if( !csvObject->load( fileName)){
      QString msg;
      msg.sprintf( "Unable parse file '%s'", qPrintable( fileName));
      QMessageBox::warning( this, "Error", msg, QMessageBox::Ok);
      return;
   }
   // get dictionary file name :
   QString   fileDictionary;
   QFileInfo fileInfo( fileName);
   fileDictionary = fileInfo.path() + QString( QDir::separator()) + fileInfo.baseName() + QString( DICTIONARY_SUFFIX);
   csvDictionary->array()->setRowsCount( 0);     // clear contents
   if( !QFile::exists( fileDictionary)){
      QMessageBox::information( this, "Info",
                                "Dictionary not found",
                                QMessageBox::Ok);
   } else {
      // open dictionary file name :
      if( !csvDictionary->load( fileDictionary)){
         QString msg;
         msg.sprintf( "Unable parse file '%s'", qPrintable( fileDictionary));
         QMessageBox::warning( this, "Error", msg, QMessageBox::Ok);
         return;
      }
   }
   // clear sources :
   ui->textHeader->clear();
   ui->textDefaults->clear();
   ui->textMenu->clear();
   ui->textMenuHeader->clear();
   ui->textStrings->clear();
   // run generator :
   if( !generator->parse( csvObject->array(), csvDictionary->array())){
      QMessageBox::warning( this, "Error", "Unable generate files", QMessageBox::Ok);
      return;
   }
   // create sources :
   currentMenu = 0;                                        // set first menu page
   menuSource = generator->menuSource();                   // generate all menus
   menuHeader = generator->menuHeader();                   // generate all menu headers
   ui->textHeader->insertPlainText( generator->headerSource());
   ui->textDefaults->insertPlainText( generator->defaultsSource());
   if( menuSource.isEmpty()){
      ui->textMenu->insertPlainText( QString());           // empty menu list
   } else {
      ui->textMenu->insertPlainText( menuSource[ currentMenu]);
   }
   if( menuHeader.isEmpty()){
      ui->textMenuHeader->insertPlainText( QString());     // empty menu header list
   } else {
      ui->textMenuHeader->insertPlainText( menuHeader[ currentMenu]);
   }
   ui->textStrings->insertPlainText( generator->stringsSource());
   // show object page :
   on_actionObject_triggered();
} // on_actionOpen_triggered

//------------------------------------------------------------------------------
//   Save
//------------------------------------------------------------------------------

void MainWindow::on_actionSave_triggered()
{
   QString saveName;
   QFileInfo saveInfo( fileName);
   saveName = saveInfo.path() + QString( QDir::separator()) + saveInfo.baseName();
   saveName = QFileDialog::getSaveFileName( this, "Save file",
                                            saveName,
                                            "All files (*.*)");
   if( saveName.isNull()){
      return;
   }
   QFileInfo fileInfo( saveName);
   TextFile  file;
   // save header :
   QString  headerName;
   headerName = fileInfo.path() + QString( QDir::separator()) + fileInfo.baseName() + QString( HEADER_SUFFIX);
   file.setFileName( headerName);
   file.save( ui->textHeader->toPlainText());
   // save defaults :
   QString  defaultsName;
   defaultsName = fileInfo.path() + QString( QDir::separator()) + fileInfo.baseName() + QString( DEFAULTS_SUFFIX);
   file.setFileName( defaultsName);
   file.save( ui->textDefaults->toPlainText());
   // save menu :
   QStringList menuFiles = generator->menuFileNames();
   QString     menuName;
   for( int i = 0; i < menuFiles.count(); i++){
      // source code :
      menuName = fileInfo.path() + QString( QDir::separator()) + menuFiles[ i] + QString( MENU_SUFFIX);
      file.setFileName( menuName);
      file.save( menuSource[ i]);
      // header file :
      menuName = fileInfo.path() + QString( QDir::separator()) + menuFiles[ i] + QString( MENU_HEADER_SUFFIX);
      file.setFileName( menuName);
      file.save( menuHeader[ i]);
   }
   // save strings :
   QString stringsName;
   stringsName = fileInfo.path() + QString( QDir::separator()) + fileInfo.baseName() + QString( STRINGS_SUFFIX);
   file.setFileName( stringsName);
   file.save( ui->textStrings->toPlainText());
} // on_actionSave_triggered

//------------------------------------------------------------------------------
//   Object
//------------------------------------------------------------------------------

void MainWindow::on_actionObject_triggered()
{
   ui->pageSelector->setCurrentIndex( PAGE_OBJECT);
   ui->actionPrevious->setEnabled( false);
   ui->actionNext->setEnabled( false);
} // on_actionObject_triggered

//------------------------------------------------------------------------------
//   Dictionary
//------------------------------------------------------------------------------

void MainWindow::on_actionDictionary_triggered()
{
   ui->pageSelector->setCurrentIndex( PAGE_DICTIONARY);
   ui->actionPrevious->setEnabled( false);
   ui->actionNext->setEnabled( false);
} // on_actionDictionary_triggered

//------------------------------------------------------------------------------
//   Header
//------------------------------------------------------------------------------

void MainWindow::on_actionHeader_triggered()
{
   ui->pageSelector->setCurrentIndex( PAGE_HEADER);
   ui->actionPrevious->setEnabled( false);
   ui->actionNext->setEnabled( false);
} // on_actionHeader_triggered

//------------------------------------------------------------------------------
//   Defaults
//------------------------------------------------------------------------------

void MainWindow::on_actionDefaults_triggered()
{
   ui->pageSelector->setCurrentIndex( PAGE_DEFAULTS);
   ui->actionPrevious->setEnabled( false);
   ui->actionNext->setEnabled( false);
} // on_actionDefaults_triggered

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MainWindow::on_actionMenu_triggered()
{
   ui->pageSelector->setCurrentIndex( PAGE_MENU);
   ui->actionPrevious->setEnabled( true);
   ui->actionNext->setEnabled( true);
} // on_actionMenu_triggered

//------------------------------------------------------------------------------
//   Menu header
//------------------------------------------------------------------------------

void MainWindow::on_actionMenuHeader_triggered()
{
   ui->pageSelector->setCurrentIndex( PAGE_MENU_HEADER);
   ui->actionPrevious->setEnabled( true);
   ui->actionNext->setEnabled( true);
} // on_actionMenuHeader_triggered

//------------------------------------------------------------------------------
//   Strings
//------------------------------------------------------------------------------

void MainWindow::on_actionStrings_triggered()
{
   ui->pageSelector->setCurrentIndex( PAGE_STRINGS);
   ui->actionPrevious->setEnabled( false);
   ui->actionNext->setEnabled( false);
} // on_actionStrings_triggered

//------------------------------------------------------------------------------
//   Previous
//------------------------------------------------------------------------------

void MainWindow::on_actionPrevious_triggered()
{
   if( currentMenu > 0){
      currentMenu--;
   }
   ui->textMenu->clear();
   ui->textMenu->insertPlainText( menuSource[ currentMenu]);
   ui->textMenuHeader->clear();
   ui->textMenuHeader->insertPlainText( menuHeader[ currentMenu]);
} // on_actionPrevious_triggered

//------------------------------------------------------------------------------
//   Next
//------------------------------------------------------------------------------

void MainWindow::on_actionNext_triggered()
{
   if( currentMenu < menuSource.size() - 1){
      currentMenu++;
   }
   ui->textMenu->clear();
   ui->textMenu->insertPlainText( menuSource[ currentMenu]);
   ui->textMenuHeader->clear();
   ui->textMenuHeader->insertPlainText( menuHeader[ currentMenu]);
} // on_actionNext_triggered
