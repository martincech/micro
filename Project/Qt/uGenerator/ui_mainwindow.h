/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Sat 21. Jul 11:20:25 2012
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QPlainTextEdit>
#include <QtGui/QStackedWidget>
#include <QtGui/QStatusBar>
#include <QtGui/QTableView>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionObject;
    QAction *actionDictionary;
    QAction *actionHeader;
    QAction *actionMenu;
    QAction *actionStrings;
    QAction *actionDefaults;
    QAction *actionPrevious;
    QAction *actionNext;
    QAction *actionMenuHeader;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QStackedWidget *pageSelector;
    QWidget *pageObject;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_4;
    QLabel *labelObject;
    QTableView *tableObject;
    QWidget *pageDictionary;
    QHBoxLayout *horizontalLayout_6;
    QVBoxLayout *verticalLayout_5;
    QLabel *labelDictionary;
    QTableView *tableDictionary;
    QWidget *pageHeader;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout;
    QLabel *labelHeader;
    QPlainTextEdit *textHeader;
    QWidget *pageDefaults;
    QHBoxLayout *horizontalLayout_7;
    QVBoxLayout *verticalLayout_6;
    QLabel *labelDefaults;
    QPlainTextEdit *textDefaults;
    QWidget *pageMenu;
    QHBoxLayout *horizontalLayout_4;
    QVBoxLayout *verticalLayout_2;
    QLabel *labelMenu;
    QPlainTextEdit *textMenu;
    QWidget *pageMenuHeader;
    QHBoxLayout *horizontalLayout_8;
    QVBoxLayout *verticalLayout_7;
    QLabel *labelMenuHeader;
    QPlainTextEdit *textMenuHeader;
    QWidget *pageString;
    QHBoxLayout *horizontalLayout_5;
    QVBoxLayout *verticalLayout_3;
    QLabel *labelStrings;
    QPlainTextEdit *textStrings;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(759, 550);
        QFont font;
        font.setFamily(QString::fromUtf8("Courier New"));
        MainWindow->setFont(font);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/image/generator.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/image/open.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionOpen->setIcon(icon1);
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/image/save.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSave->setIcon(icon2);
        actionObject = new QAction(MainWindow);
        actionObject->setObjectName(QString::fromUtf8("actionObject"));
        actionDictionary = new QAction(MainWindow);
        actionDictionary->setObjectName(QString::fromUtf8("actionDictionary"));
        actionHeader = new QAction(MainWindow);
        actionHeader->setObjectName(QString::fromUtf8("actionHeader"));
        actionMenu = new QAction(MainWindow);
        actionMenu->setObjectName(QString::fromUtf8("actionMenu"));
        actionStrings = new QAction(MainWindow);
        actionStrings->setObjectName(QString::fromUtf8("actionStrings"));
        actionDefaults = new QAction(MainWindow);
        actionDefaults->setObjectName(QString::fromUtf8("actionDefaults"));
        actionPrevious = new QAction(MainWindow);
        actionPrevious->setObjectName(QString::fromUtf8("actionPrevious"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/image/leftarrow.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionPrevious->setIcon(icon3);
        actionNext = new QAction(MainWindow);
        actionNext->setObjectName(QString::fromUtf8("actionNext"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/image/rightarrow.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionNext->setIcon(icon4);
        actionMenuHeader = new QAction(MainWindow);
        actionMenuHeader->setObjectName(QString::fromUtf8("actionMenuHeader"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pageSelector = new QStackedWidget(centralWidget);
        pageSelector->setObjectName(QString::fromUtf8("pageSelector"));
        pageObject = new QWidget();
        pageObject->setObjectName(QString::fromUtf8("pageObject"));
        horizontalLayout_2 = new QHBoxLayout(pageObject);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        labelObject = new QLabel(pageObject);
        labelObject->setObjectName(QString::fromUtf8("labelObject"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("MS Shell Dlg 2"));
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setWeight(75);
        labelObject->setFont(font1);

        verticalLayout_4->addWidget(labelObject);

        tableObject = new QTableView(pageObject);
        tableObject->setObjectName(QString::fromUtf8("tableObject"));

        verticalLayout_4->addWidget(tableObject);


        horizontalLayout_2->addLayout(verticalLayout_4);

        pageSelector->addWidget(pageObject);
        pageDictionary = new QWidget();
        pageDictionary->setObjectName(QString::fromUtf8("pageDictionary"));
        horizontalLayout_6 = new QHBoxLayout(pageDictionary);
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        labelDictionary = new QLabel(pageDictionary);
        labelDictionary->setObjectName(QString::fromUtf8("labelDictionary"));
        labelDictionary->setFont(font1);

        verticalLayout_5->addWidget(labelDictionary);

        tableDictionary = new QTableView(pageDictionary);
        tableDictionary->setObjectName(QString::fromUtf8("tableDictionary"));

        verticalLayout_5->addWidget(tableDictionary);


        horizontalLayout_6->addLayout(verticalLayout_5);

        pageSelector->addWidget(pageDictionary);
        pageHeader = new QWidget();
        pageHeader->setObjectName(QString::fromUtf8("pageHeader"));
        horizontalLayout_3 = new QHBoxLayout(pageHeader);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        labelHeader = new QLabel(pageHeader);
        labelHeader->setObjectName(QString::fromUtf8("labelHeader"));
        labelHeader->setFont(font1);

        verticalLayout->addWidget(labelHeader);

        textHeader = new QPlainTextEdit(pageHeader);
        textHeader->setObjectName(QString::fromUtf8("textHeader"));
        textHeader->setUndoRedoEnabled(false);
        textHeader->setLineWrapMode(QPlainTextEdit::NoWrap);
        textHeader->setReadOnly(true);

        verticalLayout->addWidget(textHeader);


        horizontalLayout_3->addLayout(verticalLayout);

        pageSelector->addWidget(pageHeader);
        pageDefaults = new QWidget();
        pageDefaults->setObjectName(QString::fromUtf8("pageDefaults"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("MS Shell Dlg 2"));
        pageDefaults->setFont(font2);
        horizontalLayout_7 = new QHBoxLayout(pageDefaults);
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        labelDefaults = new QLabel(pageDefaults);
        labelDefaults->setObjectName(QString::fromUtf8("labelDefaults"));
        QFont font3;
        font3.setPointSize(12);
        font3.setBold(true);
        font3.setWeight(75);
        labelDefaults->setFont(font3);

        verticalLayout_6->addWidget(labelDefaults);

        textDefaults = new QPlainTextEdit(pageDefaults);
        textDefaults->setObjectName(QString::fromUtf8("textDefaults"));
        textDefaults->setFont(font);
        textDefaults->setLineWrapMode(QPlainTextEdit::NoWrap);

        verticalLayout_6->addWidget(textDefaults);


        horizontalLayout_7->addLayout(verticalLayout_6);

        pageSelector->addWidget(pageDefaults);
        pageMenu = new QWidget();
        pageMenu->setObjectName(QString::fromUtf8("pageMenu"));
        horizontalLayout_4 = new QHBoxLayout(pageMenu);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        labelMenu = new QLabel(pageMenu);
        labelMenu->setObjectName(QString::fromUtf8("labelMenu"));
        labelMenu->setFont(font1);

        verticalLayout_2->addWidget(labelMenu);

        textMenu = new QPlainTextEdit(pageMenu);
        textMenu->setObjectName(QString::fromUtf8("textMenu"));
        textMenu->setUndoRedoEnabled(false);
        textMenu->setLineWrapMode(QPlainTextEdit::NoWrap);
        textMenu->setReadOnly(true);

        verticalLayout_2->addWidget(textMenu);


        horizontalLayout_4->addLayout(verticalLayout_2);

        pageSelector->addWidget(pageMenu);
        pageMenuHeader = new QWidget();
        pageMenuHeader->setObjectName(QString::fromUtf8("pageMenuHeader"));
        horizontalLayout_8 = new QHBoxLayout(pageMenuHeader);
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        labelMenuHeader = new QLabel(pageMenuHeader);
        labelMenuHeader->setObjectName(QString::fromUtf8("labelMenuHeader"));
        labelMenuHeader->setFont(font1);

        verticalLayout_7->addWidget(labelMenuHeader);

        textMenuHeader = new QPlainTextEdit(pageMenuHeader);
        textMenuHeader->setObjectName(QString::fromUtf8("textMenuHeader"));
        textMenuHeader->setLineWrapMode(QPlainTextEdit::NoWrap);

        verticalLayout_7->addWidget(textMenuHeader);


        horizontalLayout_8->addLayout(verticalLayout_7);

        pageSelector->addWidget(pageMenuHeader);
        pageString = new QWidget();
        pageString->setObjectName(QString::fromUtf8("pageString"));
        horizontalLayout_5 = new QHBoxLayout(pageString);
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        labelStrings = new QLabel(pageString);
        labelStrings->setObjectName(QString::fromUtf8("labelStrings"));
        labelStrings->setFont(font1);

        verticalLayout_3->addWidget(labelStrings);

        textStrings = new QPlainTextEdit(pageString);
        textStrings->setObjectName(QString::fromUtf8("textStrings"));
        textStrings->setUndoRedoEnabled(false);
        textStrings->setLineWrapMode(QPlainTextEdit::NoWrap);
        textStrings->setReadOnly(true);

        verticalLayout_3->addWidget(textStrings);


        horizontalLayout_5->addLayout(verticalLayout_3);

        pageSelector->addWidget(pageString);

        horizontalLayout->addWidget(pageSelector);

        MainWindow->setCentralWidget(centralWidget);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        QFont font4;
        font4.setFamily(QString::fromUtf8("MS Shell Dlg 2"));
        font4.setBold(true);
        font4.setWeight(75);
        mainToolBar->setFont(font4);
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        mainToolBar->addAction(actionOpen);
        mainToolBar->addAction(actionSave);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionObject);
        mainToolBar->addAction(actionDictionary);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionHeader);
        mainToolBar->addAction(actionDefaults);
        mainToolBar->addAction(actionMenu);
        mainToolBar->addAction(actionMenuHeader);
        mainToolBar->addAction(actionStrings);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionPrevious);
        mainToolBar->addAction(actionNext);

        retranslateUi(MainWindow);

        pageSelector->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Generator", 0, QApplication::UnicodeUTF8));
        actionOpen->setText(QApplication::translate("MainWindow", "Open", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionOpen->setToolTip(QApplication::translate("MainWindow", "Open File", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionSave->setText(QApplication::translate("MainWindow", "Save", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionSave->setToolTip(QApplication::translate("MainWindow", "Save Files", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionObject->setText(QApplication::translate("MainWindow", "Object", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionObject->setToolTip(QApplication::translate("MainWindow", "Show Objects", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionDictionary->setText(QApplication::translate("MainWindow", "Dictionary", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionDictionary->setToolTip(QApplication::translate("MainWindow", "Show Dictionary", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionHeader->setText(QApplication::translate("MainWindow", "Header", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionHeader->setToolTip(QApplication::translate("MainWindow", "Show Header", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionMenu->setText(QApplication::translate("MainWindow", "Menu", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionMenu->setToolTip(QApplication::translate("MainWindow", "Show Menu", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionStrings->setText(QApplication::translate("MainWindow", "Strings", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionStrings->setToolTip(QApplication::translate("MainWindow", "Show Strings", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionDefaults->setText(QApplication::translate("MainWindow", "Defaults", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionDefaults->setToolTip(QApplication::translate("MainWindow", "Default values", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionPrevious->setText(QApplication::translate("MainWindow", "Previous", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionPrevious->setToolTip(QApplication::translate("MainWindow", "Previous menu", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionNext->setText(QApplication::translate("MainWindow", "Next", 0, QApplication::UnicodeUTF8));
        actionMenuHeader->setText(QApplication::translate("MainWindow", "M.Header", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionMenuHeader->setToolTip(QApplication::translate("MainWindow", "Menu header source", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        labelObject->setText(QApplication::translate("MainWindow", "Object list", 0, QApplication::UnicodeUTF8));
        labelDictionary->setText(QApplication::translate("MainWindow", "Dictionary", 0, QApplication::UnicodeUTF8));
        labelHeader->setText(QApplication::translate("MainWindow", "Header File", 0, QApplication::UnicodeUTF8));
        labelDefaults->setText(QApplication::translate("MainWindow", "Default values", 0, QApplication::UnicodeUTF8));
        labelMenu->setText(QApplication::translate("MainWindow", "Menu Source", 0, QApplication::UnicodeUTF8));
        labelMenuHeader->setText(QApplication::translate("MainWindow", "Menu Header", 0, QApplication::UnicodeUTF8));
        labelStrings->setText(QApplication::translate("MainWindow", "Strings list", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
