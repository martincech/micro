//******************************************************************************
//
//   MenuMaintenanceAcceptance.h  Maintenance acceptance menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuMaintenanceAcceptance_H__
   #define __MenuMaintenanceAcceptance_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Platform_H__
   #include "Platform.h"
#endif


void MenuMaintenanceAcceptance( void);
// Menu maintenance acceptance

#endif
