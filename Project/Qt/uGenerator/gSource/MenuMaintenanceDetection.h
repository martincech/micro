//******************************************************************************
//
//   MenuMaintenanceDetection.h  Maintenance detection menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuMaintenanceDetection_H__
   #define __MenuMaintenanceDetection_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Platform_H__
   #include "Platform.h"
#endif


void MenuMaintenanceDetection( void);
// Menu maintenance detection

#endif
