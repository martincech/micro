//******************************************************************************
//
//   MenuMaintenanceDevice.c  Maintenance device menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuMaintenanceDevice.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config.h"               // Project configuration

#include "Platform.h"
#include "MenuDeviceVersion.h"
#include "MenuDeviceNext.h"
#include "MenuDevicePrevious.h"


static DefMenu( MaintenanceDeviceMenu)
   STR_DEVICE,
   STR_NAME,
   STR_VERSION,
   STR_KIND,
   STR_NEXT,
   STR_PREVIOUS,
EndMenu()

typedef enum {
   MI_DEVICE,
   MI_NAME,
   MI_VERSION,
   MI_KIND,
   MI_NEXT,
   MI_PREVIOUS
} EMaintenanceDeviceMenu;

// Local functions :

static void PlatformDeviceParameters( int Index, int y, TPlatformDevice *Parameters);
// Draw maintenance device parameters

//------------------------------------------------------------------------------
//  Menu MaintenanceDevice
//------------------------------------------------------------------------------

void MenuMaintenanceDevice( void)
// Edit maintenance device parameters
{
TMenuData MData;
int       i;
char Name[ PLATFORM_DEVICE_NAME_SIZE + 1];


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_DEVICE, MaintenanceDeviceMenu, (TMenuItemCb *)PlatformDeviceParameters, &PlatformDevice, &MData)){
         ConfigPlatformDeviceSave();
         return;
      }
      switch( MData.Item){
         case MI_DEVICE :
            MenuDeviceVersion();
            break;

         case MI_NAME :
            strcpy( Name, PlatformDevice.Name);
            if( !DInputText( STR_NAME, STR_ENTER_NAME, Name, PLATFORM_DEVICE_NAME_SIZE)){
               break;
            }
            strcpy( PlatformDevice.Name, Name);
            break;

         case MI_VERSION :
            i = PlatformDevice.Version;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_DEVICE_VERSION_MIN, PLATFORM_DEVICE_VERSION_MAX, 0)){
               break;
            }
            PlatformDevice.Version = (word)i;
            break;

         case MI_KIND :
            i = PlatformDevice.Kind;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 1, PLATFORM_DEVICE_KIND_MIN, PLATFORM_DEVICE_KIND_MAX, "%")){
               break;
            }
            PlatformDevice.Kind = (TDeviceKind)i;
            break;

         case MI_NEXT :
            MenuDeviceNext();
            break;

         case MI_PREVIOUS :
            MenuDevicePrevious();
            break;

      }
   }
} // MenuMaintenanceDevice

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void PlatformDeviceParameters( int Index, int y, TPlatformDevice *Parameters)
// Draw maintenance device parameters
{
   switch( Index){
      case MI_DEVICE :
         break;

      case MI_NAME :
         DLabelNarrow( Parameters->Name, DMENU_PARAMETERS_X, y);
         break;

      case MI_VERSION :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->Version, 0);
         break;

      case MI_KIND :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%f6.1", Parameters->Kind, "%");
         break;

      case MI_NEXT :
         break;

      case MI_PREVIOUS :
         break;

   }
} // PlatformDeviceParameters
