//******************************************************************************
//
//   PlatformDef.h  Bat2 weighing platform data
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __PlatformDef_H__
   #define __PlatformDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif


//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#define PLATFORM_SIGMA_DELTA_RATE_MIN 1
#define PLATFORM_SIGMA_DELTA_RATE_MAX 5000
#define PLATFORM_SIGMA_DELTA_RATE_DEFAULT 10

#define PLATFORM_SIGMA_DELTA_PREFILTER_MIN 1
#define PLATFORM_SIGMA_DELTA_PREFILTER_MAX 500
#define PLATFORM_SIGMA_DELTA_PREFILTER_DEFAULT 1

#define PLATFORM_PICOSTRAIN_ACCURACY_MIN 0
#define PLATFORM_PICOSTRAIN_ACCURACY_MAX 101
#define PLATFORM_PICOSTRAIN_ACCURACY_DEFAULT 10

#define PLATFORM_PICOSTRAIN_RATE_MIN 1
#define PLATFORM_PICOSTRAIN_RATE_MAX 10000
#define PLATFORM_PICOSTRAIN_RATE_DEFAULT 10

#define PLATFORM_PICOSTRAIN_PREFILTER_MIN 1
#define PLATFORM_PICOSTRAIN_PREFILTER_MAX 1000
#define PLATFORM_PICOSTRAIN_PREFILTER_DEFAULT 1

#define PLATFORM_CALIBRATION_FULL_RANGE_MIN 0
#define PLATFORM_CALIBRATION_FULL_RANGE_MAX 0
#define PLATFORM_CALIBRATION_FULL_RANGE_DEFAULT 5000

#define PLATFORM_CALIBRATION_DELAY_MIN 1000
#define PLATFORM_CALIBRATION_DELAY_MAX 50000
#define PLATFORM_CALIBRATION_DELAY_DEFAULT 1000

#define PLATFORM_CALIBRATION_DURATION_MIN 1000
#define PLATFORM_CALIBRATION_DURATION_MAX 50000
#define PLATFORM_CALIBRATION_DURATION_DEFAULT 3000

#define PLATFORM_DETECTION_AVERAGING_WINDOW_MIN 1
#define PLATFORM_DETECTION_AVERAGING_WINDOW_MAX 150
#define PLATFORM_DETECTION_AVERAGING_WINDOW_DEFAULT 10

#define PLATFORM_DETECTION_ABSOLUTE_RANGE_MIN 0
#define PLATFORM_DETECTION_ABSOLUTE_RANGE_MAX 0
#define PLATFORM_DETECTION_ABSOLUTE_RANGE_DEFAULT 1

#define PLATFORM_DETECTION_RELATIVE_RANGE_MIN 1
#define PLATFORM_DETECTION_RELATIVE_RANGE_MAX 200
#define PLATFORM_DETECTION_RELATIVE_RANGE_DEFAULT 0

#define PLATFORM_DETECTION_STABLE_WINDOW_MIN 1
#define PLATFORM_DETECTION_STABLE_WINDOW_MAX 100
#define PLATFORM_DETECTION_STABLE_WINDOW_DEFAULT 5

#define PLATFORM_DETECTION_THRESHOLD_WEIGHT_MIN 1
#define PLATFORM_DETECTION_THRESHOLD_WEIGHT_MAX 10000
#define PLATFORM_DETECTION_THRESHOLD_WEIGHT_DEFAULT 100

#define PLATFORM_ACCEPTANCE_LOW_LIMIT_MIN 0
#define PLATFORM_ACCEPTANCE_LOW_LIMIT_MAX 0
#define PLATFORM_ACCEPTANCE_LOW_LIMIT_DEFAULT 1000

#define PLATFORM_ACCEPTANCE_HIGH_LIMIT_MIN 0
#define PLATFORM_ACCEPTANCE_HIGH_LIMIT_MAX 0
#define PLATFORM_ACCEPTANCE_HIGH_LIMIT_DEFAULT 1500

#define PLATFORM_ACCEPTANCE_LOW_LIMIT_FEMALE_MIN 0
#define PLATFORM_ACCEPTANCE_LOW_LIMIT_FEMALE_MAX 0
#define PLATFORM_ACCEPTANCE_LOW_LIMIT_FEMALE_DEFAULT 500

#define PLATFORM_ACCEPTANCE_HIGH_LIMIT_FEMALE_MIN 0
#define PLATFORM_ACCEPTANCE_HIGH_LIMIT_FEMALE_MAX 0
#define PLATFORM_ACCEPTANCE_HIGH_LIMIT_FEMALE_DEFAULT 1000

#define PLATFORM_DEVICE_NAME_SIZE 8
#define PLATFORM_DEVICE_NAME_DEFAULT "MyDevice"

#define PLATFORM_DEVICE_VERSION_MIN 100
#define PLATFORM_DEVICE_VERSION_MAX 200
#define PLATFORM_DEVICE_VERSION_DEFAULT 100

#define PLATFORM_DEVICE_KIND_MIN 1
#define PLATFORM_DEVICE_KIND_MAX 1000
#define PLATFORM_DEVICE_KIND_DEFAULT 10


//------------------------------------------------------------------------------
//  Picostrain filter
//------------------------------------------------------------------------------

typedef enum {
   PICOSTRAIN_FILTER_OFF,
   PICOSTRAIN_FILTER_SINC3,
   PICOSTRAIN_FILTER_SINC5,
   _PICOSTRAIN_FILTER_LAST
} EPicostrainFilter;

//------------------------------------------------------------------------------
//  Detection mode
//------------------------------------------------------------------------------

typedef enum {
   DETECTION_MODE_ABSOLUTE,
   DETECTION_MODE_RELATIVE,
   _DETECTION_MODE_LAST
} EDetectionMode;

//------------------------------------------------------------------------------
//  Acceptance mode
//------------------------------------------------------------------------------

typedef enum {
   ACCEPTANCE_MODE_OFF,
   ACCEPTANCE_MODE_STEP,
   ACCEPTANCE_MODE_UNISEX,
   ACCEPTANCE_MODE_MIXED,
   ACCEPTANCE_MODE_SINGLE,
   _ACCEPTANCE_MODE_LAST
} EAcceptanceMode;

//------------------------------------------------------------------------------
//  Acceptance step
//------------------------------------------------------------------------------

typedef enum {
   ACCEPTANCE_STEP_ENTER,
   ACCEPTANCE_STEP_LEAVE,
   ACCEPTANCE_STEP_BOTH,
   _ACCEPTANCE_STEP_LAST
} EAcceptanceStep;



//------------------------------------------------------------------------------
//  Data types
//------------------------------------------------------------------------------

typedef byte TDeviceKind;

//------------------------------------------------------------------------------
//  Platform sigma delta
//------------------------------------------------------------------------------

typedef struct {
   byte Chop; // Chopped mode
   byte Sinc3; // SINX3 filter mode
   word Rate; // sampling rate
   word Prefilter; // averaging filter
} TPlatformSigmaDelta;

//------------------------------------------------------------------------------
//  Platform picostrain
//------------------------------------------------------------------------------

typedef struct {
   byte Filter; // OFF/SINC3/SINC5
   byte Accuracy; // accuracy coefficient
   word Rate; // sampling rate
   word Prefilter; // averaging filter
} TPlatformPicostrain;

//------------------------------------------------------------------------------
//  Platform calibration
//------------------------------------------------------------------------------

typedef struct {
   TWeight FullRange; // full range weight
   word Delay; // Wait for stabilisation
   word Duration; // Averaging samples count
} TPlatformCalibration;

//------------------------------------------------------------------------------
//  Platform detection
//------------------------------------------------------------------------------

typedef struct {
   byte Mode; // ABSOLUTE / RELATIVE
   byte AveragingWindow; // Filter
   TWeight AbsoluteRange; // Absolute stabilisation range
   word RelativeRange; // Relative stabilisation range
   byte StableWindow; // Stabilisation Time
   TWeightShort ThresholdWeight; // Minimum difference for valid sample
} TPlatformDetection;

//------------------------------------------------------------------------------
//  Platform acceptance
//------------------------------------------------------------------------------

typedef struct {
   byte Mode; // OFF / STEP / UNISEX / MIXED / SINGLE
   byte Gender; // UNKNOWN / MALE / FEMALE for Single Sex mode
   TWeight LowLimit; // Mixed mode Low Limit Male
   TWeight HighLimit; // Mixed mode High Limit Male
   TWeight LowLimitFemale; // Mixed mode Low Limit Female
   TWeight HighLimitFemale; // Mixed mode High Limit Female
   byte Step; // accept on ENTER / LEAVE / BOTH
} TPlatformAcceptance;

//------------------------------------------------------------------------------
//  Platform device
//------------------------------------------------------------------------------

typedef struct {
   TDeviceVersion Device;
   char Name[ PLATFORM_DEVICE_NAME_SIZE + 1]; // Device name
   word Version; // Device version
   byte _Dummy; // alignment only
   TDeviceKind Kind; // Kind test
} TPlatformDevice;



//------------------------------------------------------------------------------
#endif
