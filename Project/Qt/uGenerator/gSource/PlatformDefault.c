//------------------------------------------------------------------------------
//  Platform sigma delta
//------------------------------------------------------------------------------

TPlatformSigmaDelta PlatformSigmaDeltaDefault = {
   /* Chop */ NO,
   /* Sinc3 */ NO,
   /* Rate */ PLATFORM_SIGMA_DELTA_RATE_DEFAULT,
   /* Prefilter */ PLATFORM_SIGMA_DELTA_PREFILTER_DEFAULT
};

//------------------------------------------------------------------------------
//  Platform picostrain
//------------------------------------------------------------------------------

TPlatformPicostrain PlatformPicostrainDefault = {
   /* Filter */ PICOSTRAIN_FILTER_SINC3,
   /* Accuracy */ PLATFORM_PICOSTRAIN_ACCURACY_DEFAULT,
   /* Rate */ PLATFORM_PICOSTRAIN_RATE_DEFAULT,
   /* Prefilter */ PLATFORM_PICOSTRAIN_PREFILTER_DEFAULT
};

//------------------------------------------------------------------------------
//  Platform calibration
//------------------------------------------------------------------------------

TPlatformCalibration PlatformCalibrationDefault = {
   /* FullRange */ PLATFORM_CALIBRATION_FULL_RANGE_DEFAULT,
   /* Delay */ PLATFORM_CALIBRATION_DELAY_DEFAULT,
   /* Duration */ PLATFORM_CALIBRATION_DURATION_DEFAULT
};

//------------------------------------------------------------------------------
//  Platform detection
//------------------------------------------------------------------------------

TPlatformDetection PlatformDetectionDefault = {
   /* Mode */ DETECTION_MODE_RELATIVE,
   /* AveragingWindow */ PLATFORM_DETECTION_AVERAGING_WINDOW_DEFAULT,
   /* AbsoluteRange */ PLATFORM_DETECTION_ABSOLUTE_RANGE_DEFAULT,
   /* RelativeRange */ PLATFORM_DETECTION_RELATIVE_RANGE_DEFAULT,
   /* StableWindow */ PLATFORM_DETECTION_STABLE_WINDOW_DEFAULT,
   /* ThresholdWeight */ PLATFORM_DETECTION_THRESHOLD_WEIGHT_DEFAULT
};

//------------------------------------------------------------------------------
//  Platform acceptance
//------------------------------------------------------------------------------

TPlatformAcceptance PlatformAcceptanceDefault = {
   /* Mode */ ACCEPTANCE_MODE_UNISEX,
   /* Gender */ GENDER_UNKNOWN,
   /* LowLimit */ PLATFORM_ACCEPTANCE_LOW_LIMIT_DEFAULT,
   /* HighLimit */ PLATFORM_ACCEPTANCE_HIGH_LIMIT_DEFAULT,
   /* LowLimitFemale */ PLATFORM_ACCEPTANCE_LOW_LIMIT_FEMALE_DEFAULT,
   /* HighLimitFemale */ PLATFORM_ACCEPTANCE_HIGH_LIMIT_FEMALE_DEFAULT,
   /* Step */ ACCEPTANCE_STEP_BOTH
};

//------------------------------------------------------------------------------
//  Platform device
//------------------------------------------------------------------------------

TPlatformDevice PlatformDeviceDefault = {
   /* Deviceversion */ DeviceVersion?,
   /* Name */ PLATFORM_DEVICE_NAME_DEFAULT,
   /* Version */ PLATFORM_DEVICE_VERSION_DEFAULT,
   /* _Dummy */ 0,
   /* Kind */ PLATFORM_DEVICE_KIND_DEFAULT
};

