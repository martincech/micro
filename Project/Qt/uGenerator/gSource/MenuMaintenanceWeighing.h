//******************************************************************************
//
//   MenuMaintenanceWeighing.h  Maintenance weighing menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuMaintenanceWeighing_H__
   #define __MenuMaintenanceWeighing_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Platform_H__
   #include "Platform.h"
#endif


void MenuMaintenanceWeighing( void);
// Menu maintenance weighing

#endif
