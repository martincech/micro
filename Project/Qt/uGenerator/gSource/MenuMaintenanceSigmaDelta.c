//******************************************************************************
//
//   MenuMaintenanceSigmaDelta.c  Maintenance sigma delta menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuMaintenanceSigmaDelta.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config.h"               // Project configuration

#include "Platform.h"


static DefMenu( MaintenanceSigmaDeltaMenu)
   STR_CHOP,
   STR_SINC3,
   STR_RATE,
   STR_PREFILTER,
EndMenu()

typedef enum {
   MI_CHOP,
   MI_SINC3,
   MI_RATE,
   MI_PREFILTER
} EMaintenanceSigmaDeltaMenu;

// Local functions :

static void PlatformSigmaDeltaParameters( int Index, int y, TPlatformSigmaDelta *Parameters);
// Draw maintenance sigma delta parameters

//------------------------------------------------------------------------------
//  Menu MaintenanceSigmaDelta
//------------------------------------------------------------------------------

void MenuMaintenanceSigmaDelta( void)
// Edit maintenance sigma delta parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_SIGMA_DELTA, MaintenanceSigmaDeltaMenu, (TMenuItemCb *)PlatformSigmaDeltaParameters, &PlatformSigmaDelta, &MData)){
         ConfigPlatformSigmaDeltaSave();
         return;
      }
      switch( MData.Item){
         case MI_CHOP :
            i = PlatformSigmaDelta.Chop;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            PlatformSigmaDelta.Chop = (byte)i;
            break;

         case MI_SINC3 :
            i = PlatformSigmaDelta.Sinc3;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            PlatformSigmaDelta.Sinc3 = (byte)i;
            break;

         case MI_RATE :
            i = PlatformSigmaDelta.Rate;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_SIGMA_DELTA_RATE_MIN, PLATFORM_SIGMA_DELTA_RATE_MAX, "Hz")){
               break;
            }
            PlatformSigmaDelta.Rate = (word)i;
            break;

         case MI_PREFILTER :
            i = PlatformSigmaDelta.Prefilter;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_SIGMA_DELTA_PREFILTER_MIN, PLATFORM_SIGMA_DELTA_PREFILTER_MAX, 0)){
               break;
            }
            PlatformSigmaDelta.Prefilter = (word)i;
            break;

      }
   }
} // MenuMaintenanceSigmaDelta

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void PlatformSigmaDeltaParameters( int Index, int y, TPlatformSigmaDelta *Parameters)
// Draw maintenance sigma delta parameters
{
   switch( Index){
      case MI_CHOP :
         DLabelEnum( Parameters->Chop, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

      case MI_SINC3 :
         DLabelEnum( Parameters->Sinc3, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

      case MI_RATE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->Rate, "Hz");
         break;

      case MI_PREFILTER :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->Prefilter, 0);
         break;

   }
} // PlatformSigmaDeltaParameters
