//******************************************************************************
//
//   MenuMaintenancePicostrain.h  Maintenance picostrain menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuMaintenancePicostrain_H__
   #define __MenuMaintenancePicostrain_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Platform_H__
   #include "Platform.h"
#endif


void MenuMaintenancePicostrain( void);
// Menu maintenance picostrain

#endif
