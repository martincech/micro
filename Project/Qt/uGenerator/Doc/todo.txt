TODO list uGenerator
---------------------

--1. Opravit CSV parser. Mezi uvozovkami mohou
--   byt vlozene carky. Polozka muze byt bez delimiteru
--   (napr. cisla po exportu z OpenOffice)
   
--2. DictionaryParser.h,c pouzit QVector. Prejmenovat funkce
--   nameAt, itemsAt
   
--3. GeneratorDef.h, DataParser - zavest embedded type (Enum)
--   zde umistit CLASS_TYPE_WEIGHT jako EMBEDDED_TYPE_WEIGHT 
--   a dalsi pripadne zname datove typy (dle projektu). 
--   Zavest CLASS_TYPE_EMBEDDED pro tyto typy.
   
--4. Pri generovani CodeHeader zavest generovani konstant
--   pro stringy :
--   #define PLATFORM_DEVICE_NAME_SIZE  100.
--   Opravit deklaraci stringu v structDeclaration()
   
-- 5. lowLimit, highLimit predelat na konstanty (generovat do
--   headeru, pouzit v menu)
   
-- 6. analogicky pouzit konstanty i pro defaults

7. Units generovat jako stringy STR_UNITS_xxx,
   symboly generovat jako % - PERCENT, �C - DEGREE,
   %% - PROMILE
   
8. Domyslet persistenci pri opusteni menu. Nejspis
   pri volani menu
   
9. Generovani headeru s prilozenim definic konstant
   a enumu. K prislusnemu datovemu typu vlozit
   konstanty a enumy pred definici typedef struct.

10. MenuDialog.c & CodeMenu predelat tak, aby
    funkce menu prebirala parametr :
    MenuXXX( TXXX *Parameters). Editovani struktury
    vola MenuXXX( &Parameters->XXX)
    ?? Nebo volitelne do zahlavi datove struktury doplnit
    klicove slovo 'global' (na miste menu/data).
    Je-li pritomno, parametr se negeneruje, pouziji
    se globalni struktury a teckova notace.

11. Upravit formular - Data Type a Class ma spatnou
    mnemoniku (mozna pridat dalsi sloupec s atributy).
    Napr. klicove slovo struct by mohlo byt ve sloupci
    Class (kvuli sjednoceni s polem struktur).
    Taktez menu/method by mozna melo mit prehozeno
    klicove slovo a nazev metody. Snad i alias.
    U enumu presunout nazev enumu do Data Type
    a datovy typ enumu jako atribut do stranky
    Dictionary.
    
12. Pripravit slovnik uzivatelskych datovych typu, alespon :
    - enum
    - integer (typ, lo/hi limit, default)
    Analogicky k Embedded types by se nacital asi ze souboru
    s pevnym nazvem a adresarem (stejnym s otevrenym XLS).
        
13. Spatne se generuji case labely pro klicove slovo alias
    v Menu (selekce i menu parametry)
    
//14. V definici Defaults chybi prefix 'const' pred nazvem typu
//    struktury

//15. V menu case u prirazeni Data.Item = (typecast)i
//    je chybny typ u WeightShort (TWeight misto TWeightShortGauge)
    
//16. Formatovaci specifikace u typu fixed v menu parameters
//    je chybne %f5.1 ma byt %5.1f
    
//17. Formatovaci specifikace vcetne units - chybi %s pro units.
//    Chybne "%d", spravne "%d %s"
    
18. U promennych typu Weight/Short se v headeru definuji
    konstanty _MIN, _MAX s hodnotu 0. Negenerovat vubec
    (limity hmotnosti jsou zvlast). Zrejme nevyplnene
    sloupce u embedded typu nejak oznacit.
    
19. Je-li definovan ve slovniku identifikator typu int16,
    nenajde se ve slovniku (WeighingConfiguration.xls - Factor)
    
20. Je-li posledni polozka ve strukture SPARE, potom chybne
    definuje enum pro menu a menu items (prida prazdny radek
    a posledni polozka menu items konci carkou)
    
21. V menu pro polozku typu Time neni vyplnen parametr $UITEM$
    (viz DEditText)
    
22. Soubor Defaults.c pro promenne typu enum negeneruje konstanty
    *_DEFAULT. Umistuje do defaults primo konstantu enumu.