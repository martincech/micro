#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTime>
MainWindow::MainWindow(QWidget *parent) :
   QMainWindow(parent),
   ui(new Ui::MainWindow)
{
   ui->setupUi(this);
   socket = NULL;
   on_setport_clicked();
}

MainWindow::~MainWindow()
{
   delete ui;
}


void MainWindow::on_setport_clicked()
{
   if( socket){
      delete( socket);
   }
   socket = new QUdpSocket(this);
   socket->bind(QHostAddress::Any, ui->port->text().toInt());
   connect( socket, SIGNAL(readyRead()),this, SLOT(readData()));
   ui->text->appendPlainText("Listen on: "+ui->port->text());
}

void MainWindow::readData()
{
   QByteArray data;
   QHostAddress sender;
   quint16 senderPort;
   data.resize(socket->pendingDatagramSize());
   socket->readDatagram(data.data(), data.size(), &sender, &senderPort);

   QString header = QString("%1 <%2:%3>: ").arg(QTime::currentTime().toString(QString("hh:mm:ss.zzz"))).arg(sender.toString()).arg(senderPort);
   ui->text->appendPlainText( header + QString(data.toHex()) + "  |  ASCII: "+ QString(data));
   //send back
   socket->writeDatagram(data, sender, senderPort);
}

void MainWindow::on_pushButton_clicked()
{
    ui->text->clear();
}
