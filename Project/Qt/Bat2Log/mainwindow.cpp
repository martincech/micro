#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "Memory/Nvm.h"
#include "Log/Log.h"
#include "Log/Logger.h"

#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
   QMainWindow(parent),
   ui(new Ui::MainWindow)
{
   ui->setupUi(this);
}

MainWindow::~MainWindow()
{
   delete ui;
}

void MainWindow::on_FileOpenPushButton_clicked()
{
   FileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Files (*.bin)"));
   ui->FileNameLabel->setText(FileName);

   NvmInit();
   NvmSetup( FileName.toAscii());

   LogInit();

   int Count = LogCount();
   TLogItem Item;
   UDateTime DateTime;
   QString Module;
   ui->LogTextEdit->clear();

   for(int i = 0 ; i < Count ; i++) {
      if(!LogGet(i, &Item)) {
         return;
      }

      uDateTime( &DateTime, Item.Timestamp);


      switch(LogModuleGet(Item.Code)) {
         case LOG_MODULE_EFS:
            Module = "EFS";
            break;

         default:
            Module = "unknown";
            break;
      }

      ui->LogTextEdit->appendPlainText(
      QString("%1.%2.%3 %4:%5:%6 %7 %8")
            .arg(DateTime.Date.Day)
            .arg(DateTime.Date.Month)
            .arg(DateTime.Date.Year)
            .arg(DateTime.Time.Hour)
            .arg(DateTime.Time.Min)
            .arg(DateTime.Time.Sec)
            .arg(Module)
            .arg(Item.Code)
      );
   }
}
