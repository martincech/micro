#-------------------------------------------------
#
# Project created by QtCreator 2013-11-18T07:57:40
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = EepromFlashTest
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += debug

TEMPLATE = app

INCLUDEPATH += .
INCLUDEPATH += ../../../Library/Kinetis
INCLUDEPATH += ../../../Library/Qt
INCLUDEPATH += ../../../Library/Micro
INCLUDEPATH += ../../../Library/Device
INCLUDEPATH += ../../../Library/Win32

SOURCES += main.cpp \
    ../../../Library/Device/Eeprom/EepromFlash.c \
    ../../../Library/Qt/Flash/IFlash.cpp \
    ../../../Library/Qt/System/SysClock.c \
    ../../../Library/VisualC/Performance/Performance.c \
    ../../../Library/Micro/Time/uTime.c \
    ../../../Library/Micro/Time/uDateTime.c \
    ../../../Library/Micro/Time/uDate.c \
    ../../../Library/Micro/Time/uClock.c \
    ../../../Library/Win32/Random/Random.c

HEADERS += \
    Hardware.h
