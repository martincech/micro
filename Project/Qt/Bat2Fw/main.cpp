#include <QCoreApplication>
#include "FwCoder/FwCoder.h"
#include "Remote/SocketIfMsdQ.h"
#include <QFile>
#include <QTextStream>
#include "Memory/FileRemote.h"
#include "File/Efs.h"
#include "Fw/FwStorage.h"

#define PATH "D:\\MICRO\\Project\\Kinetis\\Bat2Compact\\Bat2Compact THUMB Release\\"
#define FILE_BIN PATH"Bat2Compact.bin"
#define FILE_HEX PATH"Bat2Compact.hex"


int main(int argc, char *argv[])
{
    SocketIfMsd *Socket = new SocketIfMsd();
    printf("Converting %s\n", FILE_HEX);
    TFwCoder *Coder = new TFwCoder();
    if( !Coder->Encode(FILE_HEX, FILE_BIN)){
       printf("Cant convert!\n");
       getchar();
       return 0;
    }


    FILE *f;
    f = fopen( FILE_BIN, "rb");
    if( !f){
       return( false);
    }

    fseek(f, 0L, SEEK_END);
    int Size = ftell(f);
    fseek(f, 0L, SEEK_SET);
    byte *Data = (byte *)malloc(Size);

    if(fread(Data, 1, Size, f) != Size) {
       printf("Cant convert!\n");
       getchar();
       return 0;
    }

    fclose(f);
    EfsRoot(PATH);

    if(!Socket->OpenForSendCmd()){
       printf("Cant convert!\n");
       free(Data);
       return 0;
    }

    if(!FileRemoteSetup( Socket)){
       printf("Cant convert!\n");
       free(Data);
       return 0;
    }

    if(!FwStorageOpen()){
       printf("Cant convert!\n");
       free(Data);
       return 0;
    }

    if(!FwStorageWrite(0, Data, Size)){
       printf("Cant convert!\n");
       free(Data);
       return 0;
    }

    FwStorageConfirm();

    FwStorageClose();

    Socket->Close();

    free(Data);
    printf("File converted, writen to: %s\n",PATH"Bat2Fw.bin");
    getchar();
    return 1;
}
