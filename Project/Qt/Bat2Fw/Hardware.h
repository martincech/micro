#ifndef HARDWARE_H
#define HARDWARE_H

#include "Unisys/Uni.h"

#define FLASH_SIZE  (8 * 1024 * 1024)

#define NVM_SIZE        FLASH_SIZE

#endif // HARDWARE_H
