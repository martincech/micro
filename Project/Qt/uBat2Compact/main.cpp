//******************************************************************************
//
//    main.cpp     uBat2Compact main
//    Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include <QtGui/QApplication>
#include <QDir>

#include "uSimulator/mainwindow.h"
#include "Platform/Wepl.h"
#include "Memory/Nvm.h"
#include "File/Efs.h"
#include "Uart/WinUart.h"
#include "Uart/UartParameters.h"
#include "Uart/UartModem.h"
#include "Gsm/Gsm.h"

#define NVM_FILE_NAME   "NvmMemory.bin"
#define EFS_NAME        "Root"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.setWindowTitle( "Bat2 Compact");
    w.show();

    // serial initialization :
    const char *SerialName = "COM1";
    if( argc > 1){
       SerialName = argv[ 1];
    }
    WeplInit();
    UartParameters *parameters = new UartParameters( &w);
    parameters->setPortName( SerialName);
    parameters->setBaudRate( 9600);
    WeplSetup( 0, parameters);

    // nonvolatile memory initialization :
    NvmInit();
    QString nvmPath;
    nvmPath  = QDir::currentPath();         // get working directory
    nvmPath += QString( "/" NVM_FILE_NAME);
    NvmSetup( nvmPath.toAscii());

    // external file system :
    QString efsPath;
    efsPath  = QDir::currentPath();         // get working directory
    efsPath += QString( "/" EFS_NAME);
    EfsRoot( efsPath.toAscii());
/*
    // GSM modem :
    const char *GsmName = "COM10";
    if( argc > 2){
       GsmName = argv[ 2];
    }
    WinUart *uart = new WinUart;
    uart->setPort( GsmName);
    UartModemSetup( uart, 0);
*/
    a.exec();         // Qt main loop

    NvmShutdown( nvmPath.toAscii());   
    return( 0);
} // main
