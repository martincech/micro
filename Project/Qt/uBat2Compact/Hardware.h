//*****************************************************************************
//
//    Hardware.h  Bat2 compact PC hardware definitions
//    Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "Unisys/Uni.h"

//#define GuiTaskScheduler()   SysScheduler()

//-----------------------------------------------------------------------------
// Timers
//-----------------------------------------------------------------------------

#define TIMER_FAST_PERIOD 10                     // System timer 'fast' tick period [ms]
#define TIMER_SLOW_PERIOD 500                    // System timer 'slow' tick period [ms]
#define TIMER_SEC_PERIOD  1000         // System second timer period [ms]
#define TIMER_FLASH1      (300/TIMER_FAST_PERIOD)                     // Flash 1 delay [ms]
#define TIMER_FLASH2      (200/TIMER_FAST_PERIOD)                     // Flash 2 delay [ms]
#define TIMER_TIMEOUT     180                    // Inactivity timeout [s]
#define TIMER_KEY_POS_SHIFT 4000/(TIMER_FLASH1 * TIMER_FAST_PERIOD)
//-----------------------------------------------------------------------------
// Graphics
//-----------------------------------------------------------------------------

#define G_WIDTH          240           // display width (X)
#define G_HEIGHT         160           // display height (Y)
#define G_PLANES           2           // color planes count

#define GRAPHIC_CONIO             1    // enable conio.h
#define PRINTF_STRING             1    // enable string resource
#define PRINTF_BUFFER             1    // use bprintf
#define GRAPHIC_LETTER_CENTERING  1    // fixed text letter centering
//#define CONIO_FLUSH             1    // automatically flush conio operations

//-----------------------------------------------------------------------------
// Nonvolatile memory
//-----------------------------------------------------------------------------

#define NVM_SIZE   (8 * 1024 * 1024)

#define FLASH_SIZE  NVM_SIZE

//-----------------------------------------------------------------------------
// Keyboard
//-----------------------------------------------------------------------------

// keyboard/event codes :
#define _K_SYSTEM_BASE            1    // skip K_NULL
#include "System/SystemKey.h"          // system events
#define _K_KEYBOARD_BASE          _K_SYSTEM_LAST
#include "Kbd/KbxKey.h"                // keyboard and user events
#define _K_USB_BASE               _K_KEY_LAST
#include "Usb/UsbKey.h"

// timing constants [ms] :
#define KBD_DEBOUNCE           20      // delay after first touch
#define KBD_AUTOREPEAT_START   300     // autorepeat delay
#define KBD_AUTOREPEAT_SPEED   200     // autorepeat repeat speed

#define SYSTEM_KEYBOARD        1       // service keyboard with default scheduler

//-----------------------------------------------------------------------------
//  GSM
//-----------------------------------------------------------------------------
#define CONNECTION_INTERNET_TYPE    0   // 0 - GSM module, 1 - MII module
#define GSM_BAUD_RATE        115200

// SMS expiration :
#define GsmSmsExpirationMins( m)   ((m)>0?((m)-1)/5:0)               // min 0  max 720 (0..143)
#define GsmSmsExpirationHours( h)  (((h) - 12) * 2 + 143)  // min 13 max 24  (144..167)
#define GsmSmsExpirationDays( d)   ((d) + 166)             // min 2  max 30  (168..196)
#define GsmSmsExpirationWeeks( w)  ((w) + 192)             // min 5  max 58  (197..255)
#define GsmSmsExpirationMax()      255                     // maximalni expirace

#define GsmPowerOn()
#define GsmPowerOff()

#define GsmPortOff()

#define GSM_SMS_EXPIRATION            GsmSmsExpirationDays(4)      // pozadovana expirace


//#ifdef __cplusplus
//   extern "C" {
//#endif
//   void GsmPowerOn( void);
//   void GsmPowerOff( void);

//#ifdef __cplusplus
//   }
//#endif


#define PowerPortInit()

#define PowerUsbh3v3On()
#define PowerUsbh3v3Off()

#define PowerUsbh5vOn()
#define PowerUsbh5vOff()

//------------------------------------------------------------------------------
// IADC parameters
//------------------------------------------------------------------------------

// ADC maximum value :
#define IADC_VALUE_MAX  0x3FF

// ADC reference voltage :
#define IADC_REFERENCE_VOLTAGE 3300      // [mV]

#define IADC_POWER_VOLTAGE  0

//-----------------------------------------------------------------------------

#include "DisplayLayout.h"
#include "Country/Country.h"

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Samples FIFO
//-----------------------------------------------------------------------------

#define FIFO_SIZE       65536            // samples FIFO capacity

//-----------------------------------------------------------------------------
// Diagnostic
//-----------------------------------------------------------------------------

#define DIAGNOSTIC_FRAME_COUNT  10    // Frame FIFO capacity

//-----------------------------------------------------------------------------
// Calibration
//-----------------------------------------------------------------------------

#include "Platform/PlatformDef.h"

#define CALIBRATION_POINTS_MAX   PLATFORM_CALIBRATION_POINTS_MAX

//-----------------------------------------------------------------------------
// Platform
//-----------------------------------------------------------------------------

#define OPTION_SIGMA_DELTA
#define OPTION_PICOSTRAIN
#define OPTION_SIMULATION
#define OPTION_COMPACT

//-----------------------------------------------------------------------------
// ADC filtering
//-----------------------------------------------------------------------------

#define FILTER_MAX_AVERAGING  50       // max. width of averaging window
#define FILTER_SLOW_RESTART    1       // slow stabilisation after restart

// Basic data types :
typedef byte TSamplesCount;            // samples counter

#define PowerUsbh3v3On()
#define PowerUsbh5vOn()
#define PowerUsbh3v3Off()
#define PowerUsbh5vOff()

//-----------------------------------------------------------------------------
// Display
//-----------------------------------------------------------------------------

// chicken icon :
#define DisplayWeighingOn()
#define DisplayWeighingOff()

// accumulator icon :
#define DisplayChargerOn()
#define DisplayChargerOff()

// antenna icon :
#define DisplaySignalOn()
#define DisplaySignalOff()

// load icon :
#define DisplayCalibrationOn()
#define DisplayCalibrationOff()

// all icon off :
#define DisplayModeOff()

// red LED :
#define DisplayErrorOn()
#define DisplayErrorOff()

// green LED :
#define DisplayOkOn()
#define DisplayOkOff()

// blue LED :
#define DisplayCommunicationOn()
#define DisplayCommunicationOff()

#define DisplayStatusOff()

#define DATE_YEAR_YYYY
//------------------------------------------------------------------------------
//  RS485
//------------------------------------------------------------------------------
#define RS485_0_PHYSICAL_DEVICE 0
#define RS485_1_PHYSICAL_DEVICE 0

#define BAT2_ETHERNET_MODIFICATION 0x0001
#define BAT2_GSM_MODIFICATION      0x0002
#define BAT2_WIFI_MODIFICATION     0x0004
#define BAT2_ZIGBEE_MODIFICATION   0x0008
#define BAT2_RS485_I1_MODIFICATION 0x0010
#define BAT2_RS485_I2_MODIFICATION 0x0020
#define BAT2_MODIFICATION (BAT2_RS485_I1_MODIFICATION | BAT2_GSM_MODIFICATION)
#endif
