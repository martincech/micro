//******************************************************************************
//
//   Main.cpp     Application main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <QtGui/QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
   QApplication a(argc, argv);
   MainWindow w;
   w.show();
   
   return a.exec();
}
