//******************************************************************************
//
//  Str.h          String codes
//  Version 1.0    (c) Robot
//
//******************************************************************************

#ifndef __Str_H__
   #define __Str_H__

#ifndef __StrDef_H__
   #include "String/StrDef.h"
#endif

#define STR_NULL (char *)0 // undefined string
//>>> #include 'Button.str'
//-----------------------------------------------------------------------------
// Message Box Buttons
//-----------------------------------------------------------------------------

#define STR_BTN_OK           (char *)1
#define STR_BTN_YES          (char *)2
#define STR_BTN_NO           (char *)3
#define STR_BTN_CANCEL       (char *)4
#define STR_BTN_EXIT         (char *)5
#define STR_BTN_SELECT       (char *)6

//>>> #include 'Edit.str'
//-----------------------------------------------------------------------------
// Error box title
//-----------------------------------------------------------------------------

#define STR_ERROR            (char *)7

//-----------------------------------------------------------------------------
// Input box
//-----------------------------------------------------------------------------

#define STR_OUT_OF_LIMITS    (char *)8

//-----------------------------------------------------------------------------
// Text box
//-----------------------------------------------------------------------------

#define STR_STRING_EMPTY     (char *)9

//-----------------------------------------------------------------------------
// Wait box
//-----------------------------------------------------------------------------

#define STR_WAIT             (char *)10
//<<< 'Edit.str'
//<<< 'Button.str'

//-----------------------------------------------------------------------------
// Standard title :
//-----------------------------------------------------------------------------

#define STR_ERROR            (char *)11

#define ENUM_YES_NO          (char *)12
#define STR_NO               (char *)12
#define STR_YES              (char *)13

#define STR_DEMO             (char *)14	// demonstracni string

// system :
#define _STR_LAST (char *)15 // strings count

#endif
