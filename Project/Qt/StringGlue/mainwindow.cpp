//******************************************************************************
//
//   MainWindow.cpp  String Glue main window
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPlainTextEdit>
#include <QMessageBox>
#include <QFileDialog>
#include <QDir>
#include <QFile>
#include <QSettings>
#include "Parse/TextFile.h"
#include "StringParser.h"

// page indexes :
typedef enum {
   PAGE_STRINGS,
   PAGE_HEADER,
   PAGE_SOURCE,
   PAGE_CSV,
   _PAGE_LAST
} EPages;

#define STRINGS_SUFFIX  ".txt"
#define HEADER_SUFFIX   ".h"
#define SOURCE_SUFFIX   ".c"
#define CSV_SUFFIX      ".csv"

#define AVR32_STRING_INDEX  0x40000000     // AVR32 string index base

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------

MainWindow::MainWindow(QWidget *parent) :
   QMainWindow(parent),
   ui(new Ui::MainWindow)
{
   ui->setupUi(this);
} // MainWindow

//------------------------------------------------------------------------------
//  Destructor
//------------------------------------------------------------------------------

MainWindow::~MainWindow()
{
   delete ui;
} // ~MainWindow

//------------------------------------------------------------------------------
//  Open
//------------------------------------------------------------------------------

void MainWindow::on_actionOpen_triggered()
{
   QSettings settings("Veit", "StringGlue");
   fileName = settings.value("FilePath").toString();
   // select file :
   fileName = QFileDialog::getOpenFileName( this, "Open file",
                                            fileName,
                                            "String files (*.str);;Text files (*.txt);;All files (*.*)");
   if( fileName.isNull()){
      return;
   }
   settings.setValue("FilePath", fileName);
   QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));    // hourglass cursor
   // parse file :
   ui->textStrings->clear();
   ui->textHeader->clear();
   ui->textSource->clear();
   ui->textCsv->clear();
   StringParser parser;
   parser.setStringIndex( 0);
   if( ui->actionAvr32->isChecked()){
      parser.setStringIndex( AVR32_STRING_INDEX);
   }
   parser.generate( fileName);
   ui->textStrings->setPlainText( parser.strings());
   ui->textHeader->setPlainText( parser.header());
   ui->textSource->setPlainText( parser.source());
   ui->textCsv->setPlainText( parser.csv());
   QApplication::restoreOverrideCursor();                       // default cursor
} // on_actionOpen_triggered

//------------------------------------------------------------------------------
//  Save
//------------------------------------------------------------------------------

void MainWindow::on_actionSave_triggered()
{
   QString saveName;
   QFileInfo saveInfo( fileName);
   saveName = saveInfo.path() + QString( QDir::separator()) + saveInfo.baseName();
   saveName = QFileDialog::getSaveFileName( this, "Save file",
                                            saveName,
                                            "All files (*.*)");
   if( saveName.isNull()){
      return;
   }
   QFileInfo fileInfo( saveName);
   TextFile  file;
   // save file :
   QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));    // hourglass cursor
   // save strings :
   QString  stringsName;
   stringsName = fileInfo.path() + QString( QDir::separator()) + fileInfo.baseName() + QString( STRINGS_SUFFIX);
   file.setFileName( stringsName);
   file.save( ui->textStrings->toPlainText());
   // save header :
   QString  headerName;
   headerName = fileInfo.path() + QString( QDir::separator()) + fileInfo.baseName() + QString( HEADER_SUFFIX);
   file.setFileName( headerName);
   file.save( ui->textHeader->toPlainText());
   // save source :
   QString  sourceName;
   sourceName = fileInfo.path() + QString( QDir::separator()) + fileInfo.baseName() + QString( SOURCE_SUFFIX);
   file.setFileName( sourceName);
   file.save( ui->textSource->toPlainText());
   // save CSV :
   QString  csvName;
   csvName = fileInfo.path() + QString( QDir::separator()) + fileInfo.baseName() + QString( CSV_SUFFIX);
   file.setFileName( csvName);
   file.save( ui->textCsv->toPlainText());
   QApplication::restoreOverrideCursor();                       // default cursor
} // on_actionSave_triggered

//------------------------------------------------------------------------------
//   Strings
//------------------------------------------------------------------------------

void MainWindow::on_actionStrings_triggered()
{
   ui->pageSelector->setCurrentIndex( PAGE_STRINGS);
} // on_actionStrings_triggered

//------------------------------------------------------------------------------
//   Header
//------------------------------------------------------------------------------

void MainWindow::on_actionHeader_triggered()
{
   ui->pageSelector->setCurrentIndex( PAGE_HEADER);
} // on_actionHeader_triggered

//------------------------------------------------------------------------------
//   Source
//------------------------------------------------------------------------------

void MainWindow::on_actionSource_triggered()
{
   ui->pageSelector->setCurrentIndex( PAGE_SOURCE);
} // on_actionSource_triggered

//------------------------------------------------------------------------------
//   CSV
//------------------------------------------------------------------------------

void MainWindow::on_actionCsv_triggered()
{
   ui->pageSelector->setCurrentIndex( PAGE_CSV);
} // on_actionCsv_triggered

//------------------------------------------------------------------------------
//   AVR32
//------------------------------------------------------------------------------

void MainWindow::on_actionAvr32_triggered()
{

} // on_actionAvr32_triggered
