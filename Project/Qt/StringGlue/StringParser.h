//******************************************************************************
//
//   StringParser.h  String list parser
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef STRINGPARSER_H
#define STRINGPARSER_H

#include <QString>
#include <QMap>
#include "Parse/Lexer.h"

//------------------------------------------------------------------------------
//   String Parser
//------------------------------------------------------------------------------

class StringParser
{
public:
   StringParser();

   void parse( QString text);
   // Returns parsed <text>

   void parseFile( QString fileName);
   // Returns parsed <fileName>

   void generate( QString fileName);
   // Generate string files of <fileName> source

   QString strings();
   // Returns generated strings

   QString header();
   // Returns generated header

   QString source();
   // Returns generated source

   QString csv();
   // Returns generated CSV

   int stringIndex();
   // Returns current string index

   void setStringIndex( int index);
   // Set string base as <index>

//------------------------------------------------------------------------------
private :
   Lexer              _lexer;
   int                _index;
   const LexicalUnit *_unit;
   QString            _strings;
   QString            _header;
   QString            _source;
   QString            _csv;
   int                _stringIndex;
   static QList<QString> _duplicationList;
   bool              _insideEnum;
   void parseStatement();
   // Returns code for statement

   void parseInclude();
   // Returns code for include statement

   void parseDefinition( bool ignoreDuplication);
   // Returns code for string definition

   void parseEnum();
   // Returns code for enum definition

   void nextSymbol();
   // Move to next symbol

   void skipSpace();
   // Skip optional space

   void skipLine();
   // Skip up to end of line

   void error();
   // Error message for current unit

   void fileError( QString fileName);
   // Error message for <fileName> not found

   //------------------------------------------------
   void generateProlog();
   // Generate output files prolog

   void generateEpilog();
   // Generate output files epilog

   void generateEnum( QString name);
   // Generate enum <name>

   void generateString( QString name, QString value, QString comment);
   // Generate string with <name>,<value> and <comment>

   void generateComment( QString text);
   // Generate comment with <text>

   void generateNewLine();
   // Generate new line

}; // StringParser

#endif // STRINGPARSER_H
