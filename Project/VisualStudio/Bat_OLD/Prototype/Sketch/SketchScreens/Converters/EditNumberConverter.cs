﻿using System;
using System.Windows.Data;

namespace SketchScreens.Converters
{
   public class EditNumberConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         int result = System.Convert.ToInt32(value);
         return result + 1;
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}