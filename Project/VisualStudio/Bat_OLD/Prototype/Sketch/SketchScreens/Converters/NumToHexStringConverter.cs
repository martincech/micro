﻿using System;
using System.Windows.Data;

namespace SketchScreens.Converters
{
   public class NumToHexStringConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         if (targetType == typeof(String) && value != null)
         {
            int val = System.Convert.ToInt32(value);
            return String.Format("{0:X}", val);
         }
         return null;
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
