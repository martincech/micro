﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SketchScreens
{
	/// <summary>
	/// Interaction logic for Curves.xaml
	/// </summary>
	public partial class Curves : UserControl
	{
		public Curves()
		{
			this.InitializeComponent();
		}

        private void toGrowthMaleCurveButton_Click(object sender, RoutedEventArgs e)
        {
           var res = VisualStateManager.GoToState(LayoutRoot, "GrowthMaleCurve_state", true);
           Console.WriteLine("---------------------------: {0}", res);
        }
	}
}