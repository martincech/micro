﻿using System;
using System.Linq;
using System.Globalization;
using Common.Library.Observable;
using Desktop.Client.NavigationService.Interface;
using Common.Desktop.Presentation;
using System.Collections.ObjectModel;

namespace Desktop.Client.NavigationService.Aplications
{
   public class NavigationNode : ValidatableObservableObject, IFormattable, INavigationNode
   {

      #region Private properties

      private string name;
      private double order;
      private ObservableCollection<NavigationNode> childViewNodes;
      private Action showAction;
      private Action closeAction;

      private IView view;
      #endregion

      /// <summary>
      /// Default constructor
      /// </summary>
      public NavigationNode()
         : this("EmptyNode",double.MaxValue,null,null,null)
      {
      }

      /// <summary>
      /// Create navigation node.
      /// </summary>
      /// <param name="name">Name of node</param>
      /// <param name="order">Order inside group</param>
      public NavigationNode(string name, double order, IView view, Action showAction, Action closeAction)
      {
         Name = name;
         Order = order;
         this.view = view;
         childViewNodes = new ObservableCollection<NavigationNode>();         
         this.ShowAction = showAction;
         this.CloseAction = closeAction;
      }

      /// <summary>
      /// Name of navigation node
      /// </summary>
      public string Name
      {
         get { return name; }
         set { SetProperty(ref name, value); }
      }

      /// <summary>
      /// Current view of this node
      /// </summary>
      /// <summary>
      /// Order inside group.
      /// </summary>
      public double Order
      {
         get { return order; }
         set { SetProperty(ref order, value); }
      }

      /// <summary>
      /// Node view
      /// </summary>
      public IView View
      {
         get { return view; }
         set { SetProperty(ref view, value); }
      }

      /// <summary>
      /// Collection of child nodes
      /// </summary>
      public ObservableCollection<NavigationNode> ChildViewNodes
      {
         get { return childViewNodes; }
         set { SetProperty(ref childViewNodes, value); }
      }

      /// <summary>
      /// Action to execute when view of this node is going to show
      /// </summary>
      public Action ShowAction
      {
         get { return showAction; }
         set { SetProperty(ref showAction, value); }
      }

      /// <summary>
      /// Action to execute when view of this node is goning to hide(close) 
      /// </summary>
      public Action CloseAction
      {
         get { return closeAction; }
         set { SetProperty(ref closeAction, value); }
      }

      /// <summary>
      /// Add new view node to node tree
      /// </summary>
      /// <param name="parent">parent node</param>
      /// <param name="nodeName">name of node</param>
      /// <param name="order">order</param>
      /// <param name="nodeView">view</param>
      /// <param name="showAction">show action</param>
      /// <param name="closeAction">close action</param>
      /// <returns>added node</returns>
      public INavigationNode AddViewNode(INavigationNode parent, string nodeName, double order, IView nodeView, Action showAction, Action closeAction)
      {

         if (parent == this)
         {
            NavigationNode nn = new NavigationNode(nodeName, order, nodeView, showAction, closeAction);

            childViewNodes.Add(nn);
            ObservableCollection<NavigationNode> ob = new ObservableCollection<NavigationNode>(childViewNodes.OrderBy(p => p.Order));
            childViewNodes.Clear();
            foreach (NavigationNode node in ob)
            {
               childViewNodes.Add(node);
            }
            return nn;
         }
         else
         {
            var vn = parent as NavigationNode;
            if (vn != null)
            {
               return vn.AddViewNode(parent, nodeName, order, nodeView, showAction, closeAction);
            }
         }
         return null;
      }

      /// <summary>
      /// Delete node and its children from tree node
      /// </summary>
      /// <param name="node">node to remove</param>
      /// <returns>true if node deleted / false if node not found</returns>
      public bool DeleteViewNode(INavigationNode node)
      {
       
         NavigationNode delNode = ChildViewNodes.Where(p => p == node).FirstOrDefault();
         if(delNode != null)
         {
            childViewNodes.Remove(delNode);
            return true;
         }
         else
         {
            bool deleted = false;
            foreach (NavigationNode delN in ChildViewNodes) 
            {
               deleted = delN.DeleteViewNode(node);
               if (deleted) 
               {
                  return true;
               }
            }
            return false;
         }
      }

      /// <summary>
      /// Find node by name
      /// </summary>
      /// <param name="name">name of node</param>
      /// <returns>node</returns>
      public INavigationNode GetNode(string name)
      {
         NavigationNode svn;
         svn = childViewNodes.Where(p => p.Name == name).FirstOrDefault();
         if (svn != null)
         {
            return svn;
         }
         foreach (NavigationNode sVN in childViewNodes)
         {
            svn = sVN.GetNode(name) as NavigationNode;
            if (svn != null)
            {
               return svn;
            }
         }
         return null;
      }   


      #region Implementation of IFormattable

      #region Overrides of Object

      /// <summary>
      /// Returns a string that represents the current object.
      /// </summary>
      /// <returns>
      /// A string that represents the current object.
      /// </returns>
      public override string ToString()
      {
         return ToString("", CultureInfo.CurrentCulture);
      }

      #endregion

      public string ToString(string format, IFormatProvider formatProvider)
      {
         return Name;
      }

      #endregion

   }
}
