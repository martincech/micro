using System;
using Common.Desktop.Applications;
using Common.Desktop.Presentation;
using Desktop.Client.NavigationService.Interface;

namespace Desktop.Client.NavigationService.Aplications
{
   public abstract class NavigationServiceViewModel : ViewModel<NavigationNode>, INavigationService
   {
      #region Implementation of INavigationService

      /// <summary>
      /// Adds a navigation node in the navigation view.
      /// </summary>
      /// <param name="name">The name of the node.</param>
      /// <param name="showView">The <see cref="IView"/> to be visible when node is selected</param>
      /// <param name="order">The order defines the position in the group. The navigation list is ordered from lower to higher numbers.</param>
      /// <param name="parent">The parent navigation node of this node</param>
      /// <param name="showAction">The show action which is called when the user selects the node.</param>
      /// <param name="closeAction">The close action which is called when the node is deselected.</param>
      /// <returns>The created navigation node.</returns>
      public abstract INavigationNode AddNavigationNode(string name, double? order = null, IView showView = null,
         INavigationNode parent = null, Action showAction = null, Action closeAction = null);

      /// <summary>
      /// Removes previously added navigation node. When <see cref="node"/> has siblings then those siblings are removed as well.
      /// </summary>
      /// <param name="node">Existing node to be removed</param>
      public abstract void RemoveNavigationNode(INavigationNode node);

      /// <summary>
      /// Currently selected node by user
      /// </summary>
      public abstract INavigationNode SelectedNode { get; }

      #endregion
   }
}