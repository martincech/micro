﻿using System;
using System.Linq;
using System.Windows.Input;
using Common.Desktop.Applications;
using Desktop.Client.NavigationService.Interface;
using Common.Desktop.Presentation;
using System.Collections.ObjectModel;

namespace Desktop.Client.NavigationService.Aplications
{
   public class NavigationNodeViewModel : NavigationServiceViewModel
   {
      #region Private prop
      private RelayCommand<INavigationNode> selectViewCommand;      
      #endregion

      public const string ROOT_NODE_NAME = "NavNode";


      #region Public property

     

      /// <summary>
      /// Currently selected node by user
      /// </summary>
      public override INavigationNode SelectedNode
      { 
         get { return Model; }
       
      }

      /// <summary>
      /// Command to change active node(view)
      /// </summary>
      public ICommand SelectViewCommand
      {
         get
         {
            if (selectViewCommand == null)
            {
               selectViewCommand = new RelayCommand<INavigationNode>(node =>
               {

                  NavigationNode gvn = (NavigationNode)node;
                  if (gvn != null)
                  {
                     if (gvn.View == null)
                     {
                        var newGVN = GetFirstNodeWithView(gvn.ChildViewNodes);
                        if (newGVN != null) 
                        {
                           gvn = newGVN; 
                        }
                     }
                  }
                  else
                  {
                     gvn = GetFirstNodeWithView(Model.ChildViewNodes);
                     if (gvn == null)
                     {
                        gvn = (NavigationNode)node;
                     }
                  }
                  if (Model != null && Model != node) 
                  {
                    
                     if (Model.CloseAction != null)
                     {
                        Model.CloseAction();
                     }
                  }
                  Model = gvn as NavigationNode;
                  if (gvn != null)
                  {
                     if (gvn.ShowAction != null)
                     {
                        gvn.ShowAction();
                     }
                  }
                  UpdateCommands();
               },
               node => SelectedNode != node);
            }
            return selectViewCommand;
         }
      }

      #endregion

      /// <summary>
      /// Find node by name
      /// </summary>
      /// <param name="name">name of node</param>
      /// <returns>node</returns>
      public INavigationNode GetNode(string name)
      {
         INavigationNode svn;
         svn = Models.Where(p => p.Name == name).FirstOrDefault();
         if (svn == null) 
         {
            foreach(NavigationNode node in Models)
            {
               svn = node.GetNode(name);
               if (svn != null) 
               {
                  break;
               }
            }
         }
         return svn;
      }

      #region Implementation of INavigationService

      /// <summary>
      /// Adds a navigation node in the navigation view.
      /// </summary>
      /// <param name="name">The name of the node.</param>
      /// <param name="showView">The <see cref="IView"/> to be visible when node is selected</param>
      /// <param name="order">The order defines the position in the group. The navigation list is ordered from lower to higher numbers.</param>
      /// <param name="parent">The parent navigation node of this node</param>
      /// <param name="showAction">The show action which is called when the user selects the node.</param>
      /// <param name="closeAction">The close action which is called when the node is deselected.</param>
      /// <returns>The created navigation node.</returns>
      public override INavigationNode AddNavigationNode(string name, double? order = null, IView showView = null,
         INavigationNode parent = null, Action showAction = null, Action closeAction = null)
      {
         NavigationNode newNode;
         if (parent == null)
         {
            newNode = new NavigationNode(name, order ?? double.MaxValue, showView, showAction, closeAction);
            Models.Add(newNode);

            ObservableCollection<NavigationNode> ob = new ObservableCollection<NavigationNode>(Models.OrderBy(p => p.Order));
            Models.Clear();
            foreach (NavigationNode node in ob)
            {
               Models.Add(node);
            }
            return newNode;
         }
         else
         {
            return (parent as NavigationNode).AddViewNode(parent, name, order ?? double.MaxValue, showView, showAction, closeAction);
         }            
      }

      /// <summary>
      /// Removes previously added navigation node. When <see cref="node"/> has siblings then those siblings are removed as well.
      /// </summary>
      /// <param name="node">Existing node to be removed</param>
      public override void RemoveNavigationNode(INavigationNode node)
      {
         if (Models.Contains(node)) 
         {
            Models.Remove(node as NavigationNode);
         } 
         else 
         {
            foreach (NavigationNode parentNode in Models)
            {
               if (parentNode.DeleteViewNode(node)) 
               {
                  return;
               } 
            }
         }
         
      }
      #endregion


      #region Private helpers

      /// <summary>
      /// Find first child node with existing view  
      /// </summary>
      /// <param name="gvn"></param>
      /// <returns></returns>
      private NavigationNode GetFirstNodeWithView(ObservableCollection<NavigationNode> gvn)
      {
         if (gvn == null) 
         {
            return null;
         }
         foreach (NavigationNode node in gvn) 
         {
            if (node.View != null)
            {
               return node;
            }
            return GetFirstNodeWithView(node.ChildViewNodes);
         }
         return null;
      }

      /// <summary>
      /// Update command
      /// </summary>
      private void UpdateCommands()
      {
         selectViewCommand.RaiseCanExecuteChanged();
      }

      #endregion
   }
}
