﻿using System.Windows;
using Desktop.Client.NavigationService.Aplications;
using Desktop.Client.NavigationService.Interface;
using System.Windows.Controls;
using System;
using System.Windows.Controls.Primitives;
using System.Collections.Generic;

namespace Desktop.Client.NavigationService.Presentations
{
   /// <summary>
   /// Interaction logic for NavigationWindowView.xaml
   /// </summary>
   public partial class NavigationWindowView : INavigationView
   {
      public NavigationWindowView()
      {
         InitializeComponent();
      }

      private void SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
      {
         NavigationNodeViewModel vm = this.DataContext as NavigationNodeViewModel;

         //Call command from viewmodel     
         if ((vm != null) && (vm.SelectViewCommand.CanExecute(((NavigationNode)e.NewValue))))
            vm.SelectViewCommand.Execute(((NavigationNode)e.NewValue));
         ResultTreeView.SelectedItemChanged -= SelectedItemChanged;
         if (ResultTreeView.SelectedItem != vm.Model)
         {
            SelectItem(ResultTreeView, vm.Model);
         }
         ResultTreeView.SelectedItemChanged += SelectedItemChanged;
      }

      private bool SelectItem(ItemsControl parent, object itemToSelect)
      {
         if (parent == null) 
         {
            return false;
         }
         var childTreeNode = parent.ItemContainerGenerator.ContainerFromItem(itemToSelect) as TreeViewItem;
         if (childTreeNode != null)
         {
            childTreeNode.Focus();
            childTreeNode.IsExpanded = true;
            return childTreeNode.IsSelected = true;
         }

         if (parent.Items.Count > 0)
         {
            foreach (object childItem in parent.Items)
            {
               var childItemsControl = parent.ItemContainerGenerator.ContainerFromItem(childItem) as ItemsControl;              
               if (SelectItem(childItemsControl, itemToSelect))
                  return true;
            }
         }
         return false;
      }

   }

}
