﻿using System.ServiceModel;
using System.Threading;
using System.Windows;
using System.Linq;
using Connection.Interface;
using Desktop.Client.MainWindow.Interface;
using Desktop.Client.Server;
using System;
using System.ComponentModel;
using Common.Library.Annotations;
using System.Runtime.CompilerServices;
using Connection.Interface.Contract;
using System.ServiceModel.Channels;
using System.Windows.Controls;
using System.Collections.Generic;
using Desktop.Client.MainWindow.Aplications;
using System.Windows.Input;
using Desktop.Client.NavigationService.Aplications;
using Common.Desktop.Applications;
using Desktop.Client.NavigationService.Interface;
using System.Collections.ObjectModel;
using Desktop.Client.ConectedDevices.Aplications.Proxy;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Connection.Interface.Domain;
using Ploeh.AutoFixture;
using Common.Library.Messenger;

namespace Desktop.Client.MainWindow.Presentation
{
   public partial class MainWindow : IMainWindow, INotifyPropertyChanged, IBat2ErrorEventsContract
   {
      //private readonly IConnectedDevicesController connectedDevicesController;
      private ServiceHost eventsHost;   
      private ChannelFactory<IBat2CommandContract> commandCF;
      private ICommand selectViewCommand;
      private MainWindowViewModel mainWindowVM;
      private bool wasBusy = false;

      public MainWindow(ChannelFactory<IBat2CommandContract> commandCF)
      {     
        // this.connectedDevicesController = connectedDevicesController;
         this.commandCF = commandCF;
         InitializeComponent();

         Messenger.Default.Register<Bat2ErrorEventsContractMessage>(
         this, msg => msg.ActionToInvoke(this));
      }

      private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
      {
          /*
         ThreadPool.QueueUserWorkItem(p =>
         {
            ThreadPool.QueueUserWorkItem(param => connectedDevicesController.Shutdown());

            while (connectedDevicesController.IsRunning)
            {
               Thread.Sleep(100);
            }
            eventsHost.Close();
         });
           * */
      }

      private void Window_Loaded(object sender, RoutedEventArgs e)
      {
          
         ThreadPool.QueueUserWorkItem(p =>
         {
            eventsHost = new ServiceHost(typeof(Bat2EventsContractService));
            eventsHost.Open();
       
            IBat2CommandContract client = commandCF.CreateChannel();
            try
            {
                client.ConnectedDevicesGet();
            }
            catch(Exception ex)
            {
                ShowError("Service is not running. Please start Bat2WcfSerice."); 
            }
        
         });    
         /*
         Bat2EventsContractService es = new Bat2EventsContractService();
         Fixture fixture = new Fixture {RepeatCount = 3};
         fixture.Customize<ArchiveItem>(composer =>
          composer.Without(m => m.HourFrom).Without(m => m.HourTo)
          );
         IEnumerable<Bat2DeviceData> data = fixture.CreateMany<Bat2DeviceData>();
         int i = 0;
         foreach(Bat2DeviceData device in data)
         {
            //all device set gs, event mask to the same value
             device.Configuration.GsmMessage.EventMask = (uint)Bat2Library.GsmEventMaskE.GSM_EVENT_STATUS_CHANGE;
             
            i++;
             if (i % 2 == 1)
             {
                 device.Context.WeighingContext.Status = (byte)Bat2Library.WeighingStatusE.WEIGHING_STATUS_STOPPED;           
             }
             else
             { 
                 device.Context.WeighingContext.Status = (byte)Bat2Library.WeighingStatusE.WEIGHING_STATUS_WEIGHING;
             }
             es.DeviceConnected(device);
         }*/
         // es.ErrorRised(data.FirstOrDefault(),new ErrorMessage("Zarizeni nenalezeno!","",""));
      }

  
      

      public event PropertyChangedEventHandler PropertyChanged;

      [NotifyPropertyChangedInvocator]
      protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
      {
         var handler = PropertyChanged;
         if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
      }
     
      public void ShowMessage(string message)
      {
         Dispatcher.Invoke(() => MessageBox.Show(this, message));
      }

      public void ShowWarning(string message)
      {
         Dispatcher.Invoke(() => MessageBox.Show(this, message, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning));
      }

      public void ShowError(string message)
      {
         Dispatcher.Invoke(() => MessageBox.Show(this, message, "Error", MessageBoxButton.OK, MessageBoxImage.Error));
      }

      public bool? ShowQuestion(string message)
      {
         MessageBoxResult res =
            Dispatcher.Invoke(
               () => MessageBox.Show(this, message, "Question", MessageBoxButton.OKCancel, MessageBoxImage.Question));
         return (res == MessageBoxResult.None)
            ? (bool?) null
            : (res == MessageBoxResult.OK || res == MessageBoxResult.Yes);
      }

      public bool ShowYesNoQuestion(string message)
      {
         MessageBoxResult res =
            Dispatcher.Invoke(
               () => MessageBox.Show(this, message, "Question", MessageBoxButton.YesNo, MessageBoxImage.Question));
         return (res == MessageBoxResult.Yes);
      }
      



      public ICommand SelectViewCommand
      {
          get
          {
              if (selectViewCommand == null)
              {
                  selectViewCommand = new RelayCommand<INavigationNode>(node =>
                  {

                      NavigationNode gvn = (NavigationNode)node;

                      if (gvn != null)
                      {
                          if (gvn.ShowAction != null)
                          {
                              gvn.ShowAction();
                          }
                      }
                   
                      contentControl.Content = gvn.View;
                      //UpdateCommands();

                     // TODO: To catch changes done by user 
                     //if (mainWindowVM == null)
                     //{
                     //   mainWindowVM = DataContext as MainWindowViewModel;
                     //   mainWindowVM.ConnectedDevices.Model.PropertyChanged += Model_PropertyChanged;
                     //}

                  },
                  node => true);
              }
              return selectViewCommand;
          }
      }

      /// <summary>
      /// Catch changes done by user.
      /// </summary>
      private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
      {      
         if ((sender as Bat2DeviceDataProxy).IsBusy == true)
         {
            wasBusy = true;
         }

         if (e.PropertyName != "IsBusy" && (sender as Bat2DeviceDataProxy).IsBusy != true)
         {
            if (wasBusy == true)
            {
               int countPredefinedWeighingList = (sender as Bat2DeviceDataProxy).PredefinedWeighingList.Count();
               int countPredefinedWeighings = 0;
               if ((sender as Bat2DeviceDataProxy).PredefinedWeighings != null)
               {
                  countPredefinedWeighings = (sender as Bat2DeviceDataProxy).PredefinedWeighings.Count();
               }
               
               int countContactList = (sender as Bat2DeviceDataProxy).ContactList.Count();
               int countContacts = 0;
               if ((sender as Bat2DeviceDataProxy).Contacts != null)
               {
                  countContacts = (sender as Bat2DeviceDataProxy).Contacts.Count();
               }

               if (e.PropertyName == "PredefinedWeighingList" && 
                   countPredefinedWeighingList == countPredefinedWeighings)
               {
                  wasBusy = false;
               }
               if (e.PropertyName == "ContactList" &&
                   countContactList == countContacts)
               {
                  wasBusy = false;
               }
            }
            else
            {
               mainWindowVM.ConnectedDevices.Model.IsChanged = true;
               mainWindowVM.UpdateCommands();
            }
         }
      }

     
      private void Button_Click(object sender, RoutedEventArgs e)
      {
          NavigationNode setupWindow = new NavigationNode() { Name = "Setup", Order = 1, View = new WeighingSetup() { DataContext = (this.DataContext as MainWindowViewModel).ConnectedDevices } };       
          contentControl.Content =  new MainUserControl(){ DataContext = new ObservableCollection<INavigationNode>() { setupWindow }};        
      }


      /*private void UpdateCommands() 
      {
         var objectClicked = ((Button)sender).Tag;      

         if (string.Equals(objectClicked, "Configuration"))
         {  
             contentControl.Content = new UserInterface((DataContext as MainWindowViewModel).NavigationNodes.Models);
         }
         else if (string.Equals(objectClicked, "Weighing"))
         {  
             contentControl.Content = new Weighing((DataContext as MainWindowViewModel).NavigationNodes.Models);         
         }
         else if (string.Equals(objectClicked, "DeviceInformation"))
         {
             contentControl.Content = new DeviceInformation((DataContext as MainWindowViewModel).NavigationNodes.Models);            
         }
         else if (string.Equals(objectClicked, "Contacts"))
         {
             (DataContext as MainWindowViewModel).NavigationNodes.Models.Where(p => p.View is IContactView).FirstOrDefault().ShowAction();
             contentControl.Content = new Contacts((DataContext as MainWindowViewModel).NavigationNodes.Models);           
         }
         else if (string.Equals(objectClicked, "Curves"))
         {
             (DataContext as MainWindowViewModel).NavigationNodes.Models.Where(p => p.Name.Equals("CorrectionCurves")).FirstOrDefault().ShowAction();
             (DataContext as MainWindowViewModel).NavigationNodes.Models.Where(p => p.Name.Equals("GrowthCurves")).FirstOrDefault().ShowAction();

             contentControl.Content = new Curves((DataContext as MainWindowViewModel).NavigationNodes.Models);            
         }
         else if (string.Equals(objectClicked, "Setup"))
         {
             contentControl.Content = new WeighingSetup((DataContext as MainWindowViewModel).NavigationNodes.Models);          
         }
      }*/




      public void ErrorRised(Bat2DeviceData deviceData, ErrorMessage errorMessage)
      {
          ShowError(errorMessage.Message);
      }
   }
}