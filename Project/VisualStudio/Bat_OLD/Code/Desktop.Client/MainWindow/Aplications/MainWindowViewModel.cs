﻿using System.Collections.Generic;
using System.Windows.Threading;
using Common.Desktop.Applications;
using Desktop.Client.ConectedDevices.Aplications.Proxy;
using Desktop.Client.MainWindow.Interface;
using Desktop.Client.MessageService.Interface;
using Desktop.Client.NavigationService.Aplications;
using Desktop.Client.ToolBarService.Interface;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Desktop.Client.NavigationService.Interface;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

namespace Desktop.Client.MainWindow.Aplications
{
   public class MainWindowViewModel : ViewModel, IToolBarService, IMessageService
   {
      private ViewModel<Bat2DeviceDataProxy> connectedDevices;
      private ICommand toolBarCommand;
      private ICommand saveAllCommand;
      private ICommand reloadAllCommand;

      private NavigationServiceViewModel navigationNodes;
      private ObservableCollection<IToolBarNode> toolBarNodes;

      public MainWindowViewModel()
      {
         //DispatcherHelper.UIDispatcher.UnhandledException += UnhandledException;
         ToolBarNodes = new ObservableCollection<IToolBarNode>();

      } 

      private void UnhandledException(object sender, DispatcherUnhandledExceptionEventArgs args)
      {
         ShowError(args.Exception.Message);
      }

      public ViewModel<Bat2DeviceDataProxy> ConnectedDevices
      {
         get { return connectedDevices; }
         set 
         {
             SetProperty(ref connectedDevices, value);            
         }
      }     

      public NavigationServiceViewModel NavigationNodes
      {
         get { return navigationNodes; }
         set { SetProperty(ref navigationNodes, value); }
      }

      public ObservableCollection<IToolBarNode> ToolBarNodes
      {
         get { return toolBarNodes; }
         private set { SetProperty(ref toolBarNodes, value); }
      }

      #region Implementation of IToolBarService

      /// <summary>
      /// Adds the specified tool bar node.
      /// </summary>
      /// <param name="node">The tool bar node to add.</param>
      public void AddToolBarNode(IToolBarNode node)
      {         
         ToolBarNodes.Add(node);
      }

      public ICommand ToolBarCommand
      {
         get
         {
            if (toolBarCommand == null)
            {
                toolBarCommand = new RelayCommand<IToolBarNode>(node =>
               {
                  if(node.SelectAction != null)
                  {
                     node.SelectAction();
                  }
                  UpdateCommands();
               },
               node => node.CanSelect);
            }
            return toolBarCommand;
         }
      }

      public ICommand SaveAllCommand
      {
          get
          {
              if (saveAllCommand == null)
              {
                  saveAllCommand = new RelayCommand<Bat2DeviceDataProxy>(node =>
                  {
                      if (node == null) 
                      {                          
                          return;
                      }
                      if (ConnectedDevices.Model.Configuration != null) 
                      {
                          node.SaveProperty(() =>  node.Configuration);
                      }
                      if (ConnectedDevices.Model.GrowthCurves != null)
                      {
                          node.SaveProperty(() => node.GrowthCurves);
                      }
                      if (ConnectedDevices.Model.CorrectionCurves != null)
                      {
                          node.SaveProperty(() => node.CorrectionCurves);
                      }
                      if (ConnectedDevices.Model.Contacts != null)
                      {
                          node.SaveProperty(() => node.Contacts);
                      }
                      if (ConnectedDevices.Model.WeighingPlans != null)
                      {
                          node.SaveProperty(() => node.WeighingPlans);
                      }
                      if (ConnectedDevices.Model.PredefinedWeighings != null)
                      {
                          node.SaveProperty(() => node.PredefinedWeighings);
                      }
                      UpdateCommands();
                  },
                  node => true 
                  //{
                  //   if (node != null)
                  //   {                      
                  //      return node.IsChanged;
                  //   }
                  //   return false;
                  //}                  
                  );
              }
              return saveAllCommand;
          }
      }

      public ICommand ReloadAllCommand
      {
          get
          {
              if (reloadAllCommand == null)
              {
                  reloadAllCommand = new RelayCommand<Bat2DeviceDataProxy>(node =>
                  {
                      if (node == null)
                      {
                          ShowWarning("Device not selected");
                          return;
                      }

                      node.Context = null;
                      node.LoadProperty(() => node.Context);
                 
                      node.Configuration.DeviceInfo = null;
                      node.Configuration.Country = null;
                      node.Configuration.WeightUnits = null;
                      node.Configuration.PlatformCalibration = null;
                      node.Configuration.DisplayConfiguration = null;
                      node.Configuration.WeighingConfiguration = null;
                      node.Configuration.Rs485Options = null;
                      node.Configuration.ModbusOptions = null;
                      node.Configuration.MegaviOptions = null;
                      node.Configuration.SmsGateOptions = null;
                      node.Configuration.DacsOptions = null;
                      node.Configuration.EthernetOptions = null;
                      node.Configuration.GsmMessage = null;
                      node.LoadProperty(() => node.Configuration, true);

                      node.GrowthCurves = null;
                      node.LoadProperty(() => node.GrowthCurves);

                      node.CorrectionCurves = null;
                      node.LoadProperty(() => node.CorrectionCurves);

                      node.Contacts = null;
                      node.LoadProperty(() => node.Contacts);

                      node.WeighingPlans = null;
                      node.LoadProperty(() => node.WeighingPlans);

                      node.PredefinedWeighings = null;
                      node.LoadProperty(() => node.PredefinedWeighings);

                      UpdateCommands();                                   
                  },
                  node => true);
              }
              return reloadAllCommand;
          }
      }

     
      /// <summary>
      /// Raise CanExecuteChanged on all commands
      /// </summary>
      public void UpdateCommands()
      {
         ((Command)ToolBarCommand).RaiseCanExecuteChanged();
         ((Command)SaveAllCommand).RaiseCanExecuteChanged();
      }


      /// <summary>
      /// Adds the specified tool bar nodes.
      /// </summary>
      /// <param name="nodes">The tool bar nodes to add.</param>
      public void AddToolBarNode(IEnumerable<IToolBarNode> nodes)
      {
         foreach (IToolBarNode node in nodes) 
         {
            ToolBarNodes.Add(node);
         }
      }

      /// <summary>
      /// Remove single tool bar node.
      /// </summary>
      /// <param name="node">The node to be removed</param>
      public void RemoveToolBarNode(IToolBarNode node)
      {
         ToolBarNodes.Remove(node);
      }

      /// <summary>
      /// Remove multiple tool bar nodes.
      /// </summary>
      /// <param name="nodes">Nodes to be removed</param>
      public void RemoveToolBarNode(IEnumerable<IToolBarNode> nodes)
      {
         foreach (IToolBarNode node in nodes) 
         {
            ToolBarNodes.Remove(node);
         }
      }

      /// <summary>
      /// Remove all tool bar nodes
      /// </summary>
      public void RemoveToolBarNode()
      {
         ToolBarNodes.Clear();
      }

      #endregion

      #region Implementation of IMessageService

      /// <summary>
      /// Shows the message.
      /// </summary>
      /// <param name="owner">The window that owns this Message Window.</param>
      /// <param name="message">The message.</param>
      public void ShowMessage(string message)
      {
         ((IMainWindow) View).ShowMessage(message);
      }

      /// <summary>
      /// Shows the message as warning.
      /// </summary>
      /// <param name="owner">The window that owns this Message Window.</param>
      /// <param name="message">The message.</param>
      public void ShowWarning(string message)
      {
         ((IMainWindow)View).ShowWarning(message);
      }

      /// <summary>
      /// Shows the message as error.
      /// </summary>
      /// <param name="owner">The window that owns this Message Window.</param>
      /// <param name="message">The message.</param>
      public void ShowError(string message)
      {
         ((IMainWindow)View).ShowError(message);
      }

      /// <summary>
      /// Shows the specified question.
      /// </summary>
      /// <param name="owner">The window that owns this Message Window.</param>
      /// <param name="message">The question.</param>
      /// <returns><c>true</c> for yes, <c>false</c> for no and <c>null</c> for cancel.</returns>
      public bool? ShowQuestion(string message)
      {
         return ((IMainWindow)View).ShowQuestion(message);
      }

      /// <summary>
      /// Shows the specified yes/no question.
      /// </summary>
      /// <param name="owner">The window that owns this Message Window.</param>
      /// <param name="message">The question.</param>
      /// <returns><c>true</c> for yes and <c>false</c> for no.</returns>
      public bool ShowYesNoQuestion(string message)
      {
         return ((IMainWindow)View).ShowYesNoQuestion(message);
      }

      #endregion
   }
}
