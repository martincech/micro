﻿using Desktop.Client.ModelViews.Presentation;
using Desktop.Client.NavigationService.Aplications;
using Desktop.Client.NavigationService.Interface;
using System.Collections;
using System.Collections.Generic;

namespace Desktop.Client.SampleData
{
   public class SampleNavigationNodeViewModel : NavigationNodeViewModel
   {
      public SampleNavigationNodeViewModel() 
      {
         // CONFIGURATION GROUP
          INavigationNode curNode = AddNavigationNode("Weighing", 1, new WeighingView() {DataContext = new SampleWeighingViewModel()});
         curNode = AddNavigationNode("1.1", 1, new CountryView(), curNode);
         curNode = AddNavigationNode("1.1.1", 1, null, curNode);
         curNode = AddNavigationNode("1.1.1.1", 1, new WeightUnitView(), curNode);
         curNode = AddNavigationNode("WeighingPlan", 2, new WeighingPlanView() { DataContext = new SampleWeighingPlanViewModel()});
         INavigationNode group = curNode = AddNavigationNode("3", 3);
         curNode = AddNavigationNode("3.1", 1, new CountryView(), group);
         curNode = AddNavigationNode("3.2", 2, new WeightUnitView(), group);
         curNode = AddNavigationNode("3.3", 3, new WeightUnitView(), group);
         curNode = AddNavigationNode("3.3.1", 1, new WeightUnitView(), curNode);
      }

      public IEnumerable<INavigationNode> List
      {
          get { return this.Models; }
      }
   }
}
