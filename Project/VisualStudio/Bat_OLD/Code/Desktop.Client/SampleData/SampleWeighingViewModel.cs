﻿using ViewModels;
using Desktop.Client.ModelViews.Applications;
using Desktop.Client.ModelViews.Interface;
using Common.Desktop.Presentation;
using System;
using System.ComponentModel.Composition;
using System.Linq;

namespace Desktop.Client.SampleData
{
   public class SampleWeighingViewModel : WeighingViewModel
   {
      public SampleWeighingViewModel() 
         : base(new MockIWeighingView(),
                new SampleCorrectionCurveViewModel(),
                new SampleGrowthCurveViewModel(),
                new SampleGrowthCurveViewModel(),
                new SampleWeighingPlanViewModel())
      {
         var conf = SampleDataProvider.CreateWeighings().First();
         conf.Status = Bat2Library.WeighingStatusE.WEIGHING_STATUS_SUSPENDED;
         conf.PrevStatus = Bat2Library.WeighingStatusE.WEIGHING_STATUS_STOPPED;
         conf.DayActive = true;
         conf.Day = 2;
         conf.TargetWeightFemale = 120;
         conf.TargetWeightMale = 150;
         conf.Correction = 3;
         conf.DayCloseAt = new DateTime(2014, 12, 12, 7, 20, 0);
         conf.StartAt = new DateTime(2014, 12, 1, 5, 0, 0);
         conf.Id = 7;
         conf.DayDuration = new TimeSpan(12, 0, 0);

         /*conf.WeighingPlan.SyncWithDayStart = true;
         conf.WeighingPlan.Name = "plan name";

         WeighingTime time1 = new WeighingTime();
         time1.From = new DateTime(1, 1, 1, 10, 10, 0);
         time1.To = new DateTime(1, 1, 1, 14, 0, 0);
         WeighingTime time2 = new WeighingTime();
         time2.From = new DateTime(1, 1, 1, 18, 30, 0);
         time2.To = new DateTime(1, 1, 1, 20, 5, 0);
         conf.WeighingPlan.WeighingTime.Clear();
         conf.WeighingPlan.WeighingTime.Add(time1);
         conf.WeighingPlan.WeighingTime.Add(time2);*/

         /*conf.CorrectionCurve.Name = "Curve #1";
         conf.CorrectionCurve.Points.Clear();
         for (int i=0; i < 5; i++)
         {
            CurvePoint point = new CurvePoint();
            point.ValueX = i;
            point.ValueY = i;
            conf.CorrectionCurve.Points.Add(point);
         }*/

         /*conf.GrowthCurveFemale.Name = "Female growth";
         conf.GrowthCurveMale.Name = "Male growth";
         conf.GrowthCurveFemale.Points.Clear();
         conf.GrowthCurveMale.Points.Clear();
         for (int i = 0; i < 5; i++)
         {
            CurvePoint point = new CurvePoint();
            point.ValueX = 2 * i;
            point.ValueY = 3 * i;
            conf.GrowthCurveFemale.Points.Add(point);

            point.ValueX += 3;
            point.ValueY += 1;
            conf.GrowthCurveMale.Points.Add(point);
         }*/

         Model = conf;
      }
   }

   [PartNotDiscoverable]
   internal class MockIWeighingView : MockView, IWeighingDetailView
   {
   }
}
