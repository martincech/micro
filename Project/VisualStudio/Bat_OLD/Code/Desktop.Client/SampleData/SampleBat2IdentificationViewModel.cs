﻿using ViewModels;
using Desktop.Client.ModelViews.Applications;
using Desktop.Client.ModelViews.Interface;
using Common.Desktop.Presentation;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;

namespace Desktop.Client.SampleData
{
    public class SampleBat2IdentificationViewModel : Bat2IdentificationViewModel
    {
        public SampleBat2IdentificationViewModel()
         : base(new MockIBat2IdentificationView())
      {
          var devices = SampleDataProvider.CreateBat2Identifications();
          var dev = devices.First();

          dev.Name = "Bat2super";
          dev.SerialNumber = 28;
          dev.Version.Class = Bat2Library.DeviceClassE.DEVICE_SCALE_COMPACT;
          dev.Version.Modification = Bat2Library.DeviceModificationE.DEVICE_MODIFICATION_SIGMA_DELTA;
          dev.Version.HardwareMajor = 1;
          dev.Version.HardwareMinor = 12;
          dev.Version.HardwareBuild = 54;
          dev.Version.SoftwareMajor = 2;
          dev.Version.SoftwareMinor = 3;
          dev.Version.SoftwareBuild = 93;
    
         Model = dev;

         foreach (var d in devices)
         {
            Models.Add(d);
         }
      }
    }

    [PartNotDiscoverable]
    internal class MockIBat2IdentificationView : MockView, IBat2IdentificationDetailView
    {
    }
}
