﻿using ViewModels;
using Desktop.Client.ModelViews.Applications;
using Desktop.Client.ModelViews.Interface;
using Common.Desktop.Presentation;
using System.ComponentModel.Composition;
using System.Linq;

namespace Desktop.Client.SampleData
{
   public class SampleGrowthCurveViewModel : GrowthCurveViewModel
   {
      public SampleGrowthCurveViewModel()
         : base(new MockIGrowthCurveView())
      {
         var curves = SampleDataProvider.CreateGrowthCurves();
         var curve = curves.First();

         curve.Points.Clear();

         curve.Name = "growth curve";
         curve.Points.Clear();
         for (int i = 0; i < 5; i++)
         {
            CurvePoint point = new CurvePoint();
            point.ValueX = 2 * i;
            point.ValueY = 3 * i + 1;
            curve.Points.Add(point);
         }

         Model = curve;

         foreach (var c in curves)
         {
            Models.Add(c);
         }
      }
   }

   [PartNotDiscoverable]
   internal class MockIGrowthCurveView : MockView, ICurveDetailView
   {
   }
}
