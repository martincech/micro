﻿using System;
using Connection.Interface.Contract;

namespace Desktop.Client.Server
{
   internal class Bat2ActionEventsContractMessage
   {

      public Bat2ActionEventsContractMessage(Action<IBat2ActionEventsContract> actionToInvoke)
      {
         ActionToInvoke = actionToInvoke;
      }
      public Action<IBat2ActionEventsContract> ActionToInvoke { get; private set; }
   }
}
