﻿using System;
using System.Windows.Data;

namespace Desktop.Client.ModelViews.Converters
{
   public class TimeSpanWithoutDaysConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         if (targetType == typeof(TimeSpan?) && value != null)
         {
            TimeSpan time = (TimeSpan)value;
            return new TimeSpan(0, time.Hours, time.Minutes, time.Seconds);
         }
         return null;
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         if (targetType == typeof(TimeSpan))
         {
            TimeSpan time = (TimeSpan)value;
            return new TimeSpan(0, time.Hours, time.Minutes, time.Seconds);
         }
         return null;
      }
   }
}
