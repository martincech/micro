﻿using Common.Desktop.Applications;
using ViewModels;
using Desktop.Client.ModelViews.Interface;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows.Input;

namespace Desktop.Client.ModelViews.Applications
{
   [Export, Export(typeof(ViewModel)), Export(typeof(ViewModel<Contact>))]
   public class ContactsViewModel : ViewModel<Contact>
   {
      #region Private fields

      private RelayCommand addModelCommand;
      private RelayCommand<object> deleteModelCommand;

      #endregion

      #region Public interfaces

      #region Constructors

      public ContactsViewModel(
         IContactView view)
         : base(view)
      {
      }

      #endregion

      #region Commands for models

      /// <summary>
      /// Add new <see cref="ViewModels.Contact"/> to <see cref="Models"/> collection and map this object properties to newly created object.
      /// <see cref="ViewModels.Contact"/> will be set tho newly created object.
      /// </summary>
      public ICommand AddModelCommand
      {
         get
         {
            if (addModelCommand == null)
            {
               addModelCommand = new RelayCommand(
                  () =>
                  {
                     var conf = new Contact();
                     AddModelWithOptionalSelect(conf, true);
                     Validate();
                  });
            }
            return addModelCommand;
         }
      }

      /// <summary>
      /// Delete <see cref="ViewModels.Contact"/> or collection of <see cref="ViewModels.Contact"/> from <see cref="Models"/> collection. When parameter is null than <see cref="Model"/>
      /// is deleted (have to be non-null), when parameter is non-null than this object or objects are deleted (have to be one of <see cref="Models"/> items).
      /// </summary>
      public ICommand DeleteModelCommand
      {
         get
         {
            if (deleteModelCommand == null)
            {
               deleteModelCommand = new RelayCommand<object>(
                  c =>
                  {
                     IEnumerable<Contact> selList = null;
                     if (c == null)
                     {
                        selList = new List<Contact>(SelectedModels);
                     }
                     else if (c is IEnumerable<Contact>)
                     {
                        selList = c as IEnumerable<Contact>;
                     }

                     if (selList != null)
                     {
                        foreach (var o in selList)
                        {
                           DeleteModelWithOptionalMove(o, true);
                        }
                     }
                     else
                     {
                        DeleteModelWithOptionalMove(c as Contact, true);
                     }
                     Validate();
                  },
                  c =>
                  {
                     return true;
                     //if (c == null)
                     //{
                     //   return Model != null;
                     //}
                     //return Models.Contains(c as Contact);
                  });
            }
            return deleteModelCommand;
         }
      }

      #endregion

      #endregion
   }
}
