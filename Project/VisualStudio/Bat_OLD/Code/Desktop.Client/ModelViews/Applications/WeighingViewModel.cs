﻿using Common.Desktop.Applications;
using ViewModels;
using Desktop.Client.ModelViews.Interface;
using System.ComponentModel.Composition;
using System.Windows.Input;

namespace Desktop.Client.ModelViews.Applications
{
   [Export, Export(typeof(ViewModel)), Export(typeof(ViewModel<Weighing>))]
   public class WeighingViewModel : ViewModel<Weighing>
   {
      #region Private fields

      private ViewModel<CorrectionCurve> correctionCurveVm;   
      private ViewModel<GrowthCurve> growthCurveMaleVm;
      private ViewModel<GrowthCurve> growthCurveFemaleVm;

      private ViewModel<WeighingPlan> weighingPlanVm;  

      #endregion

      #region Public interface

      #region Constructors

      public WeighingViewModel(IWeighingView view)
         : base(view)
      {
      }

      [ImportingConstructor]  
      public WeighingViewModel(
         IWeighingDetailView view,
         ViewModel<CorrectionCurve> corrCurve,
         ViewModel<GrowthCurve> growthMale,
         ViewModel<GrowthCurve> growthFemale,
         ViewModel<WeighingPlan> weighingPlanVm)
         : base(view)
      {
         PropertyChanged += WeighingViewModel_PropertyChanged;

         CorrectionCurveVm = corrCurve;
         GrowthCurveMaleVm = growthMale;
         GrowthCurveFemaleVm = growthFemale;
         this.WeighingPlanVm = weighingPlanVm;
      }   

      #endregion

      #region Selected model properties

      public ViewModel<CorrectionCurve> CorrectionCurveVm
      {
         get { return correctionCurveVm; }
         set { SetProperty(ref correctionCurveVm, value); }
      }

      public ViewModel<GrowthCurve> GrowthCurveMaleVm
      {
         get { return growthCurveMaleVm; }
         set { SetProperty(ref growthCurveMaleVm, value); }
      }

      public ViewModel<GrowthCurve> GrowthCurveFemaleVm
      {
         get { return growthCurveFemaleVm; }
         set { SetProperty(ref growthCurveFemaleVm, value); }
      }

      public ViewModel<WeighingPlan> WeighingPlanVm
      {
         get { return weighingPlanVm; }
         set { SetProperty(ref weighingPlanVm, value); }
      }

      #endregion

      #endregion

      #region Private helpers

      private void WeighingViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
      {
         if (e.PropertyName == "View" || e.PropertyName == "Model")
         {
            if (Model != null)
            {
               Model.CorrectionCurve = CorrectionCurveVm.Model;
               Model.GrowthCurveMale = GrowthCurveMaleVm.Model;
               Model.GrowthCurveFemale = GrowthCurveFemaleVm.Model;
               Model.WeighingPlan = WeighingPlanVm.Model;
            }
         }
      }

      #endregion
   }
}
