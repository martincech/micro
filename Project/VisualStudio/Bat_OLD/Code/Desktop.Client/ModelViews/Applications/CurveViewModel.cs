﻿using Common.Desktop.Applications;
using ViewModels;
using Desktop.Client.ModelViews.Interface;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;

namespace Desktop.Client.ModelViews.Applications
{
   public abstract class CurveViewModel<T> : ViewModel<T> where T : Curve, new()
   {
      #region Protected fields

      private string descriptionX;
      private string descriptionY;
      private string unitX;
      private string unitY;
      private CurvePoint lastEditPoint;
      private double transfer;

      // axis limits
      private double yMin;
      private double yMax;
      private double xMax;

      private RelayCommand addModelCommand;
      private RelayCommand<object> deleteModelCommand;

      ObservableCollection<CurvePoint> pointsView;
      #endregion

      #region Public interfaces

      #region Constructors

      public CurveViewModel(ICurveView view)
         : base(view)
      {
         DescriptionX = "Days";
         pointsView = new ObservableCollection<CurvePoint>();
         pointsView.CollectionChanged += pointsView_CollectionChanged;
      }

      #endregion

      #region Selected model properties

      //range of y axis 
      public double YMin
      {
         get { return yMin; }
         protected set { SetProperty(ref yMin, value); }
      }

      public double YMax
      {
         get { return yMax; }
         protected set { SetProperty(ref yMax, value); }
      }

      public double XMax
      {
         get { return xMax; }
         protected set { SetProperty(ref xMax, value); }
      }

      /// <summary>
      /// Description curves's axis X
      /// </summary>
      public string DescriptionX
      {
         get { return descriptionX; }
         set { SetProperty(ref descriptionX, value); }
      }

      /// <summary>
      /// Description curve's axis Y
      /// </summary>
      public string DescriptionY
      {
         get { return descriptionY; }
         set { SetProperty(ref descriptionY, value); }
      }

      /// <summary>
      /// Unit on axis X
      /// </summary>
      public string UnitX
      {
         get { return unitX; }
         set { SetProperty(ref unitX, value); }
      }

      /// <summary>
      /// Unit on axis Y
      /// </summary>
      public string UnitY
      {
         get { return unitY; }
         set { SetProperty(ref unitY, value); }
      }

      public CurvePoint LastEditPoint
      {
         get { return lastEditPoint; }
         set { SetPropertyAndValidate(ref lastEditPoint, value); }
      }

      /// <summary>
      /// Transfer unit between units in model and view
      /// </summary>
      public double Transfer
      {
         get { return transfer; }
         set { SetProperty(ref transfer, value); }
      }

      public ObservableCollection<CurvePoint> PointsView
      {
         get 
         {          
            if (Model != null && Model.Points is ObservableCollection<CurvePoint>)
            {
               pointsView = Model.Points as ObservableCollection<CurvePoint>;
               return pointsView;
            }          
            pointsView.Clear();         
            return pointsView;
         }
         private set 
         {
            SetPropertyAndValidate(ref pointsView, value);
         }
      }
      
      #endregion

      #region Commands for models

      /// <summary>
      /// Add new <see cref="ViewModelsWeightUnit"/> to <see cref="Models"/> collection and map this object properties to newly created object.
      /// <see cref="ViewModelsWeightUnit"/> will be set tho newly created object.
      /// </summary>
      public ICommand AddModelCommand
      {
         get
         {
            if (addModelCommand == null)
            {
               addModelCommand = new RelayCommand(
                  () =>
                  {
                     var conf = new T();
                     conf.Name = "Curve";
                     conf.Points = new ObservableCollection<CurvePoint>();

                     AddModelWithOptionalSelect(conf, true);
                     Validate();
                  });
            }
            return addModelCommand;
         }
      }

      /// <summary>
      /// Delete <see cref="ViewModelsWeightUnit"/> or collection of <see cref="ViewModelsWeightUnit"/> from <see cref="Models"/> collection. When parameter is null than <see cref="Model"/>
      /// is deleted (have to be non-null), when parameter is non-null than this object or objects are deleted (have to be one of <see cref="Models"/> items).
      /// </summary>
      public ICommand DeleteModelCommand
      {
         get
         {
            if (deleteModelCommand == null)
            {
               deleteModelCommand = new RelayCommand<object>(
                  c =>
                  {
                     IEnumerable<T> selList = null;
                     if (c == null)
                     {
                        selList = new List<T>(SelectedModels);
                     }
                     else if (c is IEnumerable<T>)
                     {
                        selList = c as IEnumerable<T>;
                     }

                     if (selList != null)
                     {
                        foreach (var o in selList)
                        {
                           DeleteModelWithOptionalMove(o, true);
                        }
                     }
                     else
                     {
                        DeleteModelWithOptionalMove(c as T, true);
                     }
                     Validate();
                  },
                  c =>
                  {
                     return true;
                     //if (c == null)
                     //{
                     //   return Model != null;
                     //}
                     //return Models.Contains(c);
                  });
            }
            return deleteModelCommand;
         }
      }     

      #endregion

      #endregion

      #region Private helpers

      private void pointsView_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {

         if (Model == null || Model.Points == null)
         {
            return;
         }       
         switch (e.Action)
         {
            case NotifyCollectionChangedAction.Add:            
               foreach (CurvePoint point in e.NewItems)
               {               
                  Model.Points.Add(point);                  
               }
               //if point was edit instead of insert, collection must be sorted
               IEnumerable<CurvePoint> l = Model.Points.OrderBy(x => x.ValueX);
               Model.Points = new List<CurvePoint>(l);
               break;

            case NotifyCollectionChangedAction.Remove:
               foreach (CurvePoint point in e.OldItems)
               {
                  Model.Points.Remove(point);               
               }
               break;

            /*case NotifyCollectionChangedAction.Reset:
               Debug.Assert(sender != pointsView);
               pointsView.CollectionChanged -= pointsView_CollectionChanged;
               foreach (CurvePoint point in Model.Points)             
               {
                  pointsView.Add(point);
               }
               pointsView.CollectionChanged += pointsView_CollectionChanged;
               break;*/
         }
      }

      #endregion
   }
}
