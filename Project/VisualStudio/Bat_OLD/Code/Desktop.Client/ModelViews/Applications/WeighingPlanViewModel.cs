﻿using System;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.Composition;
using Bat2Library;
using Common.Desktop.Applications;
using ViewModels;
using Desktop.Client.ModelViews.Interface;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace Desktop.Client.ModelViews.Applications
{
   [Export, Export(typeof (ViewModel)), Export(typeof (ViewModel<WeighingPlan>))]
   public class WeighingPlanViewModel : ViewModel<WeighingPlan>
   {
      #region Private fields

      private RelayCommand addModelCommand;
      private RelayCommand<object> deleteModelCommand;

      //Support for WeighingDaysMaskE enum multiselect
      private ObservableCollection<WeighingDaysMaskE> selectedWeighingDaysDaysOfWeek;
      private bool recalcCollection;

      #endregion

      #region Public interface

      #region Constructors

      [ImportingConstructor]
      public WeighingPlanViewModel(
         IWeighingPlanView view)
         : base(view)
      {
         PropertyChanged += WeighingPlanViewModel_PropertyChanged;
         SelectedWeighingDaysDaysOfWeek = new ObservableCollection<WeighingDaysMaskE>();
         SelectedWeighingDaysDaysOfWeek.CollectionChanged += SelectedWeighingDaysDaysOfWeekChanged;
         recalcCollection = true;
      }

      #endregion

      #region Selected model properties

      /// <summary>
      /// Weighing day of week
      /// </summary>
      public WeighingDaysMaskE WeighingDaysDaysOfWeek
      {
         get { return Model == null ? 0 : Model.WeighingDays.DaysOfWeek; }
         set
         {
            if (Model == null)
            {
               return;
            }
            Model.WeighingDays.DaysOfWeek = value;
            if (recalcCollection)
            {
               SelectedWeighingDaysDaysOfWeek.CollectionChanged -= SelectedWeighingDaysDaysOfWeekChanged;
               SelectedWeighingDaysDaysOfWeek.Clear();
               foreach (WeighingDaysMaskE ev in typeof (WeighingDaysMaskE).GetEnumValues())
               {
                  if (WeighingDaysDaysOfWeek.HasFlag(ev))
                  {
                     SelectedWeighingDaysDaysOfWeek.Add(ev);
                  }
               }
               SelectedWeighingDaysDaysOfWeek.CollectionChanged += SelectedWeighingDaysDaysOfWeekChanged;
            }
         }
      }

      /// <summary>
      /// Weighing day of week as collection
      /// </summary>
      public ObservableCollection<WeighingDaysMaskE> SelectedWeighingDaysDaysOfWeek
      {
         get { return selectedWeighingDaysDaysOfWeek; }
         private set { SetProperty(ref selectedWeighingDaysDaysOfWeek, value); }
      }

      #endregion

      #region Commands for models

      /// <summary>
      /// Add new <see cref="ViewModelsWeightUnit"/> to <see cref="Models"/> collection and map this object properties to newly created object.
      /// <see cref="ViewModelsWeightUnit"/> will be set tho newly created object.
      /// </summary>
      public ICommand AddModelCommand
      {
         get
         {
            if (addModelCommand == null)
            {
               addModelCommand = new RelayCommand(
                  () =>
                  {
                     var conf = new WeighingPlan();
                     AddModelWithOptionalSelect(conf, true);
                     Validate();
                  });
            }
            return addModelCommand;
         }
      }

      /// <summary>
      /// Delete <see cref="ViewModelsWeightUnit"/> or collection of <see cref="ViewModelsWeightUnit"/> from <see cref="Models"/> collection. When parameter is null than <see cref="Model"/>
      /// is deleted (have to be non-null), when parameter is non-null than this object or objects are deleted (have to be one of <see cref="Models"/> items).
      /// </summary>
      public ICommand DeleteModelCommand
      {
         get
         {
            if (deleteModelCommand == null)
            {
               deleteModelCommand = new RelayCommand<object>(
                  c =>
                  {
                     IEnumerable<WeighingPlan> selList = null;
                     if (c == null)
                     {
                        selList = new List<WeighingPlan>(SelectedModels);
                     }
                     else if (c is IEnumerable<WeighingPlan>)
                     {
                        selList = c as IEnumerable<WeighingPlan>;
                     }

                     if (selList != null)
                     {
                        foreach (var o in selList)
                        {
                           DeleteModelWithOptionalMove(o, true);
                        }
                     }
                     else
                     {
                        DeleteModelWithOptionalMove(c as WeighingPlan, true);
                     }
                     Validate();
                  },
                  c =>
                  {                 
                     return true;
                     //if (c == null)
                     //{
                     //   return Model != null;
                     //}
                     //return Models.Contains(c);
                  });
            }
            return deleteModelCommand;
         }
      }

      #endregion

      #endregion

      #region Private methods

      /// <summary>
      /// Property changed event handler
      /// </summary>
      /// <param name="sender">this</param>
      /// <param name="e"></param>
      private void WeighingPlanViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         //Debug.Assert(sender == this);
         if (e.PropertyName == "View" || e.PropertyName == "Model")
         {
            if (Model != null)
            {
               WeighingDaysDaysOfWeek = Model.WeighingDays.DaysOfWeek;            
            }         
         }
      }    

      private void SelectedWeighingDaysDaysOfWeekChanged(object sender,
         System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
      {
         WeighingDaysMaskE mask = default(WeighingDaysMaskE);
         foreach (WeighingDaysMaskE ev in SelectedWeighingDaysDaysOfWeek)
         {
            mask |= ev;
         }
         recalcCollection = false;
         WeighingDaysDaysOfWeek = mask;
         recalcCollection = true;
      }

      #endregion
   }
}