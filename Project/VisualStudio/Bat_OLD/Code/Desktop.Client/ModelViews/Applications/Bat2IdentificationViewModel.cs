﻿using Common.Desktop.Applications;
using ViewModels;
using Desktop.Client.ModelViews.Interface;
using System.ComponentModel.Composition;

namespace Desktop.Client.ModelViews.Applications
{
   [Export, Export(typeof(ViewModel)), Export(typeof(ViewModel<Bat2Identification>))]
   public class Bat2IdentificationViewModel : ViewModel<Bat2Identification>
   {
      #region Public interfaces

      #region Constructors

      /// <summary>
      /// Initializes a new instance of the <see cref="ViewModel"/> class for single view and attaches itself as <c>DataContext</c> to the view.
      /// </summary>
      /// <param name="view">The view which will have this ViewModel attached as <c>DataContext</c>.</param>
      [ImportingConstructor]
      public Bat2IdentificationViewModel(
         IBat2IdentificationView view)
         : base(view)
      {
      }

      #endregion

      #endregion
   }
}
