﻿using Common.Desktop.Applications;
using ViewModels;
using Desktop.Client.ModelViews.Interface;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System;

namespace Desktop.Client.ModelViews.Applications
{
   [Export, Export(typeof(ViewModel)), Export(typeof(ViewModel<CorrectionCurve>))]
   public class CorrectionCurveViewModel : CurveViewModel<CorrectionCurve>
   {
      #region Constructors

      [ImportingConstructor]
      public CorrectionCurveViewModel(ICurveView view)
         : base(view)
      {
         YMax = Bat2Library.CorrectionCurveC.CORRECTION_MAX;
         YMin = Bat2Library.CorrectionCurveC.CORRECTION_MIN;
         Transfer = 0.1;

         //XMax = 100;  //TODO: use constant from Bat2Library

         DescriptionY = "Percent";
         UnitY = "%";
      }

      #endregion

      #region Additional validation

      protected override IEnumerable<ValidationResult> AdditionalValidationRules(object value, string propertyName)
      {
         string errorMsg = "";

         if (propertyName == "Model")
         {
            return base.AdditionalValidationRules(value, propertyName);
         }

         if (propertyName == "Name")
         {
            string name = value as string;
            if (name.Length <= Bat2Library.CorrectionCurveC.CURVE_NAME_SIZE_MAX_LENGTH)
            {
               return base.AdditionalValidationRules(value, propertyName);
            }
            //errorMsg = Resources.CorrectionCurve_Name_Size;
            errorMsg = "Correction curve - name size!";
         }

         if (propertyName == "LastEditPoint" && value != null)
         {
            CurvePoint cPoint = value as CurvePoint;
            
            if (Model.Points.Count > Bat2Library.CorrectionCurveC.POINT_COUNT_MAX)
            {
               //errorMsg = Resources.CorrectionCurve_Points_Count;
               errorMsg = "Correction curve - too much points";
            }
            else if (cPoint.ValueY >= Bat2Library.CorrectionCurveC.CORRECTION_MIN &&
                     cPoint.ValueY <= Bat2Library.CorrectionCurveC.CORRECTION_MAX &&
                     cPoint.ValueX >= 0 /*&&
                     cPoint.ValueX <= 100//TODO: constant  */
                     )
            {
               return base.AdditionalValidationRules(value, propertyName);
            }
            else
            {
               //errorMsg = Resources.CorrectionCurve_InvalidPoint;
               errorMsg = "Correction curve - invalid point";
            }
         }

         var valRes = new List<ValidationResult>
         {
            new ValidationResult(errorMsg, new []{propertyName})    
         };
         return valRes;
      }
    
      #endregion
   }
}
