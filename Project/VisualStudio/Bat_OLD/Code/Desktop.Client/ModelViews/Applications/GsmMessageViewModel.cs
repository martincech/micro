﻿using Bat2Library;
using Common.Desktop.Applications;
using ViewModels;
using Desktop.Client.ModelViews.Interface;
using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Windows.Input;
using Common.Desktop.Presentation;

namespace Desktop.Client.ModelViews.Applications
{
   [Export, Export(typeof(ViewModel)), Export(typeof(ViewModel<GsmMessage>))]
   public class GsmMessageViewModel : ViewModel<GsmMessage>
   {
      #region Private fields

      private bool recalcCollection;
      private ObservableCollection<GsmEventMaskE> gsmEvents;

      #endregion

      #region Public interfaces

      #region Constructors

      internal class MockIGsmPowerOptionTimeView : MockView, IView
      {
      }

      [ImportingConstructor]
      public GsmMessageViewModel(IGsmMessageView view)
         : base(view)
      {
         PropertyChanged += GsmMessageViewModel_PropertyChanged;    

         SelectedEvents = new ObservableCollection<GsmEventMaskE>();
         SelectedEvents.CollectionChanged += SelectedEvents_CollectionChanged;
         recalcCollection = true;
      }

      #endregion

      #region Selected model properties

      public GsmEventMaskE EventsEventMask
      {
         get { return Model == null? 0: Model.Events.EventMask; }
         set
         {
            if (Model == null)
            {
               return;
            }
            Model.Events.EventMask = value;
            if (recalcCollection)
            {
               SelectedEvents.CollectionChanged -= SelectedEvents_CollectionChanged;
               SelectedEvents.Clear();
               foreach (GsmEventMaskE ev in typeof(GsmEventMaskE).GetEnumValues())
               {
                  if (EventsEventMask.HasFlag(ev))
                  {
                     SelectedEvents.Add(ev);
                  }
               }
               SelectedEvents.CollectionChanged += SelectedEvents_CollectionChanged;
            }
         }
      }

      public ObservableCollection<GsmEventMaskE> SelectedEvents
      {
         get { return gsmEvents; }
         private set { SetProperty(ref gsmEvents, value); }
      }    

      #endregion

      #endregion

      #region Private helpers

      /// <summary>
      /// Property changed event handler
      /// </summary>
      /// <param name="sender">this</param>
      /// <param name="e"></param>
      private void GsmMessageViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         //Debug.Assert(sender == this);
         if (e.PropertyName == "View" || e.PropertyName == "Model")
         {
            if (Model != null)
            {
               EventsEventMask = Model.Events.EventMask;             
            }          
         }
      } 

      private void SelectedEvents_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
      {
         GsmEventMaskE mask = default(GsmEventMaskE);
         foreach (GsmEventMaskE ev in SelectedEvents)
         {
            mask |= ev;
         }
         recalcCollection = false;
         EventsEventMask = mask;
         recalcCollection = true;
      }

      #endregion
   }
}
