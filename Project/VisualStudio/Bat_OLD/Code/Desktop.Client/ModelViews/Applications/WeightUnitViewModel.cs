﻿using Common.Desktop.Applications;
using ViewModels;
using Desktop.Client.ModelViews.Interface;
using System.ComponentModel.Composition;

namespace Desktop.Client.ModelViews.Applications
{
   [Export, Export(typeof(ViewModel)), Export(typeof(ViewModel<WeightUnit>))]
   public class WeightUnitViewModel : ViewModel<WeightUnit>
   {
      #region Private fields
      #endregion

      #region Public interfaces

      #region Constructors

      [ImportingConstructor]
      public WeightUnitViewModel(IWeightUnitView view)
         : base(view)
      {
      }

      #endregion

      #endregion
   }
}
