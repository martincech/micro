﻿using System.ComponentModel.Composition;
using Common.Desktop.Presentation;

namespace Desktop.Client.ModelViews.Interface
{
   /// <summary>
   /// Weighing view base
   /// </summary>
   [InheritedExport]
   public interface IWeighingView : IView
   {
   }

   /// <summary>
   /// Weighing settings detail view
   /// </summary>
   [InheritedExport]
   public interface IWeighingDetailView : IWeighingView, IDetailView
   {
   }
}
