﻿using System.ComponentModel.Composition;
using Common.Desktop.Presentation;

namespace Desktop.Client.ModelViews.Interface
{
   [InheritedExport]
   public interface IContactView : IView
   {
   }

   [InheritedExport]
   public interface IContactDetailView : IContactView, IDetailView
   {
   }

   [InheritedExport]
   public interface IContactListView : IContactView, IListView
   {
   }
}
