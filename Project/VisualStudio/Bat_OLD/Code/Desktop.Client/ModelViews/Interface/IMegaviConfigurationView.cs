﻿using System.ComponentModel.Composition;
using Common.Desktop.Presentation;

namespace Desktop.Client.ModelViews.Interface
{
   [InheritedExport]
   public interface IMegaviConfigurationView : IView
   {
   }

   [InheritedExport]
   public interface IMegaviConfigurationDetailView : IMegaviConfigurationView, IDetailView
   {
   }
}
