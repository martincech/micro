﻿using System.ComponentModel.Composition;
using Common.Desktop.Presentation;

namespace Desktop.Client.ModelViews.Interface
{
   [InheritedExport]
   public interface ICalibrationView : IView
   {
   }

   [InheritedExport]
   public interface ICalibrationDetailView : ICalibrationView, IDetailView
   {
   }
}
