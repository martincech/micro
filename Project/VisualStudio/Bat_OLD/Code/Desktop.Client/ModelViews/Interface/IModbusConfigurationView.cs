﻿using System.ComponentModel.Composition;
using Common.Desktop.Presentation;

namespace Desktop.Client.ModelViews.Interface
{
   [InheritedExport]
   public interface IModbusConfigurationView : IView
   {
   }

   [InheritedExport]
   public interface IModbusConfigurationDetailView : IModbusConfigurationView, IDetailView
   {
   }
}
