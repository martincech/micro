﻿using System.ComponentModel.Composition;
using Common.Desktop.Presentation;

namespace Desktop.Client.ModelViews.Interface
{
   [InheritedExport]
   public interface IWeighingConfigurationView : IView
   {
   }

   [InheritedExport]
   public interface IWeighingConfigurationDetailView : IWeighingConfigurationView, IDetailView
   {
   }

   [InheritedExport]
   public interface IWeighingConfigurationListView : IWeighingConfigurationView, IListView
   {
   }
}
