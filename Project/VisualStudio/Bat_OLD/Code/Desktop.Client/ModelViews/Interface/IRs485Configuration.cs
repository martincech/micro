﻿using System.ComponentModel.Composition;
using Common.Desktop.Presentation;

namespace Desktop.Client.ModelViews.Interface
{
   [InheritedExport]
   public interface IRs485ConfigurationView : IView
   {
   }

   [InheritedExport]
   public interface IRs485ConfigurationDetailView : IRs485ConfigurationView, IDetailView
   {
   }
}
