﻿using System.ComponentModel.Composition;
using Common.Desktop.Presentation;

namespace Desktop.Client.ModelViews.Interface
{
   /// <summary>
   /// Country view base
   /// </summary>
   [InheritedExport]
   public interface ICountryView : IView
   {
   }

   /// <summary>
   /// Country settings detail view
   /// </summary>
   [InheritedExport]
   public interface ICountryDetailView : ICountryView, IDetailView
   {
   }
}