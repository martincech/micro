﻿using Desktop.Client.ModelViews.Interface;

namespace Desktop.Client.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for WeightUnitView.xaml
   /// </summary>
   public partial class WeightUnitView : IWeightUnitDetailView
   {
      public WeightUnitView()
      {
         InitializeComponent();
      }
   }
}
