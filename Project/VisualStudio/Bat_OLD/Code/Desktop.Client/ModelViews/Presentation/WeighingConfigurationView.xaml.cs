﻿using Desktop.Client.ModelViews.Applications;
using Desktop.Client.ModelViews.Interface;
using System;
using System.Linq;
using System.Collections.ObjectModel;
using ViewModels;
using System.Windows.Data;
using System.Windows.Controls;

namespace Desktop.Client.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for WeighingConfigurationView.xaml
   /// </summary>
   public partial class WeighingConfigurationView : IWeighingConfigurationDetailView
   {
      #region Fields and properties

      public ObservableCollection<CorrectionCurve> corrCurveList;
      public ObservableCollection<GrowthCurve> growthCurveList;

      #endregion

      #region Constructors

      public WeighingConfigurationView()
      {
         InitializeComponent();

         CorrectionCurveIndexComboBox.SelectionChanged += CorrectionCurveIndexComboBox_SelectionChanged;
         StandardMaleCurveIndexComboBox.SelectionChanged += StandardMaleCurveIndexComboBox_SelectionChanged;
         StandardFemaleCurveIndexComboBox.SelectionChanged += StandardFemaleCurveIndexComboBox_SelectionChanged;   
      }

      #endregion

      #region Private helpers

      #region ComboBoxes Selection changed

      private void StandardFemaleCurveIndexComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
      {
         if (StandardFemaleCurveIndexComboBox.SelectedIndex == 0 || StandardFemaleCurveIndexComboBox.SelectedIndex == -1)
         {
            StandardFemaleCurveIndexComboBox.SelectedItem = growthCurveList[0];
         }
      }

      private void StandardMaleCurveIndexComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
      {
         if (StandardMaleCurveIndexComboBox.SelectedIndex == 0 || StandardMaleCurveIndexComboBox.SelectedIndex == -1)
         {
            StandardMaleCurveIndexComboBox.SelectedItem = growthCurveList[0];
         }
      }

      private void CorrectionCurveIndexComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
      {      
         if (CorrectionCurveIndexComboBox.SelectedIndex == 0 || CorrectionCurveIndexComboBox.SelectedIndex == -1)
         {
            CorrectionCurveIndexComboBox.SelectedItem = corrCurveList[0];
         }
      }

      #endregion

      private void CopyPredefinedWeighingButton_Click(object sender, System.Windows.RoutedEventArgs e)
      {
         CopyPredefinedWeighingExpander.IsExpanded = false;
      }

      private void GrowthCurveIndexComboBox_IsEnabledChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
      {
         WeighingConfigurationViewModel vm = DataContext as WeighingConfigurationViewModel;
         if (vm == null || vm.Model == null) { return; }

         if (!MaleGrowthCurveIndexComboBox.IsEnabled && 
             vm.Model.TargetWeight.Mode == Bat2Library.PredictionModeE.PREDICTION_MODE_AUTOMATIC)
         {
            vm.Model.MaleGender.GrowthCurveIndex = -1;
         }

         if (!FemaleGrowthCurveIndexComboBox.IsEnabled && 
             vm.Model.TargetWeight.Mode == Bat2Library.PredictionModeE.PREDICTION_MODE_AUTOMATIC)
         {
            vm.Model.FemaleGender.GrowthCurveIndex = -1;
         }       
      }

      /// <summary>
      /// Events after user control is load.
      /// Set Model as first item from collection Models.
      /// Set comboBox source.
      /// </summary>
      private void WCView_Loaded(object sender = null, System.Windows.RoutedEventArgs e = null)
      {
         WeighingConfigurationViewModel vm = DataContext as WeighingConfigurationViewModel;
         if (!vm.IsPredefinedWeighing)
         {
            vm.Model = vm.Models.FirstOrDefault();           
         }

         //Set correction curve index comboBox source       
         corrCurveList = GetListOfCurvesWithDefaultItem<CorrectionCurve>(vm.CorrectionCurves);
         CorrectionCurveIndexComboBox.ItemsSource = corrCurveList;     

         //Set standard curve index comboBox source     
         growthCurveList = GetListOfCurvesWithDefaultItem<GrowthCurve>(vm.GrowthCurves);
         StandardFemaleCurveIndexComboBox.ItemsSource = growthCurveList;
         StandardMaleCurveIndexComboBox.ItemsSource = growthCurveList;      
      }

      private ObservableCollection<T> GetListOfCurvesWithDefaultItem<T>(ObservableCollection<T> curves) where T : Curve, new()
      {
         ObservableCollection<T> list = new ObservableCollection<T>();
         T newCurve = new T() { Name = Desktop.Client.Properties.Resources.Empty };
         list.Add(newCurve);
         foreach(T c in curves)
         {
            list.Add(c);
         }

         return list;
      }

      #endregion
   }
}
