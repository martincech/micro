﻿using Desktop.Client.ModelViews.Interface;

namespace Desktop.Client.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for ContactView.xaml
   /// </summary>
   public partial class ContactView : IContactDetailView
   {
      public ContactView()
      {
         InitializeComponent();
      }
   }
}
