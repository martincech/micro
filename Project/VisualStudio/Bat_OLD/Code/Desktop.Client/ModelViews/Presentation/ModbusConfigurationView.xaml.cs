﻿using Desktop.Client.ModelViews.Interface;

namespace Desktop.Client.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for ModbusConfigurationView.xaml
   /// </summary>
   public partial class ModbusConfigurationView : IModbusConfigurationDetailView
   {
      public ModbusConfigurationView()
      {
         InitializeComponent();
      }
   }
}
