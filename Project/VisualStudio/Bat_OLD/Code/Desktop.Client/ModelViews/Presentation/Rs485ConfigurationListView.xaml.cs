﻿using Desktop.Client.ModelViews.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Desktop.Client.ModelViews.Presentation
{
    /// <summary>
    /// Interaction logic for Rs485ConfigurationListView.xaml
    /// </summary>
    public partial class Rs485ConfigurationListView : IRs485ConfigurationDetailView
    {
        public Rs485ConfigurationListView()
        {
            InitializeComponent();
        }
    }
}
