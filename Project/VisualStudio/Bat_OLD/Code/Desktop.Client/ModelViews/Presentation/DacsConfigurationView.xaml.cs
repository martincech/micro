﻿using Desktop.Client.ModelViews.Interface;

namespace Desktop.Client.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for DacsConfiguration.xaml
   /// </summary>
   public partial class DacsConfigurationView : IDacsConfigurationDetailView
   {
      public DacsConfigurationView()
      {
         InitializeComponent();
      }
   }
}
