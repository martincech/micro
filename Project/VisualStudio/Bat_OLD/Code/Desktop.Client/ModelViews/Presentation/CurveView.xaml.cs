﻿using Common.Library.Observable;
using Common.Presentation;
using Desktop.Client.ModelViews.Converters;
using Desktop.Client.ModelViews.Interface;
using OxyPlot;
using OxyPlot.Annotations;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Reflection;
using System.Windows;
using ViewModels;

namespace Desktop.Client.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for CurveView.xaml
   /// </summary>
   [PartCreationPolicy(CreationPolicy.NonShared)]
   public partial class CurveView : ICurveDetailView
   {
      #region fields and properties

      public PlotModel Model { get; set; }    

      private CollectionsSynchronizer synchro;
      private LineSeries lineSerie;             // line serie for correct points
      private TextAnnotation annotation;
      private RectangleAnnotation defaultAnnotation;
      private RectangleAnnotation yMaxAnnotation;
      private RectangleAnnotation yMinAnnotation;

      private string axisXName;
      private string axisYName;
      private double axisXAlignment;
      private double axisYAlignment;
      private double transfer;               // transfer unit
      private int round;                     // size of fractional part
      private int indexOfPointToMove;
      private bool checkPoint;               // check point when it has inserted or removed
      private DataPoint? lastValidPosition;  // last insert/move point, that is at correct position    
      private DataPointToCurvePointConverter dataPointToCurvePointConverter;

      //private object model;
      private ObservableCollection<CurvePoint> vmPoints;
      private ObservableCollection<DataPoint> DataPoints { get; set; }
      private string outOfRangeArea = "Out of range"; //TODO: use string from resources

      #endregion

      #region Public interfaces

      #region Constructor

      public CurveView()
      {
         DataPoints = new ObservableCollection<DataPoint>();
         //vmPoints = new ObservableCollection<CurvePoint>();
         DataPoints.CollectionChanged += (s, e) => Update();
         round = 0;

         Model = new PlotModel { LegendSymbolLength = 24 };
         InitLineSerie();
         InitGraph();

         InitializeComponent();

         indexOfPointToMove = -1;
         checkPoint = false;
         lastValidPosition = null;

         // Subscribe to the mouse down event on the line series
         Model.MouseDown += Model_MouseDown;
         Model.MouseMove += Model_MouseMove;
         Model.MouseUp += Model_MouseUp;
      }

      #endregion

      #endregion

      #region Mouse and Key events

      /// <summary>
      /// Mouse down event on lineserie
      /// </summary>
      private void lineSerie_MouseDown(object sender, OxyMouseDownEventArgs e)
      {
         // only handle the left mouse button (right button can still be used to pan)
         if (e.ChangedButton == OxyMouseButton.Left)
         {
            if (DataPoints.Count == 0)
               return;

            int indexOfNearestPoint = (int)Math.Round(e.HitTestResult.Index);
            if (indexOfNearestPoint >= DataPoints.Count)
            {
               indexOfNearestPoint = DataPoints.Count - 1;
            }

            var nearestPoint = lineSerie.Transform(DataPoints[indexOfNearestPoint]);
            // Check if we are near a point
            if ((nearestPoint - e.Position).Length < 10)
            {  // Start editing this point
               indexOfPointToMove = indexOfNearestPoint;
               lastValidPosition = DataPoints[indexOfPointToMove];
               lineSerie.KeyDown += lineSerie_KeyDown;
            }
            else
            {  // otherwise create a point on the current line segment    
               CreateNewPointOnLineSegment((int)e.HitTestResult.Index + 1, e.Position);
            }

            ViewAnnotation(e.Position);
            // Change the linestyle while editing
            lineSerie.LineStyle = LineStyle.DashDot;
            // Remember to refresh/invalidate of the plot
            Model.InvalidatePlot(true);
            // Set the event arguments to handled - no other handlers will be called.
            e.Handled = true;
         }
      }

      /// <summary>
      /// Mouse move event on Oxyplot line serie
      /// </summary>  
      private void lineSerie_MouseMove(object sender, OxyMouseEventArgs e)
      {
         if (indexOfPointToMove >= 0)
         {  // Move the point being edited
            MovePointAtGraph(e.Position);
            ViewAnnotation(e.Position);
            Model.InvalidatePlot(true);
            e.Handled = true;
         }
      }

      /// <summary>
      /// Mouse up event on Oxyplot line serie
      /// </summary>
      private void lineSerie_MouseUp(object sender, OxyMouseEventArgs e)
      {
         // Stop editing
         if (indexOfPointToMove >= 0)
         {
            SetViewModelLastPointProperty(DataPoints[indexOfPointToMove]);

            if (!IsValidPoint(indexOfPointToMove) || PointHasErrors())
            {
               if (lastValidPosition != null)
               {
                  DataPoints[indexOfPointToMove] = (DataPoint)lastValidPosition;
                  lastValidPosition = null;
               }
               else
               {
                  DataPoints.RemoveAt(indexOfPointToMove);
               }
            }
         }

         indexOfPointToMove = -1;
         lineSerie.LineStyle = LineStyle.Solid;

         Model.Annotations.Remove(annotation);
         Model.InvalidatePlot(true);
         e.Handled = false;

         lineSerie.KeyDown -= lineSerie_KeyDown;
         UpdateCommands();
      }

      /// <summary>
      /// Key down event on lineserie
      /// </summary>  
      private void lineSerie_KeyDown(object sender, OxyKeyEventArgs e)
      {
         if (e.Key == OxyKey.Delete && indexOfPointToMove >= 0)
         {
            DeleteSelectedPoint();
            Model.Annotations.Remove(annotation);
            Model.InvalidatePlot(true);
            UpdateCommands();
         }
      }

      /// <summary>
      /// Mouse down event on Oxyplot model
      /// </summary>   
      private void Model_MouseDown(object sender, OxyMouseDownEventArgs e)
      {
         if (e.ChangedButton == OxyMouseButton.Left)
         {  // Add a point to the line series  
            AddNewPointToLineSerie(e.Position);
            lineSerie.LineStyle = LineStyle.DashDot;
            UpdateCommands();

            ViewAnnotation(e.Position);
            Model.InvalidatePlot(true);
            e.Handled = true;
         }
         else if (e.ChangedButton == OxyMouseButton.Middle)
         {  //zoom to max
            GraphZoomToMax();
         }
      }

      private void Model_MouseMove(object sender, OxyMouseEventArgs e)
      {
         if (indexOfPointToMove >= 0)
         {
            MovePointAtGraph(e.Position);
            ViewAnnotation(e.Position);
            Model.InvalidatePlot(true);
            e.Handled = true;
         }
      }

      /// <summary>
      /// Mouse up event on Oxyplot model
      /// </summary>    
      private void Model_MouseUp(object sender, OxyMouseEventArgs e)
      {
         if (DataPoints.Count != 0)
         {
            int index = DataPoints.Count - 1;
            SetViewModelLastPointProperty(DataPoints[index]);

            if (checkPoint == true &&
               (!IsValidPoint(index) || PointHasErrors()))
            {  // delete last insert point because is invalid
               DataPoints.RemoveAt(index);
               checkPoint = false;
            }
         }

         if (PointHasErrors())
         {
            SetViewModelLastPointProperty(new DataPoint());
         }
         indexOfPointToMove = -1;
         lineSerie.LineStyle = LineStyle.Solid;
         Model.Annotations.Remove(annotation);
         Model.InvalidatePlot(true);
         e.Handled = false;
         UpdateCommands();
      }

      #endregion

      #region Private helpers

      /// <summary>
      /// Change point position in DataPoints collection.
      /// </summary>
      /// <param name="position">position of point which is moved</param>
      private void MovePointAtGraph(ScreenPoint position)
      {
         DataPoint point = lineSerie.InverseTransform(position);
         DataPoints[indexOfPointToMove] = new DataPoint(Math.Round(point.X), Math.Round(point.Y, round));
      }

      /// <summary>
      /// Zoom lineserie's axis to max points range.
      /// </summary>     
      private void GraphZoomToMax()
      {
         GraphZoomToMax(null);
      }

      private void GraphZoomToMax(ObservableCollection<DataPoint> points)
      {
         if (DataPoints.Count == 0 && (points == null || points.Count == 0))
         {
            lineSerie.XAxis.Zoom(0, 20);
            lineSerie.YAxis.Zoom(0, 20);
            return;
         }

         double[] range = PointsRange(points);
         double newY = 0.0;
         double newX = 0.0;
         double precision = 0.05;         
         newY = (lineSerie.YAxis.ActualMaximum - lineSerie.YAxis.ActualMinimum) * precision;
         newX = (lineSerie.XAxis.ActualMaximum - lineSerie.XAxis.ActualMinimum) * precision;

         do
         {
            lineSerie.XAxis.Zoom(range[0] - newX, range[1] + newX);
            lineSerie.YAxis.Zoom(range[2] - newY, range[3] + newY);

            axisXAlignment = newX;
            axisYAlignment = newY;

            newY = (lineSerie.YAxis.ActualMaximum - lineSerie.YAxis.ActualMinimum) * precision;
            newX = (lineSerie.XAxis.ActualMaximum - lineSerie.XAxis.ActualMinimum) * precision;
         } while (Math.Abs(axisXAlignment - newX) > precision || Math.Abs(axisYAlignment - newY) > precision);
      }

      /// <summary>
      /// Insert new point to graph
      /// </summary>
      /// <param name="position">point position</param>
      private void AddNewPointToLineSerie(ScreenPoint position)
      {
         DataPoint point = lineSerie.InverseTransform(position);

         // check if point is valid     
         SetViewModelLastPointProperty(new DataPoint(Math.Round(point.X), Math.Round(point.Y, round)));
         if (!PointHasErrors())
         {
            DataPoints.Add(new DataPoint(Math.Round(point.X), Math.Round(point.Y, round)));
            checkPoint = true;
            indexOfPointToMove = DataPoints.Count - 1;
         }
      }

      /// <summary>
      /// Get flag presents view model validation result.
      /// </summary>
      /// <returns>true - error in validation; false - no errors</returns>
      private bool PointHasErrors()
      {
         var getErr = DataContext.GetType().GetProperty("HasErrors", BindingFlags.Public | BindingFlags.Instance);
         Debug.Assert(getErr != null);
         return (bool)getErr.GetGetMethod().Invoke(DataContext, null);
      }

      /// <summary>
      /// Set view model property LastEditPoint by new local point
      /// </summary>
      /// <param name="point">new/edited point</param>
      private void SetViewModelLastPointProperty(DataPoint point)
      {
         var prop = DataContext.GetType().GetProperty("LastEditPoint", BindingFlags.Public | BindingFlags.Instance);
         Debug.Assert(prop != null && prop.CanWrite);

         DataPointToCurvePointConverter pointConverter = new DataPointToCurvePointConverter();
         pointConverter.transfer = transfer;
         object cPoint = pointConverter.Convert(point, typeof(CurvePoint), null, null);

         prop.SetValue(DataContext, cPoint);
      }

      /// <summary>
      /// Insert new point to graph on existing line segment
      /// </summary>
      /// <param name="index">index where point will be save.</param>
      /// <param name="position">point position</param>
      private void CreateNewPointOnLineSegment(int index, ScreenPoint position)
      {
         DataPoint insertPoint = lineSerie.InverseTransform(position);
         DataPoints.Insert(index, new DataPoint(Math.Round(insertPoint.X), Math.Round(insertPoint.Y, round)));

         indexOfPointToMove = index;
         lastValidPosition = null;
      }

      /// <summary>
      /// Delete selected point in graph.
      /// </summary>
      private void DeleteSelectedPoint()
      {
         DataPoints.RemoveAt(indexOfPointToMove);
         indexOfPointToMove = -1;
         lastValidPosition = null;
      }

      /// <summary>
      /// Check if last modified/added point is valid
      /// </summary>
      /// <param name="index">index of point</param>
      /// <returns>true - point is valid; false - invalid</returns>
      private bool IsValidPoint(int index)
      {
         if (index > 0 && DataPoints[index].X <= DataPoints[index - 1].X)
         {
            return false;
         }

         if ((index < DataPoints.Count - 1) &&
            DataPoints[index].X >= DataPoints[index + 1].X)
         {
            return false;
         }

         return true;
      }

      /// <summary>
      /// Check if last modified/added point is valid
      /// </summary>
      /// <param name="point">invalid point is check if is correct now</param>
      /// <returns>true - point is valid; false - invalid</returns>
      private bool IsValidPoint(DataPoint point)
      {
         int last = DataPoints.Count - 1;

         if (point.X < 0 || (DataPoints.Count > 0 && point.X <= DataPoints[last].X))
         {
            return false;
         }

         return true;
      }

      /// <summary>
      /// Gets range of points used in the graph.
      /// </summary>
      /// <returns>range{ minX, maxX, minY, maxY }</returns>
      private double[] PointsRange(ObservableCollection<DataPoint> points)
      {
         double minX = double.MaxValue;
         double minY = double.MaxValue;
         double maxX = double.MinValue;
         double maxY = double.MinValue;

         if (points == null)
         {
            points = DataPoints;
         }

         foreach (DataPoint point in points)
         {
            double x = point.X;
            double y = point.Y;

            if (x < minX)
               minX = x;
            if (x > maxX)
               maxX = x;

            if (y < minY)
               minY = y;
            if (y > maxY)
               maxY = y;
         }

         return new double[4] { minX, maxX, minY, maxY };
      }

      /// <summary>
      /// View point coordinates annotation.
      /// </summary>
      /// <param name="position">position of selected point</param>
      private void ViewAnnotation(ScreenPoint position)
      {
         DataPoint point = lineSerie.InverseTransform(position);
         double alignment = (lineSerie.YAxis.ActualMaximum - lineSerie.YAxis.ActualMinimum) * 0.3;
         point.Y = Math.Round(point.Y, round) + alignment;
         point.X = Math.Round(point.X);

         annotation.TextPosition = point;
         annotation.TextHorizontalAlignment = OxyPlot.HorizontalAlignment.Center;
         annotation.TextVerticalAlignment = OxyPlot.VerticalAlignment.Top;

         if (!Model.Annotations.Contains(annotation))
         {
            Model.Annotations.Add(annotation);
         }

         string x = axisXName ?? "X";
         string y = axisYName ?? "Y";

         annotation.Text = x + ": " + Math.Round(point.X) + "\n" + y + ": " + Math.Round(point.Y - alignment, round);
      }

      private void pView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
      {
         var oldInpc = e.OldValue as INotifyPropertyChanged;
         if (oldInpc != null)
         {
            oldInpc.PropertyChanged -= inpc_PropertyChanged;
         }

         var inpc = DataContext as INotifyPropertyChanged;
         if (inpc != null)
         {
            inpc.PropertyChanged += inpc_PropertyChanged;
         }
      }

      /// <summary>
      /// Synchronize view model property (sender) with local property
      /// </summary>
      private void inpc_PropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         switch (e.PropertyName)
         {
            case "Model":
               if (synchro != null)
               {
                  synchro.List = null;
               }
               DataPoints.Clear();
            
               var prop = DataContext.GetType().GetProperty("PointsView", BindingFlags.Instance | BindingFlags.Public);
               Debug.Assert(prop != null);
               vmPoints = prop.GetGetMethod().Invoke(DataContext, null) as ObservableCollection<CurvePoint>;
               dataPointToCurvePointConverter = new DataPointToCurvePointConverter();
               dataPointToCurvePointConverter.transfer = transfer;
             
               if (synchro == null)
               {
                   synchro = new CollectionsSynchronizer(vmPoints, DataPoints, dataPointToCurvePointConverter);                 
               }
               else 
               {
                   synchro.List = vmPoints;
               }
               /*synchro.List = vmPoints;
               synchro.DependentList = DataPoints;
               synchro.Converter = dataPointToCurvePointConverter;*/


               if (lineSerie.XAxis != null && lineSerie.YAxis != null)
               {
                  GraphZoomToMax();
               }
               Model.InvalidatePlot(true);
               break;       
         }
      }

      private void SetAxisMaximumYAnnotation(object sender)
      {
         double ymax = GetPropertyByReflection<double>(sender, "YMax") * transfer;

         if (Math.Round(ymax) > 0)
         {
            //Model.Annotations.Add(new LineAnnotation() { Type = LineAnnotationType.Horizontal, Y = ymax, Color = OxyColors.Red });
            yMaxAnnotation.MinimumY = ymax;
            Model.Annotations.Remove(yMaxAnnotation);
            Model.Annotations.Add(yMaxAnnotation);
            Model.Axes[1].AbsoluteMaximum = ymax * 1.05;
         }
      }

      private void SetAxisMinimumYAnnotation(object sender)
      {
         double ymin = GetPropertyByReflection<double>(sender, "YMin") * transfer;
         Model.Annotations.Remove(defaultAnnotation);
         //Model.Annotations.Add(new LineAnnotation() { Type = LineAnnotationType.Horizontal, Y = ymin, Color = OxyColors.Red });
         yMinAnnotation.MaximumY = ymin;
         Model.Annotations.Remove(yMinAnnotation);
         Model.Annotations.Add(yMinAnnotation);

         if (Math.Round(ymin) == 0)
         {
            ymin = -2;
         }
         Model.Axes[1].AbsoluteMinimum = ymin * 1.05;
      }

      /// <summary>
      /// Retrieve property value by reflection from view model.
      /// </summary>
      /// <typeparam name="T">value type</typeparam>
      /// <param name="sender">object</param>
      /// <param name="name">property name</param>
      /// <returns>value</returns>
      private T GetPropertyByReflection<T>(object sender, string name)
      {
         var prop = sender.GetType().GetProperty(name, BindingFlags.Public | BindingFlags.Instance);
         Debug.Assert(prop != null);
         return (T)prop.GetGetMethod().Invoke(sender, null);
      }


      private void UpdateCommands()
      {        
         //refresh plot
         Model.InvalidatePlot(true);
      }

      /// <summary>
      /// Update when Datapoints changed
      /// </summary>
      private void Update()
      {  
         /*
         if (model == null)
         {
            model = GetPropertyByReflection<object>(DataContext, "Model");
         }
         else
         {
            object newModel = GetPropertyByReflection<object>(DataContext, "Model");
            if (newModel != model)
            {  //change curve (model)
               model = newModel;

               axisXAlignment = 1.0;
               axisYAlignment = 1.0;

               ObservableCollection<DataPoint> tmpPoints = new ObservableCollection<DataPoint>();
               DataPointToCurvePointConverter converter = new DataPointToCurvePointConverter();
               converter.transfer = transfer;

               // Zoom to maximum for current points.
               // DataPoints is not synchronize with view model at the moment
               // therefore are use tmpPoints.            
               var points = (model as Curve).Points;             
               if (points != null)
               {
                  foreach (CurvePoint cPoint in points)
                  {
                     DataPoint p = (DataPoint)converter.ConvertBack(cPoint, typeof(DataPoint), null, null);
                     tmpPoints.Add(p);
                  }
               }             
               //GraphZoomToMax(tmpPoints);
            }
         }*/
         UpdateCommands();
      }

      private void InitLineSerie()
      {
         if (Model == null)
         {
            throw new Exception("Graph model doesn't initialize yet");
         }

         lineSerie = new LineSeries
         {
            Color = OxyColors.Green,
            MarkerType = MarkerType.Square,
            MarkerSize = 5,
            MarkerStroke = OxyColors.White,
            MarkerFill = OxyColors.Green,
            MarkerStrokeThickness = 1.5           
         };
         lineSerie.ItemsSource = DataPoints;      
         Model.Series.Add(lineSerie);

         lineSerie.MouseDown += lineSerie_MouseDown;
         lineSerie.MouseMove += lineSerie_MouseMove;
         lineSerie.MouseUp += lineSerie_MouseUp;
      }

      private void InitGraph()
      {
         if (Model == null)
         {
            throw new Exception("Graph model doesn't initialize yet");
         }

         axisXAlignment = 1.0;
         axisYAlignment = 1.0;
         annotation = new TextAnnotation();
         annotation.Background = OxyColors.SeaShell;
         //Model.Annotations.Add(new LineAnnotation() { Type = LineAnnotationType.Vertical, X = 0, Color = OxyColors.Blue });
         Model.Annotations.Add(new RectangleAnnotation { MaximumX = 0.0, Text = outOfRangeArea, TextRotation = 270, Fill = OxyColor.FromAColor(80, OxyColors.Gray) });

         // X-axis
         Model.Axes.Add(new LinearAxis
         {
            Position = AxisPosition.Bottom,
            AbsoluteMinimum = -2,
            MinimumRange = 20,
         });
         // Y-axis
         Model.Axes.Add(new LinearAxis
         {
            Position = AxisPosition.Left,
            MinimumRange = 5,
            IntervalLength = 20,          
         });

         //defaultAnnotation = new LineAnnotation() { Type = LineAnnotationType.Horizontal, Y = 0, Color = OxyColors.Red };
         defaultAnnotation = new RectangleAnnotation() { MaximumY = 0, Text = outOfRangeArea, Fill = OxyColor.FromAColor(70, OxyColors.Red) };
         Model.Annotations.Add(defaultAnnotation);
         Model.Axes[1].AbsoluteMinimum = -5;

         yMaxAnnotation = new RectangleAnnotation
         {
            Text = outOfRangeArea,
            Fill = OxyColor.FromAColor(70, OxyColors.Red)
         };
         yMinAnnotation = new RectangleAnnotation
         {
            Text = outOfRangeArea,
            Fill = OxyColor.FromAColor(70, OxyColors.Red)
         };
      }

      #endregion

      /// <summary>
      /// Set graph's parameter after control is loaded.
      /// </summary>
      private void Load(object sender, RoutedEventArgs e)
      {
         //Transfer
         transfer = GetPropertyByReflection<double>(DataContext, "Transfer");
         if (transfer > 0)
         {
            SetAxisMaximumYAnnotation(DataContext);
            SetAxisMinimumYAnnotation(DataContext);
         }

         round = 0;
         if (transfer == 0.1)
         {
            round = 1;
         }

         //Model.Points   
         var prop = DataContext.GetType().GetProperty("PointsView", BindingFlags.Instance | BindingFlags.Public);
         Debug.Assert(prop != null);

         vmPoints = prop.GetGetMethod().Invoke(DataContext, null) as ObservableCollection<CurvePoint>;
         dataPointToCurvePointConverter = new DataPointToCurvePointConverter();
         dataPointToCurvePointConverter.transfer = transfer;

         if (synchro == null)
         {
            synchro = new CollectionsSynchronizer(vmPoints, DataPoints, dataPointToCurvePointConverter);
         }

         //DescriptionX":
         axisXName = GetPropertyByReflection<string>(DataContext, "DescriptionX");
         Model.Axes[0].Title = axisXName;
               
         //DescriptionY
         axisYName = GetPropertyByReflection<string>(DataContext, "DescriptionY");
         Model.Axes[1].Title = axisYName;
               
         //UnitX
         Model.Axes[0].Unit = GetPropertyByReflection<string>(DataContext, "UnitX");
              
         //UnitY
         Model.Axes[1].Unit = GetPropertyByReflection<string>(DataContext, "UnitY");
               
         //YMax
         SetAxisMaximumYAnnotation(DataContext);
             
         //YMin
         SetAxisMinimumYAnnotation(DataContext);
               
         //XMax
         double xmax = GetPropertyByReflection<double>(DataContext, "XMax");
         if (xmax > 0.0)
         {
            Model.Annotations.Add(new RectangleAnnotation { MinimumX = xmax, Text = outOfRangeArea, TextRotation = 270, Fill = OxyColor.FromAColor(80, OxyColors.Gray) });
            Model.Axes[0].AbsoluteMaximum = xmax * 1.05;
         }        
      }
   }
}
