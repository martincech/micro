﻿using Desktop.Client.ModelViews.Interface;

namespace Desktop.Client.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for ContactListView.xaml
   /// </summary>
   public partial class ContactListView : IContactListView
   {
      public ContactListView()
      {
         InitializeComponent();
      }
   }
}
