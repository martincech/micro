﻿using Desktop.Client.ModelViews.Interface;

namespace Desktop.Client.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for DisplayConfigurationView.xaml
   /// </summary>
   public partial class DisplayConfigurationView : IDisplayConfigurationDetailView
   {
      public DisplayConfigurationView()
      {
         InitializeComponent();
      }
   }
}
