﻿using Desktop.Client.ModelViews.Interface;

namespace Desktop.Client.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for CalibrationView.xaml
   /// </summary>
   public partial class CalibrationView : ICalibrationDetailView
   {
      public CalibrationView()
      {
         InitializeComponent();
      }

      private void dataGrid_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
      {
         dataGrid.UnselectAllCells();
      }
   }
}
