﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Desktop.Client.ToolBarService.Interface
{
   public interface IToolBarService : INotifyPropertyChanged
   {
      /// <summary>
      /// Adds the specified tool bar node.
      /// </summary>
      /// <param name="node">The tool bar node to add.</param>
      void AddToolBarNode(IToolBarNode node);

      /// <summary>
      /// Adds the specified tool bar nodes.
      /// </summary>
      /// <param name="nodes">The tool bar nodes to add.</param>
      void AddToolBarNode(IEnumerable<IToolBarNode> nodes);

      /// <summary>
      /// Remove single tool bar node.
      /// </summary>
      /// <param name="node">The node to be removed</param>
      void RemoveToolBarNode(IToolBarNode node);

      /// <summary>
      /// Remove multiple tool bar nodes.
      /// </summary>
      /// <param name="nodes">Nodes to be removed</param>
      void RemoveToolBarNode(IEnumerable<IToolBarNode> nodes);

      /// <summary>
      /// Remove all tool bar nodes
      /// </summary>
      void RemoveToolBarNode();
   }
}