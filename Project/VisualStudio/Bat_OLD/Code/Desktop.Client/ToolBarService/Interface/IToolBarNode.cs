﻿using System;
using System.ComponentModel;

namespace Desktop.Client.ToolBarService.Interface
{
   public interface IToolBarNode : INotifyPropertyChanged
   {
      /// <summary>
      /// Gets the name of the node.
      /// </summary>
      object ViewDelegate { get; }

      /// <summary>
      /// Gets the toolbar tip
      /// </summary>
      object ToolTip { get; }

      /// <summary>
      /// Action to be executed when toolbar node selected
      /// </summary>
      Action SelectAction { get; }

      /// <summary>
      /// Can be this node selected?
      /// </summary>
      bool CanSelect { get; set; }
   }
}