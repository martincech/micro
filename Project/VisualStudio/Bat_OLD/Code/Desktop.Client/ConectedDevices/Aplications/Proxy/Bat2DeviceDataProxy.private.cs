﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using Connection.Interface.Domain;
using ViewModels;
using Common.Desktop.Presentation;
using ArchiveItem = ViewModels.ArchiveItem;
using Contact = ViewModels.Contact;
using Country = ViewModels.Country;
using DisplayConfiguration = ViewModels.DisplayConfiguration;
using GsmMessage = ViewModels.GsmMessage;
using WeighingConfiguration = ViewModels.WeighingConfiguration;
using WeighingPlan = ViewModels.WeighingPlan;

namespace Desktop.Client.ConectedDevices.Aplications.Proxy
{
   public partial class Bat2DeviceDataProxy
   {
      #region Event handlers

      protected virtual void OnStatusMessageHandler(string e)
      {
         var handler = StatusMessageHandler;

         if (handler != null)
         {
            var msg = string.Format("{0}(SN: {1}) {2}",
               origin.Configuration.DeviceInfo.Name,
               SerialNumber, e);
            handler(this, msg);
         }
      }

      #endregion

      #region Mapping methods

      private void MapModelFromOrigin([CallerMemberName] string propertyName = null)
      {
         DispatcherHelper.RunAsync(() =>
         {
            if (propertyName == "Configuration" || propertyName == "")
            {
               InitConfiguration();
               if (GetIsLoaded(() => Configuration))
               {
                  Country.Map(Configuration.Country);
                  WeightUnit.Map(Configuration.WeightUnits);
                  DisplayConfiguration.Map(Configuration.DisplayConfiguration);
                  GsmMessage.Map(Configuration.GsmMessage);
                  EthernetConfiguration.Map(Configuration.EthernetOptions);
                  Bat2Identification.Map(Configuration.VersionInfo, Configuration.DeviceInfo);
                  Calibration.Map(Configuration.PlatformCalibration);
                  WeighingConfiguration.Map(Configuration.WeighingConfiguration);
                  ModbusConfiguration.Map(Configuration.ModbusOptions);
                  MegaviConfiguration.Map(Configuration.MegaviOptions);
                  SmsGateConfiguration.Map(Configuration.SmsGateOptions);
                  DacsConfiguration.Map(Configuration.DacsOptions);
                  EthernetConfiguration.Map(Configuration.EthernetOptions);

                  MapCollection(Configuration.Rs485Options, ref rs485Configuration);
               }

              
            }

            if (propertyName == "Context" || propertyName == "")
            {
               InitContext();
               if (GetIsLoaded(() => Context))
               {
                  Weighing.Map(Context.WeighingContext);
               }
            }

            if (propertyName == "Archive" || propertyName == "")
            {
               MapCollection(Archive, ref archiveItems);
            }

            if (propertyName == "GrowthCurves" || propertyName == "")
            {
               MapCollection(GrowthCurves, ref growthCurveList);
            }
            if (propertyName == "CorrectionCurves" || propertyName == "")
            {
               MapCollection(CorrectionCurves, ref correctionCurveList);
            }
            if (propertyName == "Contacts" || propertyName == "")
            {
               MapCollection(Contacts, ref contactList);
            }
            if (propertyName == "WeighingPlans" || propertyName == "")
            {
               MapCollection(WeighingPlans, ref weighingPlanList);
            }

            if (propertyName == "PredefinedWeighings" || propertyName == "")
            {
               MapCollection(PredefinedWeighings, ref predefinedWeighingList);
            }
         });
      }

      private static void MapCollection<T, S>(IEnumerable<T> from, ref ObservableCollection<S> to)
         where S : class, new()
      {
         Debug.Assert(typeof (T).Assembly == typeof (Configuration).Assembly);
         Debug.Assert(typeof (S).Assembly == typeof (Bat2Identification).Assembly);

         var fromArray = @from == null ? new T[] {} : (@from as T[] ?? @from.ToArray());

         for (var i = 0; i < fromArray.Length; i++)
         {
            var tItem = fromArray[i];
            if (i <= to.Count)
            {
               dynamic sItem = new S();
               sItem.Map(tItem);
               to.Add(sItem);
            }
            else
            {
               dynamic sItem = to[i];
               sItem.Map(tItem);
            }
         }
         while (to.Count > fromArray.Length)
         {
            to.RemoveAt(to.Count - 1);
         }
      }

      #endregion

      #region Init methods

      private void InitProperties()
      {
         InitConfiguration();
         InitContext();
         InitCollections();
      }

      private void InitCollections()
      {
         if (Rs485Configuration != null)
         {
            return;
         }
         Rs485Configuration = new ObservableCollection<Rs485Configuration>();
         ArchiveItems = new ObservableCollection<ArchiveItem>();
         GrowthCurveList = new ObservableCollection<GrowthCurve>();
         CorrectionCurveList = new ObservableCollection<CorrectionCurve>();
         ContactList = new ObservableCollection<Contact>();
         WeighingPlanList = new ObservableCollection<WeighingPlan>();
         PredefinedWeighingList = new ObservableCollection<WeighingConfiguration>();
      }

      private void InitContext()
      {
         if (Weighing != null)
         {
            return;
         }
         Weighing = new Weighing();
      }

      private void InitConfiguration()
      {
         if (Country != null)
         {
            return;
         }
         Country = new Country();
         WeightUnit = new WeightUnit();
         DisplayConfiguration = new DisplayConfiguration();
         GsmMessage = new GsmMessage();
         EthernetConfiguration = new EthernetConfiguration();
         Bat2Identification = new Bat2Identification();
         Calibration = new Calibration();
         WeighingConfiguration = new WeighingConfiguration();
         ModbusConfiguration = new ModbusConfiguration();
         MegaviConfiguration = new MegaviConfiguration();
         SmsGateConfiguration = new SmsGateConfiguration();
         DacsConfiguration = new DacsConfiguration();
         EthernetConfiguration = new EthernetConfiguration();
         GsmMessage = new GsmMessage();
      }

      #endregion
   }
}