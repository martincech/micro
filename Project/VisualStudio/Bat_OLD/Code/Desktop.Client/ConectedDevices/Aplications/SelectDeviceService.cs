﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Common.Library.Messenger;
using Connection.Interface.Contract;
using Common.Desktop.Applications;
using Desktop.Client;
using Desktop.Client.ConectedDevices.Aplications.Proxy;
using Desktop.Client.ConectedDevices.Interface;
using Desktop.Client.MessageService.Interface;
using ViewModels;
using Desktop.Client.ModelViews.Applications;
using Desktop.Client.ModelViews.Presentation;
using Desktop.Client.NavigationService.Interface;
using Desktop.Client.Server;
using Desktop.Client.ToolBarService.Aplications;
using Desktop.Client.ToolBarService.Interface;
using Bat2DeviceData = Connection.Interface.Domain.Bat2DeviceData;
using Common.Library;
using Common.Services.Client;
using System.ComponentModel;
using System.ServiceModel;
using System.Reflection;
using Common.Desktop.Presentation;
using Desktop.Client.NavigationService.Aplications;
using System.Collections.ObjectModel;
using Desktop.Client.MainWindow.Presentation;

namespace Desktop.Client.ConectedDevices.Aplications
{
   public class SelectDeviceService : ISelectDeviceService
      
   {
      private readonly INavigationService navigationService;
      private readonly IMessageService messageService;
      private readonly Dictionary<INavigationNode, ViewModel> navigationNodes;     
      private INavigationNode interfaceNode;
      private Bat2DeviceDataProxy selectedDevice;
      private Dictionary<INavigationNode, CollectionsSynchronizer> nodesDataSync;
      private readonly IToolBarService toolBarService;
      private IEnumerable<Bat2DeviceDataProxy> selectedDevices;

      public SelectDeviceService(
         INavigationService navigationService,
         IToolBarService toolBarService,
         IMessageService messageService,
         ChannelFactory<IBat2CommandContract> clientFactory)
      {
         this.navigationService = navigationService;
         this.toolBarService = toolBarService;
         this.messageService = messageService;
        
         navigationNodes = new Dictionary<INavigationNode, ViewModel>();
         nodesDataSync = new Dictionary<INavigationNode, CollectionsSynchronizer>();
      }

      public Bat2DeviceDataProxy SelectedDevice
      {
         private get { return selectedDevice; }
         set
         {
            if (selectedDevice != null && value != null)
            {
               selectedDevice.PropertyChanged -= RefreshNavigationNodes;
               selectedDevice = value;
               RefreshAllNavigationNodes();
               selectedDevice.PropertyChanged += RefreshNavigationNodes;
               
            }
            else if (selectedDevice == null && value != null)
            {
               selectedDevice = value;
               selectedDevice.PropertyChanged += RefreshNavigationNodes;
               if (navigationNodes.Count > 0)
               {
                   RefreshAllNavigationNodes();
               }
               else
               {
                   CreateNavigationNodes();
               }
            }
            else 
            {
               selectedDevice.PropertyChanged -= RefreshNavigationNodes;
               selectedDevice = value;
              // ClearNavigationNodes();
            }
         }
      }

      public IEnumerable<Bat2DeviceDataProxy> SelectedDevices
      {
         private get { return selectedDevices; }
         set
         {
            if (selectedDevices != null && selectedDevices.Any())
            {
               foreach (var proxy in selectedDevices)
               {
                  //Messenger.Default.Unregister<Bat2ActionEventsContractMessage>(this, proxy.SerialNumber, msg => msg.ActionToInvoke(this));
               }
               
            }

            selectedDevices = value.ToList();

            if (selectedDevices != null && SelectedDevices.Any())
            {
              
            }
         }
      }

      private void RefreshAllNavigationNodes() 
      {
          /*
          nodesDataSync.Clear();
          navigationNodes.Clear();
          CreateNavigationNodes();
           * */

          
          
         foreach (PropertyInfo info in typeof(Bat2DeviceDataProxy).GetProperties()) 
         {
            RefreshNavigationNodes(this, new PropertyChangedEventArgs(info.Name));
           
         }
         RefreshNavigationNodes(SelectedDevice, new PropertyChangedEventArgs("IsBusy"));
        
      }

      private void RefreshNavigationNodes(object sender, PropertyChangedEventArgs e)
      {
         //Debug.Assert(sender == Model);
         KeyValuePair<INavigationNode, ViewModel> vm;
         KeyValuePair<INavigationNode, CollectionsSynchronizer> cs;
         switch (e.PropertyName)
         {
            case "Country":
               vm = navigationNodes.FirstOrDefault(kv => kv.Value is ViewModel<Country>);
               Debug.Assert(vm.Value != null);
               ((ViewModel<Country>)vm.Value).Model = SelectedDevice.Country;
               break;
            case "WeightUnit":
               vm = navigationNodes.FirstOrDefault(kv => kv.Value is ViewModel<WeightUnit>);
               Debug.Assert(vm.Value != null);
               ((ViewModel<WeightUnit>)vm.Value).Model = SelectedDevice.WeightUnit;
               break;
            case "DisplayConfiguration":
               vm = navigationNodes.FirstOrDefault(kv => kv.Value is ViewModel<DisplayConfiguration>);
               Debug.Assert(vm.Value != null);
               ((ViewModel<DisplayConfiguration>)vm.Value).Model = SelectedDevice.DisplayConfiguration;
               break;
            case "EthernetConfiguration":
               vm = navigationNodes.FirstOrDefault(kv => kv.Value is ViewModel<EthernetConfiguration>);
               Debug.Assert(vm.Value != null);
               ((ViewModel<EthernetConfiguration>)vm.Value).Model = SelectedDevice.EthernetConfiguration;
               break;
            case "GsmMessage":
               vm = navigationNodes.FirstOrDefault(kv => kv.Value is ViewModel<GsmMessage>);
               Debug.Assert(vm.Value != null);
               ((ViewModel<GsmMessage>)vm.Value).Model = SelectedDevice.GsmMessage;
               break;
            case "Rs485Configuration":
               vm = navigationNodes.FirstOrDefault(kv => kv.Value is ViewModel<Rs485Configuration>);
               Debug.Assert(vm.Value != null);
               cs = nodesDataSync.FirstOrDefault(kv => kv.Key == vm.Key);
               cs.Value.List = SelectedDevice.Rs485Configuration;
               break;
            case "SmsGateConfiguration":
            case "MegaviConfiguration":
            case "ModbusConfiguration":
            case "DacsConfiguration":
               break;
            case "Bat2Identification":
               vm = navigationNodes.FirstOrDefault(kv => kv.Value is ViewModel<Bat2Identification>);
               Debug.Assert(vm.Value != null);
               ((ViewModel<Bat2Identification>)vm.Value).Model = SelectedDevice.Bat2Identification;
               break;
            case "Weighing":
               vm = navigationNodes.FirstOrDefault(kv => kv.Value is ViewModel<Weighing>);
               if (vm.Key == null)
               {
                   return; 
               }
               ((WeighingViewModel)vm.Value).CorrectionCurveVm.Model = SelectedDevice.Weighing.CorrectionCurve;
               ((WeighingViewModel)vm.Value).GrowthCurveMaleVm.Model = SelectedDevice.Weighing.GrowthCurveMale;
               ((WeighingViewModel)vm.Value).GrowthCurveFemaleVm.Model = SelectedDevice.Weighing.GrowthCurveFemale;
               ((WeighingViewModel)vm.Value).WeighingPlanVm.Model = SelectedDevice.Weighing.WeighingPlan;
               ((ViewModel<Weighing>)vm.Value).Model = SelectedDevice.Weighing;
               break;
            case "ContactList":
               vm = navigationNodes.FirstOrDefault(kv => kv.Value is ViewModel<Contact>);
               Debug.Assert(vm.Value != null);
               cs = nodesDataSync.FirstOrDefault(kv => kv.Key == vm.Key);
               cs.Value.List = SelectedDevice.ContactList;
               break;
            case "WeighingConfiguration":
               vm = navigationNodes.FirstOrDefault(kv => kv.Key.Name == Properties.Resources.WeighingConfiguration);
               if (vm.Key == null)
               {
                   return;
               }
               ((ViewModel<WeighingConfiguration>)vm.Value).Model = SelectedDevice.WeighingConfiguration;
               break;
            case "GrowthCurveList":
               vm = navigationNodes.FirstOrDefault(kv => kv.Value is ViewModel<GrowthCurve>);
               Debug.Assert(vm.Value != null);
               cs = nodesDataSync.FirstOrDefault(kv => kv.Key == vm.Key);
               cs.Value.List = SelectedDevice.GrowthCurveList;
               break;
            case "CorrectionCurveList":
               vm = navigationNodes.FirstOrDefault(kv => kv.Value is ViewModel<CorrectionCurve>);
               Debug.Assert(vm.Value != null);
               cs = nodesDataSync.FirstOrDefault(kv => kv.Key == vm.Key);
               cs.Value.List = SelectedDevice.CorrectionCurveList;
               break;           
            case "PredefinedWeighingList":
               vm = navigationNodes.FirstOrDefault(kv => kv.Key.Name == Properties.Resources.PredefinedWeighings);
               Debug.Assert(vm.Value != null);
               cs = nodesDataSync.FirstOrDefault(kv => kv.Key == vm.Key);
               cs.Value.List = SelectedDevice.PredefinedWeighingList;
               break;
            case "WeighingPlanList":
               vm = navigationNodes.FirstOrDefault(kv => kv.Value is ViewModel<WeighingPlan>);
               Debug.Assert(vm.Value != null);
               cs = nodesDataSync.FirstOrDefault(kv => kv.Key == vm.Key);
               cs.Value.List = SelectedDevice.WeighingPlanList;
               break;
            case "Calibration":
               vm = navigationNodes.FirstOrDefault(kv => kv.Value is ViewModel<Calibration>);
               Debug.Assert(vm.Value != null);
               ((ViewModel<Calibration>)vm.Value).Model = SelectedDevice.Calibration;
               break;
            case "IsBusy":
               var data = (sender as Bat2DeviceDataProxy);
               if (data == null) 
               {
                  return;
               }
               if (data.Context == null) 
               {
                  return;
               }
               DispatcherHelper.RunAsync(() =>
               {                      
                  if (data.Context == null)
                  {
                     return;
                  }
                  //DateTime dt = DateTime.Now;
                  //while (data.Context == null) 
                  //{                  
                  //}
                  //TimeSpan timeSpan = dt - DateTime.Now;
                  //Console.WriteLine(timeSpan.ToString());         

                  if (data.Context.WeighingContext.Status == (byte)Bat2Library.WeighingStatusE.WEIGHING_STATUS_STOPPED)
                  {
                     vm = navigationNodes.FirstOrDefault(kv => kv.Key.Name == Properties.Resources.RunningWeighing);
                     if (vm.Key == null) 
                     {
                        return;
                     }
                     navigationNodes.Remove(vm.Key);
                     navigationService.RemoveNavigationNode(vm.Key);

                     ViewModel vmm = new WeighingConfigurationViewModel(new WeighingConfigurationView())
                     { 
                        //Model = SelectedDevice.WeighingConfiguration,
                        CorrectionCurves = SelectedDevice.CorrectionCurveList,
                        GrowthCurves = SelectedDevice.GrowthCurveList,
                        WeighingPlans = SelectedDevice.WeighingPlanList,
                        PredefinedWeighing = selectedDevice.PredefinedWeighingList,
                        IsPredefinedWeighing = false
                     };            
                     INavigationNode group = navigationNodes.FirstOrDefault(n => n.Key.Name == Properties.Resources.Weighing).Key;
                     INavigationNode subGroup = navigationService.AddNavigationNode(Properties.Resources.WeighingConfiguration, 1, vmm.View, group);
                     (group.View.DataContext as ObservableCollection<INavigationNode>).RemoveAt(0);
                     (group.View.DataContext as ObservableCollection<INavigationNode>).Insert(0, subGroup);
                     navigationNodes.Add(subGroup, vmm);
                     (vmm as WeighingConfigurationViewModel).Models.Add(SelectedDevice.WeighingConfiguration);
                  }
                  else if (SelectedDevice.Weighing.Status != Bat2Library.WeighingStatusE.WEIGHING_STATUS_UNDEFINED)
                  {
                     vm = navigationNodes.FirstOrDefault(kv => kv.Key.Name == Properties.Resources.WeighingConfiguration);
                     if (vm.Key == null) 
                     {
                        return;
                     }
                     navigationNodes.Remove(vm.Key);
                     navigationService.RemoveNavigationNode(vm.Key);

                     CorrectionCurveViewModel ccVM = new CorrectionCurveViewModel(new CurveView());
                     GrowthCurveViewModel gcMVM = new GrowthCurveViewModel(new CurveView());
                     GrowthCurveViewModel gcFVM = new GrowthCurveViewModel(new CurveView());
                     WeighingPlanViewModel wpVM = new WeighingPlanViewModel(new WeighingPlanView());
                     if (SelectedDevice.Weighing != null)
                     {
                        ccVM.Model = SelectedDevice.Weighing.CorrectionCurve;
                        gcMVM.Model = SelectedDevice.Weighing.GrowthCurveMale;
                        gcFVM.Model = SelectedDevice.Weighing.GrowthCurveFemale;
                        wpVM.Model = SelectedDevice.Weighing.WeighingPlan;
                     }

                     ViewModel vmm = new WeighingViewModel(new WeighingView(), ccVM, gcMVM, gcFVM, wpVM) { Model = SelectedDevice.Weighing };
                     INavigationNode group = navigationNodes.FirstOrDefault(n => n.Key.Name == Properties.Resources.Weighing).Key;
                     INavigationNode subGroup = navigationService.AddNavigationNode(Properties.Resources.RunningWeighing, 0.5, vmm.View, group);
                     (group.View.DataContext as ObservableCollection<INavigationNode>).RemoveAt(0);
                     (group.View.DataContext as ObservableCollection<INavigationNode>).Insert(0, subGroup);
                     navigationNodes.Add(subGroup, vmm);
                  }
               });
               break;
         }
      }

      private void CreateNavigationNodes()
      {
         ViewModel vm;

         #region DEVICE INFO GROUP

         var info = navigationService.AddNavigationNode(Properties.Resources.Info, 0.5, new MainUserControl());
         navigationNodes.Add(info, null);

         #region Info window

         vm = new Bat2IdentificationViewModel(new Bat2IdentificationView()) { Model = SelectedDevice.Bat2Identification };
         var deviceInfo = navigationService.AddNavigationNode(Properties.Resources.Info, 1, vm.View, info);
         navigationNodes.Add(deviceInfo, vm);

         #endregion

         info.View.DataContext = new ObservableCollection<INavigationNode>() { deviceInfo };

         #endregion

         #region CONFIGURATION GROUP

         var configuration = navigationService.AddNavigationNode(Properties.Resources.Configuration, 1, new MainUserControl());
         navigationNodes.Add(configuration, null);

         #region UserInterface window

         INavigationNode userInterface = navigationService.AddNavigationNode(Properties.Resources.UserInterface, 1, new SubUserControl(), configuration);

         //--Country
         vm = new CountryViewModel(new CountryView()) { Model = SelectedDevice.Country };        
         var node1 = navigationService.AddNavigationNode(Properties.Resources.Country, 1, vm.View, userInterface);
         navigationNodes.Add(node1, vm);
        
         //--DisplayConfiguration
         vm = new DisplayConfigurationViewModel(new DisplayConfigurationView()) { Model = SelectedDevice.DisplayConfiguration };
        
         var node2 = navigationService.AddNavigationNode(Properties.Resources.DisplayConfiguration, 2, vm.View, userInterface);
         navigationNodes.Add(node2, vm);
          
         //--WeightUnit
         vm = new WeightUnitViewModel(new WeightUnitView()) { Model = SelectedDevice.WeightUnit };       
         var node3 = navigationService.AddNavigationNode(Properties.Resources.WeightUnit, 3, vm.View, userInterface);
         navigationNodes.Add(node3, vm);

         userInterface.View.DataContext = new ObservableCollection<INavigationNode>() { node1, node2, node3 };

         #endregion

         #region Calibration window
              
         //-Calibration
         vm = new CalibrationViewModel(new CalibrationView()) { Model = SelectedDevice.Calibration };
         var calibration = navigationService.AddNavigationNode(Properties.Resources.Calibration, 2, vm.View, configuration);
         navigationNodes.Add(calibration, vm);

         #endregion

         #region Interfaces window

         INavigationNode interfaces = navigationService.AddNavigationNode(Properties.Resources.Interfaces, 3, new SubUserControlHorizontal(), configuration);

         //--Rs485Configuration
         vm = new Rs485ConfigurationViewModel(new Rs485ConfigurationListView()) { };
         INavigationNode rs485Configuration = navigationService.AddNavigationNode(Properties.Resources.Rs485Configuration,1, vm.View, interfaces);
         nodesDataSync.Add(rs485Configuration, new CollectionsSynchronizer(SelectedDevice.Rs485Configuration, (vm as ViewModel<Rs485Configuration>).Models));
         navigationNodes.Add(rs485Configuration, vm);
         (vm as Rs485ConfigurationViewModel).DacsConfiguration = SelectedDevice.DacsConfiguration;
         (vm as Rs485ConfigurationViewModel).ModbusConfiguration = SelectedDevice.ModbusConfiguration;
         (vm as Rs485ConfigurationViewModel).MegaviConfiguration = SelectedDevice.MegaviConfiguration;
         (vm as Rs485ConfigurationViewModel).SmsGateConfiguration = SelectedDevice.SmsGateConfiguration;
         
         //--Ethernet configuration
         vm = new EthernetConfigurationViewModel(new EthernetConfigurationView()) { Model = SelectedDevice.EthernetConfiguration };
         INavigationNode ethernetConfiguration = navigationService.AddNavigationNode(Properties.Resources.EthernetConfiguration, 2, vm.View, interfaces);
         navigationNodes.Add(ethernetConfiguration, vm);

         interfaces.View.DataContext = new ObservableCollection<INavigationNode>() { rs485Configuration, ethernetConfiguration };

        #endregion

         #region GSM message window

         vm = new GsmMessageViewModel(new GsmMessageView()) { /*Model = SelectedDevice.GsmMessage*/ };
         INavigationNode gsmMessage = navigationService.AddNavigationNode(Properties.Resources.GsmMessage, 4, vm.View, configuration);
         navigationNodes.Add(gsmMessage, vm);
         (vm as GsmMessageViewModel).Models.Add(SelectedDevice.GsmMessage);

         #endregion

         configuration.View.DataContext = new ObservableCollection<INavigationNode>() { userInterface, calibration, interfaces, gsmMessage };

         #endregion
        
         #region ACTUAL WEIGHING GROUP

         //Weighing root
        
         INavigationNode weighing = navigationService.AddNavigationNode(Properties.Resources.Weighing, 2, new MainUserControl(),null,() => 
         {
             if (SelectedDevice != null)
             {
                 SelectedDevice.LoadProperty(() => SelectedDevice.Context);
                 SelectedDevice.LoadProperty(() => SelectedDevice.WeighingPlans);
                 SelectedDevice.LoadProperty(() => SelectedDevice.PredefinedWeighings);
                 SelectedDevice.LoadProperty(() => SelectedDevice.CorrectionCurves);
                 SelectedDevice.LoadProperty(() => SelectedDevice.GrowthCurves);
             }
         });
         navigationNodes.Add(weighing, null);
         #region Weighing window

         //Weighing
         CorrectionCurveViewModel ccVM = new CorrectionCurveViewModel(new CurveView());
         GrowthCurveViewModel gcMVM = new GrowthCurveViewModel(new CurveView());
         GrowthCurveViewModel gcFVM = new GrowthCurveViewModel(new CurveView());
         WeighingPlanViewModel wpVM = new WeighingPlanViewModel(new WeighingPlanView());
         if (SelectedDevice.Weighing != null)
         {
            ccVM.Model = SelectedDevice.Weighing.CorrectionCurve;
            gcMVM.Model = SelectedDevice.Weighing.GrowthCurveMale;
            gcFVM.Model = SelectedDevice.Weighing.GrowthCurveFemale;
            wpVM.Model = SelectedDevice.Weighing.WeighingPlan;
         }
         INavigationNode node = null;
         if (SelectedDevice.Weighing.Status == Bat2Library.WeighingStatusE.WEIGHING_STATUS_STOPPED)
         {           
            //Stoped weighing(Weighing configuration)
            vm = new WeighingConfigurationViewModel(new WeighingConfigurationView()) 
            { 
                //Model = SelectedDevice.WeighingConfiguration,
                CorrectionCurves = SelectedDevice.CorrectionCurveList,         
                GrowthCurves = SelectedDevice.GrowthCurveList,
                WeighingPlans = SelectedDevice.WeighingPlanList,
                PredefinedWeighing = selectedDevice.PredefinedWeighingList,
                IsPredefinedWeighing = false
            };
            node = navigationService.AddNavigationNode(Properties.Resources.WeighingConfiguration, 1, vm.View, weighing);
            navigationNodes.Add(node, vm);
            (vm as WeighingConfigurationViewModel).Models.Add(SelectedDevice.WeighingConfiguration);
         }
         else if (SelectedDevice.Weighing.Status != Bat2Library.WeighingStatusE.WEIGHING_STATUS_UNDEFINED)
         {
            //Runing weighing
            vm = new WeighingViewModel(new WeighingView(), ccVM, gcMVM, gcFVM, wpVM) { Model = SelectedDevice.Weighing };
            node = navigationService.AddNavigationNode(Properties.Resources.RunningWeighing, 1, vm.View, weighing);
            navigationNodes.Add(node, vm);
         }

         #endregion

         #region Predefined weighing window

         // Weighing configuration list(Predefinded weighings)
         vm = new WeighingConfigurationViewModel(new WeighingConfigurationListView())
         {
             //Model = SelectedDevice.WeighingConfiguration,
             CorrectionCurves = SelectedDevice.CorrectionCurveList,
             GrowthCurves = SelectedDevice.GrowthCurveList,
             WeighingPlans = SelectedDevice.WeighingPlanList,
             PredefinedWeighing = selectedDevice.PredefinedWeighingList
         };
         var predefinedWeighings = navigationService.AddNavigationNode(Properties.Resources.PredefinedWeighings, 2, vm.View, weighing);
         nodesDataSync.Add(predefinedWeighings, new CollectionsSynchronizer(SelectedDevice.PredefinedWeighingList, (vm as ViewModel<WeighingConfiguration>).Models));
         navigationNodes.Add(predefinedWeighings, vm);

         #endregion

         #region Weighing plans window

         //Weighing plan list
         vm = new WeighingPlanViewModel(new WeighingPlanListView()) { };
         var weighingPlans = navigationService.AddNavigationNode(Properties.Resources.WeighingPlans, 3, vm.View, weighing);
         nodesDataSync.Add(weighingPlans, new CollectionsSynchronizer(SelectedDevice.WeighingPlanList, (vm as ViewModel<WeighingPlan>).Models));
         navigationNodes.Add(weighingPlans, vm);

         #endregion

         weighing.View.DataContext = new ObservableCollection<INavigationNode>() { node, predefinedWeighings, weighingPlans };

         #endregion

         #region CONTACTS GROUP

         INavigationNode contacts = navigationService.AddNavigationNode(Properties.Resources.Contacts, 3, new MainUserControl(), null, () =>
         {
             if (SelectedDevice != null)
                 SelectedDevice.LoadProperty(() => SelectedDevice.Contacts);
         });        

         #region Contacts window
         
         vm = new ContactsViewModel(new ContactListView());        
         var contact = navigationService.AddNavigationNode(Properties.Resources.Contacts, 1, vm.View, contacts);
         nodesDataSync.Add(contact, new CollectionsSynchronizer(SelectedDevice.ContactList, (vm as ViewModel<Contact>).Models));
         navigationNodes.Add(contact, vm);

         #endregion

         contacts.View.DataContext = new ObservableCollection<INavigationNode>() { contact };

         #endregion          
           
         #region CURVES GROUP

         INavigationNode curves = navigationService.AddNavigationNode(Properties.Resources.Curves, 4, new MainUserControl(),null, () =>
         {
             if (SelectedDevice != null)
             {
                 SelectedDevice.LoadProperty(() => SelectedDevice.CorrectionCurves);
                 SelectedDevice.LoadProperty(() => SelectedDevice.GrowthCurves);
             }
         });
         navigationNodes.Add(curves, null);

         #region Correction curves window

         vm = new CorrectionCurveViewModel(new CurveListView());
         var correctionCurves = navigationService.AddNavigationNode(Properties.Resources.CorrectionCurves, 1, vm.View, curves);
         nodesDataSync.Add(correctionCurves, new CollectionsSynchronizer(SelectedDevice.CorrectionCurveList, (vm as ViewModel<CorrectionCurve>).Models));
         navigationNodes.Add(correctionCurves, vm);
         
         #endregion

         #region Growth curves window

         vm = new GrowthCurveViewModel(new CurveListView());
         var growthCurves = navigationService.AddNavigationNode(Properties.Resources.GrowthCurves, 2, vm.View, curves);
         nodesDataSync.Add(growthCurves, new CollectionsSynchronizer(SelectedDevice.GrowthCurveList, (vm as ViewModel<GrowthCurve>).Models));
         navigationNodes.Add(growthCurves, vm);

         #endregion

         curves.View.DataContext = new ObservableCollection<INavigationNode>() { correctionCurves, growthCurves };
        
         #endregion         
      }

      private void RsConfigurationNodes(INavigationNode group)
      {
         if (SelectedDevice.Rs485Configuration == null || SelectedDevice.Rs485Configuration.Count == 0)
         {
            foreach (var node in navigationNodes.Where(kv => kv.Value is ViewModel<Rs485Configuration>).ToList())
            {
               navigationNodes.Remove(node.Key);
               navigationService.RemoveNavigationNode(node.Key);
            }
            return;
         }
         var rsNodes = navigationNodes.Where(kv => kv.Value is ViewModel<Rs485Configuration>).ToList();
         int i;
         for (i = 0; i < rsNodes.Count; i++)
         {
            // existing nodes
            if (i >= SelectedDevice.Rs485Configuration.Count)
            {
               break;
            }
            (rsNodes[i].Value as ViewModel<Rs485Configuration>).Model = SelectedDevice.Rs485Configuration[i];
         }
         while (i < SelectedDevice.Rs485Configuration.Count)
         {
            var inc = 1/SelectedDevice.Rs485Configuration.Count;
            // RS 485
            var vm = new Rs485ConfigurationViewModel(new Rs485ConfigurationView()) { Model = SelectedDevice.Rs485Configuration[i] };
            var curNode = navigationService.AddNavigationNode(string.Format("Rs485 interface {0}", i), 1 + inc * i, vm.View,
               group);
            navigationNodes.Add(curNode, vm);
            vm.DacsConfiguration = SelectedDevice.DacsConfiguration;
            vm.ModbusConfiguration = SelectedDevice.ModbusConfiguration;
            vm.MegaviConfiguration = SelectedDevice.MegaviConfiguration;
            vm.SmsGateConfiguration = SelectedDevice.SmsGateConfiguration;
            i++;
         }

         while (rsNodes.Count > i)
         {
            navigationNodes.Remove(rsNodes[i].Key);
            navigationService.RemoveNavigationNode(rsNodes[i].Key);
            i++;
         }
      }

      private void ClearNavigationNodes()
      {
         foreach (var navigationNode in navigationNodes)
         {
            navigationService.RemoveNavigationNode(navigationNode.Key);
         }
         navigationNodes.Clear();
         nodesDataSync.Clear();
      }

   }
}