using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using Common.Library.Messenger;
using Connection.Interface.Contract;
using Connection.Interface.Domain;
using Common.Desktop.Applications;
using Desktop.Client.ConectedDevices.Aplications.Proxy;
using Desktop.Client.ConectedDevices.Interface;
using Desktop.Client.MessageService.Interface;
using Desktop.Client.Server;
using Common.Desktop.Presentation;
using Common.Services.Client;
using System;
using Desktop.Client.ToolBarService.Aplications;
using System.Windows.Input;
using Bat2Library;
using System.Collections;
using System.Collections.Generic;

namespace Desktop.Client.ConectedDevices.Aplications
{
   public class ConnectedDevicesViewModel : ViewModel<Bat2DeviceDataProxy>,
      IBat2ConnectionEventsContract
   {
      #region Private properties

      private readonly ISelectDeviceService selectDeviceService;
      private readonly IMessageService messageService;
      private readonly ChannelFactory<IBat2DataCommandContract> clientFactory;
      private readonly ChannelFactory<IBat2CommandContract> clientDataFactory;
      private static readonly ManualResetEvent CanIvokeAction = new ManualResetEvent(true);   

      private RelayCommand timeGetCommand;
      private RelayCommand<DateTime> timeSetCommand;
      private RelayCommand<DateTime?> weighingStartCommand;
      private RelayCommand weighingStopCommand;
      private RelayCommand weighingSuspendCommand;
      private RelayCommand weighingReleaseCommand;
      private RelayCommand writeSettingsCommand;

      private Bat2DeviceDataProxy prevModel;
      #endregion

      #region Constructor

      public ConnectedDevicesViewModel(
         ISelectDeviceService selectDeviceService,
         IMessageService messageService,
         ChannelFactory<IBat2DataCommandContract> clientFactory,
         ChannelFactory<IBat2CommandContract> clientDataFactory)
      {
         this.selectDeviceService = selectDeviceService;
         this.messageService = messageService;
         this.clientFactory = clientFactory;
         this.clientDataFactory = clientDataFactory;
        
         Messenger.Default.Register<Bat2ConnectionEventsContractMessage>(
            this, msg => msg.ActionToInvoke(this));

        // Messenger.Default.Register<Bat2ActionEventsContractMessage>(
        //    this, msg => msg.ActionToInvoke(this));
         
         PropertyChanged += CheckModelChange;
         SelectedModels.CollectionChanged += SelectedModelsChanged;
      }

      #endregion

      #region Implementation of IBat2ConnectionEventsContract

      /// <summary>
      /// Event is invoked when device is connected.
      /// </summary>
      /// <param name="deviceData">device data according to connected device</param>
      public void DeviceConnected(Bat2DeviceData deviceData)
      {
         ThreadPool.QueueUserWorkItem(param =>
         {
            var proxy = new Bat2DeviceDataProxy(deviceData, clientFactory);
            proxy.LoadProperty(() => proxy.Context);            

            DispatcherHelper.RunAsync(() =>
            {
               lock (Models)
               {
                  proxy.StatusMessageHandler += ProxyOnStatusMessageHandler;
                  Models.Add(proxy);
               }
            });
         });
      }

      /// <summary>
      /// Event is invoked when device is disconnected.
      /// </summary>
      /// <param name="deviceData">device data according to disconnected device</param>
      public void DeviceDisconnected(Bat2DeviceData deviceData)
      {
         ThreadPool.QueueUserWorkItem(param => DispatcherHelper.RunAsync(() =>
         {
            lock (Models)
            {
               var proxy = ProxyForDeviceData(deviceData);
               if (proxy == null) return;

               proxy.StatusMessageHandler -= ProxyOnStatusMessageHandler;
               Models.Remove(proxy);
            }
         }));
      }

      private void ProxyOnStatusMessageHandler(object sender, string s)
      {
         Debug.Assert(sender is Bat2DeviceDataProxy);
         var proxy = sender as Bat2DeviceDataProxy;
         messageService.ShowMessage(string.Format("{0} (SN {1}) :\n{2}", proxy.Bat2Identification.Name,
            proxy.SerialNumber, s));
      }

      #endregion

      #region Commands

      #region Action commands

      public ICommand WriteSettingsCommand
      {
         get
         {
            if (writeSettingsCommand == null)
            {
               writeSettingsCommand = new RelayCommand(
                  () =>
                     // execute method
                     InvokeAction<object>(
                        (commander, data, param) => commander.WriteData(data)),
                  // can execute method
                  () => CommandCommonCanExecute);
            }
            return writeSettingsCommand;
         }
      }

      /// <summary>
      /// Command to get time from device
      /// </summary>
      public ICommand TimeGetCommand
      {
         get
         {
            if (timeGetCommand == null)
            {
               timeGetCommand = new RelayCommand
                  (() =>
                     // execute method
                     InvokeAction<object>(
                     (commander, data, param) => commander.TimeGet(data)),
                  // can execute method
                     () => CommandCommonCanExecute
                  );
            }
            return timeGetCommand;
         }
      }

      /// <summary>
      /// Command for set time to device
      /// </summary>
      public ICommand TimeSetCommand
      {
         get
         {
            if (timeSetCommand == null)
            {
               timeSetCommand = new RelayCommand<DateTime>
                  (time =>
                     // execute method
                     InvokeAction<DateTime>(
                        (commander, data, param) => commander.TimeSet(data, time)),
                  // can execute method
                     time => time != default(DateTime) && CommandCommonCanExecute
                  );
            }
            return timeSetCommand;
         }
      }

      /// <summary>
      /// Command for start weighing
      /// </summary>
      public ICommand WeighingStartCommand
      {
         get
         {
            if (weighingStartCommand == null)
            {
               weighingStartCommand = new RelayCommand<DateTime?>
                  (time =>
                     // execute method
                     InvokeAction<DateTime?>(
                        (commander, data, param) => commander.WeighingStart(data, time)),                // can execute method
                     time => CommandCommonCanExecute &&
                             (Model.Context.WeighingContext.Status ==
                              (byte)WeighingStatusE.WEIGHING_STATUS_STOPPED ||
                              Model.Context.WeighingContext.Status ==
                              (byte)WeighingStatusE.WEIGHING_STATUS_WAIT)
                  );
            }
            return weighingStartCommand;
         }
      }

      /// <summary>
      /// Command for stop current weighing
      /// </summary>
      public ICommand WeighingStopCommand
      {
         get
         {
            if (weighingStopCommand == null)
            {
               weighingStopCommand = new RelayCommand
                  (() =>
                     // execute method
                     InvokeAction<object>(
                        (commander, data, param) => commander.WeighingStop(data)),
                  // can execute method
                     () => CommandCommonCanExecute &&
                           (Model.Context.WeighingContext.Status !=
                            (byte)WeighingStatusE.WEIGHING_STATUS_STOPPED)
                  );
            }
            return weighingStopCommand;
         }
      }

      /// <summary>
      /// Command for pause current weighing
      /// </summary>
      public ICommand WeighingSuspendCommand
      {
         get
         {
            if (weighingSuspendCommand == null)
            {
               weighingSuspendCommand = new RelayCommand
                  (() =>
                     // execute method
                     InvokeAction<object>(
                        (commander, data, param) => commander.WeighingSuspend(data)),
                  // can execute method
                     () => CommandCommonCanExecute &&
                           (Model.Context.WeighingContext.Status ==
                            (byte)WeighingStatusE.WEIGHING_STATUS_WEIGHING ||
                            Model.Context.WeighingContext.Status ==
                            (byte)WeighingStatusE.WEIGHING_STATUS_RELEASED)
                  );
            }
            return weighingSuspendCommand;
         }
      }

      /// <summary>
      /// Command for unpause previously paused weighing
      /// </summary>
      public ICommand WeighingReleaseCommand
      {
         get
         {
            if (weighingReleaseCommand == null)
            {
               weighingReleaseCommand = new RelayCommand
                  (() =>
                     // execute method
                     InvokeAction<object>(
                        (commander, data, param) => commander.WeighingRelease(data)),
                  // can execute method
                     () => CommandCommonCanExecute &&
                           (Model.Context.WeighingContext.Status ==
                            (byte)WeighingStatusE.WEIGHING_STATUS_SUSPENDED)
                  );
            }
            return weighingReleaseCommand;
         }
      }

      #endregion


      #endregion

      #region Commands helper

      private delegate void CommanderDelegate<in S>(IBat2CommandContract commander, Bat2DeviceData data, S param);

      private void InvokeAction<S>(
         CommanderDelegate<S> commanderFunc,
         S funcParam = default(S))
      {
         ThreadPool.QueueUserWorkItem(param =>
         {
            ActionStatusChange(true); // lock as well
            var data = new Bat2DeviceData { Configuration = Model.Configuration };
            clientDataFactory.RemoteAction(commander => commanderFunc(commander, data, funcParam));
            ActionStatusChange(false);
         });
      }

      private void ActionStatusChange(bool to)
      {
         lock (CanIvokeAction)
         {
            if (to)
            {
               CanIvokeAction.WaitOne();
               CanIvokeAction.Reset();
            }
            else
            {
               CanIvokeAction.Set();
            }
         }
      }

      private bool CommandCommonCanExecute
      {
         get
         {
            return Model != null &&
                   Model.GetIsLoaded(() => Model.Configuration) && Model.GetIsLoaded(() => Model.Context) && !Model.IsBusy && CanIvokeAction.WaitOne(0);
         }
      }

     

      #endregion

      #region Helper methods

      #region Model changes

      private void SelectedModelsChanged(object sender,
         NotifyCollectionChangedEventArgs e)
      {
         if (Model != null) 
         {
            Model.PropertyChanged -= ModelPropertyChanged;
         }
         Model = SelectedModels.Count == 1 ? SelectedModels.First() : null;
         if (Model != null)
         {
            Model.PropertyChanged += ModelPropertyChanged;
         }
         selectDeviceService.SelectedDevices = SelectedModels;
         UpdateCommands();
      }

      private void UpdateCommands()
      {
         DispatcherHelper.RunAsync(() =>
         {          
            ((Command)WriteSettingsCommand).RaiseCanExecuteChanged();
            ((Command)TimeGetCommand).RaiseCanExecuteChanged();
            ((Command)TimeSetCommand).RaiseCanExecuteChanged();
            ((Command)WeighingStartCommand).RaiseCanExecuteChanged();
            ((Command)WeighingStopCommand).RaiseCanExecuteChanged();
            ((Command)WeighingSuspendCommand).RaiseCanExecuteChanged();
            ((Command)WeighingReleaseCommand).RaiseCanExecuteChanged();
         });

      }

      private void CheckModelChange(object sender, PropertyChangedEventArgs e)
      {
         if (sender == this)
         {
            if (e.PropertyName == "Model")
            {                 

               selectDeviceService.SelectedDevice = Model;
               
            }
         }
      }

      private void ModelPropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         if (e.PropertyName == "IsBusy") 
         {
            UpdateCommands();           
         }       
      }

      #endregion

      private Bat2DeviceDataProxy ProxyForDeviceData(Bat2DeviceData deviceData)
      {
         return Models.FirstOrDefault(
            device =>
               device.SerialNumber == deviceData.Configuration.VersionInfo.SerialNumber);
      }

      #endregion
   }
}