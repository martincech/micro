﻿using System.ComponentModel.Composition;
using Common.Desktop.Presentation;

namespace Desktop.Client.ConectedDevices.Interface
{
   [InheritedExport]
   public interface IConnectedDevicesView : IView
   {
   }
}