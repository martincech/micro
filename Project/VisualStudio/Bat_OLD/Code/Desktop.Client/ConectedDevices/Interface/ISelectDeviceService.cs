using System.Collections.Generic;
using Desktop.Client.ConectedDevices.Aplications.Proxy;

namespace Desktop.Client.ConectedDevices.Interface
{
   public interface ISelectDeviceService
   {
      Bat2DeviceDataProxy SelectedDevice
      {
         set;
      }

      IEnumerable<Bat2DeviceDataProxy> SelectedDevices { set; }
   }
}