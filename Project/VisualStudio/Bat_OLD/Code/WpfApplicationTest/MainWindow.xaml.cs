﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Desktop.Client.SampleData;

namespace WpfApplicationTest
{
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow : Window
   {
      public MainWindow()
      {
         InitializeComponent();

         //for test use only
         //SampleWeighingViewModel sample = new SampleWeighingViewModel();
         //SampleGsmMessageViewModel sample = new SampleGsmMessageViewModel();
         //SampleContactViewModel sample = new SampleContactViewModel();  
         //SampleCorrectionCurveViewModel sample = new SampleCorrectionCurveViewModel();        
         //SampleCalibrationViewModel sample = new SampleCalibrationViewModel();
         //SampleWeighingPlanViewModel sample = new SampleWeighingPlanViewModel();
         SampleWeighingConfigurationViewModel sample = new SampleWeighingConfigurationViewModel();
         //SampleDisplayConfigurationViewModel sample = new SampleDisplayConfigurationViewModel();

         DataContext = sample;
      }
   }
}
