//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ViewModels
{
   using Ploeh.AutoFixture;
   
   public static partial class SampleDataProvider
   {
         private const int ITEM_COUNT = 5;
         private static readonly Fixture DataGenerator;
   
         static SampleDataProvider()
         {
            DataGenerator = new Fixture {RepeatCount = ITEM_COUNT};
            DataGenerator.Customize(new MultipleCustomization());
         }
   }
}
