﻿using System.Collections.ObjectModel;
using Common.Library.Observable;

namespace ViewModels.Desktop
{
   public class ConnectedDevicesVm : ObservableObject
   {
      private IdentificationContext selectedDevice;
      private ObservableCollection<IdentificationContext> devices;

      public IdentificationContext SelectedDevice
      {
         get { return selectedDevice; }
         set { SetProperty(ref selectedDevice, value, "SelectedDevice"); }
      }

      public ObservableCollection<IdentificationContext> Devices
      {
         get { return devices; }
         set { SetProperty(ref devices, value, "Devices"); }
      }
   }
}