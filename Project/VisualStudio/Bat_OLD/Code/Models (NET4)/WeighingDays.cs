//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ViewModels
{
   
   using System;
   using System.ComponentModel;
   using System.ComponentModel.DataAnnotations;
   using Common.Library.Observable;
   
   public partial class WeighingDays : ValidatableObservableObject
   {
      #region Private fields
      
      private short startDay;
      private short suspendedDays;
      private short onlineDays;
      private Bat2Library.WeighingDaysMaskE daysOfWeek;
      private Bat2Library.WeighingDaysModeE mode;
      
      #endregion
      
      static WeighingDays()
      {
         TypeDescriptor.AddProviderTransparent(
            new AssociatedMetadataTypeTypeDescriptionProvider(
               typeof(WeighingDays), typeof(WeighingDaysMetadata)), typeof(WeighingDays));
      }
      /// <summary>
      /// Start day of weighing
      /// </summary>
      public virtual short StartDay { get{ return startDay; } set{ SetPropertyAndValidate(ref startDay, value); } }
      /// <summary>
      /// Suspended days for specific mode only.
      /// </summary>
      public virtual short SuspendedDays { get{ return suspendedDays; } set{ SetPropertyAndValidate(ref suspendedDays, value); } }
      /// <summary>
      /// Online days for specific mode only.
      /// </summary>
      public virtual short OnlineDays { get{ return onlineDays; } set{ SetPropertyAndValidate(ref onlineDays, value); } }
      /// <summary>
      /// Mask for days of week mode
      /// </summary>
      public virtual Bat2Library.WeighingDaysMaskE DaysOfWeek { get{ return daysOfWeek; } set{ SetPropertyAndValidate(ref daysOfWeek, value); } }
      /// <summary>
      /// Mode of weighing days
      /// </summary>
      public virtual Bat2Library.WeighingDaysModeE Mode { get{ return mode; } set{ SetPropertyAndValidate(ref mode, value); } }
   }
}
