//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ViewModels
{
    
   
   using System;
   using System.Collections.Generic;
   using System.ComponentModel;
   using System.ComponentModel.DataAnnotations;
   using Common.Library.Observable;
   
   public partial class WeighingConfiguration : ValidatableObservableObject
   {
      #region Private fields
      
      private short initialDayNumber;
      private Bat2Library.WeighingConfigurationMenuMaskE menuMask;
      private string flockName;
      private string name;
      private int correctionCurveIndex;
      private int predefinedIndex;
      private System.TimeSpan dayStart;
      private int weighingPlanIndex;
      private bool planning;
      private TargetWeight targetWeight;
      private Detection detection;
      private WeighingGenderConfiguration maleGender;
      private WeighingGenderConfiguration femaleGender;
      private StatisticConfiguration statistics;
      
      #endregion
      
      #region Constructors
   
      static WeighingConfiguration()
      {
         TypeDescriptor.AddProviderTransparent(
            new AssociatedMetadataTypeTypeDescriptionProvider(
               typeof(WeighingConfiguration), typeof(WeighingConfigurationMetadata)), typeof(WeighingConfiguration));
      }
      
      public WeighingConfiguration()
      {
         this.TargetWeight = new TargetWeight();
         this.Detection = new Detection();
         this.MaleGender = new WeighingGenderConfiguration();
         this.FemaleGender = new WeighingGenderConfiguration();
         this.Statistics = new StatisticConfiguration();
         AditionalConstructor();
      }
   
      partial void AditionalConstructor();
   
      #endregion
      public virtual short InitialDayNumber { get{ return initialDayNumber; } set{ SetPropertyAndValidate(ref initialDayNumber, value); } }
      public virtual Bat2Library.WeighingConfigurationMenuMaskE MenuMask { get{ return menuMask; } set{ SetPropertyAndValidate(ref menuMask, value); } }
      public virtual string FlockName { get{ return flockName; } set{ SetPropertyAndValidate(ref flockName, value); } }
      public virtual string Name { get{ return name; } set{ SetPropertyAndValidate(ref name, value); } }
      public virtual int CorrectionCurveIndex { get{ return correctionCurveIndex; } set{ SetPropertyAndValidate(ref correctionCurveIndex, value); } }
      public virtual int PredefinedIndex { get{ return predefinedIndex; } set{ SetPropertyAndValidate(ref predefinedIndex, value); } }
      public virtual System.TimeSpan DayStart { get{ return dayStart; } set{ SetPropertyAndValidate(ref dayStart, value); } }
      public virtual int WeighingPlanIndex { get{ return weighingPlanIndex; } set{ SetPropertyAndValidate(ref weighingPlanIndex, value); } }
      public virtual bool Planning { get{ return planning; } set{ SetPropertyAndValidate(ref planning, value); } }
   
      public virtual TargetWeight TargetWeight { get{ return targetWeight; } set{ SetPropertyAndValidate(ref targetWeight, value); } }
      public virtual Detection Detection { get{ return detection; } set{ SetPropertyAndValidate(ref detection, value); } }
      public virtual WeighingGenderConfiguration MaleGender { get{ return maleGender; } set{ SetPropertyAndValidate(ref maleGender, value); } }
      public virtual WeighingGenderConfiguration FemaleGender { get{ return femaleGender; } set{ SetPropertyAndValidate(ref femaleGender, value); } }
      public virtual StatisticConfiguration Statistics { get{ return statistics; } set{ SetPropertyAndValidate(ref statistics, value); } }
   }
}
