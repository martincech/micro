﻿using Connection.Interface.Domain;
using Connection.Manager;
using Connection.Manager.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Desktop.WindowsService
{
    public static class SocketSendArchive
    {
        private const int TRY_SEND_TIME = 10000;
        /// <summary>
        /// Read Archive from device
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="origin">basic data from device</param>
        public static void SocketSendArchive_DeviceConnected(object sender, Bat2DeviceData origin)
        {
            Func<IBat2DataReader, object> callback;
            callback = reader => reader.LoadArchive();

            var dataOriginator = GetDeviceDataInstance(origin);
            var dReader = Bat2ConnectionManager.Instance().GetDataReader(dataOriginator);
            if (dReader == null)
            {               
                return;
            }
            ThreadPool.QueueUserWorkItem(
               (r =>
               {
                   lock (r)
                   {
                       if (callback(r as IBat2DataReader) == null)
                       {
                           return;
                       }
                   }
                   try
                   {
                       if (dataOriginator == null) { return; }
                       if (dataOriginator.Archive == null) { return; }
                       if (!dataOriginator.Archive.Any()) { return; }
                       if (dataOriginator.Configuration == null) { return; }
                       if (dataOriginator.Configuration.EthernetOptions == null) { return; }
                       if (!dataOriginator.Configuration.EthernetOptions.Enabled) { return; }
                       if (dataOriginator.Configuration.EthernetOptions.Address == null) { return; }
                       SocketSendArchiveData(dataOriginator);
                   }
                   catch
                   {
                       RepeatSendData(dataOriginator);
                   }
               }), dReader);
        }

        /// <summary>
        /// Trying to send archive data 
        /// </summary>
        /// <param name="dataOriginator"></param>
        internal static void RepeatSendData(Bat2DeviceData dataOriginator) 
        { 
            bool sent = false;
            while (GetDeviceDataInstance(dataOriginator) != null && !sent) 
            {
                try
                {
                    SocketSendArchiveData(dataOriginator);
                    sent = true;
                    Thread.Sleep(TRY_SEND_TIME);
                }
                catch{}
            }
        }


        /// <summary>
        /// Sending data to specific ip address 
        /// </summary>
        /// <param name="data">data with archive items</param>
        internal static void SocketSendArchiveData(Bat2DeviceData data)
        {  
            TcpClient tcpClient = new TcpClient(data.Configuration.EthernetOptions.Address, data.Configuration.EthernetOptions.Port);
            Socket socket = tcpClient.Client;
            List<IGrouping<short, ArchiveItem>> list = data.Archive.Where(p => (p.HourFrom == 0 && p.HourTo == 23)).GroupBy(p => p.Day).ToList();
            Bat2Library.WeightUnitsE unit = (Bat2Library.WeightUnitsE)data.Configuration.WeightUnits.Units;
            
            foreach (IGrouping<short, ArchiveItem> group in list)
            {
                string message = "";
                if (group.ToList().Count == 1) 
                {
                     ArchiveItem m = group.FirstOrDefault();
                     message = GenerateShortMessage(m, data.Configuration.VersionInfo.SerialNumber, unit);
                }
                else if (group.ToList().Count == 2)
                {  
                    ArchiveItem f = group.Where(p => p.Sex == (byte)Bat2Library.SexE.SEX_FEMALE).FirstOrDefault();
                    ArchiveItem m = group.Where(p => p.Sex == (byte)Bat2Library.SexE.SEX_MALE).FirstOrDefault();
                    if (f == null || m == null) 
                    {
                        return;
                    }

                    message = GenerateLongMessage(f, m, data.Configuration.VersionInfo.SerialNumber, unit);
                   
                }
                else 
                {
                    return;
                }
                try
                {      
                    Send(socket, Encoding.UTF8.GetBytes(message), 0, message.Length, 5000);
                }
                catch (Exception ex) 
                {
                    RepeatSendData(data);
                }
            }
        }

        internal static string GenerateShortMessage(ArchiveItem m, uint serialNumber, Bat2Library.WeightUnitsE unit)
        {
           string message = String.Format("SCALE {0} DAY {1:000} {2:dd/MM/yyyy} Cnt {3:0000} Avg {4:00.000} Gain {5:00.000} Sig {6:0.000} Cv {7:000} Uni {8:000}",
                         serialNumber.ToString("X"), m.Day, m.Timestamp, m.Count, WeighConvert(m.Average, unit), WeighConvert(m.Gain, unit), WeighConvert(m.Sigma, unit), (double)m.Sigma / (double)m.Average * 100, m.Uniformity);
           return message;
        }

        internal static string GenerateLongMessage(ArchiveItem f, ArchiveItem m, uint serialNumber, Bat2Library.WeightUnitsE unit)
        {
           string message = String.Format("SCALE {0} DAY {1:000} {2:dd/MM/yyyy} FEMALES: Cnt {3:0000} Avg {4:00.000} Gain {5:00.000} Sig {6:0.000} Cv {7:000} Uni {8:000} MALES: Cnt {9:0000} Avg {10:00.000} Gain {11:00.000} Sig {12:0.000} Cv {13:000} Uni {14:000}",
                            serialNumber.ToString("X"), f.Day, f.Timestamp,
                            f.Count, WeighConvert(f.Average, unit), WeighConvert(f.Gain, unit), WeighConvert(f.Sigma, unit), (double)f.Sigma / (double)f.Average * 100, f.Uniformity,
                            m.Count, WeighConvert(m.Average, unit), WeighConvert(m.Gain, unit), WeighConvert(m.Sigma, unit), (double)m.Sigma / (double)m.Average * 100, m.Uniformity);
           return message;
        }

        /// <summary>
        /// Convert value to actua weight unit
        /// </summary>
        /// <param name="baseNumber"></param>
        /// <param name="unit"></param>
        /// <returns></returns>
        //TODO: Fix coverter
        internal static double WeighConvert(int baseNumber, Bat2Library.WeightUnitsE unit) 
        {
            double number = 0;
            switch (unit) 
            {
                case Bat2Library.WeightUnitsE.WEIGHT_UNITS_G:
                    number = (double)baseNumber / 10;
                    break;
                case Bat2Library.WeightUnitsE.WEIGHT_UNITS_KG:
                    number = ((double)baseNumber / 10) / 1000;
                    break;
                case Bat2Library.WeightUnitsE.WEIGHT_UNITS_LB:
                    number = (((double)baseNumber / 10) / 1000) * 2.204624420183777;
                    break;
            }

            return number;
        }

        internal static void Send(Socket socket, byte[] buffer, int offset, int size, int timeout)
        {
            int startTickCount = Environment.TickCount;
            int sent = 0;  
            do
            {
                if (Environment.TickCount > startTickCount + timeout)
                    throw new Exception("Timeout.");
                try
                {
                    sent += socket.Send(buffer, offset + sent, size - sent, SocketFlags.None);
                }
                catch (SocketException ex)
                {
                    if (ex.SocketErrorCode == SocketError.WouldBlock ||
                        ex.SocketErrorCode == SocketError.IOPending ||
                        ex.SocketErrorCode == SocketError.NoBufferSpaceAvailable)
                    {   
                        Thread.Sleep(30);
                    }
                    else
                        throw ex;  
                }
            } while (sent < size);
        }

        private static Bat2DeviceData GetDeviceDataInstance(Bat2DeviceData origin)
        {
            return Bat2ConnectionManager.Instance()
               .ConnectedDevices.FirstOrDefault(
                  device => device.Configuration.VersionInfo.SerialNumber == origin.Configuration.VersionInfo.SerialNumber);
        }

    }
}
