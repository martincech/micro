﻿using System;
using Bat2Library;
using ViewModels.Properties;

namespace ViewModels
{
   public partial class Contact
   {
      public void Map(Connection.Interface.Domain.Contact contact)
      {
         if (contact == null)
         {
            throw new ArgumentNullException("contact", Resources.Contact_can_t_be_null);
         }

         SendHistogram = contact.SendHistogram;
         RemoteControl = contact.RemoteControl;
         Events = contact.Events;
         Commands = contact.Commands;
         Statistics = contact.Statistics;
         SmsFormat = (GsmSmsFormatE)contact.SmsFormat;
         Name = contact.Name;
         Phone = contact.PhoneNumber;
      }
   }
}
