﻿using System;
using Bat2Library;
using ViewModels.Properties;

namespace ViewModels
{
   public partial class DisplayConfiguration
   {
      public void Map(Connection.Interface.Domain.DisplayConfiguration configuration)
      {
         if (configuration == null)
         {
            throw new ArgumentNullException("configuration", Resources.DisplayConfiguration_can_t_be_null);
         }

         Contrast = configuration.Contrast;
         Mode = (DisplayModeE)configuration.Mode;
         Backlight.Duration = configuration.BacklightDuration;
         Backlight.Intensity = configuration.BacklightIntensity;
         Backlight.Mode = (BacklightModeE)configuration.BacklightMode;
      }
   }
}
