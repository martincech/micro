﻿using System;
using Bat2Library;
using Connection.Interface.Domain;
using ViewModels.Properties;

namespace ViewModels
{
   public partial class WeightUnit
   {
      private static WeightCapacityE ConvertRange(WeightUnitsE units, int rangeNum)
      {
         switch (units)
         {
            case WeightUnitsE.WEIGHT_UNITS_G:
               if (rangeNum == WeightUnitsC.G_EXT_RANGE)
               {
                  return (WeightCapacityE.WEIGHT_CAPACITY_EXTENDED);
               }
               break;
            case WeightUnitsE.WEIGHT_UNITS_LB:
               if (rangeNum == WeightUnitsC.LB_EXT_RANGE)
               {
                  return (WeightCapacityE.WEIGHT_CAPACITY_EXTENDED);
               }
               break;
            case WeightUnitsE.WEIGHT_UNITS_KG:
               if (rangeNum == WeightUnitsC.KG_EXT_RANGE)
               {
                  return (WeightCapacityE.WEIGHT_CAPACITY_EXTENDED);
               }
               break;
         }
         return WeightCapacityE.WEIGHT_CAPACITY_NORMAL;
      }


      public void Map(WeightUnits weightUnit)
      {
         if (weightUnit == null)
         {
            throw new ArgumentNullException("weightUnit", Resources.WeightUnit_can_t_be_null);
         }

         Division = weightUnit.Division;
         Units = (WeightUnitsE)weightUnit.Units;
         Capacity = ConvertRange(Units, weightUnit.Range);
      }
   }
}
