﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Bat2Library;
using ViewModels.Properties;

namespace ViewModels
{
   public partial class WeighingPlan
   {
      public void Map(Connection.Interface.Domain.WeighingPlan plan)
      {
         if (plan == null)
         {
            throw new ArgumentNullException("plan", Resources.WeighingPlan_can_t_be_null);
         }

         SyncWithDayStart = plan.SyncWithDayStart;
         Name = plan.Name;
         WeighingDays.StartDay = plan.WeighingDaysStartDay;
         WeighingDays.SuspendedDays = plan.WeighingDaysSuspendedDays;
         WeighingDays.OnlineDays = plan.WeighingDaysOnlineDays;
         WeighingDays.DaysOfWeek = (WeighingDaysMaskE)plan.WeighingDaysDays;
         WeighingDays.Mode = (WeighingDaysModeE)plan.WeighingDaysMode;

         // WeighingTime
         WeighingTime = new ObservableCollection<WeighingTime>();
         if (plan.WeighingTimes == null) return;
         foreach (var wTime in plan.WeighingTimes.Select(t => new WeighingTime {From = t.From, To = t.To}))
         {
            WeighingTime.Add(wTime);
         }
      }
   }
}
