﻿using System;
using Bat2Library;
using ViewModels.Properties;

namespace ViewModels
{
   public partial class Bat2Identification
   {
      public void Map(Connection.Interface.Domain.VersionInfo varsionInfo,
         Connection.Interface.Domain.DeviceInfo device)
      {
         if (varsionInfo == null)
         {
            throw new ArgumentNullException("varsionInfo", Resources.VersionInfo_can_t_be_null);
         }
         if (device == null)
         {
            throw new ArgumentNullException("device", Resources.DeviceInfo_can_t_be_null);
         }
         Version.HardwareBuild = varsionInfo.HardwareBuild;
         Version.HardwareMinor = varsionInfo.HardwareMinor;
         Version.HardwareMajor = varsionInfo.HardwareMajor;
         Version.SoftwareBuild = varsionInfo.SoftwareBuild;
         Version.SoftwareMinor = varsionInfo.SoftwareMinor;
         Version.SoftwareMajor = varsionInfo.SoftwareMajor;
         Version.Modification = (DeviceModificationE)varsionInfo.Modification;
         Version.Class = (DeviceClassE)varsionInfo.Class;
         SerialNumber = Convert.ToInt32((uint) varsionInfo.SerialNumber);
         Name = device.Name;
         Password = device.Password;
      }
   }
}
