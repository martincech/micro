﻿using System;
using Bat2Library;
using ViewModels.Properties;

namespace ViewModels
{
   public partial class Country
   {
      public void Map(Connection.Interface.Domain.Country country)
      {
         if (country == null)
         {
            throw new ArgumentNullException("country", Resources.Country_can_t_be_null);
         }

         Language = (LanguageE)country.Language;
         CountryCode = (CountryE)country.CountryCode;

         Locale.CodePage = (CodePageE)country.CodePage;
         Locale.DateFormat = (DateFormatE)country.DateFormat;
         Locale.DaylightSavingType = (DaylightSavingE)country.DaylightSavingType;
         Locale.Separators = country.DateSeparator1.ToString() + country.DateSeparator2.ToString() +
                                             country.TimeSeparator.ToString();
         Locale.TimeFormat = (TimeFormatE)country.TimeFormat;
      }
   }
}
