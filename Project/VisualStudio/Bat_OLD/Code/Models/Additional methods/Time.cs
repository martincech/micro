﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using ViewModels.Properties;

namespace ViewModels
{
   public partial class Time : IFormattable
   {
      protected override IEnumerable<ValidationResult> AdditionalValidationRules(object value, string propertyName)
      {
         if ((propertyName != "From" || propertyName != "To")
              && (DateTime.Compare(From, To) < 0))
         {
            return base.AdditionalValidationRules(value, propertyName);
         }

         var valRes = new List<ValidationResult>
         {
            new ValidationResult(Resources.FromBeforeTo, new []{"From","To"})      
         };
         return valRes;
      }

      public override string ToString()
      {
         return ToString("", CultureInfo.CurrentCulture);
      }

      public string ToString(string format, IFormatProvider formatProvider)
      {
         return string.Format(formatProvider, "{0} - {1}", From, To);
      }
   }
}
