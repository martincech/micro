﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using Bat2Library;
using Bat2Library.Localization;
using ViewModels.Properties;

namespace ViewModels
{
   public partial class WeightUnit : IFormattable
   {
      #region Implementation of IFormattable

      /// <summary>
      /// Returns a string that represents the current object.
      /// </summary>
      /// <returns>
      /// A string that represents the current object.
      /// </returns>
      public override string ToString()
      {
         return ToString("", CultureInfo.CurrentCulture);
      }

      /// <summary>
      /// Formats the value of the current instance using the specified format.
      /// </summary>
      /// <returns>
      /// The value of the current instance in the specified format.
      /// </returns>
      /// <param name="format">The format to use.-or- A null reference (Nothing in Visual Basic) to use the default format defined for the type of the <see cref="T:System.IFormattable"/> implementation. </param><param name="formatProvider">The provider to use to format the value.-or- A null reference (Nothing in Visual Basic) to obtain the numeric format information from the current locale setting of the operating system. </param>
      public string ToString(string format, IFormatProvider formatProvider)
      {
         return String.Format(formatProvider, "{0}", ResourceEnumConverter.ConvertToString(Units));
      }

      #endregion

      #region Additional constructor method

      partial void AditionalConstructor()
      {
         PropertyChanged += (sender, e) =>
         {
            if (e.PropertyName == "Units")
            {
               RaisePropertyChanged(() => Decimals);
               RaisePropertyChanged(() => MaxDivision);
               RaisePropertyChanged(() => MinDivision);
            }
         };
      }

      #endregion

      #region Additional properties

      /// <summary>
      /// Decimal places of <see cref="ViewModels.WeightUnit.Division"/>
      /// </summary>
      public short Decimals
      {
         get
         {
            switch (Units)
            {
               case WeightUnitsE.WEIGHT_UNITS_G:
                  return WeightUnitsC.G_DECIMALS;
               case WeightUnitsE.WEIGHT_UNITS_KG:
                  return WeightUnitsC.KG_DECIMALS;
               case WeightUnitsE.WEIGHT_UNITS_LB:
                  return WeightUnitsC.LB_DECIMALS;
               default:
                  return 1;
            }
         }
      }

      /// <summary>
      /// Maximum value of <see cref="ViewModels.WeightUnit.Division"/> property
      /// </summary>
      public int MaxDivision
      {
         get
         {
            switch (Units)
            {
               case WeightUnitsE.WEIGHT_UNITS_G:
                  return WeightUnitsC.G_MAX_DIVISION;
               case WeightUnitsE.WEIGHT_UNITS_KG:
                  return WeightUnitsC.KG_MAX_DIVISION;
               case WeightUnitsE.WEIGHT_UNITS_LB:
                  return WeightUnitsC.LB_MAX_DIVISION;
               default:
                  return 1;
            }
         }
      }

      /// <summary>
      /// Minimum value of <see cref="ViewModels.WeightUnit.Division"/> property
      /// </summary>
      public int MinDivision
      {
         get
         {
            switch (Units)
            {
               case WeightUnitsE.WEIGHT_UNITS_G:
                  return WeightUnitsC.G_DIVISION;
               case WeightUnitsE.WEIGHT_UNITS_KG:
                  return WeightUnitsC.KG_DIVISION;
               case WeightUnitsE.WEIGHT_UNITS_LB:
                  return WeightUnitsC.LB_DIVISION;
               default:
                  return 1;
            }
         }
      }

      #endregion

      #region Additional validation

      /// <summary>
      /// Specify additional validation results based on child rules. Override this method in subclasses to get additional validation results
      /// which is not based on attributes
      /// </summary>
      /// <param name="value">value to be set on property</param>
      /// <param name="propertyName">property name to be validated</param>
      /// <returns></returns>
      protected override IEnumerable<ValidationResult> AdditionalValidationRules(object value, string propertyName)
      {
         if (
            propertyName != "Division" || propertyName != "Unit" ||
            (Division >= MinDivision && Division <= MaxDivision))
         {
            return base.AdditionalValidationRules(value, propertyName);
         }

         var valRes = new List<ValidationResult>
         {
            new ValidationResult(String.Format(Resources.DivisionRange, MinDivision/Math.Pow(10, Decimals),
               MaxDivision/Math.Pow(10, Decimals)), new []{propertyName})
         };
         return valRes;
      }

      #endregion
   }
}