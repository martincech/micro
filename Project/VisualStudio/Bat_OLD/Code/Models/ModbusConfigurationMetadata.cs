//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ViewModels
{
   using System;
   using System.ComponentModel.DataAnnotations;
   
   [MetadataType(typeof(ModbusConfigurationMetadata))]
   public partial class ModbusConfiguration
   {
      internal sealed class ModbusConfigurationMetadata
   	{
      
        [Display(Name = "Reply Delay")]
      	public short ReplyDelay { get; set; }
   
        [Display(Name = "Parity")]
      	public Bat2Library.ModbusParityE Parity { get; set; }
   
        [Display(Name = "Data Bits")]
      	public Bat2Library.ModbusDataBitsE DataBits { get; set; }
   
        [Display(Name = "Baud Rate")]
      	public int BaudRate { get; set; }
   
        [Display(Name = "Address")]
      	public byte Address { get; set; }
   
        [Display(Name = "Mode")]
      	public Bat2Library.ModbusTransmissionModeE Mode { get; set; }
   
   	}
   }
}
