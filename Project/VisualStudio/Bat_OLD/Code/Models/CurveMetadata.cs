//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ViewModels
{
   using System;
   using System.ComponentModel.DataAnnotations;
   using System.Collections.Generic;         
   
   [MetadataType(typeof(CurveMetadata))]
   public partial class Curve
   {
      internal sealed class CurveMetadata
   	{
      
        [Required(ErrorMessage=null, ErrorMessageResourceName ="NameRequired", ErrorMessageResourceType = typeof(ViewModels.Properties.Resources))]
   	  [StringLength(15, ErrorMessage=null, ErrorMessageResourceName ="MaxLength", ErrorMessageResourceType = typeof(ViewModels.Properties.Resources))]
        [Display(Name = "Name")]
      	public string Name { get; set; }
   
        public ICollection<CurvePoint> Points { get; set; }
   
   	}
   }
}
