//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ViewModels
{
    
   
   using System;
   using System.Collections.Generic;
   using System.ComponentModel;
   using System.ComponentModel.DataAnnotations;
   using Common.Library.Observable;
   
   public partial class Bat2Identification : RecursiveValidatableObservableObject
   {
      #region Private fields
      
      private int serialNumber;
      private string name;
      private string password;
      private DeviceVersion version;
      
      #endregion
      
      #region Constructors
   
      static Bat2Identification()
      {
         TypeDescriptor.AddProviderTransparent(
            new AssociatedMetadataTypeTypeDescriptionProvider(
               typeof(Bat2Identification), typeof(Bat2IdentificationMetadata)), typeof(Bat2Identification));
      }
      
      public Bat2Identification()
      {
         this.Version = new DeviceVersion();
         AditionalConstructor();
      }
   
      partial void AditionalConstructor();
   
      #endregion
      public virtual int SerialNumber { get{ return serialNumber; } set{ SetPropertyAndValidate(ref serialNumber, value); } }
      /// <summary>
      /// 
      /// </summary>
      public virtual string Name { get{ return name; } set{ SetPropertyAndValidate(ref name, value); } }
      /// <summary>
      /// 
      /// </summary>
      public virtual string Password { get{ return password; } set{ SetPropertyAndValidate(ref password, value); } }
   
      public virtual DeviceVersion Version { get{ return version; } set{ SetPropertyAndValidate(ref version, value); } }
   }
}
