//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ViewModels
{
   
   using System;
   using System.ComponentModel;
   using System.ComponentModel.DataAnnotations;
   using Common.Library.Observable;
   
   public partial class TargetWeight : RecursiveValidatableObservableObject
   {
      #region Private fields
      
      private Bat2Library.OnlineAdjustmentE onlineAdjustment;
      private Bat2Library.SexE sex;
      private Bat2Library.SexDifferentiationE sexDifferentiation;
      private Bat2Library.PredictionGrowthE growth;
      private Bat2Library.PredictionModeE mode;
      
      #endregion
      
      static TargetWeight()
      {
         TypeDescriptor.AddProviderTransparent(
            new AssociatedMetadataTypeTypeDescriptionProvider(
               typeof(TargetWeight), typeof(TargetWeightMetadata)), typeof(TargetWeight));
      }
      /// <summary>
      /// Online (when campaign is running) adjustment of growth curve.
      /// </summary>
      public virtual Bat2Library.OnlineAdjustmentE OnlineAdjustment { get{ return onlineAdjustment; } set{ SetPropertyAndValidate(ref onlineAdjustment, value); } }
      /// <summary>
      /// Sex of birds.
      /// </summary>
      public virtual Bat2Library.SexE Sex { get{ return sex; } set{ SetPropertyAndValidate(ref sex, value); } }
      /// <summary>
      /// Do sex diferentiation?
      /// </summary>
      public virtual Bat2Library.SexDifferentiationE SexDifferentiation { get{ return sexDifferentiation; } set{ SetPropertyAndValidate(ref sexDifferentiation, value); } }
      /// <summary>
      /// Prediction growth mode
      /// </summary>
      public virtual Bat2Library.PredictionGrowthE Growth { get{ return growth; } set{ SetPropertyAndValidate(ref growth, value); } }
      /// <summary>
      /// Prediction mode
      /// </summary>
      public virtual Bat2Library.PredictionModeE Mode { get{ return mode; } set{ SetPropertyAndValidate(ref mode, value); } }
   }
}
