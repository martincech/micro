﻿using Common.Desktop.Presentation;
using Common.Tests;
using Desktop.Client.MainWindow.Aplications;
using Desktop.Client.SampleData;
using Desktop.Client.ToolBarService.Interface;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace Desktop.Client.Tests.UnitTests
{
   /// <summary>
   /// Summary description for MainWindowViewModelTests
   /// </summary>
   [TestClass]
   public class MainWindowViewModelTests
   {
      private MainWindowViewModel viewModel;

      [TestInitialize]
      public void Init()
      {
         DispatcherHelper.Initialize();
         viewModel = new SampleMainWindowViewModel();
         DispatcherUtil.DoEvents();
      }
      [TestMethod]
      public void ToolBarNodeActionExecuted_WhenToolBarNodeClicked()
      {
         var executed = false;
         var node = new SampleToolBarNode { CanSelect = true, SelectAction = () => { executed = true; } };
         viewModel.AddToolBarNode(node);
         Assert.IsFalse(executed);
         Assert.IsTrue(viewModel.ToolBarCommand.CanExecute(node));
         viewModel.ToolBarCommand.Execute(node);
         Assert.IsTrue(executed);
      }

      [TestMethod]
      public void ToolBarNodesChanged_WhenToolBarNodesAdded()
      {         
         var node = new SampleToolBarNode();
         var count = viewModel.ToolBarNodes.Count;
         Assert.IsFalse(viewModel.ToolBarNodes.Contains(node));

         viewModel.AddToolBarNode(node);
         Assert.IsTrue(viewModel.ToolBarNodes.Count == count + 1);
         Assert.IsTrue(viewModel.ToolBarNodes.Contains(node));

         count = viewModel.ToolBarNodes.Count;
         var nodeList = new List<SampleToolBarNode>();
         var node2 = new SampleToolBarNode();
         var node3 = new SampleToolBarNode();
         nodeList.Add(node2);
         nodeList.Add(node3);
         viewModel.AddToolBarNode(nodeList);
         Assert.IsTrue(viewModel.ToolBarNodes.Count == count + 2);
         Assert.IsTrue(viewModel.ToolBarNodes.Contains(node2));
         Assert.IsTrue(viewModel.ToolBarNodes.Contains(node3));

      }

      [TestMethod]
      public void ToolBarNodesChanged_WhenToolBarNodesDeleted()
      {
         var node = viewModel.ToolBarNodes.FirstOrDefault();
         var count = viewModel.ToolBarNodes.Count;
         Assert.IsTrue(viewModel.ToolBarNodes.Contains(node));

         viewModel.RemoveToolBarNode(node);
         Assert.IsTrue(viewModel.ToolBarNodes.Count == count - 1);
         Assert.IsFalse(viewModel.ToolBarNodes.Contains(node));

         var nodeList = new List<IToolBarNode>();
         var node2 = viewModel.ToolBarNodes.FirstOrDefault();
         var node3 = viewModel.ToolBarNodes.FirstOrDefault();
         nodeList.Add(node2);
         nodeList.Add(node3);
         viewModel.RemoveToolBarNode(nodeList);
         Assert.IsTrue(viewModel.ToolBarNodes.Count == count - 2);
         Assert.IsFalse(viewModel.ToolBarNodes.Contains(node2));
         Assert.IsFalse(viewModel.ToolBarNodes.Contains(node3));
      }

      [TestMethod]
      public void ToolBarNodesChanged_WhenToolBarNodesCleared()
      {
         Assert.IsTrue(viewModel.ToolBarNodes.Count > 0);
         viewModel.RemoveToolBarNode();
         Assert.IsTrue(viewModel.ToolBarNodes.Count == 0);
      }
   }
}
