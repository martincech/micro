﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Desktop.Applications;
using Common.Desktop.Presentation;
using Common.Tests;
using Connection.Interface.Domain;
using Desktop.Client.ConectedDevices.Aplications;
using Desktop.Client.ConectedDevices.Aplications.Proxy;
using Desktop.Client.MessageService.Interface;
using ViewModels;
using Desktop.Client.NavigationService.Aplications;
using Desktop.Client.NavigationService.Interface;
using Desktop.Client.ToolBarService.Interface;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ploeh.AutoFixture;
using ArchiveItem = Connection.Interface.Domain.ArchiveItem;

namespace Desktop.Client.Tests.UnitTests
{
   [TestClass]
   public class SelectDeviceServiceTest
   {
      private IEnumerable<Bat2DeviceDataProxy> proxies;

      private void RemoveNodeRecursively(NavigationNode navigationNode, ref List<NavigationNode> navNodes)
      {
         navNodes.Remove(navigationNode);
         foreach (var node in navigationNode.ChildViewNodes)
         {
            RemoveNodeRecursively(node, ref navNodes);
         }
      }

      private static Mock<INavigationService> SetupNavMock(Action<NavigationNode> addNodeAction, Action<NavigationNode> removeNodeAction)
      {
         var navMock = new Mock<INavigationService>();
         navMock.Setup(mock =>
            mock.AddNavigationNode(It.IsAny<string>(), It.IsAny<double?>(), It.IsAny<IView>(),
               It.IsAny<INavigationNode>(), It.IsAny<Action>(), It.IsAny<Action>()))
            .Returns(() =>
            {
               var n = new NavigationNode("dummy", 1, null, null, null);
               addNodeAction(n);
               return n;
            });
         navMock.Setup(mock =>
            mock.AddNavigationNode(It.IsAny<string>(), It.IsAny<double?>(), It.IsAny<IView>(),
               It.IsNotNull<INavigationNode>(), It.IsAny<Action>(), null))
            .Returns<string, double?, IView, INavigationNode, Action, Action>((s, d, v, parentNode, a, c) =>
            {
               var n = new NavigationNode(s, d.HasValue ? d.Value : 0, v, a, null);
// ReSharper disable once PossibleNullReferenceException
               (parentNode as NavigationNode).ChildViewNodes.Add(n);
               addNodeAction(n);
               return n;
            });
         navMock.Setup(mock =>
            mock.AddNavigationNode(It.IsAny<string>(), It.IsAny<double?>(), It.IsAny<IView>(),
               It.IsNotNull<INavigationNode>(), null, null))
            .Returns<string, double?, IView, INavigationNode, Action, Action>((s, d, v, parentNode, a, c) =>
            {
               var n = new NavigationNode(s, d.HasValue ? d.Value : 0, v, null, null);
// ReSharper disable once PossibleNullReferenceException
               (parentNode as NavigationNode).ChildViewNodes.Add(n);
               addNodeAction(n);
               return n;
            });
         navMock.Setup(mock =>
            mock.RemoveNavigationNode(It.IsAny<INavigationNode>()))
            .Callback((INavigationNode node) => removeNodeAction(node as NavigationNode));

         return navMock;
      }
  
      [TestInitialize]
      public void Init()
      {
         DispatcherHelper.Initialize();
         var fixture = new Fixture {RepeatCount = 5};
         fixture.Customize<ArchiveItem>(composer =>
            composer.Without(m => m.HourFrom).Without(m => m.HourTo)
            );
         fixture.Customize<Bat2DeviceDataProxy>(composer => composer
            .FromFactory(() => new Bat2DeviceDataProxy(fixture.Create<Bat2DeviceData>(), null)));
         proxies = fixture.CreateMany<Bat2DeviceDataProxy>();
         DispatcherUtil.DoEvents();
      }

      [TestMethod]
      public void NavigationNodesSetup_WhenSelectedDeviceChanged()
      {
         //setup the mocks
         var navNodes = new List<NavigationNode>();
         var navMock = SetupNavMock(node=> navNodes.Add(node), node => RemoveNodeRecursively(node, ref navNodes));
         var toolMock = new Mock<IToolBarService>();
         var messageMock = new Mock<IMessageService>();

         var uut = new SelectDeviceService(navMock.Object, toolMock.Object, messageMock.Object, null);

         // set first time, create nodes
         uut.SelectedDevice = proxies.First();
         Assert.IsTrue(navNodes.Any());

         // set second time, update models
         uut.SelectedDevice = proxies.Last();
         Assert.IsTrue(navNodes.Any());

         // set to null, clear nodes
         uut.SelectedDevice = null;
         Assert.IsFalse(navNodes.Any());
      }

      [TestMethod]
      public void NavigationNodesSetup_RS485()
      {
         //setup the mocks
         var navNodes = new List<NavigationNode>();
         var navMock = SetupNavMock(node => navNodes.Add(node), node => RemoveNodeRecursively(node, ref navNodes));
         var toolMock = new Mock<IToolBarService>();
         var messageMock = new Mock<IMessageService>();

         var uut = new SelectDeviceService(navMock.Object, toolMock.Object, messageMock.Object, null);

         var p1 = proxies.ElementAt(0);
         var p2 = proxies.ElementAt(1);
         var p3 = proxies.ElementAt(2);
         // set first time, create nodes

         uut.SelectedDevice = p1;
         Assert.AreEqual(p1.Rs485Configuration.Count(), navNodes.Count(n => n.View != null && n.View.DataContext is ViewModel<Rs485Configuration>));

         // set second time, update models
         p2.Rs485Configuration.RemoveAt(0);
         uut.SelectedDevice = p2;
         Assert.AreEqual(p2.Rs485Configuration.Count(), navNodes.Count(n => n.View != null && n.View.DataContext is ViewModel<Rs485Configuration>));
         
         // set third time, update models
         uut.SelectedDevice = p3;
         Assert.IsTrue(navNodes.Any());
         Assert.AreEqual(p3.Rs485Configuration.Count(), navNodes.Count(n => n.View != null && n.View.DataContext is ViewModel<Rs485Configuration>));
      }
   }
}