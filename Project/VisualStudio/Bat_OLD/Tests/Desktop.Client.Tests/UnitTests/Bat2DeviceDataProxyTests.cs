﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading;
using Common.Desktop.Presentation;
using Common.Tests;
using Connection.Interface.Contract;
using Connection.Interface.Domain;
using Desktop.Client.ConectedDevices.Aplications.Proxy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ploeh.AutoFixture;
using ArchiveItem = Connection.Interface.Domain.ArchiveItem;
using WeighingPlan = Connection.Interface.Domain.WeighingPlan;

namespace Desktop.Client.Tests.UnitTests
{
   [TestClass]
   public class Bat2DeviceDataProxyTests
   {
      private static Bat2DeviceDataProxy _proxy;
      private Fixture fixture;


      [TestInitialize]
      public void Init()
      {
         DispatcherHelper.Initialize();
         fixture = new Fixture {RepeatCount = 5};
         fixture.Customize<ArchiveItem>(composer =>
            composer.Without(m => m.HourFrom).Without(m => m.HourTo)
            );
         var commanderMock = new MockIBat2DataCommandContract();
         var factoryMock = new MockIChannelFactory<IBat2DataCommandContract>(commanderMock);

         _proxy = new Bat2DeviceDataProxy(fixture.Create<Bat2DeviceData>(), factoryMock);
         DispatcherUtil.DoEvents();
      }


      [TestMethod]
      public void IsLoadedAndIsLoading_IsValid()
      {
         foreach (
            var prop in
               typeof (Bat2DeviceData).GetProperties()
                  .Select(property => typeof (Bat2DeviceDataProxy).GetProperty(property.Name,
                     BindingFlags.NonPublic | BindingFlags.Instance)))
         {
            Assert.IsNotNull(prop);
            if (prop.GetMethod.Invoke(_proxy, null) == null)
            {
               Assert.IsFalse(_proxy.GetIsLoaded(prop.Name));
            }
            else
            {
               Assert.IsTrue(_proxy.GetIsLoaded(prop.Name));
            }
            Assert.IsFalse(_proxy.GetIsLoading(prop.Name));
         }
      }

      [TestMethod]
      public void IsLoadingSingleProp()
      {
         _proxy.Context = null;
         _proxy.Contacts = null;
         Assert.IsFalse(_proxy.GetIsLoaded("Context"));
         Assert.IsFalse(_proxy.GetIsLoading("Context"));
         Assert.IsFalse(_proxy.GetIsLoaded("Contacts"));
         Assert.IsFalse(_proxy.GetIsLoading("Contacts"));

         LoadProperty(_proxy, "Context");
         LoadProperty(_proxy, "Contacts");
         // allow proxy invoke
         // simulate success on context

         Assert.AreEqual(1, IsLoadingCount());
         Assert.IsTrue(_proxy.GetIsLoading("Context"));
         Assert.IsFalse(_proxy.GetIsLoaded("Context"));
         _proxy.DataRead(fixture.Create<Bat2DeviceData>());
         Assert.IsFalse(_proxy.GetIsLoading("Context"));
         Assert.IsTrue(_proxy.GetIsLoaded("Context"));

         // simulate success on contacts

         Assert.AreEqual(1, IsLoadingCount());
         Assert.IsTrue(_proxy.GetIsLoading("Contacts"));
         Assert.IsFalse(_proxy.GetIsLoaded("Contacts"));
         _proxy.DataRead(fixture.Create<Bat2DeviceData>());
         Assert.IsFalse(_proxy.GetIsLoading("Contacts"));
         Assert.IsTrue(_proxy.GetIsLoaded("Contacts"));
      }

      [TestMethod]
      public void IsLoading_NoResponse_EventualyEnds()
      {
         _proxy.Context = null;
         _proxy.Contacts = null;

         Assert.IsFalse(_proxy.GetIsLoaded("Context"));
         Assert.IsFalse(_proxy.GetIsLoading("Context"));
         LoadProperty(_proxy, "Context");

         Assert.IsTrue(_proxy.GetIsLoading("Context"));
         Assert.IsFalse(_proxy.GetIsLoaded("Context"));
         new ManualResetEvent(false).WaitOne(5000);
         Assert.IsFalse(_proxy.GetIsLoading("Context"));
         Assert.IsFalse(_proxy.GetIsLoaded("Context"));
         Assert.AreEqual(0, IsLoadingCount());
      }

      #region Collection properties mapping

      [TestMethod]
      public void CollectionProperties_MappedValidly_Rs485Configuration()
      {
         fixture.RepeatCount = 2;
         var conf = fixture.Create<Configuration>();
         _proxy.Configuration = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(2, _proxy.Rs485Configuration.Count);

         conf = fixture.Create<Configuration>();
         conf.Rs485Options = new List<Rs485Options>();

         fixture.RepeatCount = 3;
         conf = fixture.Create<Configuration>();
         _proxy.Configuration = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(3, _proxy.Rs485Configuration.Count);

         conf.Rs485Options = new List<Rs485Options>();
         _proxy.Configuration = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(0, _proxy.Rs485Configuration.Count);
      }

      [TestMethod]
      public void CollectionProperties_MappedValidly_ArchiveItems()
      {
         fixture.RepeatCount = 2;
         var conf = fixture.CreateMany<ArchiveItem>();
         _proxy.Archive = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(2, _proxy.ArchiveItems.Count);

         conf = new List<ArchiveItem>();
         _proxy.Archive = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(0, _proxy.ArchiveItems.Count);

         fixture.RepeatCount = 3;
         conf = fixture.CreateMany<ArchiveItem>();
         _proxy.Archive = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(3, _proxy.ArchiveItems.Count);

         _proxy.Archive = null;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(0, _proxy.ArchiveItems.Count);
      }

      [TestMethod]
      public void CollectionProperties_MappedValidly_GrowthCurves()
      {
         fixture.RepeatCount = 2;
         var conf = fixture.CreateMany<Curve>();
         _proxy.GrowthCurves = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(2, _proxy.GrowthCurveList.Count);

         conf = new List<Curve>();
         _proxy.GrowthCurves = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(0, _proxy.GrowthCurveList.Count);

         fixture.RepeatCount = 3;
         conf = fixture.CreateMany<Curve>();
         _proxy.GrowthCurves = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(3, _proxy.GrowthCurveList.Count);

         _proxy.GrowthCurves = null;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(0, _proxy.GrowthCurveList.Count);
      }

      [TestMethod]
      public void CollectionProperties_MappedValidly_CorrectionCurves()
      {
         fixture.RepeatCount = 2;
         var conf = fixture.CreateMany<Curve>();
         _proxy.CorrectionCurves = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(2, _proxy.CorrectionCurveList.Count);

         conf = new List<Curve>();
         _proxy.CorrectionCurves = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(0, _proxy.CorrectionCurveList.Count);

         fixture.RepeatCount = 3;
         conf = fixture.CreateMany<Curve>();
         _proxy.CorrectionCurves = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(3, _proxy.CorrectionCurveList.Count);

         _proxy.CorrectionCurves = null;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(0, _proxy.CorrectionCurveList.Count);
      }

      [TestMethod]
      public void CollectionProperties_MappedValidly_Contacts()
      {
         fixture.RepeatCount = 2;
         var conf = fixture.CreateMany<Contact>();
         _proxy.Contacts = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(2, _proxy.ContactList.Count);

         conf = new List<Contact>();
         _proxy.Contacts = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(0, _proxy.ContactList.Count);

         fixture.RepeatCount = 3;
         conf = fixture.CreateMany<Contact>();
         _proxy.Contacts = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(3, _proxy.ContactList.Count);

         _proxy.Contacts = null;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(0, _proxy.ContactList.Count);
      }

      [TestMethod]
      public void CollectionProperties_MappedValidly_PredefinedWeighingConfigurations()
      {
         fixture.RepeatCount = 2;
         var conf = fixture.CreateMany<WeighingConfiguration>();
         _proxy.PredefinedWeighings = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(2, _proxy.PredefinedWeighingList.Count);

         conf = new List<WeighingConfiguration>();
         _proxy.PredefinedWeighings = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(0, _proxy.PredefinedWeighingList.Count);

         fixture.RepeatCount = 3;
         conf = fixture.CreateMany<WeighingConfiguration>();
         _proxy.PredefinedWeighings = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(3, _proxy.PredefinedWeighingList.Count);

         _proxy.PredefinedWeighings = null;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(0, _proxy.PredefinedWeighingList.Count);
      }

      [TestMethod]
      public void CollectionProperties_MappedValidly_WeighingPlanList()
      {
         fixture.RepeatCount = 2;
         var conf = fixture.CreateMany<WeighingPlan>();
         _proxy.WeighingPlans = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(2, _proxy.WeighingPlanList.Count);

         conf = new List<WeighingPlan>();
         _proxy.WeighingPlans = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(0, _proxy.WeighingPlanList.Count);

         fixture.RepeatCount = 3;
         conf = fixture.CreateMany<WeighingPlan>();
         _proxy.WeighingPlans = conf;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(3, _proxy.WeighingPlanList.Count);

         _proxy.WeighingPlans = null;
         DispatcherUtil.DoEvents();
         Assert.AreEqual(0, _proxy.WeighingPlanList.Count);
      }

      #endregion

      private static int IsLoadingCount()
      {
         return
            typeof (Bat2DeviceData).GetProperties()
               .Select(property =>
                  typeof (Bat2DeviceDataProxy).GetProperty(property.Name, BindingFlags.NonPublic | BindingFlags.Instance))
               .Select(prop => _proxy.GetIsLoading(prop.Name) ? 1 : 0).Sum();
      }

      private static void LoadProperty(Bat2DeviceDataProxy proxy, string property)
      {
         var ev = new ManualResetEvent(false);
         proxy.LoadProperty(property);
         ThreadPool.QueueUserWorkItem(c =>
         {
            while (!_proxy.IsBusy)
            {
               Thread.Sleep(20);}
            ev.Set();
         });
         ev.WaitOne();
      }
   }

   public class MockIBat2DataCommandContract : IBat2DataCommandContract, ICommunicationObject
   {
      #region Implementation of IBat2DataCommandContract

      /// <summary>
      /// Send command to device to read all data from it - same as calling 
      /// all other read methods from this interface separatelly
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void ReadData(Bat2DeviceData deviceData)
      {
      }

      /// <summary>
      /// Send command to device to read just configuration from it.
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void ReadConfig(Bat2DeviceData deviceData)
      {
      }

      /// <summary>
      /// Send command to device to read just context from it
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void ReadContext(Bat2DeviceData deviceData)
      {
      }

      /// <summary>
      /// Send command to device to read whole archive from it
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void ReadArchive(Bat2DeviceData deviceData)
      {
      }

      /// <summary>
      /// Send command to device to read growth curves from it
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void ReadGrowthCurves(Bat2DeviceData deviceData)
      {
      }

      /// <summary>
      /// Send command to device to read correction curves from it
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void ReadCorrectionCurves(Bat2DeviceData deviceData)
      {
      }

      /// <summary>
      /// Send command to device to read contact list from it
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void ReadContactList(Bat2DeviceData deviceData)
      {
      }

      /// <summary>
      /// Send command to device to read weighing plans from it
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void ReadWeighingPlans(Bat2DeviceData deviceData)
      {
      }

      /// <summary>
      /// Send command to device to read predefined weighing configurations from it
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void ReadPredefinedWeighings(Bat2DeviceData deviceData)
      {
      }

      /// <summary>
      /// Send command to device to write data to it - same as calling
      /// all write methods from this interface separatelly
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void WriteData(Bat2DeviceData deviceData)
      {
      }

      /// <summary>
      /// Send command to device to write just configuration to it.
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void WriteConfig(Bat2DeviceData deviceData)
      {
      }

      /// <summary>
      /// Send command to device to write just growth curves to it.
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void WriteGrowthCurves(Bat2DeviceData deviceData)
      {
      }

      /// <summary>
      /// Send command to device to write just correction curves to it.
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void WriteCorrectionCurves(Bat2DeviceData deviceData)
      {
      }

      /// <summary>
      /// Send command to device to write just contact list to it.
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void WriteContactList(Bat2DeviceData deviceData)
      {
      }

      /// <summary>
      /// Send command to device to write just weighing plans to it.
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void WriteWeighingPlans(Bat2DeviceData deviceData)
      {
      }

      /// <summary>
      /// Send command to device to write just predefined weighing configurations to it
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void WritePredefinedWeighings(Bat2DeviceData deviceData)
      {
      }

      #endregion

      #region Implementation of ICommunicationObject

      /// <summary>
      /// Causes a communication object to transition immediately from its current state into the closed state.  
      /// </summary>
      public void Abort()
      {
      }

      /// <summary>
      /// Causes a communication object to transition from its current state into the closed state.  
      /// </summary>
      /// <exception cref="T:System.ServiceModel.CommunicationObjectFaultedException"><see cref="M:System.ServiceModel.ICommunicationObject.Close"/> was called on an object in the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception><exception cref="T:System.TimeoutException">The default close timeout elapsed before the <see cref="T:System.ServiceModel.ICommunicationObject"/> was able to close gracefully.</exception>
      public void Close()
      {
      }

      /// <summary>
      /// Causes a communication object to transition from its current state into the closed state.  
      /// </summary>
      /// <param name="timeout">The <see cref="T:System.Timespan"/> that specifies how long the send operation has to complete before timing out.</param><exception cref="T:System.ServiceModel.CommunicationObjectFaultedException"><see cref="M:System.ServiceModel.ICommunicationObject.Close"/> was called on an object in the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception><exception cref="T:System.TimeoutException">The timeout elapsed before the <see cref="T:System.ServiceModel.ICommunicationObject"/> was able to close gracefully.</exception>
      public void Close(TimeSpan timeout)
      {
      }

      /// <summary>
      /// Begins an asynchronous operation to close a communication object.
      /// </summary>
      /// <returns>
      /// The <see cref="T:System.IAsyncResult"/> that references the asynchronous close operation. 
      /// </returns>
      /// <param name="callback">The <see cref="T:System.AsyncCallback"/> delegate that receives notification of the completion of the asynchronous close operation.</param><param name="state">An object, specified by the application, that contains state information associated with the asynchronous close operation.</param><exception cref="T:System.ServiceModel.CommunicationObjectFaultedException"><see cref="M:System.ServiceModel.ICommunicationObject.BeginClose"/> was called on an object in the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception><exception cref="T:System.TimeoutException">The default timeout elapsed before the <see cref="T:System.ServiceModel.ICommunicationObject"/> was able to close gracefully.</exception>
      public IAsyncResult BeginClose(AsyncCallback callback, object state)
      {
         throw new NotImplementedException();
      }

      /// <summary>
      /// Begins an asynchronous operation to close a communication object with a specified timeout.
      /// </summary>
      /// <returns>
      /// The <see cref="T:System.IAsyncResult"/> that references the asynchronous close operation.
      /// </returns>
      /// <param name="timeout">The <see cref="T:System.Timespan"/> that specifies how long the send operation has to complete before timing out.</param><param name="callback">The <see cref="T:System.AsyncCallback"/> delegate that receives notification of the completion of the asynchronous close operation.</param><param name="state">An object, specified by the application, that contains state information associated with the asynchronous close operation.</param><exception cref="T:System.ServiceModel.CommunicationObjectFaultedException"><see cref="M:System.ServiceModel.ICommunicationObject.BeginClose"/> was called on an object in the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception><exception cref="T:System.TimeoutException">The specified timeout elapsed before the <see cref="T:System.ServiceModel.ICommunicationObject"/> was able to close gracefully.</exception>
      public IAsyncResult BeginClose(TimeSpan timeout, AsyncCallback callback, object state)
      {
         throw new NotImplementedException();
      }

      /// <summary>
      /// Completes an asynchronous operation to close a communication object.
      /// </summary>
      /// <param name="result">The <see cref="T:System.IAsyncResult"/> that is returned by a call to the <see cref="M:System.ServiceModel.ICommunicationObject.BeginClose"/> method.</param><exception cref="T:System.ServiceModel.CommunicationObjectFaultedException"><see cref="M:System.ServiceModel.ICommunicationObject.BeginClose"/> was called on an object in the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception><exception cref="T:System.TimeoutException">The timeout elapsed before the <see cref="T:System.ServiceModel.ICommunicationObject"/> was able to close gracefully.</exception>
      public void EndClose(IAsyncResult result)
      {
      }

      /// <summary>
      /// Causes a communication object to transition from the created state into the opened state.  
      /// </summary>
      /// <exception cref="T:System.ServiceModel.CommunicationException">The <see cref="T:System.ServiceModel.ICommunicationObject"/> was unable to be opened and has entered the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception><exception cref="T:System.TimeoutException">The default open timeout elapsed before the <see cref="T:System.ServiceModel.ICommunicationObject"/> was able to enter the <see cref="F:System.ServiceModel.CommunicationState.Opened"/> state and has entered the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception>
      public void Open()
      {
      }

      /// <summary>
      /// Causes a communication object to transition from the created state into the opened state within a specified interval of time.
      /// </summary>
      /// <param name="timeout">The <see cref="T:System.Timespan"/> that specifies how long the send operation has to complete before timing out.</param><exception cref="T:System.ServiceModel.CommunicationException">The <see cref="T:System.ServiceModel.ICommunicationObject"/> was unable to be opened and has entered the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception><exception cref="T:System.TimeoutException">The specified timeout elapsed before the <see cref="T:System.ServiceModel.ICommunicationObject"/> was able to enter the <see cref="F:System.ServiceModel.CommunicationState.Opened"/> state and has entered the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception>
      public void Open(TimeSpan timeout)
      {
      }

      /// <summary>
      /// Begins an asynchronous operation to open a communication object.
      /// </summary>
      /// <returns>
      /// The <see cref="T:System.IAsyncResult"/> that references the asynchronous open operation. 
      /// </returns>
      /// <param name="callback">The <see cref="T:System.AsyncCallback"/> delegate that receives notification of the completion of the asynchronous open operation.</param><param name="state">An object, specified by the application, that contains state information associated with the asynchronous open operation.</param><exception cref="T:System.ServiceModel.CommunicationException">The <see cref="T:System.ServiceModel.ICommunicationObject"/> was unable to be opened and has entered the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception><exception cref="T:System.TimeoutException">The default open timeout elapsed before the <see cref="T:System.ServiceModel.ICommunicationObject"/> was able to enter the <see cref="F:System.ServiceModel.CommunicationState.Opened"/> state and has entered the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception>
      public IAsyncResult BeginOpen(AsyncCallback callback, object state)
      {
         throw new NotImplementedException();
      }

      /// <summary>
      /// Begins an asynchronous operation to open a communication object within a specified interval of time.
      /// </summary>
      /// <returns>
      /// The <see cref="T:System.IAsyncResult"/> that references the asynchronous open operation. 
      /// </returns>
      /// <param name="timeout">The <see cref="T:System.Timespan"/> that specifies how long the send operation has to complete before timing out.</param><param name="callback">The <see cref="T:System.AsyncCallback"/> delegate that receives notification of the completion of the asynchronous open operation.</param><param name="state">An object, specified by the application, that contains state information associated with the asynchronous open operation.</param><exception cref="T:System.ServiceModel.CommunicationException">The <see cref="T:System.ServiceModel.ICommunicationObject"/> was unable to be opened and has entered the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception><exception cref="T:System.TimeoutException">The specified timeout elapsed before the <see cref="T:System.ServiceModel.ICommunicationObject"/> was able to enter the <see cref="F:System.ServiceModel.CommunicationState.Opened"/> state and has entered the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception>
      public IAsyncResult BeginOpen(TimeSpan timeout, AsyncCallback callback, object state)
      {
         throw new NotImplementedException();
      }

      /// <summary>
      /// Completes an asynchronous operation to open a communication object.
      /// </summary>
      /// <param name="result">The <see cref="T:System.IAsyncResult"/> that is returned by a call to the <see cref="M:System.ServiceModel.ICommunicationObject.BeginOpen"/> method.</param><exception cref="T:System.ServiceModel.CommunicationException">The <see cref="T:System.ServiceModel.ICommunicationObject"/> was unable to be opened and has entered the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception><exception cref="T:System.TimeoutException">The timeout elapsed before the <see cref="T:System.ServiceModel.ICommunicationObject"/> was able to enter the <see cref="F:System.ServiceModel.CommunicationState.Opened"/> state and has entered the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception>
      public void EndOpen(IAsyncResult result)
      {
      }

      /// <summary>
      /// Gets the current state of the communication-oriented object.
      /// </summary>
      /// <returns>
      /// The value of the <see cref="T:System.ServiceModel.CommunicationState"/> of the object.
      /// </returns>
      public CommunicationState State { get; private set; }

      public event EventHandler Closed;
      public event EventHandler Closing;
      public event EventHandler Faulted;
      public event EventHandler Opened;
      public event EventHandler Opening;

      #endregion
   }

   public class MockIChannelFactory<T> : IChannelFactory<T>
   {
      private readonly T chan;

      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public MockIChannelFactory(T chan)
      {
         this.chan = chan;
      }

      public T CreateChannel()
      {
         return chan;
      }

      #region Implementation of ICommunicationObject

      /// <summary>
      /// Causes a communication object to transition immediately from its current state into the closed state.  
      /// </summary>
      public void Abort()
      {
      }

      /// <summary>
      /// Causes a communication object to transition from its current state into the closed state.  
      /// </summary>
      /// <exception cref="T:System.ServiceModel.CommunicationObjectFaultedException"><see cref="M:System.ServiceModel.ICommunicationObject.Close"/> was called on an object in the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception><exception cref="T:System.TimeoutException">The default close timeout elapsed before the <see cref="T:System.ServiceModel.ICommunicationObject"/> was able to close gracefully.</exception>
      public void Close()
      {
      }

      /// <summary>
      /// Causes a communication object to transition from its current state into the closed state.  
      /// </summary>
      /// <param name="timeout">The <see cref="T:System.Timespan"/> that specifies how long the send operation has to complete before timing out.</param><exception cref="T:System.ServiceModel.CommunicationObjectFaultedException"><see cref="M:System.ServiceModel.ICommunicationObject.Close"/> was called on an object in the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception><exception cref="T:System.TimeoutException">The timeout elapsed before the <see cref="T:System.ServiceModel.ICommunicationObject"/> was able to close gracefully.</exception>
      public void Close(TimeSpan timeout)
      {
      }

      /// <summary>
      /// Begins an asynchronous operation to close a communication object.
      /// </summary>
      /// <returns>
      /// The <see cref="T:System.IAsyncResult"/> that references the asynchronous close operation. 
      /// </returns>
      /// <param name="callback">The <see cref="T:System.AsyncCallback"/> delegate that receives notification of the completion of the asynchronous close operation.</param><param name="state">An object, specified by the application, that contains state information associated with the asynchronous close operation.</param><exception cref="T:System.ServiceModel.CommunicationObjectFaultedException"><see cref="M:System.ServiceModel.ICommunicationObject.BeginClose"/> was called on an object in the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception><exception cref="T:System.TimeoutException">The default timeout elapsed before the <see cref="T:System.ServiceModel.ICommunicationObject"/> was able to close gracefully.</exception>
      public IAsyncResult BeginClose(AsyncCallback callback, object state)
      {
         return null;
      }

      /// <summary>
      /// Begins an asynchronous operation to close a communication object with a specified timeout.
      /// </summary>
      /// <returns>
      /// The <see cref="T:System.IAsyncResult"/> that references the asynchronous close operation.
      /// </returns>
      /// <param name="timeout">The <see cref="T:System.Timespan"/> that specifies how long the send operation has to complete before timing out.</param><param name="callback">The <see cref="T:System.AsyncCallback"/> delegate that receives notification of the completion of the asynchronous close operation.</param><param name="state">An object, specified by the application, that contains state information associated with the asynchronous close operation.</param><exception cref="T:System.ServiceModel.CommunicationObjectFaultedException"><see cref="M:System.ServiceModel.ICommunicationObject.BeginClose"/> was called on an object in the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception><exception cref="T:System.TimeoutException">The specified timeout elapsed before the <see cref="T:System.ServiceModel.ICommunicationObject"/> was able to close gracefully.</exception>
      public IAsyncResult BeginClose(TimeSpan timeout, AsyncCallback callback, object state)
      {
         return null;
      }

      /// <summary>
      /// Completes an asynchronous operation to close a communication object.
      /// </summary>
      /// <param name="result">The <see cref="T:System.IAsyncResult"/> that is returned by a call to the <see cref="M:System.ServiceModel.ICommunicationObject.BeginClose"/> method.</param><exception cref="T:System.ServiceModel.CommunicationObjectFaultedException"><see cref="M:System.ServiceModel.ICommunicationObject.BeginClose"/> was called on an object in the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception><exception cref="T:System.TimeoutException">The timeout elapsed before the <see cref="T:System.ServiceModel.ICommunicationObject"/> was able to close gracefully.</exception>
      public void EndClose(IAsyncResult result)
      {
      }

      /// <summary>
      /// Causes a communication object to transition from the created state into the opened state.  
      /// </summary>
      /// <exception cref="T:System.ServiceModel.CommunicationException">The <see cref="T:System.ServiceModel.ICommunicationObject"/> was unable to be opened and has entered the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception><exception cref="T:System.TimeoutException">The default open timeout elapsed before the <see cref="T:System.ServiceModel.ICommunicationObject"/> was able to enter the <see cref="F:System.ServiceModel.CommunicationState.Opened"/> state and has entered the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception>
      public void Open()
      {
      }

      /// <summary>
      /// Causes a communication object to transition from the created state into the opened state within a specified interval of time.
      /// </summary>
      /// <param name="timeout">The <see cref="T:System.Timespan"/> that specifies how long the send operation has to complete before timing out.</param><exception cref="T:System.ServiceModel.CommunicationException">The <see cref="T:System.ServiceModel.ICommunicationObject"/> was unable to be opened and has entered the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception><exception cref="T:System.TimeoutException">The specified timeout elapsed before the <see cref="T:System.ServiceModel.ICommunicationObject"/> was able to enter the <see cref="F:System.ServiceModel.CommunicationState.Opened"/> state and has entered the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception>
      public void Open(TimeSpan timeout)
      {
      }

      /// <summary>
      /// Begins an asynchronous operation to open a communication object.
      /// </summary>
      /// <returns>
      /// The <see cref="T:System.IAsyncResult"/> that references the asynchronous open operation. 
      /// </returns>
      /// <param name="callback">The <see cref="T:System.AsyncCallback"/> delegate that receives notification of the completion of the asynchronous open operation.</param><param name="state">An object, specified by the application, that contains state information associated with the asynchronous open operation.</param><exception cref="T:System.ServiceModel.CommunicationException">The <see cref="T:System.ServiceModel.ICommunicationObject"/> was unable to be opened and has entered the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception><exception cref="T:System.TimeoutException">The default open timeout elapsed before the <see cref="T:System.ServiceModel.ICommunicationObject"/> was able to enter the <see cref="F:System.ServiceModel.CommunicationState.Opened"/> state and has entered the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception>
      public IAsyncResult BeginOpen(AsyncCallback callback, object state)
      {
         return null;
      }

      /// <summary>
      /// Begins an asynchronous operation to open a communication object within a specified interval of time.
      /// </summary>
      /// <returns>
      /// The <see cref="T:System.IAsyncResult"/> that references the asynchronous open operation. 
      /// </returns>
      /// <param name="timeout">The <see cref="T:System.Timespan"/> that specifies how long the send operation has to complete before timing out.</param><param name="callback">The <see cref="T:System.AsyncCallback"/> delegate that receives notification of the completion of the asynchronous open operation.</param><param name="state">An object, specified by the application, that contains state information associated with the asynchronous open operation.</param><exception cref="T:System.ServiceModel.CommunicationException">The <see cref="T:System.ServiceModel.ICommunicationObject"/> was unable to be opened and has entered the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception><exception cref="T:System.TimeoutException">The specified timeout elapsed before the <see cref="T:System.ServiceModel.ICommunicationObject"/> was able to enter the <see cref="F:System.ServiceModel.CommunicationState.Opened"/> state and has entered the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception>
      public IAsyncResult BeginOpen(TimeSpan timeout, AsyncCallback callback, object state)
      {
         return null;
      }

      /// <summary>
      /// Completes an asynchronous operation to open a communication object.
      /// </summary>
      /// <param name="result">The <see cref="T:System.IAsyncResult"/> that is returned by a call to the <see cref="M:System.ServiceModel.ICommunicationObject.BeginOpen"/> method.</param><exception cref="T:System.ServiceModel.CommunicationException">The <see cref="T:System.ServiceModel.ICommunicationObject"/> was unable to be opened and has entered the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception><exception cref="T:System.TimeoutException">The timeout elapsed before the <see cref="T:System.ServiceModel.ICommunicationObject"/> was able to enter the <see cref="F:System.ServiceModel.CommunicationState.Opened"/> state and has entered the <see cref="F:System.ServiceModel.CommunicationState.Faulted"/> state.</exception>
      public void EndOpen(IAsyncResult result)
      {
      }

      /// <summary>
      /// Gets the current state of the communication-oriented object.
      /// </summary>
      /// <returns>
      /// The value of the <see cref="T:System.ServiceModel.CommunicationState"/> of the object.
      /// </returns>
      public CommunicationState State { get; private set; }

      public event EventHandler Closed;
      public event EventHandler Closing;
      public event EventHandler Faulted;
      public event EventHandler Opened;
      public event EventHandler Opening;

      public T1 GetProperty<T1>() where T1 : class
      {
         return null;
      }

      #endregion

      #region Implementation of IChannelFactory<T>

      public T CreateChannel(EndpointAddress to)
      {
         return chan;
      }

      public T CreateChannel(EndpointAddress to, Uri via)
      {
         return chan;
      }

      #endregion
   }
}