﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ViewModels;
using System.Collections.ObjectModel;
using Desktop.Client.ConectedDevices.Aplications.Proxy;
using System.Collections.Generic;
using Common.Library.Observable;

namespace Desktop.Client.Tests.UnitTests
{
   [TestClass]
   public class RecursiveValidatableObjectTests
   {
      private MockDevice device;
      private bool IsChanged;

      [TestMethod]
      public void PropertyChangedOnCollection()
      {        
         device = new MockDevice();
         device.Contacts = CreateContactsList();        

         device.PropertyChanged += (o, e) => 
         {
            IsChanged = true;
         };
         
         // simple property
         IsChanged = false;
         device.Test = true;
         Assert.AreEqual(true, IsChanged);
         
         // collection changed (add item)
         IsChanged = false;
         device.Contacts.Add(new Contact() { Name = "NoName" });
         Assert.AreEqual(true, IsChanged);
         
         // change property inside collection
         IsChanged = false;
         device.Contacts.Last().Name = "New Super Name";
         Assert.AreEqual(true, IsChanged);        
      }

      private ObservableCollection<Contact> CreateContactsList()
      {
         ObservableCollection<Contact> contacts = new ObservableCollection<Contact>();

         for(int i = 0; i < 3; i++)
         {
            Contact c = new Contact();
            c.Name = "Name 0" + i;
            c.Phone = "+420" + i + i + i + i;
            contacts.Add(c);
         }
         return contacts;
      }
   }

   #region Mock device

   public class MockDevice : RecursiveValidatableObservableObject
   {
      private ObservableCollection<Contact> contacts;
      private bool test;   

      public ObservableCollection<Contact> Contacts 
      {
         get { return contacts; }
         set { SetPropertyAndValidate(ref contacts, value); }       
      }   

      public bool Test 
      {
         get { return test; }
         set { SetPropertyAndValidate(ref test, value); }
      }

      public MockDevice()
      {
         test = false;       
      }
   }

   #endregion
}
