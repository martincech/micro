﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Desktop.Client.NavigationService.Aplications;

namespace Desktop.Client.Tests.UnitTests
{
   /// <summary>
   /// Summary description for NavigationNodeViewModelTests
   /// </summary>
   [TestClass]
   public class NavigationNodeViewModelTests
   {

      NavigationNodeViewModel viewModel;

      [TestInitialize]
      public void Init()
      {
         viewModel = new SampleData.SampleNavigationNodeViewModel();
      }

      [TestMethod]
      public void HasModels_WhenCreated()
      {
         Assert.IsNotNull(viewModel.Models);
      }

       [TestMethod]
       public void ChildDeleted_WhenParentDeleted()
       { 
          var parent = viewModel.Models.FirstOrDefault();
          var child = parent.ChildViewNodes.FirstOrDefault();
          Assert.IsNotNull(parent);
          Assert.IsNotNull(child);

          viewModel.RemoveNavigationNode(parent);

          Assert.IsFalse(viewModel.Models.Contains(parent));
          Assert.IsFalse(viewModel.Models.Contains(child));
       }

       [TestMethod]
       public void TreeNodeIsSorted_WhenAddedNewNode()
       {
          var first = viewModel.Models.FirstOrDefault();
          var last = viewModel.Models.LastOrDefault();

          Assert.IsTrue(first == viewModel.Models[0]);
          Assert.IsTrue(last == viewModel.Models[viewModel.Models.Count-1]);

          var subFirst = first.ChildViewNodes.FirstOrDefault();
          var subLast = first.ChildViewNodes.LastOrDefault();

          Assert.IsTrue(subFirst == first.ChildViewNodes[0]);
          Assert.IsTrue(subLast == first.ChildViewNodes[first.ChildViewNodes.Count - 1]);

          var newSubFirst = viewModel.AddNavigationNode("newSubFirst",subFirst.Order-1,null,first);
          var newSubLast = viewModel.AddNavigationNode("newSubLast",subLast.Order+1,null,first);

          Assert.IsFalse(subFirst == first.ChildViewNodes[0]);
          Assert.IsFalse(subLast == first.ChildViewNodes[first.ChildViewNodes.Count - 1]);

          Assert.IsTrue(newSubFirst == first.ChildViewNodes[0]);
          Assert.IsTrue(newSubLast == first.ChildViewNodes[first.ChildViewNodes.Count - 1]);

          var newFirst = viewModel.AddNavigationNode("newFirst", first.Order - 1);
          var newLast = viewModel.AddNavigationNode("newLast", last.Order + 1);

          Assert.IsFalse(first == viewModel.Models[0]);
          Assert.IsFalse(last == viewModel.Models[viewModel.Models.Count - 1]);

          Assert.IsTrue(newFirst == viewModel.Models[0]);
          Assert.IsTrue(newLast == viewModel.Models[viewModel.Models.Count - 1]);

       }

       [TestMethod]
       public void TreeNodeIsSorted_WhenOrderNotSet()
       {          
          var last = viewModel.AddNavigationNode("NodeLast1");
          Assert.IsTrue(viewModel.Models.LastOrDefault() == last);
          var last2 = viewModel.AddNavigationNode("NodeLast2");
          Assert.IsTrue(viewModel.Models.LastOrDefault() == last2);
       }

       [TestMethod]
       public void ModelChanged_WhenSelectedNodeChanged()
       {
          Assert.IsNull(viewModel.Model);          
          var model = viewModel.Models.Where(p => p.View != null).FirstOrDefault();
          Assert.IsNotNull(model);
          viewModel.SelectViewCommand.CanExecute(model);
          viewModel.SelectViewCommand.Execute(model);
          Assert.IsTrue(model == viewModel.Model);
       }

       [TestMethod]
       public void SelectedNodeChanged_WhenSelectedNodeHasNoView()
       {
          Assert.IsNull(viewModel.Model);
          var model = viewModel.Models.Where(p => p.View == null && p.ChildViewNodes.Where(c => c.View != null).FirstOrDefault() != null).FirstOrDefault();
          Assert.IsNotNull(model);
          Assert.IsTrue(viewModel.SelectViewCommand.CanExecute(model));
          viewModel.SelectViewCommand.Execute(model);
          Assert.IsTrue(viewModel.Model == model.ChildViewNodes.Where(p => p.View != null).FirstOrDefault());
       }

   }
}
