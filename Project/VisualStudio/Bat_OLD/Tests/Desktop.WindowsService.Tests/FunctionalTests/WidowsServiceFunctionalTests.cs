﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ServiceModel;
using Connection.Interface.Contract;
using Connection.Interface.Domain;
using System.Threading;
using Common.Library.Messenger;


namespace Desktop.WindowsService.Tests.FunctionalTest
{
    /// <summary>
    /// Summary description for WidowsServiceTests
    /// </summary>
    [TestClass]
    public class WidowsServiceFunctionalTests
    {
    }

    internal class Bat2CommandServiceSample : Bat2CommandService
    {
        Bat2CommandService bb = new Bat2CommandService();
        public void Start()
        {  
            base.OnStart(null);
        }
        public void Stop()
        {
            base.OnStop();
        }
    }

    internal class EventReceivedArgs : EventArgs
    {
        public Bat2DeviceData DeviceData { get; internal set; }
        public Bat2EventType Operation { get; internal set; }
        public object[] Args { get; internal set; }

        public EventReceivedArgs(Bat2DeviceData deviceData, Bat2EventType operation, params object[] args)
        {
            this.DeviceData = deviceData;
            this.Operation = operation;
            this.Args = args;
        }
    }

    internal class Bat2EventHandler : IBat2EventsContract
    {
        public event EventHandler<EventReceivedArgs> EventReceived;

        public void DeviceConnected(Connection.Interface.Domain.Bat2DeviceData deviceData)
        {
            if (EventReceived != null)
                EventReceived(this, new EventReceivedArgs(deviceData, Bat2EventType.DeviceConnected));
        }

        public void DeviceDisconnected(Connection.Interface.Domain.Bat2DeviceData deviceData)
        {
            if (EventReceived != null)
                EventReceived(this, new EventReceivedArgs(deviceData, Bat2EventType.DeviceDisconnected));
        }

        public void DataRead(Connection.Interface.Domain.Bat2DeviceData data)
        {
            if (EventReceived != null)
                EventReceived(this, new EventReceivedArgs(data, Bat2EventType.DataRead));
        }

        public void DataWriten(Connection.Interface.Domain.Bat2DeviceData data)
        {
            if (EventReceived != null)
                EventReceived(this, new EventReceivedArgs(data, Bat2EventType.DataWriten));
        }

        public void WeighingStarted(Connection.Interface.Domain.Bat2DeviceData deviceData)
        {
            if (EventReceived != null)
                EventReceived(this, new EventReceivedArgs(deviceData, Bat2EventType.WeighingStarted));
        }

        public void WeighingPlanned(Connection.Interface.Domain.Bat2DeviceData deviceData, DateTime at)
        {
            if (EventReceived != null)
                EventReceived(this, new EventReceivedArgs(deviceData, Bat2EventType.WeighingPlanned, new object[] { at }));
        }

        public void WeighingStopped(Connection.Interface.Domain.Bat2DeviceData deviceData)
        {
            if (EventReceived != null)
                EventReceived(this, new EventReceivedArgs(deviceData, Bat2EventType.WeighingStopped));
        }

        public void WeighingSuspended(Connection.Interface.Domain.Bat2DeviceData deviceData)
        {
            if (EventReceived != null)
                EventReceived(this, new EventReceivedArgs(deviceData, Bat2EventType.WeighingSuspended));
        }

        public void WeighingReleased(Connection.Interface.Domain.Bat2DeviceData deviceData)
        {
            if (EventReceived != null)
                EventReceived(this, new EventReceivedArgs(deviceData, Bat2EventType.WeighingReleased));
        }

        public void TimeSet(Connection.Interface.Domain.Bat2DeviceData deviceData, DateTime time)
        {
            if (EventReceived != null)
                EventReceived(this, new EventReceivedArgs(deviceData, Bat2EventType.TimeSet, new object[] { time }));
        }

        public void TimeGet(Connection.Interface.Domain.Bat2DeviceData deviceData, DateTime time)
        {
            if (EventReceived != null)
                EventReceived(this, new EventReceivedArgs(deviceData, Bat2EventType.TimeGet, new object[] { time }));
        }

        public void ErrorRised(Bat2DeviceData deviceData, ErrorMessage errorMessage)
        {
            if (EventReceived != null)
                EventReceived(this, new EventReceivedArgs(deviceData, Bat2EventType.ErrorRised, new object[] { errorMessage }));
        }
    }
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    internal class Bat2EventsContractService : IBat2EventsContract
    {
        #region Implementation of IBat2ConnectionEventsContract

        /// <summary>
        /// Event is invoked when device is connected.
        /// </summary>
        /// <param name="deviceData">device data according to connected device</param>
        public void DeviceConnected(Bat2DeviceData deviceData)
        {
            MessengerSend((new Bat2ConnectionEventsContractMessage(e => e.DeviceConnected(deviceData))), deviceData);
        }

        /// <summary>
        /// Event is invoked when device is disconnected.
        /// </summary>
        /// <param name="deviceData">device data according to disconnected device</param>
        public void DeviceDisconnected(Bat2DeviceData deviceData)
        {
            MessengerSend((new Bat2ConnectionEventsContractMessage(e => e.DeviceDisconnected(deviceData))), deviceData);
        }

        #endregion

        #region Implementation of IBat2DataReadEventsContract

        /// <summary>
        /// Event is invoked when new data are readed from device.
        /// </summary>
        /// <param name="deviceData">Data readed from device</param>
        public void DataRead(Bat2DeviceData deviceData)
        {
            MessengerSend((new Bat2DataReadEventsContractMessage(e => e.DataRead(deviceData))), deviceData);
        }

        #endregion

        #region Implementation of IBat2DataWriteEventsContract

        /// <summary>
        /// Event is invoked when new data are writen to device
        /// </summary>
        /// <param name="deviceData">Data writen to device</param>
        public void DataWriten(Bat2DeviceData deviceData)
        {
            MessengerSend((new Bat2DataWriteEventsContractMessage(e => e.DataWriten(deviceData))), deviceData);
        }

        #endregion

        #region Implementation of IBat2ActionEventsContract

        public void WeighingStarted(Bat2DeviceData deviceData)
        {
            MessengerSend((new Bat2ActionEventsContractMessage(e => e.WeighingStarted(deviceData))), deviceData);
        }

        public void WeighingPlanned(Bat2DeviceData deviceData, DateTime at)
        {
            MessengerSend((new Bat2ActionEventsContractMessage(e => e.WeighingPlanned(deviceData, at))), deviceData);
        }

        public void WeighingStopped(Bat2DeviceData deviceData)
        {
            MessengerSend((new Bat2ActionEventsContractMessage(e => e.WeighingStopped(deviceData))), deviceData);
        }

        public void WeighingSuspended(Bat2DeviceData deviceData)
        {
            MessengerSend((new Bat2ActionEventsContractMessage(e => e.WeighingSuspended(deviceData))), deviceData);
        }

        public void WeighingReleased(Bat2DeviceData deviceData)
        {
            MessengerSend((new Bat2ActionEventsContractMessage(e => e.WeighingReleased(deviceData))), deviceData);
        }

        public void TimeSet(Bat2DeviceData deviceData, DateTime time)
        {
            MessengerSend((new Bat2ActionEventsContractMessage(e => e.TimeSet(deviceData, time))), deviceData);
        }

        public void TimeGet(Bat2DeviceData deviceData, DateTime time)
        {
            MessengerSend(new Bat2ActionEventsContractMessage(e => e.TimeGet(deviceData, time)), deviceData);
        }

        public void ErrorRised(Bat2DeviceData deviceData, ErrorMessage errorMessage)
        {
            MessengerSend(new Bat2ErrorEventsContractMessage(e => e.ErrorRised(deviceData, errorMessage)), deviceData);
        }

        #endregion
        /// <summary>
        /// Send message with and without serial number token
        /// </summary>
        private void MessengerSend<T>(T message, Bat2DeviceData deviceData)
        {
            Messenger.Default.Send(message);
            Messenger.Default.Send(message, deviceData.Configuration.VersionInfo.SerialNumber);
        }

        
    }

    internal class Bat2DataWriteEventsContractMessage
    {

        public Bat2DataWriteEventsContractMessage(Action<IBat2DataWriteEventsContract> actionToInvoke)
        {
            ActionToInvoke = actionToInvoke;
        }
        public Action<IBat2DataWriteEventsContract> ActionToInvoke { get; private set; }
    }

    internal class Bat2DataReadEventsContractMessage
    {

        public Bat2DataReadEventsContractMessage(Action<IBat2DataReadEventsContract> actionToInvoke)
        {
            ActionToInvoke = actionToInvoke;
        }
        public Action<IBat2DataReadEventsContract> ActionToInvoke { get; private set; }
    }

    internal class Bat2ConnectionEventsContractMessage
    {

        public Bat2ConnectionEventsContractMessage(Action<IBat2ConnectionEventsContract> actionToInvoke)
        {
            ActionToInvoke = actionToInvoke;
        }
        public Action<IBat2ConnectionEventsContract> ActionToInvoke { get; private set; }
    }

    internal class Bat2ActionEventsContractMessage
    {

        public Bat2ActionEventsContractMessage(Action<IBat2ActionEventsContract> actionToInvoke)
        {
            ActionToInvoke = actionToInvoke;
        }
        public Action<IBat2ActionEventsContract> ActionToInvoke { get; private set; }
    }

    internal class Bat2ErrorEventsContractMessage
    {

        public Bat2ErrorEventsContractMessage(Action<IBat2ErrorEventsContract> actionToInvoke)
        {
            ActionToInvoke = actionToInvoke;
        }
        public Action<IBat2ErrorEventsContract> ActionToInvoke { get; private set; }
    }

    internal enum Bat2EventType 
    {
        DeviceConnected,
        DeviceDisconnected,
        DataRead,
        DataWriten,
        WeighingStarted,
        WeighingPlanned,
        WeighingStopped,
        WeighingSuspended,
        WeighingReleased,
        TimeSet,
        TimeGet,
        ErrorRised
    }
}
