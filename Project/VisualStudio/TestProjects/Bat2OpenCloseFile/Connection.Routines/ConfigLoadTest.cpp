#include "ConfigLoadTest.h"

#using "Connection.Manager.Native.dll" as_friend
using namespace System;
using namespace Connection::Routines;
using namespace Connection::Manager::Interface::Domain;
using namespace Connection::Manager::Native;

ConfigLoadTest::ConfigLoadTest(HidDevice ^device)
{
   if (!UsbBat2Device::IsValid(device->Vid, device->Pid))
   { 
      throw gcnew ArgumentOutOfRangeException(String::Format("This hid device ({0}/{1}) is not valid bat2 device", device->Vid.ToString("X"), device->Pid.ToString("X")));
   }
   this->device = device;
}


bool ConfigLoadTest::LoadConfig()
{
   try{
      Bat2DataRW rw(gcnew UsbBat2Device(gcnew Bat2UsbSocket(device)));
      Connection::Manager::Interface::Domain::Configuration ^config = rw.LoadConfig();
      return config != nullptr && config->IsValid;
   }
   catch (Exception ^ex)
   {
      return false;
   }
}