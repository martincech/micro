﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connection.Usb;

namespace Accu_GUI
{
   public class Charger
   {
      private Bq2425x bq2425x;
      private static int registersNum = 7;   // registers count

      private byte[] registers;

      public event EventHandler<byte[]> DataReaded;

      /// <summary>
      /// Class initialize.
      /// </summary>
      /// <param name="usb">HID device</param>    
      public Charger(HidDevice usb)
      {
         bq2425x = new Bq2425x(usb);         
         registers = new byte[registersNum];

         bq2425x.RegistersReaded += bq2425x_RegistersReaded;
      }

      public void bq2425x_RegistersReaded(object sender, byte[] e)
      {
         /*Console.WriteLine("Charger - data content");      
         System.Console.WriteLine("Content: {0}", BitConverter.ToString(e));
         Console.WriteLine("\nEnd");*/
     
         for (int i = 0; i < registersNum; i++)
         {
            registers[i] = e[i];
         }

         InvokeEvent(e);
      }
   

      private void InvokeEvent(byte[] data)
      {
         if (DataReaded != null)
         {
            DataReaded(this, data);
         }
      }


      /// <summary>
      /// Call Bq2425x for request charger type
      /// </summary>
      /// <returns>type</returns>
      public void ChargerType()
      {        
         bq2425x.RegisterRead((int)ModelView.TChargerRegisters.Type + 1); 
      }

      /// <summary>
      /// Returns name of charger type. Method must be calls after process reading from device.
      /// </summary>
      /// <returns>charger type</returns>
      public string GetChargerType()
      {
         byte register = registers[(int)ModelView.TChargerRegisters.Type];
         //parse raw data answer
         int type = register & 0x03;  //clear higher 6 bits
         string typeChargerString = "";

         if (type == (int)Bq2425x.TTypeCharger.DCP)
         {
            typeChargerString = "DCP";
         }
         else if (type == (int)Bq2425x.TTypeCharger.CDP)
         {
            typeChargerString = "CDP";
         }
         else if (type == (int)Bq2425x.TTypeCharger.SDP)
         {
            typeChargerString = "SDP";
         }
         else
         {
            typeChargerString = "Apple/TT/Non-standard";
         }

         return typeChargerString;
      }

      /// <summary>
      /// Call Bq2425x for request current limit.
      /// </summary>
      public void ChargeCurrentLimit()
      {
         bq2425x.RegisterRead((int)ModelView.TChargerRegisters.CurrentLimit + 1);
      }

      /// <summary>
      /// Get charge current limit value. Unit: mA
      /// If returns: -1 value - External ILIM current limit
      ///              0 value - No input current limit.
      /// Method must be calls after process reading from device.             
      /// </summary>
      /// <returns>current limit</returns>
      public int GetChargeCurrentLimit()
      {
         byte register = registers[(int)ModelView.TChargerRegisters.CurrentLimit];
         // data
         int limit = (register & 0x70) >> 4;  //clear bit 7 and 3-0 and shift right
         int currentLimit = 0;

         switch(limit)
         {
            case 0:
               currentLimit = 100;
               break;
            case 1:
               currentLimit = 150;
               break;
            case 2:
               currentLimit = 500;
               break;
            case 3:
               currentLimit = 900;
               break;
            case 4:
               currentLimit = 1500;
               break;
            case 5:
               currentLimit = 2000;
               break;
            case 6:  //External ILIM current limit
               currentLimit = -1;
               break;
            case 7:  //No input current limit
               currentLimit = 0;
               break;
         }
     
         return currentLimit; 
      }

      /// <summary>
      /// Call Bq2425x for request current value.
      /// </summary>
      public void ChargeCurrent()
      {
         bq2425x.RegisterRead((int)ModelView.TChargerRegisters.Current + 1);
      }

      /// <summary>
      /// Get Bq2425x charge current value. Unit: mA
      /// Method must be calls after process reading from device.
      /// </summary>
      /// <returns>charge current</returns>
      public int GetChargeCurrent()
      {
         byte register = registers[(int)ModelView.TChargerRegisters.Current];
         // data
         int current = register & 0xf8;  //clear lower 3 bits
         int chargeCurrent = 500;

         if ((current & 0x80) != 0)
         {
            chargeCurrent += 800;
         }
         if ((current & 0x40) != 0)
         {
            chargeCurrent += 400;
         }
         if ((current & 0x20) != 0)
         {
            chargeCurrent += 200;
         }
         if ((current & 0x10) != 0)
         {
            chargeCurrent += 100;
         }
         if ((current & 0x08) != 0)
         {
            chargeCurrent += 50;
         }
       
         return chargeCurrent;
      }
   }
}
