﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connection.Usb;

namespace Accu_GUI
{
   public class ModelView
   {
      // Fuel Gauge fields
      public double FGAccuVoltage { get; private set; }
      public byte[] FGAccuVoltageRaw;
      public double FGAccuCurrent { get; private set; }
      public byte[] FGAccuCurrentRaw;
      public double FGAccuTemperature { get; private set; }
      public byte[] FGAccuTemperatureRaw;

      // Charger fields
      public string ChType { get; private set; }
      public int ChCurrentLimit { get; private set; }
      public int ChChargerCurrent { get; private set; }
      public byte[] ChRegisters;

      private HidDevice usb;
      private FuelGauge fuelGauge;
      private Charger charger;
      private const int vid = 0xffff;
      private const int pid = 0xffff;

      public enum TEAccuRpcCmd
      {
         CMD_MAX17047_READ,
         CMD_MAX17047_WRITE,
         CMD_BQ2425X_READ,
         CMD_BQ2425X_WRITE
      };

      public enum TModuleType
      {
         Max17047,
         Bq2425x
      };

      public enum TPacketSize
      {
         Max17047Read = 3,
         Bq2425xRead = 1
      };

      public enum TChargerRegisters
      {
         Type = 2,
         CurrentLimit = 1,
         Current = 3
      };

      public ModelView()
      {
         usb = new HidDevice(vid, pid);
         //fuelGauge = new FuelGauge(usb);
         charger = new Charger(usb);

         FGAccuCurrent = 0;
         FGAccuCurrentRaw = new byte[2];
         FGAccuTemperature = 0;
         FGAccuTemperatureRaw = new byte[2];
         FGAccuVoltage = 0;
         FGAccuVoltageRaw = new byte[2];

         ChType = "";
         ChCurrentLimit = 0;
         ChChargerCurrent = 0;
         ChRegisters = new byte[7];

         charger.DataReaded += charger_DataReaded;
      }

      void charger_DataReaded(object sender, byte[] e)
      {
         //TODO: test if sender is Charger or FuelGauge and process data e

         ChType = charger.GetChargerType();
         ChRegisters[(int)TChargerRegisters.Type] = e[(int)TChargerRegisters.Type];

         ChCurrentLimit = charger.GetChargeCurrentLimit();
         ChRegisters[(int)TChargerRegisters.CurrentLimit] = e[(int)TChargerRegisters.CurrentLimit];

         ChChargerCurrent = charger.GetChargeCurrent();
         ChRegisters[(int)TChargerRegisters.Current] = e[(int)TChargerRegisters.Current];        
      
         //TODO: FuelGauge service
      }


      public void UpdateData()
      {
         /*FGAccuCurrent = fuelGauge.GetAccuCurrent();
         FGAccuCurrentRaw = fuelGauge.RawData;

         FGAccuTemperature = fuelGauge.GetAccuTemperature();
         FGAccuTemperatureRaw = fuelGauge.RawData;

         FGAccuVoltage = fuelGauge.GetAccuVoltage();
         FGAccuVoltageRaw = fuelGauge.RawData;*/

         charger.ChargerType();         
         charger.ChargeCurrentLimit();       
         charger.ChargeCurrent();       
      }

      public void Close()
      {
         usb.Close();
      }
   }
}
