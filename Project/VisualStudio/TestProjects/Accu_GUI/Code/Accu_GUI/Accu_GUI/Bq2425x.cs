﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Connection.Usb;

namespace Accu_GUI
{
    public class Bq2425x
    {    
      private Rpc rpc;
      private byte[] registers;
      private const int registersNum = 7;                  // registers count

      public event EventHandler<byte[]> RegistersReaded;

      public enum TTypeCharger
      {
         DCP,
         CDP,
         SDP,
         Apple_TT
      };

      public Bq2425x(HidDevice usb)
      {
         rpc = new Rpc(usb);       
         registers = new byte[registersNum];        

         rpc.SuccesfullyReaded += rpc_SuccesfullyReaded;                 
      } 

      void rpc_SuccesfullyReaded(object sender, byte[] e)
      {
         //TODO: data dojdou i na modul Max17047 a obracene, proto musim otestovat, zda data patri spravnemu modelu


         //test prijatych dat
         /*Console.WriteLine("Content of Response");      
         System.Console.WriteLine("Content: {0}", BitConverter.ToString(e));
         Console.WriteLine("\nEnd");*/

         //parse packet to only registers data
         for (int i = 0; i < registersNum; i++)
         {
            registers[i] = e[i+2];
         }
     
         InvokeEvent(registers);
      }   

      private void InvokeEvent(byte[] data)
      {
         if (RegistersReaded != null)
         {
            RegistersReaded(this, data);
         }
      }


      /// <summary>
      /// Fill registers by raw data from device response
      /// </summary>
      /*public void GetChargerRegisters()
      {
         Array.Clear(ChargerRegisters, 0, ChargerRegisters.Length);

         for (int i = 0; i < registersNum; i++)
         {
            ChargerRegisters[i] = rpc.Response[i + 2];
         }
      }*/

 
      /// <summary>
      /// Read all Bq2425x registers and returned required register.
      /// </summary>
      /// <param name="registerNumber">number of required register</param>
      /// <returns>register from Bq2425x</returns>
      public void RegisterRead(int registerNumber)
      {
         if (registerNumber < 1 || registerNumber > 7)
            throw new ArgumentException("Number of register must be from 1 to 7.");
               
         rpc.Read(ModelView.TModuleType.Bq2425x, registerNumber);        
      }          

      /// <summary>
      /// Write to registers
      /// </summary>
      /// <param name="registerNumber"></param>
      /// <param name="data"></param>
      public void RegisterWrite(int registerNumber, byte data)
      {
         if (registerNumber < 1 || registerNumber > 7)
            throw new ArgumentException("Number of register must be from 1 to 7.");
        
         registers[registerNumber - 1] = data;
         rpc.Write(ModelView.TModuleType.Bq2425x, registerNumber, registers);
      }
   }
}
