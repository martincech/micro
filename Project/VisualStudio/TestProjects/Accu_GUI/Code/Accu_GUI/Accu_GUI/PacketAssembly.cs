﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accu_GUI
{
   /// <summary>
   /// Class for assembly read or write packet.
   /// </summary>
   class PacketAssembly
   {
      private ModelView.TModuleType modul;         // module type
      public int CommandType { get; private set; } // command type

      public PacketAssembly(ModelView.TModuleType modul)
      {
         this.modul = modul;
      }
    
      /// <summary>
      /// Create packet for read.
      /// </summary>
      /// <param name="address">register address</param>
      /// <returns>packet for send to device</returns>
      public byte[] CreatePacket(int address)
      {
         int size = 0;

         if (modul == ModelView.TModuleType.Max17047)
         {
            size = (int)ModelView.TPacketSize.Max17047Read;          
            CommandType = (int)ModelView.TEAccuRpcCmd.CMD_MAX17047_READ;          
         }
         else if (modul == ModelView.TModuleType.Bq2425x)
         {
            size = (int)ModelView.TPacketSize.Bq2425xRead;
            CommandType = (int)ModelView.TEAccuRpcCmd.CMD_BQ2425X_READ;
         }

         byte[] packet = new byte[size];
         packet[0] = (byte)CommandType;

         if (modul == ModelView.TModuleType.Max17047)
         {
            packet[1] = (byte)address; // adress                
            packet[2] = 0x01;          // required data count
         }

         return packet;
      }

      /// <summary>
      /// Create packet for write to registers.
      /// </summary>
      /// <param name="address">register address</param>
      /// <param name="data">writing data</param>
      /// <returns>packet for send to device</returns>
      public byte[] CreatePacket(int address, byte[] data)
      {
         int size = 0;

         if (modul == ModelView.TModuleType.Max17047)
         {
            size = (int)ModelView.TPacketSize.Max17047Read;
            CommandType = (int)ModelView.TEAccuRpcCmd.CMD_MAX17047_WRITE;
         }
         else if (modul == ModelView.TModuleType.Bq2425x)
         {
            size = (int)ModelView.TPacketSize.Bq2425xRead;
            CommandType = (int)ModelView.TEAccuRpcCmd.CMD_BQ2425X_WRITE;
         }

         byte[] packet = new byte[size];
         packet[0] = (byte)CommandType;

         if (modul == ModelView.TModuleType.Max17047)
         {
            packet[1] = (byte)address; // adress
            packet[2] = 0x01;          // count of writing data
         }

         byte[] sendPacket = new byte[packet.Length + data.Length];
         System.Buffer.BlockCopy(packet, 0, sendPacket, 0, packet.Length);
         System.Buffer.BlockCopy(data, 0, sendPacket, packet.Length, data.Length);

         return sendPacket;
      }
   }
}
