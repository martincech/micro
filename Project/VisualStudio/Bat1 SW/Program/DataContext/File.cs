﻿using Bat1Library;
using DataModel;

namespace DataContext
{
   /// <summary>
   /// One file
   /// </summary>
   public class File : NameNote
   {
      /// <summary>
      /// File-specific setup
      /// </summary>
      public readonly FileConfig FileConfig;

      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="name"></param>
      /// <param name="note"></param>
      /// <param name="fileConfig"></param>
      public File(string name, string note, FileConfig fileConfig) : base(name, note)
      {
         FileConfig = fileConfig;
      }


      internal static File FromDbFile(DataModel.File file)
      {
         if (file == null)
         {
            return null;
         }
         var fileConfig = new FileConfig();
         fileConfig.WeighingConfig.Saving.EnableMoreBirds = file.EnableMoreBirds.HasValue && file.EnableMoreBirds.Value;
         fileConfig.WeighingConfig.Saving.NumberOfBirds = file.NumberOfBirds;
         fileConfig.WeighingConfig.WeightSorting.Mode = (WeightSorting) file.WeightSortingMode;
         fileConfig.WeighingConfig.WeightSorting.LowLimit = file.LowLimit;
         fileConfig.WeighingConfig.WeightSorting.HighLimit = file.HighLimit;
         fileConfig.WeighingConfig.Saving.Mode = (SavingMode) file.SavingMode;
         fileConfig.WeighingConfig.Saving.Filter = file.Filter;
         fileConfig.WeighingConfig.Saving.StabilisationTime = file.StabilizationTime;
         fileConfig.WeighingConfig.Saving.StabilisationRange = file.StabilizationRange;
         fileConfig.WeighingConfig.Saving.MinimumWeight = file.MinimumWeight;

         // Vytvorim novy soubor a vratim
         return new File(file.Name, file.Note, fileConfig);
      }

      internal static File FromDbFile(ManualResult file)
      {
         if (file == null)
         {
            return null;
         }
         var fileConfig = new FileConfig();

         // Vytvorim novy soubor a vratim
         return new File(file.Name, file.Note, fileConfig);
      }

      internal static File FromDbFile(WeighingFile file)
      {
         if (file == null)
         {
            return null;
         }
         var fileConfig = new FileConfig();

         // Vytvorim novy soubor a vratim
         return new File(file.Name, file.Note, fileConfig);
      }

      internal void ToDbFile(ref DataModel.File dbFile)
      {
         dbFile.Name = Name;
         dbFile.Note = Note;
         dbFile.EnableMoreBirds = FileConfig.WeighingConfig.Saving.EnableMoreBirds;
         dbFile.NumberOfBirds = (byte) FileConfig.WeighingConfig.Saving.NumberOfBirds;
         dbFile.WeightSortingMode = (byte) FileConfig.WeighingConfig.WeightSorting.Mode;
         dbFile.LowLimit = FileConfig.WeighingConfig.WeightSorting.LowLimit;
         dbFile.HighLimit = FileConfig.WeighingConfig.WeightSorting.HighLimit;
         dbFile.SavingMode = (byte) FileConfig.WeighingConfig.Saving.Mode;
         dbFile.Filter = FileConfig.WeighingConfig.Saving.Filter;
         dbFile.StabilizationTime = FileConfig.WeighingConfig.Saving.StabilisationTime;
         dbFile.StabilizationRange = FileConfig.WeighingConfig.Saving.StabilisationRange;
         dbFile.MinimumWeight = FileConfig.WeighingConfig.Saving.MinimumWeight;
      }

      internal void ToDbFile(ref SampleResult dbFile)
      {
         dbFile.Name = Name;
         dbFile.Note = Note;        
      }
   }
}