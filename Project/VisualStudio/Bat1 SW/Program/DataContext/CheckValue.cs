﻿using System.Linq;

namespace DataContext
{
   public static class CheckValue
   {
      /// <summary>
      /// Allowable characters in text saved to the scale
      /// </summary>
      private static readonly char[] ValidScaleChars = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
         'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
         'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
         ' ', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
         '_', '+', '-', '*', '/', '=', '!', '?', '.', ',', ':' ,';' ,'%', '#', '&'};

      /// <summary>
      /// Check if character fits into the specified set of chars
      /// </summary>
      /// <param name="ch">Character to check</param>
      /// <param name="validChars">Array of valid characters</param>
      /// <returns>True if valid</returns>
      public static bool CheckScaleChar(char ch, char[] validChars)
      {
         return validChars.Any(validChar => ch == validChar);
      }

      /// <summary>
      /// Check if character is valid for the scale
      /// </summary>
      /// <param name="ch">Character to check</param>
      /// <returns>True if valid</returns>
      public static bool CheckScaleChar(char ch)
      {
         return CheckScaleChar(ch, ValidScaleChars);
      }

      /// <summary>
      /// Check text saved to the scale. Text can be empty.
      /// </summary>
      /// <param name="text">Text to check</param>
      /// <returns>True if text is valid</returns>
      public static bool CheckScaleText(string text)
      {
         return text.All(CheckScaleChar);
      }

      /// <summary>
      /// Check name saved to the scale. Name cannot be empty.
      /// </summary>
      /// <param name="name">Text to check</param>
      /// <returns>True if text is valid</returns>
      public static bool CheckScaleName(string name)
      {
         return name != "" && CheckScaleText(name);
      }
   }
}
