﻿using System;

namespace DataContext {

    /// <summary>
    /// Info about one weighing for searching
    /// </summary>
    public struct WeighingSearchInfo {
        /// <summary>
        /// Weighing Id in the database
        /// </summary>
        public long Id;

        /// <summary>
        /// Timestamp of the first sample
        /// </summary>
        public DateTime MinDateTime;

        /// <summary>
        /// Timestamp of the last sample
        /// </summary>
        public DateTime MaxDateTime;

        /// <summary>
        /// File name
        /// </summary>
        public string FileName;

        /// <summary>
        /// Scale name
        /// </summary>
        public string ScaleName;

        /// <summary>
        /// Weighing note
        /// </summary>
        public string Note;
    }
}
