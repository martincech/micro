using System;
using System.Collections.Generic;
using Bat1Library;

namespace DataContext
{
   /// <summary>
   /// Flock definition
   /// </summary>
   public class FlockDefinition
   {
      // Kazde hejno muze obsahovat vice souboru, cimz se pokryje situace, kdy se kurata presouvaji z jedne haly/farmy do jine.

      /// <summary>
      /// Flock name
      /// </summary>
      public string Name;

      /// <summary>
      /// Flock note
      /// </summary>
      public string Note;

      /// <summary>
      /// Default standard growth curve of the flock (curve for males with mixed sex)
      /// </summary>
      public Curve CurveDefault;

      /// <summary>
      /// Standard growth curve of females, when mixed sex is used
      /// </summary>
      public Curve CurveFemales;

      /// <summary>
      /// Date and time when the flock was started
      /// </summary>
      public DateTime Started;
      //{
      //   get { return flockFileList[0].From; }
      //} // Datum zacatku prvniho souboru

      /// <summary>
      /// Day number corresponding to the date when the flock was started
      /// </summary>
      public int StartedDay;

      /// <summary>
      /// List of files and date ranges included in the flock. Sorted by From date.
      /// </summary>
      public List<FlockFile> FlockFileList
      {
         get { return flockFileList; }
      }

      private List<FlockFile> flockFileList;      

      /// <summary>
      /// Flock ID in the database (for simple updates in the database)
      /// </summary>
      public long Id;

      public bool IsActive;

      /// <summary>
      /// Constructor
      /// </summary>
      public FlockDefinition()
      {
         Name = "";
         Note = "";
         CurveDefault = null;
         CurveFemales = null;
         StartedDay = 1;
         flockFileList = new List<FlockFile>();
         Id = -1;
         IsActive = true;
      }

      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="name">Flock name</param>
      public FlockDefinition(string name) : this()
      {
         Name = name;
      }

      /// <summary>
      /// Copy constructor
      /// </summary>
      /// <param name="sourceFlockDefinition">Source flock to copy from</param>
      /// <param name="copyId">Copy flock ID as well</param>
      public FlockDefinition(FlockDefinition sourceFlockDefinition, bool copyId)
      {
         Name = sourceFlockDefinition.Name;
         Note = sourceFlockDefinition.Note;
         CurveDefault = CopyCurve(sourceFlockDefinition.CurveDefault);
         CurveFemales = CopyCurve(sourceFlockDefinition.CurveFemales);
         StartedDay = sourceFlockDefinition.StartedDay;

         flockFileList = new List<FlockFile>(sourceFlockDefinition.flockFileList);
         if (copyId)
         {
            Id = sourceFlockDefinition.Id;
         }
         else
         {
            Id = -1;
         }
         IsActive = sourceFlockDefinition.IsActive;
      }

      /// <summary>
      /// Copy growth curve
      /// </summary>
      /// <param name="sourceCurve">Source curve</param>
      /// <returns>Curve copy</returns>
      private static Curve CopyCurve(Curve sourceCurve)
      {
         if (sourceCurve == null)
         {
            return null; // Zadna krivka neni zadana
         }
         var curve = new Curve(sourceCurve) {Id = sourceCurve.Id};
         return curve;
      }

      /// <summary>
      /// Check if file index is valid within the flock
      /// </summary>
      /// <param name="index">File index</param>
      /// <returns>True if valid</returns>
      public bool CheckIndex(int index)
      {
         return index >= 0 && index < flockFileList.Count;
      }

      /// <summary>
      /// Sort the list by from date
      /// </summary>
      private void Sort()
      {        
         flockFileList.Sort(FlockFile.CompareByFrom);
      }

      /// <summary>
      /// Add new file to the flock
      /// </summary>
      /// <param name="fileName">File name</param>
      /// <param name="from">From date</param>
      /// <param name="to">To date</param>
      /// <returns>True if successful</returns>
      public bool Add(string fileName, long id, DateTime from, DateTime to)
      {
         // Zkontroluju platnost jmena
         if (!CheckValue.CheckScaleName(fileName))
         {
            return false;
         }

         // Od-Do musi jit postupne
         if (from > to)
         {
            return false;
         }

         // Ulozim novy bod
         flockFileList.Add(new FlockFile(fileName, id, from, to));

         // Na zaver setridim seznam podle dne
         Sort();

         return true;
      }

      /// <summary>
      /// Delete file from the flock
      /// </summary>
      /// <param name="index">File index</param>
      public void Delete(int index)
      {
         if (!CheckIndex(index))
         {
            return;
         }
         flockFileList.RemoveAt(index);
      }

      /// <summary>
      /// Compare function for List(FlockDefinition).Sort()
      /// </summary>
      /// <param name="x">First parameter</param>
      /// <param name="y">Second parameter</param>
      /// <returns>1, -1 or 0</returns>
      public static int CompareByName(FlockDefinition x, FlockDefinition y)
      {
         var result = String.CompareOrdinal(x.Name, y.Name);
         if (result < 0)
         {
            return -1;
         }
         return result > 0 ? 1 : 0;
      }

      /// <summary>
      /// Compare function for List(FlockDefinition).Sort()
      /// </summary>
      /// <param name="x">First parameter</param>
      /// <param name="y">Second parameter</param>
      /// <returns>1, -1 or 0</returns>
      public static int CompareByTime(FlockDefinition x, FlockDefinition y)
      {
         var result = DateTime.Compare(x.Started, y.Started);
         if (result != 0)
         {
            return result; // Datumy jsou rozdilne
         }

         // Datumy jsou shodne, rozhodne nazev
         return CompareByName(x, y);
      }

      /// <summary>
      /// Get curve for a specified flag
      /// </summary>
      /// <param name="flag">Flag</param>
      /// <returns>Curve</returns>
      public Curve GetCurve(Flag flag)
      {
         if (flag == Flag.FEMALE)
         {
            // Samice maji specialni krivku
            return CurveFemales;
         }
         // Samci nebo vse ostatni pouzivaji krivku default
         return CurveDefault;
      }

      /// <summary>
      /// Check if the flock is currently running (i.e. the flock is not closed)
      /// </summary>
      /// <returns>True if the flock is current</returns>
      //public bool IsCurrent()
      //{
      //   return IsActive;
      //   // Projedu vsechny soubory a pokud je datum Do u libovolneho z nich roven DateTime.MaxValue, hejno bezi
      //   //return flockFileList.Any(flockFile => flockFile.To.Date == DateTime.MaxValue.Date);
      //}

      /// <summary>
      /// Set flock To date
      /// </summary>
      /// <param name="toDateTime">To date</param>
      //private void SetToDate(DateTime toDateTime)
      //{
      //   // U vsech souboru nastavim datum Do na toDateTime
      //   // FlockFile je struct, tj. musim vytvorit seznam znovu.
      //   var newFlockFileList = flockFileList.Select(flockFile => new FlockFile(flockFile.FileName, flockFile.Id, flockFile.From, toDateTime)).ToList();

      //   // Preberu novy seznam
      //   flockFileList = newFlockFileList;
      //}

      /// <summary>
      /// Set flock as current
      /// </summary>
      //public void SetCurrent()
      //{
      //   IsActive = true;
      //   // U vsech souboru nastavim datum Do na DateTime.MaxValue
      //   //SetToDate(DateTime.MaxValue);
      //}

      /// <summary>
      /// Set flock as closed
      /// </summary>
      /// <param name="closedDateTime">Closed date</param>
      //public void SetClosed(DateTime closedDateTime)
      //{
      //   IsActive = false;
      //   // U vsech souboru nastavim datum Do na closedDateTime
      //   SetToDate(closedDateTime);
      //}

      internal static FlockDefinition FromDbFlock(DataModel.Flock flock)
      {
         if (flock == null)
         {
            return null;
         }      

         var flockDefinition = new FlockDefinition
         {
            Id = flock.FlockId,
            Name = flock.Name,
            Note = flock.Note,
            StartedDay = (int) (flock.StartedDay.HasValue ? flock.StartedDay : 0),
            CurveDefault = Curve.FromDbCurve(flock.CurveDefault),
            CurveFemales = Curve.FromDbCurve(flock.CurveFemale),
            Started = flock.FromDateTime
         };
         return flockDefinition;
      }
   }
}