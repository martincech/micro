﻿namespace DataContext {
   /// <summary>
    /// One weighing in memory
    /// </summary>
    public class WeighingData {
        /// <summary>
        /// Weighing Id in database
        /// </summary>
        public long Id { get { return id; } }
        private long id;

        /// <summary>
        /// Result type (dowloaded from new scale, old scale or manually entered)
        /// </summary>
        public ResultType ResultType { get { return resultType; } }
        private ResultType resultType;

        /// <summary>
        /// Record source in the database (saved in the SW or imported)
        /// </summary>
        public RecordSource RecordSource { get { return recordSource; } set { recordSource = value; } }
        private RecordSource recordSource;

        /// <summary>
        /// File (pointing to a file in ScaleConfig)
        /// </summary>
        public File File { get { return file; } }
        private File file;

        /// <summary>
        /// List of samples
        /// </summary>
        public SampleList SampleList { get { return sampleList; } set { sampleList = value; } }
        private SampleList sampleList;

        /// <summary>
        /// Global setup of the scale
        /// </summary>
        public ScaleConfig ScaleConfig { get { return scaleConfig; } }
        private ScaleConfig scaleConfig;

        /// <summary>
        /// Weighing note
        /// </summary>
        public string Note { get { return note; } set { note = value; } }
        private string note;

        /// <summary>
        /// Constructor
        /// </summary>
        public WeighingData(long id, File file,
                            SampleList sampleList, ScaleConfig scaleConfig, string note) 
        {
            this.id           = id;
            this.file         = file;
            this.sampleList   = sampleList;
            this.scaleConfig  = scaleConfig;
            this.note         = note;
            resultType = ResultType.MANUAL;
        }      

        public WeighingData(long id, ResultType resultType, RecordSource recordSource, File file,
                             SampleList sampleList, Bat1Scale scaleConfig, string note)
        {
           this.id = id;
           this.resultType = resultType;
           this.recordSource = recordSource;
           this.file = file;
           this.sampleList = sampleList;
           this.scaleConfig = scaleConfig;
           this.note = note;
        }
    }
}
