namespace DataContext
{
   /// <summary>
   /// CSV delimiter
   /// </summary>
   public enum CsvDelimiter {
      COMMA,
      SEMICOLON,
      TAB
   }
}