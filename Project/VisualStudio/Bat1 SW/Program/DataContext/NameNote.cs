﻿using System;

namespace DataContext
{
   public class NameNote
   {
      /// <summary>
      /// Item name
      /// </summary>
      public string Name { get; private set; }

      /// <summary>
      /// Item note
      /// </summary>
      public string Note { get; private set; }

      /// <summary>
      /// Empty contructor
      /// </summary>
      public NameNote()
      {
         Name = Note = "";
      }

      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="name">Name</param>
      /// <param name="note">Note</param>
      public NameNote(string name, string note)
      {
         // Zkontroluju platnost jmena
         if (!CheckValue.CheckScaleName(name))
         {
            throw new ArgumentException("Name is not valid");
         }

         // Zkontroluju platnost poznamky
         if (!CheckValue.CheckScaleText(note))
         {
            throw new ArgumentException("Note is not valid");
         }

         // Ulozim
         Name = name;
         Note = note;
      }

      /// <summary>
      /// Rename
      /// </summary>
      /// <param name="newName">New name</param>
      public bool Rename(string newName)
      {
         // Zkontroluju platnost jmena
         if (!CheckValue.CheckScaleName(newName))
         {
            return false;
         }
         Name = newName; // Jmeno je v poradku
         return true;
      }

      /// <summary>
      /// Rename with Unicode characters (use only with averaging in flocks)
      /// </summary>
      /// <param name="newName">New name</param>
      public void RenameUnicode(string newName)
      {
         // Platnost jmena nekontroluju
         Name = newName;
      }

      /// <summary>
      /// Change note
      /// </summary>
      /// <param name="newNote">New note</param>
      public bool SetNote(string newNote)
      {
         // Zkontroluju platnost poznamky
         if (!CheckValue.CheckScaleText(newNote))
         {
            return false;
         }
         Note = newNote;
         return true;
      }

      /// <summary>
      /// Compare function for List(NameNote).Sort()
      /// </summary>
      /// <param name="x">First parameter</param>
      /// <param name="y">Second parameter</param>
      /// <returns>1, -1 or 0</returns>
      public static int CompareByName(NameNote x, NameNote y)
      {
         // Musim tridit podle ASCII hodnot, stejne jako ve vaze
         var result = String.CompareOrdinal(x.Name, y.Name);
         if (result < 0)
         {
            return -1;
         }
         return result > 0 ? 1 : 0;
      }

      /// <summary>
      /// Return NAME (NOTE). If note isn't specified, returns only NAME.
      /// </summary>
      /// <returns>Full string</returns>
      public override string ToString()
      {
         var str = Name;
         if (Note != "")
         {
            str += " (" + Note + ")";
         }
         return str;
      }
   }
}