﻿namespace DataContext
{
   /// <summary>
   /// One point in a growth curve
   /// </summary>
   public struct CurvePoint
   {
      /// <summary>
      /// Day number
      /// </summary>
      public int Day { get; set; }

      /// <summary>
      /// Weight
      /// </summary>
      public float Weight { get; set; }

      /// <summary>
      /// Constrictor
      /// </summary>
      /// <param name="day"></param>
      /// <param name="weight"></param>
      public CurvePoint(int day, float weight) : this()
      {
         Day = day;
         Weight = weight;
      }

      /// <summary>
      /// Compare function for List(GrowthCurvePoint).Sort()
      /// </summary>
      /// <param name="x">First parameter</param>
      /// <param name="y">Second parameter</param>
      /// <returns>1, -1 or 0</returns>
      public static int CompareByDay(CurvePoint x, CurvePoint y)
      {
         if (x.Day < y.Day)
         {
            return -1;
         }
         if (x.Day > y.Day)
         {
            return 1;
         }
         return 0;
      }
   }
}
