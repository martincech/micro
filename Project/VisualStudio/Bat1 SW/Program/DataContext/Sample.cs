﻿//******************************************************************************
//
//   Sample.cs      One sample stored in the Bat1 scale
//   Version 1.0
//
//******************************************************************************

using System;
using Bat1Library;

namespace DataContext
{
   /// <summary>
   /// One stored sample
   /// </summary>
   public struct Sample
   {
      /// <summary>
      /// Date and time when the sample was saved
      /// </summary>
      public DateTime DateTime { get; set; }

      /// <summary>
      /// Weight in kg/g/lb
      /// </summary>
      public float Weight { get; set; }

      /// <summary>
      /// Flag
      /// </summary>
      public Flag Flag { get; set; }

      private static int WeightComparison(Sample x, Sample y)
      {
         if (x.Weight < y.Weight)
         {
            return -1;
         }
         if (x.Weight > y.Weight)
         {
            return 1;
         }
         return 0;
      }

      private static int FlagComparison(Sample x, Sample y)
      {
         if (x.Flag < y.Flag)
         {
            return -1;
         }
         return x.Flag > y.Flag ? 1 : 0;
      }

      /// <summary>
      /// Compare function for List(Sample).Sort()
      /// </summary>
      /// <param name="x">First parameter</param>
      /// <param name="y">Second parameter</param>
      /// <returns>1, -1 or 0</returns>
      public static int CompareByTime(Sample x, Sample y)
      {
         var result = DateTime.Compare(x.DateTime, y.DateTime);
         if (result != 0)
         {
            return result; // Datumy jsou rozdilne
         }

         // Datumy jsou shodne, rozhodne hmotnost
         result = WeightComparison(x, y);
         if (result != 0)
         {
            return result; // Hmotnosti jsou rozdilne
         }

         // Musi rozhodnout flag
         return FlagComparison(x, y);
      }

      /// <summary>
      /// Compare function for List(Sample).Sort()
      /// </summary>
      /// <param name="x">First parameter</param>
      /// <param name="y">Second parameter</param>
      /// <returns>1, -1 or 0</returns>
      public static int CompareByWeight(Sample x, Sample y)
      {
         var result = WeightComparison(x, y);
         if (result != 0)
         {
            return result; // Hmotnosti jsou rozdilne
         }

         // Hmotnosti jsou shodne, rozhodne datum
         result = DateTime.Compare(x.DateTime, y.DateTime);
         if (result != 0)
         {
            return result; // Datumy jsou rozdilne
         }

         // Musi rozhodnout flag
         return FlagComparison(x, y);
      }

      internal static Sample FromDbSample(DataModel.Sample dbSample)
      {
         return new Sample
         {
            Weight = (float) dbSample.Weight,
            Flag = (Flag) dbSample.Flag,
            DateTime = dbSample.SavedDateTime
         };
      }
   }
}