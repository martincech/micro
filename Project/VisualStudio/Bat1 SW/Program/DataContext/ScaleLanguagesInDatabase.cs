namespace DataContext
{
   /// <summary>
   /// List of unique language codes used in the scale. These codes are used only for saving the scale language
   /// to the DB. The order of languages corresponds to scale version 7.01.x and must NOT be changed. New languages
   /// in higher scale versions are always added to the end of the list. Thanks to this, the list of languages
   /// stays always compatible with older SW versions.
   /// </summary>
   public enum ScaleLanguagesInDatabase {
      CZECH,
      DUTCH,
      ENGLISH,
      FINNISH,
      FRENCH,
      GERMAN,
      JAPANESE,
      PORTUGUESE,
      RUSSIAN,
      SPANISH,
      TURKISH,        // Tady konci puvodni verze vahy 7.00.0 a 7.01.0 (obe maji shodne jazyky)
      HUNGARIAN,      // Madarstina pridana ve vaze 7.02.0
      POLISH,         // Polstina pridana ve vaze 7.03.0
      ITALIAN         // Italstina pridana ve vaze 7.04.0
   }
}