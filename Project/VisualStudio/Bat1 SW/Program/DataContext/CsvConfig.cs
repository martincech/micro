namespace DataContext
{
   /// <summary>
   /// CSV export config
   /// </summary>
   public struct CsvConfig {
      /// <summary>
      /// Predefined sets of parameters
      /// </summary>
      public CsvTemplate Template;
        
      /// <summary>
      /// Column delimiter
      /// </summary>
      public CsvDelimiter Delimiter;

      /// <summary>
      /// Decimal separator
      /// </summary>
      public CsvDecimalSeparator DecimalSeparator;

      /// <summary>
      /// File encoding 
      /// </summary>
      public CsvEncoding Encoding;
   }
}