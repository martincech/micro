﻿using BatLibrary;

namespace DataContext
{
   public class Bat2OldScale : ScaleConfig
   {
      public Bat2OldScale(Units units)
         : base(units)
      {
      }

      public Bat2OldScale(ScaleConfig config)
         : base(config)
      {
      }
   }
}
