﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bat1Library;
using Bat1Library.Statistics;
using BatLibrary;

namespace DataContext
{
   /// <summary>
   /// Results of one statistic calculation
   /// </summary>
   public class StatisticResult
   {
      /// <summary>
      /// Number of histogram slots
      /// </summary>
// ReSharper disable once InconsistentNaming
      public static int HISTOGRAM_SLOT_COUNT = 39;

      /// <summary>
      /// Flag used for calculation
      /// </summary>
      public readonly Flag Flag;

      /// <summary>
      /// Number of samples
      /// </summary>
      public readonly int Count;

      /// <summary>
      /// Average weight rounded to grams (i.e. the same as in the scale)
      /// </summary>
      public float Average;

      /// <summary>
      /// Exact average weight (not rounded) used for calculations like Count * Average
      /// </summary>
      public float ExactAverage;

      /// <summary>
      /// Standard deviation rounded to grams (i.e. the same as in the scale)
      /// </summary>
      public float Sigma;

      /// <summary>
      /// Exact Sigma (not rounded) used for units conversions
      /// </summary>
      public float ExactSigma;

      /// <summary>
      /// Coeficient of variation
      /// </summary>
      public readonly float Cv;

      /// <summary>
      /// Uniformity
      /// </summary>
      public readonly float Uniformity;

      public readonly float? Gain;

      /// <summary>
      /// Weighing speed in chicks per hour
      /// </summary>
      public double WeighingSpeed;

      /// <summary>
      /// Histogram
      /// </summary>
      public Histogram Histogram;

      /// <summary>
      /// Date and time when the weighing was started
      /// </summary>
      public readonly DateTime WeighingStarted;

      /// <summary>
      /// Constructor used with data downloaded from scales
      /// </summary>
      /// <param name="sampleList">List of samples</param>
      /// <param name="flag">Filter by flag</param>
      /// <param name="uniformityRange">Uniformity range in percents</param>
      /// <param name="histogramConfig"></param>
      /// <param name="units"></param>
      public StatisticResult(SampleList sampleList, Flag flag, int uniformityRange, HistogramConfig histogramConfig,
         Units units)
      {
         var statistics = new Statistics();

         // Ulozim si flag
         Flag = flag;

         // Naplnim data do vypoctu statistiky
         sampleList.First(flag);
         while (sampleList.Read(flag))
         {
            statistics.Add(sampleList.Sample.Weight);
         }

         // Vypoctu vsechny statisticke hodnoty
         // Average a Sigma vypoctu rovnou jako ve vaze, tj. na gramy. Prumer 3.26598 se tak prevede napr. na
         // 3.26500002, tuto hodnotu musim jeste pred zobrazenim zaokrouhlit normalne aritmeticky.
         Count = statistics.Count();

         ExactAverage = statistics.Average();
         Average = float.Parse(DisplayFormat.CutWeight(ExactAverage, units)); // Jako ve vaze
         ExactSigma = statistics.Sigma();
         Sigma = float.Parse(DisplayFormat.CutWeight(ExactSigma, units)); // Jako ve vaze
         Cv = statistics.Variation(Average, Sigma);
         // Pocitam pomoci Average i Sigma zaokrouhleneho na gramy, aby to sedelo s vahou
         Uniformity = statistics.Uniformity(uniformityRange, Average);
         // Pocitam pomoci Average zaokrouhleneho na gramy, aby to sedelo s vahou
         var span = sampleList.MaxDateTime - sampleList.MinDateTime; // Delka vazeni
         if (Math.Abs(span.TotalHours) > 0)
         {
            WeighingSpeed = Count/span.TotalHours;
         }
         else
         {
            WeighingSpeed = 1;
         }

         // Histogram
         Histogram = statistics.Histogram(histogramConfig, units, HISTOGRAM_SLOT_COUNT);

         // Zacatek vazeni beru z prvniho vzorku
         WeighingStarted = sampleList.MinDateTime;
      }

      /// <summary>
      /// Constructor used with manually entered results
      /// </summary>
      /// <param name="flag"></param>
      /// <param name="count"></param>
      /// <param name="average"></param>
      /// <param name="sigma"></param>
      /// <param name="cv"></param>
      /// <param name="uniformity"></param>
      /// <param name="weighingStarted"></param>
      /// <param name="gain"></param>
      public StatisticResult(Flag flag, int count, float average, float sigma, float cv, float uniformity,
         DateTime weighingStarted, float? gain = null)
      {
         // Preberu zadane hodnoty
         Flag = flag;
         Count = count;
         ExactAverage = Average = average;
         ExactSigma = Sigma = sigma;
         Cv = cv;
         Uniformity = (float)Math.Round((Decimal)uniformity, 1, MidpointRounding.AwayFromZero);
         Gain = gain;

         // Ostatni hodnoty vynuluju
         Histogram = null;
         WeighingSpeed = 0;

         // Zacatek vazeni zadal rucne
         WeighingStarted = weighingStarted;
      }

      public StatisticResult(Flag flag, int count, float average, float sigma, float cv, float uniformity,
         DateTime weighingStarted, float? gain, Histogram histogram) 
            : this(flag, count, average, sigma, cv, uniformity, weighingStarted, gain)
      {
         Histogram = histogram;
      }

      public static float ViewUniformity(float uniformity)
      {
         return (uniformity * 0.1f);
      }

      public static short ReCalcUniformity(float uniformity)
      {
         return (short)((Decimal)uniformity * 10);
      }


      /// <summary>
      /// Join histograms to total one.
      /// </summary>
      /// <param name="histograms">List of histograms from Bat2</param>
      /// <param name="samples">List of samples from Bat1</param>
      /// <returns>final histogram</returns>
      public static Histogram JoinHistograms(IEnumerable<Histogram> histograms, IEnumerable<SampleList> samples)
      {
         int countBelow = 0, countAbove = 0;
         var valueList = new List<float>();    
         double min = double.MaxValue, max = 0;

         // Get values from Bat2 histograms
         foreach (var hist in histograms)
         {
            if (hist == null)
            {  // Manual result without histogram
               return null;
            }
            
            //if there are any values below or above
            if (hist.Counts[0] > 0)
            {
               countBelow += hist.Counts[0];
            }
            if (hist.Counts[hist.Counts.Count() - 1] > 0)
            {
               countAbove += hist.Counts[hist.Counts.Count() - 1];
            }

            // Add values to valuelist
            for (var i = 1; i < hist.Counts.Count() - 2; i++)
            {
               if (hist.Counts[i] > 0)
               {
                  var count = hist.Counts[i];
                  while (count > 0)
                  {
                     valueList.Add(hist.Values[i]);
                     count--;
                  }
               }
            }

            // Get axis range from histograms         
            // It's sufficient check first and last item for range, because items are sorted
            var pMax = hist.Values[hist.Counts.Count() - 1];
            var pMin = hist.Values[0];
            min = pMin < min ? pMin : min;
            max = pMax > max ? pMax : max;     
         }

         // Get axis range from samples
         foreach (var samplelist in samples)
         {
            samplelist.First();
            while (samplelist.Read())
            {
               valueList.Add(samplelist.Sample.Weight);
               min = samplelist.Sample.Weight < min ? samplelist.Sample.Weight : min;
               max = samplelist.Sample.Weight > max ? samplelist.Sample.Weight : max;
            }
         }

         //join
         var joinedHistogram = new Histogram(HISTOGRAM_SLOT_COUNT)
         {
            Step = (float)((max - min) / HISTOGRAM_SLOT_COUNT)
         };

         //Add values
         joinedHistogram.Values[0] = joinedHistogram.Step;
         for (var i = 1; i < HISTOGRAM_SLOT_COUNT; i++)
         {
            joinedHistogram.Values[i] = joinedHistogram.Values[i - 1] + joinedHistogram.Step;
         }

         //Add count for each column
         foreach (var value in valueList)
         {
            if (value < joinedHistogram.Values[1] - joinedHistogram.Step / 2.0)
            {  // below
               joinedHistogram.Counts[0]++;
               continue;
            }
            if (value > joinedHistogram.Values[HISTOGRAM_SLOT_COUNT - 2] + joinedHistogram.Step / 2.0)
            {  // above
               joinedHistogram.Counts[HISTOGRAM_SLOT_COUNT - 1]++;
               continue;
            }
            // inside
            for (var i = 1; i < HISTOGRAM_SLOT_COUNT - 1; i++)
            {
               if (value < joinedHistogram.Values[i] + joinedHistogram.Step / 2.0)
               {
                  joinedHistogram.Counts[i]++;
                  break;
               }
            }
         }

         //add rest of values
         joinedHistogram.Counts[0] += countBelow;
         joinedHistogram.Counts[HISTOGRAM_SLOT_COUNT - 1] += countAbove;        
         return joinedHistogram;
      }
   }
}