﻿using System;
using System.Net;
using Bat2Library;
using BatLibrary;
using Bat2Library.Connection.Interface.Domain;

namespace DataContext
{
   public class Bat2Scale : ScaleConfig
   {
      public int SerialNumber;
      public CodePageE CodePage;
      public ScaleConfigVersion HwVersion;
      public short Modification;
      public short Class;
      public Ethernet Ethernet = new Ethernet();
      public bool SavePower;
      public DacsOptions DacsOption = new DacsOptions();
      public MegaviOptions MegaviOptions = new MegaviOptions();
      public ModbusOptions ModbusOptions = new ModbusOptions();
      public CellularData CellularData = new CellularData();
      //public ObservableCollection<Rs485Options> Rs485Optionses = new ObservableCollection<Rs485Options>();
      public PlatformCalibration PlatformCalibration = new PlatformCalibration();
      public DataPublication DataPublication = new DataPublication();
      public WeighingConfiguration WeighingConfiguration = new WeighingConfiguration();
      //public ObservableCollection<string> CorrectionCurvesNames = new ObservableCollection<string>();
      //public ObservableCollection<string> GrowthCurvesNames = new ObservableCollection<string>();
      //public ObservableCollection<string> WeighingPlansNames = new ObservableCollection<string>();
      //public GsmMessage GsmMessage = new GsmMessage();

      public Bat2Scale() : base()
      {
         DefaultBat2Config();
      }

      public Bat2Scale(Bat2Scale config)
         : base(config)
      {
         SerialNumber = config.SerialNumber;
      }

      public Bat2Scale(ScaleConfig config)
         : base(config)
      {
         DefaultBat2Config();
      }

      public Bat2Scale(Units units)
         : base(units)
      {
         DefaultBat2Config();
      }

      private void DefaultBat2Config()
      {
         SerialNumber = -1;
         CodePage = 0;
      }

      internal static Bat2Scale FromDbScaleConfig(DataModel.Bat2Scale dbConfig)
      {
         var scaleConfig = ScaleConfig.FromDbScaleConfig(dbConfig);
         var bat2Config = new Bat2Scale(scaleConfig);
         bat2Config.SerialNumber = dbConfig.SerialNumber;
         bat2Config.CodePage = (CodePageE)dbConfig.CodePage.Value;
         bat2Config.HwVersion.Major = dbConfig.HwMajorVersion.Value;
         bat2Config.HwVersion.Minor = dbConfig.HwMinorVersion.Value;
         bat2Config.HwVersion.Build = dbConfig.HwBuildVersion.Value;
         bat2Config.Modification = dbConfig.Modification.Value;
         bat2Config.Class = dbConfig.Class.Value;
         bat2Config.Display.Contrast = dbConfig.DisplayContrast.Value;
         bat2Config.SavePower = dbConfig.SavePower.Value;
         bat2Config.Ethernet.Dhcp = dbConfig.Ethernet.Dhcp.Value;
         bat2Config.Ethernet.Gateway = new IPAddress(BitConverter.GetBytes(dbConfig.Ethernet.Gateway.Value));
         bat2Config.Ethernet.Ip = new IPAddress(BitConverter.GetBytes(dbConfig.Ethernet.Ip.Value));
         bat2Config.Ethernet.PrimaryDns = new IPAddress(BitConverter.GetBytes(dbConfig.Ethernet.PrimaryDns.Value));
         bat2Config.Ethernet.SubnetMask = new IPAddress(BitConverter.GetBytes(dbConfig.Ethernet.SubnetMask.Value));
         bat2Config.DacsOption.Address = dbConfig.DacsOptions.Address.Value;
         bat2Config.DacsOption.DeviceAddress = dbConfig.DacsOptions.DeviceAddress.Value;
         bat2Config.DacsOption.Version = (DacsVersionE)dbConfig.DacsOptions.Version;
         bat2Config.MegaviOptions.Address = dbConfig.MegaviOptions.Address.Value;
         bat2Config.MegaviOptions.Code = dbConfig.MegaviOptions.Code.Value;
         bat2Config.MegaviOptions.DeviceAddress = dbConfig.MegaviOptions.DeviceAddress.Value;
         bat2Config.ModbusOptions.Address = dbConfig.ModbusOptions.Address.Value;
         bat2Config.ModbusOptions.BaudRate = dbConfig.ModbusOptions.BaudRate.Value;
         bat2Config.ModbusOptions.DataBits = (ModbusDataBitsE)dbConfig.ModbusOptions.DataBits;
         bat2Config.ModbusOptions.DeviceAddress = dbConfig.ModbusOptions.DeviceAddress.Value;
         bat2Config.ModbusOptions.Mode = (ModbusTransmissionModeE)dbConfig.ModbusOptions.Mode;
         bat2Config.ModbusOptions.Parity = (ModbusParityE)dbConfig.ModbusOptions.Parity;
         bat2Config.ModbusOptions.ReplyDelay = dbConfig.ModbusOptions.ReplyDelay.Value;
         bat2Config.CellularData.Apn = dbConfig.CellularData.Apn;
         bat2Config.CellularData.Password = dbConfig.CellularData.Password;
         bat2Config.CellularData.Username = dbConfig.CellularData.Username;
         bat2Config.Units.Decimals = dbConfig.UnitsDecimals.Value;
         bat2Config.Units.Division = dbConfig.UnitsDivision;
         bat2Config.Units.MaxDivision = dbConfig.UnitsDivisionMax.Value;
         bat2Config.Units.Range = dbConfig.UnitsRange.Value;
         bat2Config.Units.Units = (Units)dbConfig.Units;
      
         //bat2Config.Rs485Optionses.Clear();
         //bat2Config.Rs485Optionses.Add(FromDbRs485Options(dbConfig.Rs485Options1));
         //bat2Config.Rs485Optionses.Add(FromDbRs485Options(dbConfig.Rs485Options2));

         //TODO: PlatformCalibration, DataPublication, WeighingConfiguration
         //bat2Config.PlatformCalibration.Delay = dbConfig.p

         return bat2Config;
      }

      private static Rs485Options FromDbRs485Options(DataModel.Rs485Options option)
      {
         var rs = new Rs485Options
         {
            Enabled = option.Enabled.Value,
            Mode = (Rs485ModeE)option.Mode.Value,
            PhysicalDeviceAddress = option.PhysicalInterfaceAddress.Value,           
         };
         return rs;
      }    

      internal DataModel.Bat2Scale ToDbScaleConfig(DataModel.Bat2Scale dbConfig)
      {
         var scaleConfig = base.ToDbScaleConfig(dbConfig);
         dbConfig = (DataModel.Bat2Scale)scaleConfig;         
         dbConfig.SerialNumber = SerialNumber;
         dbConfig.CodePage = (byte)CodePage;
         dbConfig.HwMajorVersion = (byte)HwVersion.Major;
         dbConfig.HwMinorVersion = (byte)HwVersion.Minor;
         dbConfig.HwBuildVersion = (byte)HwVersion.Build;
         dbConfig.Modification = Modification;
         dbConfig.Class = Class;
         dbConfig.DisplayContrast = (byte)Display.Contrast;
         dbConfig.SavePower = SavePower;
         dbConfig.Ethernet.Dhcp = Ethernet.Dhcp;
         dbConfig.Ethernet.Gateway = BitConverter.ToInt32(Ethernet.Gateway.GetAddressBytes(),0);
         dbConfig.Ethernet.Ip = BitConverter.ToInt32(Ethernet.Ip.GetAddressBytes(),0);
         dbConfig.Ethernet.PrimaryDns =  BitConverter.ToInt32(Ethernet.PrimaryDns.GetAddressBytes(),0);
         dbConfig.Ethernet.SubnetMask =  BitConverter.ToInt32(Ethernet.SubnetMask.GetAddressBytes(),0);
         dbConfig.DacsOptions.Address = DacsOption.Address;
         dbConfig.DacsOptions.DeviceAddress = DacsOption.DeviceAddress;
         dbConfig.DacsOptions.Version = (byte)DacsOption.Version;
         dbConfig.MegaviOptions.Address = MegaviOptions.Address;
         dbConfig.MegaviOptions.Code = MegaviOptions.Code;
         dbConfig.MegaviOptions.DeviceAddress = MegaviOptions.DeviceAddress;
         dbConfig.ModbusOptions.Address = ModbusOptions.Address;
         dbConfig.ModbusOptions.BaudRate = ModbusOptions.BaudRate;
         dbConfig.ModbusOptions.DataBits = (byte)ModbusOptions.DataBits;
         dbConfig.ModbusOptions.DeviceAddress = ModbusOptions.DeviceAddress;
         dbConfig.ModbusOptions.Mode = (byte)ModbusOptions.Mode;
         dbConfig.ModbusOptions.Parity = (byte)ModbusOptions.Parity;
         dbConfig.ModbusOptions.ReplyDelay = ModbusOptions.ReplyDelay;
         dbConfig.CellularData.Apn = CellularData.Apn;
         dbConfig.CellularData.Password = CellularData.Password;
         dbConfig.CellularData.Username = CellularData.Username;
         dbConfig.UnitsDecimals = (byte)Units.Decimals;
         dbConfig.UnitsDivision = Units.Division;
         dbConfig.UnitsDivisionMax = (short)Units.MaxDivision;
         dbConfig.UnitsRange = Units.Range;
         dbConfig.Units = (byte) Units.Units;       
         
         //dbConfig.Rs485Options1 = ToDbRs485Options(Rs485Optionses[0]);
         //dbConfig.Rs485Options2 = ToDbRs485Options(Rs485Optionses[1]);
         
         return dbConfig;
      }

      private DataModel.Rs485Options ToDbRs485Options(Rs485Options rs)
      {
         var option = new DataModel.Rs485Options
         {
            Enabled = rs.Enabled,
            Mode = (byte)rs.Mode,
            PhysicalInterfaceAddress = rs.PhysicalDeviceAddress,          
         };
         return option;      
      }     
   }
}
