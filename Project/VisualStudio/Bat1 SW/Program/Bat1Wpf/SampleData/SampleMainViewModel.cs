﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Bat1Wpf.ModelViews.Applications;
using Bat1Wpf.ModelViews.Models;
using Bat1Wpf.Utilities;

namespace Bat1Wpf.SampleData
{
   public class SampleMainViewModel : MainViewModel
   {
      private static ObservableCollection<CommandObject> list = new CreateCommandObjectItems().CreateMenuItems();

      public SampleMainViewModel() : base(list)
      {
         UiElement = (FrameworkElement)list.Last().Content;
      }
   }
}
