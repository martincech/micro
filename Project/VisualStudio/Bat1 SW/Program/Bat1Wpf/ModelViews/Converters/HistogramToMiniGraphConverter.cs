﻿using System;
using System.Drawing;
using System.Windows;
using System.Windows.Data;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using Bat1;

namespace Bat1Wpf.ModelViews.Converters
{
   public class HistogramToMiniGraphConverter : IValueConverter
   {
      private const int MINIGRAPH_HEIGHT = 20;

      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         var hist = (int[])value;
         if (hist == null) return null;

         var bitmap = new Bitmap(HistogramMiniGraph.GetTotalWidth(hist.Length), MINIGRAPH_HEIGHT);
         HistogramMiniGraph.Draw(hist, Graphics.FromImage(bitmap), bitmap.Height - 1, 1, true);

         var hBitmap = bitmap.GetHbitmap();      
         return Imaging.CreateBitmapSourceFromHBitmap(
                     hBitmap,
                     IntPtr.Zero,
                     Int32Rect.Empty,
                     BitmapSizeOptions.FromEmptyOptions());            
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
