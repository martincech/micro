﻿using System.Collections.ObjectModel;
using Utilities.Observable;

namespace Bat1Wpf.ModelViews.Models
{
   public class Scale : ObservableObject
   {
      private string name;
      private int id;
      private bool isConnected;
      private ObservableCollection<Archive> archives;

      public string Name { get { return name; } set { SetProperty(ref name, value); } }
      public int Id { get { return id; } set { SetProperty(ref id, value); } }
      public bool IsConnected { get { return isConnected; } set { SetProperty(ref isConnected, value); } }
      public ObservableCollection<Archive> Archives { get { return archives; } set { SetProperty(ref archives, value); } }
   }
}
