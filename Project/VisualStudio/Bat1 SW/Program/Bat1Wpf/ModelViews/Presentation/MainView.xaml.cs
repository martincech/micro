﻿using System.Windows.Controls;

namespace Bat1Wpf.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for MainView.xaml
   /// </summary>
   public partial class MainView : UserControl
   { 
      public MainView()
      {
         InitializeComponent();        
      }      
   }
}
