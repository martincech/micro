﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Bat1Wpf.ModelViews.Models;
using Bat2Library;
using Utilities.Observable;

namespace Bat1Wpf.ModelViews.Applications
{
   public class ScaleBat1ViewModel : ObservableObject
   {
      #region Private fields

      private ObservableCollection<CommandObject> scaleCommands;   
      private Scale scale;
      private WeightUnitsE unit;
      private ObservableCollection<CommandObject> archiveCommands;    
      private ObservableCollection<CommandObject> commands;
      private const int DIVIDE_MIN = 1;
      private const int DIVIDE_MAX = 10;

      #endregion

      #region Public interfaces

      #region Constructors

      public ScaleBat1ViewModel(IEnumerable<CommandObject> scaleCmds,
                                IEnumerable<CommandObject> archiveCmds,
                                IEnumerable<CommandObject> cmds)
      {
         ScaleCommands = new ObservableCollection<CommandObject>(scaleCmds);
         ArchiveCommands = new ObservableCollection<CommandObject>(archiveCmds);
         Commands = new ObservableCollection<CommandObject>(cmds);     

         DivideColl = new ObservableCollection<int>();
         for (var i = DIVIDE_MIN; i <= DIVIDE_MAX; i++)
         {
            DivideColl.Add(i);
         }            
      }

      #endregion 

      public ObservableCollection<int> DivideColl { get; set; }

      public ObservableCollection<CommandObject> ScaleCommands { get { return scaleCommands; } set { SetProperty(ref scaleCommands, value); } }
      public Scale Scale { get { return scale; } set { SetProperty(ref scale, value); } }
      public WeightUnitsE Unit { get { return unit; } set { SetProperty(ref unit, value); } }
      public ObservableCollection<CommandObject> ArchiveCommands { get { return archiveCommands; } set { SetProperty(ref archiveCommands, value); } }
      public ObservableCollection<CommandObject> Commands { get { return commands; } set { SetProperty(ref commands, value); } }

      #endregion     
   }
}
