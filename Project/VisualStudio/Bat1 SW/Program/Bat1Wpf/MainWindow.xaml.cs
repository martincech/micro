﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Bat1Wpf.ModelViews.Applications;
using Bat1Wpf.Utilities;

namespace Bat1Wpf
{
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow : Window
   {
      public MainWindow()
      {
         InitializeComponent();

         var menuItems = new CreateCommandObjectItems().CreateMenuItems();
         var vm = new MainViewModel(menuItems);
         MainView.DataContext = vm;

         // TODO:
         // create view models
         var scaleBat1Vm = new ScaleBat1ViewModel(
               new CreateCommandObjectItems().CreateScaleBat1ScaleItems(),
               new CreateCommandObjectItems().CreateScaleBat1ArchiveItems(),
               new CreateCommandObjectItems().CreateScaleBat1Items());
         
         // assigned view model to view
         ((UserControl) menuItems.First().Content).DataContext = scaleBat1Vm;
      }
   }
}
