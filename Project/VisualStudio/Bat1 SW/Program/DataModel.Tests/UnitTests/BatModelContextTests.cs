﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataModel.Tests.UnitTests
{
   [TestClass]
   public class BatModelContextTests
   {
      /// <summary>
      /// Clear data from database among tests.
      /// </summary>
      [TestInitialize]
      public void Init()
      {
         using (var db = new BatModelContainer())
         {
            if (db.Database.Exists())
            {
               db.Database.Delete();
            }
            //db.Database.Create();
            db.Database.CreateIfNotExists();
            Assert.IsTrue(db.Database.Exists());
         }
      }


      [TestMethod]
      //[ExpectedException(typeof(System.Data.Entity.Validation.DbUnexpectedValidationException))]
      [ExpectedException(typeof(System.Data.Entity.Validation.DbEntityValidationException))]
      public void AddCurvesWithoutRequiredParameters()
      {   
         using (var db = new BatModelContainer())
         {
            db.Curves.Add(new Curve());
            db.SaveChanges();                 
         }
      }


      [TestMethod]
      public void AddCurves()
      {
         using (var db = new BatModelContainer())
         {
            Curve curve = new Curve();
            curve.Name = "krivka";          

            db.Curves.Add(curve);
            db.SaveChanges();

            var curves = from s in db.Curves
                         select s;
            Assert.AreEqual(1, curves.Count());
         }
      }

      [TestMethod]
      public void SetupOperation()
      {
         using (var db = new BatModelContainer())
         {
            short value = 2;
            Setup testSetup = new Setup();
            testSetup.Language = value;

            var setups = from s in db.Setups
               select s;
            Assert.AreEqual(0, setups.Count());

            db.Setups.Add(testSetup);
            db.SaveChanges();

            setups = from s in db.Setups
                         select s;
            Assert.AreEqual(1, setups.Count());
            Assert.AreEqual(value, setups.First().Language);
         }
      }

      [TestMethod]
      public void AddFileWithSamples()
      {
         using (var db = new BatModelContainer())
         {
            Bat1Scale scale = new Bat1Scale();
            scale.Name = "TestScale";
            scale.ScaleName = "NoScale";
            scale.DateSeparator1 = ".";
            scale.DateSeparator2 = ".";
            scale.TimeSeparator = ":";
            db.ScaleConfigs.Add(scale);

            File file = new File();
            file.Name = "Soubor";
            file.Note = "";
            scale.Files = new List<WeighingFile>();
            scale.Files.Add(file);

            file.Samples = new List<Sample>();
            Sample sample = new Sample();
            sample.Weight = 100;
            sample.Flag = 0;
            sample.SavedDateTime = DateTime.Now;
            file.Samples.Add(sample);

            db.SaveChanges();

            Assert.AreEqual(1, db.ScaleConfigs.Count());
            Assert.AreEqual(1, db.WeighingFiles.OfType<File>().Count());
            Assert.AreEqual(1, db.Samples.Count());
         }
      }

      [TestMethod]
      public void DeleteFileWithSamples()
      {
         AddFileWithSamples();

         using (var db = new BatModelContainer())
         {
            // It's only 1 scale with 1 file at database
            var scale = db.ScaleConfigs.OfType<Bat1Scale>().First();
            var weighing = scale.Files.First();
          
            var file = db.WeighingFiles.OfType<SampleResult>().First(f => f.Id == weighing.Id);           
            db.Samples.RemoveRange(file.Samples);        
            db.WeighingFiles.Remove(file);

            db.SaveChanges();
            Assert.AreEqual(0, db.WeighingFiles.Count());
            Assert.AreEqual(0, db.WeighingFiles.OfType<File>().Count());
            Assert.AreEqual(0, db.Samples.Count());
            Assert.AreEqual(0, db.WeighingFiles.OfType<SampleResult>().Count());
         }
      }


      [TestMethod]
      public void AddManualResult()
      {
         using (var db = new BatModelContainer())
         {
            Bat1Scale scale = new Bat1Scale();
            scale.Name = "TestScale";
            scale.ScaleName = "NoScale";
            scale.DateSeparator1 = ".";
            scale.DateSeparator2 = ".";
            scale.TimeSeparator = ":";
            db.ScaleConfigs.Add(scale);

            ManualResult mr = new ManualResult();
            mr.Name = "test";
            mr.Note = "";
            mr.OriginDateTime = DateTime.Now;
            //mr.Flag = 0;
            //mr.Count = 10;
            //mr.Average = 2;
            //mr.Sigma = 5;
            //mr.Cv = 3;
            //mr.Uniformity = 4;
            scale.Files = new List<WeighingFile>();
            scale.Files.Add(mr);

            db.SaveChanges();

            Assert.AreEqual(1, db.ScaleConfigs.Count());
            Assert.AreEqual(1, db.WeighingFiles.OfType<ManualResult>().Count());
         }
      }

      [TestMethod]
      public void DeleteManualResult()
      {
         AddManualResult();

         using (var db = new BatModelContainer())
         {
            var scale = db.ScaleConfigs.OfType<Bat1Scale>().First();        
            var weighing = scale.Files.First();

            var manualResult = db.WeighingFiles.OfType<ManualResult>().First(f => f.Id == weighing.Id);
            db.Set<ManualResult>().Remove(manualResult);

            db.ScaleConfigs.Remove(scale);
            db.SaveChanges();
            Assert.AreEqual(0, db.WeighingFiles.Count());
            Assert.AreEqual(0, db.WeighingFiles.OfType<ManualResult>().Count());
         }
      }

      [TestMethod]
      public void DeleteBat1Scale()
      {      
         AddFileWithSamples();

         using (var db = new BatModelContainer())
         {
            var scale = db.ScaleConfigs.OfType<Bat1Scale>().First();            

            DeleteInheritEntities(scale, db);
            db.ScaleConfigs.Remove(scale);

            db.SaveChanges();
            Assert.AreEqual(0, db.ScaleConfigs.Count());
         }
      }

      [TestMethod]
      public void AddGroup()
      {
         AddFileWithSamples();

         using (var db = new BatModelContainer())
         {
            var scale = db.ScaleConfigs.OfType<Bat1Scale>().First();
            var file = db.WeighingFiles.OfType<File>().First(); //scale.Files.First();

            Group group = new Group();
            group.Name = "Grupa1";
            group.Note = "";
            group.WeighingFiles = new List<WeighingFile> {file};

            scale.Groups.Add(group);
            db.SaveChanges();

            Assert.AreEqual(1, db.Groups.Count());
            Assert.AreEqual(1, db.WeighingFiles.Count());
         }
      }

      [TestMethod]
      public void DeleteFile_BelongingGroup()
      {
         AddGroup();
         using (var db = new BatModelContainer())
         {
            var scale = db.ScaleConfigs.OfType<Bat1Scale>().First();
   
            // another possibility
            //DeleteInheritEntities(scale, db);
            //db.ScaleConfigs.Remove(scale);
            //db.SaveChanges();

            // second possibility
            var weighing = scale.Files.First();

            var file = db.WeighingFiles.OfType<File>().First(f => f.Id == weighing.Id);
            db.Samples.RemoveRange(file.Samples);

            //var exist = scale.Groups.Select(g => g.WeighingFiles.Select(f => f.Id == weighing.Id)).First();
            //if (exist.First())
            //{
            //   var group = scale.Groups.First(f => f.WeighingFiles);
            //}
            var gr = file.Groups.FirstOrDefault(f => f.ScaleConfigId == file.ScaleConfigScaleConfigId && f.WeighingFiles.Contains(file));
            int expectedGroups = 0;
            if (gr != null)
            {
               //if (gr.WeighingFiles.Count == 1)
               //{
               //   //containts only this file => delete group
               //   db.Groups.Remove(gr);
               //   expectedGroups = 0;
               //}
               //else
               //{
               //   //delete only file
               //   expectedGroups = 1;
               //}
               expectedGroups = 1;
               gr.WeighingFiles.Remove(file);
            }

            db.Set<File>().Remove(file);

            db.SaveChanges();
            Assert.AreEqual(0, db.WeighingFiles.Count());
            Assert.AreEqual(0, db.WeighingFiles.OfType<File>().Count());
            Assert.AreEqual(0, db.Samples.Count());
            Assert.AreEqual(0, db.WeighingFiles.OfType<SampleResult>().Count());

            Assert.AreEqual(expectedGroups, db.Groups.Count());
         }
      }





      private void DeleteInheritEntities(Bat1Scale scale, BatModelContainer db)
      {
         db.Set<ManualResult>()
            .RemoveRange(
               db.WeighingFiles.OfType<ManualResult>()
                  .Where(f => f.ScaleConfigScaleConfigId == scale.ScaleConfigId));

         foreach (var file in db.WeighingFiles.OfType<SampleResult>().Where(f => f.ScaleConfigScaleConfigId == scale.ScaleConfigId).ToList())
         {         
            db.Entry(file).Collection("Groups").Load();           
            file.Groups.Clear();

            db.Samples.RemoveRange(file.Samples);
            db.WeighingFiles.Remove(file);
         }
      }
   }
}
