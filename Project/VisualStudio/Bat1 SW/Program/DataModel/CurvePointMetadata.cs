//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
namespace DataModel
{
   using System;
   using System.ComponentModel.DataAnnotations;
   using System.ComponentModel.DataAnnotations.Schema;
   
   [MetadataType(typeof(CurvePointMetadata))]
   public partial class CurvePoint
   {
      internal sealed class CurvePointMetadata
   	{
      
        [Display(Name = "Day Number")]
      	public short DayNumber { get; set; }
   
        [Display(Name = "Weight")]
      	public double Weight { get; set; }
   
        [Display(Name = "Curve")]
      	public long CurveId { get; set; }
   
        [Key, Editable(false)]
        [Display(Name = "Id")]
      	public long Id { get; set; }
   
   	}
   }
}
