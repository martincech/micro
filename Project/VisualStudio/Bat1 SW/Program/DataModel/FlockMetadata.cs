//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
namespace DataModel
{
   using System;
   using System.ComponentModel.DataAnnotations;
   using System.ComponentModel.DataAnnotations.Schema;
   using System.Collections.Generic;         
   
   [MetadataType(typeof(FlockMetadata))]
   public partial class Flock
   {
      internal sealed class FlockMetadata
   	{
      
        [Required(ErrorMessage=null, ErrorMessageResourceName ="NameRequired", ErrorMessageResourceType = typeof(DataModel.Properties.Resources))]
   	  [StringLength(30, ErrorMessage=null, ErrorMessageResourceName ="MaxLength", ErrorMessageResourceType = typeof(DataModel.Properties.Resources))]
        [Display(Name = "Name")]
      	public string Name { get; set; }
   
   	  [StringLength(100, ErrorMessage=null, ErrorMessageResourceName ="MaxLength", ErrorMessageResourceType = typeof(DataModel.Properties.Resources))]
        [Display(Name = "Note")]
      	public string Note { get; set; }
   
        [Display(Name = "Started Day")]
      	public Nullable<short> StartedDay { get; set; }
   
        [DataType(DataType.DateTime)]
        [Display(Name = "From Date Time")]
      	public System.DateTime FromDateTime { get; set; }
   
        [DataType(DataType.DateTime)]
        [Display(Name = "To Date Time")]
      	public System.DateTime ToDateTime { get; set; }
   
        [Key, Editable(false)]
        [Display(Name = "Flock")]
      	public long FlockId { get; set; }
   
        public ICollection<WeighingFile> WeighingFiles { get; set; }
   
        public Curve CurveFemale { get; set; }
   
        public Curve CurveDefault { get; set; }
   
   	}
   }
}
