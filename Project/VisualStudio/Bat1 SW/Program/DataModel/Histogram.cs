//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
namespace DataModel
{
    
   
   using System;
   using System.ComponentModel;
   using System.ComponentModel.DataAnnotations;
   using System.ComponentModel.DataAnnotations.Schema;
   
   public partial class Histogram
   {
      #region Constructors
   
      static Histogram()
      {
         TypeDescriptor.AddProviderTransparent(
            new AssociatedMetadataTypeTypeDescriptionProvider(
               typeof(Histogram), typeof(HistogramMetadata)), typeof(Histogram));
      }
      
      public Histogram()
      {
         AditionalConstructor();
      }
   
      partial void AditionalConstructor();
   
      #endregion
   
      #region Primary keys
   
      /// <summary>
      /// This is primary key property!
      /// </summary>
      [Key]
      [Column(Order = 1)]
      public virtual int Id { get; set; }
   
      #endregion
      public virtual byte Value { get; set; }
   
      public virtual Statistic Statistic { get; set; }
   }
}
