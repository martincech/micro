//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
namespace DataModel
{
   using System;
   using System.ComponentModel.DataAnnotations;
   using System.ComponentModel.DataAnnotations.Schema;
   
   [MetadataType(typeof(CellularDataMetadata))]
   public partial class CellularData
   {
      internal sealed class CellularDataMetadata
   	{
      
        [Display(Name = "Apn")]
      	public string Apn { get; set; }
   
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
      	public string Password { get; set; }
   
        [Display(Name = "Username")]
      	public string Username { get; set; }
   
   	}
   }
}
