//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
namespace DataModel
{
   using System;
   using System.ComponentModel.DataAnnotations;
   using System.ComponentModel.DataAnnotations.Schema;
   using System.Collections.Generic;         
   
   [MetadataType(typeof(StatisticMetadata))]
   public partial class Statistic
   {
      internal sealed class StatisticMetadata
   	{
      
        [Display(Name = "Flag")]
      	public byte Flag { get; set; }
   
        [Display(Name = "Count")]
      	public short Count { get; set; }
   
        [Display(Name = "Average")]
      	public double Average { get; set; }
   
        [Display(Name = "Sigma")]
      	public double Sigma { get; set; }
   
        [Display(Name = "Cv")]
      	public double Cv { get; set; }
   
        [Display(Name = "Uniformity")]
      	public short Uniformity { get; set; }
   
        [Display(Name = "Gain")]
      	public Nullable<double> Gain { get; set; }
   
        [Key, Editable(false)]
        [Display(Name = "Statistic")]
      	public int StatisticId { get; set; }
   
        public ICollection<Histogram> Histograms { get; set; }
   
        [Required(ErrorMessage=null, ErrorMessageResourceName ="ManualResultRequired", ErrorMessageResourceType = typeof(DataModel.Properties.Resources))]
        public ManualResult ManualResult { get; set; }
   
   	}
   }
}
