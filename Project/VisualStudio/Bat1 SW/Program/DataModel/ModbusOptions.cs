//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
namespace DataModel
{
   
   using System;
   using System.ComponentModel;
   using System.ComponentModel.DataAnnotations;
   using System.ComponentModel.DataAnnotations.Schema;
   
   public partial class ModbusOptions
   {
      static ModbusOptions()
      {
         TypeDescriptor.AddProviderTransparent(
            new AssociatedMetadataTypeTypeDescriptionProvider(
               typeof(ModbusOptions), typeof(ModbusOptionsMetadata)), typeof(ModbusOptions));
      }
      public virtual Nullable<byte> Address { get; set; }
      public virtual Nullable<int> BaudRate { get; set; }
      public virtual Nullable<byte> DataBits { get; set; }
      public virtual Nullable<byte> DeviceAddress { get; set; }
      public virtual Nullable<byte> Mode { get; set; }
      public virtual Nullable<byte> Parity { get; set; }
      public virtual Nullable<short> ReplyDelay { get; set; }
   }
}
