﻿using System.Data.Common;
using Database;
using DataContext;

namespace Bat1.Database {
    public class DatabaseTableCurvePoints : DatabaseTable {

        // Tabulka s definici bodu v jednotlivych rustovych krivkach.

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseTableCurvePoints(DatabaseFactory factory)
            : base("CurvePoints", factory) {
        }
        
        /// <summary>
        /// Create new table
        /// </summary>
        public void Create() {
            Create("CREATE TABLE " + tableName + " ("
                 + "CurveId "   + factory.TypeLong      + " NOT NULL,"  // ID krivky, do ktere bod patri
                 + "DayNumber " + factory.TypeSmallInt  + ","           // Cislo dne
                 + "Weight "    + factory.TypeFloat     + ","           // Hmotnost
                 + "CONSTRAINT FK_P_Curves FOREIGN KEY (CurveId) REFERENCES Curves(CurveId)"
                 + ")");
        }

        /// <summary>
        /// Add points of new curve
        /// </summary>
        /// <param name="curve">Curve with valid ID</param>
        public void Add(Curve curve) {
            // Ulozim body nove krivky
            if (curve.CurvePoints.Count == 0) {
                return;     // Krivka neobsahuje zadne body
            }
            using (var command = factory.CreateCommand("INSERT INTO " + tableName + " VALUES (@CurveId, "
                                                           + "@DayNumber, @Weight)")) {
                foreach (var point in curve.CurvePoints) {
                    command.Parameters.Clear();
                    command.Parameters.Add(factory.CreateParameter("@CurveId",          curve.Id));
                    command.Parameters.Add(factory.CreateParameter("@DayNumber", (short)point.Day));
                    command.Parameters.Add(factory.CreateParameter("@Weight",    point.Weight));
                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Delete points of a specified curve
        /// </summary>
        /// <param name="weighingId">Curve Id</param>
        /// <returns>Number of rows affected</returns>
        public int Delete(long curveId) {
            using (var command = factory.CreateCommand("DELETE FROM " + tableName + " WHERE CurveId = @CurveId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@CurveId", curveId));
                return command.ExecuteNonQuery();
            }
        }

        private CurvePoint ReaderToCurvePoint(DbDataReader reader) {
            var point = new CurvePoint();
            point.Day    = (short) reader["DayNumber"];
            point.Weight = (float) reader["Weight"];
            return point;
        }
        
        /// <summary>
        /// Load curve points from database
        /// </summary>
        /// <param name="curve">Curve to load the points to</param>
        public void Load(ref Curve curve) {
            using (var command = factory.CreateCommand("SELECT * FROM " + tableName + " WHERE CurveId = @CurveId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@CurveId", curve.Id));
                using (var reader = command.ExecuteReader()) {
                    // Vyplnim seznam bodu v zadane krivce
                    curve.CurvePoints.Clear();
                    while (reader.Read()) {
                        curve.CurvePoints.Add(ReaderToCurvePoint(reader));
                    }
                }
            }
        }

        /// <summary>
        /// Load curve list from database
        /// </summary>
        /// <param name="curveList">Curve list to load the points to</param>
        public void LoadList(ref CurveList curveList) {
            for (var i = 0; i < curveList.List.Count; i++) {
                var curve = curveList.List[i];
                Load(ref curve);
            }
        }

    
    }
}
