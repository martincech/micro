﻿using System;
using System.Collections.Generic;
using Database;
using DataContext;

namespace Bat1.Database {
    public class DatabaseTableGroups : DatabaseTable {

        // Tabulka se skupinami definovanymi v ramci globalniho configu. Definice souboru ve skupinach je
        // v tabulce GroupFiles.
    
        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseTableGroups(DatabaseFactory factory)
            : base("Groups", factory) {
        }
        
        /// <summary>
        /// Create new table
        /// </summary>
        public void Create() {
            Create("CREATE TABLE " + tableName + " ("
                 + "GroupId "            + factory.TypeLong + " PRIMARY KEY AUTOINCREMENT,"
                 + "ScaleConfigId "      + factory.TypeLong     + " NOT NULL,"      // Nastaveni, ke kteremu skupina patri
                 + "Name "               + factory.TypeNVarChar + "(15) NOT NULL,"
                 + "Note "               + factory.TypeNVarChar + "(15),"           // Poznamka ve vaze, tj. pouze 15 znaku
                 + "CONSTRAINT FK_P_ScaleConfigs FOREIGN KEY (ScaleConfigId) REFERENCES ScaleConfigs(ScaleConfigId)"
                 + ")");
        }

        /// <summary>
        /// Add new group
        /// </summary>
        /// <param name="name">Group name</param>
        /// <param name="note">Group note</param>
        /// <param name="scaleConfigId">ScaleConfig Id this group belongs to</param>
        /// <returns>Id of new group</returns>
        public long Add(string name, string note, long scaleConfigId) {
            // Jmeno musi byt zadane
            if (name == "") {
                throw new Exception("Group name cannot be empty");
            }
            
            // Ulozim novou skupinu
            using (var command = factory.CreateCommand("INSERT INTO " + tableName + " VALUES (NULL, @ScaleConfigId, "
                                                           + "@Name, @Note); SELECT last_insert_rowid()")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@ScaleConfigId", scaleConfigId));
                command.Parameters.Add(factory.CreateParameter("@Name",          name));
                command.Parameters.Add(factory.CreateParameter("@Note",          note));
                return (long)command.ExecuteScalar();
            }
        }

        /// <summary>
        /// Delete all groups that belong to specified config
        /// </summary>
        /// <param name="scaleConfigId">Scale config ID</param>
        /// <returns>Number of rows affected</returns>
        public int Delete(long scaleConfigId) {
            using (var command = factory.CreateCommand("DELETE FROM " + tableName + " WHERE ScaleConfigId = @ScaleConfigId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@ScaleConfigId", scaleConfigId));
                return command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Load names and notes of groups that belong to specified config
        /// </summary>
        /// <param name="scaleConfigId">ScaleConfig Id the groups belong to</param>
        /// <param name="idList">List of group IDs corresponding to the returned group list</param>
        /// <returns>Group list</returns>
        public FileGroupList Load(long scaleConfigId, out List<long> idList) {
            using (var command = factory.CreateCommand("SELECT GroupId, Name, Note FROM " + tableName + " WHERE ScaleConfigId = @ScaleConfigId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@ScaleConfigId", scaleConfigId));
                using (var reader = command.ExecuteReader()) {
                    var groupList = new FileGroupList();
                    idList = new List<long>();
                    while (reader.Read()) {
                        groupList.Add((string)reader["Name"], (string)reader["Note"]);
                        idList.Add((long)reader["GroupId"]);          // Pridam do seznamu ID
                    }
                    return groupList;
                }
            }
        }

        /// <summary>
        /// Load list of group IDs that belong to specified config
        /// </summary>
        /// <param name="scaleConfigId">Scale config ID</param>
        /// <returns>List of group IDs</returns>
        public List<long> LoadIds(long scaleConfigId) {
            using (var command = factory.CreateCommand("SELECT GroupId FROM " + tableName + " WHERE ScaleConfigId = @ScaleConfigId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@ScaleConfigId", scaleConfigId));
                using (var reader = command.ExecuteReader()) {
                    var idList = new List<long>();
                    while (reader.Read()) {
                        idList.Add((long)reader["GroupId"]);
                    }
                    return idList;
                }
            }
        }


    }
}
