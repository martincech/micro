﻿using System;
using Bat2Library.Connection.Interface.Contract;

namespace Bat1.Services
{
   internal class Bat2ConnectionEventsContractMessage
   {

      public Bat2ConnectionEventsContractMessage(Action<IBat2ConnectionEventsContract> actionToInvoke)
      {
         ActionToInvoke = actionToInvoke;
      }
      public Action<IBat2ConnectionEventsContract> ActionToInvoke { get; private set; }
   }
}
