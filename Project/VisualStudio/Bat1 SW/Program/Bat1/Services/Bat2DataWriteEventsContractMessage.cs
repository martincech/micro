﻿using System;
using Bat2Library.Connection.Interface.Contract;

namespace Bat1.Services
{
   internal class Bat2DataWriteEventsContractMessage
   {

      public Bat2DataWriteEventsContractMessage(Action<IBat2DataWriteEventsContract> actionToInvoke)
      {
         ActionToInvoke = actionToInvoke;
      }
      public Action<IBat2DataWriteEventsContract> ActionToInvoke { get; private set; }
   }
}
