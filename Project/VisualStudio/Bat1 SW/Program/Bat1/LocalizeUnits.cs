﻿using BatLibrary;

namespace Bat1
{
   public static class LocalizeUnits
   {
      public static string GetUnits(Units units)
      {
         switch (units)
         {
            case Units.KG:
               return Properties.Resources.KG;
            case Units.G:
               return Properties.Resources.G;
            case Units.LB:
               return Properties.Resources.LB;
            default:
               return "";
         }
      }
   }
}
