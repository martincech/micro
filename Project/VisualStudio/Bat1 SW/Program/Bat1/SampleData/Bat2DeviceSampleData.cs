﻿using System;
using System.Collections.Generic;
using Bat2Library.Connection.Interface.Domain;

namespace Bat1.SampleData
{
   public class Bat2DeviceSampleData
   {
      public Bat2DeviceData Bat2DeviceData { get; set; }

      public Bat2DeviceSampleData()
      {      
         Bat2DeviceData = new Bat2DeviceData();
         Bat2DeviceData.Configuration.VersionInfo.Modification = 0;
         Bat2DeviceData.Configuration.VersionInfo.SerialNumber = 123;
         Bat2DeviceData.Configuration.VersionInfo.SoftwareBuild = 0;
         Bat2DeviceData.Configuration.VersionInfo.SoftwareMajor = 1;
         Bat2DeviceData.Configuration.VersionInfo.SoftwareMinor = 0;
         Bat2DeviceData.Configuration.VersionInfo.Class = 2;
         Bat2DeviceData.Configuration.VersionInfo.HardwareBuild = 0;
         Bat2DeviceData.Configuration.VersionInfo.HardwareMajor = 1;
         Bat2DeviceData.Configuration.VersionInfo.HardwareMinor = 0;
         Bat2DeviceData.Configuration.DeviceInfo.Name = "Bat2Sample";

         Bat2DeviceData.Configuration.WeighingConfiguration = new WeighingConfiguration
         {
            UniformityRange = 10,
            HistogramRange = 10,
            HistogramStep = 10
         };

         var archiveItem1 = new ArchiveItem
         {
            Timestamp = new DateTime(2012, 12, 12, 12, 12, 59),
            Count = 30,
            Average = 470,
            Sigma = 1,
            Uniformity = 1000,
            Gain = 9,
            Histogram = new List<byte> { 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 3, 5, 10, 8, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
         };

         var archiveItem2 = new ArchiveItem
         {
            Timestamp = new DateTime(2013, 3, 13, 3, 15, 0),
            Count = 30,
            Average = 250,
            Sigma = 35,
            Uniformity = 985,
            Gain = 11,
            Histogram = new List<byte> { 0, 0, 0, 1, 2, 5, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 2, 7, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 }
         };

         Bat2DeviceData.Archive = new List<ArchiveItem> { archiveItem1, archiveItem2 };       
      }
   }
}
