Petr Good Morning!...... do you have an idea why the data is not separated in to individual columns in some of these data reports?

Thanks


Best Regards, 

Dwain Johnson

Farm Weigh Systems
4100 Whispering Pines Dr.
Marshville, NC 28103

T: 704.753.4300  F: 704.753.4338

-----Original Message-----
From: Garritty, Dr. Chip <Chip.Garritty@merial.com>
To: Dwain Johnson (FarmWeigh@aol.com) <FarmWeigh@aol.com>
Sent: Fri, Jun 24, 2011 8:53 pm
Subject: FW: Live weight data
Dwain,
What would cause all the data to download into one column (see spreadsheets (6-13-11 thru 6-17-11) ?  The previous weeks data did not have this problem (6-6-11 thru 6-10-11).  
 
- Chip Garritty, DVM, MAM, Diplomate ACPV
   cell (479) 567-0324
   fax  (678) 638-8619
