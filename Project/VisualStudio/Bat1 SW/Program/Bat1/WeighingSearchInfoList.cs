using System.Collections.Generic;
using DataContext;

namespace Bat1
{
   public class WeighingSearchInfoList {
      /// <summary>
      /// List of weighing info
      /// </summary>
      public  List<WeighingSearchInfo> WeighingInfoList { get { return weighingInfoList; } }
      private List<WeighingSearchInfo> weighingInfoList;

      /// <summary>
      /// Constructor
      /// </summary>
      public WeighingSearchInfoList() {
         weighingInfoList = new List<WeighingSearchInfo>();
      }

      /// <summary>
      /// Compare function for List(WeighingSearchInfo).Sort()
      /// </summary>
      /// <param name="x">First parameter</param>
      /// <param name="y">Second parameter</param>
      /// <returns>1, -1 or 0</returns>
      private int CompareByTime(WeighingSearchInfo x, WeighingSearchInfo y) {
         if (x.MinDateTime < y.MinDateTime) {
            return -1;
         }
         if (x.MinDateTime > y.MinDateTime) {
            return 1;
         }
            
         // Datumy jsou shodne, porovnam soubory
         var result = x.FileName.CompareTo(y.FileName);
         if (result < 0) {
            return -1;
         }
         if (result > 0) {
            return 1;
         }
            
         // Soubory jsou taky shodne, porovnam jeste vahy
         result = x.ScaleName.CompareTo(y.ScaleName);
         if (result < 0) {
            return -1;
         }
         if (result > 0) {
            return 1;
         }

         // Vse je shodne
         return 0;
      }

      /// <summary>
      /// Add weighing info into the list
      /// </summary>
      /// <param name="weighingInfo">Weighing info to add</param>
      public void Add(WeighingSearchInfo weighingInfo) {
         if (weighingInfoList.Exists(delegate(WeighingSearchInfo i) { return i.Id == weighingInfo.Id; })) {
            // Uz v seznamu je
            return;
         }
         // Pridam a setridim seznam
         weighingInfoList.Add(weighingInfo);
      }

      /// <summary>
      /// Sort the list by time
      /// </summary>
      public void Sort() {
         weighingInfoList.Sort(CompareByTime);
      }

      /// <summary>
      /// Delete item at index
      /// </summary>
      /// <param name="index">Index</param>
      public void Delete(int index) {
         weighingInfoList.RemoveAt(index);
      }

      /// <summary>
      /// Delete all items
      /// </summary>
      public void Clear() {
         weighingInfoList.Clear();
      }
   }
}