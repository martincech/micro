﻿using System;
using System.Windows.Forms;
using Bat1.Properties;

namespace Bat1 {

   /// <summary>
    /// Check entered values
    /// </summary>
    static class CheckValue {
      public static void SelectControl(Control control) {
            control.Focus();
            if (control.GetType().ToString().EndsWith("TextBox")) {
                var textBox = (TextBox)control;
                textBox.SelectAll();
            } else if (control.GetType().ToString().EndsWith("ComboBox")) {
                var comboBox = (ComboBox)control;
                comboBox.SelectAll();
            }
        }

        /// <summary>
        /// Show invalid value message and focus the control
        /// </summary>
        /// <param name="control">Control to focus or null</param>
      public static void InvalidValueMessage(Control control, string fieldName, bool minMax, double min, double max)
      {
            MessageBox.Show(Resources.VALUE_NOT_VALID + " : " + fieldName.ToLower().Replace(":",""), Program.ApplicationName);
            if (minMax)
            {
                ToolTip tt = new ToolTip();
                tt.Show(min + " - " + max, control, 50, -20, 7000);
            }
            if (control == null) {
                return;
            }
            SelectControl(control);
        }

        /// <summary>
        /// Get scale name from a control (combobox, textbox)
        /// </summary>
        /// <param name="control">Control with entered text</param>
        /// <param name="name">Returned text</param>
        /// <returns>True if successful</returns>
        public static bool GetScaleName(Control control,string fieldName, out string name) {
            if (!DataContext.CheckValue.CheckScaleName(control.Text)) {
                InvalidValueMessage(control, fieldName,false,0,0);
                name = "";
                return false;
            }
            name = control.Text;
            return true;
        }
    
        /// <summary>
        /// Get scale text from a control (combobox, textbox)
        /// </summary>
        /// <param name="control">Control with entered text</param>
        /// <param name="text">Returned text</param>
        /// <returns>True if successful</returns>
        public static bool GetScaleText(Control control, string fieldName, out string text)
        {
           if (!DataContext.CheckValue.CheckScaleText(control.Text))
           {
                InvalidValueMessage(control, fieldName, false, 0, 0);
                text = "";
                return false;
            }
            text = control.Text;
            return true;
        }

        public static bool GetInt(Control control, string fieldName, out int i, int minValue, int maxValue)
        {
           if (!Int32.TryParse(control.Text, out i)) {
                InvalidValueMessage(control, fieldName, true, minValue, maxValue);
                return false;
            }
            if (i < minValue || i > maxValue) {
                InvalidValueMessage(control, fieldName, true, minValue, maxValue);
                return false;
            }
            return true;
        }

        public static bool GetDouble(Control control, string fieldName, out double d, double minValue, double maxValue) {
           if (!Double.TryParse(control.Text, out d)) {
                InvalidValueMessage(control,fieldName, true, minValue, maxValue);
                return false;
            }
            if (d < minValue || d > maxValue) {
                InvalidValueMessage(control, fieldName, true, minValue, maxValue);
                return false;
            }
            return true;
        }

    }
}
