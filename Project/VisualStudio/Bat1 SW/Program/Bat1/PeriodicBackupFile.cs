using System;

namespace Bat1
{
   /// <summary>
   /// Information about one periodic backup file
   /// </summary>
   public struct PeriodicBackupFile {
      /// <summary>
      /// File name
      /// </summary>
      public string Name;

      /// <summary>
      /// Date and time when the backup was created
      /// </summary>
      public DateTime Created;
   }
}