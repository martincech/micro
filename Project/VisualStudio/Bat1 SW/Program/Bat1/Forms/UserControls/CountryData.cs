using Bat1Library;

namespace Bat1
{
   struct CountryData {
      public string             Title;
      public Country            Country;
      public Language           Language;
      public DateFormat         DateFormat;
      public char               DateSeparator1;
      public char               DateSeparator2;
      public TimeFormat         TimeFormat;
      public char               TimeSeparator;
      public DaylightSavingMode DaylightSavingMode;

      public CountryData(string title, Country country, Language language, DateFormat dateFormat, char dateSeparator1,
         char dateSeparator2, TimeFormat timeFormat, char timeSeparator, DaylightSavingMode daylightSavingMode) {
         Title              = title;
         Country            = country;
         Language           = language;
         DateFormat         = dateFormat;
         DateSeparator1     = dateSeparator1;
         DateSeparator2     = dateSeparator2;
         TimeFormat         = timeFormat;
         TimeSeparator      = timeSeparator;
         DaylightSavingMode = daylightSavingMode;
         }
   };
}