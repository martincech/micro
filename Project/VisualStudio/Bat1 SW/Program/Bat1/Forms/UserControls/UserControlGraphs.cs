﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Bat1.Properties;
using Bat1.Utilities;
using Bat1Library;
using BatLibrary;
using DataContext;
using Graphic;
using ZedGraph;
using CurveList = DataContext.CurveList;

namespace Bat1 {
   public partial class UserControlGraphs : UserControl {
        
        // Posledni prumernou hmotnost si musim pamatovat pro kazdou osu Y zvlast. Pokud ne a pokud by vybral
        // Gain na obou osach, vypocet by bych chybny.
        private float lastLeftAverage;
        private float lastRightAverage;
        
        /// <summary>
        /// Flock list to draw
        /// </summary>
        public List<Flock> flockList;

        /// <summary>
        /// Flag to draw
        /// </summary>
        public Flag flag;

        /// <summary>
        /// List of basic colors
        /// </summary>
        private readonly List<Color> basicColorList;

        /// <summary>
        /// List of colors used for individual flocks
        /// </summary>
        private List<Color> flockColorList;

        /// <summary>
        /// Color of a standard growth curve
        /// </summary>
        private Color growthCurveColor = Color.Black;

        /// <summary>
        /// List of standard growth curves used in the flocks
        /// </summary>
        private CurveList curveList;

        /// <summary>
        /// Units used
        /// </summary>
        private Units units;

        public ZedGraphControl Graph { get { return userControlTimeGraph.Graph; } }

        public UserControlGraphs() {
            InitializeComponent();

            // Vytvorim seznam zakladnich barev, ze kterych se odvozuji barvy krivek
            basicColorList = new List<Color>();
            basicColorList.Add(Color.FromArgb(0xee, 0x2e, 0x2f));
            basicColorList.Add(Color.FromArgb(0x00, 0x8c, 0x48));
            basicColorList.Add(Color.FromArgb(0x30, 0x30, 0xc0));
            basicColorList.Add(Color.FromArgb(0xf4, 0x7d, 0x23));
            basicColorList.Add(Color.FromArgb(0x66, 0x2c, 0x91));
            basicColorList.Add(Color.FromArgb(0xe7, 0xc8, 0x00));
            basicColorList.Add(Color.FromArgb(0xa2, 0x1d, 0x21));
            basicColorList.Add(Color.FromArgb(0xe1, 0x7d, 0xb2));
            basicColorList.Add(Color.FromArgb(0x52, 0x52, 0x52));
            basicColorList.Add(Color.FromArgb(0x00, 0x85, 0x8c));
            basicColorList.Add(Color.FromArgb(0x82, 0x8c, 0x00));
            basicColorList.Add(Color.FromArgb(0x9f, 0x70, 0x1c));

            // Naplnim combobox leve osy a default vyberu rustovou krivku
            comboBoxLeftAxisType.Items.Add(Resources.GROWTH_CURVE);
            AddCommonCurveTypes(comboBoxLeftAxisType);
            SetY1ComboBoxIndex(GraphType.GROWTH_CURVE);

            // Naplnim combobox prave osy a default nevyberu nic
            comboBoxRightAxisType.Items.Add(Resources.RIGHT_AXIS + "...");
            AddCommonCurveTypes(comboBoxRightAxisType);
            SetY2ComboBoxIndex(GraphType.NONE);
            
            // Zaregistruju eventy vstupu a odchodu mysi z grafu, kvuli zobrazeni helpu k ovladani grafu
            userControlTimeGraph.Graph.MouseEnter += new EventHandler(GraphMouseEnter);
            userControlTimeGraph.Graph.MouseLeave += new EventHandler(GraphMouseLeave);
        }

        /// <summary>
        /// Add common curve types for both left and right axis
        /// </summary>
        /// <param name="comboBox"></param>
        private void AddCommonCurveTypes(ComboBox comboBox) {
            comboBox.Items.Add(Resources.GAIN);
            comboBox.Items.Add(Resources.COUNT);
            comboBox.Items.Add(Resources.STANDARD_DEVIATION);
            comboBox.Items.Add(Resources.CV);
            comboBox.Items.Add(Resources.UNIFORMITY);
            comboBox.Items.Add(Resources.SPEED);
        }
        
        private void GraphMouseEnter(object sender, EventArgs e) {
            pictureBoxMouseInfo.Image = Resources.GraphHelpActive;
        }

        private void GraphMouseLeave(object sender, EventArgs e) {
            pictureBoxMouseInfo.Image = Resources.GraphHelpInactive;
        }

        /// <summary>
        /// Set graph so it can be printed
        /// </summary>
        public void SetForPrinting(string title) {
            userControlTimeGraph.SetForPrinting(title);
        }

        /// <summary>
        /// Set graph to show on a screen
        /// </summary>
        public void SetForScreen() {
            userControlTimeGraph.SetForScreen();
        }

        private double GetValue(StatisticResult result, GraphType graphType, bool isY2Axis) {
            switch (graphType) {
                case GraphType.GROWTH_CURVE:
                    return result.Average;

                case GraphType.GAIN:
                    // Prirustek musim pocitat zde znovu, nemuzu vyuzit jiz vypocteneho gainList z UserControlStatistics
                    double value;
                    double lastAverage = isY2Axis ? lastRightAverage : lastLeftAverage;
                    if (lastAverage == float.MinValue) {
                        value = 0;      // Prvni bod v krivce ma nulovy prirustek
                    } else {
                        value = result.Average - lastAverage;
                    }
                    if (isY2Axis) {
                        lastRightAverage = result.Average;
                    } else {
                        lastLeftAverage  = result.Average;
                    }
                    lastAverage = result.Average;
                    return value;

                case GraphType.COUNT:
                    return result.Count;

                case GraphType.SIGMA:
                    return result.Sigma;

                case GraphType.CV:
                    return result.Cv;

                case GraphType.UNIFORMITY:
                    return result.Uniformity;

                case GraphType.SPEED:
                    return result.WeighingSpeed;

                default:    // NONE
                    return 0;
            }
        }
        
        private void CreateColors(int curveCount) {
            flockColorList = new List<Color>();
            
            // Zkopiruju zakladni barvy
            flockColorList.AddRange(basicColorList);

            if (curveCount <= basicColorList.Count) {
                // Staci zakladni barvy
                return;
            }

            // Pozadovanych barev je vic, dalsi barvy musim odvodit ze zakladnich barev

            // Vytvorim si seznam se zakladnimi barvami ve formatu HSL, abych mohl jednoduse ubirat sytost
            var basicColorHslList = new List<ColorRgb>();
            foreach (var color in basicColorList) {
                basicColorHslList.Add(new ColorRgb(color));
            }

            var missingSets = curveCount / basicColorList.Count;    // Pocet dalsich setu barev (1 set = 1 sestava zakladnich barev)
            if (curveCount % basicColorList.Count == 0) {
                missingSets--;      // Korekce, 24/12 = 2, ale chybi jen 1 set
            }
            var lightnessStep = 0.5 / missingSets;       // Celkem zvysim jas o 50%, ktere rozdelim mezi vsechny sety
            var saturationStep = 0.4 / missingSets;      // Celkem zvysim saturaci o 40%, ktere rozdelim mezi vsechny sety

            // Vytvorim dalsi sety
            for (var i = 0; i < missingSets; i++) {
                foreach (var color in basicColorList) {
                    double h, s, l;
                    ColorRgb.Rgb2Hsl(new ColorRgb(color), out h, out s, out l);
                    var oldS = s;
                    
                    // Zvysim jas
                    if (oldS == 0) {
                        // Sedou zesvetluju vic
                        l *= 1.0 + (i + 1) * 4.0 * lightnessStep;
                    } else {
                        // Barevne odstiny pomaleji
                        l *= 1.0 + (i + 1) * lightnessStep;
                    }
                    if (l > 0.9) {
                        l = 0.9;        // Aby to nebyla bila
                    }

                    // Snizim saturaci
                    s *= 1.0 - (i + 1) * saturationStep;
                    if (oldS > 0.0 && s < 0.1) {
                        s = 0.1;        // Aby to nebyla uplne seda (puvodni sedou ponecham)
                    }

                    flockColorList.Add(ColorRgb.Hsl2Rgb(h, s, l));
                }
            }
        }

        private Color GetFlockColor(int flockIndex) {
            if (flockIndex >= flockColorList.Count) {
                return flockColorList[flockColorList.Count - 1];    // Natvrdo posledni
            }
            return flockColorList[flockIndex];
        }
        
        private string GetYAxisTitle(GraphType graphType) {
            var unitsString = " [" + units.ToLocalizedString() + "]";

            switch (graphType) {
                case GraphType.GROWTH_CURVE:
                    // Growth curve v comboboxu nahradim hmotnosti
                    return Resources.WEIGHT + unitsString;

                case GraphType.GAIN:
                    return Resources.GAIN + unitsString;

                case GraphType.COUNT:
                    return Resources.COUNT;

                case GraphType.SIGMA:
                    return Resources.STANDARD_DEVIATION + unitsString;
                    
                case GraphType.CV:
                    return Resources.CV + " [%]";
            
                case GraphType.UNIFORMITY:
                    return Resources.UNIFORMITY + " [%]";

                case GraphType.SPEED:
                    return Resources.SPEED + " [1/" + Resources.HOUR + "]";

                default:    // NONE
                    return "";
            }
        }
        
        private void SetYAxisTitle(GraphType graphType, bool isY2Axis) {
            var title = GetYAxisTitle(graphType);

            if (isY2Axis) {
                userControlTimeGraph.SetAxisY2(title);
            } else {
                userControlTimeGraph.SetAxisY1(title);
            }
        }
        
        private GraphType GetY1GraphType() {
            return (GraphType)(comboBoxLeftAxisType.SelectedIndex + 1);     // NONE na leve ose neni
        }

        private void SetY1ComboBoxIndex(GraphType graphType) {
            comboBoxLeftAxisType.SelectedIndex = (int)graphType - 1;        // NONE na leve ose neni
        }

        private GraphType GetY2GraphType() {
            if (comboBoxRightAxisType.SelectedIndex == 0) {
                return GraphType.NONE;
            }
            return (GraphType)(comboBoxRightAxisType.SelectedIndex + 1);    // GROWTH_CURVE na leve ose neni
        }

        private void SetY2ComboBoxIndex(GraphType graphType) {
            if (graphType == GraphType.NONE) {
                comboBoxRightAxisType.SelectedIndex = 0;
                return;
            }
            comboBoxRightAxisType.SelectedIndex = (int)graphType + 1;       // GROWTH_CURVE na prave ose neni
        }
        
        private void RedrawLines() {
            // Zobrazim casove prubehy
            userControlTimeGraph.ClearCurves();

            if (flockList == null) {
                return;
            }
            
            // Zjistim vybrane veliciny na leve ose Y
            var y1GraphType = GetY1GraphType();

            // Zjistim vybrane veliciny na prave ose Y
            var y2GraphType = GetY2GraphType();

            // Podle poctu krivek vytvorim barevnou paletu
            CreateColors(flockList.Count);

            // Vykreslim krivky
            var flockIndex = 0;
            foreach (var flock in flockList) {
                var pointListY1 = new List<DayDoublePoint>();
                var pointListY2 = new List<DayDoublePoint>();
                lastLeftAverage  = float.MinValue;        // Starting value
                lastRightAverage = float.MinValue;        // Starting value
                foreach (var weighing in flock.WeighingList.List) {
                    var result = weighing.WeighingResults.GetResult(flag);
                    if (result == null) {
                        continue;       // Zobrazeny flag v tomto vazeni neni
                    }
                    
                    // Leva osa
                    DayDoublePoint pointY1;
                    pointY1.day   = flock.DateToDay(weighing.GetMinDateTime(flag));
                    pointY1.value = GetValue(result, y1GraphType, false);
                    pointListY1.Add(pointY1);
                    
                    // Prava osa
                    DayDoublePoint pointY2;
                    pointY2.day   = pointY1.day;    // Den shodny s prubehem na leve ose
                    pointY2.value = GetValue(result, y2GraphType, true);
                    pointListY2.Add(pointY2);
                }
                userControlTimeGraph.AddCurve(flock.Definition.Name, pointListY1, GetFlockColor(flockIndex), false);
                if (y2GraphType == GraphType.NONE) {
                    userControlTimeGraph.Graph.GraphPane.Y2Axis.IsVisible = false;  // Schovam osu
                } else {
                    userControlTimeGraph.Graph.GraphPane.Y2Axis.IsVisible = true;   // Zobrazim osu
                    userControlTimeGraph.AddCurve(flock.Definition.Name, pointListY2, GetFlockColor(flockIndex), true);
                }
                flockIndex++;
            }

            // Teoreticka rustova krivka - jen pokud je na leve ose zobrazena rustova krivka a pokud je nejaka
            // teoreticka krivka vybrana
            if (GetY1GraphType() == GraphType.GROWTH_CURVE && comboBoxCurves.SelectedIndex > 0) {
                var curve = curveList.List[comboBoxCurves.SelectedIndex - 1];
                var pointList = new List<DayDoublePoint>();
                foreach (var curvePoint in curve.CurvePoints) {
                    DayDoublePoint point;
                    point.day   = curvePoint.Day;
                    point.value = curvePoint.Weight;
                    pointList.Add(point);
                }
                userControlTimeGraph.AddCurve(curve.Name, pointList, growthCurveColor, false);
            }

            // Popis osy Y podle vybraneho typu grafu
            SetYAxisTitle(y1GraphType, false);
            SetYAxisTitle(y2GraphType, true);
            
            // Odzoomuju a prekreslim
            userControlTimeGraph.FinishAddingCurves();
        }

        private void ShowCurveList(bool isVisible) {
            comboBoxCurves.Visible = isVisible;
        }
        
        private void RedrawCurveList() {
            ShowCurveList(true);
            comboBoxCurves.Items.Clear();
            comboBoxCurves.Items.Add(Resources.STANDARD_GROWTH_CURVE + "...");

            if (curveList.List.Count == 0) {
                // Nepouziva rustove krivky, skryju seznam
                ShowCurveList(false);
                return;
            }
            
            // Pridam dalsi krivky
            foreach (var curve in curveList.List) {
                comboBoxCurves.Items.Add(curve.Name);
            }
        }

        private void AddCurve(Curve curve) {
            if (curve == null) {
                return;     // Krivka neni definovana
            }
            if (curveList.Exists(curve.Name)) {
                return;     // Krivka uz v seznamu je
            }
            curveList.Add(curve);
        }
        
        /// <summary>
        /// Read units from first weighing inthe flock list
        /// </summary>
        /// <param name="flockList">Flock list</param>
        /// <returns>Units</returns>
        private Units GetUnits(List<Flock> flockList) {
            foreach (var flock in flockList) {
                foreach (var weighing in flock.WeighingList.List) {
                    return weighing.WeighingData.ScaleConfig.Units.Units;
                }
            }
            return Units.KG;       // Pokud nenajdu, vratim default Kg
        }
        
        /// <summary>
        /// Set graph data and redraw
        /// </summary>
        /// <param name="flockList">Flock list</param>
        /// <param name="flag">Flag to draw</param>
        public void SetData(List<Flock> flockList, Flag flag) {
            // Preberu data
            this.flockList = flockList;
            this.flag      = flag;

            // Vytvorim seznam teoretickych rustovych krivek (v grafu muze zobrazit jen jednu vybranou).
            // Do seznamu vlozim jen rustove krivky, ktere odpovidaji vybranemu flagu
            curveList = new CurveList();
            foreach (var flock in flockList) {
                switch (flag) {
                    case Flag.ALL:
                        // Pokud vazi samce a samice a vybral vsechny flagy, dam na vyber obe krivky
                        AddCurve(flock.Definition.CurveDefault);
                        AddCurve(flock.Definition.CurveFemales);
                        break;

                    case Flag.FEMALE:
                        // Samice maji specialni krivku
                        AddCurve(flock.Definition.CurveFemales);
                        break;

                    default:
                        // Samci nebo vse ostatni pouzivaji krivku default
                        AddCurve(flock.Definition.CurveDefault);
                        break;
                }
            }
            
            // Jednotky nactu z prvniho vazeni (jsou ve vsech vazenich stejne)
            units = GetUnits(flockList);
            
            // Typy grafu nemenim, ponecham ty, ktere uz ma vybrane. Po dobu behu programu se tak typ
            // grafu pamatuje. Pouze po spusteni programu se nastavi na default.

            // Prekreslim seznam krivek a pokusim se ponechat dosud vybranou
            var selectedCurve = comboBoxCurves.Text;     // Zapamatuju si dosavadni vybranou krivku
            RedrawCurveList();
            if (comboBoxCurves.Items.Contains(selectedCurve)) {
                // Nova hejna obsahuji dosud vybranou teoretickou krivku => vyberu ji opet
                comboBoxCurves.SelectedIndex = comboBoxCurves.Items.IndexOf(selectedCurve);
            } else {
                // Nova hejna dosud vybranou krivku neobsahuji, nevyberu zadnou
                comboBoxCurves.SelectedIndex = 0;
            }

            // Pokud nema vybranou rustovou krivku, schovam seznam teoretickych krivek
            if (GetY1GraphType() != GraphType.GROWTH_CURVE) {
                ShowCurveList(false);
            }

            // Prekreslim prubehy
            RedrawLines();
        }

        private void comboBoxLeftType_SelectionChangeCommitted(object sender, EventArgs e) {
            if (curveList == null) {
                return;
            }
            
            // Vyber teoretickych rustovych krivek zobrazim jen pokud vybral rustovou krivku a pokud
            // je v seznamu nejaka teoreticka krivka
            ShowCurveList(GetY1GraphType() == GraphType.GROWTH_CURVE && curveList.List.Count > 0);

            RedrawLines();
        }

        private void comboBoxCurves_SelectionChangeCommitted(object sender, EventArgs e) {
            RedrawLines();
        }


    }
}
