﻿using System;
using System.Windows.Forms;
using DataContext;
using Desktop.Wpf.Presentation;

namespace Bat1 {
   public partial class UserControlScaleConfigPower : UserControlScaleConfigBase, IConfigPower
   {

        public UserControlScaleConfigPower(Bat1Scale scaleConfig, bool readOnly) {
            InitializeComponent();

            // Preberu Read-only
            this.readOnly = readOnly;
            Enabled = !readOnly;

            // Preberu config
            SetScaleConfig(scaleConfig);
        }

        public override void Redraw() {
            isLoading = true;

            try {
                numericUpDownPeriod.Value = scaleConfig.PowerOffTimeout / 60;   // Je ulozeno v sekundach
            } finally {
                isLoading = false;
            }
        }

        private void IntegerNumberKeyPress(object sender, KeyPressEventArgs e) {
            // Pustim jen cislice
            if (!KeyFilter.IsInt(e.KeyChar)) {
                e.Handled = true;
            }
        }

        private void ControlsToConfig() {
            if (readOnly || isLoading) {
                return;
            }
            scaleConfig.PowerOffTimeout = (int)numericUpDownPeriod.Value * 60;      // Prepocitam na sekundy
        }

        private void numericUpDownPeriod_ValueChanged(object sender, EventArgs e) {
            ControlsToConfig();
        }


        #region Implementation of IView

        /// <summary>
        /// Gets or sets the data context of the view.
        /// This will be set to ViewModel of the view.
        /// </summary>
        public object DataContext { get; set; }

        #endregion
   }

    public interface IConfigPower : IView
    {
    }
}
