﻿using System;
using System.Windows;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;

namespace Bat1
{
   /// <summary>
   /// Interaction logic for App.xaml
   /// </summary>
   public partial class App : Application
   {
      protected override void OnStartup(StartupEventArgs e)
      {
         base.OnStartup(e);    
         AppDomain.CurrentDomain.UnhandledException += HandlerMethod;

         // Show Main window       
         MainWindow = Program.Main();
         //Debug.Assert(MainWindow != null);
          if (MainWindow == null)
          {
             Environment.Exit(1);
          }
          MainWindow.Show();
      }

      private void HandlerMethod(object sender, UnhandledExceptionEventArgs e)
      {      
         var ex = (Exception)e.ExceptionObject;
         MessageBox.Show(ex.Message + ex.StackTrace,
            "Fatal Error", MessageBoxButton.OK, MessageBoxImage.Stop);
      }
   }
}
