﻿using System.Windows.Forms;
using DataContext;

namespace Bat1 {
    /// <summary>
    /// UserControlScaleConfig base class
    /// </summary>
    public class UserControlScaleConfigBase : UserControl {
        /// <summary>
        /// Scale config to edit
        /// </summary>
        protected Bat1Scale scaleConfig;      

        /// <summary>
        /// Read-only mode
        /// </summary>
        protected bool readOnly;

        /// <summary>
        /// Values are being loaded into the controls
        /// </summary>
        protected bool isLoading = false;

        /// <summary>
        /// Set new config
        /// </summary>
        /// <param name="scaleConfig">Scale config to edit</param>
        public virtual void SetScaleConfig(Bat1Scale scaleConfig) 
        {
            // Preberu config
            this.scaleConfig = scaleConfig;

            // Prekreslim
            Redraw();
        }      

        /// <summary>
        /// Redraw control
        /// </summary>
        public virtual void Redraw() {
        }

        private void InitializeComponent()
        {
         this.SuspendLayout();
         // 
         // UserControlScaleConfigBase
         // 
         this.Name = "UserControlScaleConfigBase";
         this.ResumeLayout(false);

        }

    }
}
