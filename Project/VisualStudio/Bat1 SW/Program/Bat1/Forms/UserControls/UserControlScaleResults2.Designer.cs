namespace Bat1 {
    partial class UserControlScaleResults2 {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
         this.components = new System.ComponentModel.Container();
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlScaleResults2));
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
         this.saveFileDialogCsv = new System.Windows.Forms.SaveFileDialog();
         this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
         this.buttonExportSelected = new System.Windows.Forms.Button();
         this.buttonExportAll = new System.Windows.Forms.Button();
         this.buttonSaveSelected = new System.Windows.Forms.Button();
         this.buttonSaveAll = new System.Windows.Forms.Button();
         this.comboBoxUnits = new System.Windows.Forms.ComboBox();
         this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
         this.customControlHistogramDataGridViewWeighings = new Bat1.CustomControlHistogramDataGridView();
         this.ColumnDateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnSex = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnAverage = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnUniformity = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnSigma = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnGain = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnTargetWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnTargetWeightFemale = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnHistogram = new System.Windows.Forms.DataGridViewImageColumn();
         this.ColumnIsSaved = new System.Windows.Forms.DataGridViewCheckBoxColumn();
         this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
         this.buttonSaveUnsaved = new System.Windows.Forms.Button();
         this.comboBoxScaleNames = new System.Windows.Forms.ComboBox();
         this.labelUnits = new System.Windows.Forms.Label();
         this.label1 = new System.Windows.Forms.Label();
         this.label2 = new System.Windows.Forms.Label();
         this.labelConnected = new System.Windows.Forms.Label();
         this.ColumnFile = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.tableLayoutPanel.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.customControlHistogramDataGridViewWeighings)).BeginInit();
         this.flowLayoutPanel1.SuspendLayout();
         this.SuspendLayout();
         // 
         // saveFileDialogCsv
         // 
         this.saveFileDialogCsv.DefaultExt = "csv";
         // 
         // buttonExportSelected
         // 
         resources.ApplyResources(this.buttonExportSelected, "buttonExportSelected");
         this.buttonExportSelected.FlatAppearance.BorderSize = 0;
         this.buttonExportSelected.Name = "buttonExportSelected";
         this.buttonExportSelected.TabStop = false;
         this.toolTip1.SetToolTip(this.buttonExportSelected, resources.GetString("buttonExportSelected.ToolTip"));
         this.buttonExportSelected.UseVisualStyleBackColor = true;
         this.buttonExportSelected.Click += new System.EventHandler(this.buttonExportSelected_Click);
         // 
         // buttonExportAll
         // 
         resources.ApplyResources(this.buttonExportAll, "buttonExportAll");
         this.buttonExportAll.Name = "buttonExportAll";
         this.toolTip1.SetToolTip(this.buttonExportAll, resources.GetString("buttonExportAll.ToolTip"));
         this.buttonExportAll.UseVisualStyleBackColor = true;
         this.buttonExportAll.Click += new System.EventHandler(this.buttonExportAll_Click);
         // 
         // buttonSaveSelected
         // 
         resources.ApplyResources(this.buttonSaveSelected, "buttonSaveSelected");
         this.buttonSaveSelected.FlatAppearance.BorderSize = 0;
         this.buttonSaveSelected.Name = "buttonSaveSelected";
         this.buttonSaveSelected.TabStop = false;
         this.toolTip1.SetToolTip(this.buttonSaveSelected, resources.GetString("buttonSaveSelected.ToolTip"));
         this.buttonSaveSelected.UseVisualStyleBackColor = true;
         this.buttonSaveSelected.Click += new System.EventHandler(this.buttonSaveSelected_Click);
         // 
         // buttonSaveAll
         // 
         resources.ApplyResources(this.buttonSaveAll, "buttonSaveAll");
         this.buttonSaveAll.Name = "buttonSaveAll";
         this.toolTip1.SetToolTip(this.buttonSaveAll, resources.GetString("buttonSaveAll.ToolTip"));
         this.buttonSaveAll.UseVisualStyleBackColor = true;
         this.buttonSaveAll.Click += new System.EventHandler(this.buttonSaveAll_Click);
         // 
         // comboBoxUnits
         // 
         resources.ApplyResources(this.comboBoxUnits, "comboBoxUnits");
         this.comboBoxUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.comboBoxUnits.FormattingEnabled = true;
         this.comboBoxUnits.Items.AddRange(new object[] {
            resources.GetString("comboBoxUnits.Items"),
            resources.GetString("comboBoxUnits.Items1"),
            resources.GetString("comboBoxUnits.Items2")});
         this.comboBoxUnits.Name = "comboBoxUnits";
         this.toolTip1.SetToolTip(this.comboBoxUnits, resources.GetString("comboBoxUnits.ToolTip"));
         this.comboBoxUnits.SelectedIndexChanged += new System.EventHandler(this.comboBoxUnits_SelectedIndexChanged);
         this.comboBoxUnits.SelectionChangeCommitted += new System.EventHandler(this.comboBoxUnits_SelectionChangeCommitted);
         // 
         // tableLayoutPanel
         // 
         resources.ApplyResources(this.tableLayoutPanel, "tableLayoutPanel");
         this.tableLayoutPanel.Controls.Add(this.customControlHistogramDataGridViewWeighings, 0, 2);
         this.tableLayoutPanel.Controls.Add(this.flowLayoutPanel1, 0, 3);
         this.tableLayoutPanel.Controls.Add(this.comboBoxScaleNames, 1, 0);
         this.tableLayoutPanel.Controls.Add(this.labelUnits, 3, 0);
         this.tableLayoutPanel.Controls.Add(this.comboBoxUnits, 4, 0);
         this.tableLayoutPanel.Controls.Add(this.label1, 0, 0);
         this.tableLayoutPanel.Controls.Add(this.label2, 0, 1);
         this.tableLayoutPanel.Controls.Add(this.labelConnected, 2, 0);
         this.tableLayoutPanel.Name = "tableLayoutPanel";
         this.tableLayoutPanel.VisibleChanged += new System.EventHandler(this.tableLayoutPanel_VisibleChanged);
         // 
         // customControlHistogramDataGridViewWeighings
         // 
         this.customControlHistogramDataGridViewWeighings.AllowUserToAddRows = false;
         this.customControlHistogramDataGridViewWeighings.AllowUserToDeleteRows = false;
         this.customControlHistogramDataGridViewWeighings.AllowUserToResizeRows = false;
         this.customControlHistogramDataGridViewWeighings.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
         this.customControlHistogramDataGridViewWeighings.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
         this.customControlHistogramDataGridViewWeighings.BackgroundColor = System.Drawing.SystemColors.Window;
         this.customControlHistogramDataGridViewWeighings.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.customControlHistogramDataGridViewWeighings.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
         dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
         dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
         dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
         dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
         dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
         dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
         dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
         this.customControlHistogramDataGridViewWeighings.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
         this.customControlHistogramDataGridViewWeighings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.customControlHistogramDataGridViewWeighings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnDateTime,
            this.ColumnCount,
            this.ColumnSex,
            this.ColumnAverage,
            this.ColumnUniformity,
            this.ColumnSigma,
            this.ColumnGain,
            this.ColumnTargetWeight,
            this.ColumnTargetWeightFemale,
            this.ColumnHistogram,
            this.ColumnIsSaved});
         this.tableLayoutPanel.SetColumnSpan(this.customControlHistogramDataGridViewWeighings, 5);
         resources.ApplyResources(this.customControlHistogramDataGridViewWeighings, "customControlHistogramDataGridViewWeighings");
         this.customControlHistogramDataGridViewWeighings.Name = "customControlHistogramDataGridViewWeighings";
         this.customControlHistogramDataGridViewWeighings.ReadOnly = true;
         this.customControlHistogramDataGridViewWeighings.RowHeadersVisible = false;
         this.customControlHistogramDataGridViewWeighings.RowTemplate.Height = 18;
         this.customControlHistogramDataGridViewWeighings.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
         // 
         // ColumnDateTime
         // 
         this.ColumnDateTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         resources.ApplyResources(this.ColumnDateTime, "ColumnDateTime");
         this.ColumnDateTime.Name = "ColumnDateTime";
         this.ColumnDateTime.ReadOnly = true;
         this.ColumnDateTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnCount
         // 
         this.ColumnCount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
         this.ColumnCount.DefaultCellStyle = dataGridViewCellStyle2;
         resources.ApplyResources(this.ColumnCount, "ColumnCount");
         this.ColumnCount.Name = "ColumnCount";
         this.ColumnCount.ReadOnly = true;
         this.ColumnCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnSex
         // 
         this.ColumnSex.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
         this.ColumnSex.DefaultCellStyle = dataGridViewCellStyle3;
         resources.ApplyResources(this.ColumnSex, "ColumnSex");
         this.ColumnSex.Name = "ColumnSex";
         this.ColumnSex.ReadOnly = true;
         this.ColumnSex.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnAverage
         // 
         this.ColumnAverage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
         this.ColumnAverage.DefaultCellStyle = dataGridViewCellStyle4;
         resources.ApplyResources(this.ColumnAverage, "ColumnAverage");
         this.ColumnAverage.Name = "ColumnAverage";
         this.ColumnAverage.ReadOnly = true;
         this.ColumnAverage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnUniformity
         // 
         this.ColumnUniformity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
         this.ColumnUniformity.DefaultCellStyle = dataGridViewCellStyle5;
         resources.ApplyResources(this.ColumnUniformity, "ColumnUniformity");
         this.ColumnUniformity.Name = "ColumnUniformity";
         this.ColumnUniformity.ReadOnly = true;
         this.ColumnUniformity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnSigma
         // 
         this.ColumnSigma.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
         this.ColumnSigma.DefaultCellStyle = dataGridViewCellStyle6;
         resources.ApplyResources(this.ColumnSigma, "ColumnSigma");
         this.ColumnSigma.Name = "ColumnSigma";
         this.ColumnSigma.ReadOnly = true;
         this.ColumnSigma.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnGain
         // 
         this.ColumnGain.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
         this.ColumnGain.DefaultCellStyle = dataGridViewCellStyle7;
         resources.ApplyResources(this.ColumnGain, "ColumnGain");
         this.ColumnGain.Name = "ColumnGain";
         this.ColumnGain.ReadOnly = true;
         this.ColumnGain.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnTargetWeight
         // 
         this.ColumnTargetWeight.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
         this.ColumnTargetWeight.DefaultCellStyle = dataGridViewCellStyle8;
         resources.ApplyResources(this.ColumnTargetWeight, "ColumnTargetWeight");
         this.ColumnTargetWeight.Name = "ColumnTargetWeight";
         this.ColumnTargetWeight.ReadOnly = true;
         this.ColumnTargetWeight.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnTargetWeightFemale
         // 
         this.ColumnTargetWeightFemale.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
         this.ColumnTargetWeightFemale.DefaultCellStyle = dataGridViewCellStyle9;
         resources.ApplyResources(this.ColumnTargetWeightFemale, "ColumnTargetWeightFemale");
         this.ColumnTargetWeightFemale.Name = "ColumnTargetWeightFemale";
         this.ColumnTargetWeightFemale.ReadOnly = true;
         this.ColumnTargetWeightFemale.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnHistogram
         // 
         this.ColumnHistogram.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
         dataGridViewCellStyle10.NullValue = ((object)(resources.GetObject("dataGridViewCellStyle10.NullValue")));
         this.ColumnHistogram.DefaultCellStyle = dataGridViewCellStyle10;
         resources.ApplyResources(this.ColumnHistogram, "ColumnHistogram");
         this.ColumnHistogram.Name = "ColumnHistogram";
         this.ColumnHistogram.ReadOnly = true;
         // 
         // ColumnIsSaved
         // 
         this.ColumnIsSaved.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         resources.ApplyResources(this.ColumnIsSaved, "ColumnIsSaved");
         this.ColumnIsSaved.Name = "ColumnIsSaved";
         this.ColumnIsSaved.ReadOnly = true;
         this.ColumnIsSaved.Resizable = System.Windows.Forms.DataGridViewTriState.True;
         // 
         // flowLayoutPanel1
         // 
         resources.ApplyResources(this.flowLayoutPanel1, "flowLayoutPanel1");
         this.tableLayoutPanel.SetColumnSpan(this.flowLayoutPanel1, 5);
         this.flowLayoutPanel1.Controls.Add(this.buttonExportSelected);
         this.flowLayoutPanel1.Controls.Add(this.buttonExportAll);
         this.flowLayoutPanel1.Controls.Add(this.buttonSaveUnsaved);
         this.flowLayoutPanel1.Controls.Add(this.buttonSaveSelected);
         this.flowLayoutPanel1.Controls.Add(this.buttonSaveAll);
         this.flowLayoutPanel1.Name = "flowLayoutPanel1";
         // 
         // buttonSaveUnsaved
         // 
         resources.ApplyResources(this.buttonSaveUnsaved, "buttonSaveUnsaved");
         this.buttonSaveUnsaved.Name = "buttonSaveUnsaved";
         this.buttonSaveUnsaved.UseVisualStyleBackColor = true;
         this.buttonSaveUnsaved.Click += new System.EventHandler(this.buttonSaveUnsaved_Click);
         // 
         // comboBoxScaleNames
         // 
         resources.ApplyResources(this.comboBoxScaleNames, "comboBoxScaleNames");
         this.comboBoxScaleNames.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.comboBoxScaleNames.FormattingEnabled = true;
         this.comboBoxScaleNames.Items.AddRange(new object[] {
            global::Bat1.Properties.Resources.SELECT_SCALES_NONE});
         this.comboBoxScaleNames.Name = "comboBoxScaleNames";
         this.comboBoxScaleNames.SelectedIndexChanged += new System.EventHandler(this.comboBoxScaleNames_SelectedIndexChanged);
         // 
         // labelUnits
         // 
         resources.ApplyResources(this.labelUnits, "labelUnits");
         this.labelUnits.Name = "labelUnits";
         // 
         // label1
         // 
         resources.ApplyResources(this.label1, "label1");
         this.label1.Name = "label1";
         // 
         // label2
         // 
         resources.ApplyResources(this.label2, "label2");
         this.tableLayoutPanel.SetColumnSpan(this.label2, 5);
         this.label2.Name = "label2";
         // 
         // labelConnected
         // 
         resources.ApplyResources(this.labelConnected, "labelConnected");
         this.labelConnected.Name = "labelConnected";
         // 
         // ColumnFile
         // 
         this.ColumnFile.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         resources.ApplyResources(this.ColumnFile, "ColumnFile");
         this.ColumnFile.Name = "ColumnFile";
         this.ColumnFile.ReadOnly = true;
         this.ColumnFile.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // UserControlScaleResults2
         // 
         resources.ApplyResources(this, "$this");
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.Controls.Add(this.tableLayoutPanel);
         this.Name = "UserControlScaleResults2";
         this.Load += new System.EventHandler(this.UserControlScaleResults2_Load);
         this.tableLayoutPanel.ResumeLayout(false);
         this.tableLayoutPanel.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.customControlHistogramDataGridViewWeighings)).EndInit();
         this.flowLayoutPanel1.ResumeLayout(false);
         this.flowLayoutPanel1.PerformLayout();
         this.ResumeLayout(false);

        }

        #endregion

        private CustomControlHistogramDataGridView customControlHistogramDataGridViewWeighings;
        private System.Windows.Forms.Button buttonExportSelected;
        private System.Windows.Forms.Button buttonSaveSelected;
        private System.Windows.Forms.SaveFileDialog saveFileDialogCsv;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button buttonSaveAll;
        private System.Windows.Forms.Button buttonExportAll;
        private System.Windows.Forms.ComboBox comboBoxScaleNames;
        private System.Windows.Forms.Label labelUnits;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFile;
        private System.Windows.Forms.Label labelConnected;
        private System.Windows.Forms.Button buttonSaveUnsaved;
        private System.Windows.Forms.ComboBox comboBoxUnits;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSex;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAverage;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnUniformity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSigma;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnGain;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTargetWeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTargetWeightFemale;
        private System.Windows.Forms.DataGridViewImageColumn ColumnHistogram;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColumnIsSaved;

    }
}
