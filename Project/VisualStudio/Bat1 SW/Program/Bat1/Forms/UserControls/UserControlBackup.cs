﻿using System;
using System.IO;
using System.Windows.Forms;
using Bat1.Database.Backup;
using Bat1.Properties;

namespace Bat1 {
    public partial class UserControlBackup : UserControl {
        public UserControlBackup() {
            InitializeComponent();
        }

        private void buttonBackup_Click(object sender, EventArgs e) {
            // Zeptam se na umisteni zalohy
            // Zeptam se na jmeno souboru
            var saveFileDialog = new SaveFileDialog();
            saveFileDialog.DefaultExt = FileExtension.BACKUP;
            saveFileDialog.Filter     = String.Format(Resources.FILE_FORMAT_BACKUP, Program.ApplicationName) + " (*." + FileExtension.BACKUP + ")|*." + FileExtension.BACKUP;
            if (saveFileDialog.ShowDialog() != DialogResult.OK) {
                return;
            }

            // Nastavim spravnou priponu
            var fileName = Path.ChangeExtension(saveFileDialog.FileName, FileExtension.BACKUP);
            
            // Zalohuju
            Refresh();
            Cursor.Current = Cursors.WaitCursor;
            try {
                Backup.BackupWithCompaction(fileName);
                Cursor.Current = Cursors.Default;
                MessageBox.Show(Resources.BACKUP_SUCCESSFUL, Program.ApplicationName);
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void buttonRestore_Click(object sender, EventArgs e) {
            new FormRestoreBackup().ShowDialog();
        }
    }
}
