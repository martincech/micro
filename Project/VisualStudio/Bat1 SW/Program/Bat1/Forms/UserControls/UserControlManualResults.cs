﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Bat1.Properties;
using Bat1Library;
using BatLibrary;
using DataContext;

namespace Bat1 
{
    public partial class UserControlManualResults : UserControlResultsBase 
    {
        enum WeighingType 
        {
            NORMAL,
            MIXED_SEX,
            SORTING_2,
            SORTING_3
        }

        private Weighing editWeighing = null;   

        public UserControlManualResults() {
            InitializeComponent();        

            if (LicenseManager.UsageMode == LicenseUsageMode.Designtime) {
                return;         // V designeru z databaze nenahravam
            }

            // Predam combobox s vahami, soubor musim obsluhovat zvlast
            SetScaleComboBox(comboBoxScaleName);

            // Zacinam plnit data do controlu
            isLoading = true;

            try {
                // Nactu seznamy vah a souboru z databaze, zaroven se vyplni combobox vah
                LoadLists();

                // Vyplnim combobox souboru
                LoadFileComboBox();

                // Default zvolim normalni vazeni
                comboBoxFlag.SelectedIndex = (int)WeighingType.NORMAL;
                SetWeighingType(WeighingType.NORMAL);
                
                // Nahraju z databaze naposledy pouzite jednotky - staci jednou pri startu programu
                LoadUnits();
            } finally {
                isLoading = false;
            }
        }

        public bool Save() {
            // Vaha
            string scaleName;
            if (!CheckValue.GetScaleName(comboBoxScaleName,label6.Text, out scaleName)) {
                return false;
            }

            // Soubor
            string name;
            if (!CheckValue.GetScaleName(comboBoxFileName,label9.Text, out name))
            {
                return false;
            }
            var file = new NameNote(name, textBoxFileNote.Text);
            
            // Start vazeni
            var startDateTime = new DateTime(dateTimePickerStartDate.Value.Year, dateTimePickerStartDate.Value.Month, dateTimePickerStartDate.Value.Day,
                                                  dateTimePickerStartTime.Value.Hour, dateTimePickerStartTime.Value.Minute, dateTimePickerStartTime.Value.Second);            

            // Jednotky
            var units = (Units)comboBoxUnits.SelectedIndex;
            
            // Statistika
            List<StatisticResult> resultList;
            if (!GetResults(out resultList, units)) {
                return false;
            }

            if (editWeighing == null) {
                // Zkontroluju, zda uz vazeni v databazi neexistuje (kombinace soubor - start vazeni)
                if (CanSaveWeighing(file.Name, startDateTime) != DialogResult.Yes) {
                    return true;     // Vazeni uz existuje a nechce ho prepsat
                }
            } else {
                // Pokud edituje vazeni, nejprve smazu z databaze puvodni vazeni
                Program.Database.DeleteWeighing(editWeighing.WeighingData.Id);
            }

           var type = ScaleType.SCALE_BAT1;
           var serialNumber = -1;
           double histogramStep = 0;

           if (editWeighing != null)
           {
              type = editWeighing.ScaleType;
              histogramStep = editWeighing.WeighingData.ScaleConfig.StatisticConfig.Histogram.Step;
              if (ScaleType.SCALE_BAT2 == type)
              {
                 var config = editWeighing.WeighingData.ScaleConfig as Bat2Scale;
                 if (config != null)
                 {
                    serialNumber = config.SerialNumber;
                 }
              }
           
              // Copy histograms from existing weighing
              foreach (var result in resultList)
              {   // check appropriate flag (if None, takes histogram from All)
                 var flag = result.Flag;
                 if (flag == Flag.NONE)
                 {
                    flag = Flag.ALL;
                 }

                 var stat = editWeighing.WeighingResults.StatisticResultList.FirstOrDefault(r => r.Flag == flag);
                 if (stat != null)
                 {
                    result.Histogram = stat.Histogram;
                 }
              }
           }         
         
           // Ulozim vazeni do databaze
           Program.Database.SaveWeighing(scaleName, file, startDateTime, units, resultList, null, histogramStep, type, serialNumber);

            // Ulozim do databaze posledne pouzite jednotky
            SaveUnits();

            // Pokud zadal novou vahu, musim ji pridat do seznamu
            AddScale(scaleName);

            // Pokud zadal novy soubor, musim ho pridat do seznamu
            AddFile(file);

            // Novy seznam souboru musim nahrat rucne
            var oldText = comboBoxFileName.Text;
            LoadFileComboBox();
            comboBoxFileName.Text = oldText;           // Obnovim vybranou polozku

            // Zobrazim info, ze se ulozilo (pouze pokud zaklada nove vazeni, tj. ne pri editaci)
            if (editWeighing == null) {
                MessageBox.Show(Resources.WEIGHING_SAVED, Program.ApplicationName);
            }

            return true;
        }

        public void LoadWeighing(Weighing weighing) 
        {
            // Ulozim si
            editWeighing = weighing;

            // Vaha
            comboBoxScaleName.Text = weighing.WeighingData.ScaleConfig.ScaleName;

            // Soubor
            comboBoxFileName.Text = weighing.WeighingData.File.Name;
            textBoxFileNote.Text  = weighing.WeighingData.File.Note;
            
            // Start vazeni
            dateTimePickerStartDate.Value = weighing.GetMinDateTime().Date;
            dateTimePickerStartTime.Value = weighing.GetMinDateTime();          

            // Jednotky
            var units = weighing.WeighingData.ScaleConfig.Units.Units;
            comboBoxUnits.SelectedIndex = (int)units;        
            SetUserControlWeighingResult(weighing, units);
        }

       /// <summary>
       /// Disable some controls that can't be change in specific mode
       /// </summary>
       public void DisableControls()
       {
          comboBoxScaleName.Enabled = false;
          comboBoxFlag.Enabled = false;
          userControlWeighingResultNormal.SetReadOnly();         
       }

        private void SetUserControlWeighingResult(Weighing weighing, Units newUnits)
        {
           // Statistika podle flagu, ktere vazeni obsahuje
           if (weighing.WeighingResults.FlagList.Contains(Flag.FEMALE) && weighing.WeighingResults.FlagList.Contains(Flag.MALE))
           {
              // Manual by sex
              SetWeighingType(WeighingType.MIXED_SEX);
              comboBoxFlag.SelectedIndex = (int)WeighingType.MIXED_SEX;

              // Import file from Bat2Old can contain only females data
              var male = weighing.WeighingResults.StatisticResultList.FirstOrDefault(s => s.Flag == Flag.MALE);
              if (male != null)
              {
                 userControlWeighingResultMales.SetResult(weighing.WeighingResults.StatisticResultList[0], newUnits, weighing.WeighingData.Id);
               
              }
              else
              {  //set active tabpage on female
                 tabControlResults.SelectedTab = tabPageFemales;
              }

              userControlWeighingResultFemales.SetResult(weighing.WeighingResults.StatisticResultList[1], newUnits, weighing.WeighingData.Id);
           }
           else if (weighing.WeighingResults.FlagList.Contains(Flag.OK))
           {
              // Sorting into 3 groups
              SetWeighingType(WeighingType.SORTING_3);
              comboBoxFlag.SelectedIndex = (int)WeighingType.SORTING_3;
              userControlWeighingResultLight.SetResult(weighing.WeighingResults.StatisticResultList[0], newUnits, weighing.WeighingData.Id);
              userControlWeighingResultOk.SetResult(weighing.WeighingResults.StatisticResultList[1], newUnits, weighing.WeighingData.Id);
              userControlWeighingResultHeavy.SetResult(weighing.WeighingResults.StatisticResultList[2], newUnits, weighing.WeighingData.Id);
           }
           else if (weighing.WeighingResults.FlagList.Contains(Flag.LIGHT))
           {
              // Sorting into 2 groups
              SetWeighingType(WeighingType.SORTING_2);
              comboBoxFlag.SelectedIndex = (int)WeighingType.SORTING_2;
              userControlWeighingResultLight.SetResult(weighing.WeighingResults.StatisticResultList[0], newUnits, weighing.WeighingData.Id);
              userControlWeighingResultHeavy.SetResult(weighing.WeighingResults.StatisticResultList[1], newUnits, weighing.WeighingData.Id);
           }
           else
           {
              // Normal
              SetWeighingType(WeighingType.NORMAL);
              comboBoxFlag.SelectedIndex = (int)WeighingType.NORMAL;
              userControlWeighingResultNormal.SetResult(weighing.WeighingResults.StatisticResultList[0], newUnits, weighing.WeighingData.Id);
              tabPageResults.Text = weighing.WeighingResults.FlagList.Contains(Flag.FEMALE) ? tabPageFemales.Text : tabPageMales.Text;
           }


            if (weighing.ScaleType != ScaleType.SCALE_BAT1)
            {
                userControlWeighingResultNormal.ShowGain(true);
                userControlWeighingResultFemales.ShowGain(true);
                userControlWeighingResultMales.ShowGain(true);
                userControlWeighingResultLight.ShowGain(true);
                userControlWeighingResultOk.ShowGain(true);
                userControlWeighingResultHeavy.ShowGain(true);
            }
            else
            {
                userControlWeighingResultNormal.ShowGain(false);
                userControlWeighingResultFemales.ShowGain(false);
                userControlWeighingResultMales.ShowGain(false);
                userControlWeighingResultLight.ShowGain(false);
                userControlWeighingResultOk.ShowGain(false);
                userControlWeighingResultHeavy.ShowGain(false);
            }
        }


        private void SetWeighingType(WeighingType type) {
            tabControlResults.TabPages.Clear();
            switch (type) {
                case WeighingType.NORMAL:
                    tabControlResults.TabPages.Add(tabPageResults);
                    break;

                case WeighingType.MIXED_SEX:
                    tabControlResults.TabPages.Add(tabPageMales);
                    tabControlResults.TabPages.Add(tabPageFemales);
                    break;

                case WeighingType.SORTING_2:
                    tabControlResults.TabPages.Add(tabPageLight);
                    tabControlResults.TabPages.Add(tabPageHeavy);
                    break;

                case WeighingType.SORTING_3:
                    tabControlResults.TabPages.Add(tabPageLight);
                    tabControlResults.TabPages.Add(tabPageOk);
                    tabControlResults.TabPages.Add(tabPageHeavy);
                    break;
            }
        }

        /// <summary>
        /// Load last used units from database
        /// </summary>
        private void LoadUnits() {
            comboBoxUnits.SelectedIndex = (int)Program.Setup.ManualUnits;
        }

        /// <summary>
        /// Save selected units to database
        /// </summary>
        private void SaveUnits() {
            Program.Setup.ManualUnits = (Units)comboBoxUnits.SelectedIndex;
            Program.Database.SaveSetup();
        }

        private void LoadFileComboBox() {
            comboBoxFileName.Items.Clear();
            foreach (var file in fileList) {
                comboBoxFileName.Items.Add(file.Name);
            }
        }

        private bool AddResult(UserControlWeighingResult control, List<StatisticResult> resultList, Units units, Flag flag,
                               TabControl tabControl, TabPage tabPage) {
            StatisticResult result;
            if (!control.GetResult(units, flag, out result, tabControl, tabPage)) {
                return false;
            }
            resultList.Add(result);
            return true;
        }

        private bool GetResults(out List<StatisticResult> resultList, Units units) {
            resultList = new List<StatisticResult>();
            
            // Statistika podle vybraneho typu vazeni
            switch ((WeighingType)comboBoxFlag.SelectedIndex) {
                case WeighingType.NORMAL:
                    if (!AddResult(userControlWeighingResultNormal, resultList, units, Flag.NONE, tabControlResults, tabPageResults)) {
                        return false;
                    }
                    break;

                case WeighingType.MIXED_SEX:
                    if (!AddResult(userControlWeighingResultMales, resultList, units, Flag.MALE, tabControlResults, tabPageMales)) {
                        return false;
                    }
                    if (!AddResult(userControlWeighingResultFemales, resultList, units, Flag.FEMALE, tabControlResults, tabPageFemales)) {
                        return false;
                    }
                    break;

                case WeighingType.SORTING_2:
                    if (!AddResult(userControlWeighingResultLight, resultList, units, Flag.LIGHT, tabControlResults, tabPageLight)) {
                        return false;
                    }
                    if (!AddResult(userControlWeighingResultHeavy, resultList, units, Flag.HEAVY, tabControlResults, tabPageHeavy)) {
                        return false;
                    }
                    break;

                case WeighingType.SORTING_3:
                    if (!AddResult(userControlWeighingResultLight, resultList, units, Flag.LIGHT, tabControlResults, tabPageLight)) {
                        return false;
                    }
                    if (!AddResult(userControlWeighingResultOk, resultList, units, Flag.OK, tabControlResults, tabPageOk)) {
                        return false;
                    }
                    if (!AddResult(userControlWeighingResultHeavy, resultList, units, Flag.HEAVY, tabControlResults, tabPageHeavy)) {
                        return false;
                    }
                    break;
            }

            return true;
        }
        
        private void comboBoxScaleName_KeyPress(object sender, KeyPressEventArgs e) 
        {
            if (!KeyFilter.IsScaleChar(e.KeyChar)) 
            {
                e.Handled = true;
            }
        }

        private void comboBoxFileName_TextChanged(object sender, EventArgs e) {
            if (isLoading) {
                return;         // Data se nahravaji, nevykresluju
            }

            // Pokusim se nacist z databaze poznamku k prave zadanemu souboru
            textBoxFileNote.Text = fileList.GetNote(comboBoxFileName.Text);
        }

        private void comboBoxFlag_SelectionChangeCommitted(object sender, EventArgs e) {
            SetWeighingType((WeighingType)comboBoxFlag.SelectedIndex);
            comboBoxFlag.Focus();     // Vratim focus
        }

        private void comboBoxUnits_SelectionChangeCommitted(object sender, EventArgs e)
        {
           var actualUnits = (Units) comboBoxUnits.SelectedIndex;
           if (editWeighing != null)
           {
              SetUserControlWeighingResult(editWeighing, actualUnits);
           }          
        }
    }
}
