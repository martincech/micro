﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Bat1.Exporting;
using Bat1.Properties;
using Bat1.Utilities;
using Bat1Library;
using BatLibrary;
using DataContext;
using Desktop.WinForms;

namespace Bat1 {
    public partial class UserControlStatisticsTable : UserControl {
        /// <summary>
        /// List of displayed weighings
        /// </summary>
        public WeighingList WeighingList { get { return weighingList; } }
        private WeighingList weighingList;

        /// <summary>
        /// List of displayed flocks
        /// </summary>
        public List<Flock> FlockList { get { return flockList; } }
        private List<Flock> flockList;

        /// <summary>
        /// Displayed flag
        /// </summary>
        public Flag DisplayedFlag { get { return displayedFlag; } }
        private Flag displayedFlag = Flag.ALL;

        /// <summary>
        /// True when loading weighings from the database
        /// </summary>
        private bool isLoadingWeighings = false;

        /// <summary>
        /// Units that are used for all weighings
        /// </summary>
        private Units displayedUnits;

        public UserControlStatisticsTable() {
            InitializeComponent();

            // Zobrazim prazdny obsah
            ClearData();
        }

        /// <summary>
        /// Clear all statistic results
        /// </summary>
        public void ClearData() {
            // Smazu seznamy
            weighingList = new WeighingList();
            flockList    = null;

            // Smazu zobrazeny obsah
            DisplayWeighingList(false);                 // Tabulka vazeni
            ClearWeighingDetails();                     // Histogram a vzorky
            HideFlocks();                               // Hejna
        }

        public void RedrawStatistics(List<WeighingSearchInfo> weighingSearchInfoList) {
            Cursor.Current = Cursors.WaitCursor;
            try {
                weighingList = CreateWeighingList(weighingSearchInfoList);
                weighingList.Sort();        // Setridim podle data

                // Pokud je v seznamu vazeni pouzito vice jednotek, musim vsechny vazeni prepocist na jedny jednotky
                if (!CheckUnits()) {
                    weighingList.Clear();       // Smazu seznam vazeni
                    ClearWeighingDetails();
                    // Pokracuju dal v prekresleni
                }

                RedrawStatistics(weighingList);

                // Schovam hejna
                HideFlocks();
            } finally {
                isLoadingWeighings = false;
                Cursor.Current = Cursors.Default;
            }
        }

        private void RedrawStatistics(WeighingList weighingList) {
            // Pozor, vsechna vazeni uz musi byt prepoctena na 1 jednotky. Pouzite jednotky uz jsou ulozene
            // v displayedUnits

            // Vypoctu celkove statistiky
            weighingList.CalculateTotalStatistics();

            // Zacinam plnit data do controlu
            isLoadingWeighings = true;

            ClearWeighingDetails();

            // Default zobrazim vsechny flagy
            displayedFlag = Flag.ALL;
            
            // Zobrazim seznam vazeni
            DisplayWeighingList(true);

            // Skocim do tabulky s vazenimi
            dataGridWeighings.Focus();
            
            // Vykreslim histogram a vzorky vybraneho vazeni
            DisplaySelectedWeighing();
        }

        public void RedrawLastWeighings() {
            // Nactu vybrana vazeni do pameti, jsou uz setridena podle datumu, vahy a souboru
            RedrawStatistics(Program.Database.ReadLastWeighings());
        }

        public void RedrawCurrentFlocks() {
            // Default zobrazim vsechny flagy - musim rucne jiz zde, pred vypocem prirustku. Pokud by mel vybrany
            // najaky flag a neprepnul bych zde na vsechny flagy, prirustky se vypoctou pro vybrany flag, ale
            // po vypoctu se zobrazi vsechny flagy. Zde nastavim jen promennou displayedFlag, combobox az pozdeji,
            // protoze jeste neznam seznam flagu.
            displayedFlag = Flag.ALL;

            // Aktivni hejna vykresluju vzdy bez casoveho omezeni
            RedrawStatistics(Program.Database.LoadCurrentFlockList());
        }

        public void SetFlag(int flagComboBoxIndex) {
            // Nactu vybrany flag
            if (flagComboBoxIndex == 0) {
                displayedFlag = Flag.ALL;       // Na prvni pozici je text "Sex/Limit..."
            } else {
                displayedFlag = weighingList.FlagList[flagComboBoxIndex - 1];   
            }
            
            // Pokud zobrazuju hejna, musim prepocist cilove hmotnosti (male/female muzou mit jine krivky)
            if (flockList != null) {
                CalculateFlockData(flockList, displayedFlag);

                // Setridim seznam vazeni podle cisla dne (az po vypoctu dne, cilove hmotnosti a prirustku)
                weighingList.SortByFlockDay();
            }
            
            // Zapamatuju si prave vybrane vazeni (bude chtit asi porovanavat)
            var selectedIndex = dataGridWeighings.SelectedRows[0].Index;
            
            // Zobrazim seznam vazeni
            if (flockList != null) {
                DisplayWeighingList(true);
            } else {
                DisplayWeighingList(false);
            }

            // Vratim se na puvodne vybrany radek
            dataGridWeighings.Rows[selectedIndex].Selected = true;

            // Vykreslim histogram a vzorky vybraneho vazeni
            DisplaySelectedWeighing();
        }

        private void ShowFlockColumns(bool isVisible) {
            if (isVisible) {
                dataGridWeighings.Columns["ColumnFile"].HeaderText = Resources.Flock;
            } else {
                dataGridWeighings.Columns["ColumnFile"].HeaderText = Resources.FILE;
            }
            dataGridWeighings.Columns["ColumnDay"].Visible        = isVisible;
            dataGridWeighings.Columns["ColumnTarget"].Visible     = isVisible;
            dataGridWeighings.Columns["ColumnDifference"].Visible = isVisible;
            dataGridWeighings.Columns["ColumnAbove"].Visible      = isVisible;
            dataGridWeighings.Columns["ColumnBelow"].Visible      = isVisible;
           // dataGridWeighings.Columns["ColumnGain"].Visible       = isVisible;
        }

        private void HideFlocks() {
            flockList = null;

            // Zakazu menu hejn
            ShowFlockColumns(false);
        }
        
        private void ShowFlocks() {
            // Povolim menu hejn
            ShowFlockColumns(true);
        }

        private void AddWeighingToDataGrid(string started, string day, string fileName, string target, string difference,
                                           string above, string below, string gain, StatisticResult result, ResultType resultType) {
            // Pokud je result == null, vazeni neobsahuje zobrazeny flag
            
            // Vytvorim nahled histogramu
            Bitmap bitmap;
            if (result == null || result.Histogram == null) {
                bitmap = new Bitmap(1, 1);      // Vytvorim prazdnou bitmapu, jinak se v DataGridu zobrazuje krizek
            } else {
                bitmap = new Bitmap(HistogramMiniGraph.GetTotalWidth(result.Histogram.Counts.Length), dataGridWeighings.RowTemplate.Height - 2);
                HistogramMiniGraph.Draw(result.Histogram.Counts, Graphics.FromImage(bitmap), bitmap.Height - 1, 1, true);
            }

            // Pridam radek do tabulky
            if (result == null) 
            {
                dataGridWeighings.Rows.Add("",
                                           "",
                                           fileName,
                                           "",
                                           "",
                                           "",
                                           "",
                                           "",
                                           "",
                                           "",
                                           "",
                                           "",
                                           "",
                                           "",
                                           bitmap
                                           );
            } 
            else
            {
                var units = " " + LocalizeUnits.GetUnits(displayedUnits);
                var avg = ConvertWeight.Convert(result.Average, Units.G, displayedUnits);
                var sigma = ConvertWeight.Convert(result.Sigma, Units.G, displayedUnits);
                dataGridWeighings.Rows.Add(started,
                                           day,
                                           fileName,
                                           result.Count.ToString("N0"),
                                           DisplayFormat.RoundWeight(avg, displayedUnits) + units,
                                           target + units,
                                           difference,
                                           above,
                                           below,
                                           CheckGain(gain, displayedUnits),
                                           DisplayFormat.RoundWeight(sigma, displayedUnits) + units,
                                           DisplayFormat.Cv(result.Cv) + " %",
                                           DisplayFormat.Uniformity(Bat1Library.Statistics.Statistics.ConvertUniformity(result.Uniformity)) + " %",
                                           result.WeighingSpeed.ToString("N0"),
                                           bitmap
                                           );
            }
        }

       private string CheckGain(string gain, Units units)
       {
          if (gain.Equals("-") || gain.Equals(""))
          {
             return gain;
          }

          var unitString = " " + LocalizeUnits.GetUnits(displayedUnits);
          double g;
          if (Double.TryParse(gain, out g))
          {
             var convertGain = ConvertWeight.Convert(g, Units.G, units).ToString("g");
             return convertGain + unitString;
          }
          return "";
       }

       private int GetCurrentDay(DateTime from, int startedDay)
       {
          var span = DateTime.Now.Date - from.Date;
          var currentDay = startedDay + (int) span.TotalDays;
          return currentDay;
       }

        private void DisplayWeighingList(bool showFlocks) 
        {
            // Zobrazim seznam vazeni
            dataGridWeighings.Rows.Clear();
            var index = 0;
            foreach (var weighing in weighingList.List) 
            {
                // Vytvorim stringy pro extra hodnoty hejna
                string day = "", target = "", difference = "", above = "", below = "", gain = "";
                var name = weighing.GetMinDateTime(displayedFlag).ToString("g");
                if (showFlocks)
                {
                    day = weighing.FlockData.Day.ToString("N0");
                    target = DisplayFormat.RoundWeight(weighing.FlockData.Target, displayedUnits);
                    difference = DisplayFormat.RoundWeight(weighing.FlockData.Difference, displayedUnits);
                    above = weighing.FlockData.CountAbove.ToString("N0");
                    below = weighing.FlockData.CountBelow.ToString("N0");
                   gain = DisplayFormat.RoundWeight(weighing.FlockData.Gain, displayedUnits);
                }



                if (flockList != null)
                {
                    var flockFile = flockList.First(f => f.Definition.Name.Equals(weighing.WeighingData.File.Name));
                    name = flockFile.Definition.Started.ToString("g");
                    day = GetCurrentDay(flockFile.Definition.Started, flockFile.Definition.StartedDay).ToString();
                }
                else
                {
                    var result = weighing.WeighingResults.GetResult(displayedFlag);
                    if (result != null)
                    {

                        gain = weighing.WeighingResults.GetResult(displayedFlag).Gain != null
                            ? DisplayFormat.RoundWeight((double) weighing.WeighingResults.GetResult(displayedFlag).Gain,
                                displayedUnits)
                            : "-";
                    }
                }

                // Pridam data do tabulky                              
               AddWeighingToDataGrid(name,
                                     day,
                                     weighing.WeighingData.File.Name,
                                     target,
                                     difference,
                                     above,
                                     below,
                                     weighing.ScaleType == ScaleType.SCALE_BAT1 ? "-" : gain,
                                     weighing.WeighingResults.GetResult(displayedFlag),
                                     weighing.WeighingData.ResultType);
                index++;
            }
            
            // Celkove vysledky zobrazim jen pokud vybral vice vazeni
            if (weighingList.List.Count > 1) 
            {
                AddWeighingToDataGrid(Resources.TOTAL + " (" + weighingList.List.Count.ToString() + ")",
                                      "",
                                      "",
                                      "",
                                      "",
                                      "",
                                      "",
                                      "",
                                      weighingList.TotalstatisticResults.GetResult(displayedFlag),
                                      ResultType.MANUAL);       // Celkove vysledky vzdy zaokrouhluju
            }
        }
        
        private void DisplaySamples(WeighingData weighingData, Flag flag)
        {
           userControlSamples.SetData(weighingData == null ? null : weighingData.SampleList, flag, displayedUnits);
        }

       private void DisplayHistogram(WeighingResults weighingResults, WeighingData weighingData,
                                      Flag flag, int uniformityRange) 
       {
            // Nactu vysledky pro zadany flag
            var result = weighingResults.GetResult(flag);
            if (result == null) {
                // Tento flag ve vazeni neni
                userControlHistogram.ClearData();
                return;
            }

            // Vykreslim            
            userControlHistogram.SetData(result.Histogram, result.Average, uniformityRange, displayedUnits);
        }
        
        private void DisplaySelectedWeighing() {
            if (dataGridWeighings.Rows.Count == 0) {
                return;         // V tabulce neni zadne vazeni
            }

            if (dataGridWeighings.SelectedCells.Count == 0) {
                return;         // Neni nic vybrane, nemelo by nastat
            }

            // Zjistim vybrane vazeni
            var selectedIndex = dataGridWeighings.SelectedRows[0].Index;
            WeighingResults weighingResults;
            WeighingData weighingData;
            int uniformityRange;
            if (dataGridWeighings.Rows.Count == 1 || selectedIndex < dataGridWeighings.Rows.Count - 1) {
                // Vybral nektere vazeni (pokud je zobrazeno pouze 1 vazeni, radek TOTAL se nezobrazuje)
                weighingResults = weighingList.List[selectedIndex].WeighingResults;
                weighingData    = weighingList.List[selectedIndex].WeighingData;
                uniformityRange = weighingData.ScaleConfig.StatisticConfig.UniformityRange;
            } else {
                // Vybral celkem (posledni radek)
                weighingResults = weighingList.TotalstatisticResults;
                weighingData    = null;
                uniformityRange = weighingList.List[0].WeighingData.ScaleConfig.StatisticConfig.UniformityRange;
            }

            Cursor.Current = Cursors.WaitCursor;
            try 
            {
               if (weighingData == null || weighingData.SampleList == null)
               {  // Hide Samples tabPage if any sample doesn't exist
                  tabControlBottom.TabPages.Remove(tabPageSamples);
                  tabControlBottom.SelectedIndex = 0;
               }
               else if (!tabControlBottom.TabPages.Contains(tabPageSamples))
               {
                  tabControlBottom.TabPages.Add(tabPageSamples);
               }

               // Pro zrychleni vykresluju jen to, co je prave zobrazene
               switch (tabControlBottom.SelectedIndex) 
               {
                  case 0:
                     DisplayHistogram(weighingResults, weighingData, displayedFlag, uniformityRange);
                     break;
                  case 1:
                     DisplaySamples(weighingData, displayedFlag);
                     break;
               }
            } 
            finally 
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool CheckUnits() {
            // Pokud jsou v seznamu vazeni jen jedny jednotky, je vse v poradku
            if (weighingList.UnitsList.Count == 0) {
                return true;
            } 
            if (weighingList.UnitsList.Count == 1) {
                displayedUnits = weighingList.List[0].WeighingData.ScaleConfig.Units.Units;     // Ulozim si
                return true;
            }

            // V seznamu vazeni je pouzito vice jednotek, musim vsechna vazeni prepocist na jedny jednotky
            var form = new FormUnits(weighingList.UnitsList[0]);      // Default vyberu jednotky z 1. vazeni
            if (form.ShowDialog() != DialogResult.OK) {
                return false;
            }

            // Prepoctu vsechna vazeni
            weighingList.ChangeUnits(form.SelectedUnits);

            // Ulozim si pouzite jednotky
            displayedUnits = form.SelectedUnits;

            return true;
        }

        private void ClearWeighingDetails() {
            userControlHistogram.ClearData();
            DisplaySamples(null, displayedFlag);
        }

        private WeighingList CreateWeighingList(List<WeighingSearchInfo> weighingSearchInfoList) {
            var weighingList = new WeighingList();
            
            foreach (var searchinfo in weighingSearchInfoList) {
                weighingList.Add(Program.Database.LoadWeighing(searchinfo.Id));
            }

            return weighingList;
        }

        private WeighingList CreateWeighingList(List<Flock> flockList, Flag flag) {
            // Ze seznamu hejn vytvorim seznam jednotlivych vazeni
            var weighingList = new WeighingList();
            foreach (var flock in flockList) {
                foreach (var weighing in flock.WeighingList.List) {
                    weighingList.Add(weighing);
                }
            }
            return weighingList;
        }

        private void CalculateFlockData(List<Flock> flockList, Flag flag) {
            // U vsech vazeni v seznamu hejn vypoctu cislo dne, cilovou hmotnost a prirustek.
            // Musim delat zvlast, az mam zajisteno, ze vsechna vazeni maji nastavene stejne jednotky
            foreach (var flock in flockList) {
                // Teoreticke krivky prepoctu na zobrazene jednotky (jsou uz v displayedUnits)
                var curve = flock.Definition.GetCurve(flag);
                if (curve != null) {
                    curve.SetNewUnits(displayedUnits);
                }

                // Z jednotlivych vazeni v hejnu vypoctu potrebne udaje
                var lastAverage = float.MinValue;        // Pocatecni hodnota pro vypocet prirustku
                foreach (var weighing in flock.WeighingList.List) {
                    // Cislo dne v hejnu
                    var flockDay = flock.DateToDay(weighing.GetMinDateTime(flag));
                    weighing.FlockData.Day = flockDay;
                    
                    // Cilova hmotnost z teoreticke rustove krivky, uz ji mam nactenou a prepoctenou na spravne jednotky
                    if (curve == null) {
                        weighing.FlockData.Target = 0;
                    } else {
                        // Vypoctu rovnou zaokrouhlenou hodnotu v gramech
                        weighing.FlockData.Target = float.Parse(DisplayFormat.RoundWeight((float)curve.CalculateWeight(flockDay),
                                                                                          weighing.WeighingData.ScaleConfig.Units.Units));
                    }

                    // Rozdil prumeru od cilove hmotnosti a prirustek od minuleho vazeni
                    // Average i Target jsou uz zaokrouhlene na gramy, tj. nemusim ted nic zaokrouhlovat
                    var result = weighing.WeighingResults.GetResult(flag);
                    if (result == null) {
                        weighing.FlockData.Difference = 0;  // Vazeni tento flag neobsahuje
                        weighing.FlockData.Gain = 0;        
                    } else {
                        weighing.FlockData.Difference = result.Average - weighing.FlockData.Target;
                        if (lastAverage == float.MinValue) {
                            weighing.FlockData.Gain = 0;    // Prvni bod v krivce ma nulovy prirustek
                        } else {
                            weighing.FlockData.Gain = result.Average - lastAverage;
                        }
                        lastAverage = result.Average;
                    }

                    // Pocet vazeni nad a pod cilovou hmotnosti
                    weighing.FlockData.CountAbove = 0;
                    weighing.FlockData.CountBelow = 0;
                    if (weighing.WeighingData.SampleList != null && result != null) {
                        // U rucne zadaneho vazeni nejsou zadne vzorky (SampleList = NULL)
                        // Vazeni obsahuje tento flag (pokud neobsahuje, ponecham pocty nulove)
                        weighing.WeighingData.SampleList.First(flag);
                        while (weighing.WeighingData.SampleList.Read(flag)) {
                            if (weighing.WeighingData.SampleList.Sample.Weight >= weighing.FlockData.Target) {
                                weighing.FlockData.CountAbove++;
                            } else {
                                weighing.FlockData.CountBelow++;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Redraws statistics
        /// </summary>
        /// <param name="flockDefinitionList">List of flock definitions</param>
        /// <param name="fromDate">Additional time filter (or DateTime.MinValue when NA)</param>
        /// <param name="toDate">Additional time filter (or DateTime.MaxValue when NA)</param>
        public void RedrawStatistics(FlockDefinitionList flockDefinitionList, DateTime fromDate, DateTime toDate) {
            Cursor.Current = Cursors.WaitCursor;
            try {
                // Sestavim seznam hejn vcetne vysledku
                flockList = new List<Flock>();
                foreach (var flockDefinition in flockDefinitionList.List) {
                    flockList.Add(new Flock(flockDefinition, fromDate, toDate));
                }

                // Ze seznamu hejn vytvorim seznam jednotlivych vazeni. Seznam prozatim nebude setriden podle data,
                // ale podle hejn (to je treba pro vypocet hodnot hejna jako Day, Target, Difference a Gain).
                // Setrideni podle data provedu az na zaver.
                weighingList = CreateWeighingList(flockList, displayedFlag);

                // Pokud je v seznamu vazeni pouzito vice jednotek, musim vsechna vazeni prepocist na jedny jednotky
                if (!CheckUnits()) {
                    weighingList.Clear();       // Smazu seznam vazeni
                    flockList.Clear();          // Smazu seznam hejn
                    ClearWeighingDetails();
                    // Pokracuju dal v prekresleni
                }

                // Az po prepoctu jednotek muzu sestavit seznamy extra udaju pro hejna
                CalculateFlockData(flockList, displayedFlag);

                // Setridim seznam vazeni podle cisla dne (az po vypoctu dne, cilove hmotnosti a prirustku)
                weighingList.SortByFlockDay();
                
                // Prekreslim
                RedrawStatistics(weighingList);

                // Zobrazim hejna
                ShowFlocks();
            } finally {
                isLoadingWeighings = false;
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary>
        /// Redraws statistics
        /// </summary>
        /// <param name="flockDefinitionList">List of flock definitions</param>
        private void RedrawStatistics(FlockDefinitionList flockDefinitionList) {
            RedrawStatistics(flockDefinitionList, DateTime.MinValue, DateTime.MaxValue);    // Bez casoveho omezeni
        }

        private void DrawTriangle(Graphics graphics, int x, int y, bool isDown) {
            var triangleColor = Color.FromArgb(80, Color.Black);
            var pen = new Pen(triangleColor);
            int yIncrement;
            if (isDown) {
                yIncrement = 1;
            } else {
                yIncrement = -1;
            }
            // FillRectangle nevykresluje presne, radsi kreslim rucne jenotlive cary
            graphics.DrawLine(pen, x, y, x + 4, y);
            y += yIncrement;
            graphics.DrawLine(pen, x + 1, y, x + 3, y);
            y += yIncrement;
            var pt = new Bitmap(1, 1);
            pt.SetPixel(0, 0, triangleColor);
            graphics.DrawImageUnscaled(pt, x + 2, y);
        }

        private void dataGridWeighings_SelectionChanged(object sender, EventArgs e) {
            if (isLoadingWeighings) {
                // Data se nahravaji, nevykresluju
                return;
            }
            
            // Vykreslim histogram a vzorky vybraneho vazeni
            DisplaySelectedWeighing();
        }

        private void tabControlBottom_Selected(object sender, TabControlEventArgs e) {
            DisplaySelectedWeighing();
        }

        private void splitContainerBottom_Paint(object sender, PaintEventArgs e) {
            // Vykreslim caru a sipky, aby bylo patrne, ze se da graf zvetsit
            var splitContainer = (SplitContainer)sender;
            var lineY = splitContainer.SplitterDistance + splitContainer.SplitterWidth / 2;
            var lineColor = Color.FromArgb(30, Color.Black);
            e.Graphics.DrawLine(new Pen(lineColor), 0, lineY, splitContainer.Width, lineY);
            DrawTriangle(e.Graphics, 0, lineY - 2, false);
            DrawTriangle(e.Graphics, 0, lineY + 2, true);
            DrawTriangle(e.Graphics, splitContainer.Width - 5, lineY - 2, false);
            DrawTriangle(e.Graphics, splitContainer.Width - 5, lineY + 2, true);
        }

        private void splitContainerBottom_SplitterMoved(object sender, SplitterEventArgs e) {
            // Po posunu splitteru musim prekreslit, jinak v nekterych situacich focus zmrsi obsah
            var splitContainer = (SplitContainer)sender;
            splitContainer.Invalidate();
        }

        private void splitContainerBottom_Resize(object sender, EventArgs e) {
            // Po zvetseni splitteru musim taky prekreslit
            var splitContainer = (SplitContainer)sender;
            splitContainer.Invalidate();
        }

        private void buttonPrintSelectedWeighing_Click(object sender, EventArgs e) {
            if (dataGridWeighings.Rows.Count == 0) {
                return;         // V tabulce neni zadne vazeni
            }

            if (dataGridWeighings.SelectedCells.Count == 0) {
                return;         // Neni nic vybrane, nemelo by nastat
            }

            // Pred tiskem musim preskocit na stranku s histogramem, aby se graf inicializoval a spravne vykreslil
            tabControlBottom.SelectedTab = tabPageHistogram;
            tabControlBottom.Refresh();

            // Nactu vysledek prave vybraneho vazeni. Nemuzu predavat primo Weighing, protoze radek TOTAL obsahuje
            // jen vysledek, ne cele vazeni. Je treba mit moznost vytisknout i celkove vysledky.
            var index = dataGridWeighings.SelectedRows[0].Index;
            StatisticResult statisticResult;
            Weighing weighing = null;
            if (index < weighingList.List.Count) {
                statisticResult = weighingList.List[index].WeighingResults.GetResult(displayedFlag);
                weighing = weighingList.List[index];

                
                if (weighing.WeighingData.SampleList == null)
                {
                    //Bat 2 do not contains samples + need to implement histogram for bat2 first
                    throw new NotImplementedException("Missing implementation for Bat2.");
                }
            } else {
                statisticResult = weighingList.TotalstatisticResults.GetResult(displayedFlag);
            }
            if (statisticResult == null) {
                return;     // Zobrazeny flag v tomto vazeni neni
            }
            
            // Tisknu
            using (var graphics = CreateGraphics()) {
	            var printWeighing = new PrintWeighing(graphics);
	            
                // Spolecna data pro vsechny pripady
                printWeighing.StatisticResult = statisticResult;
                printWeighing.Flag = displayedFlag;
                printWeighing.Graph = userControlHistogram.Graph;
                printWeighing.Units = weighingList.List[0].WeighingData.ScaleConfig.Units.Units;    // Z 1. vazeni

                // Data podle toho, zda jde o vazeni nebo TOTAL
                if (index < weighingList.List.Count) 
                {
                   // Weighing 
                   printWeighing.IsTotal = false;
                   printWeighing.FileName = weighing.WeighingData.File.Name;

                   if (weighing.WeighingData.SampleList != null)
                   {
                      printWeighing.From = weighing.WeighingData.SampleList.MinDateTime;
                      printWeighing.To = weighing.WeighingData.SampleList.MaxDateTime;   
                   }
                   else   // manual result
                   {
                      printWeighing.IsManualResult = true;
                      var lastIndex = weighing.WeighingResults.StatisticResultList.Count - 1;
                      printWeighing.From = weighing.WeighingResults.StatisticResultList[lastIndex].WeighingStarted;
                      var gain = weighing.WeighingResults.StatisticResultList[lastIndex].Gain;
                      if (gain != null)
                      {
                         printWeighing.Gain = (float)gain;
                      }
                   }              
                }
                else 
                {  // Total
                   printWeighing.IsTotal = true;
                   printWeighing.FileName = Resources.TOTAL;
                }

                // Flock nebo vazeni zvlast
                if (flockList != null && !printWeighing.IsTotal) {
                    // Vazeni je soucasti hejna
                    printWeighing.IsFlock    = true;
                    printWeighing.Day        = weighing.FlockData.Day;
                    printWeighing.Target     = weighing.FlockData.Target;
                    printWeighing.Difference = weighing.FlockData.Difference;
                    printWeighing.CountAbove = weighing.FlockData.CountAbove;
                    printWeighing.CountBelow = weighing.FlockData.CountBelow;
                    printWeighing.Gain       = weighing.FlockData.Gain;
                } else {
                    // Bez hejna nebo radek TOTAL (ten nema ani u hejn vyplnene udaje hejna)
                    printWeighing.IsFlock = false;
                }
                
                userControlHistogram.SetForPrinting();
                try {
                    printWeighing.Print();
                } finally {
                    userControlHistogram.SetForScreen();
                }
            }

            // Prekreslim, graf menil rozmery atd.
            userControlHistogram.Refresh();
        }

        private void buttonPrintTable_Click(object sender, EventArgs e) {
            var title = "";
            if (displayedFlag != Flag.ALL) {
                title = Resources.SEX_LIMIT + ": " + displayedFlag.ToLocalizedString();
            }
            PrintDGV.Print(dataGridWeighings, Program.PrinterSettings, title, true, true, false);
        }

        private void buttonExportSelectedWeighing_Click(object sender, EventArgs e) {
            if (dataGridWeighings.Rows.Count == 0) {
                return;         // V tabulce neni zadne vazeni
            }

            if (dataGridWeighings.SelectedCells.Count == 0) {
                return;         // Neni nic vybrane, nemelo by nastat
            }

            var index = dataGridWeighings.SelectedRows[0].Index;
            if (index >= weighingList.List.Count) {
                return;         // Radek total nemuzu exportovat
            }

            // Nactu prave vybrane vazeni
            var weighing = weighingList.List[index];

            // Exportuju, zprumerovane vazeni z hejna nelze exportovat do formatu Bat1
            var isFlocks = weighing.WeighingData.Id < 0;
            new Export(this).ExportWeighing(weighing, displayedFlag, !isFlocks);   
        }

        private void buttonExportAllWeighings_Click(object sender, EventArgs e) {
            if (dataGridWeighings.Rows.Count == 0) {
                return;         // V tabulce neni zadne vazeni
            }

            if (dataGridWeighings.SelectedCells.Count == 0) {
                return;         // Neni nic vybrane, nemelo by nastat
            }

            // Ze seznamu vazeni musim vyhazet prumerovana vazeni hejn, ty nelz exportovat.
            // Zalozim novy seznam vazeni, do ktereho nakopiruju pouze vazeni nactene z DB
            var databaseWeighingList = new List<Weighing>();
            var isFlocks = false;
            foreach (var weighing in weighingList.List) {
                if (weighing.WeighingData.Id < 0) {
                    isFlocks = true;    // Zprumerovane vazeni z hejna nelze exportovat, nastavim flag
                }
                databaseWeighingList.Add(weighing);
            }
            if (databaseWeighingList.Count == 0) {
                return;         // V seznamu nezbylo zadne vazeni
            }
            
            // Exportuju, vysledky z hejn nelze exportovat do formatu Bat1
            new Export(this).ExportWeighingList(databaseWeighingList, displayedFlag, !isFlocks);
        }

        private void buttonExportTable_Click(object sender, EventArgs e) {
            if (dataGridWeighings.Rows.Count == 0) {
                return;         // V tabulce neni zadne vazeni
            }

            if (dataGridWeighings.SelectedCells.Count == 0) {
                return;         // Neni nic vybrane, nemelo by nastat
            }

            // Exportuju
            new Export(this).ExportStatisticsTable(weighingList, displayedFlag, flockList != null);
        }

    }
}
