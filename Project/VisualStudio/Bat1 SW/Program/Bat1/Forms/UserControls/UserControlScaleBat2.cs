﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Bat1.Properties;
using Bat1.SampleData;
using Bat2Library.Connection.Interface.Domain;
using Bat2Library.Connection.Interface.IO;
using Utilities.IOC;
using Bat2Library.Connection.Interface.Domain.Old;

namespace Bat1
{
   public partial class UserControlScaleBat2 : UserControl, IBat2ScaleControl
   {
      #region Private properties

      private IBat2NewDeviceManager manager;

      private delegate void ConnectCallback(object sender, Bat2DeviceData e);

      private delegate void DisconnectCallback(object sender, Bat2DeviceData e);

      #endregion

      public UserControlScaleBat2()
      {
         InitializeComponent();
         ThreadPool.QueueUserWorkItem(param =>
         {
            manager = IocContainer.Container.Get<IBat2NewDeviceManager>();
            foreach (var dev in manager.ConnectedDevices)
            {
               manager_DeviceConnected(this, dev);
            }
            manager.DeviceConnected += manager_DeviceConnected;
            manager.DeviceDisconnected += manager_DeviceDisconnected;

#if DEBUG
            // Sample device          
            var sample = new Bat2DeviceSampleData();
            manager_DeviceConnected(this, sample.Bat2DeviceData);
#endif

         });
      }
      #region Private methods

      private void manager_DeviceConnected(object sender, Bat2DeviceData e)
      {
          if (e is OldBat2DeviceData || e == null)
          {
              return;
          }

         if (userControlScaleResults2.InvokeRequired)
         {
            // If userControlScaleResults2 run in different thread then was been create,
            // it was called in origin thread.
            ConnectCallback con = manager_DeviceConnected;
            Invoke(con, new[] {sender, e});
         }
         else
         {

             var dev = FindDevice(e);
            if (dev == null)
            {
                dev = new Bat2Device(e, manager);
               userControlScaleResults2.Devices.Add(dev);
            }
            else
            {
                dev.AddDevice(e);
            }
          
            userControlScaleResults2.Update(true);         
          }
      }

      private void manager_DeviceDisconnected(object sender, Bat2DeviceData e)
      {
         if (userControlScaleResults2.InvokeRequired)
         {
            DisconnectCallback disCon = manager_DeviceDisconnected;
            Invoke(disCon, new[] {sender, e});
         }
         else
         {
             var dev = FindDevice(e);
            if (dev != null)
            {
                dev.RemoveDevice(e);
            }

            userControlScaleResults2.Update();
         }
      }

      private Bat2Device FindDevice(Bat2DeviceData device)
      {
         var serialNumber = device.Configuration.VersionInfo.SerialNumber;
         var dev = userControlScaleResults2.Devices.FirstOrDefault(i => i.SerialNumber == serialNumber);
         return dev;
      }

      /// <summary>
      /// Set computer time to scale.
      /// </summary>
      private void buttonSetDateTime_Click(object sender, EventArgs e)
      {
         var index = userControlScaleResults2.ActualDevice;
         if (index == -1)
         {
            return;
         }

         var device = userControlScaleResults2.Devices[index];

         if (device.IsConnected == false)
         {
            Cursor.Current = Cursors.Default;
            MessageBox.Show(Resources.SCALE_NOT_CONNECTED);
            return;
         }

         if (MessageBox.Show(Resources.SET_DATE_TIME, buttonSetDateTime.Text, MessageBoxButtons.YesNo) !=
             DialogResult.Yes)
         {
            return;
         }

         device.SetTime(DateTime.Now);
      }

      /// <summary>
      /// Reading archive. 
      /// </summary>
      private void buttonRead_Click(object sender, EventArgs e)
      {
         var index = userControlScaleResults2.ActualDevice;
         if (index >= 0 && userControlScaleResults2.Devices[index].IsConnected)
         {
            Cursor.Current = Cursors.WaitCursor;
            userControlScaleResults2.Devices[index].ReadArchive();
            userControlScaleResults2.Update(true);
            Cursor.Current = Cursors.Default;
         }
         else
         {
            MessageBox.Show(Resources.SCALE_NOT_CONNECTED);
         }
      }

      private void buttonSetParameters_Click(object sender, EventArgs e)
      {
         //TODO: which device's settings will be loaded if more devices with the same serial number will be connect?
         if (userControlScaleResults2.ActualDevice < 0)
         {
            return;
         }

         try
         {
            Cursor.Current = Cursors.WaitCursor;
            var dev = userControlScaleResults2.Devices[userControlScaleResults2.ActualDevice].GetDevice();
            Cursor.Current = Cursors.Default;
            if (dev != null)
            {
               new FormScaleSetup(dev).ShowDialog();
            }
         }
         catch (Exception ex)
         {          
            MessageBox.Show(ex.Message);
         }
         finally
         {
            Cursor.Current = Cursors.Default;
         }
      }

      private void flashButton_Click(object sender, EventArgs e)
      {
         var fileDialog = new OpenFileDialog();
         fileDialog.Filter = Resources.SCALE_FLASH_FILE_FILTER;
         fileDialog.FilterIndex = 1;
         if (fileDialog.ShowDialog() != DialogResult.OK)
         {
            return;
         }

         if (MessageBox.Show(Resources.SCALE_FLASH_CONFIRM, Resources.SCALE_FLASH_TITLE, MessageBoxButtons.YesNo) !=
             DialogResult.Yes)
         {
            return;
         }

         var continuousProgressWindow = new FormScaleFlashing();
         var backgroundWorker = new BackgroundWorker();
         backgroundWorker.DoWork += (senders, arguments) =>
         {
            //TODO:Call flashing function
            Thread.Sleep(5000);
         };
         backgroundWorker.RunWorkerCompleted += (senders, arguments) =>
         {
            continuousProgressWindow.DialogResult = DialogResult.OK;
            continuousProgressWindow.Close();
            continuousProgressWindow.Dispose();
         };

         backgroundWorker.RunWorkerAsync();
         continuousProgressWindow.ShowDialog(this);
      }

      #endregion

      #region Implementation of IView

      /// <summary>
      /// Gets or sets the data context of the view.
      /// This will be set to ViewModel of the view.
      /// </summary>
      public object DataContext { get; set; }

      #endregion

       private void buttonSetName_Click(object sender, EventArgs e)
       {
          // Nactu stavajici jmeno
          var index = userControlScaleResults2.ActualDevice;
          if (index < 0)
          {
             MessageBox.Show(Resources.SCALE_NOT_CONNECTED, Program.ApplicationName);
             return;
          }
          
          // Necham uzivatele jmeno editovat
          var form = new FormName(buttonSetName.Text, userControlScaleResults2.Devices[index].Name, true);
          if (form.ShowDialog() != DialogResult.OK)
          {
             return;
          }

          // Ulozim nove jmeno
          Cursor.Current = Cursors.WaitCursor;
          Refresh();
          try
          {
             userControlScaleResults2.Devices[index].SetScaleName(form.EnteredName);
          }
          finally
          {
             Cursor.Current = Cursors.Default;
          }
       }
   }

   public interface IBat2ScaleControl : IScaleControl
   {
   }
}
