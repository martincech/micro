﻿using System.Windows.Forms;
using Bat1.Properties;

namespace Bat1
{
    public partial class FormScaleFlashing : Form
    {
        public FormScaleFlashing()
        {
            InitializeComponent();
            richTextBoxMessage.Rtf = Resources.SCALE_FLASH_WARNING_RTF;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
            base.OnFormClosing(e);
        }
    }
}
