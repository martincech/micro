﻿using System.Windows.Forms;
using BatLibrary;

namespace Bat1 {
    public partial class FormUnits : Form {
        /// <summary>
        /// Selected units
        /// </summary>
        public Units SelectedUnits { get { return (Units)comboBoxUnits.SelectedIndex; } }
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="units">Units selected by default</param>
        public FormUnits(Units units) {
            InitializeComponent();

            comboBoxUnits.SelectedIndex = (int)units;
            comboBoxUnits.Focus();
        }

    }
}
