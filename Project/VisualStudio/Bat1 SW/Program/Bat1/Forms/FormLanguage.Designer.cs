﻿namespace Bat1 {
    partial class FormLanguage {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLanguage));
         this.label2 = new System.Windows.Forms.Label();
         this.buttonCancel = new System.Windows.Forms.Button();
         this.buttonOk = new System.Windows.Forms.Button();
         this.listBoxLanguage = new System.Windows.Forms.ListBox();
         this.SuspendLayout();
         // 
         // label2
         // 
         resources.ApplyResources(this.label2, "label2");
         this.label2.Name = "label2";
         // 
         // buttonCancel
         // 
         this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
         resources.ApplyResources(this.buttonCancel, "buttonCancel");
         this.buttonCancel.Name = "buttonCancel";
         this.buttonCancel.UseVisualStyleBackColor = true;
         // 
         // buttonOk
         // 
         resources.ApplyResources(this.buttonOk, "buttonOk");
         this.buttonOk.Name = "buttonOk";
         this.buttonOk.UseVisualStyleBackColor = true;
         this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
         // 
         // listBoxLanguage
         // 
         this.listBoxLanguage.FormattingEnabled = true;
         resources.ApplyResources(this.listBoxLanguage, "listBoxLanguage");
         this.listBoxLanguage.Name = "listBoxLanguage";
         this.listBoxLanguage.DoubleClick += new System.EventHandler(this.buttonOk_Click);
         // 
         // FormLanguage
         // 
         this.AcceptButton = this.buttonOk;
         resources.ApplyResources(this, "$this");
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.CancelButton = this.buttonCancel;
         this.Controls.Add(this.listBoxLanguage);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.buttonCancel);
         this.Controls.Add(this.buttonOk);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.MaximizeBox = false;
         this.MinimizeBox = false;
         this.Name = "FormLanguage";
         this.ShowIcon = false;
         this.ResumeLayout(false);
         this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.ListBox listBoxLanguage;
    }
}