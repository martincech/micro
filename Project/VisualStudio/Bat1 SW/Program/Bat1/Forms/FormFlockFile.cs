﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DataContext;

namespace Bat1 
{
    public partial class FormFlockFile : Form 
    {
       private List<FlockFile> fileList;
       public List<FlockFile> selectedFileList;
       
       private List<Weighing> weighingList; 

       public FormFlockFile(List<FlockFile> fileList) 
       {
          InitializeComponent();

          // Preberu seznam
          this.fileList = fileList;
          weighingList = new List<Weighing>(); 
          GetWeighings();
           
          // Vyberu prvni soubor        
          if (dataGridViewFiles.Rows.Count > 0)
          {
             dataGridViewFiles.Rows[0].Selected = true;
             buttonDetail.Enabled = true;
          }
          Cursor.Current = Cursors.Default;

          dataGridViewFiles.SelectionChanged += dataGridViewFiles_SelectionChanged;
       }

       private void dataGridViewFiles_SelectionChanged(object sender, EventArgs e)
       {
          buttonDetail.Enabled = false || dataGridViewFiles.SelectedRows.Count > 0;
       }

       private void GetWeighings()
       {
          dataGridViewFiles.Rows.Clear();
          foreach (var file in fileList)
          {
             var weighing = Program.Database.LoadWeighing(file.Id);

             dataGridViewFiles.Rows.Add(weighing.GetMinDateTime().ToString("g"),
                                        file.FileName,
                                        weighing.WeighingData.ScaleConfig.ScaleName,
                                        weighing.WeighingData.File.Note
                );
             weighingList.Add(weighing);
          }

          dataGridViewFiles_SelectionChanged(null, null);
       }

        private void FormFlockFile_Shown(object sender, EventArgs e) 
        {
            // Nastavim focus
           dataGridViewFiles.Focus();
        }

        private void buttonOk_Click(object sender, EventArgs e) 
        {
            // Sestavim seznam vybranych souboru
            selectedFileList = new List<FlockFile>();          
            for (var i = 0; i < dataGridViewFiles.Rows.Count; i++)
            {
               if (dataGridViewFiles.Rows[i].Selected)
               {
                  selectedFileList.Add(fileList[i]);
               }
            }

            // Hotovo
            DialogResult = DialogResult.OK;
        }
     
        private void dataGridViewFiles_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
           if (dataGridViewFiles.SelectedRows.Count <= 0)
           {
              return;
           }

           buttonOk_Click(null, null);
        }

        private void buttonDetail_Click(object sender, EventArgs e)
        {
           ShowDetails();
        }

        /// <summary>
        /// Return currently selected weighing
        /// </summary>
        /// <returns>Weighing</returns>
        private Weighing GetFirstSelectedWeighing()
        {
           if (dataGridViewFiles.Rows.Count == 0 || 
               dataGridViewFiles.SelectedRows.Count == 0)
           {
              return null;
           }

           return weighingList[dataGridViewFiles.SelectedRows[0].Index];          
        }

        /// <summary>
        /// Show weighing details.
        /// </summary>
        private void ShowDetails()
        {
           var weighing = GetFirstSelectedWeighing();
           if (weighing == null)
           {
              return;         // Tabulka je prazdna
           }

           if (weighing.WeighingData.SampleList == null)
           {
              // Rucni vazeni
              var formManual = new FormManualResults(weighing, true);
              formManual.ShowDialog();
           }
           else
           {
              // Vazeni z vahy

              // Predani 12tis vzorku trva, dam presypaci hodiny
              FormEditWeighing form;
              Cursor.Current = Cursors.WaitCursor;
              try
              {
                 // Nahraju z databaze seznam souboru
                 var fileListUnique = new NameNoteUniqueList();
                 foreach (var file in Program.Database.LoadFileNameNoteList())
                 {
                    fileListUnique.Add(file);
                 }

                 // Predam data oknu
                 form = new FormEditWeighing(weighing.WeighingData, fileListUnique, weighing.WeighingData.ScaleConfig, true, false);
              }
              finally
              {
                 Cursor.Current = Cursors.Default;
              }

              form.ShowDialog();
           }         
        }
    }
}
