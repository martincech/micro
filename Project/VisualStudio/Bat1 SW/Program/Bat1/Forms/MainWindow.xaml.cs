﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using Desktop.Wpf.Presentation;
using Application = System.Windows.Forms.Application;

namespace Bat1
{
   public partial class MainWindow : Window
   {
      private readonly IScaleControl userControlScale;
      private readonly IWeighingsControl userControlWeighings;
      private readonly IStatisticsControl userControlStatisticsTabs;
      private readonly IFlocksControl userControlFlocks;
      private readonly IMaintenanceControl userControlMaintenance;

      /// <summary>
      /// List of created user controls
      /// </summary>
      private readonly List<IView> listCreatedPages;

      public MainWindow(
         IScaleControl userControlScale,
         IWeighingsControl userControlWeighings,
         IStatisticsControl userControlStatisticsTabs,
         IFlocksControl userControlFlocks,
         IMaintenanceControl userControlMaintenance)
      {    
         this.userControlScale = userControlScale;
         this.userControlWeighings = userControlWeighings;
         this.userControlStatisticsTabs = userControlStatisticsTabs;
         this.userControlFlocks = userControlFlocks;
         this.userControlMaintenance = userControlMaintenance;
          
         InitializeComponent();
         listCreatedPages = new List<IView>();
         // Nastavim ikonu
         Icon = Imaging.CreateBitmapSourceFromHIcon(
            Properties.Resources.Bat1Icon.Handle,
            Int32Rect.Empty,
            BitmapSizeOptions.FromEmptyOptions());

         // Nastavim nazev aplikace
         Title = Program.ApplicationName;

         // Velikost okna nastavim podle rozliseni monitoru
         var areaWidth = SystemInformation.WorkingArea.Width;
         var areaHeight = SystemInformation.WorkingArea.Height;

         // Pokud je rozliseni monitoru mene nez 1024x768, zmensim okno
         if (areaWidth <= 1024 && areaHeight <= 768)
         {
            Width = areaWidth - 10;
            Height = areaHeight - 50;
         }

         // Pri vyssim DPI se default rozmer okna 1024x768 zvetsi (120DPI = 1292x812), tj. muze byt vetsi
         // nez velikost obrazovky napr. 1280x800.
         if (Width > areaWidth || Height > areaHeight)
         {
            // Nastavim okno pres celou obrazovku
            Width = areaWidth;
            Height = areaHeight;
         }         
      }

      private void ClearIconsCheck()
      {
         BatSynchronize.Visibility = Visibility.Collapsed;
         WindowsFormHost.Visibility = Visibility.Visible;
      }

      /// <summary>
      /// Add control to list
      /// </summary>
      /// <param name="control"></param>
      private void AddPage(IView control)
      {
         Control pageControl;
         if (control is UserControl)
         {
            pageControl = control as UserControl;
         }
         else if (control is UIElement)
         {
            pageControl = new ElementHost {Child = control as UIElement};
         }
         else
         {
            return;
         }
         if (PanelPages.Controls.Contains(pageControl))
         {
            return;
         }
         pageControl.Dock = DockStyle.Fill;
         listCreatedPages.Add(control);
         PanelPages.Controls.Add(pageControl);
      }

      /// <summary>
      /// Hide all pages except of visibleControl
      /// </summary>
      /// <param name="visibleControl">Control that should stay visible or null to hide all pages</param>
      private void HideAllPagesExcept(IView visibleControl)
      {
         foreach (var control in listCreatedPages)
         {
            if (control != visibleControl)
            {
               control.Hide();
            }
         }
      }

      /// <summary>
      /// Show specified page
      /// </summary>
      /// <param name="visibleControl"></param>
      private void ShowPage(IView visibleControl)
      {
         // Schovam vsechny detaily krome tech, ktere mam zobrazit
         HideAllPagesExcept(visibleControl);

         if (visibleControl == null)
         {
            // Chce pouze schovat vsechny detaily, nic nezobrazuju
            return;
         }

         // Zobrazim zvolene detaily
         visibleControl.Show();
      }

      /// <summary>
      /// Show scale page
      /// </summary>
      private void ShowScale()
      {
         AddPage(userControlScale);
         // Zobrazim
         ShowPage(userControlScale);
      }

      /// <summary>
      /// Show weighings page
      /// </summary>
      private void ShowWeighings()
      {
         AddPage(userControlWeighings);
         // Control uz existuje, pouze znovu nahraju vazeni z databaze (mohl nacist nova vazeni z vahy)
         userControlWeighings.ReloadWeighingList();

         // Zobrazim
         ShowPage(userControlWeighings);
      }

      /// <summary>
      /// Show statistics page
      /// </summary>
      private void ShowStatistics()
      {
         AddPage(userControlStatisticsTabs);
         // Zobrazim
         ShowPage(userControlStatisticsTabs);

         // Updatuju Last weighings
         // Pozor: pri volani teto fce uz musi byt userControlStatisticsTabs zobrazeny. Nastavuje se zde property
         // Visible u orniho panelu a pokud je cely control skryty, Visible zustane vzdy false a nenastavi se.
         userControlStatisticsTabs.ClearData();
      }

      /// <summary>
      /// Show flocks page
      /// </summary>
      private void ShowFlocks()
      {
         AddPage(userControlFlocks);
         userControlFlocks.ClearData();

         // Zobrazim
         ShowPage(userControlFlocks);
      }

      /// <summary>
      /// Show maintenance page
      /// </summary>
      private void ShowMaintenance()
      {
         AddPage(userControlMaintenance);
         // Zobrazim
         ShowPage(userControlMaintenance);
      }

      private void ToolStripButtonWeighings_Click(object sender, RoutedEventArgs e)
      {
         System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
         try
         {
            ShowWeighings();
            ClearIconsCheck();
            //toolStripButtonWeighings.Checked = true;
         }
         finally
         {
            System.Windows.Forms.Cursor.Current = Cursors.Default;
         }
      }

      private void ToolStripButtonFlocks_Click(object sender, RoutedEventArgs e)
      {
         //if (toolStripButtonFlocks.Checked)
         //{
         //   return; // Hejna jsou prave zobrazene
         //}

         System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
         try
         {
            ClearIconsCheck();
            //toolStripButtonFlocks.Checked = true;
            ShowFlocks();
         }
         finally
         {
            System.Windows.Forms.Cursor.Current = Cursors.Default;
         }
      }

      private void ToolStripButtonMaintenance_Click(object sender, RoutedEventArgs e)
      {
         //if (toolStripButtonMaintenance.Checked)
         //{
         //   return; // Udrzba je prave zobrazena
         //}

         ClearIconsCheck();
         //toolStripButtonMaintenance.Checked = true;
         ShowMaintenance();
      }

      private void ToolStripButtonReadData_Click(object sender, RoutedEventArgs e)
      {
         System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
         try
         {
            ShowScale();
            ClearIconsCheck();
            //toolStripButtonScale.Checked = true;
         }
         finally
         {
            System.Windows.Forms.Cursor.Current = Cursors.Default;
         }
      }

      private void Window_Loaded(object sender, RoutedEventArgs e)
      {
        // Application.EnableVisualStyles();
      }

      private void ToolStripButtonStatistics_Click(object sender, RoutedEventArgs e)
      {
         //if (toolStripButtonStatistics.Checked)
         //{
         //   return; // Statistika je prave zobrazena
         //}

         System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
         try
         {
            ClearIconsCheck();
            //toolStripButtonStatistics.Checked = true;
            ShowStatistics();
         }
         finally
         {
            System.Windows.Forms.Cursor.Current = Cursors.Default;
         }
      }

      private void ToolStripButtonSynchronize_Click(object sender, RoutedEventArgs e)
      {
         //Hide all possible visible pages
         HideAllPagesExcept(null);

         WindowsFormHost.Visibility = Visibility.Collapsed;
         BatSynchronize.Visibility = Visibility.Visible;
      }    

      private void Window_Closing(object sender, CancelEventArgs e)
      {
         //Zkontroluju, zda byla naposledy nactena data ulozena do DB
         if (!SaveStatus.Check(Program.ApplicationName))
         {
            e.Cancel = true; // Nechce pokracovat
         }
      }
   }
}