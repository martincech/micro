﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Media;
using Bat2Library.Connection.Interface.Domain;
using OxyPlot;
using OxyPlot.Annotations;
using OxyPlot.Axes;

namespace Bat1.Forms.Bat2.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for TimeView.xaml
   /// </summary>
   public partial class TimeView : UserControl
   {
      #region Fields and properties

      public PlotModel GraphModel { get; private set; }
      public bool HasError { get; private set; }

      private ObservableCollection<RectangleAnnotation> annotations; //list of all annotations    
      private RectangleAnnotation range;  //actual inserted annotation
      private double startx;
      private bool annotationClicked;
      private bool editAnnotation;
      private bool splitAnnotation;
      private int editAnnotationIndex;
      private double startXEdit; //original x coordinate before editing
      private bool initialize;

      private static OxyColor colorNormal = OxyColor.FromAColor(120, OxyColors.Blue);
      private static Brush defaultBorder = new SolidColorBrush(Color.FromRgb(227, 233, 239));

      // time constants
      private const int MINUTE = 60;
      private const int HOUR = 60 * MINUTE;
      private const int DAY = HOUR * 24 - 1;

      private const double PRECISION = 0.00001;

      #endregion

      #region Public interface

      #region Constructors

      public TimeView()
      {
         HasError = false;
         initialize = true;
         GraphModel = new PlotModel();
         annotations = new ObservableCollection<RectangleAnnotation>();        
         range = new RectangleAnnotation { Fill = colorNormal, MinimumX = 0, MaximumX = 0 };
         startx = double.NaN;
         GraphModel.Annotations.Add(range);
         annotationClicked = false;
         splitAnnotation = false;

         InitAxis();
         InitializeComponent();
         
         // mouse events
         GraphModel.MouseDown += GraphModel_MouseDown;
         GraphModel.MouseMove += GraphModel_MouseMove;
         GraphModel.MouseUp += GraphModel_MouseUp;

         GraphModel.Axes[0].TransformChanged += TimeView_AxisChanged;             
      }

      #endregion

      #endregion

      #region Mouse events

      private void GraphModel_MouseDown(object sender, OxyMouseDownEventArgs e)
      {
         if (e.ChangedButton == OxyMouseButton.Left && annotationClicked == false)
         {
            if (!GraphModel.Annotations.Contains(range))
            {
               GraphModel.Annotations.Add(range);
            }
            startx = range.InverseTransform(e.Position).X;

            // round to actual minor step of x-axis     
            startx = RoundToStep(startx);
            range.MinimumX = startx;
            range.MaximumX = startx;
            GraphModel.InvalidatePlot(true);
            e.Handled = true;
         }
      }

      private void GraphModel_MouseMove(object sender, OxyMouseEventArgs e)
      {
         if (!double.IsNaN(startx))
         {
            var x = range.InverseTransform(e.Position).X;
            var minX = RoundToStep(Math.Min(x, startx));
            var maxX = RoundToStep(Math.Max(x, startx));

            range.MinimumX = (minX < 0.0) ? 0.0 : minX;
            range.MaximumX = (maxX > DAY) ? DAY : maxX;

            var from = TimeSpan.FromSeconds(range.MinimumX);
            var to = TimeSpan.FromSeconds(range.MaximumX);
            range.Text = String.Format("{0}\n{1}", from.ToString(@"hh\:mm"), to.ToString(@"hh\:mm"));

            GraphModel.InvalidatePlot(true);
            e.Handled = true;
         }

         if (annotationClicked == true && editAnnotationIndex < annotations.Count)
         {  // Editing annotation
            var x = annotations[editAnnotationIndex].InverseTransform(e.Position).X;
            var newX = RoundToStep(x);
            if (newX > DAY)
            {
               newX = DAY;
            }
            if (newX < 0.0)
            {
               newX = 0.0;
            }

            //click on last segment in annotation = edit
            if (editAnnotation == true)
            {
               if (newX < annotations[editAnnotationIndex].MinimumX)
               {
                  annotations[editAnnotationIndex].MaximumX = annotations[editAnnotationIndex].MinimumX;
               }
               else if (Math.Abs(startx - x) > PRECISION) // startXEdit != x
               {
                  annotations[editAnnotationIndex].MaximumX = newX;
               }

               var from = TimeSpan.FromSeconds(annotations[editAnnotationIndex].MinimumX);
               var to = TimeSpan.FromSeconds(annotations[editAnnotationIndex].MaximumX);
               annotations[editAnnotationIndex].Text = String.Format("{0}\n{1}", from.ToString(@"hh\:mm"), to.ToString(@"hh\:mm"));
            }

            //click on non-last segment, if move left, delete segment and create new annotation
            if (editAnnotation == false &&
                newX <= (startXEdit - (GraphModel.Axes[0].ActualMinorStep * 0.75)) &&
                newX >= annotations[editAnnotationIndex].MinimumX)
            {
               if (splitAnnotation == true)
               {  //new annotation behind editing place
                  var ann = new RectangleAnnotation
                  {
                     Fill = colorNormal,
                     MinimumX = RoundToStep(startXEdit),
                     MaximumX = annotations[editAnnotationIndex].MaximumX,
                  };
                  var fromNew = TimeSpan.FromSeconds(ann.MinimumX);
                  var toNew = TimeSpan.FromSeconds(ann.MaximumX);
                  ann.Text = String.Format("{0}\n{1}", fromNew.ToString(@"hh\:mm"), toNew.ToString(@"hh\:mm"));

                  AddNewAnnotation(ann);
                  AddModel(fromNew, toNew);

                  splitAnnotation = false;
               }

               //old reduced annotation 
               annotations[editAnnotationIndex].MaximumX = newX;
               var from = TimeSpan.FromSeconds(annotations[editAnnotationIndex].MinimumX);
               var to = TimeSpan.FromSeconds(annotations[editAnnotationIndex].MaximumX);
               annotations[editAnnotationIndex].Text = String.Format("{0}\n{1}", from.ToString(@"hh\:mm"), to.ToString(@"hh\:mm"));
            }

            GraphModel.InvalidatePlot(true);
         }
      }

      private void GraphModel_MouseUp(object sender, OxyMouseEventArgs e)
      {
         if (annotationClicked == true &&
            (Math.Abs(annotations[editAnnotationIndex].MaximumX - annotations[editAnnotationIndex].MinimumX) <= PRECISION))  //annotations[editAnnotationIndex].MaximumX == annotations[editAnnotationIndex].MinimumX
         {
            GraphModel.Annotations.Remove(annotations[editAnnotationIndex]);
            annotations.RemoveAt(editAnnotationIndex);
         }

         annotationClicked = false;
         editAnnotation = false;

         if (startx.Equals(double.NaN) || (Math.Abs(range.MinimumX - range.MaximumX) <= PRECISION))  //range.MinimumX == range.MaximumX
         {
            GraphModel.Annotations.Remove(range);
            CheckJoinAnnotations();
            SynchronizeModels();
            GraphModel.InvalidatePlot(true);
            startx = double.NaN;
            return;
         }

         AddNewAnnotation(null);

         var from = TimeSpan.FromSeconds(range.MinimumX);
         var to = TimeSpan.FromSeconds(range.MaximumX);
         AddModel(from, to);

         startx = double.NaN;
         GraphModel.Annotations.Remove(range);

         CheckJoinAnnotations();
         SynchronizeModels();
         GraphModel.InvalidatePlot(true);
      }

      private void ExistingAnnotations_MouseDown(object sender, OxyMouseDownEventArgs e)
      {
         if (e.ChangedButton == OxyMouseButton.Left)
         {
            annotationClicked = true;

            editAnnotationIndex = annotations.IndexOf((RectangleAnnotation)sender);
            startXEdit = annotations[editAnnotationIndex].InverseTransform(e.Position).X;

            if (startXEdit <= annotations[editAnnotationIndex].MaximumX &&
               startXEdit >= (annotations[editAnnotationIndex].MaximumX - GraphModel.Axes[0].ActualMinorStep))
            {
               editAnnotation = true;
            }

            splitAnnotation = true;
         }
      }

      /// <summary>
      /// Change graph's scale when graph is zoom in/out.
      /// </summary>     
      private void TimeView_AxisChanged(object sender, EventArgs e)
      {
         const int hourConst = 45080;
         const int halHourConst = 12600;
         const int quarterConst = 10350;
         const int tenMinutesConst = 8000;
         const int fiveMinutesConst = 2000;

         const int distance1 = 5;
         const int distance2 = 10;
         const int distance3 = 15;
         const int distance4 = 30;

         var diff = GraphModel.Axes[0].ActualMaximum - GraphModel.Axes[0].ActualMinimum;
         double step = HOUR;

         if (diff > hourConst)
         {
            step = HOUR;
         }
         else if (diff <= hourConst && diff > halHourConst)
         {
            step = MINUTE * distance4;
         }
         else if (diff <= halHourConst && diff > quarterConst)
         {
            step = MINUTE * distance3;
         }
         else if (diff <= quarterConst && diff > tenMinutesConst)
         {
            step = MINUTE * distance2;
         }
         else if (diff <= tenMinutesConst && diff > fiveMinutesConst)
         {
            step = MINUTE * distance1;
         }
         else if (diff <= fiveMinutesConst)
         {
            step = MINUTE;
         }

         GraphModel.Axes[0].MinorStep = step;
      }

      #endregion

      #region Private helpers

      /// <summary>
      /// Test for join more annotations to one bigger 
      /// </summary>
      private void CheckJoinAnnotations()
      {
         for (var i = 1; i < annotations.Count; i++)
         {
            var before = annotations[i - 1];
            var after = annotations[i];

            if (before.MaximumX >= after.MinimumX)
            {
               if (before.MaximumX < after.MaximumX)
               {
                  annotations[i - 1].MaximumX = after.MaximumX;
               }
               var from = TimeSpan.FromSeconds(annotations[i - 1].MinimumX);
               var to = TimeSpan.FromSeconds(annotations[i - 1].MaximumX);
               annotations[i - 1].Text = String.Format("{0}\n{1}", from.ToString(@"hh\:mm"), to.ToString(@"hh\:mm"));

               GraphModel.Annotations.Remove(annotations[i]);
               annotations.RemoveAt(i);
               i -= 1;
            }
         }
      }


      private void AddNewAnnotation(RectangleAnnotation insertedAnnotation)
      {
         if (insertedAnnotation == null)
         {
            insertedAnnotation = range;
         }

         var newAnnotation = new RectangleAnnotation
         {
            Fill = colorNormal,
            MinimumX = insertedAnnotation.MinimumX,
            MaximumX = insertedAnnotation.MaximumX,
            Text = insertedAnnotation.Text,
         };

         //insert new annotation to ordered list => find index where be located
         int i;
         if (annotations.Count > 0 && insertedAnnotation.MinimumX < annotations[0].MinimumX)
         {
            i = 0;
         }
         else
         {
            for (i = 1; i < annotations.Count; i++)
            {
               var before = annotations[i - 1];
               var after = annotations[i];

               if (before.MinimumX <= insertedAnnotation.MinimumX &&
                   after.MinimumX > insertedAnnotation.MinimumX)
               {
                  break;
               }
            }
         }

         if (i >= annotations.Count)
         {
            annotations.Add(newAnnotation);
            annotations[annotations.Count - 1].MouseDown += ExistingAnnotations_MouseDown;
            GraphModel.Annotations.Add(annotations[annotations.Count - 1]);
         }
         else
         {
            annotations.Insert(i, newAnnotation);
            annotations[i].MouseDown += ExistingAnnotations_MouseDown;
            GraphModel.Annotations.Insert(i, annotations[i]);
         }        
      }

      /// <summary>
      /// Round to selected step
      /// </summary>
      /// <param name="number">number which will be round</param>
      /// <param name="stepParameter">if null, step use actual minor step of x axis</param>
      /// <returns>rounded number</returns>
      private double RoundToStep(double number, double? stepParameter = null)
      {
         var step = stepParameter ?? GraphModel.Axes[0].ActualMinorStep;
         var rest = number % step;
         if (rest < (step * 0.75))
         {
            number -= rest;
         }
         else
         {
            number += (step - rest);
         }

         return number;
      }


      private void InitAxis()
      {
         if (GraphModel == null)
         {
            return;
         }
         // x-axis
         GraphModel.Axes.Add(new TimeSpanAxis
         {
            MajorGridlineStyle = LineStyle.Solid,
            MinorGridlineStyle = LineStyle.Dot,
            Position = AxisPosition.Bottom,
            StringFormat = "h:mm",
            AbsoluteMinimum = 0,
            AbsoluteMaximum = HOUR * 24,
            Maximum = HOUR * 24,
            MinimumRange = MINUTE * 10,
            MinorStep = HOUR
         });

         // y-axis (invisible)
         GraphModel.Axes.Add(new LinearAxis
         {
            Position = AxisPosition.Left,
            AbsoluteMaximum = 100,
            AbsoluteMinimum = 0,
            IsZoomEnabled = false,
            IsAxisVisible = false
         });
      }

      /// <summary>
      /// Click event on add new time model.
      /// </summary>
      private void AddTimeModel_Click(object sender, System.Windows.RoutedEventArgs e)
      {        
         if (!ViewModelHasErrors())
         {
            var from = new TimeSpan(TimePickerFrom.Value.Value.Hours, TimePickerFrom.Value.Value.Minutes, 0);
            var to = new TimeSpan(TimePickerTo.Value.Value.Hours, TimePickerTo.Value.Value.Minutes, 0);
            var annotationsCount = annotations.Count;

            AddNewAnnotation(CreateAnnotation(from, to));
            CheckJoinAnnotations();
            GraphModel.InvalidatePlot(true);

            // if annotations count changed, add new model, else synchronize model's properties
            if (annotationsCount < annotations.Count)
            {
               AddModel(from, to);
            }
            else
            {
               SynchronizeModels();
            }
         }
      }

      /// <summary>
      /// Click event on delete time model.
      /// </summary>
      private void DeleteTimeModel_Click(object sender, System.Windows.RoutedEventArgs e)
      {
         if (!ViewModelHasErrors())
         {
            var delTimeFrom = new TimeSpan(TimePickerFrom.Value.Value.Hours, TimePickerFrom.Value.Value.Minutes, 0);
            var delTimeTo = new TimeSpan(TimePickerTo.Value.Value.Hours, TimePickerTo.Value.Value.Minutes, 0);

            var delFrom = delTimeFrom.TotalSeconds;
            var delTo = delTimeTo.TotalSeconds;

            for (var i = annotations.Count - 1; i >= 0; i--)
            {
               if (annotations[i].MaximumX <= delTo && annotations[i].MinimumX >= delFrom)
               {  //delete whole annotation
                  GraphModel.Annotations.Remove(annotations[i]);
                  annotations.RemoveAt(i);
               }
               else if (annotations[i].MaximumX > delTo && annotations[i].MinimumX < delTo)
               {
                  //edit end of current annotation
                  double min = annotations[i].MinimumX;
                  annotations[i].MinimumX = delTo;
                  annotations[i].Text = String.Format("{0}\n{1}", delTimeTo.ToString(@"hh\:mm"), TimeSpan.FromSeconds(annotations[i].MaximumX).ToString(@"hh\:mm"));

                  if (min < delFrom)
                  {  //delete middle of annotation => create new annotation
                     annotations.Insert(i, CreateAnnotation(TimeSpan.FromSeconds(min), delTimeFrom));
                     AddModel(TimeSpan.FromSeconds(min), delTimeFrom);
                     annotations[i].MouseDown += ExistingAnnotations_MouseDown;
                     GraphModel.Annotations.Insert(i, annotations[i]);
                  }
               }
               else if (annotations[i].MinimumX < delFrom)
               {  //delete end part of current annotation
                  annotations[i].MaximumX = delFrom;
                  annotations[i].Text = String.Format("{0}\n{1}", TimeSpan.FromSeconds(annotations[i].MinimumX).ToString(@"hh\:mm"), delTimeFrom.ToString(@"hh\:mm"));
               }
            }

            SynchronizeModels();
            GraphModel.InvalidatePlot(true);
         }
      }

      /// <summary>
      /// Create new annotation from selected time period.
      /// </summary>
      /// <param name="from">start value</param>
      /// <param name="to">stop value</param>
      /// <returns>created annotation</returns>
      private RectangleAnnotation CreateAnnotation(TimeSpan from, TimeSpan to)
      {
         var fromNumber = from.TotalSeconds;
         var toNumber = to.TotalSeconds;

         var ann = new RectangleAnnotation
         {
            Fill = colorNormal,
            MinimumX = fromNumber,
            MaximumX = toNumber,
            Text = String.Format("{0}\n{1}", @from.ToString(@"hh\:mm"), to.ToString(@"hh\:mm")),
         };
         return ann;
      }      

      /// <summary>
      /// Call method in view model to add new model.
      /// </summary>
      private void AddModel(TimeSpan from, TimeSpan to)
      {
         var list = DataContext as ObservableCollection<TimeRange>;
         var newTime = new TimeRange
         {
            From = new DateTime(1, 1, 1, @from.Hours, @from.Minutes, 0),
            To = new DateTime(1, 1, 1, to.Hours, to.Minutes, 0)
         };

         list.Add(newTime);      
      }

      private void SynchronizeModels()
      {
         var listFrom = new List<DateTime>();
         var listTo = new List<DateTime>();
         GetTimesLists(ref listFrom, ref listTo);

         var list = DataContext as ObservableCollection<TimeRange>;
         int i;
         for (i = 0; i < listFrom.Count; i++)
         {   //update list of times by annotations in graph
            list[i].From = listFrom[i];
            list[i].To = listTo[i];
         }

         //delete redundant times
         while (i < list.Count)
         {
            list.RemoveAt(i);
         }

         DataContext = list;
      }

      /// <summary>
      /// Return lists of time interval used in annotations.
      /// </summary>
      private void GetTimesLists(ref List<DateTime> listFrom, ref List<DateTime> listTo)
      {
         listFrom.Clear();
         listTo.Clear();

         foreach (var ann in annotations)
         {
            var timeFrom = TimeSpan.FromSeconds(ann.MinimumX);
            var dateFrom = new DateTime(1, 1, 1, timeFrom.Hours, timeFrom.Minutes, 0);

            var timeTo = TimeSpan.FromSeconds(ann.MaximumX);
            var dateTo = new DateTime(1, 1, 1, timeTo.Hours, timeTo.Minutes, 0);

            listFrom.Add(dateFrom);
            listTo.Add(dateTo);
         }
      }

      /// <summary>
      /// Get flag presents view model validation result.
      /// </summary>
      /// <returns>true - error in validation; false - no errors</returns>      
      private bool ViewModelHasErrors()
      {
         if (!TimePickerTo.Value.HasValue || !TimePickerFrom.Value.HasValue)
         {
            HasError = true;
            return true;
         }

         var testFrom = new DateTime(1, 1, 1, TimePickerFrom.Value.Value.Hours, TimePickerFrom.Value.Value.Minutes, 0);
         var testTo = new DateTime(1, 1, 1, TimePickerTo.Value.Value.Hours, TimePickerTo.Value.Value.Minutes, 0);

         if (DateTime.Compare(testFrom, testTo) < 0)
         {
            HasError = false;
            return false;
         }       
         HasError = true;
         return true;       
      }

      /// <summary>
      /// Load initialize time range values to graph.
      /// </summary>
      private void TimeViewGraph_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
      {
         if (initialize == false)
         {  //Change model => delete graph and load initialize data
            GraphModel.Annotations.Clear();
            annotations.Clear();            
         }
         initialize = false;
       
         //get list of time range
         var list = e.NewValue as ObservableCollection<TimeRange>;        

         //add time to graph
         var ann = new RectangleAnnotation();
         ann.Fill = colorNormal;

         foreach (var time in list)
         {
            var fromTime = new TimeSpan(time.From.Hour, time.From.Minute, time.From.Second);
            var toTime = new TimeSpan(time.To.Hour, time.To.Minute, time.To.Second);

            ann.MinimumX = fromTime.TotalSeconds;
            ann.MaximumX = toTime.TotalSeconds;
            ann.Text = String.Format("{0}\n{1}", fromTime.ToString(@"hh\:mm"), toTime.ToString(@"hh\:mm"));

            AddNewAnnotation(ann);
         }

         GraphModel.InvalidatePlot(true);
      }

      private void timePicker_ValueChanged(object sender, System.Windows.RoutedPropertyChangedEventArgs<object> e)
      {
         if (TimePickerTo == null || TimePickerFrom == null)
         {
            return;
         }

         if (ViewModelHasErrors())
         {
            TimePickerFrom.BorderBrush = Brushes.Red;
            TimePickerTo.BorderBrush = Brushes.Red;

            var toolTipErr = new ToolTip {Content = "Time From is later than To!"};
            TimePickerFrom.ToolTip = toolTipErr;
            TimePickerTo.ToolTip = toolTipErr;
         }
         else
         {
            TimePickerFrom.BorderBrush = defaultBorder;
            TimePickerTo.BorderBrush = defaultBorder;

            TimePickerFrom.ToolTip = null;
            TimePickerTo.ToolTip = null;
         }
      }

      #endregion
   }
}
