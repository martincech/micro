﻿using Bat2Library;
using DataContext;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat1.Forms.Bat2.ModelViews.Applications
{
   public class Bat2IdentificationViewModel : ObservableObject
   {
      #region Private fields

      private int serialNumber;
      private string name;
      //private string password;
      private int hardwareBuild;
      private int hardwareMinor;
      private int hardwareMajor;
      private int softwareBuild;
      private int softwareMinor;
      private int softwareMajor;
      private DeviceModificationE modification;
      private DeviceClassE @class;

      #endregion

      #region Public interface

      #region Constructors

      public Bat2IdentificationViewModel(IView view, Bat2Scale device)
      {
         SerialNumber = device.SerialNumber;
         Name = device.ScaleName;
         HardwareMajor = device.HwVersion.Major;
         HardwareMinor = device.HwVersion.Minor;
         HardwareBuild = device.HwVersion.Build;
         SoftwareMajor = device.SwVersion.Major;
         SoftwareMinor = device.SwVersion.Minor;
         SoftwareBuild = device.SwVersion.Build;
         Modification = (DeviceModificationE)device.Modification;
         Class = (DeviceClassE)device.Class;

         view.DataContext = this;
      }

      #endregion

      #region Properties

      public int SerialNumber { get { return serialNumber; } set { SetProperty(ref serialNumber, value); } }
    
      public string Name { get { return name; } set { SetProperty(ref name, value); } }
     
      //public string Password { get { return password; } set { SetProperty(ref password, value); } }

      /// <summary>
      /// Hardware build version
      /// </summary>
      public int HardwareBuild { get { return hardwareBuild; } set { SetProperty(ref hardwareBuild, value); } }
      /// <summary>
      /// Hardware minor version
      /// </summary>
      public int HardwareMinor { get { return hardwareMinor; } set { SetProperty(ref hardwareMinor, value); } }
      /// <summary>
      /// Hardware major version
      /// </summary>
      public int HardwareMajor { get { return hardwareMajor; } set { SetProperty(ref hardwareMajor, value); } }
      /// <summary>
      /// Software build version
      /// </summary>
      public int SoftwareBuild { get { return softwareBuild; } set { SetProperty(ref softwareBuild, value); } }
      /// <summary>
      /// Software minor version
      /// </summary>
      public int SoftwareMinor { get { return softwareMinor; } set { SetProperty(ref softwareMinor, value); } }
      /// <summary>
      /// Software major version
      /// </summary>
      public int SoftwareMajor { get { return softwareMajor; } set { SetProperty(ref softwareMajor, value); } }
      /// <summary>
      /// This device modification type.
      /// </summary>
      public DeviceModificationE Modification { get { return modification; } set { SetProperty(ref modification, value); } }
      /// <summary>
      /// Class of device
      /// </summary>
      public DeviceClassE Class { get { return @class; } set { SetProperty(ref @class, value); } }

      #endregion 

      #endregion
   }
}
