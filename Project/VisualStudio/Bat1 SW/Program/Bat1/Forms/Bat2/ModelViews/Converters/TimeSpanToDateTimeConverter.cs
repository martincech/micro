﻿using System;
using System.Windows.Data;

namespace Bat1.Forms.Bat2.ModelViews.Converters
{
   [ValueConversion(typeof(TimeSpan), typeof(DateTime))]
   public class TimeSpanToDateTimeConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         DateTime time;
         if (targetType == typeof(TimeSpan?) && DateTime.TryParse(value.ToString(), out time))
         {
            return new TimeSpan(time.Hour, time.Minute, time.Second);
         }
         return null;
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         TimeSpan time;
         if (targetType == typeof(DateTime) && TimeSpan.TryParse(value.ToString(), out time))
         {
            return new DateTime(1, 1, 1, time.Hours, time.Minutes, time.Seconds);
         }
         return null;
      }
   }
}
