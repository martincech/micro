﻿using System.Windows;
using Desktop.Wpf.Presentation;

namespace Bat1.Forms.Bat2.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for Bat2IdentificationView.xaml
   /// </summary>
   public partial class Bat2IdentificationView : IBat2IdentificationView
   {    
      public Bat2IdentificationView()
      {
         InitializeComponent();     
      }


      public void Show()
      {
         Visibility = Visibility.Visible;
      }

      public void Hide()
      {
         Visibility = Visibility.Collapsed;
      }
   }

   public interface IBat2IdentificationView : IView
   {
   }
}
