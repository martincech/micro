﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Bat1.Forms.Bat2.ModelViews.Applications;
using Bat2Library;
using Bat2Library.Desktop.Presentation.Converters;

namespace Bat1.Forms.Bat2.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for Rs485OptionsView.xaml
   /// </summary>
   public partial class Rs485OptionsView : UserControl
   {
      public Rs485OptionsView()
      {
         InitializeComponent();      
      }

      private void ModeComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
      {
         var vm = DataContext as Rs485OptionsListViewModel;
         if (vm == null) return;

         var nextModel = vm.Models.First(m => m != vm.Model);

         var other = nextModel.Mode.ToString().ToLower();
         var otherSplit = other.Split('_');
         var current = ModeComboBox.SelectedItem.ToString().ToLower();

         if (current.Equals(otherSplit.Last()))
         {
            if (!nextModel.Enabled) //other model is not enable => new value is valid
            {
               var c = new Rs485ModeEConverter();
               nextModel.Mode = (Rs485ModeE)c.ConvertBack(e.RemovedItems[0], typeof(Rs485ModeE), null, null);
               return;
            }

            ModeComboBox.SelectedItem = e.RemovedItems[0];
            MessageBox.Show("Selected item is already set on the other interface.", "Error");
         }
      }
   }
}
