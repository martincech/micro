﻿using System.Net;
using Bat2Library.Connection.Interface.Domain;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat1.Forms.Bat2.ModelViews.Applications
{
   public class EthernetViewModel : ValidatableObservableObject
   {
      #region Private fields

      private bool dhcp;
      private IPAddress gateway;
      private IPAddress ip;
      private IPAddress primaryDns;
      private IPAddress secondaryDns;
      private IPAddress subnetMask;

      #endregion

      #region Public interface

      #region Constructors

      public EthernetViewModel(IView view, Ethernet ethernet)
      {
         Dhcp = ethernet.Dhcp;
         Gateway = ethernet.Gateway;
         Ip = ethernet.Ip;
         PrimaryDns = ethernet.PrimaryDns;
         SubnetMask = ethernet.SubnetMask;

         view.DataContext = this;
      }

      #endregion

      #region Properties

      public bool Dhcp
      {
         get
         {
            return dhcp;
         }
         set
         {
            SetProperty(ref dhcp, value);
         }
      }

      public IPAddress Gateway
      {
         get
         {
            return gateway;
         }
         set
         {
            SetPropertyAndValidate(ref gateway, value);
         }
      }

      public IPAddress Ip
      {
         get
         {
            return ip;
         }
         set
         {
            SetPropertyAndValidate(ref ip, value);
         }
      }

      public IPAddress PrimaryDns { get { return primaryDns; } set { SetPropertyAndValidate(ref primaryDns, value); } }
      public IPAddress SecondaryDns { get { return secondaryDns; } set { SetPropertyAndValidate(ref secondaryDns, value); } }
      public IPAddress SubnetMask { get { return subnetMask; } set { SetPropertyAndValidate(ref subnetMask, value); } }

      #endregion 

      #endregion

      #region Private helpers

      //protected override IEnumerable<ValidationResult> AdditionalValidationRules(object value, string propertyName)
      //{
      //   IPAddress val;
      //   var isIp = IPAddress.TryParse(value.ToString(), out val);
      //   var errorMessage = "";
      //   var variable = "";

      //   if (!isIp)
      //   {
      //      switch (propertyName)
      //      {
      //         case "GateWay":
      //            variable = "GateWay";
      //            errorMessage = "error";
      //            break;
      //         case "Ip":
      //            variable = "Ip";
      //            errorMessage = "error";
      //            break;
      //         case "PrimaryDns":
      //            variable = "PrimaryDns";
      //            errorMessage = "error";
      //            break;
      //         case "SecondaryDns":
      //            variable = "SecondaryDns";
      //            errorMessage = "error";
      //            break;
      //         case "SubnetMask":
      //            variable = "SubnetMask";
      //            errorMessage = "error";
      //            break;
      //      }
      //   }
      //   else
      //   {
      //      return base.AdditionalValidationRules(value, propertyName);
      //   }    

      //   var valRes = new List<ValidationResult>
      //   {
      //      new ValidationResult(errorMessage, new[] { variable })      
      //   };
      //   return valRes;
      //}

      #endregion
   }
}
