﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Bat1.Forms.Bat2.ModelViews.Presentation;
using Bat1.Properties;
using Bat2Library;
using Bat2Library.Connection.Interface.Domain;
using BatLibrary;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat1.Forms.Bat2.ModelViews.Applications
{
   public class WeighingConfigurationViewModel : ObservableObject
   {
      #region Private fields

      private const int DECIMAL_HIGH = 1000;
      private const double DECIMAL_LOW = 0.001;
      private const int BYTE_MAX = byte.MaxValue;
      private const int DEFAULT_CURVES_COUNT = 1;
      private int correctionCurveIndexWithDefault;
      private int maleGrowthCurveIndexWithDefault;
      private int femaleGrowthCurveIndexWithDefault;
      private int weighingPlanIndexWithDefault;

      private string name;
      private string flock;
      private WeighingConfigurationMenuMaskE menuMask;
      private short initialDay;
      private int correctionCurveIndex;
      private int predefinedIndex;
      private DateTime dayStart;
      private int maleInitialWeight;
      private int maleGrowthCurveIndex;
      private int femaleInitialWeight;
      private int femaleGrowthCurveIndex;
      private OnlineAdjustmentE adjustTargetWeights;
      private SexE sex;
      private SexDifferentiationE sexDifferentiation;
      private PredictionGrowthE growth;
      private PredictionModeE mode;
      private byte filter;
      private byte stabilizationTime;
      private byte stabilizationRange;
      private PlatformStepModeE step;
      private byte maleMarginAbove;
      private byte maleMarginBelow;
      private byte femaleMarginAbove;
      private byte femaleMarginBelow;
      private byte shortPeriod;
      private StatisticShortTypeE shortType;
      private byte uniformityRange;
      private short histogramStep;
      private byte histogramRange;
      private HistogramModeE histogramMode;
      private bool planning;
      private int weighingPlanIndex;

      //Support for WeighingDaysMaskE enum multiselect
      private ObservableCollection<WeighingConfigurationMenuMaskE> selectedMenuMask;
      private bool recalcCollection;

      private ObservableCollection<string> correctionCurvesNames;
      private ObservableCollection<string> growthCurvesNames;
      private ObservableCollection<string> weighingPlansNames;

      private WeightUnitsE units;
      private double divisionMale;
      private double divisionFemale;

      private StatisticsViewModel statisticsVm;
      private WeighingCapacity weighingCapacity;

      #endregion

      #region Public interface

      #region Constructors

      public WeighingConfigurationViewModel(IView view,
                                            WeighingConfiguration conf, 
                                            IEnumerable<string> corrections,
                                            IEnumerable<string> growths,
                                            IEnumerable<string> plans,
                                            Bat1Library.UnitsConfig unitsConfig)
      {
         SelectedMenuMask = new ObservableCollection<WeighingConfigurationMenuMaskE>();
         SelectedMenuMask.CollectionChanged += SelectedMenuMaskChanged;
         recalcCollection = true;

         statisticsVm = new StatisticsViewModel(((WeighingConfigurationView)view).Statistics, conf, unitsConfig);
         Name = conf.Name;
         Flock = conf.Flock;
         MenuMask = conf.MenuMask;
         InitialDay = conf.InitialDay;
         PredefinedIndex = conf.PredefinedIndex;
         DayStart = conf.DayStart;
         MaleInitialWeight = conf.MaleInitialWeight;
         FemaleInitialWeight = conf.FemaleInitialWeight;
         AdjustTargetWeights = conf.AdjustTargetWeights;
         Sex = conf.Sex;
         SexDifferentiation = conf.SexDifferentiation;
         Growth = conf.Growth;
         Mode = (PredictionModeE)conf.Mode;
         Filter = conf.Filter;
         StabilizationTime = conf.StabilizationTime;
         StabilizationRange = conf.StabilizationRange;
         Step = conf.Step;
         MaleMarginAbove = conf.MaleMarginAbove;
         MaleMarginBelow = conf.MaleMarginBelow;
         FemaleMarginAbove = conf.FemaleMarginAbove;
         FemaleMarginBelow = conf.FemaleMarginBelow;
         ShortPeriod = conf.ShortPeriod;
         ShortType = conf.ShortType;
         UniformityRange = conf.UniformityRange;
         HistogramStep = conf.HistogramStep;
         HistogramRange = conf.HistogramRange;
         HistogramMode = conf.HistogramMode;
         Planning = conf.Planning;
         Units = (WeightUnitsE)unitsConfig.Units;
         WeighingCapacity = unitsConfig.WeighingCapacity;
         DivisionMale = MaleInitialWeight;
         DivisionFemale = FemaleInitialWeight;

         CorrectionCurveIndexWithDefault = conf.CorrectionCurveIndex + DEFAULT_CURVES_COUNT;
         MaleGrowthCurveIndexWithDefault = conf.MaleGrowthCurveIndex + DEFAULT_CURVES_COUNT;
         FemaleGrowthCurveIndexWithDefault = conf.FemaleGrowthCurveIndex + DEFAULT_CURVES_COUNT;
         WeighingPlanIndexWithDefault = conf.WeighingPlanIndex + DEFAULT_CURVES_COUNT;

         CorrectionCurvesNames = new ObservableCollection<string>(corrections);      
         GrowthCurvesNames =  new ObservableCollection<string>(growths);
         WeighingPlansNames = new ObservableCollection<string>(plans);

         CorrectionCurvesNames.Insert(0, Resources.Empty);
         GrowthCurvesNames.Insert(0, Resources.Empty);
         WeighingPlansNames.Insert(0, Resources.Empty);

         view.DataContext = this;
      }    

      #endregion

      #region Properties

      /// <summary>
      /// Menu mask as collection
      /// </summary>
      public ObservableCollection<WeighingConfigurationMenuMaskE> SelectedMenuMask
      {
         get { return selectedMenuMask; }
         private set { SetProperty(ref selectedMenuMask, value); }
      }

      public WeighingConfigurationMenuMaskE MenuMask
      {
         get { return menuMask; }
         set
         {
            //menuMask = value;
            SetProperty(ref menuMask, value); 

            if (recalcCollection)
            {
               SelectedMenuMask.CollectionChanged -= SelectedMenuMaskChanged;
               SelectedMenuMask.Clear();
               foreach (WeighingConfigurationMenuMaskE ev in typeof(WeighingConfigurationMenuMaskE).GetEnumValues())
               {
                  if (MenuMask.HasFlag(ev))
                  {
                     SelectedMenuMask.Add(ev);
                  }
               }
               SelectedMenuMask.CollectionChanged += SelectedMenuMaskChanged;
            }
         }
      }

      public int CorrectionCurveIndex
      {
         get { return GetIndex(CorrectionCurveIndexWithDefault); }      
      }

      public int CorrectionCurveIndexWithDefault
      {
         get { return correctionCurveIndexWithDefault; }
         set { SetProperty(ref correctionCurveIndexWithDefault, value); }
      }

      public int MaleGrowthCurveIndex
      {
         get { return GetIndex(MaleGrowthCurveIndexWithDefault); }
      }

      public int MaleGrowthCurveIndexWithDefault
      {
         get { return maleGrowthCurveIndexWithDefault; }
         set { SetProperty(ref maleGrowthCurveIndexWithDefault, value); }
      }

      public int FemaleGrowthCurveIndex
      {
         get { return GetIndex(FemaleGrowthCurveIndexWithDefault); }       
      }

      public int FemaleGrowthCurveIndexWithDefault
      {
         get { return femaleGrowthCurveIndexWithDefault; }
         set { SetProperty(ref femaleGrowthCurveIndexWithDefault, value); }
      }

      public int WeighingPlanIndex
      {
         get { return GetIndex(WeighingPlanIndexWithDefault); }        
      }

      public int WeighingPlanIndexWithDefault
      {
         get { return weighingPlanIndexWithDefault; }
         set { SetProperty(ref weighingPlanIndexWithDefault, value); }
      }

      public ObservableCollection<string> CorrectionCurvesNames { get { return correctionCurvesNames; } set { SetProperty(ref correctionCurvesNames, value); } }
      public ObservableCollection<string> GrowthCurvesNames { get { return growthCurvesNames; } set { SetProperty(ref growthCurvesNames, value); } }
      public ObservableCollection<string> WeighingPlansNames { get { return weighingPlansNames; } set { SetProperty(ref weighingPlansNames, value); } }

      public string Name { get { return name; } set { SetProperty(ref name, value); } }
      public string Flock { get { return flock; } set { SetProperty(ref flock, value); } }
      public short InitialDay { get { return initialDay; } set { SetProperty(ref initialDay, value); } }
      public int PredefinedIndex { get { return predefinedIndex; } set { SetProperty(ref predefinedIndex, value); } }
      public DateTime DayStart { get { return dayStart; } set { SetProperty(ref dayStart, value); } }
      public int MaleInitialWeight { get { return maleInitialWeight; } set { SetProperty(ref maleInitialWeight, value); } }
      public int FemaleInitialWeight { get { return femaleInitialWeight; } set { SetProperty(ref femaleInitialWeight, value); } }
      public OnlineAdjustmentE AdjustTargetWeights { get { return adjustTargetWeights; } set { SetProperty(ref adjustTargetWeights, value); } }
      public SexE Sex { get { return sex; } set { SetProperty(ref sex, value); } }
      public SexDifferentiationE SexDifferentiation { get { return sexDifferentiation; } set { SetProperty(ref sexDifferentiation, value); } }
      public PredictionGrowthE Growth { get { return growth; } set { SetProperty(ref growth, value); } }
      public PredictionModeE Mode { get { return mode; } set { SetProperty(ref mode, value); } }
      public byte Filter { get { return filter; } set { SetProperty(ref filter, value); } }
      public byte StabilizationTime { get { return stabilizationTime; } set { SetProperty(ref stabilizationTime, value); } }
      public byte StabilizationRange { get { return stabilizationRange; } set { SetProperty(ref stabilizationRange, value); } }
      public PlatformStepModeE Step { get { return step; } set { SetProperty(ref step, value); } }
      public byte MaleMarginAbove { get { return maleMarginAbove; } set { SetProperty(ref maleMarginAbove, value); } }
      public byte MaleMarginBelow { get { return maleMarginBelow; } set { SetProperty(ref maleMarginBelow, value); } }
      public byte FemaleMarginAbove { get { return femaleMarginAbove; } set { SetProperty(ref femaleMarginAbove, value); } }
      public byte FemaleMarginBelow { get { return femaleMarginBelow; } set { SetProperty(ref femaleMarginBelow, value); } }
      public byte ShortPeriod { get { return shortPeriod; } set { SetProperty(ref shortPeriod, value); } }
      public StatisticShortTypeE ShortType { get { return shortType; } set { SetProperty(ref shortType, value); } }
      public byte UniformityRange { get { return uniformityRange; } set { SetProperty(ref uniformityRange, value); } }
      public short HistogramStep { get { return histogramStep; } set { SetProperty(ref histogramStep, value); } }
      public byte HistogramRange { get { return histogramRange; } set { SetProperty(ref histogramRange, value); } }
      public HistogramModeE HistogramMode { get { return histogramMode; } set { SetProperty(ref histogramMode, value); } }
      public bool Planning { get { return planning; } set { SetProperty(ref planning, value); } }

      public WeighingCapacity WeighingCapacity
      {
         get
         {
            return weighingCapacity;
         }
         set
         {
            SetProperty(ref weighingCapacity, value);
            statisticsVm.WeighingCapacity = value;
         }
      }

      public WeightUnitsE Units
      {
         get
         {
            return units;
         }
         set
         {       
            DivisionFemale = ConvertWeight.Convert(DivisionFemale, (Units)units, (Units)value);
            DivisionMale = ConvertWeight.Convert(DivisionMale, (Units)units, (Units)value);

            SetProperty(ref units, value);
            statisticsVm.Units = value;
         }
      }

      public double DivisionMale
      {
         get
         {
            return divisionMale;
         }
         set
         {
            SetProperty(ref divisionMale, value);
            var newValue = value;
            if (Units != WeightUnitsE.WEIGHT_UNITS_G)
            {
               newValue *= DECIMAL_HIGH;
            }
            MaleInitialWeight = (int)newValue;
         }
      }

      public double DivisionFemale
      {
         get
         {
            return divisionFemale;
         }
         set
         {
            SetProperty(ref divisionFemale, value);
            var newValue = value;
            if (Units != WeightUnitsE.WEIGHT_UNITS_G)
            {
               newValue *= DECIMAL_HIGH;
            }
            FemaleInitialWeight = (int)newValue;
         }
      }

      #endregion 

      #endregion

      #region Private Helpers

      private void SelectedMenuMaskChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         var mask = default(WeighingConfigurationMenuMaskE);
         foreach (var ev in SelectedMenuMask)
         {
            mask |= ev;
         }
         recalcCollection = false;
         MenuMask = mask;
         recalcCollection = true;
      }

      /// <summary>
      /// Edit curve index without default curves to type byte.
      /// </summary>
      /// <param name="value">curve index with default curves</param>
      /// <returns>curve index</returns>
      private int GetIndex(int value)
      {
         var index = value - DEFAULT_CURVES_COUNT;
         if (index > BYTE_MAX || index == -1)
         {
            index = BYTE_MAX;
         }
         return index;
      }

      #endregion
   }
}
