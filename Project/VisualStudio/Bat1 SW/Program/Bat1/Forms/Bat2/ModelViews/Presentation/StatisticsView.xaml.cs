﻿using System.Windows;
using Bat1.Forms.Bat2.ModelViews.Applications;
using Desktop.Wpf.Presentation;

namespace Bat1.Forms.Bat2.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for StatisticsView.xaml
   /// </summary>
   public partial class StatisticsView : IStatisticsView
   {
      private StatisticsViewModel vm;

      public StatisticsView()
      {
         InitializeComponent();     
      }

      public StatisticsView(bool readOnly)
      {
         InitializeComponent();     
         HourlyPanel.Visibility = Visibility.Collapsed;
         Margin = new Thickness(0,5,0,0);
         Layout.IsEnabled = !readOnly;
      }


      public void Show()
      {
         Visibility = Visibility.Visible;
      }

      public void Hide()
      {
         Visibility = Visibility.Collapsed;
      }
   }

   public interface IStatisticsView : IView
   {
   }
}
