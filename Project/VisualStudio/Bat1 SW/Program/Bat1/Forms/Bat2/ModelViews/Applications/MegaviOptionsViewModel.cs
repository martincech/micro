﻿using Bat2Library.Connection.Interface.Domain;
using Utilities.Observable;

namespace Bat1.Forms.Bat2.ModelViews.Applications
{
   public class MegaviOptionsViewModel : ObservableObject
   {
      #region Private fields

      private byte code;
      private byte address;
      private byte deviceAddress;

      #endregion

      #region Public interface

      #region Constructors

      public MegaviOptionsViewModel(MegaviOptions option)
      {
         Address = option.Address;
         Code = option.Code;
         DeviceAddress = option.DeviceAddress;
      }

      #endregion

      #region Properties

      public byte Code { get { return code; } set { SetProperty(ref code, value); } }
      public byte Address { get { return address; } set { SetProperty(ref address, value); } }
      public byte DeviceAddress { get { return deviceAddress; } set { SetProperty(ref deviceAddress, value); } }

      #endregion 

      #endregion
   }
}
