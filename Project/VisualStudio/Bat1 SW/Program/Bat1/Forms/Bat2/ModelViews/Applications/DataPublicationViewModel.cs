﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Bat2Library;
using Bat2Library.Connection.Interface.Domain;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat1.Forms.Bat2.ModelViews.Applications
{
   public class DataPublicationViewModel : ValidatableObservableObject
   {
      #region Private fields

      private DateTime sendAt;
      private ushort acceleratedPeriod;
      private ushort accelerateFromDay;
      private ushort period;
      private ushort startFromDay;

      private DataPublicationInterfaceE @interface;
      private string password;
      private string url;
      private string username;

      #endregion

      #region Public interface

      #region Constructors

      public DataPublicationViewModel(IView view, DataPublication data)
      {
         SendAt = data.SendAt;
         AcceleratedPeriod = data.AcceleratedPeriod;
         AccelerateFromDay = data.AccelerateFromDay;
         Period = data.Period;
         StartFromDay = data.StartFromDay;
         Interface = data.Interface;
         Password = data.Password;
         Url = data.Url;
         Username = data.Username;

         view.DataContext = this;
      }

      #endregion

      #region Properties

      /// <summary>
      /// Send every period at this time
      /// </summary>
      public DateTime SendAt { get { return sendAt; } set { SetProperty(ref sendAt, value); } }
      /// <summary>
      /// Accelerate day period, greather than <see cref="Period"/>
      /// </summary>
      public ushort AcceleratedPeriod { get { return acceleratedPeriod; } set { SetPropertyAndValidate(ref acceleratedPeriod, value); } }
      /// <summary>
      /// Start faster period from this day
      /// </summary>
      public ushort AccelerateFromDay { get { return accelerateFromDay; } set { SetPropertyAndValidate(ref accelerateFromDay, value); } }
      /// <summary>
      /// Normal day period of sending, lower than <see cref="AcceleratedPeriod"/>
      /// </summary>
      public ushort Period { get { return period; } set { SetPropertyAndValidate(ref period, value); } }
      /// <summary>
      /// Start sending statistics from this day number
      /// </summary>
      public ushort StartFromDay { get { return startFromDay; } set { SetPropertyAndValidate(ref startFromDay, value); } }

      public DataPublicationInterfaceE Interface { get { return @interface; } set { SetProperty(ref @interface, value); } }

      public string Password
      {
         get
         {
            return password;
         }
         set
         {
            SetProperty(ref password, value);
         }
      }

      public string Url { get { return url; } set { SetProperty(ref url, value); } }
      public string Username { get { return username; } set { SetProperty(ref username, value); } }

      #endregion 

      #endregion

      #region Private helpers

      protected override IEnumerable<ValidationResult> AdditionalValidationRules(object value, string propertyName)
      {
         int val;
         Int32.TryParse(value.ToString(), out val);
         string errorMessage = "";
         IEnumerable<string> objects = new[] {""};

         if (propertyName.Equals("Period") && val <= 0)
         {
            objects = new[] {"Period"};
            errorMessage = "error"; /*Resources.FromBeforeTo*/          
         }

         if (propertyName.Equals("AcceleratedPeriod") && val <= 0)
         {
            objects = new[] { "AcceleratedPeriod" };
            errorMessage = "error";
         }

         if (propertyName.Equals("AccelerateFromDay") && val < StartFromDay)
         {
            objects = new[] { "AccelerateFromDay" };
            errorMessage = "error";          
         }

         if (propertyName.Equals("StartFromDay") && val > AccelerateFromDay)
         {
            objects = new[] { "StartFromDay" };
            errorMessage = "error";
         }

         if (!errorMessage.Equals(""))
         {
            var valRes = new List<ValidationResult>
            {
               new ValidationResult(errorMessage, objects)      
            };
            return valRes;
         }

         return base.AdditionalValidationRules(value, propertyName);                       
      }

      #endregion
   }
}
