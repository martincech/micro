﻿using System.Globalization;
using Bat2Library;
using DataContext;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat1.Forms.Bat2.ModelViews.Applications
{
   public class CountryViewModel : ObservableObject
   {
      #region Private fields

      /// <summary>
      /// Date separators the user can choose from
      /// </summary>
      private char[] dateSeparators;

      /// <summary>
      /// Time separators the user can choose from
      /// </summary>
      private char[] timeSeparatos;

      private CountryE countryCode;
      private LanguageE language;
      private CodePageE codePage;
      private DateFormatE dateFormat;
      private char dateSeparator1;
      private char dateSeparator2;
      private TimeFormatE timeFormat;
      private char timeSeparator;
      private DaylightSavingE daylightSaving;

      private string dateExample;
      private string timeExample;

      #endregion

      #region Public interface

      #region Constructors

      public CountryViewModel(IView view, ScaleConfig device)
      {
         CountryCode = (CountryE)device.Country;
         Language = (LanguageE)device.LanguageInDatabase;
         DateFormat = (DateFormatE)device.DateFormat;
         DateSeparator1 = device.DateSeparator1;
         DateSeparator2 = device.DateSeparator2;
         TimeFormat = (TimeFormatE)device.TimeFormat;
         TimeSeparator = device.TimeSeparator;
         DaylightSaving = (DaylightSavingE)device.DaylightSavingMode;

         UpdateDateExample();
         UpdateTimeExample();

         TimeSeparators = new[] { ':', '.' };
         DateSeparators = new[] { '.', '-', '/', ',', ' ' };

         view.DataContext = this;

         if ((device as Bat2Scale) != null)
         {
            CodePage = ((Bat2Scale)device).CodePage;   
         }
      }

      #endregion

      #region Properties

      public char[] TimeSeparators
      {
         get { return timeSeparatos; }
         set { SetProperty(ref timeSeparatos, value); }
      }

      public char[] DateSeparators
      {
         get { return dateSeparators; }
         set { SetProperty(ref dateSeparators, value); }
      }

      public DaylightSavingE DaylightSaving
      {
         get { return daylightSaving; }
         set { SetProperty(ref daylightSaving, value); }     
      }

      public TimeFormatE TimeFormat
      {
         get { return timeFormat; }
         set
         {
            SetProperty(ref timeFormat, value); 
            UpdateTimeExample();
         }     
      }

      public char DateSeparator2
      {
         get { return dateSeparator2; }
         set
         {
            SetProperty(ref dateSeparator2, value);
            UpdateDateExample();          
         }
      }

      public char DateSeparator1
      {
         get { return dateSeparator1; }
         set
         {
            SetProperty(ref dateSeparator1, value);
            UpdateDateExample();         
         }
      }

      public DateFormatE DateFormat
      {
         get { return dateFormat; }
         set
         {
            SetProperty(ref dateFormat, value); 
            UpdateDateExample();
         }
      }

      public CountryE CountryCode
      {
         get { return countryCode; }
         set { SetProperty(ref countryCode, value); }
      }

      public LanguageE Language
      {
         get { return language; }
         set { SetProperty(ref language, value); }
      }

      public CodePageE CodePage
      {
         get { return codePage; }
         set { SetProperty(ref codePage, value); }
      }

      public char TimeSeparator
      {
         get { return timeSeparator; }
         set
         {
            SetProperty(ref timeSeparator, value);         
            UpdateTimeExample();
         }
      }

      #endregion

      /// <summary>
      /// Example of date with current settings.
      /// </summary>
      public string DateFormater
      {
         get { return dateExample; }
         private set
         {
            SetProperty(ref dateExample, value);
            RaisePropertyChanged(() => DateTimeFormater);
         }
      }

      /// <summary>
      /// Example of time with current settings.
      /// </summary>
      public string TimeFormater
      {
         get { return timeExample; }
         private set
         {
            SetProperty(ref timeExample, value);
            RaisePropertyChanged(() => DateTimeFormater);
         }
      }

      /// <summary>
      /// Concatenation of
      /// <see cref="DateFormater"/> <see cref="TimeFormater"/> with ' '(space) in between
      ///  </summary>
      public string DateTimeFormater
      {
         get { return dateExample + " " + timeExample; }
      }

      #endregion

      #region Private helpers

      /// <summary>
      /// Update example date string
      /// </summary>
      private void UpdateDateExample()
      {        
         var fSep = "'" + DateSeparator1.ToString(CultureInfo.InvariantCulture) + "'";
         var sSep = "'" + DateSeparator2.ToString(CultureInfo.InvariantCulture) + "'";
         switch (DateFormat)
         {
            case DateFormatE.DATE_FORMAT_DDMMYYYY:
               DateFormater = "dd" + fSep + "MM" + sSep + "yy";
               break;
            case DateFormatE.DATE_FORMAT_MMDDYYYY:
               DateFormater = "MM" + fSep + "dd" + sSep + "yy";
               break;
            case DateFormatE.DATE_FORMAT_YYYYMMDD:
               DateFormater = "yy" + fSep + "MM" + sSep + "dd";
               break;
            case DateFormatE.DATE_FORMAT_YYYYDDMM:
               DateFormater = "yy" + fSep + "dd" + sSep + "MM";
               break;
            case DateFormatE.DATE_FORMAT_DDMMMYYYY:
               DateFormater = "dd" + fSep + "MMM" + sSep + "yy";
               break;
            case DateFormatE.DATE_FORMAT_MMMDDYYYY:
               DateFormater = "MMM" + fSep + "dd" + sSep + "yy";
               break;
            case DateFormatE.DATE_FORMAT_YYYYMMMDD:
               DateFormater = "yy" + fSep + "MMM" + sSep + "dd";
               break;
            case DateFormatE.DATE_FORMAT_YYYYDDMMM:
               DateFormater = "yy" + fSep + "dd" + sSep + "MMM";
               break;
         }
      }

      /// <summary>
      /// update example time string
      /// </summary>
      private void UpdateTimeExample()
      {       
         var sep = "'" + TimeSeparator.ToString(CultureInfo.InvariantCulture) + "'";
         switch (TimeFormat)
         {
            case TimeFormatE.TIME_FORMAT_12:
               TimeFormater = "hh" + sep + "mmtt";
               break;
            case TimeFormatE.TIME_FORMAT_24:
               TimeFormater = "HH" + sep + "mm";
               break;
         }
      }

      #endregion
   }
}
