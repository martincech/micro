﻿using Bat2Library;
using Bat2Library.Connection.Interface.Domain;
using Utilities.Observable;

namespace Bat1.Forms.Bat2.ModelViews.Applications
{
   public class ModbusOptionsViewModel : ObservableObject
   {
      #region Private fields

      private short replyDelay;
      private ModbusParityE parity;
      private ModbusDataBitsE dataBits;
      private int baudRate;
      private byte address;
      private byte deviceAddress;
      private ModbusTransmissionModeE mode;

      #endregion

      #region Public interface

      #region Constructors

      public ModbusOptionsViewModel(ModbusOptions option)
      {
         Address = option.Address;
         BaudRate = option.BaudRate;
         DataBits = option.DataBits;
         Address = option.Address;
         DeviceAddress = option.DeviceAddress;
         Mode = option.Mode;
         Parity = option.Parity;
         ReplyDelay = option.ReplyDelay;
      }

      #endregion

      #region Properties

      public short ReplyDelay { get { return replyDelay; } set { SetProperty(ref replyDelay, value); } }
      public ModbusParityE Parity { get { return parity; } set { SetProperty(ref parity, value); } }
      public ModbusDataBitsE DataBits { get { return dataBits; } set { SetProperty(ref dataBits, value); } }
      public int BaudRate { get { return baudRate; } set { SetProperty(ref baudRate, value); } }
      public byte Address { get { return address; } set { SetProperty(ref address, value); } }
      public byte DeviceAddress { get { return deviceAddress; } set { SetProperty(ref deviceAddress, value); } }
      public ModbusTransmissionModeE Mode { get { return mode; } set { SetProperty(ref mode, value); } }    

      #endregion     

      #endregion
   }
}
