﻿using System.Windows;
using Bat1.Forms.Bat2.ModelViews.Applications;
using Desktop.Wpf.Presentation;

namespace Bat1.Forms.Bat2.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for GsmMessageView.xaml
   /// </summary>
   public partial class GsmMessageView : IGsmMessageView
   {
      private GsmMessageViewModel vm;

      public GsmMessageView()
      {
         InitializeComponent();          
      }


      public void Show()
      {
         Visibility = Visibility.Visible;
      }

      public void Hide()
      {
         Visibility = Visibility.Collapsed;
      }

      private void GsmMessageView_OnLoaded(object sender, RoutedEventArgs e)
      {
         vm = DataContext as GsmMessageViewModel;
         if (vm == null) return;

         var time = new TimeView { DataContext = vm.SwitchOnTimes };
         TimeStackPanel.Children.Add(time);
      }
   }

   public interface IGsmMessageView : IView
   {
   }
}
