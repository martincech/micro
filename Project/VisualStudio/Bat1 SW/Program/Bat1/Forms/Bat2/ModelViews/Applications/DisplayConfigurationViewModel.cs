﻿using Bat2Library;
using DataContext;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat1.Forms.Bat2.ModelViews.Applications
{
   public class DisplayConfigurationViewModel : ObservableObject
   {
      #region Private fields

      private byte contrast;
      private DisplayModeE mode;
      private bool savePower;

      private short backlightDuration;
      private byte backlightIntensity;
      private BacklightModeE backlightMode;

      #endregion

      #region Public interface

      #region Constructors

      public DisplayConfigurationViewModel(IView view, ScaleConfig device)
      {
         Contrast = (byte)device.Display.Contrast;
         Mode = (DisplayModeE)device.Display.Mode;
         BacklightDuration = (short)device.Display.Backlight.Duration;
         BacklightIntensity = (byte)device.Display.Backlight.Intensity;
         BacklightMode = (BacklightModeE)device.Display.Backlight.Mode;

         view.DataContext = this;
      
         if ((device as Bat2Scale) != null)
         {
            SavePower = ((Bat2Scale)device).SavePower;   
         }       
      } 
    
      #endregion

      #region Properties

      public byte Contrast { get { return contrast; } set { SetProperty(ref contrast, value); } }
      public DisplayModeE Mode { get { return mode; } set { SetProperty(ref mode, value); } }
      public bool SavePower { get { return savePower; } set { SetProperty(ref savePower, value); } }

      public short BacklightDuration { get { return backlightDuration; } set { SetProperty(ref backlightDuration, value); } }
      public byte BacklightIntensity { get { return backlightIntensity; } set { SetProperty(ref backlightIntensity, value); } }
      public BacklightModeE BacklightMode { get { return backlightMode; } set { SetProperty(ref backlightMode, value); } }

      #endregion 

      #endregion
   }
}
