using System;
using System.Collections.Generic;
using DataContext;

namespace Bat1
{
   /// <summary>
   /// Filter during weighing selection
   /// </summary>
   public class WeighingSearchInfoFilter {
      /// <summary>
      /// Complete sorted list of weighing info
      /// </summary>
      private List<WeighingSearchInfo> weighingInfoList;

      /// <summary>
      /// List of weighing info that fit the filter
      /// </summary>
      public  List<WeighingSearchInfo> FilteredInfoList { get { return filteredInfoList; } }
      private List<WeighingSearchInfo> filteredInfoList = new List<WeighingSearchInfo>();

      /// <summary>
      /// List of all files
      /// </summary>
      public  List<string> FilesList { get { return filesList; } }
      private List<string> filesList = new List<string>();

      /// <summary>
      /// List of all scales
      /// </summary>
      public  List<string> ScalesList { get { return scalesList; } }
      private List<string> scalesList = new List<string>();

      /// <summary>
      /// File name for filtration (or NULL for all files)
      /// </summary>
      public  string FileFilter { get { return fileFilter; } set { fileFilter = value; } }
      private string fileFilter = null;

      /// <summary>
      /// Scale name for filtration (or NULL for all scales)
      /// </summary>
      public  string ScaleFilter { get { return scaleFilter; } set { scaleFilter = value; } }
      private string scaleFilter = null;

      /// <summary>
      /// Minimum date of weighing for filtration (or DateTime.MinValue when not limited)
      /// </summary>
      public  DateTime MinDateFilter { get { return minDateFilter.Date; } set { minDateFilter = value.Date; } }
      private DateTime minDateFilter = DateTime.MinValue.Date;

      /// <summary>
      /// Maximum date of weighing for filtration (or DateTime.MaxValue when not limited)
      /// </summary>
      public  DateTime MaxDateFilter { get { return maxDateFilter.Date; } set { maxDateFilter = value.Date; } }
      private DateTime maxDateFilter = DateTime.MaxValue.Date;

      /// <summary>
      /// Load weighing info
      /// </summary>
      /// <param name="weighingInfoList">Complete sorted list of weighing info</param>
      public void Load(List<WeighingSearchInfo> weighingInfoList) {
         this.weighingInfoList = weighingInfoList;
            
         // Default je filtr vypnuty, zverejnim vsechny polozky
         filteredInfoList.AddRange(weighingInfoList);

         // Sestavim seznamy souboru a vah
         filesList.Clear();
         scalesList.Clear();
         foreach (var weighingInfo in weighingInfoList) {
            if (!filesList.Exists(delegate(string s) { return s == weighingInfo.FileName; })) {
               filesList.Add(weighingInfo.FileName);
            }
            if (!scalesList.Exists(delegate(string s) { return s == weighingInfo.ScaleName; })) {
               scalesList.Add(weighingInfo.ScaleName);
            }
         }

         // Na zaver seznamy setridim podle abecedy
         filesList.Sort();
         scalesList.Sort();
      }

      /// <summary>
      /// Execute the filter and fill FilteredInfoList
      /// </summary>
      public void Execute() {
         filteredInfoList.Clear();
         foreach (var weighingInfo in weighingInfoList) {
            if (MinDateFilter != DateTime.MinValue.Date && weighingInfo.MinDateTime.Date < MinDateFilter) {
               // Datum posledniho vzorku je mensi nez minimalni datum ve filtru
               continue;
            }

            if (MaxDateFilter != DateTime.MaxValue.Date && weighingInfo.MinDateTime.Date > MaxDateFilter)
            {
               // Datum prvniho vzorku je vetsi nez maximalni datum ve filtru
               continue;
            }


            if (FileFilter != null && weighingInfo.FileName != FileFilter) {
               continue;
            }
            if (ScaleFilter != null && weighingInfo.ScaleName != ScaleFilter) {
               continue;
            }
            // Vazeni vyhovuje, pridam ho do seznamu
            filteredInfoList.Add(weighingInfo);
         }
      }

   }
}