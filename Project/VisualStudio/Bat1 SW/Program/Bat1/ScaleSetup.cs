﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Bat1.Forms.Bat2.ModelViews.Applications;
using Bat1.Forms.Bat2.ModelViews.Presentation;
using Bat2Library.Connection.Interface.Domain;
using BatLibrary;
using DataContext;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat1
{
   /// <summary>
   /// Create two lists of IView for view scale's configuration.
   /// </summary>
   public class ScaleSetup : ObservableObject
   {
      #region Fields and properties

      public List<IView> Configuration { get; private set; }
      public List<IView> Settings { get; private set; }

      private Bat1Scale scaleBat1;
      private Bat2Scale scaleBat2;
      private bool readOnly;

      private WeightUnitView weightUnitView;
      private WeightUnitViewModel weightUnitVm;

      private WeighingConfigurationView weighingConfigurationView;
      private WeighingConfigurationViewModel weighingConfigurationVm;

      private PasswordView passwordView;
      private PasswordViewModel passwordVm;

      private CountryView countryView;
      private CountryViewModel countryVm;

      private DisplayConfigurationView displayConfigurationView;
      private DisplayConfigurationViewModel displayConfigurationVm;

      private PlatformCalibrationView platformCalibrationView;
      private PlatformCalibrationViewModel platformCalibrationVm;

      private StatisticsView statisticsView;
      private StatisticsViewModel statisticsVm;

      #endregion

      #region Public interface

      #region Constructors

      public ScaleSetup(Bat1Scale scaleBat1, bool readOnly)
      {
         this.scaleBat1 = scaleBat1;
         this.readOnly = readOnly;     
         InitializeBat1ControlsLists();
      }

      public ScaleSetup(Bat2Scale scaleBat2, Bat2DeviceData device)
      {
         this.scaleBat2 = scaleBat2;     
         InitializeBat2ControlsLists(device);
      }

      #endregion

      #endregion

      #region Private helpers

      private void InitializeBat1ControlsLists()
      {
         displayConfigurationView = new DisplayConfigurationView(readOnly);
         displayConfigurationVm = new DisplayConfigurationViewModel(displayConfigurationView, scaleBat1);

         statisticsView = new StatisticsView(readOnly);
         statisticsVm = new StatisticsViewModel(statisticsView, scaleBat1.StatisticConfig, scaleBat1.Units);

         Configuration = new List<IView>
         {
            new UserControlScaleConfigFiles(scaleBat1, Properties.Resources.UserControlScaleConfigFiles, readOnly),
            new UserControlScaleConfigGroups(scaleBat1, Properties.Resources.UserControlScaleConfigGroups, readOnly),
            new UserControlScaleConfigSaving(scaleBat1, readOnly),
            displayConfigurationView,
            new UserControlScaleConfigSounds(scaleBat1, readOnly),
            statisticsView
         };

         weightUnitView = new WeightUnitView(readOnly);
         weightUnitVm = new WeightUnitViewModel(weightUnitView, scaleBat1.Units);
         weightUnitVm.PropertyChanged += WeightUnitVm_PropertyChanged;

         passwordView = new PasswordView(readOnly);
         passwordVm = new PasswordViewModel(passwordView, scaleBat1);

         countryView = new CountryView(readOnly);
         countryVm = new CountryViewModel(countryView, scaleBat1);

         Settings = new List<IView>
         {
            countryView,
            weightUnitView,
            new UserControlScaleConfigPrinter(scaleBat1, readOnly),
            new UserControlScaleConfigPower(scaleBat1, readOnly),
            passwordView
         };
      }

      private void WeightUnitVm_PropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         if (e.PropertyName.Equals("Units"))
         {
            var newUnits = ((WeightUnitViewModel)sender).Units;
            if (scaleBat2 != null)
            {
               weighingConfigurationVm.Units = newUnits;
               platformCalibrationVm.Units = newUnits;
            }
            if (scaleBat1 != null)
            {
               statisticsVm.Units = newUnits;
            }
         }

         if (e.PropertyName.Equals("Capacity"))
         {
            var newCapacity = ((WeightUnitViewModel)sender).Capacity;
            if (scaleBat2 != null)
            {
               weighingConfigurationVm.WeighingCapacity = (WeighingCapacity)newCapacity;              
            }
            if (scaleBat1 != null)
            {
               statisticsVm.WeighingCapacity = (WeighingCapacity)newCapacity;
            }
         }
      }

      private void InitializeBat2ControlsLists(Bat2DeviceData device)
      {
         // Load properties which aren't saved in database
         var correctionCurvesNames = device.CorrectionCurves != null ?
               device.CorrectionCurves.Select(curve => curve.Name).ToList() : new List<string>();
         var growthCurvesNames = device.GrowthCurves != null ?
               device.GrowthCurves.Select(curve => curve.Name).ToList() : new List<string>();
         var weighingPlansNames = device.WeighingPlans != null ?
               device.WeighingPlans.Select(plan => plan.Name).ToList() : new List<string>();

         var rs1 = device.Configuration.Rs485Options[0];
         var rs2 = device.Configuration.Rs485Options[1];

         weightUnitView = new WeightUnitView();
         weightUnitVm = new WeightUnitViewModel(weightUnitView, scaleBat2.Units);
         weightUnitVm.PropertyChanged += WeightUnitVm_PropertyChanged;

         weighingConfigurationView = new WeighingConfigurationView();
         weighingConfigurationVm = new WeighingConfigurationViewModel(weighingConfigurationView, scaleBat2.WeighingConfiguration, correctionCurvesNames, growthCurvesNames,
                  weighingPlansNames, scaleBat2.Units);

         passwordView = new PasswordView();
         passwordVm = new PasswordViewModel(passwordView, scaleBat2);

         countryView = new CountryView();
         countryVm = new CountryViewModel(countryView, scaleBat2);

         displayConfigurationView = new DisplayConfigurationView();
         displayConfigurationVm = new DisplayConfigurationViewModel(displayConfigurationView, scaleBat2);

         platformCalibrationView = new PlatformCalibrationView();
         platformCalibrationVm = new PlatformCalibrationViewModel(platformCalibrationView, scaleBat2.PlatformCalibration, scaleBat2.Units.Units);

         var rs485OptionsListView = new Rs485OptionsListView();
         var rs485OptionsListVm = new Rs485OptionsListViewModel(rs485OptionsListView, rs1, rs2, scaleBat2.ModbusOptions, scaleBat2.MegaviOptions, scaleBat2.DacsOption);

         var cellularDataView = new CellularDataView();
         var cellularDataVm = new CellularDataViewModel(cellularDataView, scaleBat2.CellularData);

         var ethernetView = new EthernetView();
         var ethernetVm = new EthernetViewModel(ethernetView, scaleBat2.Ethernet);

         var dataPublicationView = new DataPublicationView();
         var dataPublicationVm = new DataPublicationViewModel(dataPublicationView, scaleBat2.DataPublication);

         var gsmMessageView = new GsmMessageView();
         var gsmMessageVm = new GsmMessageViewModel(gsmMessageView, device.Configuration.GsmMessage);

         var bat2IdentificationView = new Bat2IdentificationView();
         var bat2IdentificationVm = new Bat2IdentificationViewModel(bat2IdentificationView, scaleBat2);

         Configuration = new List<IView>
         {
            bat2IdentificationView,
            displayConfigurationView,
            gsmMessageView,
            dataPublicationView,
            ethernetView,
            cellularDataView,          
            rs485OptionsListView,
            weighingConfigurationView,         
            platformCalibrationView
         };

         Settings = new List<IView>
         {
            countryView,
            weightUnitView,
            passwordView
         };
      }
      #endregion
   }
}
