using BatLibrary;

namespace Bat1
{
   /// <summary>
   /// Units descriptor
   /// </summary>
   public struct UnitsConfig {
      public int     Range;                      // weighing range
      public Units   Units;                      // weighing units TUnitsEnum
      public int     Decimals;                   // decimals count
      public int     MaxDivision;                // division limit
      public double  Division;                   // scale division
      public WeighingCapacity WeighingCapacity;  // Weighing capacity 30 or 50kg
   }
}