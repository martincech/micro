using Bat1Library;

namespace Bat1
{
   /// <summary>
   /// Print configuration
   /// </summary>
   public struct PrintConfig {
      public int       PaperWidth;                    // Paper width [mm]
      public ComFormat CommunicationFormat;           // Serial format TComFormat enum
      public int       CommunicationSpeed;            // Serial speed [Bd]
   }
}