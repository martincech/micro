﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;
using Bat2Config.Properties;
using Bat2Library.Configuration;
using Bat2Library.Connection.Interface.Domain;
using Desktop.Wpf.Applications;
using Desktop.Wpf.Presentation;
using Utilities.Observable;
using System.IO;
using Bat2Config.Map;
using Utilities.Extensions;

namespace Bat2Config.ViewModel
{
   public class CommandViewModel : ObservableObject
   {
      private ObservableCollection<ModelExport> views;

      private ICommand addCommand;
      private ICommand deleteCommand;
      private ICommand saveCommand;

      private int serialNumber;
      private string name;

      public CommandViewModel(IView view)
      {
         AvailableCommands = new ObservableCollection<CommandParams>()
         {
            new CommandParams(){Title=Resources.CommadStartWeighing, HasTime = true, Time = DateTime.Now, Now = true},
            new CommandParams(){Title=Resources.CommadStopWeighing},
            new CommandParams(){Title=Resources.CommadPauseWeighing},
            new CommandParams(){Title=Resources.CommadReleaseWeighing}
         };

         ExecuteCommands = new ObservableCollection<CommandParams>();
         view.DataContext = this;
      }

      public ObservableCollection<ModelExport> Views
      {
         get { return views; }
         set { SetProperty(ref views, value); }
      }
      public ObservableCollection<CommandParams> AvailableCommands { get; set; }
      public ObservableCollection<CommandParams> ExecuteCommands { get; set; }

      public ICommand AddCommand
      {
         get
         {
            return addCommand ??
                   (addCommand =
                      new RelayCommand(
                         (command) =>
                         {
                            var add = (CommandParams)command;
                            AvailableCommands.Remove(add);
                            ExecuteCommands.Add(add);
                            UpdateCommands();
                         }));
         }
      }

      public ICommand DeleteCommand
      {
         get
         {
            return deleteCommand ?? (deleteCommand = new RelayCommand((command) =>
            {
               var remove = (CommandParams)command;
               AvailableCommands.Add(remove);
               ExecuteCommands.Remove(remove);

               UpdateCommands();
            }));
         }
      }

      private void UpdateCommands()
      {
         ((RelayCommand)AddCommand).RaiseCanExecuteChanged();
         ((RelayCommand)DeleteCommand).RaiseCanExecuteChanged();
         ((RelayCommand)SaveCommand).RaiseCanExecuteChanged();
      }

      public ICommand SaveCommand
      {
         get
         {
            return saveCommand ??
                   (saveCommand =
                      new RelayCommand(
                         () =>
                         {
                            var dialog = new FolderBrowserDialog();
                            DialogResult result = dialog.ShowDialog();
                            if (result == DialogResult.OK)
                            {
                               SaveConfig(dialog.SelectedPath);
                               Process.Start(dialog.SelectedPath);
                            }
                            UpdateCommands();
                         }, () => !string.IsNullOrEmpty(Name)));
         }
      }

      public int SerialNumber
      {
         get { return serialNumber; }
         set
         {
            SetProperty(ref serialNumber, value);
            UpdateCommands();
         }
      }

      public string Name
      {
         get { return name; }
         set
         {
            SetProperty(ref name, value);
            UpdateCommands();
         }
      }

      private void SaveConfig(string path)
      {
         var id = SerialNumber;
         var data = StoreVM.Instance;
         StoreVM.Instance.ConfigurationVM.Bat2IdentificationVM.SerialNumber = SerialNumber;
         StoreVM.Instance.ConfigurationVM.Bat2IdentificationVM.Name = Name;
         FileWriter fw = new FileWriter(path, id);

         Bat2DeviceData toSaveData = new Bat2DeviceData();
         if (ToSave(Properties.Resources.ExportContacts))
         {
            foreach (var contact in data.ContactListVM.ContatList)
            {
               contact.PhoneNumber = contact.PhoneNumber.Trim();
               contact.PhoneNumber = contact.PhoneNumber.StartsWith("+")
                  ? contact.PhoneNumber
                  : "+" + contact.PhoneNumber;
               contact.PhoneNumber = new string(contact.PhoneNumber.Where(c => !Char.IsWhiteSpace(c)).ToArray());
            }
            toSaveData.Contacts = data.ContactListVM.ContatList;
         }

         if (ToSave(Properties.Resources.ExportGrowthC))
         {
            toSaveData.GrowthCurves = data.CurveVM.GrowthCurve.Select(curve => curve.MapFrom()).ToList();
         }

         if (ToSave(Properties.Resources.ExportCorrectionC))
         {
            toSaveData.CorrectionCurves = data.CurveVM.CorrectionCurve.Select(curve => curve.MapFrom()).ToList();
         }
         if (ToSave(Properties.Resources.ExportPredefinedWeighings))
         {
            toSaveData.PredefinedWeighings = data.PredefinedWeigingsVM.PredefinedWeighings.Select(x => x.Map()).ToList();
         }
         if (ToSave(Properties.Resources.ExportWeighingPlans))
         {
            toSaveData.WeighingPlans = data.WeighingPlansVM.WeighignPlans;
         }
         if (ToSave(Properties.Resources.ExportConfig))
         {
            toSaveData.Configuration = new Bat2Library.Connection.Interface.Domain.Configuration()
           {
              CellularData = data.ConfigurationVM.CellularDataModel,
              Country = data.ConfigurationVM.CountryModel,
              DacsOptions = data.ConfigurationVM.DacsOptionsModel,
              DataPublication = data.ConfigurationVM.DataPublicationModel,
              DeviceInfo = data.ConfigurationVM.DeviceInfoModel,
              DisplayConfiguration = data.ConfigurationVM.DisplayConfigurationModel,
              Ethernet = data.ConfigurationVM.EthernetModel,
              GsmMessage = data.ConfigurationVM.GsmMessageModel,
              MegaviOptions = data.ConfigurationVM.MegaviOptionsModel,
              ModbusOptions = data.ConfigurationVM.ModbusOptionsModel,
              Rs485Options = data.ConfigurationVM.Rs485OptionsModel.ToList(),
              VersionInfo = data.ConfigurationVM.Bat2IdentificationVM.Map(),
              WeighingConfiguration = data.ConfigurationVM.WeighingConfigurationModel,
              WeightUnits = data.ConfigurationVM.WeightUnitModel
           };
         }

         fw.SaveAll(toSaveData);

         foreach (var command in ExecuteCommands)
         {

            if (command.Title == Resources.CommadStartWeighing)
            {
               if (command.Now)
               {
                  command.Time = null;
               }

               fw.SaveAction_WeighingStart(command.Time);
            }

            if (command.Title == Resources.CommadStopWeighing)
            {
               fw.SaveAction_WeighingStop();
            }

            if (command.Title == Resources.CommadPauseWeighing)
            {
               fw.SaveAction_WeighingSuspend();
            }

            if (command.Title == Resources.CommadReleaseWeighing)
            {
               fw.SaveAction_WeighingRelease();
            }
         }
      }

      private bool ToSave(string viewName)
      {
         return Views.Any(x => x.View == viewName && x.Export);
      }
   }

   public class CommandParams
   {
      public string Title { get; set; }
      public bool HasTime { get; set; }
      public DateTime? Time { get; set; }
      public bool Now { get; set; }
   }

   public class ModelExport
   {
      public string View { get; set; }
      public bool Export { get; set; }
   }
}
