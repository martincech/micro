using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Desktop.Wpf.Applications;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat2Config.ViewModel.Shared
{
   public class NavigationViewModel : ObservableObject
   {

      private ObservableCollection<Navigation> navigationList;
      private Navigation activeItem;

      private ICommand nextViewCommand;
      private ICommand prevViewCommand;

      public NavigationViewModel()
      {
         NavigationList = new ObservableCollection<Navigation>();
      }


      public ObservableCollection<Navigation> NavigationList
      {
         get { return navigationList; }
         set { SetProperty(ref navigationList, value); }
      }

      public Navigation ActiveItem
      {
         get { return activeItem; }
         set
         {
            SetProperty(ref activeItem, value);
            RaisePropertyChanged("NextItem");
            RaisePropertyChanged("PrevItem");
            UpdateCommnads();
         }
      }

      public Navigation NextItem
      {
         get
         {
            var index = navigationList.IndexOf(ActiveItem);
            if (index >= 0 && index < navigationList.Count - 1)
            {
               return navigationList[index + 1];
            }
            return null;
         }
      }

      public Navigation PrevItem
      {
         get
         {
            var index = navigationList.IndexOf(ActiveItem);
            if (index >= 1)
            {
               return navigationList[index - 1];
            }
            return null;
         }
      }

      public ICommand NextViewCommand
      {
         get
         {
            return nextViewCommand ?? (nextViewCommand = new RelayCommand(
               () =>
               {
                  ActiveItem = NextItem;
                 
               },
               () => NextItem != null && CanChageView(ActiveItem.CanNext)));
         }
      }

      public ICommand PrevViewCommand
      {
         get
         {
            return prevViewCommand ?? (prevViewCommand = new RelayCommand(
               () =>
               {
                  ActiveItem = PrevItem;
               },
               () => PrevItem != null && CanChageView(ActiveItem.CanPrev)));
         }
      }

      private bool CanChageView(Func<bool> action)
      {
         if (action == null)
         {
            return true;
         }
         return action.Invoke();
      }


      public void UpdateCommnads()
      {
         ((RelayCommand)NextViewCommand).RaiseCanExecuteChanged();
         ((RelayCommand)PrevViewCommand).RaiseCanExecuteChanged();
      }

   }

   public class Navigation
   {
      public string Title { get; set; }
      public string TitleText { get; set; }
      public IView View { get; set; }

      public Func<bool> CanNext { get; set; }
      public Func<bool> CanPrev { get; set; }
   }
}