﻿using System.Net;
using Bat2Library.Connection.Interface.Domain;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat2Config.ViewModel.Configuration
{
   public class EthernetViewModel : ObservableObject
   {
      #region Private fields

      private bool dhcp;
      private IPAddress gateway;
      private IPAddress ip;
      private IPAddress primaryDns;
  
      private IPAddress subnetMask;

      #endregion

      #region Public interface

      #region Constructors

      public EthernetViewModel(IView view, Ethernet ethernet)
      {
         Dhcp = ethernet.Dhcp;
         Gateway = ethernet.Gateway.ToString();
         Ip = ethernet.Ip.ToString();
         PrimaryDns = ethernet.PrimaryDns.ToString();
         SubnetMask = ethernet.SubnetMask.ToString();

         view.DataContext = this;
      }

      #endregion

      #region Properties

      public bool Dhcp
      {
         get
         {
            return dhcp;
         }
         set
         {
            SetProperty(ref dhcp, value);
         }
      }

      public string Gateway
      {
         get
         {
            return gateway.ToString();
         }
         set
         {
            IPAddress ipAdd = null;
            if (!IPAddress.TryParse(value,out ipAdd))
            {
               ipAdd = new IPAddress(0);
            }
            SetProperty(ref gateway, GetIp(value));
         }
      }

      public string Ip
      {
         get
         {
            return ip.ToString();
         }
         set
         {

            SetProperty(ref ip, GetIp(value));
         }
      }

      public string PrimaryDns
      {
         get
         {
            return primaryDns.ToString();
            
         }
         set
         {

            SetProperty(ref primaryDns, GetIp(value));
         }
      }

      public string SubnetMask
      {
         get
         {
            return subnetMask.ToString();
            
         }
         set
         {
            SetProperty(ref subnetMask, GetIp(value));
         }
      }

      #endregion 

      #endregion

      #region Private helpers

      private IPAddress GetIp(string ip)
      {
         IPAddress ipAdd = null;
         if (!IPAddress.TryParse(ip, out ipAdd))
         {
            ipAdd = new IPAddress(0);
         }
         return ipAdd;
      }

   
      #endregion
   }
}
