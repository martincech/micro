﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using Bat2Config.Map;
using Bat2Library;
using Bat2Library.Connection.Interface.Domain;
using BatLibrary;
using Desktop.Wpf.Applications;
using Utilities.Observable;
using CurvePoint = Bat2Config.Models.CurvePoint;

namespace Bat2Config.ViewModel.Configuration
{
   public class WeighingConfigurationViewModel : ObservableObject
   {
      #region Private fields

      private ICommand configCommand;
      private bool showPredefined;
      private WeighingConfigurationViewModel predefinedConfiguration;
      private ObservableCollection<WeighingConfigurationViewModel> predefinedConfigurationList;


      private const int DECIMAL_HIGH = 1000;
      private const double DECIMAL_LOW = 0.001;
      private const int BYTE_MAX = byte.MaxValue;
      private const int DEFAULT_CURVES_COUNT = 0;
      private int correctionCurveIndexWithDefault;
      private int maleGrowthCurveIndexWithDefault;
      private int femaleGrowthCurveIndexWithDefault;
      private int weighingPlanIndexWithDefault;
      private bool useCurve;


      private string name;
      private string flock;
      private WeighingConfigurationMenuMaskE menuMask;
      private short initialDay;
      private int correctionCurveIndex;
      private int predefinedIndex;
      private DateTime dayStart;
      private int maleInitialWeight;
      private int maleGrowthCurveIndex;
      private int femaleInitialWeight;
      private int femaleGrowthCurveIndex;
      private OnlineAdjustmentE adjustTargetWeights;
      private SexE sex;
      private SexDifferentiationE sexDifferentiation;
      private PredictionGrowthE growth;
      private PredictionModeE mode;
      private byte filter;
      private byte stabilizationTime;
      private byte stabilizationRange;
      private PlatformStepModeE step;
      private byte maleMarginAbove;
      private byte maleMarginBelow;
      private byte femaleMarginAbove;
      private byte femaleMarginBelow;

      private bool planning;
      private int weighingPlanIndex;

      //Support for WeighingDaysMaskE enum multiselect
      private ObservableCollection<WeighingConfigurationMenuMaskE> selectedMenuMask;
      private bool recalcCollection;

      private ObservableCollection<Models.Curve> correctionCurvesNames;
      private ObservableCollection<Models.Curve> growthCurvesNames;
      private ObservableCollection<WeighingPlan> weighingPlansNames;

      private WeightUnitsE units;
      private double divisionMale;
      private double divisionFemale;

      private StatisticsViewModel statisticsVm;
      //private WeighingCapacity weighingCapacity;

      #endregion

      #region Public interface

      #region Constructors

      public WeighingConfigurationViewModel(WeighingConfiguration conf, bool showPredefined)
      {
         SelectedMenuMask = new ObservableCollection<WeighingConfigurationMenuMaskE>();
         SelectedMenuMask.CollectionChanged += SelectedMenuMaskChanged;
         recalcCollection = true;
         ShowPredefined = showPredefined;

         SetModelData(conf);

         CorrectionCurvesNames = new ObservableCollection<Models.Curve>();
         //GrowthCurvesNames = new ObservableCollection<Models.Curve>();
         GrowthCurvesNames = StoreVM.Instance.CurveVM.GrowthCurve;

         WeighingPlansNames = new ObservableCollection<WeighingPlan>();

         //CorrectionCurvesNames.Insert(0, Properties.Resources.Empty);
         //GrowthCurvesNames.Insert(0, Properties.Resources.Empty);
         //WeighingPlansNames.Insert(0, Properties.Resources.Empty);
      
         PropertyChanged += OnPropertyChanged;      
      }

      private void SetModelData(WeighingConfiguration conf)
      {
         StatisticsVm = new StatisticsViewModel(null, conf, (Units)Units);
         Name = conf.Name;
         Flock = conf.Flock;
         MenuMask = conf.MenuMask;
         InitialDay = conf.InitialDay;
         PredefinedIndex = conf.PredefinedIndex;
         DayStart = conf.DayStart;
         MaleInitialWeight = conf.MaleInitialWeight;
         FemaleInitialWeight = conf.FemaleInitialWeight;
         AdjustTargetWeights = conf.AdjustTargetWeights;
         Sex = conf.Sex;
         SexDifferentiation = conf.SexDifferentiation;
         Growth = conf.Growth;
         Mode = (PredictionModeE)conf.Mode;
         Filter = conf.Filter;
         StabilizationTime = conf.StabilizationTime;
         StabilizationRange = conf.StabilizationRange;
         Step = conf.Step;
         MaleMarginAbove = conf.MaleMarginAbove;
         MaleMarginBelow = conf.MaleMarginBelow;
         FemaleMarginAbove = conf.FemaleMarginAbove;
         FemaleMarginBelow = conf.FemaleMarginBelow;
         Planning = conf.Planning;
         UseCurve = conf.CorrectionCurveIndex < 255 && CorrectionCurvesNames != null;
         //Units = conf;
         //WeighingCapacity = unitsConfig.WeighingCapacity;
         DivisionMale = ConvertWeight.Convert(MaleInitialWeight, BatLibrary.Units.G, (Units)Units);
         DivisionFemale = ConvertWeight.Convert(FemaleInitialWeight, BatLibrary.Units.G, (Units)Units);

         CorrectionCurveIndexWithDefault = conf.CorrectionCurveIndex + DEFAULT_CURVES_COUNT;
         MaleGrowthCurveIndexWithDefault = conf.MaleGrowthCurveIndex + DEFAULT_CURVES_COUNT;
         FemaleGrowthCurveIndexWithDefault = conf.FemaleGrowthCurveIndex + DEFAULT_CURVES_COUNT;
         WeighingPlanIndexWithDefault = conf.WeighingPlanIndex + DEFAULT_CURVES_COUNT;
      }

      #endregion

      #region Properties

      /// <summary>
      /// Menu mask as collection
      /// </summary>
      public ObservableCollection<WeighingConfigurationMenuMaskE> SelectedMenuMask
      {
         get { return selectedMenuMask; }
         private set { SetProperty(ref selectedMenuMask, value); }
      }

      public WeighingConfigurationMenuMaskE MenuMask
      {
         get { return menuMask; }
         set
         {
            SetProperty(ref menuMask, value);

            if (recalcCollection)
            {
               SelectedMenuMask.CollectionChanged -= SelectedMenuMaskChanged;
               SelectedMenuMask.Clear();
               foreach (WeighingConfigurationMenuMaskE ev in typeof(WeighingConfigurationMenuMaskE).GetEnumValues())
               {
                  if (MenuMask.HasFlag(ev))
                  {
                     SelectedMenuMask.Add(ev);
                  }
               }
               SelectedMenuMask.CollectionChanged += SelectedMenuMaskChanged;
            }
         }
      }

      public int CorrectionCurveIndex
      {
         get { return GetIndex(CorrectionCurveIndexWithDefault); }
      }

      public int CorrectionCurveIndexWithDefault
      {
         get { return correctionCurveIndexWithDefault; }
         set { SetProperty(ref correctionCurveIndexWithDefault, value); }
      }

      public int MaleGrowthCurveIndex
      {
         get { return GetIndex(MaleGrowthCurveIndexWithDefault); }
      }

      public int MaleGrowthCurveIndexWithDefault
      {
         get { return maleGrowthCurveIndexWithDefault; }
         set { SetProperty(ref maleGrowthCurveIndexWithDefault, value); }
      }

      public int FemaleGrowthCurveIndex
      {
         get { return GetIndex(FemaleGrowthCurveIndexWithDefault); }
      }

      public int FemaleGrowthCurveIndexWithDefault
      {
         get { return femaleGrowthCurveIndexWithDefault; }
         set { SetProperty(ref femaleGrowthCurveIndexWithDefault, value); }
      }

      public int WeighingPlanIndex
      {
         get { return GetIndex(WeighingPlanIndexWithDefault); }
      }

      public int WeighingPlanIndexWithDefault
      {
         get { return weighingPlanIndexWithDefault; }
         set { SetProperty(ref weighingPlanIndexWithDefault, value); }
      }

      public ObservableCollection<Models.Curve> CorrectionCurvesNames { get { return correctionCurvesNames; } set { SetProperty(ref correctionCurvesNames, value); } }

      public ObservableCollection<Models.Curve> GrowthCurvesNames
      {
         get
         {
            return growthCurvesNames;
         }
         set
         {
            SetProperty(ref growthCurvesNames, value);
         }
      }

      public ObservableCollection<WeighingPlan> WeighingPlansNames { get { return weighingPlansNames; } set { SetProperty(ref weighingPlansNames, value); } }

      public string Name { get { return name; } set { SetProperty(ref name, value); } }
      public string Flock { get { return flock; } set { SetProperty(ref flock, value); } }
      public short InitialDay { get { return initialDay; } set { SetProperty(ref initialDay, value); } }
      public int PredefinedIndex { get { return predefinedIndex; } set { SetProperty(ref predefinedIndex, value); } }
      public DateTime DayStart { get { return dayStart; } set { SetProperty(ref dayStart, value); } }
      public int MaleInitialWeight { get { return maleInitialWeight; } set { SetProperty(ref maleInitialWeight, value); } }
      public int FemaleInitialWeight { get { return femaleInitialWeight; } set { SetProperty(ref femaleInitialWeight, value); } }
      public OnlineAdjustmentE AdjustTargetWeights { get { return adjustTargetWeights; } set { SetProperty(ref adjustTargetWeights, value); } }
      public SexE Sex { get { return sex; } set { SetProperty(ref sex, value); } }

      public SexDifferentiationE SexDifferentiation
      {
         get
         {
            return sexDifferentiation;
         }
         set
         {
            SetProperty(ref sexDifferentiation, value);
         }
      }

      public PredictionGrowthE Growth { get { return growth; } set { SetProperty(ref growth, value); } }
      public PredictionModeE Mode { get { return mode; } set { SetProperty(ref mode, value); } }
      public byte Filter { get { return filter; } set { SetProperty(ref filter, value); } }
      public byte StabilizationTime { get { return stabilizationTime; } set { SetProperty(ref stabilizationTime, value); } }
      public byte StabilizationRange { get { return stabilizationRange; } set { SetProperty(ref stabilizationRange, value); } }
      public PlatformStepModeE Step { get { return step; } set { SetProperty(ref step, value); } }
      public byte MaleMarginAbove { get { return maleMarginAbove; } set { SetProperty(ref maleMarginAbove, value); } }
      public byte MaleMarginBelow { get { return maleMarginBelow; } set { SetProperty(ref maleMarginBelow, value); } }
      public byte FemaleMarginAbove { get { return femaleMarginAbove; } set { SetProperty(ref femaleMarginAbove, value); } }
      public byte FemaleMarginBelow { get { return femaleMarginBelow; } set { SetProperty(ref femaleMarginBelow, value); } }
      public bool Planning { get { return planning; } set { SetProperty(ref planning, value); } }

      //public WeighingCapacity WeighingCapacity
      //{
      //   get
      //   {
      //      return weighingCapacity;
      //   }
      //   set
      //   {
      //      SetProperty(ref weighingCapacity, value);
      //      statisticsVm.WeighingCapacity = value;
      //   }
      //}

      public WeightUnitsE Units
      {
         get
         {
            return units;
         }
         set
         {
            DivisionFemale = ConvertWeight.Convert(DivisionFemale, (Units)units, (Units)value);
            DivisionMale = ConvertWeight.Convert(DivisionMale, (Units)units, (Units)value);

            SetProperty(ref units, value);
            statisticsVm.Units = value;
         }
      }

      public double DivisionMale
      {
         get
         {
            return divisionMale;
         }
         set
         {
            SetProperty(ref divisionMale, value);
            var newValue = value;
            if (Units != WeightUnitsE.WEIGHT_UNITS_G)
            {
               newValue *= DECIMAL_HIGH;
            }
            MaleInitialWeight = (int)newValue;
         }
      }

      public double DivisionFemale
      {
         get
         {
            return divisionFemale;
         }
         set
         {
            SetProperty(ref divisionFemale, value);
            var newValue = value;
            if (Units != WeightUnitsE.WEIGHT_UNITS_G)
            {
               newValue *= DECIMAL_HIGH;
            }
            FemaleInitialWeight = (int)newValue;
         }
      }

      public StatisticsViewModel StatisticsVm
      {
         get { return statisticsVm; }
         set
         {
            SetProperty(ref statisticsVm, value);
            statisticsVm.Units = Units;
         }
      }

      public ICommand ConfigCommand
      {
         get
         {
            if (configCommand == null)
            {
               configCommand = new RelayCommand((predefined) =>
               {
                  var conf = (WeighingConfigurationViewModel)predefined;
                  if (conf != null)
                  {
                     SetModelData(conf.Map());
                     PredefinedIndex = PredefinedConfigurationList.IndexOf(conf);
                  }
                  else
                  {
                     PredefinedIndex = 255;
                  }

               });
            }
            return configCommand;
         }

      }

      public WeighingConfigurationViewModel PredefinedConfiguration
      {
         get { return predefinedConfiguration; }
         set
         {
            SetProperty(ref predefinedConfiguration, value);
            SetModelData(PredefinedConfiguration.Map());
            PredefinedIndex = PredefinedConfigurationList.IndexOf(value);
         }
      }

      public ObservableCollection<WeighingConfigurationViewModel> PredefinedConfigurationList
      {
         get { return predefinedConfigurationList; }
         set { SetProperty(ref predefinedConfigurationList, value); }
      }

      public bool ShowPredefined
      {
         get { return showPredefined; }
         set { SetProperty(ref showPredefined, value); }
      }

      public bool UseCurve
      {
         get { return useCurve; }
         set
         {
            SetProperty(ref useCurve, value);
            if (!useCurve)
            {
               CorrectionCurveIndexWithDefault = 255;
            }
            else
            {
               if (CorrectionCurvesNames.Count > 0)
               {
                  CorrectionCurveIndexWithDefault = 0;
               }
            }
         }
      }

      #endregion

      /// <summary>
      /// Set initial male/female weight by growth curve 
      /// if mode prediction is set to growth curve.
      /// </summary>
      public void SetInitialWeights()
      {
         OnPropertyChanged(this, new PropertyChangedEventArgs("Mode"));
      }

      /// <summary>
      /// Update initial weight.
      /// </summary>
      /// <param name="point">updated curve point</param>
      public void UpdateInitialWeight(CurvePoint point)
      {
         // if updated point is use for initial weight then update value
         if (Mode == PredictionModeE.PREDICTION_MODE_GROWTH_CURVE &&
             GrowthCurvesNames[MaleGrowthCurveIndex].Points.Contains(point))
         {
            var pointIndex = GrowthCurvesNames[MaleGrowthCurveIndex].Points.IndexOf(point);
            var closestIndex = ClosestPointsIndex(GrowthCurvesNames[MaleGrowthCurveIndex].Points, InitialDay);
            if (pointIndex == closestIndex)
            {
               DivisionMale = GetClosestWeight(MaleGrowthCurveIndex);
            }
         }
      }

      #endregion

      #region Private Helpers

      private void SelectedMenuMaskChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         var mask = default(WeighingConfigurationMenuMaskE);
         foreach (var ev in SelectedMenuMask)
         {
            mask |= ev;
         }
         recalcCollection = false;
         MenuMask = mask;
         recalcCollection = true;
      }

      /// <summary>
      /// Edit curve index without default curves to type byte.
      /// </summary>
      /// <param name="value">curve index with default curves</param>
      /// <returns>curve index</returns>
      private int GetIndex(int value)
      {
         var index = value - DEFAULT_CURVES_COUNT;
         if (index > BYTE_MAX || index == -1)
         {
            index = BYTE_MAX;
         }
         return index;
      }

      private void OnPropertyChanged(object sender, PropertyChangedEventArgs args)
      {
         if (args.PropertyName.Equals("MaleGrowthCurveIndexWithDefault"))
         {
            UpdateInitialWeight(SexE.SEX_MALE);
         }

         if (args.PropertyName.Equals("FemaleGrowthCurveIndexWithDefault"))
         {
            UpdateInitialWeight(SexE.SEX_FEMALE);
         }

         if ((args.PropertyName.Equals("Mode") && Mode == PredictionModeE.PREDICTION_MODE_GROWTH_CURVE) ||
             args.PropertyName.Equals("InitialDay"))
         {
            if (MaleGrowthCurveIndex < GrowthCurvesNames.Count)
            {
               UpdateInitialWeight(SexE.SEX_MALE);
            }
            //check sex differentiation and update female's initial weight
            if (SexDifferentiation == SexDifferentiationE.SEX_DIFFERENTIATION_YES &&
                FemaleGrowthCurveIndex < GrowthCurvesNames.Count)
            {
               UpdateInitialWeight(SexE.SEX_FEMALE);
            }
         }

         if (args.PropertyName.Equals("SexDifferentiation") &&
             SexDifferentiation == SexDifferentiationE.SEX_DIFFERENTIATION_YES &&
             Mode == PredictionModeE.PREDICTION_MODE_GROWTH_CURVE)
         {
            UpdateInitialWeight(SexE.SEX_FEMALE);
         }
      }

      /// <summary>
      /// Update initial weight.
      /// </summary>
      /// <param name="updatedSex">sex</param>
      private void UpdateInitialWeight(SexE updatedSex)
      {
         switch (updatedSex)
         {
            case SexE.SEX_MALE:
               DivisionMale = GetClosestWeight(MaleGrowthCurveIndex);
               break;
            case SexE.SEX_FEMALE:
               DivisionFemale = GetClosestWeight(FemaleGrowthCurveIndex); ;
               break;
         }
      }

      /// <summary>
      /// Get the closest weight from growth curve and InitialDay property.
      /// </summary>
      /// <param name="curveIndex">index of growth curve</param>
      /// <returns>closest curve's weight</returns>
      private double GetClosestWeight(int curveIndex)
      {
         var curve = GrowthCurvesNames[curveIndex];
         var index = ClosestPointsIndex(curve.Points, InitialDay);
         var weight = curve.Points.ElementAt(index).ValueY;
         var convertedValue = ConvertWeight.Convert(weight * 0.1, BatLibrary.Units.G, (Units)Units);
         return convertedValue;
      }

      /// <summary>
      /// Find the closest point index by ValueX.
      /// </summary>
      /// <param name="points">collection of curve's points</param>
      /// <param name="target">target value</param>
      /// <returns>collection index of the closest point</returns>
      private int ClosestPointsIndex(ICollection<CurvePoint> points, int target)
      {
         var coll = points.ToList();
         var closestIndex = -1;
         var minDifference = int.MaxValue;
         for (var i = 0; i < coll.Count(); i++)
         {
            var difference = Math.Abs((long)coll[i].ValueX - target);
            if (minDifference > difference)
            {
               minDifference = (int)difference;
               closestIndex = i;

               if (difference == 0) break;
            }
         }

         return closestIndex;
      }

      #endregion
   }
}
