﻿using Bat2Library;
using Bat2Library.Connection.Interface.Domain;
using Utilities.Observable;

namespace Bat2Config.ViewModel.Configuration
{
   public class DacsOptionsViewModel : ObservableObject
   {
      #region Private fields

      private byte address;
      private byte deviceAddress;
      private DacsVersionE version;

      #endregion

      #region Public interface

      #region Constructors

      public DacsOptionsViewModel(DacsOptions option)
      {
         Address = option.Address;
         DeviceAddress = option.DeviceAddress;
         Version = option.Version;
      }   

      #endregion

      #region Properties

      public byte Address { get { return address; } set { SetProperty(ref address, value); } }
      public byte DeviceAddress { get { return deviceAddress; } set { SetProperty(ref deviceAddress, value); } }
      public DacsVersionE Version { get { return version; } set { SetProperty(ref version, value); } }

      #endregion 

      #endregion
   }
}
