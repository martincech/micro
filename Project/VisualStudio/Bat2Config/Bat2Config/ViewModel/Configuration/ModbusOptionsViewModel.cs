﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Bat2Library;
using Bat2Library.Connection.Interface.Domain;
using Utilities.Observable;
using Bat2Library.Desktop.Localization.Converters;

namespace Bat2Config.ViewModel.Configuration
{
   public class ModbusOptionsViewModel : ObservableObject
   {
      #region Private fields

      private ModbusParityE parity;
      private int baudRate;
      private byte address;
      private ModbusModeE mode;
      private bool isSensorPack;


      #endregion

      #region Public interface

      #region Constructors

      public ModbusOptionsViewModel(ModbusOptions option, bool isSensorPack)
      {
         Address = option.Address;
         BaudRate = option.BaudRate;
         Address = option.Address;
         Mode = option.Mode;
         Parity = option.Parity;
         IsSensorPack = isSensorPack;
      }

      #endregion

      #region Properties

      public ModbusParityE Parity
      {
         get
         {
            return parity;
         }
         set
         {
            SetProperty(ref parity, value);
         }
      }

      public int BaudRate { get { return baudRate; } set { SetProperty(ref baudRate, value); } }
      public byte Address { get { return address; } set { SetProperty(ref address, value); } }
      public ModbusModeE Mode { get { return mode; } set { SetProperty(ref mode, value); } }

      public IEnumerable<ModbusModeE> ModeListSp
      {
         get
         {
            return new List<ModbusModeE>
            {
               ModbusModeE.MB_ASCII_MASTER,
               ModbusModeE.MB_RTU_MASTER
            };
         }
      }

      public IEnumerable<ModbusModeE> ModeListMb
      {
         get
         {
            return new List<ModbusModeE>
            {
               ModbusModeE.MB_ASCII,
               ModbusModeE.MB_RTU
            };
         }
      }

      public bool IsSensorPack
      {
         get { return isSensorPack; }
         set
         {
            SetProperty(ref isSensorPack, value);
            Mode = value ? ModbusModeE.MB_ASCII_MASTER : ModbusModeE.MB_ASCII;
         }
      }

      #endregion

      #endregion
   }
}
