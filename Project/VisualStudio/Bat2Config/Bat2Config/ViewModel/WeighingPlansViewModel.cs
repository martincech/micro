﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Input;
using Bat2Library;
using Bat2Library.Connection.Interface.Domain;
using Desktop.Wpf.Applications;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat2Config.ViewModel
{
   public class WeighingPlansViewModel : ObservableObject
   {
      private ObservableCollection<WeighingPlan> weighignPlans;
      private WeighingPlan activeItem;

      private ObservableCollection<WeighingDaysMaskE> weighingDaysModeMask;
      private bool recalcCollection;
      private WeighingDaysMaskE weighingDaysDays;

      private ICommand addCommand;
      private ICommand deleteCommand;
      private ICommand addTimeCommand;

      public WeighingPlansViewModel(IView view)
      {
         weighignPlans = new ObservableCollection<WeighingPlan>();
         WeighingDaysModeMask = new ObservableCollection<WeighingDaysMaskE>();
         WeighingDaysModeMask.CollectionChanged += WeighingDaysModeMaskChanged;
         recalcCollection = true;
         view.DataContext = this;
      }


      public ObservableCollection<WeighingPlan> WeighignPlans
      {
         get { return weighignPlans; }
         set { SetProperty(ref weighignPlans, value); }
      }

      public WeighingPlan ActiveItem
      {
         get { return activeItem; }
         set
         {
            SetProperty(ref activeItem, value);
            WeighingDaysDays = value.WeighingDaysDays;
            UpdateCommands();
         }
      }

      public ObservableCollection<WeighingDaysMaskE> WeighingDaysModeMask
      {
         get { return weighingDaysModeMask; }
         private set { SetProperty(ref weighingDaysModeMask, value); }
      }

      public WeighingDaysMaskE WeighingDaysDays
      {
         get { return weighingDaysDays; }
         set
         {
            SetProperty(ref weighingDaysDays, value);

            if (recalcCollection)
            {
               WeighingDaysModeMask.CollectionChanged -= WeighingDaysModeMaskChanged;
               WeighingDaysModeMask.Clear();
               foreach (WeighingDaysMaskE ev in typeof(WeighingDaysMaskE).GetEnumValues())
               {
                  if (WeighingDaysDays.HasFlag(ev))
                  {
                     WeighingDaysModeMask.Add(ev);
                  }
               }
               WeighingDaysModeMask.CollectionChanged += WeighingDaysModeMaskChanged;
            }
         }
      }

      public ICommand AddCommand
      {
         get
         {
            return addCommand ?? (addCommand = new RelayCommand(() =>
            {
               weighignPlans.Add(new WeighingPlan() { Name = "New weighing plan", WeighingTimes = new ObservableCollection<TimeRange>() });
            }));
         }
      }

      public ICommand DeleteCommand
      {
         get
         {
            return deleteCommand ?? (deleteCommand = new RelayCommand(() =>
            {
               weighignPlans.Remove(ActiveItem);
            }, () => ActiveItem != null));
         }
      }

      public ICommand AddTimeCommand
      {
         get
         {
            return addTimeCommand ?? (addTimeCommand = new RelayCommand((times) =>
            {
               var timeList = (object[])times;
               if (timeList[0] == null || timeList[1] == null)
               {
                  return;
               }
               var from = (DateTime)timeList[0];
               var to = (DateTime)timeList[1];
               (ActiveItem.WeighingTimes as ObservableCollection<TimeRange>).Add(new TimeRange() { From = from, To = to });
            }));
         }
      }

    

      private void WeighingDaysModeMaskChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         var mask = default(WeighingDaysMaskE);
         foreach (var ev in WeighingDaysModeMask)
         {
            mask |= ev;
         }
         recalcCollection = false;
         ActiveItem.WeighingDaysDays = mask;
         recalcCollection = true;
      }

      private void UpdateCommands()
      {
         ((RelayCommand)DeleteCommand).RaiseCanExecuteChanged();
      }
   }
}
