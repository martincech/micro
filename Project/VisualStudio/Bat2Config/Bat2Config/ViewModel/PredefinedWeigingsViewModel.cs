﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using Bat2Config.View.Configuration;
using Bat2Config.ViewModel.Configuration;
using Bat2Library.Connection.Interface.Domain;
using Desktop.Wpf.Applications;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat2Config.ViewModel
{
   public class PredefinedWeigingsViewModel : ObservableObject
   {
      private ObservableCollection<WeighingConfigurationViewModel> predefinedWeighings;
      private WeighingConfigurationViewModel activeItem;

      private ICommand addCommand;
      private ICommand deleteCommand;
      private IView cView;
      public PredefinedWeigingsViewModel(IView view)
      {
         predefinedWeighings = new ObservableCollection<WeighingConfigurationViewModel>();
         cView = new WeighingConfigurationView();
         view.DataContext = this;
         
      }

      public ObservableCollection<WeighingConfigurationViewModel> PredefinedWeighings
      {
         get { return predefinedWeighings; }
         set
         {
            SetProperty(ref predefinedWeighings, value);
         }
      }

      public WeighingConfigurationViewModel ActiveItem
      {
         get { return activeItem; }
         set
         {
            SetProperty(ref activeItem, value);
            CView = new WeighingConfigurationView();
            ActiveItem.GrowthCurvesNames = StoreVM.Instance.CurveVM.GrowthCurve;
            ActiveItem.CorrectionCurvesNames = StoreVM.Instance.CurveVM.CorrectionCurve;
            ActiveItem.WeighingPlansNames = StoreVM.Instance.WeighingPlansVM.WeighignPlans;
            cView.DataContext = ActiveItem;
            ActiveItem.SetInitialWeights();
            UpdateCommands();
         }
      }

      public ICommand AddCommand
      {
         get
         {
            return addCommand ?? (addCommand = new RelayCommand(() =>
            {
               predefinedWeighings.Add(new WeighingConfigurationViewModel(new WeighingConfiguration(),false) { Name = "New configuration" });
            }));
         }
      }

      public ICommand DeleteCommand
      {
         get
         {
            return deleteCommand ?? (deleteCommand = new RelayCommand(() =>
            {
               predefinedWeighings.Remove(ActiveItem);
            }, () => ActiveItem != null));
         }
      }

      public IView CView
      {
         get
         {
            cView.DataContext = ActiveItem;
            return cView;
         }
         set { SetProperty(ref cView, value); }
      }

      private void UpdateCommands()
      {
         ((RelayCommand)DeleteCommand).RaiseCanExecuteChanged();
      }
   }
}
