﻿using System.Collections.ObjectModel;
using Utilities.Observable;

namespace Bat2Config.Models
{
   public class Curve : ObservableObject
   {
      private string _name;
      private ObservableCollection<CurvePoint> _points;

      /// <summary>
      /// Construct new empty curve
      /// </summary>
      public Curve()
      {
         Points = new ObservableCollection<CurvePoint>();
      }

      /// <summary>
      /// Curve name
      /// </summary>
      public string Name { get { return _name; } set { SetProperty(ref _name, value); } }

      /// <summary>
      /// Curve points
      /// </summary>
      public ObservableCollection<CurvePoint> Points { get { return _points; } set { SetProperty(ref _points, value); } }
   }

   public class CurvePoint : ObservableObject
   {
      private int _valueY;
      private int _valueX;

      /// <summary>
      /// Y value of curve point
      /// </summary>
      public int ValueY { get { return _valueY; } set { SetProperty(ref _valueY, value); } }

      /// <summary>
      /// X value of curve point
      /// </summary>
      public int ValueX { get { return _valueX; } set { SetProperty(ref _valueX, value); } }
   }
}
