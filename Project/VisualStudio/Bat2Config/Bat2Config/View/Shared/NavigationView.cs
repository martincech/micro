﻿using System;
using Bat2Config.ViewModel;
using Bat2Config.ViewModel.Shared;
using Desktop.Wpf.Presentation;

namespace Bat2Config.View.Shared
{
   /// <summary>
   /// Interaction logic for Navigation.xaml
   /// </summary>
   public partial class NavigationView : IView
   {

      private NavigationViewModel navigationViewModel;

      public NavigationViewModel NavigationViewModel
      {
         get { return navigationViewModel; }
         set
         {
            navigationViewModel = value;
         }
      }

      public NavigationView()
      {
         navigationViewModel = new NavigationViewModel();
         this.DataContext = navigationViewModel;
         InitializeComponent();
      }



      public void Show()
      {
         throw new NotImplementedException();
      }

      public void Hide()
      {
         throw new NotImplementedException();
      }


   }
}
