﻿using System.Windows;
using Desktop.Wpf.Presentation;

namespace Bat2Config.View.Configuration
{
   /// <summary>
   /// Interaction logic for Bat2IdentificationView.xaml
   /// </summary>
   public partial class Bat2IdentificationView : IBat2IdentificationView
   {    
      public Bat2IdentificationView()
      {
         InitializeComponent();     
      }


      public void Show()
      {
         Visibility = Visibility.Visible;
      }

      public void Hide()
      {
         Visibility = Visibility.Collapsed;
      }
   }

   public interface IBat2IdentificationView : IView
   {
   }
}
