﻿using System.Globalization;
using System.Linq;
using System.Windows.Controls;
using Bat2Config.ViewModel.Configuration;
using Bat2Library;
using Bat2Library.Desktop.Presentation.Converters;
using Xceed.Wpf.Toolkit;

namespace Bat2Config.View.Configuration
{
   /// <summary>
   /// Interaction logic for Rs485OptionsView.xaml
   /// </summary>
   public partial class Rs485OptionsView : UserControl
   {
      public Rs485OptionsView()
      {
         InitializeComponent();      
      }

      private void ModeComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
      {
         var vm = DataContext as Rs485OptionsListViewModel;
         if (vm == null) return;

         var nextModel = vm.Models.First(m => m != vm.Model);
         Rs485ModeEConverter conv = new Rs485ModeEConverter();
         Rs485ModeE current = (Rs485ModeE)conv.ConvertBack(ModeComboBox.SelectedItem, typeof(Rs485ModeE), null, CultureInfo.InvariantCulture);


         if (nextModel.Mode == current 
            || (nextModel.Mode == Rs485ModeE.RS485_MODE_MODBUS && current == Rs485ModeE.RS485_MODE_SENSOR_PACK) 
            || (nextModel.Mode == Rs485ModeE.RS485_MODE_SENSOR_PACK && current == Rs485ModeE.RS485_MODE_MODBUS))
         {
            if (!nextModel.Enabled) //other model is not enable => new value is valid
            {

               nextModel.Mode = (Rs485ModeE)conv.ConvertBack(e.RemovedItems[0], typeof(Rs485ModeE), null, null);
               return;
            }

            ModeComboBox.SelectedItem = e.RemovedItems[0];
            MessageBox.Show("Selected item is already set on the other interface.", "Error");
         }
      }
   }
}
