﻿using System.Windows;
using Desktop.Wpf.Presentation;

namespace Bat2Config.View.Configuration
{
   /// <summary>
   /// Interaction logic for CellularDataView.xaml
   /// </summary>
   public partial class CellularDataView : ICellularDataView
   {    
      public CellularDataView()
      {
         InitializeComponent();        
      }

      public void Show()
      {
         Visibility = Visibility.Visible;
      }

      public void Hide()
      {
         Visibility = Visibility.Collapsed;
      }
   }

   public interface ICellularDataView : IView
   {
   }
}
