﻿using System.Windows;
using Bat2Config.ViewModel.Configuration;
using Desktop.Wpf.Presentation;

namespace Bat2Config.View.Configuration
{
   /// <summary>
   /// Interaction logic for WeighingConfigurationView.xaml
   /// </summary>
   public partial class WeighingConfigurationView : IWeighingConfigurationView
   {
      public WeighingConfigurationView()
      {
         InitializeComponent();
      }

      public void Show()
      {
         Visibility = Visibility.Visible;
      }

      public void Hide()
      {
         Visibility = Visibility.Collapsed;
      }    
   }

   public interface IWeighingConfigurationView : IView
   {
   }
}
