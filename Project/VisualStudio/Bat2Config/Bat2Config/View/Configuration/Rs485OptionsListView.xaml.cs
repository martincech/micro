﻿using System.Windows;
using Desktop.Wpf.Presentation;

namespace Bat2Config.View.Configuration
{
   /// <summary>
   /// Interaction logic for Rs485OptionsListView.xaml
   /// </summary>
   public partial class Rs485OptionsListView : IRs485OptionsListView
   {
      public Rs485OptionsListView()
      {
         InitializeComponent();
      }


      public void Show()
      {
         Visibility = Visibility.Visible;
      }

      public void Hide()
      {
         Visibility = Visibility.Collapsed;
      }
   }

   public interface IRs485OptionsListView : IView
   {
   }
}
