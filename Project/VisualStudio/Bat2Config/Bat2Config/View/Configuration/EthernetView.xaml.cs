﻿using System.Windows;
using Desktop.Wpf.Presentation;

namespace Bat2Config.View.Configuration
{
   /// <summary>
   /// Interaction logic for EthernetConfigurationView.xaml
   /// </summary>
   public partial class EthernetView : IEthernetView
   {
      public EthernetView()
      {
         InitializeComponent();       
      }


      public void Show()
      {
         Visibility = Visibility.Visible;
      }

      public void Hide()
      {
         Visibility = Visibility.Collapsed;
      }
   }

   public interface IEthernetView : IView
   {
   }
}
