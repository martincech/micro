﻿using System.Windows;
using Desktop.Wpf.Presentation;

namespace Bat2Config.View.Configuration
{
   /// <summary>
   /// Interaction logic for PasswordView.xaml
   /// </summary>
   public partial class PasswordView : IPasswordView
   {
      public PasswordView(bool readOnly = false)
      {
         InitializeComponent();       
         Layout.IsEnabled = !readOnly;
      }   

      public void Show()
      {
         Visibility = Visibility.Visible;
      }

      public void Hide()
      {
         Visibility = Visibility.Collapsed;
      }
   }

   public interface IPasswordView : IView
   {
   }
}
