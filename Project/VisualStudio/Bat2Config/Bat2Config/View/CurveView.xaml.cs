﻿using System;
using System.Windows.Controls;
using Desktop.Wpf.Presentation;

namespace Bat2Config.View
{
   /// <summary>
   /// Interaction logic for CurveView.xaml
   /// </summary>
   public partial class CurveView : IView
   {
      public CurveView()
      {
         InitializeComponent();
      }


      public void Show()
      {
         throw new NotImplementedException();
      }

      public void Hide()
      {
         throw new NotImplementedException();
      }

      private void ListBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
      {
         if (e.AddedItems.Count > 0)
         {
            if (((ListBox) sender).Name == cCruvesListBox.Name)
            {
               gCruvesListBox.SelectedIndex = -1;
               cCruvesListBox.SelectedItem = e.AddedItems[0];
            }
            else
            {
               cCruvesListBox.SelectedIndex = -1;
               gCruvesListBox.SelectedItem = e.AddedItems[0];
            }
         }
      }
   }
}
