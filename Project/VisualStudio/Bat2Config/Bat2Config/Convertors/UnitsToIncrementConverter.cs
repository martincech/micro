﻿using System;
using System.Windows.Data;
using Bat2Library;

namespace Bat2Config.Convertors
{
   public class UnitsToIncrementConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         var units = (WeightUnitsE)value;
         return units == WeightUnitsE.WEIGHT_UNITS_G ? 1.0 : 0.001;
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
