﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Bat2Config.Convertors
{
   public class EqualToVisibilityConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         if (value == null || parameter == null)
         {
            return false;
         }
         string val = value.ToString();
         string param = parameter.ToString(); ;
         return val == param ? Visibility.Visible : Visibility.Hidden;
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
