﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using Bat2Tester.ModelViews.Aplications;
using Bat2Tester.ModelViews.Presentation;
using Desktop.Wpf.Presentation;
using ProgressBar = Desktop.Wpf.Presentation.ProgressBar;
using TextBox = Desktop.Wpf.Presentation.TextBox;

namespace Bat2Tester
{
   /// <summary>
   /// Interaction logic for App.xaml
   /// </summary>
   public partial class App
   {
      protected override void OnStartup(StartupEventArgs e)
      {
         base.OnStartup(e);
         DispatcherHelper.Initialize();
         // Show Main Window
         MainWindow = new ModelViews.Presentation.MainWindow();
         MainWindow.DataContext = new MainWindowViewModel(new Progress(), new ProgressBar(), new TextBox() { VerticalScrollBarVisibility = ScrollBarVisibility.Auto});
         MainWindow.Show();
      }
   }
}
