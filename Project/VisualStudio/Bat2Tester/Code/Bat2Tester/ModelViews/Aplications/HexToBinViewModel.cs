﻿using System.IO;
using System.Windows.Forms;
using System.Windows.Input;
using Bat2Library.Connection.Manager.Native;
using Bat2Tester.Properties;
using Desktop.Wpf.Applications;
using Utilities.Observable;
using MessageBox = System.Windows.MessageBox;

namespace Bat2Tester.ModelViews.Aplications
{
   public class HexToBinViewModel : ObservableObject
   {
      private string inputFile;
      private string outputFile;
      private RelayCommand convertCommand;
      private RelayCommand selectInputCommand;
      private RelayCommand selectOutputCommand;

      public string InputFile
      {
         get { return inputFile; }
         set
         {
            SetProperty(ref inputFile, value);
            convertCommand.RaiseCanExecuteChanged();
         }
      }

      public string OutputFile
      {
         get { return outputFile; }
         set
         {
            SetProperty(ref outputFile, value);
            convertCommand.RaiseCanExecuteChanged();
            CheckDirectory(outputFile, Resources.OutputFolderNotExist);
         }
      }

      #region Commands

      public ICommand ConvertCommand
      {
         get
         {
            return convertCommand ?? (convertCommand = new RelayCommand(
               // execute
               sender =>
               {
                  var result = FirmwareCoder.Encode(InputFile, OutputFile);
                  var msg = Resources.Conversion_failed;
                  if (result)
                  {
                     msg = Resources.Conversion_successfull;
                  }
                  MessageBox.Show(msg);
               },
               // canExecute
               param => !(InputFile == null || InputFile.Equals("") || OutputFile == null || OutputFile.Equals(""))));
         }
      }

      public ICommand SelectInputCommand
      {
         get
         {
            return selectInputCommand ?? (selectInputCommand = new RelayCommand(
               // execute
               sender =>
               {
                  var dialog = new OpenFileDialog {Filter = Resources.FirmwareFileFilter};
                  if (dialog.ShowDialog() != DialogResult.OK) return;

                  InputFile = dialog.FileName;

                  // Use path to output if output path is empty
                  if (OutputFile != null && !OutputFile.Equals("")) return;

                  var lastFolderIndex = InputFile.LastIndexOf(Path.DirectorySeparatorChar);
                  OutputFile = InputFile.Substring(0, lastFolderIndex);
               },
               // canExecute
               param => true
               ));
         }
      }

      public ICommand SelectOutputCommand
      {
         get
         {
            return selectOutputCommand ?? (selectOutputCommand = new RelayCommand(
               // execute
               sender =>
               {
                  var dialog = new FolderBrowserDialog();
                  if (dialog.ShowDialog() == DialogResult.OK)
                  {
                     OutputFile = dialog.SelectedPath;
                  }
               },
               // canExecute
               param => true
               ));
         }
      }

      #endregion

      private void CheckDirectory(string folder, string msg)
      {
         if (!Directory.Exists(folder))
         {
            MessageBox.Show(msg);
         }
      }
   }
}
