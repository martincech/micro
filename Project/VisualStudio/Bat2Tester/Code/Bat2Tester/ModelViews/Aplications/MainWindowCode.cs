﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Input;
using Bat2Library;
using Bat2Library.Connection.Interface.Domain;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;
using Bat2Tester.ModelViews.Interface;
using Bat2Tester.Properties;
using Bat2Tester.Settings;
using Desktop.Wpf.Applications;
using Desktop.Wpf.Presentation;
using GUI;
using Kinetis;
using Kinetis.JtagKey.JtagKey;
using Kinetis.RomWriter;
using Usb;

namespace Bat2Tester.ModelViews.Aplications
{
   public class MainWindowCode
   {
      private const ushort ModMask = 0xFFFF;
      private const string FtdiDeviceName = "VEIT USB Reader A";

      private readonly Flash _flashDevice;
      private readonly IProgress _progressWindow;
      private readonly EntryStorege _sNsettings;
      private CancellationTokenSource _cancellationTokenSource;
      private readonly IProgressBar _kProgressBar;

      private readonly HexCodec _hex = new HexCodec();

      public ITextBox KTextBox { get; private set; }
      public MainWindowData Data { get; private set; }

      #region Public interfaces

      #region Constructor

      public MainWindowCode(IProgress progressWindow, IProgressBar progressBar, ITextBox textBox)
      {

         Data = new MainWindowData();
         
         _progressWindow = progressWindow;
         _kProgressBar = progressBar;
         KTextBox = textBox;
         _sNsettings = new EntryStorege(KTextBox);

         Data.EzDeviceList = new ObservableCollection<string>();

         _sNsettings = new EntryStorege(KTextBox);

         Data.FwFileName = Properties.Settings.Default.FileFirmware;
         Data.FwTestFileName = Properties.Settings.Default.FileTestFirmware;
         Data.SnFilePath = _sNsettings.Path;

         _cancellationTokenSource = new CancellationTokenSource();

         _flashDevice = new Flash(new EzPort(2), KTextBox, _kProgressBar);

         _flashDevice.OnExecutionStart += flashDevice_OnExecutionStart;
         _flashDevice.OnExecutionStop += flashDevice_OnExecutionStop;

         UsbDeviceManager.Instance.DeviceConnected += usbDeviceManager_DeviceConnected;
         UsbDeviceManager.Instance.DeviceDisconnected += usbDeviceManager_DeviceDisconnected;

         IBat2NewDeviceManager deviceManager = UsbBat2NewDeviceManager.Instance;
         deviceManager.DeviceConnected += manager_DeviceConnected;
         deviceManager.DeviceDisconnected += manager_DeviceDisconnected;
      }

      #endregion

      #region Commands

      public ICommand RefreshCommand
      {
         get
         {
            return new RelayCommand(
               // execute
               sender =>
               {
                  CheckToken();
                  _flashDevice.ExecuteOperation(flash =>
                  {
                     try
                     {
                        byte[] value = new byte[4];
                        if (!InitTarget(flash))
                        {
                           return;
                        }

                        flash.Fccob.ReadOnce(Data.SnAdress, ref value);
                        Data.SerialNumber = BitConverter.ToUInt32(value, 0);
                        AddMessage(string.Format(Resources.DeviceSNExist, Data.EzDevice, Data.SerialNumber));
                        value = new byte[2];

                        flash.ReadData(out value, Data.ModAdress, value.Length);
                        Data.ActualModification = BitConverter.ToUInt16(value, 0);
                     }
                     catch (Exception e)
                     {
                        KTextBox.AppendText(e.InnerException.Message);
                     }
                  }, Data.EzDeviceList.IndexOf(Data.EzDevice), _cancellationTokenSource);
               },
               // canExecute
               param => true
               );
         }
      }

      public ICommand ChangeModCommand
      {
         get
         {
            return new RelayCommand(
               // execute
               sender =>
               {
                  var data = sender as object[];
                  if (data == null || data.Length != 2)
                  {
                     return;
                  }

                  if (data[0] is DeviceModificationE)
                  {
                     SetModification((DeviceModificationE) data[0], (bool) data[1], true);
                  }
                  else
                  {
                     SetModification(DeviceModificationE.BAT2_ETHERNET_MODIFICATION, (bool) data[1]);
                  }
               },
               // canExecute
               param => true
               );
         }

      }

      public ICommand WriteFwCommand
      {
         get
         {
            return new RelayCommand(
               // execute
               param =>
               {
                  CheckToken();
                  _flashDevice.ExecuteOperation(flash =>
                  {

                     try
                     {
                        if (!InitTarget(flash))
                        {
                           return;
                        }

                        var fileName = param == null ? Data.FwFileName : Data.FwTestFileName;
                        _hex.Decode(fileName);

                        if (param == null)
                        {
                           byte[] mod = BitConverter.GetBytes(Data.Modification);
                           mod.CopyTo(_hex.data, Data.ModAdress);
                        }

                        if (!flash.Program(_hex.data, true, 3.0f))
                        {
                           throw new Exception(Resources.ProgramingFailed);
                        }
                        CloseProgressWindow();
                     }
                     catch (Exception ex)
                     {
                        AddMessage(ex.Message);
                        CloseProgressWindow();
                     }

                  }, Data.EzDeviceList.IndexOf(Data.EzDevice), _cancellationTokenSource);
                  _progressWindow.SetText(Resources.UploadingFW);
                  ShowProgressBar();
               },
               // canExecute
               param => !string.IsNullOrEmpty(Data.EzDevice)
                        && (param == null
                           ? !string.IsNullOrEmpty(Data.FwFileName) && File.Exists(Data.FwFileName)
                           : !string.IsNullOrEmpty(Data.FwTestFileName) && File.Exists(Data.FwTestFileName))
                        && Data.EzPortConnected
               );
         }

      }

      public ICommand EraseFwCommand
      {
         get
         {
            return new RelayCommand(
               // execute
               () =>
               {
                  CheckToken();
                  _flashDevice.ExecuteOperation(flash =>
                  {
                     try
                     {

                        if (!InitTarget(flash))
                        {
                           return;
                        }
                        flash.EraseAll();

                        CloseProgressWindow();
                     }
                     catch (Exception ex)
                     {
                        AddMessage(ex.Message);
                        CloseProgressWindow();
                     }
                  }, Data.EzDeviceList.IndexOf(Data.EzDevice), _cancellationTokenSource);
                  _progressWindow.SetText(Resources.ErasingFW);
                  ShowProgressBar();
               },
               // canExecute
               () => !string.IsNullOrEmpty(Data.EzDevice) && Data.EzPortConnected
               );

         }
      }

      public ICommand WriteSnCommand
      {
         get
         {
            return new RelayCommand(
               // execute
               () =>
               {
                  CheckToken();
                  _flashDevice.ExecuteOperation(flash =>
                  {
                     try
                     {
                        if (!InitTarget(flash))
                        {
                           return;
                        }

                        byte[] value = BitConverter.GetBytes(Data.SerialNumber);

                        if (value.SequenceEqual(new byte[] {255, 255, 255, 255}) ||
                            value.SequenceEqual(new byte[] {0, 0, 0, 0}))
                        {
                           AddMessage(string.Format(Resources.DeviceNoSN, Data.EzDevice));
                           UpdateSerialNumber(true);
                        }
                        else
                        {
                           AddMessage(string.Format(Resources.DeviceSN, Data.SerialNumber));
                        }

                        var serialNumberB = BitConverter.GetBytes(Data.SerialNumber);

                        if (!flash.Fccob.ProgramOnce(Data.SnAdress, serialNumberB))
                        {
                           throw new Exception(Resources.UnableSN);
                        }
                        Thread.Sleep(100);
                        var serialNumberRead = new byte[serialNumberB.Length];
                        if (!flash.Fccob.ReadOnce(Data.SnAdress, ref serialNumberRead))
                        {
                           throw new Exception(Resources.UnableVerifySN);
                        }
                        if (!serialNumberB.SequenceEqual(serialNumberRead))
                        {
                           throw new Exception(Resources.ErrorVerifySN + BitConverter.ToUInt32(serialNumberRead.Reverse().ToArray(), 0).ToString("X"));                          
                        }
                        _sNsettings.WriteEntry(Data.SerialNumber, Data.Modification);
                     }
                     catch (Exception ex)
                     {
                       AddMessage(ex.Message);                      }
                  }, Data.EzDeviceList.IndexOf(Data.EzDevice), _cancellationTokenSource);
               },
               // canExecute
               () => !string.IsNullOrEmpty(Data.EzDevice) && Data.EzPortConnected
               );
         }

      }

      public ICommand ReadSnCommand
      {
         get
         {
            return new RelayCommand(
               // execute
               () =>
               {
                  CheckToken();
                  _flashDevice.ExecuteOperation(flash =>
                  {
                     try
                     {
                        if (!InitTarget(flash))
                        {
                           return;
                        }

                        byte[] value = new byte[4];
                        flash.Fccob.ReadOnce(Data.SnAdress, ref value);
                        AddMessage(string.Format(Resources.SerialNumberIs, Data.EzDevice, BitConverter.ToUInt32(value, 0)));
                     }
                     catch (Exception e)
                     {
                       AddMessage(e.InnerException.Message);
                     }
                  }, Data.EzDeviceList.IndexOf(Data.EzDevice), _cancellationTokenSource);
               },
               // canExecute
               () => !string.IsNullOrEmpty(Data.EzDevice) && Data.EzPortConnected
               );

         }
      }

      public ICommand SelectFileCommand
      {
         get
         {
            return new RelayCommand(
               // execute
               param =>
               {
                  string fileName = "";
                  fileName = param == null ? Data.FwFileName : Data.FwTestFileName;

                  OpenFileDialog file = new OpenFileDialog();
                  if (!string.IsNullOrEmpty(fileName))
                  {
                     if (File.Exists(fileName))
                     {
                        file.FileName = Path.GetFileName(fileName);
                        file.InitialDirectory = fileName.Remove(fileName.Length - file.FileName.Length);
                     }
                  }
                  file.Filter = Resources.FirmwareFileFilter;
                  if (file.ShowDialog() == DialogResult.OK)
                  {
                     if (param == null)
                     {
                        Data.FwFileName = file.FileName;
                     }
                     else
                     {
                        Data.FwTestFileName = file.FileName;
                     }
                  }
               },
               // canExecute
               param => true
               );

         }
      }

      public ICommand SelectPathCommand
      {
         get
         {
            return new RelayCommand(
               // execute
               () =>
               {
                  FolderBrowserDialog dialog = new FolderBrowserDialog();
                  if (dialog.ShowDialog() == DialogResult.OK)
                  {
                     Data.SnFilePath = dialog.SelectedPath;
                  }
               },
               // canExecute
               () => true
               );
         }

      }

      public ICommand TestKeyboardCommand
      {
         get
         {
            return new RelayCommand(
               // execute
               () =>
               {
                  AddMessage("Test load cell has not been implemented yet");
               },
               // canExecute
               () => Data.UsbDevice != null
               );
         }

      }

      public ICommand TestLoadCellCommand
      {
         get
         {
            return new RelayCommand(
               // execute
               () =>
               {
                  AddMessage("Test load cell has not been implemented yet");
               },
               // canExecute
               () => Data.UsbDevice != null
               );

         }
      }

      public ICommand TestAccuCommand
      {
         get
         {
            return new RelayCommand(
               // execute
               () =>
               {
                  AddMessage("Test accu has not been implemented yet");
               },
               // canExecute
               () => Data.UsbDevice != null
               );
         }

      }

      #endregion

      #endregion

      #region Events

      private void usbDeviceManager_DeviceDisconnected(object sender, UsbDevice e)
      {
         if (e.ProductName == FtdiDeviceName && !EzPort.ListUp().Any())
         {
            DispatcherHelper.RunAsync(() =>
            {
               if (Data.EzDeviceList.Contains(e.ProductName))
               {
                  if (e.ProductName == Data.EzDevice)
                  {
                     Data.EzPortConnected = false;
                  }
                  Data.EzDeviceList.Remove(e.ProductName);
                  AddMessage(Resources.FtdiDisconnected);
               }
            });

         }
      }

      private void usbDeviceManager_DeviceConnected(object sender, UsbDevice e)
      {
         if (e.ProductName == FtdiDeviceName)
         {
            DispatcherHelper.RunAsync(() =>
            {
               EzPort.ListUp();
               if (!Data.EzDeviceList.Contains(e.ProductName))
               {
                  Data.EzDeviceList.Add(e.ProductName);
                  if (string.IsNullOrEmpty(Data.EzDevice))
                  {
                     Data.EzDevice = e.ProductName;
                     AddMessage(Resources.FtdiConnected);

                     if (RefreshCommand.CanExecute(null))
                     {
                        RefreshCommand.Execute(null);
                     }
                  }
               }
            });
         }
      }

      private void manager_DeviceDisconnected(object sender, Bat2DeviceData e)
      {
         DispatcherHelper.RunAsync(() =>
         {
            if (e.Configuration.VersionInfo.SerialNumber == Data.SerialNumber)
            {
               if (Data.UsbDevice != null)
               {
                  Data.UsbDevice = null;
                  AddMessage(Resources.UsbDisconnected);
               }
            }
         });
      }

      private void manager_DeviceConnected(object sender, Bat2DeviceData e)
      {
         DispatcherHelper.RunAsync(() =>
         {
            if (e.Configuration.VersionInfo.SerialNumber == Data.SerialNumber && Data.EzPortConnected)
            {
               Data.UsbDevice = e;
               AddMessage(Resources.UsbConnected);
            }
         });
      }

      private void flashDevice_OnExecutionStart(object sender, EventArgs e)
      {

      }

      private void flashDevice_OnExecutionStop(object sender, EventArgs e)
      {

      }

      #endregion

      #region Private helpers

      /// <summary>
      /// Test if Ez port is connected to board
      /// </summary>
      /// <returns>True if connected/Else if not connected</returns>
      private bool InitTarget(Flash flash)
      {
         if (!EzPort.ListUp().Any())
         {
            AddMessage("Ftdi device not connected!");
            Data.EzDeviceList.Clear();
            return false;
         }
         Data.EzPortConnected = flash.IsConnected;
         if (!Data.EzPortConnected)
         {
            AddMessage("Ez port not connected!");
         }
         return Data.EzPortConnected;
      }

      private void CheckToken()
      {
         if (_cancellationTokenSource == null || _cancellationTokenSource.Token.IsCancellationRequested)
         {
            _cancellationTokenSource = new CancellationTokenSource();
         }
      }

      /// <summary>
      /// Update serial number
      /// </summary>
      private void UpdateSerialNumber(bool forceNew)
      {
         //Unix timestamp 4 bytes valid from '1970-01-01 00:00:01' UTC to '2038-01-19 03:14:07' UTC 
         if (Data.SerialNumber == 0 || forceNew)
         {
            Data.SerialNumber = (UInt32) (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            AddMessage(string.Format(Resources.CreatedSN, Data.SerialNumber));
         }
      }

      /// <summary>
      /// Show progress bar
      /// </summary>
      private void ShowProgressBar()
      {
         _progressWindow.ShowDialog(_kProgressBar, _cancellationTokenSource);
      }
      
      private void AddMessage(string message)
      {
         DispatcherHelper.RunAsync(() => { KTextBox.AppendText(message + Environment.NewLine); });
      }

      private void CloseProgressWindow()
      {
         DispatcherHelper.RunAsync(() =>
         {
            _kProgressBar.Value = 0;
            _progressWindow.Close();
         });
      }

      /// <summary>
      /// Set modefication value
      /// </summary>
      /// <param name="mod">data.Modification</param>
      /// <param name="isSelected">true if data.Modification is selected</param>
      private void SetModification(DeviceModificationE mod, bool isSelected, bool isEnum = false)
      {
         ushort modValue = 0x0000;

         if (!isEnum)
         {
            Data.Modification &= (ushort) (ModMask - DeviceModificationE.BAT2_ZIGBEE_MODIFICATION);
            Data.Modification &= (ushort) (ModMask - DeviceModificationE.BAT2_WIFI_MODIFICATION);
            return;
         }

         if (mod == DeviceModificationE.BAT2_WIFI_MODIFICATION)
         {
            Data.Modification &= (ushort) (ModMask - DeviceModificationE.BAT2_ZIGBEE_MODIFICATION);
         }
         else if (mod == DeviceModificationE.BAT2_ZIGBEE_MODIFICATION)
         {
            Data.Modification &= (ushort) (ModMask - DeviceModificationE.BAT2_WIFI_MODIFICATION);
         }
         modValue = (ushort) mod;

         if (isSelected)
         {
            Data.Modification |= modValue;
         }
         else
         {
            Data.Modification &= (ushort) (ModMask - modValue);
         }
      }

      #endregion

   }


}