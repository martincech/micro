﻿using System;
using System.Collections.ObjectModel;
using System.Configuration;
using Bat2Library.Connection.Interface.Domain;
using Utilities.Observable;

namespace Bat2Tester.ModelViews.Aplications
{
   public class MainWindowData : ObservableObject
   {
      private uint _serialNumber;
      private ushort _modification;
      private string _fwFileName;
      private string _fwTestFileName;
      private string _snFilePath;
      private ushort _actualModification;

      private bool _ezPortConnected;
      private ObservableCollection<string> _ezDeviceList;
      private string _ezDevice;
      private Bat2DeviceData _usbDevice;

      #region Property

      public UInt32 SerialNumber
      {
         get { return _serialNumber; }
         set { SetProperty(ref _serialNumber, value); }
      }

      public ushort Modification
      {
         get { return _modification; }
         set { SetProperty(ref _modification, value); }
      }

      public string FwFileName
      {
         get { return _fwFileName; }
         set
         {
            SetProperty(ref _fwFileName, value);
            Properties.Settings.Default.FileFirmware = value;
            Properties.Settings.Default.Save();
         }
      }

      public ObservableCollection<string> EzDeviceList
      {
         get { return _ezDeviceList; }
         set { SetProperty(ref _ezDeviceList, value); }
      }

      public string EzDevice
      {
         get { return _ezDevice; }
         set
         {
            SetProperty(ref _ezDevice, value);
            Properties.Settings.Default.DeviceName = value;
            Properties.Settings.Default.Save();
         }
      }

      public Bat2DeviceData UsbDevice
      {
         get { return _usbDevice; }
         set
         {
            SetProperty(ref _usbDevice, value);
         }
      }

      public string SnFilePath
      {
         get { return _snFilePath; }
         set
         {
            SetProperty(ref _snFilePath, value);
            Properties.Settings.Default.FolderSn = value;
            Properties.Settings.Default.Save();
         }
      }

      public int SnAdress
      {
         get { return int.Parse(ConfigurationManager.AppSettings["SnAddress"]); }
      }

      public int ModAdress
      {
         get { return int.Parse(ConfigurationManager.AppSettings["ModAddress"]); }
      }

      public bool EzPortConnected
      {
         get { return _ezPortConnected; }
         set
         {
            SetProperty(ref _ezPortConnected, value);
           
         }
      }

      public string FwTestFileName
      {
         get { return _fwTestFileName; }
         set
         {
            SetProperty(ref _fwTestFileName, value);
            Properties.Settings.Default.FileTestFirmware = value;
            Properties.Settings.Default.Save();
         }
      }

      public ushort ActualModification
      {
         get { return _actualModification; }
         set { SetProperty(ref _actualModification, value); }
      }

      #endregion
   }
}
