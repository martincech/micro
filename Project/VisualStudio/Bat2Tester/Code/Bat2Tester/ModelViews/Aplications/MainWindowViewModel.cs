﻿using System.ComponentModel;
using System.Windows.Input;
using Bat2Tester.ModelViews.Interface;
using Desktop.Wpf.Applications;
using Desktop.Wpf.Presentation;
using GUI;

namespace Bat2Tester.ModelViews.Aplications
{
   public class MainWindowViewModel
   {
      #region Private fields
     

      //EZ port
      private ICommand writeFwCommand;
      private ICommand eraseFwCommand;
      private ICommand writeSnCommand;
      private ICommand changeModCommand;
      private ICommand readSnCommand;
      private ICommand refreshCommand;
      private ICommand selectFileCommand;
      private ICommand selectPathCommand;

      //USB port
      private ICommand testKeyboardCommand;
      private ICommand testLoadCellCommand;
      private ICommand testAccuCommand;

      //Flash

      private MainWindowCode code;
      

      #endregion
      

      #region Public interfaces

      #region Constructor

      public MainWindowViewModel(IProgress progressWindow, IProgressBar progressBar, ITextBox textBox)
      {
         code = new MainWindowCode(progressWindow,progressBar,textBox);

         code.Data.PropertyChanged += Data_PropertyChanged;

         writeFwCommand = code.WriteFwCommand;
         eraseFwCommand = code.EraseFwCommand;
         writeSnCommand = code.WriteSnCommand;
         changeModCommand = code.ChangeModCommand;
         readSnCommand = code.ReadSnCommand;
         refreshCommand = code.RefreshCommand;
         selectFileCommand = code.SelectFileCommand;
         selectPathCommand = code.SelectPathCommand;

         testAccuCommand = code.TestAccuCommand;
         testKeyboardCommand = code.TestKeyboardCommand;
         testLoadCellCommand = code.TestLoadCellCommand;
      }

      void Data_PropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         UpdateCommands();
      }

      #endregion

      #region Commands

      public ICommand RefreshCommand
      {
         get
         {
            return refreshCommand;
         }
      }

      public ICommand ChangeModCommand
      {
         get
         {
            return changeModCommand;
         }
      }

      public ICommand WriteFwCommand
      {
         get
         {
            return writeFwCommand;
         }
      }

      public ICommand EraseFwCommand
      {
         get
         {
            return eraseFwCommand;
         }
      }

      public ICommand WriteSnCommand
      {
         get
         {
            return writeSnCommand;
         }
      }

      public ICommand ReadSnCommand
      {
         get
         {
            return readSnCommand;
         }
      }

      public ICommand SelectFileCommand
      {
         get
         {
            return selectFileCommand;
         }
      }

      public ICommand SelectPathCommand
      {
         get
         {
            return selectPathCommand;
         }
      }

      public ICommand TestKeyboardCommand
      {
         get
         {
            return testKeyboardCommand;
         }
      }

      public ICommand TestLoadCellCommand
      {
         get
         {
            return testLoadCellCommand;
         }
      }

      public ICommand TestAccuCommand
      {
         get
         {
            return testAccuCommand;
         }
      }

      #endregion 

      #region Property

      public MainWindowData Data
      {
         get
         {
            return code.Data;
         }
      }

      public ITextBox TextBox {
         get { return code.KTextBox; }
      }

      #endregion

      #endregion


      #region Private helpers

      /// <summary>
      /// Update commands
      /// </summary>
      private void UpdateCommands()
      {
         DispatcherHelper.RunAsync(() =>
         {
            //Ez port
            (WriteFwCommand as RelayCommand).RaiseCanExecuteChanged();
            (WriteSnCommand as RelayCommand).RaiseCanExecuteChanged();
            (ChangeModCommand as RelayCommand).RaiseCanExecuteChanged();
            (SelectFileCommand as RelayCommand).RaiseCanExecuteChanged();
            (EraseFwCommand as RelayCommand).RaiseCanExecuteChanged();
            (RefreshCommand as RelayCommand).RaiseCanExecuteChanged();
            (ReadSnCommand as RelayCommand).RaiseCanExecuteChanged();

            //USB port
            (TestKeyboardCommand as RelayCommand).RaiseCanExecuteChanged();
            (TestLoadCellCommand as RelayCommand).RaiseCanExecuteChanged();
            (TestAccuCommand as RelayCommand).RaiseCanExecuteChanged();
         });
      }

      #endregion
   }
   
}
