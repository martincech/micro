﻿using System;
using System.Globalization;
using System.Windows.Data;
using Bat2Library;

namespace Bat2Tester.ModelViews.Converters
{
   public class ModToBoolConverter : IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         var result = (short) value & (ushort)(DeviceModificationE)parameter;
         return result > 0;
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return Convert(value, targetType, parameter, culture);
      }

      #endregion
   }
}
