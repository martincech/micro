﻿using System.Threading;
using GUI;

namespace Bat2Tester.ModelViews.Interface
{
   public interface IProgress
   {
      bool? ShowDialog(IProgressBar progressBar, CancellationTokenSource tokenSource);
      void Close();
      void SetText(string text);
   }
}
