﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Media.Animation;
using GUI;
using Utilities.Extensions;

namespace Bat2Tester.Settings
{
 

   public class EntryStorege
   {
      public const string FILENAME = "Serial_number_modification.csv";
      private const string DESTINATION_FOLDER = @"\Veit\Bat2";
      private const string DEFAULT_DELIMITER = ";";

      private ITextBox textBox;
      private string delimetr;

      public EntryStorege(ITextBox textBox)
         : this(textBox, DEFAULT_DELIMITER)
      {  
      }

      public EntryStorege(ITextBox textBox, string delimetr)
      {
         this.textBox = textBox;
         this.delimetr = delimetr;
      }

      public string Path
      {
         get
         {
            if (Properties.Settings.Default.FolderSn.Equals(""))
            {
               Properties.Settings.Default.FolderSn =
                  Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + DESTINATION_FOLDER;
               Properties.Settings.Default.Save();
            }

            return Properties.Settings.Default.FolderSn;
         }
         set
         {
            Properties.Settings.Default.FolderSn = value;
            Properties.Settings.Default.Save();
         }
      }

      private string FilePath
      {
         get
         {
            return Path + @"\" + FILENAME;
         }
      }

      /// <summary>
      /// Write serial number and modification to file
      /// </summary>
      /// <param name="serialNumber">serial number</param>
      /// <param name="modification">modification</param>
      /// <returns></returns>
      public bool WriteEntry(uint serialNumber, ushort modification)
      {
         try
         {
            if (!File.Exists(FilePath))
            {
               WriteRecord(serialNumber, modification, true);
            }
            else
            {
               var data = Read(FilePath);
               var record = data.FirstOrDefault(x => x.SerialNumber == serialNumber);
               if (record == null)
               {
                  WriteRecord(serialNumber, modification, false);
               }
               else
               {
                  if (record.Modification != modification)
                  {
                     UpdateRecord(serialNumber, modification);
                  }
                  else
                  {
                     WriteMessage(Properties.Resources.MSG_SN_USED);
                  }
               }

            }
         }
         catch (Exception e)
         {
            WriteMessage(Properties.Resources.MSG_FILE_OPEN);
         }
         return true;
      }

      /// <summary>
      /// Update existing record modification
      /// </summary>
      /// <param name="serialNumber">serial number</param>
      /// <param name="modification">modification</param>
      private void UpdateRecord(uint serialNumber, ushort modification)
      {
         string[] lines = File.ReadAllLines(FilePath);
         int index = 0;
         foreach (var line in lines)
         {
            var values = line.Split(delimetr.ToCharArray());
            int sn;

            if (int.TryParse(values[Header.SERIAL_NUMBER_INDEX], NumberStyles.HexNumber,
               CultureInfo.InvariantCulture, out sn))
            {
               if (sn == serialNumber)
               {
                  index = lines.ToList().IndexOf(line);
                  break;
               }
            }
         }
         lines[index] = serialNumber.ToString("X") + delimetr + modification.ToString("X");
         File.Delete(FilePath);
         File.WriteAllLines(FilePath, lines);
         WriteMessage(string.Format(Properties.Resources.MSG_SN_UPDATED, serialNumber, modification));
      }

      /// <summary>
      /// Write new record
      /// </summary>
      /// <param name="serialNumber">serial number</param>
      /// <param name="modification">modification</param>
      /// <param name="createHeader">if true create header</param>
      private void WriteRecord(uint serialNumber, ushort modification, bool createHeader)
      {
         using (var writer = new StreamWriter(FilePath, true))
         {
            if (createHeader)
            {
               writer.WriteLine(Header.HEADER_SN + delimetr + Header.HEADER_MODIFICATION);
               WriteMessage(Properties.Resources.MSG_FILE_CREATE);
            }
            writer.WriteLine(serialNumber.ToString("X") + delimetr + modification.ToString("X"));
         }
      }

      private void WriteMessage(string message)
      {
         textBox.AppendText(message+Environment.NewLine);
      }

      /// <summary>
      /// Read file and parse serial numbers and modifications.
      /// </summary>
      /// <param name="fullName">path + file name</param>
      /// <returns>Parsed data</returns>
      private IEnumerable<Record> Read(string fullName)
      {
         var list = new List<Record>();
         using (var reader = new StreamReader(fullName))
         {
            while (!reader.EndOfStream)
            {
               var line = reader.ReadLine();
               if (line != null)
               {
                  var values = line.Split(delimetr.ToCharArray());

                  int sn;
                  byte mod;
                  var isSn = int.TryParse(values[Header.SERIAL_NUMBER_INDEX], NumberStyles.HexNumber, CultureInfo.InvariantCulture, out sn);
                  var isMod = byte.TryParse(values[Header.MODIFICATION_INDEX], NumberStyles.HexNumber, CultureInfo.InvariantCulture, out mod);

                  if (isSn && isMod)
                  {
                     list.Add(new Record { Modification = mod, SerialNumber = sn });
                  }
               }
            }
         }
         return list;
      }
   }
}
