/********************************************************************************
** Form generated from reading UI file 'bat2usbtest.ui'
**
** Created: Fri Nov 15 11:12:14 2013
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BAT2USBTEST_H
#define UI_BAT2USBTEST_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "Crt/crt.h"

QT_BEGIN_NAMESPACE

class Ui_Bat2UsbTestClass
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout_2;
    Crt *crt;
    QLabel *FileNameLabel;
    QPushButton *NvmFileSetPushButton;
    QVBoxLayout *verticalLayout_4;
    QPushButton *NvmCommitPushButton;
    QPushButton *NvmLoadPushButton;
    QPushButton *NvmSavePushButton;
    QPushButton *RemoteEnterPushButton;
    QSpacerItem *verticalSpacer;
    QSpacerItem *horizontalSpacer;
    QProgressBar *progressBar;
    QLabel *label;
    QLabel *label_2;
    QSpinBox *AddressSpinBox;
    QSpinBox *SizeSpinBox;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Bat2UsbTestClass)
    {
        if (Bat2UsbTestClass->objectName().isEmpty())
            Bat2UsbTestClass->setObjectName(QString::fromUtf8("Bat2UsbTestClass"));
        Bat2UsbTestClass->resize(853, 662);
        centralWidget = new QWidget(Bat2UsbTestClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout_3 = new QGridLayout(centralWidget);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        crt = new Crt(centralWidget);
        crt->setObjectName(QString::fromUtf8("crt"));

        gridLayout_2->addWidget(crt, 3, 1, 1, 6);

        FileNameLabel = new QLabel(centralWidget);
        FileNameLabel->setObjectName(QString::fromUtf8("FileNameLabel"));

        gridLayout_2->addWidget(FileNameLabel, 0, 3, 1, 1);

        NvmFileSetPushButton = new QPushButton(centralWidget);
        NvmFileSetPushButton->setObjectName(QString::fromUtf8("NvmFileSetPushButton"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(NvmFileSetPushButton->sizePolicy().hasHeightForWidth());
        NvmFileSetPushButton->setSizePolicy(sizePolicy);

        gridLayout_2->addWidget(NvmFileSetPushButton, 0, 1, 1, 1);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_4->setSizeConstraint(QLayout::SetDefaultConstraint);
        NvmCommitPushButton = new QPushButton(centralWidget);
        NvmCommitPushButton->setObjectName(QString::fromUtf8("NvmCommitPushButton"));

        verticalLayout_4->addWidget(NvmCommitPushButton);

        NvmLoadPushButton = new QPushButton(centralWidget);
        NvmLoadPushButton->setObjectName(QString::fromUtf8("NvmLoadPushButton"));

        verticalLayout_4->addWidget(NvmLoadPushButton);

        NvmSavePushButton = new QPushButton(centralWidget);
        NvmSavePushButton->setObjectName(QString::fromUtf8("NvmSavePushButton"));

        verticalLayout_4->addWidget(NvmSavePushButton);

        RemoteEnterPushButton = new QPushButton(centralWidget);
        RemoteEnterPushButton->setObjectName(QString::fromUtf8("RemoteEnterPushButton"));

        verticalLayout_4->addWidget(RemoteEnterPushButton);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer);


        gridLayout_2->addLayout(verticalLayout_4, 3, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer, 0, 5, 1, 1);

        progressBar = new QProgressBar(centralWidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(0);

        gridLayout_2->addWidget(progressBar, 0, 6, 1, 1);

        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_2->addWidget(label, 1, 1, 1, 1);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_2->addWidget(label_2, 1, 2, 1, 1);

        AddressSpinBox = new QSpinBox(centralWidget);
        AddressSpinBox->setObjectName(QString::fromUtf8("AddressSpinBox"));
        AddressSpinBox->setMaximum(999999999);

        gridLayout_2->addWidget(AddressSpinBox, 2, 1, 1, 1);

        SizeSpinBox = new QSpinBox(centralWidget);
        SizeSpinBox->setObjectName(QString::fromUtf8("SizeSpinBox"));
        SizeSpinBox->setMaximum(999999999);

        gridLayout_2->addWidget(SizeSpinBox, 2, 2, 1, 1);


        gridLayout_3->addLayout(gridLayout_2, 0, 0, 1, 1);

        Bat2UsbTestClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(Bat2UsbTestClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 853, 21));
        Bat2UsbTestClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(Bat2UsbTestClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        Bat2UsbTestClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(Bat2UsbTestClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        Bat2UsbTestClass->setStatusBar(statusBar);

        retranslateUi(Bat2UsbTestClass);

        QMetaObject::connectSlotsByName(Bat2UsbTestClass);
    } // setupUi

    void retranslateUi(QMainWindow *Bat2UsbTestClass)
    {
        Bat2UsbTestClass->setWindowTitle(QApplication::translate("Bat2UsbTestClass", "Bat2UsbTest", 0, QApplication::UnicodeUTF8));
        FileNameLabel->setText(QString());
        NvmFileSetPushButton->setText(QApplication::translate("Bat2UsbTestClass", "Open file", 0, QApplication::UnicodeUTF8));
        NvmCommitPushButton->setText(QApplication::translate("Bat2UsbTestClass", "Nvm commit", 0, QApplication::UnicodeUTF8));
        NvmLoadPushButton->setText(QApplication::translate("Bat2UsbTestClass", "Nvm load", 0, QApplication::UnicodeUTF8));
        NvmSavePushButton->setText(QApplication::translate("Bat2UsbTestClass", "Nvm save", 0, QApplication::UnicodeUTF8));
        RemoteEnterPushButton->setText(QApplication::translate("Bat2UsbTestClass", "Enter remote", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Bat2UsbTestClass", "Address", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Bat2UsbTestClass", "Size", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Bat2UsbTestClass: public Ui_Bat2UsbTestClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BAT2USBTEST_H
