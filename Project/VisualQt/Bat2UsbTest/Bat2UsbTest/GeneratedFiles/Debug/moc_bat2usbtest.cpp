/****************************************************************************
** Meta object code from reading C++ file 'bat2usbtest.h'
**
** Created: Fri Nov 15 11:12:14 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../bat2usbtest.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'bat2usbtest.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Bat2UsbTest[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      21,   13,   12,   12, 0x09,
      35,   12,   12,   12, 0x08,
      68,   12,   12,   12, 0x08,
      99,   12,   12,   12, 0x08,
     130,   12,   12,   12, 0x08,
     164,   12,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Bat2UsbTest[] = {
    "Bat2UsbTest\0\0Percent\0Progress(int)\0"
    "on_NvmCommitPushButton_clicked()\0"
    "on_NvmSavePushButton_clicked()\0"
    "on_NvmLoadPushButton_clicked()\0"
    "on_NvmFileSetPushButton_clicked()\0"
    "on_RemoteEnterPushButton_clicked()\0"
};

void Bat2UsbTest::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Bat2UsbTest *_t = static_cast<Bat2UsbTest *>(_o);
        switch (_id) {
        case 0: _t->Progress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_NvmCommitPushButton_clicked(); break;
        case 2: _t->on_NvmSavePushButton_clicked(); break;
        case 3: _t->on_NvmLoadPushButton_clicked(); break;
        case 4: _t->on_NvmFileSetPushButton_clicked(); break;
        case 5: _t->on_RemoteEnterPushButton_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Bat2UsbTest::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Bat2UsbTest::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_Bat2UsbTest,
      qt_meta_data_Bat2UsbTest, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Bat2UsbTest::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Bat2UsbTest::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Bat2UsbTest::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Bat2UsbTest))
        return static_cast<void*>(const_cast< Bat2UsbTest*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int Bat2UsbTest::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
