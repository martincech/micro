#include "bat2usbtest.h"
#include "Usb/Bat2Usb.h"
#include "System/System.h"
#include <QFileDialog.h>
#include <QFile.h>
#include "Memory/Nvm.h"
#include "Performance/Performance.h"

#define SetOk()      _dump->printf("OK\r\n");
#define SetError()   _dump->printf("Error\r\n");


#define NVM_FLASH_START           0xC0000000

Bat2UsbTest::Bat2UsbTest(QWidget *parent, Qt::WFlags flags)
    : QMainWindow(parent, flags)
{
   ui.setupUi(this);
   _dump = new CrtDump( ui.crt);      // initialize CrtDump utility
   _dump->setMode( CrtDump::MIXED);   // show mixed dump
   _dump->setEnable( YES);

   HidNative = new THidNative();
   HidNative->CrtSet(_dump);

   Usb = new TBat2Usb(HidNative, ui.crt);
   
   QObject::connect(Usb, SIGNAL(Progress(int)), this, SLOT(Progress(int)));

   Nvm = (byte *)malloc(NVM_SIZE);
}

Bat2UsbTest::~Bat2UsbTest()
{
   delete _dump;
   delete Usb;
   delete Nvm;
}

void Bat2UsbTest::Progress( int Percent) {
   ui.progressBar->setValue(Percent);
}

void Bat2UsbTest::on_NvmCommitPushButton_clicked( void) {
   if(!Usb->NvmCommit()) {
      SetError();
      return;
   }
   SetOk();
}

void Bat2UsbTest::on_NvmSavePushButton_clicked( void) {
   int Address = ui.AddressSpinBox->value();
   int Size = ui.SizeSpinBox->value();
   NvmSetup(FileName.toAscii().data());
   NvmLoad(Address, Nvm, Size);
int64 TimeBefore;
int64 TimeAfter;
   TimeBefore = TimerGet();
   if(!Usb->NvmSave(Address, Nvm, Size)) {
      SetError();
      return;
   }
   TimeAfter = TimerGet();
   double ElapsedTime = TimerMiliseconds(TimeAfter - TimeBefore) / 1000;
   ElapsedTime++;
   _dump->printf("Elapsed time: %0.2f s, speed %0.2f kB/s\r\n", ElapsedTime, (double)Size / ElapsedTime / 1000);
   SetOk();
}

void Bat2UsbTest::on_NvmLoadPushButton_clicked( void) {
   int Address = ui.AddressSpinBox->value();
   int Size = ui.SizeSpinBox->value();
int64 TimeBefore;
int64 TimeAfter;
   TimeBefore = TimerGet();
   if(!Usb->NvmLoad(Address, Nvm, Size)) {
      SetError();
      return;
   }
   TimeAfter = TimerGet();
   

   NvmSave(Address, Nvm, Size);
   NvmShutdown(FileName.toAscii().data());
   double ElapsedTime = TimerMiliseconds(TimeAfter - TimeBefore) / 1000;
   ElapsedTime++;
   _dump->printf("Elapsed time: %0.2f s, speed %0.2f kB/s\r\n", ElapsedTime, (double)Size / ElapsedTime / 1000);
   SetOk();
}

void Bat2UsbTest::on_NvmFileSetPushButton_clicked( void) {
   FileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Files (*.*)"));
   ui.FileNameLabel->setText(FileName);
}

void Bat2UsbTest::on_RemoteEnterPushButton_clicked( void) {
   if(!Usb->RemoteEnter()) {
      SetError();
      return;
   }
   SetOk();
}