#ifndef BAT2USBTEST_H
#define BAT2USBTEST_H

#include <QtGui/QMainWindow>
#include "ui_bat2usbtest.h"
#include "Bat2Usb.h"
#include "Crt/CrtDump.h"
#include "Usb/HidNative/HidNative.h"
#include <QString.h>

class Bat2UsbTest : public QMainWindow
{
    Q_OBJECT

public:
    Bat2UsbTest(QWidget *parent = 0, Qt::WFlags flags = 0);
    ~Bat2UsbTest();

protected slots:
   void Progress( int Percent);

private slots:
   void on_NvmCommitPushButton_clicked( void);
   void on_NvmSavePushButton_clicked( void);
   void on_NvmLoadPushButton_clicked( void);
   void on_NvmFileSetPushButton_clicked( void);
   void on_RemoteEnterPushButton_clicked( void);

private:
    Ui::Bat2UsbTestClass ui;
    TBat2Usb *Usb;
    THidNative *HidNative;
    CrtDump *_dump;
    QString FileName;
    byte *Nvm;
};

#endif // BAT2USBTEST_H
