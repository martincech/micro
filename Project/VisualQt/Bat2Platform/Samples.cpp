//******************************************************************************
//
//   Samples.cpp   Weighing samples loader
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "Samples.h"
#include <QFile>
#include <QFileInfo>
#include <QStringList>
#include <QTextStream>

#define CSV_SEPARATOR      QChar( ',')
#define CSV_LINE_MAX       512       // maximum characters per line

#define SAMPLES_COUNT_MAX  (4 * 24 * 3600 * 10)

//------------------------------------------------------------------------------
//   Constructor
//------------------------------------------------------------------------------

Samples::Samples()
{
   _samples      = new TWeightSample[ SAMPLES_COUNT_MAX];
   _samplesCount = 0;
} // MainWindow

//------------------------------------------------------------------------------
//   Destructor
//------------------------------------------------------------------------------

Samples::~Samples()
{
   if( _samples){
      delete [] _samples;
   }
} // ~MainWindow

//------------------------------------------------------------------------------
//   Load
//------------------------------------------------------------------------------

bool Samples::load( QString fileName)
{
   _samplesCount = 0;                            // clear samples count
   // load file :
   QFile file( fileName);
   if( !file.open( QIODevice::ReadOnly | QIODevice::Text)){
      return( false);
   }
   QTextStream stream( &file);
   QString     line;
   QStringList items;
   while( !stream.atEnd()){
      if( _samplesCount >= SAMPLES_COUNT_MAX){
         break;                                  // stop reading at limit
      }
      line  = stream.readLine( CSV_LINE_MAX);
      items = line.split( CSV_SEPARATOR);
      if( items.count() == 0){
         continue;                               // empty line
      }
      if( items.count() < 2){
         return( false);                         // wrong line
      }
      if( items[ 0].isEmpty() || items[ 1].isEmpty()){
         return( false);                         // empty item
      }
      _samples[ _samplesCount].Timestamp = items[ 0].toLongLong();
      _samples[ _samplesCount].Weight    = items[ 1].toInt();
      _samplesCount++;
   }
   if( _samplesCount == 0){
      return( false);
   }
   return( true);
} // load

//------------------------------------------------------------------------------
//   Count
//------------------------------------------------------------------------------

int Samples::count()
{
   return( _samplesCount);
} // count

//------------------------------------------------------------------------------
//   Samples
//------------------------------------------------------------------------------

TWeightSample *Samples::samples()
{
   return( _samples);
} // samples
