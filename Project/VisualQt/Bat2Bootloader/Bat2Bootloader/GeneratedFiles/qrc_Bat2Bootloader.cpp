/****************************************************************************
** Resource object code
**
** Created: Wed Sep 25 13:12:02 2013
**      by: The Resource Compiler for Qt version 4.8.0
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <QtCore/qglobal.h>

QT_BEGIN_NAMESPACE

QT_END_NAMESPACE


int QT_MANGLE_NAMESPACE(qInitResources_Bat2Bootloader)()
{
    return 1;
}

Q_CONSTRUCTOR_FUNCTION(QT_MANGLE_NAMESPACE(qInitResources_Bat2Bootloader))

int QT_MANGLE_NAMESPACE(qCleanupResources_Bat2Bootloader)()
{
    return 1;
}

Q_DESTRUCTOR_FUNCTION(QT_MANGLE_NAMESPACE(qCleanupResources_Bat2Bootloader))

