/********************************************************************************
** Form generated from reading UI file 'Bat2Bootloader.ui'
**
** Created: Wed Sep 25 13:12:02 2013
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BAT2BOOTLOADER_H
#define UI_BAT2BOOTLOADER_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Bat2BootloaderClass
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QPushButton *LoadFileButton;
    QLabel *FileNameLabel;
    QPushButton *FlashButton;
    QLabel *StatusLabel;
    QLabel *VersionLabel;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Bat2BootloaderClass)
    {
        if (Bat2BootloaderClass->objectName().isEmpty())
            Bat2BootloaderClass->setObjectName(QString::fromUtf8("Bat2BootloaderClass"));
        Bat2BootloaderClass->resize(546, 142);
        centralWidget = new QWidget(Bat2BootloaderClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        LoadFileButton = new QPushButton(centralWidget);
        LoadFileButton->setObjectName(QString::fromUtf8("LoadFileButton"));

        gridLayout->addWidget(LoadFileButton, 0, 0, 1, 1);

        FileNameLabel = new QLabel(centralWidget);
        FileNameLabel->setObjectName(QString::fromUtf8("FileNameLabel"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(FileNameLabel->sizePolicy().hasHeightForWidth());
        FileNameLabel->setSizePolicy(sizePolicy);

        gridLayout->addWidget(FileNameLabel, 0, 1, 1, 1);

        FlashButton = new QPushButton(centralWidget);
        FlashButton->setObjectName(QString::fromUtf8("FlashButton"));

        gridLayout->addWidget(FlashButton, 1, 0, 1, 1);

        StatusLabel = new QLabel(centralWidget);
        StatusLabel->setObjectName(QString::fromUtf8("StatusLabel"));
        sizePolicy.setHeightForWidth(StatusLabel->sizePolicy().hasHeightForWidth());
        StatusLabel->setSizePolicy(sizePolicy);

        gridLayout->addWidget(StatusLabel, 1, 1, 1, 1);

        VersionLabel = new QLabel(centralWidget);
        VersionLabel->setObjectName(QString::fromUtf8("VersionLabel"));

        gridLayout->addWidget(VersionLabel, 2, 0, 1, 2);

        Bat2BootloaderClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(Bat2BootloaderClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 546, 21));
        Bat2BootloaderClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(Bat2BootloaderClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        Bat2BootloaderClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(Bat2BootloaderClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        Bat2BootloaderClass->setStatusBar(statusBar);

        retranslateUi(Bat2BootloaderClass);

        QMetaObject::connectSlotsByName(Bat2BootloaderClass);
    } // setupUi

    void retranslateUi(QMainWindow *Bat2BootloaderClass)
    {
        Bat2BootloaderClass->setWindowTitle(QApplication::translate("Bat2BootloaderClass", "Bat2Bootloader", 0, QApplication::UnicodeUTF8));
        LoadFileButton->setText(QApplication::translate("Bat2BootloaderClass", "Load file", 0, QApplication::UnicodeUTF8));
        FileNameLabel->setText(QApplication::translate("Bat2BootloaderClass", "?", 0, QApplication::UnicodeUTF8));
        FlashButton->setText(QApplication::translate("Bat2BootloaderClass", "Flash", 0, QApplication::UnicodeUTF8));
        StatusLabel->setText(QApplication::translate("Bat2BootloaderClass", "OK", 0, QApplication::UnicodeUTF8));
        VersionLabel->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Bat2BootloaderClass: public Ui_Bat2BootloaderClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BAT2BOOTLOADER_H
