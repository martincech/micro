#include "Bat2Bootloader.h"
#include "ui_Bat2Bootloader.h"
#include <QString>
#include <QFileDialog>
#include <QApplication>
#include "Bootloader.h"

#define HEX_MAX_SIZE       10 * 1024 * 1024 // 10 MB

Bat2Bootloader::Bat2Bootloader(QWidget *parent, Qt::WFlags flags)
    : QMainWindow(parent, flags)
{
   ui.setupUi(this);
   ui.FlashButton->setEnabled(false);
}

Bat2Bootloader::~Bat2Bootloader()
{
}

void Bat2Bootloader::on_LoadFileButton_clicked( void) {
   FileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Files (*.fwc)"));

   ui.FileNameLabel->setText(FileName);
   ui.FlashButton->setEnabled(true);
}

void Bat2Bootloader::on_FlashButton_clicked( void) {
   TBootloader Bootloader("Bootloader");

   TDeviceVersion BootloaderVersion;

   ui.VersionLabel->setText("");

   if(!Bootloader.VersionGet(&BootloaderVersion)) {
      ui.StatusLabel->setText("Error");
      return;
   }

   QString Version;
   switch(BootloaderVersion.Class) {
      case DEVICE_SCALE_COMPACT:
         Version = "Scale compact";
         break;

      default:
         Version = "Unknown";
         break;
   }
   Version += QString(" | S/N: %1").arg(BootloaderVersion.SerialNumber);
   Version += QString(" | HW: %1.%2").arg(BootloaderVersion.Hardware >> 8).arg(BootloaderVersion.Hardware & 0xFF);
   Version += QString(" | Bootloader SW: %1.%2").arg(BootloaderVersion.Software >> 8).arg(BootloaderVersion.Software & 0xFF);


   ui.VersionLabel->setText(Version);

   QFile File(FileName);
   File.open(QIODevice::ReadOnly);

   QByteArray blob = File.readAll();

   ui.StatusLabel->setText("Running");
   
   if(!Bootloader.Download((byte *)blob.data(), blob.size())) {
      ui.StatusLabel->setText("Error running");
      return;
   }
   if(!Bootloader.StartApplication()) {
      ui.StatusLabel->setText("Error starting application");
      return;
   }
   ui.StatusLabel->setText("OK");
}