#ifndef Bat2Bootloader_H
#define Bat2Bootloader_H

#include <QtGui/QMainWindow>
#include "ui_Bat2Bootloader.h"
#include "Hex/HexFile.h"

class Bat2Bootloader : public QMainWindow
{
    Q_OBJECT

public:
    Bat2Bootloader(QWidget *parent = 0, Qt::WFlags flags = 0);
    ~Bat2Bootloader();

private:
   Ui::Bat2BootloaderClass ui;
   QString FileName;

private slots:
   void on_LoadFileButton_clicked( void);
   void on_FlashButton_clicked( void);
};

#endif // BAT2FLASH_H
