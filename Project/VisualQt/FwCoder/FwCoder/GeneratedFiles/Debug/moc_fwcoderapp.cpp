/****************************************************************************
** Meta object code from reading C++ file 'fwcoderapp.h'
**
** Created: Thu Sep 19 15:05:14 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../fwcoderapp.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'fwcoderapp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_FwCoderApp[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x08,
      46,   11,   11,   11, 0x08,
      78,   11,   11,   11, 0x08,
     109,   11,   11,   11, 0x08,
     140,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_FwCoderApp[] = {
    "FwCoderApp\0\0on_OpenConfigPushButton_clicked()\0"
    "on_OpenFilePushButton_clicked()\0"
    "on_EncryptPushButton_clicked()\0"
    "on_DecryptPushButton_clicked()\0"
    "on_InterpretPushButton_clicked()\0"
};

void FwCoderApp::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        FwCoderApp *_t = static_cast<FwCoderApp *>(_o);
        switch (_id) {
        case 0: _t->on_OpenConfigPushButton_clicked(); break;
        case 1: _t->on_OpenFilePushButton_clicked(); break;
        case 2: _t->on_EncryptPushButton_clicked(); break;
        case 3: _t->on_DecryptPushButton_clicked(); break;
        case 4: _t->on_InterpretPushButton_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData FwCoderApp::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject FwCoderApp::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_FwCoderApp,
      qt_meta_data_FwCoderApp, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FwCoderApp::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FwCoderApp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FwCoderApp::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FwCoderApp))
        return static_cast<void*>(const_cast< FwCoderApp*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int FwCoderApp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
