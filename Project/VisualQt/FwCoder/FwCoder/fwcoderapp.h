#ifndef FWCODERAPP_H
#define FWCODERAPP_H

#include <QtGui/QMainWindow>
#include "ui_fwcoderapp.h"
#include "FwCoder/FwCoder.h"

class FwCoderApp : public QMainWindow
{
    Q_OBJECT

public:
    FwCoderApp(QWidget *parent = 0, Qt::WFlags flags = 0);
    ~FwCoderApp();

private:
    Ui::FwCoderAppClass ui;
    TFwCoder *Coder;
    QString ConfigFileName;
    QString InputFileName;
    QString OutputFileName;

private slots:
   void on_OpenConfigPushButton_clicked( void);
   void on_OpenFilePushButton_clicked( void);
   void on_EncryptPushButton_clicked( void);
   void on_DecryptPushButton_clicked( void);
   void on_InterpretPushButton_clicked( void);
};

#endif // FWCODERAPP_H
