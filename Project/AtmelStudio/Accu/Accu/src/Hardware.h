//******************************************************************************////   Sleep.h         Low power controller//   Version 1.0     (c) VEIT Electronics////******************************************************************************#ifndef __Hardware_H__   #define __Hardware_H__   #include <asf.h>#define F_CPU              12000000ull#define F_PER              F_CPU//------------------------------------------------------------------------------
//   Iic master 0
//------------------------------------------------------------------------------#define IIC_PORT           PORTE#define IIC_SDA_PIN        (1 << 0)#define IIC_SCL_PIN        (1 << 1)#define IIC_SDA_PIN_CTRL   PIN0CTRL#define IIC_SCL_PIN_CTRL   PIN1CTRL#define IIC_WAIT           1#define IicPortInit()      IicSdaSet(); IicSclSet(); IIC_PORT.DIRSET = IIC_SDA_PIN | IIC_SCL_PIN; IIC_PORT.IIC_SDA_PIN_CTRL |= PORT_OPC_WIREDANDPULL_gc; IIC_PORT.IIC_SCL_PIN_CTRL |= PORT_OPC_WIREDANDPULL_gc
#define IicSdaSet()        IIC_PORT.OUTSET = IIC_SDA_PIN
#define IicSclSet()        IIC_PORT.OUTSET = IIC_SCL_PIN#define IicSdaClr()        IIC_PORT.OUTCLR = IIC_SDA_PIN#define IicSclClr()        IIC_PORT.OUTCLR = IIC_SCL_PIN#define IicSda()           (IIC_PORT.IN & IIC_SDA_PIN)#include "Iic/Iic.h"#define MAX17047_IIC_CHANNEL     IIC_IIC0#define BQ2425X_IIC_CHANNEL      IIC_IIC0//-----------------------------------------------------------------------------
//   Bq2425x
//------------------------------------------------------------------------------#define BQ2425X_PORT            PORTA#define BQ2425X_CE_PIN         (1 << 0)#define BQ2425X_PG_PIN         (1 << 2)#define BQ2425X_PG_PIN_CTRL     PIN2CTRL#define BQ2425X_PG_PIN_INTMASK  INT0MASK#define Bq2425xPg()           (BQ2425X_PORT.IN & BQ2425X_PG_PIN)#define Bq2425xPortInit()   BQ2425X_PORT.BQ2425X_PG_PIN_CTRL = PORT_OPC_PULLUP_gc | PORT_ISC_BOTHEDGES_gc; \
                            BQ2425X_PORT.BQ2425X_PG_PIN_INTMASK |= BQ2425X_PG_PIN; \
                            BQ2425X_PORT.INTCTRL = PORT_INT0LVL_LO_gc
                            
#define Bq2425xVinHandler()  ISR(PORTA_INT0_vect)#define Bq2425xCeAssert()    BQ2425X_PORT.DIRSET = BQ2425X_CE_PIN#define Bq2425xCeDeassert()  BQ2425X_PORT.DIRCLR = BQ2425X_CE_PIN//------------------------------------------------------------------------------
//   LEDs
//------------------------------------------------------------------------------#define LED_PORT     PORTD#define LED0         (1 << 0)#define LED1         (1 << 1)#define LED2         (1 << 2)#define LED3         (1 << 3)#define LED_MASK     (LED0 | LED1 | LED2 | LED3);#define LedInit()        LedSet( 0); LED_PORT.DIRSET = LED_MASK;#define LedSet( Leds)    LED_PORT.OUTSET = (~(Leds)) & LED_MASK; \                         LED_PORT.OUTCLR = ( (Leds)) & LED_MASK//------------------------------------------------------------------------------
//   Button
//------------------------------------------------------------------------------#define BUTTON_PORT         PORTB#define BUTTON_PIN         (1 << 2)#define BUTTON_PIN_CTRL     PIN2CTRL#define BUTTON_PIN_INTMASK  INT0MASK#define ButtonHandler()     ISR(PORTB_INT0_vect)#define ButtonPushed()     (!(BUTTON_PORT.IN & BUTTON_PIN))#define ButtonInit()   BUTTON_PORT.BUTTON_PIN_CTRL = PORT_OPC_PULLUP_gc | PORT_ISC_BOTHEDGES_gc; \
                       BUTTON_PORT.BUTTON_PIN_INTMASK |= BUTTON_PIN; \
                       BUTTON_PORT.INTCTRL = PORT_INT0LVL_LO_gc//------------------------------------------------------------------------------
//   Iic slave
//------------------------------------------------------------------------------#define IIC_SLAVE_SLEEP_CONTROL//------------------------------------------------------------------------------
//   Rtc
//------------------------------------------------------------------------------#define RTC_PERIOD      600000ull    // ms#define RtcHandler()    ISR(RTC_OVF_vect)
#define TIMER_PERIOD    100ull    // ms#define ACCU_BACKUP_PERIOD      RTC_PERIOD //------------------------------------------------------------------------------
//   SMBus
//------------------------------------------------------------------------------#define SMBUS_PORT   PORTC#define SMBUS_EN     (1 << 7)#define SMBusEnableInit()   SMBusDisable(); SMBUS_PORT.DIRSET = SMBUS_EN#define SMBusEnable()    SMBUS_PORT.OUTSET = SMBUS_EN#define SMBusDisable()   SMBUS_PORT.OUTCLR = SMBUS_EN#endif